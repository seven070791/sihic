/*
 *Este es el document ready
 */
 $(function()
 {

  
     let numeroHistorial  =$('#numeroHistorial').val();
     let id_gestacion  =$('#id_gestacion').val();
     let id_partos  =$('#id_partos').val();
     let id_abortos  =$('#id_abortos').val();
     let id_cesarias  =$('#id_cesarias').val();
     let tipo_anticonceptivo  =$('#tipo_anticonceptivo').val();
     let tipo_enfermedad_ts  =$('#tipo_enfermedad_ts').val();
    
      listar_psicologia_primaria(numeroHistorial);
      listar_metodos_Anticonceptivos(Event,tipo_anticonceptivo)
      listar_enfermedades_sexuales(Event,tipo_enfermedad_ts)
      listar_NumerosR_Gestacion(Event,id_gestacion)
      listar_NumerosR_Partos(Event,id_partos)
      listar_NumerosR_Cesarias(Event,id_cesarias)
      listar_NumerosR_Abortos(Event,id_abortos)
      //$("#textimpresiond").val('');
      let impresiondiagnostica=$('#textimpresiond').val();   
      listar_Historial_informativo(Event);
      listar_Historial_informativo_Psico(Event);
      listar_Historial_informativo_MI(Event);
      listar_Historial_informativo_PP(Event);
      listar_Historial_informativo_GB(Event);

 });
 


 function  listar_Historial_informativo_Psico(e,id)
 {
 
     e.preventDefault;
     
       url='/Listar_Historial_Informativo';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#aciones_Psicol').empty();
      $('#aciones_Psicol').append('<option value=0 selected disabled >Historial </option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#aciones_Psicol').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id)
              
               {
                    $('#aciones_Psicol').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#aciones_Psicol').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }





 

 function  listar_Historial_informativo_GB(e,id)
 {
 
     e.preventDefault;
     
       url='/Listar_Historial_Informativo';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#aciones_gb').empty();
      $('#aciones_gb').append('<option value=0 selected disabled >Seleccione </option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#aciones_gb').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id)
              
               {
                    $('#aciones_gb').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#aciones_gb').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }


 function  listar_Historial_informativo_PP(e,id)
 {
 
     e.preventDefault;
     
       url='/Listar_Historial_Informativo';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#aciones_pp').empty();
      $('#aciones_pp').append('<option value=0 selected disabled >Seleccione </option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#aciones_pp').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id)
              
               {
                    $('#aciones_pp').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#aciones_pp').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }


 function  listar_Historial_informativo_MI(e,id)
 {
 
     e.preventDefault;
     
       url='/Listar_Historial_Informativo';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#aciones_mi').empty();
      $('#aciones_mi').append('<option value=0 selected disabled >Seleccione </option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#aciones_mi').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id)
              
               {
                    $('#aciones_mi').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#aciones_mi').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }


 function  listar_Historial_informativo(e,id)
 {
 
     e.preventDefault;
     
       url='/Listar_Historial_Informativo';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#aciones').empty();
      $('#aciones').append('<option value=0 selected disabled >Seleccione </option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#aciones').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id)
              
               {
                    $('#aciones').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#aciones').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }



 function  listar_NumerosR_Gestacion(e,id_gestacion)
 {
 
     e.preventDefault;
     
       url='/Listar_Numeros_Romanos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#gestacion').empty();
      $('#gestacion').append('<option value=0>Seleccione</option>');     
      if(id_gestacion===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#gestacion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id_gestacion)
              
               {
                    $('#gestacion').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#gestacion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }


 function  listar_NumerosR_Partos(e,id_partos)
 {
 
     e.preventDefault;
     
       url='/Listar_Numeros_Romanos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#partos').empty();
      $('#partos').append('<option value=0>Seleccione</option>');     
      if(id_partos===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#partos').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id_partos)
              
               {
                    $('#partos').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#partos').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }



 function  listar_NumerosR_Cesarias(e,id_cesarias)
 {
 
     e.preventDefault;
     
       url='/Listar_Numeros_Romanos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#cesarias').empty();
      $('#cesarias').append('<option value=0>Seleccione</option>');     
      if(id_cesarias===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cesarias').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id_cesarias)
              
               {
                    $('#cesarias').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#cesarias').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }


 function  listar_NumerosR_Abortos(e,id_abortos)
 {
 
     e.preventDefault;
     
       url='/Listar_Numeros_Romanos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#abortos').empty();
      $('#abortos').append('<option value=0>Seleccione</option>');     
      if(id_abortos===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#abortos').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
 
               if(item.id===id_abortos)
              
               {
                    $('#abortos').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#abortos').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }



 function  listar_enfermedades_sexuales(e,tipo_enfermedad_ts)
 {
 
     e.preventDefault;
     
       url='/ListarEnfermedadesSexuales';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#tipo_ets').empty();
      $('#tipo_ets').append('<option value=0>Seleccione</option>');     
      if(tipo_enfermedad_ts===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#tipo_ets').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
               document.getElementById("tipo_ets").disabled=false;
 
               if(item.id===tipo_enfermedad_ts)
              
              
               {
                    $('#tipo_ets').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#tipo_ets').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }


 $("#anticon").on('change', function()
 {
     let anticonceptivo=$("#anticon").val();
     if (anticonceptivo=='1') {
       $("#tipo_anticoceptivo").removeAttr('disabled');   
     }
     else{
          document.getElementById('tipo_anticoceptivo').disabled = true;
     }
 }); 


 function  listar_metodos_Anticonceptivos(e,tipo_anticonceptivo)
 {
 
     e.preventDefault;
     
       url='/listar_metodos_Anticonceptivos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
 
 if(data.length>=1)
 {
      $('#tipo_anticoceptivo').empty();
      $('#tipo_anticoceptivo').append('<option value=0>Seleccione</option>');     
      if(tipo_anticonceptivo===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#tipo_anticoceptivo').append('<option value='+item.id+'>'+item.descripcion+'</option>');
 
          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              
               document.getElementById("tipo_anticoceptivo").disabled=false;
              
               
               if(item.id===tipo_anticonceptivo)
              
               {
                    $('#tipo_anticoceptivo').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#tipo_anticoceptivo').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }



 $("#ets").on('change', function()
 {
     let tipo_ets=$("#ets").val();
     if (tipo_ets=='1') {
       $("#tipo_ets").removeAttr('disabled');   
     }
     else{
          document.getElementById('tipo_ets').disabled = true;
     }
    
     
 }); 

//********************************************************************
 
  $('.cursor').on('click',function(e)
 { 
    //$('.texto').hide();
   $('#enfermedad_actual').val('');
   let campo = document.getElementById('enfermedad_actual');
   campo.style.color="#242323";
   campo.style.fontWeight='normal';

 });

 $(document).on('click','#btnActualizar_signos_mg', function(e)
 {
      var cedula      =$('#cedulab').val(); 
      let motivo_consulta    =$('#motivo_consulta').val(); 
      var id_especialidad=$('#id_especialidad').val();
      var id_historial_medico=$('#id_historial_medico').val()
      var n_historial=$('#numeroHistorial').val();
      var id_medico=$('#medico_id').val();
      var peso=$('#peso').val();
      var talla=$('#talla').val();
      var spo2=$('#spo2').val();
      var frecuencia_c=$('#frecuencia_c').val();
      var frecuencia_r=$('#frecuencia_r').val();
      var temperatura=$('#temperatura').val();
      var ta_alta=$('#ta_alta').val();
      var ta_baja=$('#ta_baja').val(); 
      var fecha_asistencia_normal=$('#fecha_del_dia_mi').val();
      let fechaconvertida=moment(fecha_asistencia_normal,'DD-MM-YYYY'); 
      fecha_asistencia=fechaconvertida.format('YYYY-MM-DD');
      var id_consulta=$('#id_consulta').val();
      var imc=$('#imc').val();
      let user_id=$('#id_user').val();
      let tipo_beneficiario='C';
      var url='/actualizar_signos_vitales_citas';
      if (peso=='') 
      {
           peso=0; 
      }
      if (talla=='') 
      {
           talla=0; 
           }
      if (spo2=='') 
      {
           spo2=0; 
      }if (frecuencia_c=='') 
      {
           frecuencia_c=0; 
      } if (frecuencia_r=='')
      {
           frecuencia_r=0; 
      }if (temperatura=='') 
      {
           temperatura=0; 
      } if (ta_alta=='') 
      {
           ta_alta=0; 
      } if (ta_baja=='')
      {
           ta_baja=0; 
      }if (imc=='')
      {
           imc=0; 
      }
 
      var data=
      {
      fecha_asistencia:fecha_asistencia,
      n_historial   :n_historial,
      id_medico     :id_medico,
      peso          :peso,               
      talla         :talla,
      spo2          :spo2,
      frecuencia_c  :frecuencia_c,
      frecuencia_r  :frecuencia_r,
      temperatura   :temperatura,
      ta_alta       :ta_alta,
      ta_baja       :ta_baja,
      imc:imc,
      tipo_beneficiario:tipo_beneficiario,
      motivo_consulta:motivo_consulta, 
      user_id:user_id,
      id_consulta:id_consulta,
      }
 
      //      //console.log(data);
      $.ajax
      ({
           url:url,
           method:'POST',
           data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
                
                if(data=='1')
                {
                     alert('SIGNOS VITALES ACTUALIZADOS ');
                
                     window.location = '/Actualizar_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    
                }
                else if(data=='0')
                {
                     alert('Error en la Incorporación del Registro');
                }
 
                else if(data=='2')
                {
                     alert('La cedula posee Historial');
                }
 
 
           },
           error:function(xhr, status, errorThrown)
           {
                alert(xhr.status);
                alert(errorThrown);
           }
      });
 });

 $('#aciones').on('change',function(e)
 { 
     descripcioncompleta=$('#aciones option:selected').val(); 

switch(descripcioncompleta) {
  case '1':
     $(".enfermedadactual").show(); 
     $("#table_enfermedad_actual").dataTable().fnDestroy();
     listar_enfermedad_actual()
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
     break;
  case '2':  
     $(".alergiasmedicamentos").show();
     $("#table_alergias_medicamentos").dataTable().fnDestroy();
     alergias_medicamentos();
     $(".enfermedadactual").hide(); 
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
     break;
   case '3':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").show();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide(); 
     $('.texto').show();
     $("#btnAntecedentes_patologicosf").show();
     $("#antecedentes_pf").dataTable().fnDestroy();
     antecedentespatologicosfamiliares();
     $(".habitos").hide();
   break;
   case '4':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").show();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $("#btnAntecedentes_quirurgicos").show(); 
     $("#antecedentes_qx").dataTable().fnDestroy();
     antecedentesquirurgicos();   
     $(".habitos").hide();
   break;
   case '5':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").show();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $("#btnAntecedentes_patologicosp").show();
     $("#antecedentes_pp").dataTable().fnDestroy();
     antecedentespatologicospersonales()
     $(".habitos").hide();
   break  
   case '6':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").show();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $("#btnexamen_fisico").show();
     $("#examen_fisico").dataTable().fnDestroy();
     listar_examen_fisico()
     $(".habitos").hide();
   break  
   case '7':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").show();
     $("#tableparaclinicos").dataTable().fnDestroy();
     listar_paraclinicos();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
   break  
   case '8':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").show();
     $("#impresion_diag").dataTable().fnDestroy();
     listar_impresion_diag();
     $('.texto').show();
     $(".habitos").hide();
   break  
   case '9':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").show();
     $("#table_plan").dataTable().fnDestroy();
     listar_plan();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
     $(".habitos").hide();
   break
   
   case '10':
     $(".habitos").show();
     $("#habitos_psicosociales").dataTable().fnDestroy();
     listar_habitos_psicosociales();
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $("#table_plan").dataTable().fnDestroy();
     listar_plan();
   break  

   case '12':

   
   e.preventDefault('');
   let nombre =$("#nombrepellidoB").val();
   let fecha_nacimiento =$("#fecha_nacimiento").val();
   let cedulab =$("#cedulab").val();
   let edad =$("#edad").val(); 
   let telefono =$("#telefono").val();
   let numeroHistorial =$("#numeroHistorial").val();
   $("#modal_reposos").modal("show");  
   $('#modal_reposos').find('#nombretitular').val(nombre); 
   $('#modal_reposos').find('#fecha_nacimientotitular').val(fecha_nacimiento); 
   $('#modal_reposos').find('#cedulatitular').val(cedulab);
   $('#modal_reposos').find('#edadtitular').val(edad); 
   $('#modal_reposos').find('#telefonotitular').val(telefono);
   $('#modal_reposos').find('#unidadtitular').val('CORTESIA');
   $('#modal_reposos').find('#n_historialT').val(numeroHistorial);
   
   break 
}


 });
 
 /****************************************MEDICINA GENERAL*******************/





///*****************************************/ APARTADO DE REPOSOS********************************

$(document).on('click','#btnMostrar_reposo', function(e)
{

$("#modal_reposos").modal("show"); 
$("#btnActualizar_reposos").hide();
$("#btnagregar_reposos").show(); 
$(".observacion").val("");  
//let nombre =$("#nombrepellidoB").val();  
let fecha_nacimiento =$("#fecha_nacimiento").val(); 
let edad =$("#edad").val();  
let departamento =$("#departamento").val();  
let telefono =$("#telefono").val();  
let cedulaT =$("#cedulaT").val();   
let numeroHistorial=$("#numeroHistorial").val(); 
$('#nombretitular').val(nombre);
$('#fecha_nacimientotitular').val(fecha_nacimiento);
$('#cedulatitular').val(cedulaT);
$('#edadtitular').val(edad);
$('#unidadtitular').val(departamento);
$('#telefonotitular').val(telefono);  
$('#n_historialT').val(numeroHistorial);  

});

$(document).on('click','#btnMostrar_reposo_gb', function(e)
{
$("#btnActualizar_reposos_gb").hide();
let nombre =$("#nombrepellidoT").val();  
let fecha_nacimiento =$("#fecha_nacimiento").val(); 
let edad =$("#edad").val();  
let departamento =$("#departamento").val();  
let telefono =$("#telefono").val();  
let cedulaT =$("#cedulaT").val();   
let numeroHistorial=$("#numeroHistorial").val();
$("#modal_reposos_gb").modal("show"); 
$("#btnActualizar_reposos").hide();
$("#btnagregar_reposos").show(); 
$(".observacion").val("");  
$('#r_nombretitular').val(nombre);
$('#r_fecha_nacimientotitular').val(fecha_nacimiento);
$('#r_cedulatitular').val(cedulaT);
$('#r_edadtitular').val(edad);
$('#r_unidadtitular').val(departamento);
$('#r_telefonotitular').val(telefono);  
$('#r_n_historialT').val(numeroHistorial);  

});



$("#desde").on('change', function()
{
    document.getElementById('hasta').disabled = false;
    fechaDesde_normal=$("#desde").val();
    fechaHasta_normal=$("#hasta").val();
  
     if (fechaDesde_normal=fechaHasta_normal)
    {
        alert('Error! Debe seleccionar el Tipo de Reposo en Horas ')
    }else
    {
    $("#dia_en_horas").val(0);
    }
});

$("#hasta").on('change', function()
{

fechaDesde_normal=$("#desde").val();
fechaHasta_normal=$("#hasta").val();



   
});

$("#r_desde").on('change', function()
{
    document.getElementById('r_hasta').disabled = false;
    fechaDesde_normal=$("#r_desde").val();
    fechaHasta_normal=$("#r_hasta").val();
  
     if (fechaDesde_normal=fechaHasta_normal)
    {
        alert('Error! Debe seleccionar el Tipo de Reposo en Horas ')
    }else
    {
    $("#r_dia_en_horas").val(0);
    }
});

$("#r_hasta").on('change', function()
{

fechaDesde_normal=$("#desde").val();
fechaHasta_normal=$("#hasta").val();



   
});


$("#tipo_reposo").on('change', function()
{
    let tipo_reposo=$("#tipo_reposo").val();
    if (tipo_reposo==2) {
        $("#label_retorno").hide();
        $("#hasta").hide();


        $("#labe_dias").hide();
        $("#dias").hide();
        
        $("#labe_horas").show();
        $("#horas").show();   
        let horas=$("#horas").val();      
        $("#dia_en_horas").val(horas);
    }
   else if (tipo_reposo==1) {
        $("#label_retorno").show();
        $("#hasta").show();
        
        $("#labe_horas").hide();
        $("#horas").hide();  
        
        $("#labe_dias").show();
        $("#dias").show();
    }

});

$("#horas").blur(function() {
    var valor = $(this).val();
    if(valor>8)
    {
        alert('LAS HORAS DEBEN SER IGUAL O MENOR A 8' )
    }
    else{
        $("#dia_en_horas").val(valor);
    }
    
  });

  $("#r_horas").blur(function() {
    var valor = $(this).val();
    if(valor>8)
    {
        alert('LAS HORAS DEBEN SER IGUAL O MENOR A 8' )
    }
    else{
        $("#r_dia_en_horas").val(valor);
    }
    
  });

  $("#r_tipo_reposo").on('change', function()
{
    let tipo_reposo=$("#r_tipo_reposo").val();
    if (tipo_reposo==2) {
        $("#r_label_retorno").hide();
        $("#r_hasta").hide();


        $("#r_labe_dias").hide();
        $("#r_dias").hide();
        
        $("#r_labe_horas").show();
        $("#r_horas").show();   
        let horas=$("#r_horas").val();      
        $("#r_dia_en_horas").val(horas);
    }
   else if (tipo_reposo==1) {
        $("#r_label_retorno").show();
        $("#hasta").show();
        
        $("#r_labe_horas").hide();
        $("#r_horas").hide();  
        
        $("#r_labe_dias").show();
        $("#r_dias").show();
    }

});


/**********ESTE METODO AGREGA LOS DATOS DEL REPOSOS***** */
$(document).on('click','#btnagregar_reposos', function(e)
{
let nombre =$("#nombrepellidoT").val();  
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"/"+month+"/"+now.getFullYear(); 
let fecha_formateada=now.getFullYear()+"-"+month+"-"+day;
var t_n_historial=$('#n_historialT').val();
var t_id_medico  =$('#medico_id').val();
let fecha_desde=fecha_formateada;
let fecha_hasta=fecha_formateada;

// variables para regresar al la pagina central 
var cedula      =$('#cedulab').val();
var tipobeneficiario  ='C';
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_creacion').val();
var id_historial_medico=$('#id_historial_medico').val();
var id_consulta=$('#id_consulta').val(); 
let motivo=$('#motivo_reposo').val();
let accion='GENERO UN REPOSO PARA '+' '+nombre;
let tipo_reposo=$("#tipo_reposo").val();
if(tipo_reposo==2)// VALIDACION PARA HORAS
{


    let horas=$('#horas').val();
   
    if (horas==' '||horas==0) 
    {
        alert('DEBE INTRODUCIR LAS HORAS DEL REPOSO')
    }
    else
    {
        let horas_d=$('#dia_en_horas').val();
        if (motivo.trim()==='')
        {
           alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
        }
        else
        {
           var data=
           {
           t_n_historial:t_n_historial,
           // t_id_consulta:t_id_consulta,
           t_id_medico:t_id_medico,
           fecha_desde:fecha_desde,
           fecha_hasta:fecha_hasta,
           motivo:motivo,
           accion:accion,
           horas_d:horas_d,
           }
           var url='/Agregar_Reposo';
              $.ajax
              ({
              url:url,
              method:'POST',
              data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
              dataType:'text',
              beforeSend:function(data)
              {
              //alert('Procesando Información ...');
              },
              success:function(data)
              {
             
              if(data=='1')
              {
              alert('Registro Exitoso');
              }
              else
              {
              alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
              
              },
              error:function(xhr, status, errorThrown)
              {
              alert(xhr.status);
              alert(errorThrown);
              }
              });
    }
 }
}
else if (tipo_reposo==1)// VALIDACION PARA DIAS 
{
    let horas=$('#horas').val();
    fecha_desde_normal  =$("#desde").val();
    fecha_hasta_normal  =$("#hasta").val();
    if (fecha_desde_normal==''&&fecha_hasta_normal!='') 
    {
        alert('POR FAVOR INDICAR LA FECHA DE INICIO'); 
    }
    else if (fecha_hasta_normal==''&&fecha_desde_normal=='') 
    {
        alert('POR FAVOR INGRESAR LA FECHA DE INICIO Y RETORNO');
    } 
    else if (fecha_hasta_normal==''&&fecha_desde_normal!='') 
    {
        alert('POR FAVOR INDICAR LA FECHA DE RETORNO');
    } 
    else if (fecha_hasta_normal<fecha_desde_normal) {
        alert('ERRO! LA FECHA DE INICIO ES MAYOR A LA FECHA DE RETORNO');
    }   
    else if (fecha_desde_normal==fecha_hasta_normal)
    {
        alert('Error! Debe seleccionar el Tipo de Reposo en Horas ')
    }   
    else   if (motivo.trim()==='')
    {
        alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
    }
  else
    {
        //CALCULO PARA OBTENER LA CANTIDAD DE HORAS ENTRE LOS CALENDARIOS
        fecha_desde = new Date($("#desde").val());
        fecha_hasta = new Date($("#hasta").val());
        var diff = fecha_hasta.valueOf() - fecha_desde.valueOf();
        var diffInHours = diff/1000/60/60; // Convert milliseconds to hours
        $("#dia_en_horas").val(diffInHours);
        let horas_d=$("#dia_en_horas").val();
        //CALCULO PARA OBTENER LA CANTIDAD DE DIAS EN FUNCION DE LAS HORAS
        var horast = horas_d;
        var dias = horast / 24;
        $("#dias").val(dias);      
        var data=
        {
            t_n_historial:t_n_historial,
            // t_id_consulta:t_id_consulta,
            t_id_medico:t_id_medico,
            fecha_desde:fecha_desde_normal,
            fecha_hasta:fecha_hasta_normal,
            motivo:motivo,
            accion:accion,
            horas_d:horas_d,
        }  
        // variables para regresar al la pagina central 
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val(); 
        var url='/Agregar_Reposo';
        $.ajax
        ({
        url:url,
        method:'POST',
        data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType:'json',
        beforeSend:function(data)
        {
        //alert('Procesando Información ...');
        },
        success:function(data)
        {
        if(data=='1')
        {
        alert('Registro Exitoso');
        }
        else
        {
          alert('Error en la Incorporación del registro');
        }
        window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        },
        error:function(xhr, status, errorThrown)
        {
        alert(xhr.status);
        alert(errorThrown);
        }
        });
    } 
}

});

$('#lista_reposos').on('click','.Editar', function(e)
{ 
e.preventDefault(); 


$("#modal_reposos").modal("show"); 
$("#btnagregar_reposos").hide();
$("#btnActualizar_reposos").show();
$(".observacion").val("");  
let nombre =$("#nombrepellidoT").val();  
let fecha_nacimiento =$("#fecha_nacimiento").val(); 
let edad =$("#edad").val();  
let departamento =$("#departamento").val();  
let telefono =$("#telefono").val();  
let cedulaT =$("#cedulaT").val();   
let numeroHistorial=$("#numeroHistorial").val(); 
let motivo=$(this).attr('motivo'); 
$('#nombretitular').val(nombre);
$('#fecha_nacimientotitular').val(fecha_nacimiento);
$('#cedulatitular').val(cedulaT);
$('#edadtitular').val(edad);
$('#unidadtitular').val(departamento);
$('#telefonotitular').val(telefono);  
$('#n_historialT').val(numeroHistorial);    
$('#motivo_reposo').val(motivo);
let fecha_hasta=$(this).attr('fecha_hasta');
let fechaconvertida_hasta=moment(fecha_hasta,'DD-MM-YYYY'); 
fechaconvertida_hasta=fechaconvertida_hasta.format('YYYY-MM-DD');
$('#hasta').val(fechaconvertida_hasta);
let fecha_desde=$(this).attr('fecha_desde');
let fechaconvertida_desde=moment(fecha_desde,'DD-MM-YYYY'); 
fechaconvertida_desde=fechaconvertida_desde.format('YYYY-MM-DD');
$('#desde').val(fechaconvertida_desde);

let id_reposo=$(this).attr('id_reposo');
$('#id_reposo').val(id_reposo);

});




$(document).on('click','#btnActualizar_reposos', function(e)
{
e.preventDefault();
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"/"+month+"/"+now.getFullYear(); 
let nombre =$("#nombrepellidoT").val();  
var t_n_historial=$('#n_historialT').val();
var t_id_medico  =$('#medico_id').val();
let fecha_desde=$('#desde').val();
let fecha_hasta=$('#hasta').val();
let motivo=$('#motivo_reposo').val();
let id_reposo=$('#id_reposo').val();

let accion='EDITO UN REPOSO PARA '+' '+nombre;
if (fecha_desde==''&&fecha_hasta!='') 
{
alert('POR FAVOR INDICAR LA FECHA DE INICIO');

}
else if (fecha_hasta==''&&fecha_desde=='') 
{
alert('POR FAVOR INGRESAR LA FECHA DE INICIO Y RETORNO');
} 
else if (fecha_hasta==''&&fecha_desde!='') 
{
alert('POR FAVOR INDICAR LA FECHA DE RETORNO');
} 
else if (fecha_hasta<fecha_desde) {
alert('ERRO! LA FECHA DE INICIO ES MAYOR A LA FECHA DE RETORNO');
}

else   if (motivo.trim()==='')
{
alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
}
else
{ 
var data=
{
t_n_historial:t_n_historial,
t_id_medico:t_id_medico,
fecha_desde:fecha_desde,
fecha_hasta:fecha_hasta,
motivo:motivo,
accion:accion,
id_reposo:id_reposo,
}

// variables para regresar al la pagina central 
var cedula      =$('#cedulab').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='C';
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_creacion').val();
var id_historial_medico=$('#id_historial_medico').val();


var url='/Actualizar_Reposo';
$.ajax
({
url:url,
method:'POST',
data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
dataType:'JSON',
beforeSend:function(data)
{
//alert('Procesando Información ...');
},
success:function(data)
{


if(data=='1')
{
alert('Registro Actualizado');
}
else
{
alert('Error en la Incorporación del registro');
}
window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  

},
error:function(xhr, status, errorThrown)
{
alert(xhr.status);
alert(errorThrown);
}
});
}

});


$('#lista_reposos').on('click','.Imprimir', function(e)
{ 
e.preventDefault(); 
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"-"+month+"-"+now.getFullYear(); 
let id_reposo=$(this).attr('id_reposo');
let n_historial=$(this).attr('n_historial');
var cedula      =$('#cedulaT').val();
var nombre_medico      =$('#nombre_medico').val();

window.open('../../../../../../../../PDF_Reposos/'+cedula+'/'+n_historial+'/'+id_reposo+'/'+nombre_medico+'/'+today,'_blank');


});

$('#lista_reposos').on('click','.Bloquear', function(e)    

{
var cedula      =$('#cedulaT').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='T';
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_creacion').val();
var id_historial_medico=$('#id_historial_medico').val();
Swal.fire({
title: 'Reversar el Registro?',
icon:'warning',
showCancelButton: true,
cancelButtonColor:'#d33',
confirmButtonText:'Confirmar',

}).then((result)=> {
if(result.isConfirmed){ 

Swal.fire(  

'REVERSO!!',
'Registro Actualizado.',
'success',
{

}
)

let id_reposo=$(this).attr('id_reposo');
let nombret=$(this).attr('nombre');

id_consulta = (isNaN(parseInt(id_consulta)))? 0 : parseInt(id_consulta);
let n_historial=$(this).attr('n_historial');
let borrado    =true;
let accion='REVERSO EL REPOSO DE '+' '+nombret;




var data=
{
id_reposo:id_reposo,
n_historial:n_historial,
borrado:borrado,
accion:accion

}     

var url='/Reversar_reposo';
$.ajax
({
url:url,
method:'POST',
data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
dataType:'JSON',
beforeSend:function(data)
{
//alert('Procesando Información ...');
},
success:function(data)
{



if(data==0)
{
alert('Error al Reversar el Registro');  

}

else  if(data==2)
{
alert('Hubo un error durante el reverso del la nota de entrega');  
}
setTimeout(function () {
window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  

}, 1500);

},
error:function(xhr, status, errorThrown)
{
alert(xhr.status);
alert(errorThrown);
}
});  

}

})  


});


///////////////REOSOS PSICOLOGIA//////////////////


$('#lista_reposos_psico').on('click','.Editar', function(e)
{ 
e.preventDefault(); 


$("#modal_reposos").modal("show"); 
$("#btnagregar_reposos").hide();
$("#btnActualizar_reposos").show();
$(".observacion").val("");  
let nombre =$("#nombrepellidoT").val();  
let fecha_nacimiento =$("#fecha_nacimiento").val(); 
let edad =$("#edad").val();  
let departamento =$("#departamento").val();  
let telefono =$("#telefono").val();  
let cedulaT =$("#cedulaT").val();   
let numeroHistorial=$("#numeroHistorial").val(); 
let motivo=$(this).attr('motivo'); 
$('#nombretitular').val(nombre);
$('#fecha_nacimientotitular').val(fecha_nacimiento);
$('#cedulatitular').val(cedulaT);
$('#edadtitular').val(edad);
$('#unidadtitular').val(departamento);
$('#telefonotitular').val(telefono);  
$('#n_historialT').val(numeroHistorial);    
$('#motivo_reposo').val(motivo);
let fecha_hasta=$(this).attr('fecha_hasta');
let fechaconvertida_hasta=moment(fecha_hasta,'DD-MM-YYYY'); 
fechaconvertida_hasta=fechaconvertida_hasta.format('YYYY-MM-DD');
$('#hasta').val(fechaconvertida_hasta);
let fecha_desde=$(this).attr('fecha_desde');
let fechaconvertida_desde=moment(fecha_desde,'DD-MM-YYYY'); 
fechaconvertida_desde=fechaconvertida_desde.format('YYYY-MM-DD');
$('#desde').val(fechaconvertida_desde);

let id_reposo=$(this).attr('id_reposo');
$('#id_reposo').val(id_reposo);

});

$('#lista_reposos_psico').on('click','.Imprimir', function(e)
{ 
e.preventDefault(); 
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"-"+month+"-"+now.getFullYear(); 
let id_reposo=$(this).attr('id_reposo');
let n_historial=$(this).attr('n_historial');
var cedula      =$('#cedulaT').val();
var nombre_medico      =$('#nombre_medico').val();

window.open('../../../../../../../../PDF_Reposos/'+cedula+'/'+n_historial+'/'+id_reposo+'/'+nombre_medico+'/'+today,'_blank');


});

$('#lista_reposos_psico').on('click','.Bloquear', function(e)    

{
var cedula      =$('#cedulaT').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='T';
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_creacion').val();
var id_historial_medico=$('#id_historial_medico').val();
Swal.fire({
title: 'Reversar el Registro?',
icon:'warning',
showCancelButton: true,
cancelButtonColor:'#d33',
confirmButtonText:'Confirmar',

}).then((result)=> {
if(result.isConfirmed){ 

Swal.fire(  

'REVERSO!!',
'Registro Actualizado.',
'success',
{

}
)

let id_reposo=$(this).attr('id_reposo');
let nombret=$(this).attr('nombre');

id_consulta = (isNaN(parseInt(id_consulta)))? 0 : parseInt(id_consulta);
let n_historial=$(this).attr('n_historial');
let borrado    =true;
let accion='REVERSO EL REPOSO DE '+' '+nombret;




var data=
{
id_reposo:id_reposo,
n_historial:n_historial,
borrado:borrado,
accion:accion

}     

var url='/Reversar_reposo';
$.ajax
({
url:url,
method:'POST',
data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
dataType:'JSON',
beforeSend:function(data)
{
//alert('Procesando Información ...');
},
success:function(data)
{



if(data==0)
{
alert('Error al Reversar el Registro');  

}

else  if(data==2)
{
alert('Hubo un error durante el reverso del la nota de entrega');  
}
setTimeout(function () {
window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  

}, 1500);

},
error:function(xhr, status, errorThrown)
{
alert(xhr.status);
alert(errorThrown);
}
});  

}

})  


});


///////////////REPOSOS MEDICINA INTERNA//////////////////


$('#lista_reposos_mi').on('click','.Editar', function(e)
{ 
e.preventDefault(); 


$("#modal_reposos").modal("show"); 
$("#btnagregar_reposos").hide();
$("#btnActualizar_reposos").show();
$(".observacion").val("");  
let nombre =$("#nombrepellidoT").val();  
let fecha_nacimiento =$("#fecha_nacimiento").val(); 
let edad =$("#edad").val();  
let departamento =$("#departamento").val();  
let telefono =$("#telefono").val();  
let cedulaT =$("#cedulaT").val();   
let numeroHistorial=$("#numeroHistorial").val(); 
let motivo=$(this).attr('motivo'); 
$('#nombretitular').val(nombre);
$('#fecha_nacimientotitular').val(fecha_nacimiento);
$('#cedulatitular').val(cedulaT);
$('#edadtitular').val(edad);
$('#unidadtitular').val(departamento);
$('#telefonotitular').val(telefono);  
$('#n_historialT').val(numeroHistorial);    
$('#motivo_reposo').val(motivo);
let fecha_hasta=$(this).attr('fecha_hasta');
let fechaconvertida_hasta=moment(fecha_hasta,'DD-MM-YYYY'); 
fechaconvertida_hasta=fechaconvertida_hasta.format('YYYY-MM-DD');
$('#hasta').val(fechaconvertida_hasta);
let fecha_desde=$(this).attr('fecha_desde');
let fechaconvertida_desde=moment(fecha_desde,'DD-MM-YYYY'); 
fechaconvertida_desde=fechaconvertida_desde.format('YYYY-MM-DD');
$('#desde').val(fechaconvertida_desde);

let id_reposo=$(this).attr('id_reposo');
$('#id_reposo').val(id_reposo);

});

$('#lista_reposos_mi').on('click','.Imprimir', function(e)
{ 
e.preventDefault(); 
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"-"+month+"-"+now.getFullYear(); 
let id_reposo=$(this).attr('id_reposo');
let n_historial=$(this).attr('n_historial');
var cedula      =$('#cedulaT').val();
var nombre_medico      =$('#nombre_medico').val();

window.open('../../../../../../../../PDF_Reposos/'+cedula+'/'+n_historial+'/'+id_reposo+'/'+nombre_medico+'/'+today,'_blank');


});

$('#lista_reposos_mi').on('click','.Bloquear', function(e)    

{
var cedula      =$('#cedulaT').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='T';
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_creacion').val();
var id_historial_medico=$('#id_historial_medico').val();
Swal.fire({
title: 'Reversar el Registro?',
icon:'warning',
showCancelButton: true,
cancelButtonColor:'#d33',
confirmButtonText:'Confirmar',

}).then((result)=> {
if(result.isConfirmed){ 

Swal.fire(  

'REVERSO!!',
'Registro Actualizado.',
'success',
{

}
)

let id_reposo=$(this).attr('id_reposo');
let nombret=$(this).attr('nombre');

id_consulta = (isNaN(parseInt(id_consulta)))? 0 : parseInt(id_consulta);
let n_historial=$(this).attr('n_historial');
let borrado    =true;
let accion='REVERSO EL REPOSO DE '+' '+nombret;




var data=
{
id_reposo:id_reposo,
n_historial:n_historial,
borrado:borrado,
accion:accion

}     

var url='/Reversar_reposo';
$.ajax
({
url:url,
method:'POST',
data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
dataType:'JSON',
beforeSend:function(data)
{
//alert('Procesando Información ...');
},
success:function(data)
{



if(data==0)
{
alert('Error al Reversar el Registro');  

}

else  if(data==2)
{
alert('Hubo un error durante el reverso del la nota de entrega');  
}
setTimeout(function () {
window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  

}, 1500);

},
error:function(xhr, status, errorThrown)
{
alert(xhr.status);
alert(errorThrown);
}
});  

}

})  


});



///////////////REPOSOS PASIENTE PEDIATRICO//////////////////


$('#lista_reposos_pp').on('click','.Editar', function(e)
{ 
e.preventDefault(); 


$("#modal_reposos").modal("show"); 
$("#btnagregar_reposos").hide();
$("#btnActualizar_reposos").show();
$(".observacion").val("");  
let nombre =$("#nombrepellidoT").val();  
let fecha_nacimiento =$("#fecha_nacimiento").val(); 
let edad =$("#edad").val();  
let departamento =$("#departamento").val();  
let telefono =$("#telefono").val();  
let cedulaT =$("#cedulaT").val();   
let numeroHistorial=$("#numeroHistorial").val(); 
let motivo=$(this).attr('motivo'); 
$('#nombretitular').val(nombre);
$('#fecha_nacimientotitular').val(fecha_nacimiento);
$('#cedulatitular').val(cedulaT);
$('#edadtitular').val(edad);
$('#unidadtitular').val(departamento);
$('#telefonotitular').val(telefono);  
$('#n_historialT').val(numeroHistorial);    
$('#motivo_reposo').val(motivo);
let fecha_hasta=$(this).attr('fecha_hasta');
let fechaconvertida_hasta=moment(fecha_hasta,'DD-MM-YYYY'); 
fechaconvertida_hasta=fechaconvertida_hasta.format('YYYY-MM-DD');
$('#hasta').val(fechaconvertida_hasta);
let fecha_desde=$(this).attr('fecha_desde');
let fechaconvertida_desde=moment(fecha_desde,'DD-MM-YYYY'); 
fechaconvertida_desde=fechaconvertida_desde.format('YYYY-MM-DD');
$('#desde').val(fechaconvertida_desde);

let id_reposo=$(this).attr('id_reposo');
$('#id_reposo').val(id_reposo);

});

$('#lista_reposos_pp').on('click','.Imprimir', function(e)
{ 
e.preventDefault(); 
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"-"+month+"-"+now.getFullYear(); 
let id_reposo=$(this).attr('id_reposo');
let n_historial=$(this).attr('n_historial');
var cedula      =$('#cedulaT').val();
var nombre_medico      =$('#nombre_medico').val();

window.open('../../../../../../../../PDF_Reposos/'+cedula+'/'+n_historial+'/'+id_reposo+'/'+nombre_medico+'/'+today,'_blank');


});

$('#lista_reposos_pp').on('click','.Bloquear', function(e)    

{
var cedula      =$('#cedulaT').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='T';
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_creacion').val();
var id_historial_medico=$('#id_historial_medico').val();
Swal.fire({
title: 'Reversar el Registro?',
icon:'warning',
showCancelButton: true,
cancelButtonColor:'#d33',
confirmButtonText:'Confirmar',

}).then((result)=> {
if(result.isConfirmed){ 

Swal.fire(  

'REVERSO!!',
'Registro Actualizado.',
'success',
{

}
)

let id_reposo=$(this).attr('id_reposo');
let nombret=$(this).attr('nombre');

id_consulta = (isNaN(parseInt(id_consulta)))? 0 : parseInt(id_consulta);
let n_historial=$(this).attr('n_historial');
let borrado    =true;
let accion='REVERSO EL REPOSO DE '+' '+nombret;




var data=
{
id_reposo:id_reposo,
n_historial:n_historial,
borrado:borrado,
accion:accion

}     

var url='/Reversar_reposo';
$.ajax
({
url:url,
method:'POST',
data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
dataType:'JSON',
beforeSend:function(data)
{
//alert('Procesando Información ...');
},
success:function(data)
{



if(data==0)
{
alert('Error al Reversar el Registro');  

}

else  if(data==2)
{
alert('Hubo un error durante el reverso del la nota de entrega');  
}
setTimeout(function () {
window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  

}, 1500);

},
error:function(xhr, status, errorThrown)
{
alert(xhr.status);
alert(errorThrown);
}
});  

}

})  


});



///////////////REPOSOS GINECOLOGIA Y OBSTERTRICA//////////////////


/**********ESTE METODO AGREGA LOS DATOS DEL REPOSOS***** */
$(document).on('click','#btnagregar_reposos_gb', function(e)
{

    var t_n_historial=$('#r_n_historialT').val();
    var t_id_medico  =$('#medico_id').val();
    let nombre =$("#r_nombretitular").val();   
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today= day+"/"+month+"/"+now.getFullYear(); 
    let fecha_formateada=now.getFullYear()+"-"+month+"-"+day;
    var r_t_n_historial=$('#n_historialT').val();
    var t_id_medico  =$('#medico_id').val();
    let fecha_desde=fecha_formateada;
    let fecha_hasta=fecha_formateada;
    // variables para regresar al la pagina central 
    var cedula      =$('#cedulab').val();
    var tipobeneficiario  ='F';
    var n_historial=$('#numeroHistorial').val();
    var id_especialidad=$('#id_especialidad').val();
    var fecha_asistencia=$('#fecha_creacion').val();
    var id_historial_medico=$('#id_historial_medico').val();
    var id_consulta=$('#id_consulta').val(); 
    let motivo=$('#r_motivo_reposo').val();
    let accion='GENERO UN REPOSO PARA '+' '+nombre;
    let tipo_reposo=$("#r_tipo_reposo").val();
    if(tipo_reposo==2)// VALIDACION PARA HORAS
    {
    
    
        let horas=$('#r_horas').val();
       
        if (horas==' '||horas==0) 
        {
            alert('DEBE INTRODUCIR LAS HORAS DEL REPOSO')
        }
        else
        {
            let horas_d=$('#r_dia_en_horas').val();
            if (motivo.trim()==='')
            {
               alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
            }
            else
            {
               var data=
               {
               t_n_historial:t_n_historial,
               // t_id_consulta:t_id_consulta,
               t_id_medico:t_id_medico,
               fecha_desde:fecha_desde,
               fecha_hasta:fecha_hasta,
               motivo:motivo,
               accion:accion,
               horas_d:horas_d,
               }
               var url='/Agregar_Reposo';
                  $.ajax
                  ({
                  url:url,
                  method:'POST',
                  data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                  dataType:'text',
                  beforeSend:function(data)
                  {
                  //alert('Procesando Información ...');
                  },
                  success:function(data)
                  {
                 
                  if(data=='1')
                  {
                  alert('Registro Exitoso');
                  }
                  else
                  {
                  alert('Error en la Incorporación del registro');
                  }
                  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  
                  },
                  error:function(xhr, status, errorThrown)
                  {
                  alert(xhr.status);
                  alert(errorThrown);
                  }
                  });
        }
     }
    }
    else if (tipo_reposo==1)// VALIDACION PARA DIAS 
    {
        let horas=$('#r_horas').val();
        fecha_desde_normal  =$("#r_desde").val();
        fecha_hasta_normal  =$("#r_hasta").val();
        if (fecha_desde_normal==''&&fecha_hasta_normal!='') 
        {
            alert('POR FAVOR INDICAR LA FECHA DE INICIO'); 
        }
        else if (fecha_hasta_normal==''&&fecha_desde_normal=='') 
        {
            alert('POR FAVOR INGRESAR LA FECHA DE INICIO Y RETORNO');
        } 
        else if (fecha_hasta_normal==''&&fecha_desde_normal!='') 
        {
            alert('POR FAVOR INDICAR LA FECHA DE RETORNO');
        } 
        else if (fecha_hasta_normal<fecha_desde_normal) {
            alert('ERRO! LA FECHA DE INICIO ES MAYOR A LA FECHA DE RETORNO');
        }   
        else if (fecha_desde_normal==fecha_hasta_normal)
        {
            alert('Error! Debe seleccionar el Tipo de Reposo en Horas ')
        }   
        else   if (motivo.trim()==='')
        {
            alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
        }
      else
        {
            //CALCULO PARA OBTENER LA CANTIDAD DE HORAS ENTRE LOS CALENDARIOS
            fecha_desde = new Date($("#r_desde").val());
            fecha_hasta = new Date($("#r_hasta").val());
            var diff = fecha_hasta.valueOf() - fecha_desde.valueOf();
            var diffInHours = diff/1000/60/60; // Convert milliseconds to hours
            $("#r_dia_en_horas").val(diffInHours);
            let horas_d=$("#r_dia_en_horas").val();
            //CALCULO PARA OBTENER LA CANTIDAD DE DIAS EN FUNCION DE LAS HORAS
            var horast = horas_d;
            var dias = horast / 24;
            $("#r_dias").val(dias);      
            var data=
            {
                t_n_historial:t_n_historial,
                // t_id_consulta:t_id_consulta,
                t_id_medico:t_id_medico,
                fecha_desde:fecha_desde_normal,
                fecha_hasta:fecha_hasta_normal,
                motivo:motivo,
                accion:accion,
                horas_d:horas_d,
            }  
            // variables para regresar al la pagina central 
            var cedula      =$('#cedulab').val();
            var tipobeneficiario  ='F';
            var n_historial=$('#numeroHistorial').val();
            var id_especialidad=$('#id_especialidad').val();
            var fecha_asistencia=$('#fecha_creacion').val();
            var id_historial_medico=$('#id_historial_medico').val();
            var id_consulta=$('#id_consulta').val(); 
            var url='/Agregar_Reposo';
            $.ajax
            ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'json',
            beforeSend:function(data)
            {
            //alert('Procesando Información ...');
            },
            success:function(data)
            {
            if(data=='1')
            {
            alert('Registro Exitoso');
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
            },
            error:function(xhr, status, errorThrown)
            {
            alert(xhr.status);
            alert(errorThrown);
            }
            });
        } 
    }      
});

$('#lista_reposos_gb').on('click','.Editar', function(e)
{ 
e.preventDefault(); 

$("#modal_reposos_gb").modal("show"); 
$("#btnagregar_reposos_gb").hide();
$("#btnActualizar_reposos_gb").show();
$(".observacion").val("");  
$('#cedulatitular').val('');
$('#motivo_reposo').val('');
$('#r_medico').val('0');
$('#r_especialidad').val('0');
$("#modal_reposos").modal("show"); 
$("#btnActualizar_reposos").hide();
$("#btnagregar_reposos").show(); 
$(".observacion").val("");  
let nombre =$("#nombrepellidoT").val();  
let fecha_nacimiento =$("#fecha_nacimiento").val(); 
let edad =$("#edad").val();  
let departamento =$("#departamento").val();  
let telefono =$("#telefono").val();  
let cedulaT =$("#cedulaT").val();   
let numeroHistorial=$("#numeroHistorial").val(); 
$('#r_nombretitular').val(nombre);
$('#r_fecha_nacimientotitular').val(fecha_nacimiento);
$('#r_cedulatitular').val(cedulaT);
$('#r_edadtitular').val(edad);
$('#r_unidadtitular').val(departamento);
$('#r_telefonotitular').val(telefono);  
$('#r_n_historialT').val(numeroHistorial);  
let fecha_hasta=$(this).attr('fecha_hasta');
let fechaconvertida_hasta=moment(fecha_hasta,'DD-MM-YYYY'); 
fechaconvertida_hasta=fechaconvertida_hasta.format('YYYY-MM-DD');
$('#r_hasta').val(fechaconvertida_hasta);
let fecha_desde=$(this).attr('fecha_desde');
let fechaconvertida_desde=moment(fecha_desde,'DD-MM-YYYY'); 
fechaconvertida_desde=fechaconvertida_desde.format('YYYY-MM-DD');
$('#r_desde').val(fechaconvertida_desde);
let motivo=$(this).attr('motivo');
$('#r_motivo_reposo').val(motivo);

let id_reposo=$(this).attr('id_reposo');
$('#id_reposo').val(id_reposo);  


});



$(document).on('click','#btnActualizar_reposos_gb', function(e)
{
e.preventDefault();
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"/"+month+"/"+now.getFullYear(); 

let nombre =$("#nombrepellidoT").val();  
var t_n_historial=$("#numeroHistorial").val(); 
let fecha_desde=$('#r_desde').val();
let fecha_hasta=$('#r_hasta').val();
let motivo=$('#r_motivo_reposo').val();
let id_reposo=$('#id_reposo').val();
let t_id_medico  =$('#medico_id').val();

var cedulab      =$('#cedulab').val();
var tipobeneficiariob  ='F';
let accion='EDITO UN REPOSO PARA '+' '+nombre;
if (fecha_desde==''&&fecha_hasta!='') 
{
alert('POR FAVOR INDICAR LA FECHA DE INICIO');

}
else if (fecha_hasta==''&&fecha_desde=='') 
{
alert('POR FAVOR INGRESAR LA FECHA DE INICIO Y RETORNO');
} 
else if (fecha_hasta==''&&fecha_desde!='') 
{
alert('POR FAVOR INDICAR LA FECHA DE RETORNO');
} 
else if (fecha_hasta<fecha_desde) {
alert('ERRO! LA FECHA DE INICIO ES MAYOR A LA FECHA DE RETORNO');
}

else   if (motivo.trim()==='')
{
alert('NO DEBE DEJAR EL MOTIVO DEL REPOSO  VACIO');
}
else
{ 
var data=
{
t_n_historial:t_n_historial,
t_id_medico:t_id_medico,
fecha_desde:fecha_desde,
fecha_hasta:fecha_hasta,
motivo:motivo,
accion:accion,
id_reposo:id_reposo,
}

// variables para regresar al la pagina central 
var cedula      =$('#cedulaT').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='T';
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_creacion').val();
var id_historial_medico=$('#id_historial_medico').val();


var url='/Actualizar_Reposo';
$.ajax
({
url:url,
method:'POST',
data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
dataType:'JSON',
beforeSend:function(data)
{
//alert('Procesando Información ...');
},
success:function(data)
{


if(data=='1')
{
alert('Registro Actualizado');
}
else
{
alert('Error en la Incorporación del registro');
}
window.location = '/Actualizar_Citas/'+cedulab+'/'+tipobeneficiariob+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  

},
error:function(xhr, status, errorThrown)
{
alert(xhr.status);
alert(errorThrown);
}
});
}

});



$('#lista_reposos_gb').on('click','.Imprimir', function(e)
{ 
e.preventDefault(); 
var now = new Date();
var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"-"+month+"-"+now.getFullYear(); 
let id_reposo=$(this).attr('id_reposo');
let n_historial=$(this).attr('n_historial');
var cedula      =$('#cedulaT').val();
var nombre_medico      =$('#nombre_medico').val();

window.open('../../../../../../../../PDF_Reposos/'+cedula+'/'+n_historial+'/'+id_reposo+'/'+nombre_medico+'/'+today,'_blank');


});

$('#lista_reposos_gb').on('click','.Bloquear', function(e)    

{
var cedula      =$('#cedulaT').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='T';
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_creacion').val();
var id_historial_medico=$('#id_historial_medico').val();
Swal.fire({
title: 'Reversar el Registro?',
icon:'warning',
showCancelButton: true,
cancelButtonColor:'#d33',
confirmButtonText:'Confirmar',

}).then((result)=> {
if(result.isConfirmed){ 

Swal.fire(  

'REVERSO!!',
'Registro Actualizado.',
'success',
{

}
)

let id_reposo=$(this).attr('id_reposo');
let nombret=$(this).attr('nombre');

id_consulta = (isNaN(parseInt(id_consulta)))? 0 : parseInt(id_consulta);
let n_historial=$(this).attr('n_historial');
let borrado    =true;
let accion='REVERSO EL REPOSO DE '+' '+nombret;




var data=
{
id_reposo:id_reposo,
n_historial:n_historial,
borrado:borrado,
accion:accion

}     

var url='/Reversar_reposo';
$.ajax
({
url:url,
method:'POST',
data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
dataType:'JSON',
beforeSend:function(data)
{
//alert('Procesando Información ...');
},
success:function(data)
{



if(data==0)
{
alert('Error al Reversar el Registro');  

}

else  if(data==2)
{
alert('Hubo un error durante el reverso del la nota de entrega');  
}
setTimeout(function () {
window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  

}, 1500);

},
error:function(xhr, status, errorThrown)
{
alert(xhr.status);
alert(errorThrown);
}
});  

}

})  


});


$(document).on('click','#btnCerrar', function(e)
{
var cedulab      =$('#cedulab').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='C'
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_asist').val();
var id_historial_medico=$('#id_historial_medico').val();
window.location = '/Actualizar_Citas/'+cedulab+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
})
$(document).on('click','#btnCerrar_gb', function(e)
{
var cedulab      =$('#cedulab').val();
var id_consulta=$('#id_consulta').val();
var tipobeneficiario  ='C'
var n_historial=$('#numeroHistorial').val();
var id_especialidad=$('#id_especialidad').val();
var fecha_asistencia=$('#fecha_asist').val();
var id_historial_medico=$('#id_historial_medico').val();
window.location = '/Actualizar_Citas/'+cedulab+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
})






$(document).on('click','#btnMostrar_enfermedad_actual', function(e)
{
   $("#modal_enfermedad").modal("show"); 
   $("#btnActualizar_enfermedadactual").hide();
   $("#btnagregar_enfermedadactual").show(); 
   $(".observacion").val("");
});



 $(document).on('click','#btnMostrar_enfermedad_actual', function(e)
 {
    $("#modal_enfermedad").modal("show"); 
    $("#btnActualizar_enfermedadactual").hide();
    $("#btnagregar_enfermedadactual").show(); 
    $(".observacion").val("");
 });


/**********ESTE METODO AGREGA LOS DATOS ENFERMEDAD ACTUAL***** */
$(document).on('click','#btnagregar_enfermedadactual', function(e)
{ 
   var enfermedad_actual=$('#enfermedadactual').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (enfermedad_actual.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {

     var data=
     {
        n_historial:n_historial,
        enfermedad_actual:enfermedad_actual,
        id_consulta:id_consulta,
     }

   
 
     var url='/agregar_enfermedad_actual';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
                alert('Registro Exitoso');
                window.location = '/medico_Consultas/'
             }
             else if(data===2)
             {
                alert('YA EXISTE UN REGISTRO PARA ESTA CONSULTA');
               
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
       }
 });

 $('#table_enfermedad_actual').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_enfermedad").modal("show"); 
      $("#btnagregar_enfermedadactual").hide()
      $("#btnActualizar_enfermedadactual").show() 
      $('#enfermedadactual').val(observacion);
      $('#id').val(id);

 });


 $(document).on('click','#btnActualizar_enfermedadactual', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var observacion  =$('#enfermedadactual').val();
      var enfermedad_actual=$('#enfermedadactual').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;

      var url='/actualizar_enfermedad_actual';
   if (observacion.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
      var data=
      {
        id :id ,
        observacion  :observacion,  
        today:today,      
      }
     
      
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Actualizado');
                     window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });
      }
});





$(document).on('click','#btnMostrar_alergias_medicamentos', function(e)
{

   $("#alergias_medica").modal("show"); 
   $("#btnActualizar_alergias_medi").hide();+
   $("#btnagregar_alergias_medi").show();
   $(".observacion").val("");
});


/**********ESTE METODO AGREGA LOS DATOS DE ALERGIAS A MEDICAMENTOS***** */
$(document).on('click','#btnagregar_alergias_medi', function(e)
{
  var alergias=$('#alergias').val();
  var cedula      =$('#cedulab').val();
  var tipobeneficiario  ='C';
  var n_historial=$('#numeroHistorial').val();
  var id_especialidad=$('#id_especialidad').val();
  var fecha_asistencia=$('#fecha_creacion').val();
  var id_historial_medico=$('#id_historial_medico').val();
  var id_consulta=$('#id_consulta').val();
  if (alergias.trim()==='')
  {
  alert('NO DEBE DEJAR EL CAMPO VACIO');
 }
 else
 {
      var data=
      {
         n_historial:n_historial,
         alergias:alergias,
         id_consulta,id_consulta
         
      }

      var url='/agregar_alergias_medicamentos';
      $.ajax
      ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
               //alert('Procesando Información ...');
            },
            success:function(data)
            {
               if(data===1)
               {
                  alert('Registro Exitoso');
                  window.location = '/medico_Consultas/'
               }
               else if(data===2)
               {
                  alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
               
               }
               else
               {
               alert('Error en la Incorporación del registro');
               }
               window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
         
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
      });
   } 
});



$('#lista_alergias_medicamentos').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')

        
        let id=$(this).attr('id');
        $("#alergias_medica").modal("show"); 
        $("#btnagregar_alergias_medi").hide()
        $("#btnActualizar_alergias_medi").show() 
        $('#alergias').val(observacion);
        $('#id').val(id);

   });

$(document).on('click','#btnActualizar_alergias_medi', function(e)
{
     e.preventDefault();
     var id          =$('#id').val();
     var alergias=$('#alergias').val();
     var cedula      =$('#cedulab').val();
     var tipobeneficiario  ='C';
     var n_historial=$('#numeroHistorial').val();
     var id_especialidad=$('#id_especialidad').val();
     var fecha_asistencia=$('#fecha_creacion').val();
     var id_historial_medico=$('#id_historial_medico').val();
     var id_consulta=$('#id_consulta').val();
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= now.getFullYear()+"/"+month+"/"+day;
     var url='/actualizar_alergias_medicamentos';

   if (alergias.trim()==='')
   {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
     var data=
     {
       id :id ,
       alergias  :alergias,  
       today:today,      
     }
    
    
     $.ajax
     ({
         url:url,
         method:'POST',
         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {
           
              if(data=='1')
              {
                    alert('Registro Actualizado');
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
              }
              else
              {
                   alert('Error en la Incorporación del Registro');
              }
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
          }
      });
   }
});

$(document).on('click','#btnMostrar_antecedentes_pf', function(e)
  {
     $("#antecedentes_familiares").modal("show"); 
     $("#btnActualizar_antecedentes_familiares").hide();
     $("#btnagregar_antecedentes_familiares").show();
     $(".observacion").val("");
  });

  /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES PATOLOGICOS CORTESIAES***** */
  $(document).on('click','#btnagregar_antecedentes_familiares', function(e)
  {

   var antecedentes_patologicosf=$('#antecedentes_patologicosf').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (antecedentes_patologicosf.trim()==='')
   {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
      var data=
         {
            n_historial:n_historial,
            antecedentes_patologicosf:antecedentes_patologicosf,
            id_consulta:id_consulta,
         }
      
         var url='/agregar_antecedentes_patologicosf';
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                  //alert('Procesando Información ...');
               },
               success:function(data)
               {
                  //alert(data);
                  if(data===1)
                  {
                     alert('Registro Exitoso');
                     window.location = '/medico_Consultas/'
                  }
                  else if(data===2)
                  {
                     alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                  
                  }
                  else
                  {
                  alert('Error en la Incorporación del registro');
                  }
                  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
            
            
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
         });
      }  
   });


   $('#lista_antecedentes_pf').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#antecedentes_familiares").modal("show"); 
        $("#btnagregar_antecedentes_familiares").hide()
        $("#btnActualizar_antecedentes_familiares").show() 
        $('#antecedentes_patologicosf').val(observacion);
        $('#id').val(id);

   });




   $(document).on('click','#btnActualizar_antecedentes_familiares', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var antecedentes_patologicosf=$('#antecedentes_patologicosf').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_antecedentes_patologicosf';

        if (antecedentes_patologicosf.trim()==='')
        {
         alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
        else
        {    
         var data=
         {
            id :id ,
            antecedentes_patologicosf  :antecedentes_patologicosf, 
            today:today,       
         }
         
         
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                  if(data=='1')
                  {
                        alert('Registro Actualizado');
                        window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                        alert('Error en la Incorporación del Registro');
                  }
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
            });
        }   
   
  });


  $(document).on('click','#btnMostrar_antecedentes_qx', function(e)
  {
     $("#modal_antecedentes_quirurgicos").modal("show"); 
     $("#btnActualizar_antecedentes_quirurgicos").hide();
     $("#btnagregar_antecedentes_quirurgicos").show();
     $(".observacion").val("");
  });

  
    /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES QUIRURGICOS***** */
  $(document).on('click','#btnagregar_antecedentes_quirurgicos', function(e)
  {
    
   var antecedentes_quirurgicos=$('#antecedentes_quirurgicos').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (antecedentes_quirurgicos.trim()==='')
   {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
     var data=
       {
          n_historial:n_historial,
          antecedentes_quirurgicos:antecedentes_quirurgicos,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_antecedentes_quirurgicos';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               if(data===1)
               {
                  alert('Registro Exitoso');
                  window.location = '/medico_Consultas/'
               }
               else if(data===2)
               {
                  alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                 
               }
               else
               {
                 alert('Error en la Incorporación del registro');
               }
               window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
      } 
   });

   $('#lista_antecedentes_qx_mg').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_antecedentes_quirurgicos").modal("show"); 
        $("#btnagregar_antecedentes_quirurgicos").hide()
        $("#btnActualizar_antecedentes_quirurgicos").show() 
        $('#antecedentes_quirurgicos').val(observacion);
        $('#id').val(id);

   });


   $(document).on('click','#btnActualizar_antecedentes_quirurgicos', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var antecedentes_quirurgicos=$('#antecedentes_quirurgicos').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_antecedentes_quirurgicos';
        if (antecedentes_quirurgicos.trim()==='')
        {
         alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
        else
        {
         var data=
         {
            id :id ,
            antecedentes_quirurgicos  :antecedentes_quirurgicos,  
            today:today,      
         }
         
         
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                  if(data=='1')
                  {
                        alert('Registro Actualizado');
                        window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                        alert('Error en la Incorporación del Registro');
                  }
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
            });
         }
  });


  $(document).on('click','#btnMostrar_antecedentes_pp', function(e)
  {
     $("#antecedentes_personales").modal("show"); 
     $("#btnActualizar_antecedentes_personales").hide();
     $("#btnagregar_antecedentes_personales").show(); 
     $(".observacion").val("");  
  });

     /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES PATOLOGICOS PERSONALES***** */
  $(document).on('click','#btnagregar_antecedentes_personales', function(e)
  {
      var antecedentes_patologicosp=$('#antecedentes_patologicosp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (antecedentes_patologicosp.trim()==='')
      {
       alert('NO DEBE DEJAR EL CAMPO VACIO');
      }
      else
      {
         var data=
         {
            n_historial:n_historial,
            antecedentes_patologicosp:antecedentes_patologicosp,
            id_consulta:id_consulta,
         }
      
         var url='/agregar_antecedentes_patologicosp';
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                  //alert('Procesando Información ...');
               },
               success:function(data)
               {
                  //alert(data);
                  if(data===1)
                  {
                     alert('Registro Exitoso');
                     window.location = '/medico_Consultas/'
                  }
                  else if(data===2)
                  {
                     alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                  
                  }
                  else
                  {
                  alert('Error en la Incorporación del registro');
                  }
                  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
            
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
         });
      }  
   });


   $('#lista_antecedentes_pp').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#antecedentes_personales").modal("show"); 
        $("#btnagregar_antecedentes_personales").hide()
        $("#btnActualizar_antecedentes_personales").show() 
        $('#antecedentes_patologicosp').val(observacion);
        $('#id').val(id);

   });

   $(document).on('click','#btnActualizar_antecedentes_personales', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var antecedentes_patologicosp=$('#antecedentes_patologicosp').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_antecedentes_patologicosp';

        if (antecedentes_patologicosp.trim()==='')
        {
         alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
        else
        {
         var data=
         {
            id :id ,
            antecedentes_patologicosp  :antecedentes_patologicosp,   
            today:today,     
         }
         
         
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                  if(data=='1')
                  {
                        alert('Registro Actualizado');
                        window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                        alert('Error en la Incorporación del Registro');
                  }
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
            });
         }  
   
  });

  $(document).on('click','#btnMostrar_examen_fisico', function(e)
  {
     $("#modalexamen_fisico").modal("show"); 
     $("#btnActualizar_examen_fisico").hide();
     $("#btnagregar_examen_fisico").show();   
     $(".observacion").val("");
  });

     /**********ESTE METODO AGREGA LOS DATOS DEl EXAMEN FISICO***** */
  $(document).on('click','#btnagregar_examen_fisico', function(e)
  {
      var examen_fisico=$('#obs_examen_fisico').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();

      if (examen_fisico.trim()==='')
      {
       alert('NO DEBE DEJAR EL CAMPO VACIO');
      }
      else
      {
         var data=
         {
            n_historial:n_historial,
            examen_fisico:examen_fisico,
            id_consulta:id_consulta
         }
      
         var url='/agregar_examen_fisico';
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                  //alert('Procesando Información ...');
               },
               success:function(data)
               {
                  if(data===1)
                  {
                     alert('Registro Exitoso');
                     window.location = '/medico_Consultas/'
                  }
                  else if(data===2)
                  {
                     alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                  
                  }
                  else
                  {
                  alert('Error en la Incorporación del registro');
                  }
                  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
            
            
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
         });
      }
   });



   $('#lista_examen_fisico').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modalexamen_fisico").modal("show"); 
        $("#btnagregar_examen_fisico").hide()
        $("#btnActualizar_examen_fisico").show() 
        $('#obs_examen_fisico').val(observacion);
        $('#id').val(id);

   });



   $(document).on('click','#btnActualizar_examen_fisico', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var examen_fisico=$('#obs_examen_fisico').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_examen_fisico';


      if (examen_fisico.trim()==='')
      {
       alert('NO DEBE DEJAR EL CAMPO VACIO');
      }
      else
      {
         var data=
         {
            id :id ,
            examen_fisico  :examen_fisico,    
            today:today,    
         }
         
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                  if(data=='1')
                  {
                        alert('Registro Actualizado');
                        window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                        alert('Error en la Incorporación del Registro');
                  }
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
            });
         }
  });



  $(document).on('click','#btnMostrar_paraclinicos', function(e)
  {
     $("#modal_paraclinicos").modal("show"); 
     $("#btnActualizar_paraclinicos").hide();
     $("#btnagregar_paraclinicos").show();  
     $(".observacion").val(""); 
  });



  /**********ESTE METODO AGREGA LOS DATOS DE PARACLINICOS***** */
  $(document).on('click','#btnagregar_paraclinicos', function(e)
  {
    
      var paraclinicos=$('#obs_paraclinicos').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();

      if (paraclinicos.trim()==='')
      {
       alert('NO DEBE DEJAR EL CAMPO VACIO');
      }
      else
      {
         var data=
         {
            n_historial:n_historial,
            paraclinicos:paraclinicos,
            id_consulta:id_consulta,
         }
      
         var url='/agregar_paraclinicos';
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                  //alert('Procesando Información ...');
               },
               success:function(data)
               {
                  //alert(data);
                  if(data===1)
                  {
                     alert('Registro Exitoso');
                     window.location = '/medico_Consultas/'
                  }
                  else if(data===2)
                  {
                     alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                  
                  }
                  else
                  {
                  alert('Error en la Incorporación del registro');
                  }
                  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
            
            
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
         });
      }
   });



   $('#lista_paraclinicos').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_paraclinicos").modal("show"); 
        $("#btnagregar_paraclinicos").hide()
        $("#btnActualizar_paraclinicos").show() 
        $('#obs_paraclinicos').val(observacion);
        $('#id').val(id);

   });



   $(document).on('click','#btnActualizar_paraclinicos', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var paraclinicos=$('#obs_paraclinicos').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_paraclinicos';
       
        if (paraclinicos.trim()==='')
        {
         alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
        else
        {
       
         var data=
         {
            id :id ,
            paraclinicos  :paraclinicos,
            today:today,        
         }
         
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                  if(data=='1')
                  {
                        alert('Registro Actualizado');
                        window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                        alert('Error en la Incorporación del Registro');
                  }
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
            });
         }
  });


  $(document).on('click','#btnMostrar_impresion_diag', function(e)
  {
     $("#modal_impresion_diag").modal("show"); 
     $("#btnActualizar_impresion_diag").hide();
     $("#btnagregar_impresion_diag").show();  
     $(".observacion").val(""); 
  });


    /**********ESTE METODO AGREGA LOS DATOS DEL IMPRESION DIAGNOSTICA***** */
  $(document).on('click','#btnagregar_impresion_diag', function(e)
  {
   
   var impresiondiagnostica=$('#obs_impresion_diag').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (impresiondiagnostica.trim()==='')
   {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
      var data=
       {
          n_historial:n_historial,
          impresiondiagnostica:impresiondiagnostica,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_impresiondiagnostica';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
              //alert(data);
              if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
         
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
      }
   });


   $('#lista_impresion_diag').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_impresion_diag").modal("show"); 
        $("#btnagregar_impresion_diag").hide()
        $("#btnActualizar_impresion_diag").show() 
        $('#obs_impresion_diag').val(observacion);
        $('#id').val(id);

   });


   $(document).on('click','#btnActualizar_impresion_diag', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var impresiondiagnostica=$('#obs_impresion_diag').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_impresiondiagnostica';
        if (impresiondiagnostica.trim()==='')
        {
         alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
        else
        {
         var data=
         {
            id :id ,
            impresiondiagnostica  :impresiondiagnostica,
            today:today,        
         }
         
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                  if(data=='1')
                  {
                        alert('Registro Actualizado');
                        window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                        alert('Error en la Incorporación del Registro');
                  }
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
            });
        }   
   
  });



  $(document).on('click','#btnMostrar_plan', function(e)
  {
     $("#modal_plan").modal("show"); 
     $("#btnActualizar_plan").hide();
     $("#btnagregar_plan").show();   
     $(".observacion").val("");
  });


 /**********ESTE METODO AGREGA LOS DATOS DEL PLAN***** */
  $(document).on('click','#btnagregar_plan', function(e)
  {

      var plan=$('#obs_plan').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
     
      if (plan.trim()==='')
   {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
      var data=
       {
          n_historial:n_historial,
          plan:plan,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_plan';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               //alert(data);
               if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
   } 
   });



   $('#lista_plan').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_plan").modal("show"); 
        $("#btnagregar_plan").hide()
        $("#btnActualizar_plan").show() 
        $('#obs_plan').val(observacion);
        $('#id').val(id);

   });
   

   $(document).on('click','#btnActualizar_plan', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var plan=$('#obs_plan').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_plan';
       
        if (plan.trim()==='')
        {
         alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
        else
        {
         var data=
         {
            id :id ,
            plan  :plan,
            today:today,        
         }
         
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                  if(data=='1')
                  {
                        alert('Registro Actualizado');
                        window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                        alert('Error en la Incorporación del Registro');
                  }
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
            });
         }
  });


 //*******************************************************ATENCION PSICOLOGICA PRIMARIA****************************************************

 $('#aciones_Psicol').on('change',function(e)
 { 
     descripcioncompleta=$('#aciones_Psicol option:selected').val(); 

switch(descripcioncompleta) {
  case '1':
  
   
     $(".enfermedadactual").show(); 
     //document.getElementById("enfermedadactual").style.visibility = "visible"; 

     $("#table_diagnostico").dataTable().fnDestroy();

     listar_enfermedad_actual_PSICO()
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
     break;
  case '2':

     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").show();
     $("#alergias_medicamentos").dataTable().fnDestroy();
     listar_alergias_medicamentos()
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
     break;
   case '3':

     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").show();
     $("#antecedentes_pf").dataTable().fnDestroy();
     antecedentespatologicosfamiliares()
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide(); 
     $('.texto').show();
     $(".habitos").hide();
   break;
   case '4':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").show();
     $("#antecedentes_qx").dataTable().fnDestroy();
     antecedentesquirurgicos();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
   break;
   case '5':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").show();
     $("#antecedentes_pp").dataTable().fnDestroy();
     antecedentespatologicospersonales()
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
   break  
   case '6':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").show();
     $("#examen_fisico").dataTable().fnDestroy();
     listar_examen_fisico()
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
   break  
   case '7':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").show();
     $("#tableparaclinicos").dataTable().fnDestroy();
     listar_paraclinicos();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $(".habitos").hide();
   break  
   case '8':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").show();
     $("#impresion_diag").dataTable().fnDestroy();
     listar_impresion_diag();
     $('.texto').show();
     $(".habitos").hide();
   break  
   case '9':
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").show();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $("#table_plan").dataTable().fnDestroy();
     listar_plan_PSICO();
     $(".habitos").hide();
   break  


   case '10':
     $(".habitos").show();
     $("#habitos_psicosociales").dataTable().fnDestroy();
     listar_habitos_psicosociales();
     $(".enfermedadactual").hide(); 
     $(".alergiasmedicamentos").hide();
     $(".antecedentespatologicosfamiliares").hide();
     $(".antecedentesquirurgicos").hide();
     $(".antecedentespatologicospersonales").hide();
     $(".examenfisico").hide();
     $(".paraclinicos").hide();
     $(".plan").hide();
     $(".impresiondiagnostica").hide();
     $('.texto').show();
     $("#table_plan").dataTable().fnDestroy();
     listar_plan();
   break  

   case '12':

   
   e.preventDefault('');
   let nombre =$("#nombrepellidoB").val();
   let fecha_nacimiento =$("#fecha_nacimiento").val();
   let cedulab =$("#cedulab").val();
   let edad =$("#edad").val(); 
   let telefono =$("#telefono").val();
   let numeroHistorial =$("#numeroHistorial").val();
   $("#modal_reposos").modal("show");  
   $('#modal_reposos').find('#nombretitular').val(nombre); 
   $('#modal_reposos').find('#fecha_nacimientotitular').val(fecha_nacimiento); 
   $('#modal_reposos').find('#cedulatitular').val(cedulab);
   $('#modal_reposos').find('#edadtitular').val(edad); 
   $('#modal_reposos').find('#telefonotitular').val(telefono);
   $('#modal_reposos').find('#unidadtitular').val('CORTESIA');
   $('#modal_reposos').find('#n_historialT').val(numeroHistorial);
   
   break 

  
}



 });


 $(document).on('click','#btnMostrar_enfermedad_actual_psico', function(e)
 {
    $("#modal_enfermedad_psico").modal("show"); 
    $("#btnActualizar_enfermedadactual_psico").hide();
    $("#btnagregar_enfermedadactual_psico").show(); 
    $("#enfermedadactual").val('');
    $(".observacion").val("");
 });


/**********ESTE METODO AGREGA LOS DATOS ENFERMEDAD ACTUAL***** */
$(document).on('click','#btnagregar_enfermedadactual_psico', function(e)
{ 
   var enfermedad_actual=$('#enfermedadactual_psico').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (enfermedad_actual.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {

     var data=
     {
        n_historial:n_historial,
        enfermedad_actual:enfermedad_actual,
        id_consulta:id_consulta,
     }

   
 
     var url='/agregar_enfermedad_actual';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
                alert('Registro Exitoso');
                window.location = '/medico_Consultas/'
             }
             else if(data===2)
             {
                alert('YA EXISTE UN REGISTRO PARA ESTA CONSULTA');
               
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
       }
 });

 $('#table_diagnostico').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_enfermedad_psico").modal("show"); 
      $("#btnagregar_enfermedadactual_psico").hide()
      $("#btnActualizar_enfermedadactual_psico").show() 
      $('#enfermedadactual_psico').val(observacion);
      $('#id').val(id);

 });




 $(document).on('click','#btnActualizar_enfermedadactual_psico', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var observacion  =$('#enfermedadactual_psico').val();
      var enfermedad_actual=$('#enfermedadactual_psico').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;

      var url='/actualizar_enfermedad_actual';
      if (observacion.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
        var data=
        {
             id :id ,
             observacion  :observacion,  
             today:today,      
        }
        
        
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
             },
             success:function(data)
             {
             
                  if(data=='1')
                  {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                       alert('Error en la Incorporación del Registro');
                  }
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
   }    
   
});




$(document).on('click','#btnMostrar_plan_psico', function(e)
{
   $("#modal_plan_psico").modal("show"); 
   $("#btnActualizar_plan_psico").hide();
   $("#btnagregar_plan_psico").show(); 
   $(".observacion").val("");  
});


/**********ESTE METODO AGREGA LOS DATOS DEL PLAN***** */
$(document).on('click','#btnagregar_plan_psico', function(e)
{

    var plan=$('#obs_plan_psico').val();
    var cedula      =$('#cedulab').val();
    var tipobeneficiario  ='C';
    var n_historial=$('#numeroHistorial').val();
    var id_especialidad=$('#id_especialidad').val();
    var fecha_asistencia=$('#fecha_creacion').val();
    var id_historial_medico=$('#id_historial_medico').val();
    var id_consulta=$('#id_consulta').val();
    if (plan.trim()==='')
   {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
    else
   { 
    var data=
     {
        n_historial:n_historial,
        plan:plan,
        id_consulta:id_consulta,
     }
 
     var url='/agregar_plan';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
            {
               alert('Registro Exitoso');
               window.location = '/medico_Consultas/'
            }
            else if(data===2)
            {
               alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
              
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
   }
 });



 $('#table_plan_PSICO').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_plan_psico").modal("show"); 
      $("#btnagregar_plan_psico").hide()
      $("#btnActualizar_plan_psico").show() 
      $('#obs_plan_psico').val(observacion);
      $('#id').val(id);

 });
 

 $(document).on('click','#btnActualizar_plan_psico', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var plan=$('#obs_plan_psico').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_plan';
      if (plan.trim()==='')
      {
           alert('NO DEBE DEJAR EL CAMPO VACIO');
      }
       else
      {     
        var data=
        {
             id :id ,
             plan  :plan,
             today:today,        
        }
        
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
             },
             success:function(data)
             {
             
                  if(data=='1')
                  {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                       alert('Error en la Incorporación del Registro');
                  }
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
      }
});






$('#btnagregar').on('click',function(e)
{ 
    $("#observacion").val(' ');
    $("#evolucion").val(' ');
   
    let impresiondiagnostica=$('#textimpresiond').val();
    impresiondiagnostica=impresiondiagnostica.trim();
    if (impresiondiagnostica=='')
       {
          alert('EL CAMPO IMPRESION DIAGNSTICA ES OBLIGATORIO');
       }
    else
       {
          $("#modal").modal("show"); 
          $('#modal').find('#btnGuardar').show();
          $('#modal').find('#btnActualizar').hide();
       }   
});

$('#btnGuardar').on('click',function(e)
{ 
     

     e.preventDefault();
     var numeroHistorial  =$('#numeroHistorial').val();
     var medico_id=$('#medico_id').val();
     var evolucion=$('#evolucion').val();
     var cedula_beneficiario=$('#cedulab').val();
     var tbeneficiario=$('#tipobeneficiario').val();
     var id_especialidad=$('#id_especialidad').val();
     var observacion=$('#observacion').val();
     var fecha_1ra_consulta=$('#fecha_1ra_consulta').val();
     let tipobeneficiario = tbeneficiario.trim() 
     let id_consulta=$('#id_consulta').val();
     let id_historial_medico =$('#id_historial_medico').val();
     let impresiondiagnostica=$('#textimpresiond').val();
     impresiondiagnostica=impresiondiagnostica.trim();
      // ***SE USO EL  moment PARA CONVERTIR LA FECHA******
      let fechaconvertida=moment(fecha_1ra_consulta,'DD-MM-YYYY'); 
      fechaconvertida=fechaconvertida.format('YYYY-MM-DD');

      if (evolucion.trim()==='' &&  observacion.trim()==='') {
          alert('DEBE INTRODUCIR ALGUNO DE LOS DOS CAMPOS!!!'); 
      }
     else
     {

     
     var url='/atencion_psicologia_p';
     var data=
     {
          numeroHistorial          :numeroHistorial,
          medico_id                :medico_id, 
          evolucion                :evolucion ,   
          cedula_beneficiario      :cedula_beneficiario,
          tipobeneficiario         :tipobeneficiario,
          id_especialidad          :id_especialidad,
          fecha_1ra_consulta       :fecha_1ra_consulta,
          observacion  :observacion,
          impresiondiagnostica:impresiondiagnostica,
          id_consulta:id_consulta,
     }
    
     $.ajax
     ({
         url:url,
         method:'POST',
         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {
               if(data=='1')
               {
               //           alert('Registro Incorporado');
                    alert('Registro exitoso de la ID y del Seguimiento');
               // window.location = '/Actualizar_Citas/'+cedula_beneficiario+'/'+tipobeneficiario+'/'+numeroHistorial+'/'+id_especialidad+'/'+fechaconvertida+'/'+id_historial_medico+'/'+id_consulta         
               }
               else if(data=='2')
               {
               //      alert('No se pudo se pudo registrar la impresion diagnostica ');
                    alert('Registro exitoso de la ID pero no se registró el Seguimiento');
                    
               }
               else if(data=='3')
               {
               //      alert('Ya existe un Registro en  Impresion diagnostica de Psicologia ');
                    alert('Problemas en el registro de la ID');
               }
               else if(data=='4')
               {
                    alert('Se registró el seguimiento a la ID Existente');
               }
               else if(data=='5')
               {
                    alert('No se pudo realizar el registro del seguimiento a la ID Existente');          
               }
               window.location = '/Actualizar_Citas/'+cedula_beneficiario+'/'+tipobeneficiario+'/'+numeroHistorial+'/'+id_especialidad+'/'+fechaconvertida+'/'+id_historial_medico+'/'+id_consulta 
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
          }
      });
     }
   

});


$('#table_psicologia_primaria').on('click','.Editar', function(e)
{ 
     e.preventDefault();
     let observacion=$(this).attr('observacion')
     let evolucion=$(this).attr('evolucion');
     let id=$(this).attr('id');
     $("#modal").modal("show"); 
     $('#modal').find('#btnActualizar').show();
     $('#modal').find('#btnGuardar').hide();
     $('#evolucion').val(evolucion);
     $('#observacion').val(observacion);
     $('#id_psicologiap').val(id);

});

$(document).on('click','#btnActualizar', function(e)
 {
     let observacion=$('#observacion').val();
     let evolucion=$('#evolucion').val();
     let id_psicologiap=$('#id_psicologiap').val();

     var numeroHistorial  =$('#numeroHistorial').val();
     var medico_id=$('#medico_id').val();
     var cedula_beneficiario=$('#cedulab').val();
     var tbeneficiario=$('#tipobeneficiario').val();
     var id_especialidad=$('#id_especialidad').val();
     var fecha_1ra_consulta=$('#fecha_1ra_consulta').val();
     let id_consulta=$('#id_consulta').val();
      let id_historial_medico =$('#id_historial_medico').val();
       // ***SE USO EL  moment PARA CONVERTIR LA FECHA******
       let fechaconvertida=moment(fecha_1ra_consulta,'DD-MM-YYYY'); 
       fechaconvertida=fechaconvertida.format('YYYY-MM-DD');
     let tipobeneficiario = tbeneficiario.trim() 

     if (evolucion.trim()==='' &&  observacion.trim()==='') {
          alert('DEBE INTRODUCIR ALGUNO DE LOS DOS CAMPOS!!!'); 
      }
      else{

    
          var data=
          {
               id_psicologiap:id_psicologiap,
               observacion:observacion,
               evolucion:evolucion,
               
          }
     
          var url='/actualizar_Psicologia_Primaria';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
               //alert(data);
               if(data===1)
               {
                    alert('Registro Actualizado');
               }
               else
               {
                    alert('Error en la Incorporación del registro');
               }
               window.location = '/Actualizar_Citas/'+cedula_beneficiario+'/'+tipobeneficiario+'/'+numeroHistorial+'/'+id_especialidad+'/'+fechaconvertida+'/'+id_historial_medico+'/'+id_consulta 
          
               },
               error:function(xhr, status, errorThrown)
               {
               alert(xhr.status);
               alert(errorThrown);
               }
          });
     } 
  });





// *************************************MEDICINA INTERNA************************************
$(document).on('click','#btnAgregar_signos', function(e)
{
   $(".signosAC").show(); 
   $(".enfermedadactual_mi").hide(); 
   $(".habitos_mi").hide();
   $(".medicamentos_mi").hide();
   $(".antecedentesquirurgicos_mi").hide();
   $(".antecedentespatologicospersonales_mi").hide();
   $(".examenfisico_mi").hide();
   $(".paraclinicos_mi").hide();
   $(".plan_mi").hide();
   $(".impresiondiagnostica_mi").hide();
   $('.texto_mi').hide();
});


$(document).on('click','#btnActualizar_signos_mi', function(e)
{
     e.preventDefault();

     var cedula      =$('#cedulab').val(); 
     let motivo_consulta    =$('#motivo_consulta_mi').val(); 
     var id_especialidad=$('#id_especialidad').val();
     var id_historial_medico=$('#id_historial_medico').val()
     var n_historial=$('#numeroHistorial').val();
     var id_medico=$('#medico_id').val();
     var peso=$('#peso_mi').val();
     var talla=$('#talla_mi').val();
     var spo2=$('#spo2_mi').val();
     var frecuencia_c=$('#frecuencia_c_mi').val();
     var frecuencia_r=$('#frecuencia_r_mi').val();
     var temperatura=$('#temperatura_mi').val();
     var ta_alta=$('#ta_alta_mi').val();
     var ta_baja=$('#ta_baja_mi').val(); 
     var fecha_asistencia_normal=$('#fecha_del_dia_mi').val();
     let fechaconvertida=moment(fecha_asistencia_normal,'DD-MM-YYYY'); 
     fecha_asistencia=fechaconvertida.format('YYYY-MM-DD');
     var id_consulta=$('#id_consulta').val();
     var imc=$('#imc_mi').val();
     let user_id=$('#id_user').val();
     let tipo_beneficiario='C';
     var url='/actualizar_signos_vitales_citas';

     if (peso=='') 
     {
          peso=0; 
     }
     if (talla=='') 
     {
          talla=0; 
          }
     if (spo2=='') 
     {
          spo2=0; 
     }if (frecuencia_c=='') 
     {
          frecuencia_c=0; 
     } if (frecuencia_r=='')
     {
          frecuencia_r=0; 
     }if (temperatura=='') 
     {
          temperatura=0; 
     } if (ta_alta=='') 
     {
          ta_alta=0; 
     } if (ta_baja=='')
     {
          ta_baja=0; 
     }if (imc=='')
     {
          imc=0; 
     }

     var data=
     {
     fecha_asistencia:fecha_asistencia,
     n_historial   :n_historial,
     id_medico     :id_medico,
     peso          :peso,               
     talla         :talla,
     spo2          :spo2,
     frecuencia_c  :frecuencia_c,
     frecuencia_r  :frecuencia_r,
     temperatura   :temperatura,
     ta_alta       :ta_alta,
     ta_baja       :ta_baja,
     imc:imc,
     tipo_beneficiario:tipo_beneficiario,
     motivo_consulta:motivo_consulta, 
     user_id:user_id,
     id_consulta:id_consulta,
     }

     //      //console.log(data);
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
               
               if(data=='1')
               {
                    alert('SIGNOS VITALES ACTUALIZADOS ');
               
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                   
               }
               else if(data=='0')
               {
                    alert('Error en la Incorporación del Registro');
               }

               else if(data=='2')
               {
                    alert('La cedula posee Historial');
               }


          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
     });
     
});


// ************ ACCIONES MEDICINA INTERNA*********
 $('#aciones_mi').on('change',function(e)
 { 
     descripcioncompleta=$('#aciones_mi option:selected').val(); 
//alert(descripcioncompleta);
switch(descripcioncompleta) {
  case '1':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").show(); 
     $("#table_enfermedad_actual_mi").dataTable().fnDestroy();
     listar_enfermedad_actual_mi();
     $(".habitos_mi").hide();
     $(".antecedentespatologicosfamiliares_mi").hide();
     $(".antecedentesquirurgicos_mi").hide();
     $(".antecedentespatologicospersonales_mi").hide();
     $(".examenfisico_mi").hide();
     $(".paraclinicos_mi").hide();
     $(".plan_mi").hide();
     $(".impresiondiagnostica_mi").hide();
     $('.texto_mi').show();
     
     break;
  case '2':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").hide(); 
     $(".habitos_mi").show();
     $("#habitos_psicosociales_mi").dataTable().fnDestroy();
     listar_habitos_psicosociales_mi();
     $(".antecedentespatologicosfamiliares_mi").hide();
     $(".antecedentesquirurgicos_mi").hide();
     $(".antecedentespatologicospersonales_mi").hide();
     $(".examenfisico_mi").hide();
     $(".paraclinicos_mi").hide();
     $(".plan_mi").hide();
     $(".impresiondiagnostica_mi").hide();
     $('.texto_mi').show();
    
     break;
   case '3':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").hide(); 
     $(".habitos_mi").hide();
     $(".medicamentos_mi").show();
    $("#table_medicamentos_mi").dataTable().fnDestroy();
     listar_medicamentos_mi(); 
     $(".antecedentesquirurgicos_mi").hide();
     $(".antecedentespatologicospersonales_mi").hide();
     $(".examenfisico_mi").hide();
     $(".paraclinicos_mi").hide();
     $(".plan_mi").hide();
     $(".impresiondiagnostica_mi").hide(); 
     $('.texto_mi').show();
    
   break;
   case '4':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").hide(); 
     $(".habitos_mi").hide();
     $(".medicamentos_mi").hide();
     $(".antecedentesquirurgicos_mi").show();
     $(".antecedentespatologicospersonales_mi").hide();
     $(".examenfisico_mi").hide();
     $(".paraclinicos_mi").hide();
     $(".plan_mi").hide();
     $(".impresiondiagnostica_mi").hide();
     $('.texto_mi').show();
     $("#btnAntecedentes_quirurgicos").show(); 
     $("#antecedentes_qx_mi").dataTable().fnDestroy();
     antecedentesquirurgicos_mi();   
    
   break;
   case '5':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").hide(); 
     $(".habitos_mi").hide();
     $(".medicamentos_mi").hide();
     $(".antecedentesquirurgicos_mi").hide();
     $(".antecedentespatologicospersonales_mi").show();
     $("#antecedentes_pp_mi").dataTable().fnDestroy();
     antecedentespatologicospersonales_mi()
     $(".examenfisico_mi").hide();
     $(".paraclinicos_mi").hide();
     $(".plan_mi").hide();
     $(".impresiondiagnostica_mi").hide();
     $('.texto_mi').show();
   
   break  
   case '6':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").hide(); 
     $(".habitos_mi").hide();
     $(".medicamentos_mi").hide();
     $(".antecedentesquirurgicos_mi").hide();
     $(".antecedentespatologicospersonales_mi").hide();
     $(".examenfisico_mi").show();
     $(".paraclinicos_mi").hide();
     $(".plan_mi").hide();
     $(".impresiondiagnostica_mi").hide();
     $('.texto_mi').show();
     $("#btnexamen_fisico").show();
     $("#examen_fisico_mi").dataTable().fnDestroy();
     listar_examen_fisico_mi()

   break  
   case '7':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").hide(); 
     $(".habitos_mi").hide();
     $(".medicamentos_mi").hide();
     $(".antecedentesquirurgicos_mi").hide();
     $(".antecedentespatologicospersonales_mi").hide();
     $(".examenfisico_mi").hide();
     $(".paraclinicos_mi").show();
     $(".plan_mi").hide();
     $(".impresiondiagnostica_mi").hide();
     $('.texto_mi').show();
     $("#btnparaclinicos").show();
     $("#tableparaclinicos_mi").dataTable().fnDestroy();
     listar_paraclinicos_mi();
   
   break  
   case '8':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").hide(); 
     $(".habitos_mi").hide();
     $(".medicamentos_mi").hide();
     $(".antecedentesquirurgicos_mi").hide();
     $(".antecedentespatologicospersonales_mi").hide();
     $(".examenfisico_mi").hide();
     $(".paraclinicos_mi").hide();
     $(".plan_mi").hide();
     $(".impresiondiagnostica_mi").show();
     $("#impresion_diag_mi").dataTable().fnDestroy();
     listar_impresion_diag_mi();
     $('.texto_mi').show();
    
   break  
   case '9':
     $(".signosAC").hide(); 
     $(".enfermedadactual_mi").hide(); 
     $(".habitos_mi").hide();
     $(".medicamentos_mi").hide();
     $(".antecedentesquirurgicos_mi").hide();
     $(".antecedentespatologicospersonales_mi").hide();
     $(".examenfisico_mi").hide();
     $(".paraclinicos_mi").hide();
     $(".plan_mi").show();
     $("#table_plan_mi").dataTable().fnDestroy();
     listar_plan_mi();
     $(".impresiondiagnostica_mi").hide();
     $('.texto_mi').show();
   break  

   case '12':

   
   e.preventDefault('');
   let nombre =$("#nombrepellidoB").val();
   let fecha_nacimiento =$("#fecha_nacimiento").val();
   let cedulab =$("#cedulab").val();
   let edad =$("#edad").val(); 
   let telefono =$("#telefono").val();
   let numeroHistorial =$("#numeroHistorial").val();
   $("#modal_reposos").modal("show");  
   $('#modal_reposos').find('#nombretitular').val(nombre); 
   $('#modal_reposos').find('#fecha_nacimientotitular').val(fecha_nacimiento); 
   $('#modal_reposos').find('#cedulatitular').val(cedulab);
   $('#modal_reposos').find('#edadtitular').val(edad); 
   $('#modal_reposos').find('#telefonotitular').val(telefono);
   $('#modal_reposos').find('#unidadtitular').val('CORTESIA');
   $('#modal_reposos').find('#n_historialT').val(numeroHistorial);
   
   break 



}



 });

 $(document).on('click','#btnMostrar_enfermedad_actual_mi', function(e)
 {

    $("#modal_enfermedad_mi").modal("show"); 
    $("#btnActualizar_enfermedadactual_mi").hide();+
    $("#btnagregar_enfermedadactual_mi").show();
    $(".observacion").val(""); 
 });


   /**********ESTE METODO AGREGA LOS DATOS ENFERMEDAD ACTUAL MEDICINA INTERNA***** */
  $(document).on('click','#btnagregar_enfermedadactual_mi', function(e)
  {
     var cedula      =$('#cedulab').val();
     var tipobeneficiario  ='C';
     var id_especialidad=$('#id_especialidad').val();
     var fecha_asistencia=$('#fecha_creacion').val();
     var id_historial_medico=$('#id_historial_medico').val()
     var n_historial=$('#numeroHistorial').val();
     var id_consulta=$('#id_consulta').val();
     var enfermedad_actual=$('#enfermedadactual_mi').val();
     if (enfermedad_actual.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
    else
    {
       var data=
       {
          n_historial:n_historial,
          enfermedad_actual:enfermedad_actual,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_enfermedad_actual';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               //alert(data);
              //alert(data);
              if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSULTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
         
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
      }
   });


   $('#table_enfermedad_actual_mi').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_enfermedad_mi").modal("show"); 
        $("#btnagregar_enfermedadactual_mi").hide()
        $("#btnActualizar_enfermedadactual_mi").show() 
        $('#enfermedadactual_mi').val(observacion);
        $('#id').val(id);

   });




   $(document).on('click','#btnActualizar_enfermedadactual_mi', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var observacion  =$('#enfermedadactual_mi').val();
        var enfermedad_actual=$('#enfermedadactual_mi').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;

        var url='/actualizar_enfermedad_actual';
        if (observacion.trim()==='')
        {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
       }
       else
       {
          var data=
          {
               id :id ,
               observacion  :observacion,  
               today:today,      
          }
          
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     }    
     
  });


  $(document).on('click','#btnMostrar_antecedentes_pp_mi', function(e)
  {
     $("#antecedentes_personales_mi").modal("show"); 
     $("#btnActualizar_antecedentes_personales_mi").hide();
     $("#btnagregar_antecedentes_personales_mi").show(); 
     $(".observacion").val("");  
  });

 /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES PATOLOGICOS PERSONALES***** */
  $(document).on('click','#btnagregar_antecedentes_personales_mi', function(e)
  {
      var antecedentes_patologicosp=$('#antecedentes_patologicosp_mi').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      
      if (antecedentes_patologicosp.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
          var data=
          {
               n_historial:n_historial,
               antecedentes_patologicosp:antecedentes_patologicosp,
               id_consulta:id_consulta,
          }
     
          var url='/agregar_antecedentes_patologicosp';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
                    //alert(data);
                    if(data===1)
                    {
                    alert('Registro Exitoso');
                    window.location = '/medico_Consultas/'
                    }
                    else if(data===2)
                    {
                    alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                    
                    }
                    else
                    {
                    alert('Error en la Incorporación del registro');
                    }
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     } 
   });


  
   $('#lista_antecedentes_pp_mi').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
         $("#antecedentes_personales_mi").modal("show");
        $("#btnagregar_antecedentes_personales_mi").hide()
        $("#btnActualizar_antecedentes_personales_mi").show() 
        $('#antecedentes_patologicosp_mi').val(observacion);
        $('#id').val(id);

   });

   $(document).on('click','#btnActualizar_antecedentes_personales_mi', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var antecedentes_patologicosp=$('#antecedentes_patologicosp_mi').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_antecedentes_patologicosp';
     if (antecedentes_patologicosp.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
       
        var data=
        {
          id :id ,
          antecedentes_patologicosp  :antecedentes_patologicosp,   
          today:today,     
        }
       
       
        $.ajax
        ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {
              
                 if(data=='1')
                 {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                 }
                 else
                 {
                      alert('Error en la Incorporación del Registro');
                 }
            },
            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
             }
         });
     }
  });






  $(document).on('click','#btnMostrar_antecedentes_qx_mi', function(e)
  {
     $("#modal_antecedentes_quirurgicos_mi").modal("show"); 
     $("#btnActualizar_antecedentes_quirurgicos_mi").hide();
     $("#btnagregar_antecedentes_quirurgicos_mi").show();
     $(".observacion").val("");
  });

  
    /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES QUIRURGICOS MEDICINA INTERNA***** */
  $(document).on('click','#btnagregar_antecedentes_quirurgicos_mi', function(e)
  {
    
   var antecedentes_quirurgicos=$('#antecedentes_quirurgicos_mi').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (antecedentes_quirurgicos.trim()==='')
   {
   alert('NO DEBE DEJAR EL CAMPO VACIO');
  }
  else
  {
     var data=
       {
          n_historial:n_historial,
          antecedentes_quirurgicos:antecedentes_quirurgicos,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_antecedentes_quirurgicos';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               if(data===1)
               {
                  alert('Registro Exitoso');
                  window.location = '/medico_Consultas/'
               }
               else if(data===2)
               {
                  alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                 
               }
               else
               {
                 alert('Error en la Incorporación del registro');
               }
               window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     }  
   });

   $('#lista_antecedentes_qx_mi').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_antecedentes_quirurgicos_mi").modal("show"); 
        $("#btnagregar_antecedentes_quirurgicos_mi").hide()
        $("#btnActualizar_antecedentes_quirurgicos_mi").show() 
        $('#antecedentes_quirurgicos_mi').val(observacion);
        $('#id').val(id);

   });


   $(document).on('click','#btnActualizar_antecedentes_quirurgicos_mi', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var antecedentes_quirurgicos=$('#antecedentes_quirurgicos_mi').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_antecedentes_quirurgicos';
        if (antecedentes_quirurgicos.trim()==='')
        {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
       }
       else
       {
          var data=
          {
               id :id ,
               antecedentes_quirurgicos  :antecedentes_quirurgicos,  
               today:today,      
          }
          
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     }
  });


  $(document).on('click','#btnMostrar_examen_fisico_mi', function(e)
  {
     $("#modalexamen_fisico_mi").modal("show"); 
     $("#btnActualizar_examen_fisico_mi").hide();
     $("#btnagregar_examen_fisico_mi").show();  
     $(".observacion").val("");
  });

     /**********ESTE METODO AGREGA LOS DATOS DEl EXAMEN FISICO***** */
  $(document).on('click','#btnagregar_examen_fisico_mi', function(e)
  {
      var examen_fisico=$('#obs_examen_fisico_mi').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (examen_fisico.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {  
          var data=
          {
               n_historial:n_historial,
               examen_fisico:examen_fisico,
               id_consulta:id_consulta
          }
     
          var url='/agregar_examen_fisico';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
                    if(data===1)
                    {
                    alert('Registro Exitoso');
                    window.location = '/medico_Consultas/'
                    }
                    else if(data===2)
                    {
                    alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                    
                    }
                    else
                    {
                    alert('Error en la Incorporación del registro');
                    }
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               
               
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     } 
   });



   $('#lista_examen_fisico_mi').on('click','.Editar', function(e)
   { 
        e.preventDefault();
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modalexamen_fisico_mi").modal("show"); 
        $("#btnagregar_examen_fisico_mi").hide()
        $("#btnActualizar_examen_fisico_mi").show() 
        $('#obs_examen_fisico_mi').val(observacion);
        $('#id').val(id);

   });



   $(document).on('click','#btnActualizar_examen_fisico_mi', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var examen_fisico=$('#obs_examen_fisico_mi').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_examen_fisico';
       
     if (examen_fisico.trim()==='')
     {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
        var data=
        {
          id :id ,
          examen_fisico  :examen_fisico,    
          today:today,    
        }
        
        $.ajax
        ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {
              
                 if(data=='1')
                 {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                 }
                 else
                 {
                      alert('Error en la Incorporación del Registro');
                 }
            },
            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
             }
         });
     }
  });


  $(document).on('click','#btnMostrar_paraclinicos_mi', function(e)
  {
     $("#modal_paraclinicos_mi").modal("show"); 
     $("#btnActualizar_paraclinicos_mi").hide();
     $("#btnagregar_paraclinicos_mi").show();   
     $(".observacion").val("");
  });



  /**********ESTE METODO AGREGA LOS DATOS DE PARACLINICOS***** */
  $(document).on('click','#btnagregar_paraclinicos_mi', function(e)
  {
    
      var paraclinicos=$('#obs_paraclinicos_mi').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (paraclinicos.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
          var data=
          {
               n_historial:n_historial,
               paraclinicos:paraclinicos,
               id_consulta:id_consulta,
          }
     
          var url='/agregar_paraclinicos';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
                    //alert(data);
                    if(data===1)
                    {
                    alert('Registro Exitoso');
                    window.location = '/medico_Consultas/'
                    }
                    else if(data===2)
                    {
                    alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                    
                    }
                    else
                    {
                    alert('Error en la Incorporación del registro');
                    }
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               
               
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     } 
   });



   $('#lista_paraclinicos_mi').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_paraclinicos_mi").modal("show"); 
        $("#btnagregar_paraclinicos_mi").hide()
        $("#btnActualizar_paraclinicos_mi").show() 
        $('#obs_paraclinicos_mi').val(observacion);
        $('#id').val(id);

   });



   $(document).on('click','#btnActualizar_paraclinicos_mi', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var paraclinicos=$('#obs_paraclinicos_mi').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_paraclinicos';
        if (paraclinicos.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
       
        var data=
        {
          id :id ,
          paraclinicos  :paraclinicos,
          today:today,        
        }
        
        $.ajax
        ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {
              
                 if(data=='1')
                 {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                 }
                 else
                 {
                      alert('Error en la Incorporación del Registro');
                 }
            },
            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
             }
         });
     }
  });




  $(document).on('click','#btnMostrar_impresion_diag_mi', function(e)
  {
     $("#modal_impresion_diag_mi").modal("show"); 
     $("#btnActualizar_impresion_diag_mi").hide();
     $("#btnagregar_impresion_diag_mi").show(); 
     $(".observacion").val("");  
  });


    /**********ESTE METODO AGREGA LOS DATOS DEL IMPRESION DIAGNOSTICA MEDICINA INTERNA***** */
  $(document).on('click','#btnagregar_impresion_diag_mi', function(e)
  {
   
   var impresiondiagnostica=$('#obs_impresion_diag_mi').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (impresiondiagnostica.trim()==='')
   {
   alert('NO DEBE DEJAR EL CAMPO VACIO');
  }
  else
  {
      var data=
       {
          n_historial:n_historial,
          impresiondiagnostica:impresiondiagnostica,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_impresiondiagnostica';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
              //alert(data);
              if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
         
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     } 
   });


   $('#lista_impresion_diag_mi').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_impresion_diag_mi").modal("show"); 
        $("#btnagregar_impresion_diag_mi").hide()
        $("#btnActualizar_impresion_diag_mi").show() 
        $('#obs_impresion_diag_mi').val(observacion);
        $('#id').val(id);

   });


   $(document).on('click','#btnActualizar_impresion_diag_mi', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var impresiondiagnostica=$('#obs_impresion_diag_mi').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_impresiondiagnostica';
        if (impresiondiagnostica.trim()==='')
     {
          alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
        var data=
        {
          id :id ,
          impresiondiagnostica  :impresiondiagnostica,
          today:today,        
        }
        
        $.ajax
        ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {
              
                 if(data=='1')
                 {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                 }
                 else
                 {
                      alert('Error en la Incorporación del Registro');
                 }
            },
            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
             }
         });
     }
  });

  $(document).on('click','#btnMostrar_plan_mi', function(e)
  {
     $("#modal_plan_mi").modal("show"); 
     $("#btnActualizar_plan_mi").hide();
     $("#btnagregar_plan_mi").show();  
     $(".observacion").val(""); 
  });


 /**********ESTE METODO AGREGA LOS DATOS DEL PLAN***** */
  $(document).on('click','#btnagregar_plan_mi', function(e)
  {

      var plan=$('#obs_plan_mi').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (plan.trim()==='')
     {
          alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
      else
     { 
      var data=
       {
          n_historial:n_historial,
          plan:plan,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_plan';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               //alert(data);
               if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     }
   });



   $('#lista_plan_mi').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_plan_mi").modal("show"); 
        $("#btnagregar_plan_mi").hide()
        $("#btnActualizar_plan_mi").show() 
        $('#obs_plan_mi').val(observacion);
        $('#id').val(id);

   });
   

   $(document).on('click','#btnActualizar_plan_mi', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var plan=$('#obs_plan_mi').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_plan';
        if (plan.trim()==='')
        {
             alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
         else
        {     
          var data=
          {
               id :id ,
               plan  :plan,
               today:today,        
          }
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
        }
  });

  $(document).on('click','#btnMostrar_medicamentos_mi', function(e)
  {
     $("#modal_medicamentos_mi").modal("show"); 
     $("#btnActualizar_medicamentos_mi").hide();
     $("#btnagregar_medicamentos_mi").show();   
     $(".observacion").val("");
  });
 
 
 /**********ESTE METODO AGREGA LOS DATOS DE MEDICAMENTO MEDICINA INTERNA***** */
  $(document).on('click','#btnagregar_medicamentos_mi', function(e)
  {
 
      var medicamentos=$('#obs_medicamentos_mi').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (medicamentos.trim()==='')
     {
          alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
      else
     { 
      var data=
       {
          n_historial:n_historial,
          medicamentos:medicamentos,
          id_consulta:id_consulta,
          id_especialidad:id_especialidad,
       }
   
       var url='/agregar_medicamentos';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               //alert(data);
               if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     }
   });
 
 
 
   $('#lista_medicamentos_mi').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_medicamentos_mi").modal("show"); 
        $("#btnagregar_medicamentos_mi").hide()
        $("#btnActualizar_medicamentos_mi").show() 
        $('#obs_medicamentos_mi').val(observacion);
        $('#id').val(id);
 
 
   });
   
 
   $(document).on('click','#btnActualizar_medicamentos_mi', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var medicamentos=$('#obs_medicamentos_mi').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_medicamentos';
        if (medicamentos.trim()==='')
        {
             alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
         else
        {     
          var data=
          {
               id :id ,
               medicamentos  :medicamentos,
               today:today,        
          }
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
        }
  });
 
 
 

// *******************************************************PACIENTE PEDIATRICO*****************************************************************



$(document).on('click','#btnAgregar_signos_pp', function(e)
{
   $(".signosAC_pp").show(); 
   $(".enfermedadactual_pp").hide(); 
   $(".habitos_pp").hide();
   $(".medicamentos_pp").hide();
   $(".antecedentesquirurgicos_pp").hide();
   $(".antecedentespatologicospersonales_pp").hide();
   $(".examenfisico_pp").hide();
   $(".paraclinicos_pp").hide();
   $(".plan_pp").hide();
   $(".impresiondiagnostica_pp").hide();
   $('.texto_pp').hide();
});




$(document).on('click','#btnActualizar_signos_pp', function(e)
{
     e.preventDefault();

     var cedula      =$('#cedulab').val(); 
     let motivo_consulta    =$('#motivo_consulta_pp').val(); 
     var id_especialidad=$('#id_especialidad').val();
     var id_historial_medico=$('#id_historial_medico').val()
     var n_historial=$('#numeroHistorial').val();
     var id_medico=$('#medico_id').val();
     var peso=$('#peso_pp').val();
     var talla=$('#talla_pp').val();
     var spo2=$('#spo2_pp').val();
     var frecuencia_c=$('#frecuencia_c_pp').val();
     var frecuencia_r=$('#frecuencia_r_pp').val();
     var temperatura=$('#temperatura_pp').val();
     var ta_alta=$('#ta_alta_pp').val();
     var ta_baja=$('#ta_baja_pp').val(); 
     var fecha_asistencia_normal=$('#fecha_del_dia_pp').val();
     let fechaconvertida=moment(fecha_asistencia_normal,'DD-MM-YYYY'); 
     fecha_asistencia=fechaconvertida.format('YYYY-MM-DD');
     var id_consulta=$('#id_consulta').val();
     var imc=$('#imc_pp').val();
     let user_id=$('#id_user').val();
     let tipo_beneficiario='C';
     var url='/actualizar_signos_vitales_citas';

     if (peso=='') 
     {
          peso=0; 
     }
     if (talla=='') 
     {
          talla=0; 
          }
     if (spo2=='') 
     {
          spo2=0; 
     }if (frecuencia_c=='') 
     {
          frecuencia_c=0; 
     } if (frecuencia_r=='')
     {
          frecuencia_r=0; 
     }if (temperatura=='') 
     {
          temperatura=0; 
     } if (ta_alta=='') 
     {
          ta_alta=0; 
     } if (ta_baja=='')
     {
          ta_baja=0; 
     }if (imc=='')
     {
          imc=0; 
     }

     var data=
     {
     fecha_asistencia:fecha_asistencia,
     n_historial   :n_historial,
     id_medico     :id_medico,
     peso          :peso,               
     talla         :talla,
     spo2          :spo2,
     frecuencia_c  :frecuencia_c,
     frecuencia_r  :frecuencia_r,
     temperatura   :temperatura,
     ta_alta       :ta_alta,
     ta_baja       :ta_baja,
     imc:imc,
     tipo_beneficiario:tipo_beneficiario,
     motivo_consulta:motivo_consulta, 
     user_id:user_id,
     id_consulta:id_consulta,
     }

     //      //console.log(data);
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
               
               if(data=='1')
               {
                    alert('SIGNOS VITALES ACTUALIZADOS ');
               
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                   
               }
               else if(data=='0')
               {
                    alert('Error en la Incorporación del Registro');
               }

               else if(data=='2')
               {
                    alert('La cedula posee Historial');
               }


          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
     });
     
});


// ************ ACCIONES PACIENTE PEDIATRICO*********

$('#aciones_pp').on('change',function(e)
{ 
    descripcioncompleta=$('#aciones_pp option:selected').val(); 
//alert(descripcioncompleta);
switch(descripcioncompleta) {
 case '1':
      $(".signosAC_pp").hide(); 
    $(".enfermedadactual_pp").show(); 
    $("#table_enfermedad_actual_pp").dataTable().fnDestroy();
    listar_enfermedad_actual_pp();
    $(".medicamentos_pp").hide();
    $(".antecedentespatologicosfamiliares_pp").hide();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").hide();
    $(".examenfisico_pp").hide();
    $(".paraclinicos_pp").hide();
    $(".plan_pp").hide();
    $(".impresiondiagnostica_pp").hide();
    $('.texto_pp').show();
   
    break;
 case '2':
      $(".signosAC_pp").hide(); 
    $(".enfermedadactual_pp").hide(); 
    $(".medicamentos_pp").show();
     $("#table_medicamentos_pp").dataTable().fnDestroy();
     listar_medicamentos_pp();
    $(".antecedentespatologicosfamiliares_pp").hide();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").hide();
    $(".examenfisico_pp").hide();
    $(".paraclinicos_pp").hide();
    $(".plan_pp").hide();
    $(".impresiondiagnostica_pp").hide();
    $('.texto_pp').show();
  
    break;
  case '3':
      $(".signosAC_pp").hide(); 
    $("#table_examen_fisico_pp").dataTable().fnDestroy();
    listar_examen_fisico_pp()
    $(".enfermedadactual_pp").hide(); 
    $(".medicamentos_pp").hide();
    $(".antecedentespatologicosfamiliares_pp").hide();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").hide();
    $(".examenfisico_pp").show();
    $(".paraclinicos_pp").hide();
    $(".plan_pp").hide();
    $(".impresiondiagnostica_pp").hide(); 
    $('.texto_pp').show();
  
  break;
  case '4':
      $(".signosAC_pp").hide(); 
    $(".enfermedadactual_pp").hide(); 
    $(".medicamentos_pp").hide();
    $(".antecedentespatologicosfamiliares_pp").show();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").hide();
    $(".examenfisico_pp").hide();
    $(".paraclinicos_pp").hide();
    $(".plan_pp").hide();
    $(".impresiondiagnostica_pp").hide();
    $('.texto_pp').show();
    $("#antecedentes_pf_pp").dataTable().fnDestroy();
    antecedentespatologicosfamiliares_pp();   

  break;
  case '5':
      $(".signosAC_pp").hide(); 
    $(".enfermedadactual_pp").hide(); 
    $(".medicamentos_pp").hide();
    $(".antecedentespatologicosfamiliares_pp").hide();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").show();
    $("#antecedentes_pp_pp").dataTable().fnDestroy();
    antecedentespatologicospersonales_pp()
    $(".examenfisico_pp").hide();
    $(".paraclinicos_pp").hide();
    $(".plan_pp").hide();
    $(".impresiondiagnostica_pp").hide();
    $('.texto_pp').show();
  
  break  
  case '6':
      $(".signosAC_pp").hide(); 
    $(".enfermedadactual_pp").hide(); 
    $(".medicamentos_pp").hide();
    $(".antecedentespatologicosfamiliares_pp").hide();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").hide();
    $(".examenfisico_pp").hide();
    $(".paraclinicos_pp").hide();
    $(".plan_pp").hide();
    $(".impresiondiagnostica_pp").hide();
    $('.texto_pp').show();
   
  break  
  case '7':
      $(".signosAC_pp").hide(); 
    $(".enfermedadactual_pp").hide(); 
    $(".medicamentos_pp").hide();
    $(".antecedentespatologicosfamiliares_pp").hide();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").hide();
    $(".examenfisico_pp").hide();
    $(".paraclinicos_pp").show();
    $(".plan_pp").hide();
    $(".impresiondiagnostica_pp").hide();
    $('.texto_pp').show();
    $("#btnparaclinicos").show();
    $("#tableparaclinicos_pp").dataTable().fnDestroy();
    listar_paraclinicos_pp();
  
  break  
  case '8':
      $(".signosAC_pp").hide(); 
    $(".enfermedadactual_pp").hide(); 
    $(".medicamentos_pp").hide();
    $(".antecedentespatologicosfamiliares_pp").hide();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").hide();
    $(".examenfisico_pp").hide();
    $(".paraclinicos_pp").hide();
    $(".plan_pp").hide();
    $(".impresiondiagnostica_pp").show();
    $("#impresion_diag_pp").dataTable().fnDestroy();
    listar_impresion_diag_pp();
    $('.texto_pp').show();
   
  break  
  case '9':
      $(".signosAC_pp").hide(); 
    $(".enfermedadactual_pp").hide(); 
    $(".medicamentos_pp").hide();
    $(".antecedentespatologicosfamiliares_pp").hide();
    $(".antecedentesquirurgicos_pp").hide();
    $(".antecedentespatologicospersonales_pp").hide();
    $(".examenfisico_pp").hide();
    $(".paraclinicos_pp").hide();
    $(".plan_pp").show();
    $("#table_plan_pp").dataTable().fnDestroy();
    listar_plan_pp();
    $(".impresiondiagnostica_pp").hide();
    $('.texto_pp').show();

  break  

  case '12':

  
  e.preventDefault('');
  let nombre =$("#nombrepellidoB").val();
  let fecha_nacimiento =$("#fecha_nacimiento").val();
  let cedulab =$("#cedulab").val();
  let edad =$("#edad").val(); 
  let telefono =$("#telefono").val();
  let numeroHistorial =$("#numeroHistorial").val();
  $("#modal_reposos").modal("show");  
  $('#modal_reposos').find('#nombretitular').val(nombre); 
  $('#modal_reposos').find('#fecha_nacimientotitular').val(fecha_nacimiento); 
  $('#modal_reposos').find('#cedulatitular').val(cedulab);
  $('#modal_reposos').find('#edadtitular').val(edad); 
  $('#modal_reposos').find('#telefonotitular').val(telefono);
  $('#modal_reposos').find('#unidadtitular').val('CORTESIA');
  $('#modal_reposos').find('#n_historialT').val(numeroHistorial);
  
  break 
}



});


$(document).on('click','#btnMostrar_enfermedad_actual_pp', function(e)
{

  $("#modal_enfermedad_pp").modal("show"); 
  $("#btnActualizar_enfermedadactual_pp").hide();+
  $("#btnagregar_enfermedadactual_pp").show();
  $(".observacion").val("");
   
});


 /**********ESTE METODO AGREGA LOS DATOS ENFERMEDAD ACTUAL PACIENTE PEDIATRICO***** */
$(document).on('click','#btnagregar_enfermedadactual_pp', function(e)
{
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val()
   var n_historial=$('#numeroHistorial').val();
   var id_consulta=$('#id_consulta').val();
   var enfermedad_actual=$('#enfermedadactual_pp').val();
   if (enfermedad_actual.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
  else
  {
     var data=
     {
        n_historial:n_historial,
        enfermedad_actual:enfermedad_actual,
        id_consulta:id_consulta,
     }
 
     var url='/agregar_enfermedad_actual';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
            //alert(data);
            if(data===1)
            {
               alert('Registro Exitoso');
               window.location = '/medico_Consultas/'
            }
            else if(data===2)
            {
               alert('YA EXISTE UN REGISTRO PARA ESTA CONSULTA');
              
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
       
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
    }
 });


 $('#table_enfermedad_actual_pp').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_enfermedad_pp").modal("show"); 
      $("#btnagregar_enfermedadactual_pp").hide()
      $("#btnActualizar_enfermedadactual_pp").show() 
      $('#enfermedadactual_pp').val(observacion);
      $('#id').val(id);

 });




 $(document).on('click','#btnActualizar_enfermedadactual_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var observacion  =$('#enfermedadactual_pp').val();
      var enfermedad_actual=$('#enfermedadactual_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;

      var url='/actualizar_enfermedad_actual';
      if (observacion.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
        var data=
        {
             id :id ,
             observacion  :observacion,  
             today:today,      
        }
        
        
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
             },
             success:function(data)
             {
             
                  if(data=='1')
                  {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                       alert('Error en la Incorporación del Registro');
                  }
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
   }    
   
});


$(document).on('click','#btnMostrar_antecedentes_pp_pp', function(e)
{
   $("#antecedentes_personales_pp").modal("show"); 
   $("#btnActualizar_antecedentes_personales_pp").hide();
   $("#btnagregar_antecedentes_personales_pp").show();  
   $(".observacion").val(""); 
});

/**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES PATOLOGICOS PERSONALES PACIENTE PEDIATRICO***** */
$(document).on('click','#btnagregar_antecedentes_personales_pp', function(e)
{
    var antecedentes_patologicosp=$('#antecedentes_patologicosp_pp').val();
    var cedula      =$('#cedulab').val();
    var tipobeneficiario  ='C';
    var n_historial=$('#numeroHistorial').val();
    var id_especialidad=$('#id_especialidad').val();
    var fecha_asistencia=$('#fecha_creacion').val();
    var id_historial_medico=$('#id_historial_medico').val();
    var id_consulta=$('#id_consulta').val();
    
    if (antecedentes_patologicosp.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
        var data=
        {
             n_historial:n_historial,
             antecedentes_patologicosp:antecedentes_patologicosp,
             id_consulta:id_consulta,
        }
   
        var url='/agregar_antecedentes_patologicosp';
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
                  //alert('Procesando Información ...');
             },
             success:function(data)
             {
                  //alert(data);
                  if(data===1)
                  {
                  alert('Registro Exitoso');
                  window.location = '/medico_Consultas/'
                  }
                  else if(data===2)
                  {
                  alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                  
                  }
                  else
                  {
                  alert('Error en la Incorporación del registro');
                  }
                  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
             
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
   } 
 });



 $('#lista_antecedentes_pp_pp').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
       $("#antecedentes_personales_pp").modal("show");
      $("#btnagregar_antecedentes_personales_pp").hide()
      $("#btnActualizar_antecedentes_personales_pp").show() 
      $('#antecedentes_patologicosp_pp').val(observacion);
      $('#id').val(id);

 });

 $(document).on('click','#btnActualizar_antecedentes_personales_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var antecedentes_patologicosp=$('#antecedentes_patologicosp_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_antecedentes_patologicosp';
   if (antecedentes_patologicosp.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
     
      var data=
      {
        id :id ,
        antecedentes_patologicosp  :antecedentes_patologicosp,   
        today:today,     
      }
     
     
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Actualizado');
                     window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });
   }
});

/**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES PATOLOGICOS F PACIENTE PEDIATRICO***** */
$(document).on('click','#btnMostrar_antecedentes_pf_pp', function(e)
{
   $("#antecedentes_familiares_pp").modal("show"); 
   $("#btnActualizar_antecedentes_familiares_pp").hide();
   $("#btnagregar_antecedentes_familiares_pp").show();
   $(".observacion").val("");
   
});


$(document).on('click','#btnagregar_antecedentes_familiares_pp', function(e)
{

 var antecedentes_patologicosf=$('#antecedentes_patologicosf_pp').val();
 var cedula      =$('#cedulab').val();
 var tipobeneficiario  ='C';
 var n_historial=$('#numeroHistorial').val();
 var id_especialidad=$('#id_especialidad').val();
 var fecha_asistencia=$('#fecha_creacion').val();
 var id_historial_medico=$('#id_historial_medico').val();
 var id_consulta=$('#id_consulta').val();
 if (antecedentes_patologicosf.trim()==='')
 {
 alert('NO DEBE DEJAR EL CAMPO VACIO');
}
else
{

 var data=
     {
        n_historial:n_historial,
        antecedentes_patologicosf:antecedentes_patologicosf,
        id_consulta:id_consulta,
     }
 
     var url='/agregar_antecedentes_patologicosf';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
                alert('Registro Exitoso');
                window.location = '/medico_Consultas/'
             }
             else if(data===2)
             {
                alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
               
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
   } 
 });


 $('#lista_antecedentes_pf_pp').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#antecedentes_familiares_pp").modal("show"); 
      $("#btnagregar_antecedentes_familiares_pp").hide()
      $("#btnActualizar_antecedentes_familiares_pp").show() 
      $('#antecedentes_patologicosf_pp').val(observacion);
      $('#id').val(id);

 });




 $(document).on('click','#btnActualizar_antecedentes_familiares_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var antecedentes_patologicosf=$('#antecedentes_patologicosf_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_antecedentes_patologicosf';
      if (antecedentes_patologicosf.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
        var data=
        {
             id :id ,
             antecedentes_patologicosf  :antecedentes_patologicosf, 
             today:today,       
        }
        
        
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
             },
             success:function(data)
             {
             
                  if(data=='1')
                  {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                       alert('Error en la Incorporación del Registro');
                  }
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
     }
});



$(document).on('click','#btnMostrar_antecedentes_qx_pp', function(e)
{
   $("#modal_antecedentes_quirurgicos_pp").modal("show"); 
   $("#btnActualizar_antecedentes_quirurgicos_pp").hide();
   $("#btnagregar_antecedentes_quirurgicos_pp").show();
   $(".observacion").val("");
   
});


  /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES QUIRURGICOS PACIENTE PEDIATRICO***** */
$(document).on('click','#btnagregar_antecedentes_quirurgicos_pp', function(e)
{
  
 var antecedentes_quirurgicos=$('#antecedentes_quirurgicos_pp').val();
 var cedula      =$('#cedulab').val();
 var tipobeneficiario  ='C';
 var n_historial=$('#numeroHistorial').val();
 var id_especialidad=$('#id_especialidad').val();
 var fecha_asistencia=$('#fecha_creacion').val();
 var id_historial_medico=$('#id_historial_medico').val();
 var id_consulta=$('#id_consulta').val();
 if (antecedentes_quirurgicos.trim()==='')
 {
 alert('NO DEBE DEJAR EL CAMPO VACIO');
}
else
{
   var data=
     {
        n_historial:n_historial,
        antecedentes_quirurgicos:antecedentes_quirurgicos,
        id_consulta:id_consulta,
     }
 
     var url='/agregar_antecedentes_quirurgicos';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             if(data===1)
             {
                alert('Registro Exitoso');
                window.location = '/medico_Consultas/'
             }
             else if(data===2)
             {
                alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
               
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
   }  
 });

 $('#lista_antecedentes_qx').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_antecedentes_quirurgicos_pp").modal("show"); 
      $("#btnagregar_antecedentes_quirurgicos_pp").hide()
      $("#btnActualizar_antecedentes_quirurgicos_pp").show() 
      $('#antecedentes_quirurgicos_pp').val(observacion);
      $('#id').val(id);

 });


 $(document).on('click','#btnActualizar_antecedentes_quirurgicos_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var antecedentes_quirurgicos=$('#antecedentes_quirurgicos_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_antecedentes_quirurgicos';
      if (antecedentes_quirurgicos.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
        var data=
        {
             id :id ,
             antecedentes_quirurgicos  :antecedentes_quirurgicos,  
             today:today,      
        }
        
        
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
             },
             success:function(data)
             {
             
                  if(data=='1')
                  {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                       alert('Error en la Incorporación del Registro');
                  }
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
   }
});

 /**********ESTE METODO AGREGA LOS DATOS DEl EXAMEN FISICO PACIENTE PEDIATRICO***** */
$(document).on('click','#btnMostrar_examen_fisico_pp', function(e)
{
   $("#modalexamen_fisico_pp").modal("show"); 
   $("#btnActualizar_examen_fisico_pp").hide();
   $("#btnagregar_examen_fisico_pp").show();   
   $(".observacion").val("");
});

 
$(document).on('click','#btnagregar_examen_fisico_pp', function(e)
{
    var examen_fisico=$('#obs_examen_fisico_pp').val();
    var cedula      =$('#cedulab').val();
    var tipobeneficiario  ='C';
    var n_historial=$('#numeroHistorial').val();
    var id_especialidad=$('#id_especialidad').val();
    var fecha_asistencia=$('#fecha_creacion').val();
    var id_historial_medico=$('#id_historial_medico').val();
    var id_consulta=$('#id_consulta').val();
    if (examen_fisico.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {  
        var data=
        {
             n_historial:n_historial,
             examen_fisico:examen_fisico,
             id_consulta:id_consulta
        }
   
        var url='/agregar_examen_fisico';
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
                  //alert('Procesando Información ...');
             },
             success:function(data)
             {
                  if(data===1)
                  {
                  alert('Registro Exitoso');
                  window.location = '/medico_Consultas/'
                  }
                  else if(data===2)
                  {
                  alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                  
                  }
                  else
                  {
                  alert('Error en la Incorporación del registro');
                  }
                  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
             
             
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
   } 
 });



 $('#lista_examen_fisico_pp').on('click','.Editar', function(e)
 { 
      e.preventDefault();
      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modalexamen_fisico_pp").modal("show"); 
      $("#btnagregar_examen_fisico_pp").hide()
      $("#btnActualizar_examen_fisico_pp").show() 
      $('#obs_examen_fisico_pp').val(observacion);
      $('#id').val(id);

 });



 $(document).on('click','#btnActualizar_examen_fisico_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var examen_fisico=$('#obs_examen_fisico_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_examen_fisico';
     
   if (examen_fisico.trim()==='')
   {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
      var data=
      {
        id :id ,
        examen_fisico  :examen_fisico,    
        today:today,    
      }
      
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Actualizado');
                     window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });
   }
});


$(document).on('click','#btnMostrar_paraclinicos_pp', function(e)
{
   $("#modal_paraclinicos_pp").modal("show"); 
   $("#btnActualizar_paraclinicos_pp").hide();
   $("#btnagregar_paraclinicos_pp").show();  
   $(".observacion").val(""); 
});



/**********ESTE METODO AGREGA LOS DATOS DE PARACLINICOS PACIENTE PEDIATRICO***** */
$(document).on('click','#btnagregar_paraclinicos_pp', function(e)
{
  
    var paraclinicos=$('#obs_paraclinicos_pp').val();
    var cedula      =$('#cedulab').val();
    var tipobeneficiario  ='C';
    var n_historial=$('#numeroHistorial').val();
    var id_especialidad=$('#id_especialidad').val();
    var fecha_asistencia=$('#fecha_creacion').val();
    var id_historial_medico=$('#id_historial_medico').val();
    var id_consulta=$('#id_consulta').val();
    if (paraclinicos.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
        var data=
        {
             n_historial:n_historial,
             paraclinicos:paraclinicos,
             id_consulta:id_consulta,
        }
   
        var url='/agregar_paraclinicos';
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
                  //alert('Procesando Información ...');
             },
             success:function(data)
             {
                  //alert(data);
                  if(data===1)
                  {
                  alert('Registro Exitoso');
                  window.location = '/medico_Consultas/'
                  }
                  else if(data===2)
                  {
                  alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                  
                  }
                  else
                  {
                  alert('Error en la Incorporación del registro');
                  }
                  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
             
             
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
   } 
 });



 $('#lista_paraclinicos_pp').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_paraclinicos_pp").modal("show"); 
      $("#btnagregar_paraclinicos_pp").hide()
      $("#btnActualizar_paraclinicos_pp").show() 
      $('#obs_paraclinicos_pp').val(observacion);
      $('#id').val(id);

 });



 $(document).on('click','#btnActualizar_paraclinicos_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var paraclinicos=$('#obs_paraclinicos_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_paraclinicos';
      if (paraclinicos.trim()==='')
    {
    alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
     
      var data=
      {
        id :id ,
        paraclinicos  :paraclinicos,
        today:today,        
      }
      
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Actualizado');
                     window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });
   }
});




$(document).on('click','#btnMostrar_impresion_diag_pp', function(e)
{
   $("#modal_impresion_diag_pp").modal("show"); 
   $("#btnActualizar_impresion_diag_pp").hide();
   $("#btnagregar_impresion_diag_pp").show(); 
   $(".observacion").val("");  
});


  /**********ESTE METODO AGREGA LOS DATOS DEL IMPRESION DIAGNOSTICA PACIENTE PEDIATRICO***** */
$(document).on('click','#btnagregar_impresion_diag_pp', function(e)
{
 
 var impresiondiagnostica=$('#obs_impresion_diag_pp').val();
 var cedula      =$('#cedulab').val();
 var tipobeneficiario  ='C';
 var n_historial=$('#numeroHistorial').val();
 var id_especialidad=$('#id_especialidad').val();
 var fecha_asistencia=$('#fecha_creacion').val();
 var id_historial_medico=$('#id_historial_medico').val();
 var id_consulta=$('#id_consulta').val();
 if (impresiondiagnostica.trim()==='')
 {
 alert('NO DEBE DEJAR EL CAMPO VACIO');
}
else
{
    var data=
     {
        n_historial:n_historial,
        impresiondiagnostica:impresiondiagnostica,
        id_consulta:id_consulta,
     }
 
     var url='/agregar_impresiondiagnostica';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
            //alert(data);
            if(data===1)
            {
               alert('Registro Exitoso');
               window.location = '/medico_Consultas/'
            }
            else if(data===2)
            {
               alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
              
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
       
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
   } 
 });


 $('#lista_impresion_diag_pp').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_impresion_diag_pp").modal("show"); 
      $("#btnagregar_impresion_diag_pp").hide()
      $("#btnActualizar_impresion_diag_pp").show() 
      $('#obs_impresion_diag_pp').val(observacion);
      $('#id').val(id);

 });


 $(document).on('click','#btnActualizar_impresion_diag_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var impresiondiagnostica=$('#obs_impresion_diag_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_impresiondiagnostica';
      if (impresiondiagnostica.trim()==='')
   {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
   else
   {
      var data=
      {
        id :id ,
        impresiondiagnostica  :impresiondiagnostica,
        today:today,        
      }
      
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Actualizado');
                     window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });
   }
});

$(document).on('click','#btnMostrar_plan_pp', function(e)
{
   $("#modal_plan_pp").modal("show"); 
   $("#btnActualizar_plan_pp").hide();
   $("#btnagregar_plan_pp").show();  
   $(".observacion").val(""); 
});


/**********ESTE METODO AGREGA LOS DATOS DEL PLAN PACIENTE PEDIATRICO***** */
$(document).on('click','#btnagregar_plan_pp', function(e)
{

    var plan=$('#obs_plan_pp').val();
    var cedula      =$('#cedulab').val();
    var tipobeneficiario  ='C';
    var n_historial=$('#numeroHistorial').val();
    var id_especialidad=$('#id_especialidad').val();
    var fecha_asistencia=$('#fecha_creacion').val();
    var id_historial_medico=$('#id_historial_medico').val();
    var id_consulta=$('#id_consulta').val();
    if (plan.trim()==='')
   {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
    else
   { 
    var data=
     {
        n_historial:n_historial,
        plan:plan,
        id_consulta:id_consulta,
     }
 
     var url='/agregar_plan';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
            {
               alert('Registro Exitoso');
               window.location = '/medico_Consultas/'
            }
            else if(data===2)
            {
               alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
              
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
   }
 });



 $('#lista_plan_pp').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_plan_pp").modal("show"); 
      $("#btnagregar_plan_pp").hide()
      $("#btnActualizar_plan_pp").show() 
      $('#obs_plan_pp').val(observacion);
      $('#id').val(id);

 });
 

 $(document).on('click','#btnActualizar_plan_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var plan=$('#obs_plan_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_plan';
      if (plan.trim()==='')
      {
           alert('NO DEBE DEJAR EL CAMPO VACIO');
      }
       else
      {     
        var data=
        {
             id :id ,
             plan  :plan,
             today:today,        
        }
        
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
             },
             success:function(data)
             {
             
                  if(data=='1')
                  {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                       alert('Error en la Incorporación del Registro');
                  }
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
      }
});





$(document).on('click','#btnMostrar_medicamentos_pp', function(e)
{
   $("#modal_medicamentos_pp").modal("show"); 
   $("#btnActualizar_medicamentos_pp").hide();
   $("#btnagregar_medicamentos_pp").show();  
   $(".observacion").val(""); 
});


/**********ESTE METODO AGREGA LOS DATOS DE MEDICAMENTO PACIENTE PEDIATRICO***** */
$(document).on('click','#btnagregar_medicamentos_pp', function(e)
{
    var medicamentos=$('#obs_medicamentos_pp').val();
    var cedula      =$('#cedulab').val();
    var tipobeneficiario  ='C';
    var n_historial=$('#numeroHistorial').val();
    var id_especialidad=$('#id_especialidad').val();
    var fecha_asistencia=$('#fecha_creacion').val();
    var id_historial_medico=$('#id_historial_medico').val();
    var id_consulta=$('#id_consulta').val();
    if (medicamentos.trim()==='')
   {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
   }
    else
   { 
    var data=
     {
          n_historial:n_historial,
          medicamentos:medicamentos,
          id_consulta:id_consulta,
          id_especialidad:id_especialidad,
     }
 
     var url='/agregar_medicamentos';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
            {
               alert('Registro Exitoso');
               window.location = '/medico_Consultas/'
            }
            else if(data===2)
            {
               alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
              
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
   }
 });



 $('#lista_medicamentos_pp').on('click','.Editar', function(e)
 { 
      e.preventDefault();

      let observacion=$(this).attr('observacion')
      let id=$(this).attr('id');
      $("#modal_medicamentos_pp").modal("show"); 
      $("#btnagregar_medicamentos_pp").hide()
      $("#btnActualizar_medicamentos_pp").show() 
      $('#obs_medicamentos_pp').val(observacion);
      $('#id').val(id);


 });
 

 $(document).on('click','#btnActualizar_medicamentos_pp', function(e)
 {
      e.preventDefault();
      var id          =$('#id').val();
      var medicamentos=$('#obs_medicamentos_pp').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      var now = new Date();
      var day = ("0" + now.getDate()).slice(-2);
      var month = ("0" + (now.getMonth() + 1)).slice(-2);
      var today= now.getFullYear()+"/"+month+"/"+day;
      var url='/actualizar_medicamentos';
      if (medicamentos.trim()==='')
      {
           alert('NO DEBE DEJAR EL CAMPO VACIO');
      }
       else
      {     
        var data=
        {
             id :id ,
             medicamentos  :medicamentos,
             today:today,        
        }
        
        $.ajax
        ({
             url:url,
             method:'POST',
             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
             dataType:'JSON',
             beforeSend:function(data)
             {
             },
             success:function(data)
             {
             
                  if(data=='1')
                  {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                  }
                  else
                  {
                       alert('Error en la Incorporación del Registro');
                  }
             },
             error:function(xhr, status, errorThrown)
             {
                  alert(xhr.status);
                  alert(errorThrown);
             }
        });
      }
});

 




// *****************************************************************GINECOLOGIA Y OBSTRTRICIA*****************************************************************

$(document).on('click','#btnAgregar_signos_gb', function(e)
{
     var cedula      =$('#cedulab').val(); 
     let motivo_consulta =$('#motivo_consulta_GB').val();
     var id_especialidad=$('#id_especialidad').val();
     var id_historial_medico=$('#id_historial_medico').val()
     var n_historial=$('#numeroHistorial').val();
     var id_medico=$('#medico_id').val();
     var peso=$('#peso_gb').val();
     var talla=$('#talla_gb').val();
     var spo2=$('#spo2_gb').val();
     var frecuencia_c=$('#frecuencia_c_gb').val();
     var frecuencia_r=$('#frecuencia_r_gb').val();
     var temperatura=$('#temperatura_gb').val();
     var ta_alta=$('#ta_alta_gb').val();
     var ta_baja=$('#ta_baja_gb').val(); 
     var fecha_asistencia_normal=$('#fecha_del_dia_mi').val();
     let fechaconvertida=moment(fecha_asistencia_normal,'DD-MM-YYYY'); 
     fecha_asistencia=fechaconvertida.format('YYYY-MM-DD');
     var id_consulta=$('#id_consulta').val();
     var imc=$('#imc_gb').val();
     let user_id=$('#id_user').val();
     let tipo_beneficiario='C';
     var url='/actualizar_signos_vitales_citas';
     if (peso=='') 
     {
          peso=0; 
     }
     if (talla=='') 
     {
          talla=0; 
          }
     if (spo2=='') 
     {
          spo2=0; 
     }if (frecuencia_c=='') 
     {
          frecuencia_c=0; 
     } if (frecuencia_r=='')
     {
          frecuencia_r=0; 
     }if (temperatura=='') 
     {
          temperatura=0; 
     } if (ta_alta=='') 
     {
          ta_alta=0; 
     } if (ta_baja=='')
     {
          ta_baja=0; 
     }if (imc=='')
     {
          imc=0; 
     }

     var data=
     {
     fecha_asistencia:fecha_asistencia,
     n_historial   :n_historial,
     id_medico     :id_medico,
     peso          :peso,               
     talla         :talla,
     spo2          :spo2,
     frecuencia_c  :frecuencia_c,
     frecuencia_r  :frecuencia_r,
     temperatura   :temperatura,
     ta_alta       :ta_alta,
     ta_baja       :ta_baja,
     imc:imc,
     tipo_beneficiario:tipo_beneficiario,
     motivo_consulta:motivo_consulta, 
     user_id:user_id,
     id_consulta:id_consulta,
     }

     //      //console.log(data);
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
               
               if(data=='1')
               {
                    alert('SIGNOS VITALES ACTUALIZADOS ');
               
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                   
               }
               else if(data=='0')
               {
                    alert('Error en la Incorporación del Registro');
               }

               else if(data=='2')
               {
                    alert('La cedula posee Historial');
               }


          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
     });
});


$('#aciones_gb').on('change',function(e)
 { 
     descripcioncompleta=$('#aciones_gb option:selected').val(); 
//alert(descripcioncompleta);
switch(descripcioncompleta) {
  case '1':
     $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $(".enfermedadactual_gb").show(); 
     $("#table_enfermedad_actual_gb").dataTable().fnDestroy();
     listar_enfermedad_actual_gb();
     $(".medicamentos_gb").hide();
     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").hide();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").hide();
     $('.texto_gb').show();
     $(".habitos_gb").hide();
     break;
  case '2':

     $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").hide();
     $(".antecedentespatologicosfamiliares_gb").show();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").hide();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").hide();
     $('.texto_gb').show();
     $("#antecedentes_pf_gb").dataTable().fnDestroy();
     antecedentespatologicosfamiliares_gb(); 
     $(".habitos_gb").hide();
     break;
   case '3':
   $(".form_ante_gineco_obs").hide();
   $(".enfermedadactual_gb").hide(); 
   $(".medicamentos_gb").hide();
   $(".antecedentespatologicosfamiliares_gb").hide();
   $(".antecedentesquirurgicos_gb").hide();
   $(".antecedentespatologicospersonales_gb").show();
   $("#antecedentes_pp_gb").dataTable().fnDestroy();
   antecedentespatologicospersonales_gb()
   $(".examenfisico_gb").hide();
   $(".paraclinicos_gb").hide();
   $(".plan_gb").hide();
   $(".impresiondiagnostica_gb").hide();
   $('.texto_gb').show();
   $(".habitos_gb").hide();
   break;
   case '4':

    $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").hide();
     $(".antecedentesquirurgicos_gb").show();
     $("#antecedentes_qx_gb").dataTable().fnDestroy();
     antecedentesquirurgicos_gb();
     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").hide();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").hide();
     $('.texto_gb').show(); 
     $(".habitos_gb").hide();

   break;
   case '5':
     $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $("#habitos_psicosociales_gb").dataTable().fnDestroy();
     listar_habitos_psicosociales_gb();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").hide();

     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".habitos_gb").show();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").hide();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").hide(); 
     $('.texto_gb').show();
   
   break  
   case '6':
     $("#modal_prueba").modal("show"); 
     let bandera_de_llenado=$('#bandera_de_llenado').val();
     bandera_de_llenado.trim()===''
  
     if (bandera_de_llenado=='true')
     {
       
           $('#btnActualizar_Antecedentes_gineco_obstetricos').css('display', 'block'); 
     }else
     {
          $('#btnagregar_Antecedentes_gineco_obstetricos').css('display', 'block'); 
          $("#tipo_ets").attr('disabled','disabled');
          $("#tipo_anticoceptivo").attr('disabled','disabled');
        
     }
     $(".form_ante_gineco_obs").show(); 
     $(".form_ante_gineco_obs2").show();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").hide();
     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").hide();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").hide();
     $('.texto_gb').show();
    
   break  
   case '7':
  
     $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").show();
     $("#table_medicamentos_gb").dataTable().fnDestroy();
     listar_medicamentos_gb();
     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").hide();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").hide();
     $('.texto_gb').show(); 
   $(".habitos_gb").hide();
    
   break  
   case '8':
     $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").hide();
     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").show();
     $("#table_examen_fisico_gb").dataTable().fnDestroy();
     listar_examen_fisico_gb();
     $(".paraclinicos_gb").hide();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").hide();
     $('.texto_gb').show();
     $("#btnplan").show();
    
   break  
   case '9':
     $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").hide();
     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").hide();
     $(".plan_gb").show();
     $("#table_plan_gb").dataTable().fnDestroy();
     listar_plan_gb();
     $(".impresiondiagnostica_gb").hide();
     $('.texto_gb').show();
     $("#btnplan").show();
   break  

   case '10':
     $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").hide();
     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").hide();
     $("#impresion_diag_gb").dataTable().fnDestroy();
     listar_impresion_diag_gb();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").show();
     $('.texto_gb').show();
     $("#btnplan").show();  
   break  


     case '11':

     $(".form_ante_gineco_obs").hide();
     $(".form_ante_gineco_obs2").hide();
     $(".enfermedadactual_gb").hide(); 
     $(".medicamentos_gb").hide();
     $(".antecedentespatologicosfamiliares_gb").hide();
     $(".antecedentesquirurgicos_gb").hide();
     $(".antecedentespatologicospersonales_gb").hide();
     $(".examenfisico_gb").hide();
     $(".paraclinicos_gb").show();
     $("#tableparaclinicos_gb").dataTable().fnDestroy();
     listar_paraclinicos_gb();
     $(".plan_gb").hide();
     $(".impresiondiagnostica_gb").hide();
     $('.texto_gb').show();
     $("#btnplan").show(); 
    
   break  

   case '12':

   
   e.preventDefault('');
   let nombre =$("#nombrepellidoB").val();
   let fecha_nacimiento =$("#fecha_nacimiento").val();
   let cedulab =$("#cedulab").val();
   let edad =$("#edad").val(); 
   let telefono =$("#telefono").val();
   let numeroHistorial =$("#numeroHistorial").val();
   $("#modal_reposos_gb").modal("show");  
   $('#modal_reposos_gb').find('#r_nombretitular').val(nombre); 
   $('#modal_reposos_gb').find('#r_fecha_nacimientotitular').val(fecha_nacimiento); 
   $('#modal_reposos_gb').find('#r_cedulatitular').val(cedulab);
   $('#modal_reposos_gb').find('#r_edadtitular').val(edad); 
   $('#modal_reposos_gb').find('#r_telefonotitular').val(telefono);
   $('#modal_reposos_gb').find('#r_unidadtitular').val('FAMILIAR');
   $('#modal_reposos_gb').find('#r_n_historialT').val(numeroHistorial);
   $('#modal_reposos_gb').find('#btnActualizar_reposos_gb').hide();
   break 
}



 });







 $(document).on('click','#btnMostrar_enfermedad_actual_gb', function(e)
 {
 
    $("#modal_enfermedad_gb").modal("show"); 
    $("#btnActualizar_enfermedadactual_gb").hide();+
    $("#btnagregar_enfermedadactual_gb").show();
    $(".observacion").val("");
     
 });
 
 
   /**********ESTE METODO AGREGA LOS DATOS ENFERMEDAD ACTUAL GINECOLGIA Y OBSTERTRICIA**** */
  $(document).on('click','#btnagregar_enfermedadactual_gb', function(e)
  {
     var cedula      =$('#cedulab').val();
     var tipobeneficiario  ='C';
     var id_especialidad=$('#id_especialidad').val();
     var fecha_asistencia=$('#fecha_creacion').val();
     var id_historial_medico=$('#id_historial_medico').val()
     var n_historial=$('#numeroHistorial').val();
     var id_consulta=$('#id_consulta').val();
     var enfermedad_actual=$('#enfermedadactual_gb').val();
     if (enfermedad_actual.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
    else
    {
       var data=
       {
          n_historial:n_historial,
          enfermedad_actual:enfermedad_actual,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_enfermedad_actual';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               //alert(data);
              //alert(data);
              if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSULTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
         
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
      }
   });
 
 
   $('#table_enfermedad_actual_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_enfermedad_gb").modal("show"); 
        $("#btnagregar_enfermedadactual_gb").hide()
        $("#btnActualizar_enfermedadactual_gb").show() 
        $('#enfermedadactual_gb').val(observacion);
        $('#id').val(id);
 
   });
 
 
   $(document).on('click','#btnActualizar_enfermedadactual_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var observacion  =$('#enfermedadactual_gb').val();
        var enfermedad_actual=$('#enfermedadactual_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
 
        var url='/actualizar_enfermedad_actual';
        if (observacion.trim()==='')
        {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
       }
       else
       {
          var data=
          {
               id :id ,
               observacion  :observacion,  
               today:today,      
          }
          
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     }    
     
  });
 
 
  $(document).on('click','#btnMostrar_antecedentes_gb', function(e)
  {
     $("#antecedentes_personales_gb").modal("show"); 
     $("#btnActualizar_antecedentes_personales_gb").hide();
     $("#btnagregar_antecedentes_personales_gb").show();  
     $(".observacion").val(""); 
  });
 
 /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES PATOLOGICOS PERSONALES GINECOLOGIA Y OBSTERTRICIA***** */
  $(document).on('click','#btnagregar_antecedentes_personales_gb', function(e)
  {
      var antecedentes_patologicosp=$('#antecedentes_patologicosp_gb').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      
      if (antecedentes_patologicosp.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
          var data=
          {
               n_historial:n_historial,
               antecedentes_patologicosp:antecedentes_patologicosp,
               id_consulta:id_consulta,
          }
     
          var url='/agregar_antecedentes_patologicosp';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
                    //alert(data);
                    if(data===1)
                    {
                    alert('Registro Exitoso');
                    window.location = '/medico_Consultas/'
                    }
                    else if(data===2)
                    {
                    alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                    
                    }
                    else
                    {
                    alert('Error en la Incorporación del registro');
                    }
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     } 
   });
 
 
  
   $('#lista_antecedentes_gb_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
         $("#antecedentes_personales_gb").modal("show");
        $("#btnagregar_antecedentes_personales_gb").hide()
        $("#btnActualizar_antecedentes_personales_gb").show() 
        $('#antecedentes_patologicosp_gb').val(observacion);
        $('#id').val(id);
 
   });
 
   $(document).on('click','#btnActualizar_antecedentes_personales_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var antecedentes_patologicosp=$('#antecedentes_patologicosp_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_antecedentes_patologicosp';
     if (antecedentes_patologicosp.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
       
        var data=
        {
          id :id ,
          antecedentes_patologicosp  :antecedentes_patologicosp,   
          today:today,     
        }
       
       
        $.ajax
        ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {
              
                 if(data=='1')
                 {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                 }
                 else
                 {
                      alert('Error en la Incorporación del Registro');
                 }
            },
            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
             }
         });
     }
  });
 
  /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES PATOLOGICOS C GINECOLOGIA Y OBSTERTRICIA***** */
  $(document).on('click','#btnMostrar_antecedentes_pf_gb', function(e)
  {
     $("#antecedentes_familiares_gb").modal("show"); 
     $("#btnActualizar_antecedentes_familiares_gb").hide();
     $("#btnagregar_antecedentes_familiares_gb").show();
     $(".observacion").val("");
     
  });
 
 
  $(document).on('click','#btnagregar_antecedentes_familiares_gb', function(e)
  {
 
   var antecedentes_patologicosf=$('#antecedentes_patologicosf_gb').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (antecedentes_patologicosf.trim()==='')
   {
   alert('NO DEBE DEJAR EL CAMPO VACIO');
  }
  else
  {
  
   var data=
       {
          n_historial:n_historial,
          antecedentes_patologicosf:antecedentes_patologicosf,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_antecedentes_patologicosf';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               //alert(data);
               if(data===1)
               {
                  alert('Registro Exitoso');
                  window.location = '/medico_Consultas/'
               }
               else if(data===2)
               {
                  alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                 
               }
               else
               {
                 alert('Error en la Incorporación del registro');
               }
               window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     } 
   });
 
 
   $('#lista_antecedentes_pf_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#antecedentes_familiares_gb").modal("show"); 
        $("#btnagregar_antecedentes_familiares_gb").hide()
        $("#btnActualizar_antecedentes_familiares_gb").show() 
        $('#antecedentes_patologicosf_gb').val(observacion);
        $('#id').val(id);
 
   });
 
 
 
 
   $(document).on('click','#btnActualizar_antecedentes_familiares_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var antecedentes_patologicosf=$('#antecedentes_patologicosf_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_antecedentes_patologicosf';
        if (antecedentes_patologicosf.trim()==='')
        {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
       }
       else
       {
          var data=
          {
               id :id ,
               antecedentes_patologicosf  :antecedentes_patologicosf, 
               today:today,       
          }
          
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
       }
  });
 
 
 
  $(document).on('click','#btnMostrar_antecedentes_qx_gb', function(e)
  {
     $("#modal_antecedentes_quirurgicos_gb").modal("show"); 
     $("#btnActualizar_antecedentes_quirurgicos_gb").hide();
     $("#btnagregar_antecedentes_quirurgicos_gb").show();
     $(".observacion").val("");
     
  });
 
  
    /**********ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES QUIRURGICOS GINECOLOGIA Y OBSTERTRICIA***** */
  $(document).on('click','#btnagregar_antecedentes_quirurgicos_gb', function(e)
  {
    
   var antecedentes_quirurgicos=$('#antecedentes_quirurgicos_gb').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (antecedentes_quirurgicos.trim()==='')
   {
   alert('NO DEBE DEJAR EL CAMPO VACIO');
  }
  else
  {
     var data=
       {
          n_historial:n_historial,
          antecedentes_quirurgicos:antecedentes_quirurgicos,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_antecedentes_quirurgicos';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               if(data===1)
               {
                  alert('Registro Exitoso');
                  window.location = '/medico_Consultas/'
               }
               else if(data===2)
               {
                  alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                 
               }
               else
               {
                 alert('Error en la Incorporación del registro');
               }
               window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     }  
   });
 
   $('#lista_antecedentes_qx_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_antecedentes_quirurgicos_gb").modal("show"); 
        $("#btnagregar_antecedentes_quirurgicos_gb").hide()
        $("#btnActualizar_antecedentes_quirurgicos_gb").show() 
        $('#antecedentes_quirurgicos_gb').val(observacion);
        $('#id').val(id);
 
   });
 
 
   $(document).on('click','#btnActualizar_antecedentes_quirurgicos_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var antecedentes_quirurgicos=$('#antecedentes_quirurgicos_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_antecedentes_quirurgicos';
        if (antecedentes_quirurgicos.trim()==='')
        {
        alert('NO DEBE DEJAR EL CAMPO VACIO');
       }
       else
       {
          var data=
          {
               id :id ,
               antecedentes_quirurgicos  :antecedentes_quirurgicos,  
               today:today,      
          }
          
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     }
  });
 
   /**********ESTE METODO AGREGA LOS DATOS DEl EXAMEN FISICO GINECOLOGIA Y OBSTERTRICIA***** */
  $(document).on('click','#btnMostrar_examen_fisico_gb', function(e)
  {
     $("#modalexamen_fisico_gb").modal("show"); 
     $("#btnActualizar_examen_fisico_gb").hide();
     $("#btnagregar_examen_fisico_gb").show();  
     $(".observacion").val(""); 
  });
 
   
  $(document).on('click','#btnagregar_examen_fisico_gb', function(e)
  {
      var examen_fisico=$('#obs_examen_fisico_gb').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (examen_fisico.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {  
          var data=
          {
               n_historial:n_historial,
               examen_fisico:examen_fisico,
               id_consulta:id_consulta
          }
     
          var url='/agregar_examen_fisico';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
                    if(data===1)
                    {
                    alert('Registro Exitoso');
                    window.location = '/medico_Consultas/'
                    }
                    else if(data===2)
                    {
                    alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                    
                    }
                    else
                    {
                    alert('Error en la Incorporación del registro');
                    }
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               
               
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     } 
   });
 
 
 
   $('#lista_examen_fisico_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modalexamen_fisico_gb").modal("show"); 
        $("#btnagregar_examen_fisico_gb").hide()
        $("#btnActualizar_examen_fisico_gb").show() 
        $('#obs_examen_fisico_gb').val(observacion);
        $('#id').val(id);
 
   });
 
 
 
   $(document).on('click','#btnActualizar_examen_fisico_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var examen_fisico=$('#obs_examen_fisico_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_examen_fisico';
       
     if (examen_fisico.trim()==='')
     {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
        var data=
        {
          id :id ,
          examen_fisico  :examen_fisico,    
          today:today,    
        }
        
        $.ajax
        ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {
              
                 if(data=='1')
                 {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                 }
                 else
                 {
                      alert('Error en la Incorporación del Registro');
                 }
            },
            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
             }
         });
     }
  });
 
 
  $(document).on('click','#btnMostrar_paraclinicos_gb', function(e)
  {
     $("#modal_paraclinicos_gb").modal("show"); 
     $("#btnActualizar_paraclinicos_gb").hide();
     $("#btnagregar_paraclinicos_gb").show();   
     $(".observacion").val("");
  });
 
 
 
  /**********ESTE METODO AGREGA LOS DATOS DE PARACLINICOS GINECOLOGIA Y OBSTERTRICIA***** */
  $(document).on('click','#btnagregar_paraclinicos_gb', function(e)
  {
    
      var paraclinicos=$('#obs_paraclinicos_gb').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (paraclinicos.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
          var data=
          {
               n_historial:n_historial,
               paraclinicos:paraclinicos,
               id_consulta:id_consulta,
          }
     
          var url='/agregar_paraclinicos';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
                    //alert(data);
                    if(data===1)
                    {
                    alert('Registro Exitoso');
                    window.location = '/medico_Consultas/'
                    }
                    else if(data===2)
                    {
                    alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                    
                    }
                    else
                    {
                    alert('Error en la Incorporación del registro');
                    }
                    window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
               
               
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     } 
   });
 
 
 
   $('#lista_paraclinicos_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_paraclinicos_gb").modal("show"); 
        $("#btnagregar_paraclinicos_gb").hide()
        $("#btnActualizar_paraclinicos_gb").show() 
        $('#obs_paraclinicos_gb').val(observacion);
        $('#id').val(id);
 
   });
 
 
 
   $(document).on('click','#btnActualizar_paraclinicos_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var paraclinicos=$('#obs_paraclinicos_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_paraclinicos';
        if (paraclinicos.trim()==='')
      {
      alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
       
        var data=
        {
          id :id ,
          paraclinicos  :paraclinicos,
          today:today,        
        }
        
        $.ajax
        ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {
              
                 if(data=='1')
                 {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                 }
                 else
                 {
                      alert('Error en la Incorporación del Registro');
                 }
            },
            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
             }
         });
     }
  });
 
 
 
 
  $(document).on('click','#btnMostrar_impresion_diag_gb', function(e)
  {
     $("#modal_impresion_diag_gb").modal("show"); 
     $("#btnActualizar_impresion_diag_gb").hide();
     $("#btnagregar_impresion_diag_gb").show();   
     $(".observacion").val("");
  });
 
 
    /**********ESTE METODO AGREGA LOS DATOS DEL IMPRESION DIAGNOSTICA GINECOLOGIA Y OBSTERTRICIA***** */
  $(document).on('click','#btnagregar_impresion_diag_gb', function(e)
  {
   
   var impresiondiagnostica=$('#obs_impresion_diag_gb').val();
   var cedula      =$('#cedulab').val();
   var tipobeneficiario  ='C';
   var n_historial=$('#numeroHistorial').val();
   var id_especialidad=$('#id_especialidad').val();
   var fecha_asistencia=$('#fecha_creacion').val();
   var id_historial_medico=$('#id_historial_medico').val();
   var id_consulta=$('#id_consulta').val();
   if (impresiondiagnostica.trim()==='')
   {
   alert('NO DEBE DEJAR EL CAMPO VACIO');
  }
  else
  {
      var data=
       {
          n_historial:n_historial,
          impresiondiagnostica:impresiondiagnostica,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_impresiondiagnostica';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
              //alert(data);
              if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
         
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     } 
   });
 
 
   $('#lista_impresion_diag_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_impresion_diag_gb").modal("show"); 
        $("#btnagregar_impresion_diag_gb").hide()
        $("#btnActualizar_impresion_diag_gb").show() 
        $('#obs_impresion_diag_gb').val(observacion);
        $('#id').val(id);
 
   });
 
 
   $(document).on('click','#btnActualizar_impresion_diag_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var impresiondiagnostica=$('#obs_impresion_diag_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_impresiondiagnostica';
        if (impresiondiagnostica.trim()==='')
     {
          alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
     else
     {
        var data=
        {
          id :id ,
          impresiondiagnostica  :impresiondiagnostica,
          today:today,        
        }
        
        $.ajax
        ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {
              
                 if(data=='1')
                 {
                       alert('Registro Actualizado');
                       window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                 }
                 else
                 {
                      alert('Error en la Incorporación del Registro');
                 }
            },
            error:function(xhr, status, errorThrown)
            {
                 alert(xhr.status);
                 alert(errorThrown);
             }
         });
     }
  });
 
  $(document).on('click','#btnMostrar_plan_gb', function(e)
  {
     $("#modal_plan_gb").modal("show"); 
     $("#btnActualizar_plan_gb").hide();
     $("#btnagregar_plan_gb").show();   
     $(".observacion").val("");
  });
 
 
 /**********ESTE METODO AGREGA LOS DATOS DEL PLAN GINECOLOGIA Y OBSTERTRICIA***** */
  $(document).on('click','#btnagregar_plan_gb', function(e)
  {
 
      var plan=$('#obs_plan_gb').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (plan.trim()==='')
     {
          alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
      else
     { 
      var data=
       {
          n_historial:n_historial,
          plan:plan,
          id_consulta:id_consulta,
       }
   
       var url='/agregar_plan';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               //alert(data);
               if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     }
   });
 
 
 
   $('#lista_plan_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_plan_gb").modal("show"); 
        $("#btnagregar_plan_gb").hide()
        $("#btnActualizar_plan_gb").show() 
        $('#obs_plan_gb').val(observacion);
        $('#id').val(id);
 
   });
   
 
   $(document).on('click','#btnActualizar_plan_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var plan=$('#obs_plan_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_plan';
        if (plan.trim()==='')
        {
             alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
         else
        {     
          var data=
          {
               id :id ,
               plan  :plan,
               today:today,        
          }
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
        }
  });
 
 
 
 
 
  $(document).on('click','#btnMostrar_medicamentos_gb', function(e)
  {
     $("#modal_medicamentos_gb").modal("show"); 
     $("#btnActualizar_medicamentos_gb").hide();
     $("#btnagregar_medicamentos_gb").show();  
     $(".observacion").val(""); 
  });
 
 
 /********************ESTE METODO AGREGA LOS DATOS DE MEDICAMENTO GINECOLOGIA Y OBSTERTRICIA***** */
  $(document).on('click','#btnagregar_medicamentos_gb', function(e)
  {
      var medicamentos=$('#obs_medicamentos_gb').val();
      var cedula      =$('#cedulab').val();
      var tipobeneficiario  ='C';
      var n_historial=$('#numeroHistorial').val();
      var id_especialidad=$('#id_especialidad').val();
      var fecha_asistencia=$('#fecha_creacion').val();
      var id_historial_medico=$('#id_historial_medico').val();
      var id_consulta=$('#id_consulta').val();
      if (medicamentos.trim()==='')
     {
          alert('NO DEBE DEJAR EL CAMPO VACIO');
     }
      else
     { 
      var data=
       {
          n_historial:n_historial,
          medicamentos:medicamentos,
          id_consulta:id_consulta,
          id_especialidad:id_especialidad,
       }
   
       var url='/agregar_medicamentos';
       $.ajax
       ({
            url:url,
            method:'POST',
            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
            dataType:'JSON',
            beforeSend:function(data)
            {
                 //alert('Procesando Información ...');
            },
            success:function(data)
            {
               //alert(data);
               if(data===1)
              {
                 alert('Registro Exitoso');
                 window.location = '/medico_Consultas/'
              }
              else if(data===2)
              {
                 alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
                
              }
              else
              {
                alert('Error en la Incorporación del registro');
              }
              window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
          
            },
            error:function(xhr, status, errorThrown)
            {
               alert(xhr.status);
               alert(errorThrown);
            }
       });
     }
   });
 
 
 
   $('#lista_medicamentos_gb').on('click','.Editar', function(e)
   { 
        e.preventDefault();
  
        let observacion=$(this).attr('observacion')
        let id=$(this).attr('id');
        $("#modal_medicamentos_gb").modal("show"); 
        $("#btnagregar_medicamentos_gb").hide()
        $("#btnActualizar_medicamentos_gb").show() 
        $('#obs_medicamentos_gb').val(observacion);
        $('#id').val(id);
 
 
   });
   
 
   $(document).on('click','#btnActualizar_medicamentos_gb', function(e)
   {
        e.preventDefault();
        var id          =$('#id').val();
        var medicamentos=$('#obs_medicamentos_gb').val();
        var cedula      =$('#cedulab').val();
        var tipobeneficiario  ='C';
        var n_historial=$('#numeroHistorial').val();
        var id_especialidad=$('#id_especialidad').val();
        var fecha_asistencia=$('#fecha_creacion').val();
        var id_historial_medico=$('#id_historial_medico').val();
        var id_consulta=$('#id_consulta').val();
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today= now.getFullYear()+"/"+month+"/"+day;
        var url='/actualizar_medicamentos';
        if (medicamentos.trim()==='')
        {
             alert('NO DEBE DEJAR EL CAMPO VACIO');
        }
         else
        {     
          var data=
          {
               id :id ,
               medicamentos  :medicamentos,
               today:today,        
          }
          
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
        }
  });
 












/********************ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES-GINECO_OBSTETRICO***** */
$(document).on('click','#btnagregar_Antecedentes_gineco_obstetricos', function(e)
{
    let cedula      =$('#cedulab').val();
    let tipobeneficiario  ='C';
    let n_historial=$('#numeroHistorial').val();
    let id_especialidad=$('#id_especialidad').val();
    let fecha_asistencia=$('#fecha_creacion').val();
    let id_historial_medico=$('#id_historial_medico').val();
    let id_consulta=$('#id_consulta').val();
    let gestacion=$('#gestacion').val();
    let partos=$('#partos').val();
    let cesarias=$('#cesarias').val();
    let abortos=$('#abortos').val();
    let eg=$('#eg').val();
    let f_u_r=$('#f_u_r').val();
    let menarquia=$('#menarquia').val();
    let sexarquia=$('#sexarquia').val();
    let nps=$('#nps').val();
    let ciclo_mestrual=$('#ciclo_mestrual').val(); 
    let dismenorrea=$('#dismenorrea').val();
    let eumenorrea=$('#eumenorrea').val();
    let detalle_historia=$('#detalle_historia').val();
    let diu=$('#diu').val();
    let t_cobre=$('#t_cobre').val();
    let mirena=$('#mirena').val();
    let aspiral=$('#aspiral').val();
    let i_subdermico=$('#i_subdermico').val();
    let otro=$('#otro').val();
    if($('#diu').is(':checked'))
    {
        diu='true';
    }
    else
    {
        diu='false';
    }

    if($('#i_subdermico').is(':checked'))
    {
        i_subdermico='true';
    }
    else
    {
        i_subdermico='false';
    }
    if($('#aspiral').is(':checked'))
    {
        aspiral='true';
    }
    else
    {
        aspiral='false';
    }
    if($('#mirena').is(':checked'))
    {
        mirena='true';
    }
    else
    {
        mirena='false';
    }


    if($('#t_cobre').is(':checked'))
    {
        t_cobre='true';
    }
    else
    {
        t_cobre='false';
    }


    if (dismenorrea=='1') 
    {
         dismenorrea='true'
    }

     else if (dismenorrea=='2') 
    {
         dismenorrea='false'
    }
    else  
    {
         dismenorrea='false'
    }

    
    if (eumenorrea=='1') 
    {
         eumenorrea='true'
    }
     else if (eumenorrea=='2') 
    {
         eumenorrea='false'
    }
    else  
    {
         eumenorrea='false'
    }

    let anticon=$('#anticon').val();

    if (anticon=='1') 
    {
         anticon='true'
    }
     else if (anticon=='2') 
    {
         anticon='false'
    }
    else  
    {
         anticon='false'
    }

    if (menarquia=='') 
    {
     menarquia=0; 
    }
    if (sexarquia=='') 
    {
     sexarquia=0; 
         }
    if (nps=='') 
    {
     nps=0;
     }

    
    let detalles_anticon=$('#detalles_anticon').val();
    let t_colocacion=$('#t_colocacion').val();


    let ets=$('#ets').val();
    if (ets=='1') 
    {
         ets='true'
    }
     else if (ets=='2') 
    {
         ets='false'
    }
    else  
    {
         ets='false'
    }

    let tipo_ets=$('#tipo_ets').val();
    let detalles_ets=$('#detalles_ets').val();

    let citologia=$('#citologia').val();
    let eco_mamario=$('#eco_mamario').val();
    let mamografia=$('#mamografia').val();
    let desintometria=$('#desintometria').val();
    
    let data=
     {
        n_historial:n_historial,
        id_consulta:id_consulta,
        gestacion:gestacion,
        partos:partos,
        cesarias:cesarias,
        abortos:abortos,
        eg:eg,
        f_u_r:f_u_r,
        menarquia:menarquia,
        sexarquia:sexarquia, 
        nps:nps,
        ciclo_mestrual:ciclo_mestrual,
        dismenorrea:dismenorrea,
        eumenorrea:eumenorrea,
        anticon:anticon,
        detalles_anticon:detalles_anticon,
        t_colocacion:t_colocacion,
        ets:ets,
        tipo_ets:tipo_ets,
        detalles_ets:detalles_ets,
        citologia:citologia,
        eco_mamario:eco_mamario,
        mamografia:mamografia,
        desintometria:desintometria,
        detalle_historia:detalle_historia,
        diu:diu,
        t_cobre:t_cobre,
        mirena:mirena,
        aspiral:aspiral,
        i_subdermico:i_subdermico,
        otro:otro,
     }
     var url='/agregar_Antecedentes_gineco_obstetricos';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
            {
               alert('Registro Exitoso');
               window.location = '/medico_Consultas/'
            }
            else if(data===2)
            {
               alert('YA EXISTE UN REGISTRO PARA ESTA CONSUTA');
              
            }
            else
            {
              alert('Error en la Incorporación del registro');
            }
            window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
        
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
  
 });

 $(document).on('click','#btnActualizar_Antecedentes_gineco_obstetricos', function(e)
   {
        e.preventDefault();
        let cedula      =$('#cedulab').val();
        let tipobeneficiario  ='C';
        let n_historial=$('#numeroHistorial').val();
        let id_especialidad=$('#id_especialidad').val();
        let fecha_asistencia=$('#fecha_creacion').val();
        let id_historial_medico=$('#id_historial_medico').val();
        let id_consulta=$('#id_consulta').val();
        let gestacion=$('#gestacion').val();
        let partos=$('#partos').val();
        let cesarias=$('#cesarias').val();
        let abortos=$('#abortos').val();
        let eg=$('#eg').val();
        let f_u_r=$('#f_u_r').val();
        let menarquia=$('#menarquia').val();
        let sexarquia=$('#sexarquia').val();
        let nps=$('#nps').val();
        let ciclo_mestrual=$('#ciclo_mestrual').val(); 
        let dismenorrea=$('#dismenorrea').val();
        let eumenorrea=$('#eumenorrea').val();
        let detalle_historia=$('#detalle_historia').val();
        let diu=$('#diu').val();
        let t_cobre=$('#t_cobre').val();
        let mirena=$('#mirena').val();
        let aspiral=$('#aspiral').val();
        let i_subdermico=$('#i_subdermico').val();
        let otro=$('#otro').val();
        if($('#diu').is(':checked'))
    {
        diu='true';
    }
    else
    {
        diu='false';
    }

    if($('#i_subdermico').is(':checked'))
    {
        i_subdermico='true';
    }
    else
    {
        i_subdermico='false';
    }
    if($('#aspiral').is(':checked'))
    {
        aspiral='true';
    }
    else
    {
        aspiral='false';
    }
    if($('#mirena').is(':checked'))
    {
        mirena='true';
    }
    else
    {
        mirena='false';
    }


    if($('#t_cobre').is(':checked'))
    {
        t_cobre='true';
    }
    else
    {
        t_cobre='false';
    }
        if (dismenorrea=='1') 
        {
             dismenorrea='true'
        }
    
         else if (dismenorrea=='2') 
        {
             dismenorrea='false'
        }
        else  
        {
             dismenorrea='false'
        }
    
        
        if (eumenorrea=='1') 
        {
             eumenorrea='true'
        }
         else if (eumenorrea=='2') 
        {
             eumenorrea='false'
        }
        else  
        {
             eumenorrea='false'
        }
    
        let anticon=$('#anticon').val();
    
        if (anticon=='1') 
        {
             anticon='true'
        }
         else if (anticon=='2') 
        {
             anticon='false'
        }
        else  
        {
             anticon='false'
        }
    
        if (menarquia=='') 
        {
         menarquia=0; 
        }
        if (sexarquia=='') 
        {
         sexarquia=0; 
             }
        if (nps=='') 
        {
         nps=0;
         }
    
        
        let detalles_anticon=$('#detalles_anticon').val();
        let t_colocacion=$('#t_colocacion').val();
    
    
        let ets=$('#ets').val();
        if (ets=='1') 
        {
             ets='true'
        }
         else if (ets=='2') 
        {
             ets='false'
        }
        else  
        {
             ets='false'
        }
    
        let tipo_ets=$('#tipo_ets').val();
        let detalles_ets=$('#detalles_ets').val();
    
        let citologia=$('#citologia').val();
        let eco_mamario=$('#eco_mamario').val();
        let mamografia=$('#mamografia').val();
        let desintometria=$('#desintometria').val();
        
        let data=
         {
            n_historial:n_historial,
            id_consulta:id_consulta,
            gestacion:gestacion,
            partos:partos,
            cesarias:cesarias,
            abortos:abortos,
            eg:eg,
            f_u_r:f_u_r,
            menarquia:menarquia,
            sexarquia:sexarquia, 
            nps:nps,
            ciclo_mestrual:ciclo_mestrual,
            dismenorrea:dismenorrea,
            eumenorrea:eumenorrea,
            anticon:anticon,

            detalles_anticon:detalles_anticon,
            t_colocacion:t_colocacion,
            ets:ets,
            tipo_ets:tipo_ets,
            detalles_ets:detalles_ets,
            citologia:citologia,
            eco_mamario:eco_mamario,
            mamografia:mamografia,
            desintometria:desintometria,
            detalle_historia:detalle_historia,
            diu:diu,
            t_cobre:t_cobre,
            mirena:mirena,
            aspiral:aspiral,
            i_subdermico:i_subdermico,
            otro:otro,
         }
    
        var url='/actualizar_Antecedentes_gineco_obstetricos';  
        
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
               
                    if(data=='1')
                    {
                         alert('Registro Actualizado');
                         window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta;  
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
        
  });
 







/********************ESTE METODO AGREGA LOS DATOS DE ANTECEDENTES-GINECO_OBSTETRICO***** */



   // ****************************DATA TABLES*************************
function listar_enfermedad_actual()
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
   let id_consulta  =$('#id_consulta').val();
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_enfermedad_actual').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_enfermedad_actual/"+numeroHistorial+'/'+id_consulta,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               //{data:'fecha_creacion'},  
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-success Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}


function alergias_medicamentos()
{

     
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt(medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_alergias_medicamentos').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_alergias_medicamentos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'fecha_creacion'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {
                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-success Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }

                    
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function antecedentespatologicosfamiliares()
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_pf').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentes_patologicosf/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'fecha_creacion'},
               {data:'fecha_actualizacion'},                          


               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico!=medico_id)
                  {                  
                       return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                  else
                  {
                       return '<a href="javascript:;" class="btn btn-xs btn-primary Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }       
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function antecedentesquirurgicos()
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_qx').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentesquirurgicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},  
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico!=medico_id)
                  {                  
                       return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                  else
                  {
                       return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                  }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function antecedentespatologicospersonales()
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentes_patologicosp/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},
               {data:'fecha_actualizacion'},                          


               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico!=medico_id)
                  {                  
                       return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                  else
                  {
                       return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                 
                  }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function listar_examen_fisico()
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#examen_fisico').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_examen_fisico/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico!=medico_id)
                  {                  
                       return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                  else
                  {
                       return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                  }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}
function listar_paraclinicos()
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#tableparaclinicos').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_paraclinicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico!=medico_id)
                  {                  
                       return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                  else
                  {
                       return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function listar_impresion_diag()
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#impresion_diag').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_impresion_diag/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico!=medico_id)
                  {                  
                       return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                  else
                  {
                       return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function listar_plan()
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_plan').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_plan/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico!=medico_id)
                  {                  
                       return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                  else
                  {
                       return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                  }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}
function listar_habitos_psicosociales()
 {
     let id_consulta  =$('#id_consulta').val();
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
      let historial_medico =$('#id_historial_medico').val();
      $('#habitos_psicosociales').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_habitos_personales/"+historial_medico,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'fecha_creacion'},  
                 
                                   
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }

//**********************DATATABLES MEDICINA INTERNA******************
function listar_enfermedad_actual_mi()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
    let id_consulta  =$('#id_consulta').val();
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_enfermedad_actual_mi').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_enfermedad_actual/"+numeroHistorial+'/'+id_consulta,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'fecha_creacion'},  
               {data:'fecha_actualizacion'},    
                                    


               {orderable: true,
                render:function(data, type, row)
                {
                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Detalle" style=" font-size:2px" data-toggle="tooltip" title="Historico de  Medicamentos Entregados" cedula='+row.cedula+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">print</i></a>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Detalle" style=" font-size:2px" data-toggle="tooltip" title="Historico de  Medicamentos Entregados" cedula='+row.cedula+' rol='+row.descripcion+' estatus="'+row.estatus+'"><i class="material-icons ">print</i></a>'
                   }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function antecedentespatologicospersonales_mi()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_pp_mi').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentes_patologicosp/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},
               {data:'especialidad'},
               {data:'fecha_actualizacion'},                          


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }

                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}


function antecedentesquirurgicos_mi()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_qx_mi').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentesquirurgicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},  
               {data:'especialidad'},  
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }
                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}




function listar_habitos_psicosociales_mi()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let historial_medico =$('#id_historial_medico').val();
     $('#habitos_psicosociales_mi').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_habitos_personales/"+historial_medico,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'fecha_creacion'},  
                
                                  
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function listar_examen_fisico_mi()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#examen_fisico_mi').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_examen_fisico/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }

                
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_paraclinicos_mi()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#tableparaclinicos_mi').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_paraclinicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_impresion_diag_mi()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#impresion_diag_mi').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_impresion_diag/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},  
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }

                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_plan_mi()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_plan_mi').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_plan/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},   
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }


                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function listar_medicamentos_mi()
 {
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let id_consulta  =$('#id_consulta').val();
     var id_especialidad=$('#id_especialidad').val();
      let numeroHistorial  =$('#numeroHistorial').val();
      $('#table_medicamentos_mi').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_medicamentos_mi/"+numeroHistorial+'/'+id_especialidad+'/'+id_consulta,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'}, 
                {data:'fecha_creacion'},  
                {data:'fecha_actualizacion'},    
                                     


                {orderable: true,
                 render:function(data, type, row)
                 {
                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                    }
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }



//************************DATA TABLE PSICOLOGIA PRIMARIA*************************************
 

function  llenar_combo_especialidad(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_especialidades_activas';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {   
 if(data.length>=1)
 {
      $('#especialidad').empty();
      $('#especialidad').append('<option value=0>Seleccione</option>');     
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
              

               if(item.id_especialidad===id_especialidad)
              
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }
 function  llenar_combo_MedicoTratante(e,id)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {

 if(data.length>=1)
 {
      $('#cmbmedicotratante').empty();
      $('#cmbmedicotratante').append('<option value=0>Seleccione</option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              

               if(item.id===id)
              
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+' selected>'+item.nombre+'  '+item.apellido+'</option>');
               }
               else
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }




 
 function  llenar_combo_MedicoReferido(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
           
 if(data.length>=1)
 {
      $('#cmbmedicosreferidos').empty();
      $('#cmbmedicosreferidos').append('<option value=0  selected disabled>Medicos</option>');   
      verificar=7;  
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
        data=data.filter(dato=>dato.id_especialidad==id_especialidad);
        //console.log(buscar);
           $.each(data, function(i, item)
           {
            $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

              
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $("#especialidad").on('change', function(e)
 {
     document.getElementById("cmbmedicosreferidos").disabled=false;
    id_especialidad=$('#especialidad option:selected').val(); 

    llenar_combo_MedicoReferido(e,id_especialidad)
 }); 

/*
  * Función para definir datatable:
  */
function listar_psicologia_primaria(numeroHistorial)
{
   var medico_id  =$('#medico_id').val();
   medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt( medico_id);
     $('#table_psicologia').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_psicologia_primaria/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [
               {data:'id'},
               {data:'fecha_seguimiento'},
               {data:'evolucion'},    
               {data:'nombre'},               
               {orderable: true,
                render:function(data, type, row)
                {
                  if(row.id_medico_tabla!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.observacion+'"> <i class="material-icons " >create</i></a>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.observacion+'"> <i class="material-icons " >create</i></a>'
                    }
                   }
               }
           ],
           
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],	
            		
           }
      });
}



function listar_enfermedad_actual_PSICO()
 {
      var medico_id  =$('#medico_id').val();
      medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
      let numeroHistorial  =$('#numeroHistorial').val();
      let id_consulta  =$('#id_consulta').val();
     
      $('#table_diagnostico').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_enfermedad_actual/"+numeroHistorial+'/'+id_consulta,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'}, 
                //{data:'fecha_creacion'},  
                {data:'fecha_actualizacion'},                         


                {orderable: true,
                 render:function(data, type, row)
                 {

                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-success Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }

                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }












function listar_plan_PSICO()
 {
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
      let numeroHistorial  =$('#numeroHistorial').val();
      $('#table_plan_PSICO').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_plan/"+numeroHistorial,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'}, 
                {data:'especialidad'}, 
                {data:'fecha_actualizacion'},                        


                {orderable: true,
                 render:function(data, type, row)
                 {
                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }

                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }


//******************************************************DATATABLES PACIENTE PEDIATRICO***********************************************
function listar_enfermedad_actual_pp()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
    let id_consulta  =$('#id_consulta').val();
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_enfermedad_actual_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_enfermedad_actual/"+numeroHistorial+'/'+id_consulta,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               
               {data:'fecha_actualizacion'},    
                                    


               {orderable: true,
                render:function(data, type, row)
                {
                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                   }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function antecedentespatologicospersonales_pp()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_pp_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentes_patologicosp/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},
               {data:'especialidad'},
               {data:'fecha_actualizacion'},                          


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }

                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}


function antecedentesquirurgicos_pp()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_qx_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentesquirurgicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},  
               {data:'especialidad'},  
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }
                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}




function listar_habitos_psicosociales_pp()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let historial_medico =$('#id_historial_medico').val();
     $('#habitos_psicosociales_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_habitos_personales/"+historial_medico,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'fecha_creacion'},  
                
                                  
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function listar_examen_fisico_pp()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_examen_fisico_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_examen_fisico/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }

                
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_paraclinicos_pp()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#tableparaclinicos_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_paraclinicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_impresion_diag_pp()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#impresion_diag_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_impresion_diag/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},  
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }

                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_plan_pp()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_plan_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_plan/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},   
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }


                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}


function antecedentespatologicosfamiliares_pp()
 {
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
      let numeroHistorial  =$('#numeroHistorial').val();
      $('#antecedentes_pf_pp').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_antecedentes_patologicosf/"+numeroHistorial,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'},
                {data:'especialidad'},
                {data:'fecha_actualizacion'},                          


                {orderable: true,
                 render:function(data, type, row)
                 {

                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }       
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }

 function listar_medicamentos_pp()
 {
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let id_consulta  =$('#id_consulta').val();
     let numeroHistorial  =$('#numeroHistorial').val();
     var id_especialidad=$('#id_especialidad').val();
      $('#table_medicamentos_pp').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_medicamentos_pp/"+numeroHistorial+'/'+id_especialidad+'/'+id_consulta,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'}, 
                {data:'fecha_creacion'},  
                {data:'fecha_actualizacion'},    
                                     


                {orderable: true,
                 render:function(data, type, row)
                 {
                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                    }
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }



//******************************************************DATATABLES GINECOLOGIA Y OBSTETRICIA***********************************************
function listar_enfermedad_actual_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
    let id_consulta  =$('#id_consulta').val();
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_enfermedad_actual_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_enfermedad_actual/"+numeroHistorial+'/'+id_consulta,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               
               {data:'fecha_actualizacion'},    
                                    


               {orderable: true,
                render:function(data, type, row)
                {
                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                   }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function antecedentespatologicospersonales_gb()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_pp_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentes_patologicosp/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},
               {data:'especialidad'},
               {data:'fecha_actualizacion'},                          


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }

                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}


function antecedentesquirurgicos_gb()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_qx_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentesquirurgicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},  
               {data:'especialidad'},  
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }
                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}




function listar_habitos_psicosociales_gb()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let historial_medico =$('#id_historial_medico').val();
     $('#habitos_psicosociales_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_habitos_personales/"+historial_medico,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'fecha_creacion'},  
                
                                  
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function listar_examen_fisico_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_examen_fisico_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_examen_fisico/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }

                
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_paraclinicos_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#tableparaclinicos_pp').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_paraclinicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_impresion_diag_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#impresion_diag_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_impresion_diag/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},  
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }

                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_plan_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_plan_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_plan/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},   
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }


                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}


function antecedentespatologicosfamiliares_gb()
 {
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
      let numeroHistorial  =$('#numeroHistorial').val();
      $('#antecedentes_pf_gb').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_antecedentes_patologicosf/"+numeroHistorial,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'},
                {data:'especialidad'},
                {data:'fecha_actualizacion'},                          


                {orderable: true,
                 render:function(data, type, row)
                 {

                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }       
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }

 function listar_medicamentos_gb()
 {
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let id_consulta  =$('#id_consulta').val();
      let numeroHistorial  =$('#numeroHistorial').val();
      $('#table_medicamentos_gb').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_medicamentos_pp/"+numeroHistorial,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'}, 
                {data:'fecha_creacion'},  
                {data:'fecha_actualizacion'},    
                                     


                {orderable: true,
                 render:function(data, type, row)
                 {
                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                    }
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }



//******************************************************DATATABLES GINECOLOGIA Y OBSTETRICIA***********************************************
function listar_enfermedad_actual_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
    let id_consulta  =$('#id_consulta').val();
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_enfermedad_actual_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_enfermedad_actual/"+numeroHistorial+'/'+id_consulta,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               
               {data:'fecha_actualizacion'},    
                                    


               {orderable: true,
                render:function(data, type, row)
                {
                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                   }
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function antecedentespatologicospersonales_gb()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_pp_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentes_patologicosp/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},
               {data:'especialidad'},
               {data:'fecha_actualizacion'},                          


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }

                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}


function antecedentesquirurgicos_gb()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#antecedentes_qx_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_antecedentesquirurgicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'},  
               {data:'especialidad'},  
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }
                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}




function listar_habitos_psicosociales_gb()
{
    let id_consulta  =$('#id_consulta').val();
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let historial_medico =$('#id_historial_medico').val();
     $('#habitos_psicosociales_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_habitos_personales/"+historial_medico,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'fecha_creacion'},  
                
                                  
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}

function listar_examen_fisico_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_examen_fisico_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_examen_fisico/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'

                   }

                
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_paraclinicos_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#tableparaclinicos_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_paraclinicos/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'}, 
               {data:'fecha_actualizacion'},                         


               {orderable: true,
                render:function(data, type, row)
                {

                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_impresion_diag_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#impresion_diag_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_impresion_diag/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},  
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }

                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}



function listar_plan_gb()
{
    var medico_id  =$('#medico_id').val();
    medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let numeroHistorial  =$('#numeroHistorial').val();
     $('#table_plan_gb').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":"/listar_plan/"+numeroHistorial,
            "type":"GET",
            dataSrc:''
           },
           "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
           "columns":
           [{
              
               data:'descripcion'},
               {data:'nombre'}, 
               {data:'especialidad'},   
               {data:'fecha_actualizacion'},                        


               {orderable: true,
                render:function(data, type, row)
                {


                   if(row.id_medico!=medico_id)
                   {                  
                        return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }
                   else
                   {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                   }


                 
                   }
               }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });
}


function antecedentespatologicosfamiliares_gb()
 {
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
      let numeroHistorial  =$('#numeroHistorial').val();
      $('#antecedentes_pf_gb').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_antecedentes_patologicosf/"+numeroHistorial,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'},
                {data:'especialidad'},
                {data:'fecha_actualizacion'},                          


                {orderable: true,
                 render:function(data, type, row)
                 {

                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-ligth Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar"  id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i>'
                    }       
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }

 function listar_medicamentos_gb()
 {
     var medico_id  =$('#medico_id').val();
     medico_id = (isNaN(parseInt( medico_id)))? 0 : parseInt(medico_id);
     let id_consulta  =$('#id_consulta').val();
      let numeroHistorial  =$('#numeroHistorial').val();
      $('#table_medicamentos_gb').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_medicamentos_pp/"+numeroHistorial,
             "type":"GET",
             dataSrc:''
            },
            "columnDefs": [
               {"className": "text-left", "targets": [0]}
             ],
            "columns":
            [{
               
                data:'descripcion'},
                {data:'nombre'}, 
                {data:'fecha_creacion'},  
                {data:'fecha_actualizacion'},    
                                     


                {orderable: true,
                 render:function(data, type, row)
                 {
                    if(row.id_medico!=medico_id)
                    {                  
                         return '<a href="javascript:;" class="btn btn-xs btn-light Editar disabled" style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                    }
                    else
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar " style=" font-size:1px" data-toggle="tooltip" title="Editar" evolucion="'+row.evolucion+'" id='+row.id+'  observacion="'+row.descripcion+'"> <i class="material-icons " >create</i></a>'
                    }
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }

















