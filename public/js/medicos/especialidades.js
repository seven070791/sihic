/*
 *Este es el document ready
 */
 $(function()
 {
     listar_especialidad();
   
     //Lleno la persiana de la unidad de medida
 });
 /*
  * Función para definir datatable:
  */
 function listar_especialidad()
 {
      $('#table_especialidad').DataTable
      (
       {
            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_especialidad",
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [{
                data:'id_especialidad'},
                {data:'descripcion'},
                                        
               
                

                {orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.borrado=='Bloqueado')
                   
                      {
                         return '<span id="btnbloqueado" class="btnbloqueado "readonly="readonly">'+row.borrado+'</span>'
                      }
                      else
                      {
                         return '<span id="btnactivo" class="btnactivo"readonly="readonly">'+row.borrado+'</span>'
                       
                      } 
                   
                    }
               },

               {orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.acceso_citas=='N')
                   
                      {
                         return '<span id="btnbloqueado"class="btnbloqueado "readonly="readonly">'+row.acceso_citas+'</span>'
                      }
                      else
                      {
                         return '<span id="btnactivo" class="btnactivo"readonly="readonly">'+row.acceso_citas+'</span>'
                       
                      } 
                   
                    }
               },




                {orderable: true,
                 render:function(data, type, row)
                 {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary  Editar" style=" font-size:1px" data-toggle="tooltip" title="Editar" id_especialidad='+row.id_especialidad+' descripcion="'+row.descripcion+'" estatus='+row.borrado+' acceso='+row.acceso_citas+'> <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
     $('#categoria').val('');
    
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();

      $('#modal').find('#activo').hide();

      $("#modal").modal("show");
   

 });
 
 $('#lista_de_categoria').on('click','.Editar', function(e)
 {
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').show();
     var id_especialidad           =$(this).attr('id_especialidad');  
     var descripcion   =$(this).attr('descripcion'); 
     var estatus       =$(this).attr('estatus');
     var acceso=$(this).attr('acceso');
   
     $('#descripcion').val(descripcion);
     $('#id_especialidad').val(id_especialidad);

     $('#descripcion_anterior').val(descripcion);

     if(estatus=='Activo')
     {
          $('#borrado').attr('checked','checked');
          $('#borrado').val('false');
          $('#borrado_anterior').val('false');
     }
     if(estatus=='Bloqueado')
     {
          $('#borrado').removeAttr('checked')
          $('#borrado').val('true')
          $('#borrado_anterior').val('true');
     }


     
     if(acceso=='S')
     {
          $('.acceso_citas').attr('checked','checked');
          $('.acceso_citas').val('true');
          $('#acceso_citas_anterior').val('true');
     }
     if(acceso=='N')
     {
          $('.acceso_citas').removeAttr('checked')
          $('.acceso_citas').val('false')
          $('#acceso_citas_anterior').val('false');
     }
     $("#modal").modal("show");

 });


$(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();
      var id_especialidad           =$('#id_especialidad ').val();
      var descripcion   =$('#descripcion').val();

      if($('.acceso_citas').is(':checked'))
      {
           acceso='true';
      }
      else
      {
           acceso='false';
      }
      var url='/agregar_Especialidad';
      var data=
      {
        id_especialidad  :id_especialidad  ,
        descripcion  :descripcion,    
        acceso:acceso,    
      }
     
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Incorporado');
                     window.location = '/Vita_Especialidad'; 
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });

});

$(document).on('click','#btnActualizar', function(e)
{
     var id_especialidad =$('#id_especialidad ').val();        
     var descripcion   =$('#descripcion ').val();
     var acceso='false';
     var borrado='false';
     let descripcion_anterior=$('#descripcion_anterior ').val();

     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }

     if($('.acceso_citas').is(':checked'))
     {
          acceso='true';
     }
     else
     {
          acceso='false';
     }
   
     var objeto_anterior = {
          "Descripcion": $('#descripcion_anterior ').val(),
          "Bloqueado": $('#borrado_anterior ').val(),
          "AccesoCitas": $('#acceso_citas_anterior ').val(),
             
          
      }
      var objeto_actual= {
          "Descripcion": $('#descripcion ').val(),
          "Bloqueado": borrado,
          "AccesoCitas": acceso,
         
      }
      var camposModificados = [];
      for (var propiedad in objeto_anterior) {
          if (objeto_anterior.hasOwnProperty(propiedad)) {
              if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                  camposModificados.push({
                      propiedad: propiedad,
                      valorAnterior: objeto_anterior[propiedad],
                      valorNuevo: objeto_actual[propiedad]
                  });
              }
          }
      }
      if (camposModificados.length > 0) {
          var datos_modificados = camposModificados.map(function(campo) {
              let string_anterior = campo.valorAnterior;
              let patron = /option-/;
              if (patron.test(string_anterior)) {
                  campo.valorAnterior = string_anterior.replace(patron, "");
              }
              let string_Actual = campo.valorNuevo;
              let patron_Actual = /option-/;
              if (patron.test(string_Actual)) {
                  campo.valorNuevo = string_Actual.replace(patron, "");
              }
              campo.valorAnterior = campo.valorAnterior.trim();

              return campo.valorAnterior === '' ?
                  `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                  `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
          }).join(", ");
     }
     if (datos_modificados == undefined) {
          alert('NO SE HA REALIZADO NINGUNA MODIFICACION');

      } else 
      {
          var data=
          {
               acceso:acceso,
               id_especialidad :id_especialidad ,
               descripcion :descripcion ,
               borrado        :borrado,
               datos_modificados:datos_modificados,
               descripcion_anterior:descripcion_anterior

          }
     
          var url='/actualizar_especialidad';
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
               //alert(data);
               if(data===1)
               {
                    alert('Registro Actualizado');
               }
               else
               {
                    alert('Error en la Incorporación del registro');
               }
               window.location = '/Vita_Especialidad';             
               },
               error:function(xhr, status, errorThrown)
               {
               alert(xhr.status);
               alert(errorThrown);
               }
          });
          }

 });
 $(document).on('click','#btnRegresar', function(e)
{
     window.location = '/vistamedicamentos/';
})
