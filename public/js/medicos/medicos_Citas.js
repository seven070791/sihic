/*
 *Este es el document ready
 */
 $(function()
 {
     var medico_id=$('#medico_id').val();
     citas_atendidas(medico_id);
     citas_not_atendidas(medico_id);
     listar_citas(medico_id);
     
   
     //Lleno la persiana de la unidad de medida
 });

 function citas_atendidas(medico_id)
 {
   
       url=base_url+'/listar_citas_atendidas/'+medico_id;
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'json',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {    
               //alert(data);
           $('#P_atendidos').val(data[0].total_atendidos)
              
           },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
     }); 
 }


 function citas_not_atendidas(medico_id)
 {

    
       url='/listar_citas_not_atendidas/'+medico_id;
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'json',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {    
               //alert(data);
           $('#P_no_atendidos').val(data[0].total_no_atendidos)
              
           },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
     }); 
 }


 /*
  * Función para definir datatable:
  */
 function listar_citas(medico_id)
 {
     
      $('#table_especialidad').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_citas_medicos/"+medico_id,
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [

               {orderable: true,
                    render:function(data, type, row)
                    {
                     if(row.atendido=='ATENDIDO')
                   
                      {
                         return '<input type="checkbox" checked checked onclick = "this.checked=!this.checked">';
                         
                      }
                      else
                      {
                         return '<input type="checkbox" disabled>';
                       
                      } 
                   
                    }
               },
               {orderable: true,
                    render:function(data, type, row)
                    {
                         return '<a href="javascript:;" class="btn btn-xs btn-secondary  Detalles" style=" font-size:1px" data-toggle="tooltip" title="Detalles" id='+row.id+' n_historial='+row.n_historial+' cedula='+row.cedula+' nombre='+row.nombre+'  telefono='+row.telefono+'  fecha_asistencia='+row.fecha_asistencia+'   tipo_beneficiario='+row.tipo_beneficiario+'  id_historial_medico='+row.id_historial_medico+' > <i class="material-icons " >create</i></a>'+'  '+'<a href="javascript:;" class="btn btn-xs btn-primary Imprimir" style=" font-size:1px" data-toggle="tooltip" title="Imprimir" id='+row.id+' n_historial='+row.n_historial+' cedula='+row.cedula+' nombre='+row.nombre+'  telefono='+row.telefono+'  fecha_asistencia='+row.fecha_asistencia+'   tipo_beneficiario='+row.tipo_beneficiario+'  id_historial_medico='+row.id_historial_medico+' > <i class="material-icons " >print</i></a>'
                    }
               },
               
               {data:'n_historial'},  
               {data:'cedula'},   
               {data:'nombre'},             
               {data:'fecha_creacion'},
               {data:'fecha_asistencia'},    
               {data:'telefono'},    

              

               
            ],

            
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
            
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
     $('#categoria').val('');
    
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();

      $('#modal').find('#activo').hide();

    
   

 });
 


$(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();
      var id_especialidad           =$('#id_especialidad ').val();
      var descripcion   =$('#descripcion').val();
      var url='/agregar_Especialidad';
      var data=
      {
        id_especialidad  :id_especialidad  ,
          descripcion  :descripcion,        
      }
     
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
            
               if(data=='1')
               {
                     alert('Registro Incorporado');
                     window.location = '/Vita_Especialidad'; 
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
           }
       });

});

$(document).on('click','#btnActualizar', function(e)
{
     var id_especialidad =$('#id_especialidad ').val();        
     var descripcion   =$('#descripcion ').val();

    
   
     var borrado='false';

     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }

   
     var data=
     {
          id_especialidad :id_especialidad ,
          descripcion :descripcion ,
          borrado        :borrado
     }
 
     var url='/actualizar_especialidad';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
                alert('Registro Actualizado');
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/Vita_Especialidad';             
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
 });
 $(document).on('click','#btnRegresar', function(e)
{
     window.location = '/vistamedicamentos/';
})

$('#lista_de_categoria').on('click','.Detalles', function(e)
{
    let id_consulta=$(this).attr('id');
    var id_especialidad=$('#id_especialidad').val();
    var fecha_asis=$(this).attr('fecha_asistencia'); 
    let fechaconvertida=moment(fecha_asis,'DD/MM/YYYY'); 
    let fecha_asistencia=fechaconvertida.format('DD-MM-YYYY');
    var id_historial_medico=$(this).attr('id_historial_medico');
    var n_historial      =$(this).attr('n_historial');
    var cedula      =$(this).attr('cedula');
    var tipobeneficiario  =$(this).attr('tipo_beneficiario');
 
   //alert(id_consulta);
  window.location = '/Actualizar_Citas/'+cedula+'/'+tipobeneficiario+'/'+n_historial+'/'+id_especialidad+'/'+fecha_asistencia+'/'+id_historial_medico+'/'+id_consulta; 
});

$('#lista_de_categoria').on('click','.Imprimir', function(e)
{
     e.preventDefault
     var id_historial_medico=$(this).attr('id_historial_medico');
     let cedula =$(this).attr('cedula');
     let usuario=$('#usuario').val();  
     var n_historial =$(this).attr('n_historial');
     var tipobeneficiario  =$(this).attr('tipo_beneficiario');
     
     var fecha_asis=$(this).attr('fecha_asistencia'); 
     let fechaconvertida=moment(fecha_asis,'DD/MM/YYYY'); 
     fecha_asistencia=fechaconvertida.format('DD-MM-YYYY');
     
     let id_consulta=$(this).attr('id');
     
     let id_especialidad=$('#id_especialidad').val();
   
     if (id_especialidad=='2') {
          window.open('PDF_Medicina_Interna/'+cedula+'/'+n_historial+'/'+tipobeneficiario+'/'+usuario+'/'+fecha_asistencia+'/'+id_consulta+'/'+id_historial_medico+'/'+id_especialidad,'_blank');        
     }
     else if (id_especialidad=='5'){
          window.open('PDF_Psicologia_Primaria/'+cedula+'/'+n_historial+'/'+tipobeneficiario+'/'+usuario+'/'+fecha_asistencia+'/'+id_consulta+'/'+id_historial_medico+'/'+id_especialidad,'_blank');   
     }
    
});
