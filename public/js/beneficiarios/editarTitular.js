/*
 *Este es el document ready
 */
$(function() {

    $(".motivo_estatus").hide();
    $("#modal_editar").modal("show");
    $('#observacion').val('');
    var tipodesangre_id = $('#tipodesangre_id').val();
    var estadocivil_id = $('#estadocivil_id').val();
    var grado_intruccion_id = $('#grado_intruccion_id').val();
    var ocupacion_id = $('#ocupacion_id').val();
    var ubicacion_id = $('#ubicacion').val();
    var estatus_trab = $('#borrado').val();
    if (estatus_trab == 'ACTIVO') {
        $('#estatus_tra').attr('checked', 'checked');
        $('#estatus_tra').val('true');
        $('#borrado_anterior').val('false');
    }
    if (estatus_trab == 'BLOQUEADO') {
        $('#estatus_tra').removeAttr('checked')
        $('#estatus_tra').val('false')
        $('#borrado_anterior').val('true');
    }

    llenar_combo_tipo_de_sangre(Event, tipodesangre_id)
    llenar_combo_estado_civil(Event, estadocivil_id)
    llenar_combo_Grado_Instruccion(Event, grado_intruccion_id)
    llenar_combo_Ocupacion(Event, ocupacion_id)


    let ubi_anterior = $('#ubicacion_administrativa').val();
    llenar_ubicacion_admin(Event, ubicacion_id, ubi_anterior)

    //******METODO QUE TOMA EL ESTATUS ORIGINAL DEL CHECKBO*****
    if ($('#estatus_tra').is(':checked')) {
        estatus = 'false';
        $('#estatus_actual').val(estatus);

        $('#estatus_anterior').val(estatus);
    } else {
        estatus = 'true';
        $('#estatus_actual').val(estatus);
        $('#estatus_anterior').val(estatus);
    }


});


//  *Metodo para llenar el combo tipo ocupacion
//  */
function llenar_ubicacion_admin(e, ubicacion_id, ubi_anterior) {
    e.preventDefault;


    url = '/listar_ubicaciones';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length >= 1) {
                $('#ubicacion').empty();
                $('#ubicacion').append('<option value=0 selected disabled>' + ubi_anterior);
                $('#ubicacion_anterior').append('<option value=0 selected disabled>' + ubi_anterior);
                if (ubicacion_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#ubicacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === ubicacion_id) {
                            $('#ubicacion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            
                        } else {
                            $('#ubicacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


$(document).on('change', '#ubicacion ', function(e) {
    let nombre_ubicacion = $('#ubicacion  option:selected').text();
    $('#ubicacion_administrativa').val(nombre_ubicacion);

});


//  *Metodo para llenar el combo tipo ocupacion
//  */
function llenar_combo_Ocupacion(e, ocupacion_id) {
    e.preventDefault;

    url = '/listar_Ocupacion';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length >= 1) {
                $('#ocupacion').empty();
                $('#ocupacion').append('<option value=0>Seleccione</option>');
                if (ocupacion_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#ocupacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                       
                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === ocupacion_id) {
                            $('#ocupacion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            $('#ocupacion_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#ocupacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


/*
 *Metodo para llenar el combo tipo de sangre
 */
function llenar_combo_Grado_Instruccion(e, grado_intruccion_id) {
    e.preventDefault;

    url = '/listar_Grado_Instruccion';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length >= 1) {
                $('#gradointruccion').empty();
                $('#gradointruccion').append('<option value=0>Seleccione</option>');
                if (grado_intruccion_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#gradointruccion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === grado_intruccion_id) {
                            $('#gradointruccion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            $('#gradointruccion_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');                     
                        } else {
                            $('#gradointruccion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                            
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

/*
 *Metodo para llenar el combo tipo de sangre
 */
function llenar_combo_tipo_de_sangre(e, tipodesangre_id) {

    e.preventDefault;

    url = '/listar_tipo_de_sangre';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length >= 1) {
                $('#tipodesangre').empty();
                $('#tipodesangre').append('<option value=0>Seleccione</option>');
                if (tipodesangre_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#tipodesangre').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {

                    $.each(data, function(i, item)

                        {
                            if (item.id === tipodesangre_id) {

                                $('#tipodesangre').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                                $('#tipodesangre_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                                
                          
                          
                            } else {
                                $('#tipodesangre').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                            }
                        });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

/*
 *Metodo para llenar el combo estado civil
 */
function llenar_combo_estado_civil(e, estadocivil_id) {


    e.preventDefault;

    url = '/listar_estado_civil';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $('#estadocivil').empty();
                $('#estadocivil').append('<option value=0>Seleccione</option>');
                if (estadocivil_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#estadocivil').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        $('#estadocivil_anterior').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === estadocivil_id) {
                            $('#estadocivil').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            $('#estadocivil_anterior').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#estadocivil').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                            $('#estadocivil_anterior').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}





$(document).on('click', '#btnRegresar ', function(e) {
    e.preventDefault();
    window.location = '/vistamedicamentos/';

});



$(document).on('click', '#btnCerrar1 ', function(e) {
    e.preventDefault();
    window.location = '/titulares/';

});

//******METODO QUE TOMA EL ESTATUS ACTUAL DEL CHECKBO*****
$('#estatus_tra').click(function() {

    $(".motivo_estatus").show();
    if ($('#estatus_tra').is(':checked')) {

        estatus_check = 'false';

    } else {
        estatus_check = 'true';

    }
    $('#estatus_actual').val(estatus_check);
});



// ************************METODO PARA ACTULIZAR************************
$(document).on('click', '#btnActualizar ', function(e) {

    e.preventDefault();
    var cedula_trabajador = $('#cedula_trabajador').val();
    var nombre = $('#nombre').val();
    var telefono = $('#telefono').val();
    var sexo = $('#sexo').val();
    var fecha_nacimiento_ori = $('#fecha').val();
    let fechaconvertida = moment(fecha_nacimiento_ori, 'DD/MM/YYYY');
    fecha_nacimiento = fechaconvertida.format('YYYY-MM-DD');
    var ubicacion_administrativa = $('#ubicacion_administrativa').val();
    var tipo_de_personal = $('#tipo_de_personal').val();
    var nacionalidad = $('#nacionalidad').val();
    var tipodesangre_id = $('#tipodesangre').val();
    var estadocivil_id = $('#estadocivil').val();
    var grado_intruccion_id = $('#gradointruccion').val();
    var ocupacion_id = $('#ocupacion').val();
    let observacion = $('#observacion').val();
    let estatus_modificado = $('#estatus_actual').val();
    let estatus_anterior = $('#estatus_anterior').val();
    let id_usuario = $('#id_user').val();
   
    
    //********METODO QUE VERIFICA SI SE HISO UN CAMBIO**** 
    if (estatus_anterior != estatus_modificado) {
        bandera_Modificacion = 'true'
    } else {
        bandera_Modificacion = 'false'

    }
    // ***************************************************** 

    //********CAMBIA EL ESTATUS DEL TRABAJADOR Y SU BENEFICIARIOS**** 
    if (estatus_modificado == 'false') {
        estatus_trabajador = 'false';
        estatus_beneficiario = 'false';
    } else {
        estatus_trabajador = 'true';
        estatus_beneficiario = 'true';
    }

    if (bandera_Modificacion == 'true' && observacion == '') {
        alert('DEBE INDICAR EL MOTIVO DEL CAMBIO DE ESTATUS');
    } else {
        var objeto_anterior = {
            "Nacionalidad": $('#nacionalidad_anterior option:selected').text(),
            "EstadoCivil": $('#estadocivil_anterior option:selected').text(),
            "GradoIntruccion": $('#gradointruccion_anterior option:selected').text(),
            "TipoPersonal": $('#tipo_de_personal_anterior').val(),
            "Ubicacion": $('#ubicacion_anterior option:selected').text(),
            "Inactivo": $('#borrado_anterior').val(),
            "Ocupacion": $('#ocupacion_anterior option:selected').text(),
            "TipoSangre": $('#tipodesangre_anterior option:selected').text(),
            "Sexo": $('#sexo_anterior option:selected').text(),
            "Telefono": $('#telefono_anterior').val(),
            "FechaNacimiento": $('#fecha_anterior').val(),
            
            
        }
        var objeto_actual= {
            "Nacionalidad": $('#nacionalidad option:selected').text(),
            "EstadoCivil": $('#estadocivil option:selected').text(),
            "GradoIntruccion": $('#gradointruccion option:selected').text(),
            "TipoPersonal": $('#tipo_de_personal').val(),
            "Ubicacion": $('#ubicacion option:selected').text(),
            "Inactivo": $('#estatus_actual').val(),
            "Ocupacion": $('#ocupacion option:selected').text(),
            "TipoSangre": $('#tipodesangre option:selected').text(),
            "Sexo": $('#sexo option:selected').text(),
            "Telefono": $('#telefono').val(),
            "FechaNacimiento": $('#fecha').val(),
        }
        var camposModificados = [];
        for (var propiedad in objeto_anterior) {
            if (objeto_anterior.hasOwnProperty(propiedad)) {
                if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                    camposModificados.push({
                        propiedad: propiedad,
                        valorAnterior: objeto_anterior[propiedad],
                        valorNuevo: objeto_actual[propiedad]
                    });
                }
            }
        }
        if (camposModificados.length > 0) {
            var datos_modificados = camposModificados.map(function(campo) {
                let string_anterior = campo.valorAnterior;
                let patron = /option-/;
                if (patron.test(string_anterior)) {
                    campo.valorAnterior = string_anterior.replace(patron, "");
                }
                let string_Actual = campo.valorNuevo;
                let patron_Actual = /option-/;
                if (patron.test(string_Actual)) {
                    campo.valorNuevo = string_Actual.replace(patron, "");
                }
                campo.valorAnterior = campo.valorAnterior.trim();

                return campo.valorAnterior === '' ?
                    `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                    `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
            }).join(", ");

        }
        if (datos_modificados == undefined) {
            alert('NO SE HA REALIZADO NINGUNA MODIFICACION');

        } else 
        {
            var data = {
                    datos_modificados:datos_modificados,
                    nombre:nombre,
                    cedula_trabajador: cedula_trabajador,
                    telefono: telefono,
                    sexo: sexo,
                    fecha_nacimiento: fecha_nacimiento,
                    ubicacion_administrativa: ubicacion_administrativa,
                    tipo_de_personal: tipo_de_personal,
                    nacionalidad: nacionalidad,
                    tipodesangre_id: tipodesangre_id,
                    estadocivil_id: estadocivil_id,
                    grado_intruccion_id: grado_intruccion_id,
                    ocupacion_id: ocupacion_id,
                    estatus_trabajador: estatus_trabajador,
                    estatus_beneficiario: estatus_beneficiario,
                    estatus_modificado: estatus_modificado,
                    bandera_Modificacion: bandera_Modificacion,
                    observacion: observacion,
                    id_usuario: id_usuario
                }
                // console.log(data);

            var url = '/actualizar_titular';
            $.ajax({
                url: url,
                method: 'POST',
                data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
                dataType: 'JSON',
                beforeSend: function(data) {
                    //alert('Procesando Información ...');
                },
                success: function(data) {
                    //alert(data);
                    if (data === 1) {
                        alert('Registro Actualizado');
                    } else {
                        alert('Error en la Incorporación del registro');
                    }
                    window.location = '/titulares';
                },
                error: function(xhr, status, errorThrown) {
                    alert(xhr.status);
                    alert(errorThrown);
                }
            });
        }
 }
});