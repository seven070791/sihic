
$(function()
{
    //document.getElementById("hasta").disabled=true; 
    let desde          =$('#desde').val();
    let hasta          =$('#hasta').val();
    base_url+
    let id_medico         =$('#cmbmedicos').val();
    let cedula_trabajador  =$('#cedula_cortesia').val(); 
   
    if (desde==''&&hasta=='')
    {
    
        desde='null'
        hasta='null'
         
    }

    listar_info_medicamentos(desde,hasta,id_medico);
    listar_medico(Event,cedula_trabajador);
  
});


function  listar_medico(e,cedula_trabajador)
 {
     e.preventDefault;
       //url='/listar_salidas_medicos';
       url='/listar_salidas_medicos/'+cedula_trabajador
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {          
 if(data.length>=1)
 {
      $('#cmbmedicos').empty();
      $('#cmbmedicos').append('<option value=0>Seleccione</option>');     
     
          
          $.each(data, function(i, item)
          {        
               $('#cmbmedicos').append('<option value='+item.id_medico+'>'+item.nombre+'</option>');
          });
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 function  listar_info_medicamentos(desde,hasta,id_medico)
 {
    //var url="/getAll_infomedicamento_titulares/"+$('#cedula_trabajador').val();
    let ruta_imagen =rootpath
    let fecha          =$('#fecha').val(); 
    let nombre_beneficiario         =$('#nombre_beneficiario').val();
     
    var table = $('#info_medicamento').DataTable( {
       
        
         dom: "Bfrtip",
         buttons:{
             dom: {
                 button: {
                     className: 'btn-xs'
                 },
                 
             },
             
             buttons: [
             {
                 //definimos estilos del boton de pdf
                 extend: "pdf",
                 text:'PDF',
                 className:'btn-xs btn-dark',
                 title:'Medicamentos Entregados al '+' '+fecha,
                 //messageTop: 'Fecha :'+' '+fecha,
                 messageTop: 'BENEFICIARIO :'+' '+nombre_beneficiario+' '+'(CORTESIA)',
                 
                 download: 'open',
                 exportOptions: {
                             columns: [ 0,1,2,3,4],
                             
                 },   
                 alignment: 'center',
                 
                 customize:function(doc) {

                   doc.styles.title = {
                       color: '#4c8aa0',
                       fontSize: '16',
                    alignment: 'center'                     
                      
                   },
              
                   doc.styles['td:nth-child(2)'] = {
                       width: '130px',
                       'max-width': '130px'
                   },
                   doc.styles.tableHeader = {
                       fillColor:'#4c8aa0',
                       color:'white',
                       alignment:'center',
                      
                   },
                   doc.content[1].margin = [ 18, -2, 80, 10 ]
                   doc.content[2].margin = [ 20, -2, -25, 0 ]
                   doc.content.splice(0, 0, {
                        margin: [ 0, 0, 0, 12 ],
                        alignment: 'center',
                        image:ruta_imagen,
                        width: 500,
                        title:'Medicamentos Entregados2',
                    } );
           },
             },

             {    
                  //definimos estilos del boton de excel
                  extend: "excel",
                  text:'Excel',
                  className:'btn-xs btn-dark',
                  title:'Medicamentos Entregados', 
                  messageTop: function () {
                    return 'BENEFICIARIO: '+' '+nombre_beneficiario+' '+'FECHA: '+' '+fecha+' '+'(CORTESIA)';
                    },
                  download: 'open',
                  exportOptions: {
                              columns: [ 0,1,2,3,4]
                  },
                  excelStyles: {                                                
                    "template": [
                        "blue_medium",
                        "header_blue",
                        "title_medium"
                    ]                                  
                },

             }
             ]            
         }, 
        

         "order":[[0,"desc"]],					
         "paging": true,
         "lengthChange": true,
        
         dom: 'Blfrtip',
         "searching": true,
         "lengthMenu": [
              [ 10, 25, 50, -1 ],
              [ '10', '25', '50', 'Todos' ]
         ], 
        
         "ordering": true,
         "info": true,
         "autoWidth": true,
         //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
         "ajax":
         {
            "url":base_url+"/getAll_infomedicamento_cortesia/"+$('#cedula_cortesia').val()+'/'+desde+'/'+hasta+'/'+id_medico,
          "type":"GET",
          dataSrc:''
         },
         "columns":
         [
             
            {data:'descripcion'}, 
            {data:'med_cronico'},
            {data:'cantidad'},   
            {data:'control'},      
            {data:'fecha_salida'},
            {data:'nombre_medico'},
            
           
         ],
         
   "language":
       {
           "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
              {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
              },
          ]
       
    },
});
 }
  



 $(document).on('click','#btnfiltrar ', function(e)
 {
     e.preventDefault();
     var desde           =$('#desde').val();
     var hasta          =$('#hasta').val();
     var id_medico =$('#cmbmedicos').val();
     if (desde=='') 
     {
         desde='null' 
     }
      if (hasta=='') 
     {
         hasta='null' 
     }
     $("#info_medicamento").dataTable().fnDestroy();
     listar_info_medicamentos(desde,hasta,id_medico); 
     
     if (desde=='null'&&hasta!='null') 
     {
         alert('DEDE INDICAR EL CAMPO DESDE');
         
     }
     else if (hasta=='null'&&desde!='null') 
     {
         alert('DEDE INDICAR EL CAMPO HASTA');
     } 
     else if (hasta<desde) {
       alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
     }
     
 });
 $(document).on('click','#btnlimpiar', function(e)
 {
   e.preventDefault();
   $('input[type="date"]').val('');
   let desde          =$('#desde').val();
   let hasta          =$('#hasta').val();
   let cedula_trabajador  =$('#cedula_cortesia').val();  
   if (desde==''&&hasta=='')
   {  
       desde='null'
       hasta='null'
        
   }  
     $('#cmbmedicos').empty();
     let id_medico         =$('#cmbmedicos').val(); 
     $("#info_medicamento").dataTable().fnDestroy();
     listar_medico(Event,cedula_trabajador);
     listar_info_medicamentos(desde,hasta,id_medico);
       
 });



    $(document).on('click','#btnRegresar_hacia_titulares ', function(e)
    {
        e.preventDefault();
        window.location = '/titulares/';
        
    });

    $(document).on('click','#btnAgregar ', function(e)
    {
        e.preventDefault();
        window.location = '/vistamedicamentos/';
        
    });

    