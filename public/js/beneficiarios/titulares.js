/*
 *Este es el document ready
 */

$(function() {
  
    listartitulares();

    $("#cajas").hide();
    $("#btnCerrar").hide();
    $("#btnAgregarusuario").hide();



    //alert($('#id_medicamento').val());
});




function listartitulares() {
   
    base_url
    $('#table_titulares').DataTable({
        "order": [
            [0, "asc"]
        ],
        "paging": true,
        "info": true,
        "filter": true,
        "responsive": true,
        "autoWidth": true,
        //"stateSave":true,
        "ajax": {
            "url": base_url+"/listar_titulares",
            "type": "GET",
            dataSrc: ''
        },
        "columns": [

            { data: 'cedula_trabajador' },
            { data: 'nombre' },
            { data: 'apellido' },
            //{data:'tipo_de_personal'},
            { data: 'ubicacion_administrativa' },
            { data: 'sexo' },
            { data: 'telefono' },
            { data: 'fecha_nacimiento' },
            {
                orderable: true,
                render: function(data, type, row) {

                    if (row.borrado == 'INACTIVO') {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary  Editar" style=" font-size:2px" data-toggle="tooltip" title="Editar" ocupacion_id=' + row.ocupacion_id + ' grado_intruccion_id=' + row.grado_intruccion_id + '  cedula_trabajador=' + row.cedula_trabajador + ' descripcion="' + row.descripcion + '" fecha_creacion=' + row.fecha_creacion + ' estatus=' + row.estatus + ' estadocivil_id=' + row.estado_civil + ' tipodesangre_id=' + row.estado_civil + ' > <i class="material-icons " >create</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-success  disabled="disabled" style="opacity: 0.4;font-size:4px" Familiares"  data-toggle="tooltip" title="Familiares" cedula_trabajador=' + row.cedula_trabajador + ' nombre=' + row.nombre + ' estatus="' + row.estatus + '"><i class="material-icons ">wc</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-secondary  disabled="disabled" Cortesia" style="opacity: 0.4;font-size:4px" data-toggle="tooltip" title="Cortesia" cedula_trabajador=' + row.cedula_trabajador + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">group_add</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-info disabled="disabled" Detalle" style="opacity: 0.4;font-size:4px" data-toggle="tooltip" title="Historico de  Medicamentos Entregados" cedula_trabajador=' + row.cedula_trabajador + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">add_to_photos</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-primary disabled="disabled" Historial" style="opacity: 0.4;font-size:4px" data-toggle="tooltip" title="Historial  Medico" cedula_trabajador=' + row.cedula_trabajador + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">create_new_folder</i></a>'
                            // style=" opacity: 0.4;"  
                    } else {
                        return '<a href="javascript:;" class="btn btn-xs btn-primary Editar" style=" font-size:2px" data-toggle="tooltip" title="Editar" ocupacion_id=' + row.ocupacion_id + ' grado_intruccion_id=' + row.grado_intruccion_id + '  cedula_trabajador=' + row.cedula_trabajador + ' descripcion="' + row.descripcion + '" fecha_creacion=' + row.fecha_creacion + ' estatus=' + row.estatus + ' estadocivil_id=' + row.estado_civil + ' tipodesangre_id=' + row.estado_civil + ' > <i class="material-icons " >create</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-success Familiares" style=" font-size:4px" data-toggle="tooltip" title="Familiares" cedula_trabajador=' + row.cedula_trabajador + ' nombre=' + row.nombre + ' estatus="' + row.estatus + '"><i class="material-icons ">wc</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-secondary Cortesia" style=" font-size:2px" data-toggle="tooltip" title="Cortesia" cedula_trabajador=' + row.cedula_trabajador + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">group_add</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-info Detalle" style=" font-size:2px" data-toggle="tooltip" title="Historico de  Medicamentos Entregados" cedula_trabajador=' + row.cedula_trabajador + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">add_to_photos</i></a>' + ' ' + '<a href="javascript:;" class="btn btn-xs btn-primary Historial" style=" font-size:2px" data-toggle="tooltip" title="Historial  Medico" cedula_trabajador=' + row.cedula_trabajador + ' rol=' + row.descripcion + ' estatus="' + row.estatus + '"><i class="material-icons ">create_new_folder</i></a>'
                    }


                }
            }
        ],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }],
        }
    });
}

$('#lista_titulares').on('click', '.Familiares', function(e) {

    var cedula_trabajador = $(this).attr('cedula_trabajador');
    window.location = '/listar_Familiares/' + cedula_trabajador;


});

$('#lista_titulares').on('click', '.Cortesia', function(e) {

    var cedula_trabajador = $(this).attr('cedula_trabajador');
    //alert(cedula_trabajador);
    window.location = '/listar_Cortesia/' + cedula_trabajador;


});
$('#lista_titulares').on('click', '.Detalle', function(e) {

    var cedula_trabajador = $(this).attr('cedula_trabajador');
    //alert(cedula_trabajador);
    window.location = '/info_medicamentos_titulares/' + cedula_trabajador;


});


/*
 * Función para definir datatable:
 */

$('#btnAgregar').on('click', function(e) {

    $('#modal').find('#btnGuardar').hide();
    $('#modal').find('#btnActualizar').hide();
    $('#modal').find('#borrado').hide();
    $('#modal').find('#activo').hide();
    $("#modal").modal("show");

    $("#cajas").hide();
    $("#btnCerrar").hide();
    $("#btnAgregarusuario").hide();

    $(".form-login_imagen").hide();

    // $(".datos").css("disabled", "disabled"); 

    //deshabilitar elementos de un div
    //$(".datos").children().prop('readonly', readonly);

});


$('#btnBuscar').on('click', function(e) {
    e.preventDefault();
    $("#cajas").show();
    $("#btnCerrar0").hide();
    $("#btnCerrar").show();
    $("#btnGuardar").show();
    llenar_combo_tipo_de_sangre(Event)
    llenar_combo_estado_civil(Event)
    llenar_combo_Grado_Instruccion(Event)
    llenar_combo_Ocupacion(Event)
    var cedula = $('#cedula').val();

    if (cedula == '') {
        alert('Debe Introducir la Cedula');
    }
    var sigespKey = '$2y$12$hhlBQA2SKFyl1XABdBaNg.LSOuWoI9Sz6Amu3UFS/evwksYNCYN7G';
    //var url='http://localhost/api/empleado/datos.php?cedula='+cedula+'&key='+sigespKey;
    var url = '/api/empleado/datos.php?cedula=' + cedula + '&key=' + sigespKey;
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        contentType: "application/json",
        beforeSend: function() {
            //alert('Antes');
        },
        success: function(data) {


            if (data.cedula == null) {
                alert('No existen registros coincidentes para el Nùmero de Cèdula suministrado', '#intCedula');
                $('#intCedula').focus();
            } else if (data.cedula == '000') {
                alert('Error: ' + data.cedula);
                alert('No Existen Registros ...');
            } else if (data.cedula == 555) {
                alert('Error: ' + data.cedula);
                alert('Problemas con la BD del Servicio o con la conexión a ella ...');
            } else if (data.cedula == 777) {
                alert('Error: ' + data.cedula);
                alert('Problemas de Conexión ...');
            } else if (data.cedula == 888) {
                alert('Error: ' + data.cedula);
                alert('Las claves no son iguales ...');
            } else if (data.cedula == 999) {
                alert('Error: ' + data.cedula);
                alert('No se Recibió clave alguna para la API ...');
            } else {

                $('#nombre').val(data.nombres);
                $('#apellido').val(data.apellidos);
                $('#ubicacionadministrativa').val(data.ubicacion);
                $("#tipo_de_personal").val(data.cargo);
                $("#fecha").val(data.fecha_nacimiento);
                $("#nacionalidad").val(data.nacper);

            }

        }
    });
});



$(document).on('click', '#btnGuardar', function(e) {
    e.preventDefault();
    var cedula = $('#cedula').val();
    var nombre = $('#nombre').val();
    var apellido = $('#apellido').val();
    var tipo_de_personal = $('#tipo_de_personal').val();
    var ubicacionadministrativa = $('#ubicacionadministrativa').val();
    var telefono = $('#telefono').val();
    var sexo = $('#sexo').val();
    var fecha_nacimiento = $('#fecha').val();
    var nacionalidad = $('#nacionalidad').val();
    var tipodesangre = $('#tipodesangre').val();
    var estadocivil = $('#estadocivil').val();
    var gradoinstruccion_id = $('#gradointruccion').val();
    var ocupacion_id = $('#ocupacion').val();
    var url = '/agregar_titulares';
    var ruta_regreso = '/titulares';
    var data = {
        cedula: cedula,
        nombre: nombre,
        apellido: apellido,
        tipo_de_personal: tipo_de_personal,
        ubicacionadministrativa: ubicacionadministrativa,
        telefono: telefono,
        sexo: sexo,
        fecha_nacimiento: fecha_nacimiento,
        nacionalidad: nacionalidad,
        tipodesangre: tipodesangre,
        estadocivil: estadocivil,
        gradoinstruccion_id: gradoinstruccion_id,
        ocupacion_id: ocupacion_id,
    }


    $.ajax({
        url: url,
        method: 'POST',
        data: { data: btoa(unescape(encodeURIComponent(JSON.stringify(data)))) },
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data == '2') {
                alert('Error!!! Usuario se Encuentra Registrado');
            } else if (data == '1') {
                alert('Registro Incorporado ');


                window.location = ruta_regreso;
            } else if (data == '0') {
                alert('Error en la Incorporación del Registro');
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
});

/*
 *Metodo para llenar el combo tipo de sangre
 */
function llenar_combo_tipo_de_sangre(e, tipodesangre_id) {

    e.preventDefault;

    url = '/listar_tipo_de_sangre';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(JSON.stringify(data))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length > 1) {
                $('#tipodesangre').empty();
                $('#tipodesangre').append('<option value=0>Seleccione</option>');
                if (tipodesangre_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#tipodesangre').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {

                    $.each(data, function(i, item)

                        {
                            if (item.id === tipodesangre_id) {

                                $('#tipodesangre').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                            } else {
                                $('#tipodesangre').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                            }
                        });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


/*
 *Metodo para llenar el combo estado civil
 */
function llenar_combo_estado_civil(e, estado_civil) {
    e.preventDefault;

    url = '/listar_estado_civil';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(JSON.stringify(data))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length > 1) {
                $('#estadocivil').empty();
                $('#estadocivil').append('<option value=0>Seleccione</option>');
                if (estado_civil === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#estadocivil').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === estado_civil) {
                            $('#estadocivil').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#estadocivil').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


$('#lista_titulares').on('click', '.Editar', function(e) {

    var cedula_trabajador = $(this).attr('cedula_trabajador');
    var tipodesangre_id = $(this).attr('tipodesangre_id');
    var estadocivl_id = $(this).attr('estadocivil_id');
    var grado_intruccion_id = $(this).attr('grado_intruccion_id');
    var ocupacion_id = $(this).attr('ocupacion_id');

    // alert(typeof(grado_intruccion_id));

    if (grado_intruccion_id === 'null') {
        grado_intruccion_id = 0;
    }
    if (ocupacion_id === 'null') {
        ocupacion_id = 0;
    }



    window.location = '/editar_titular/' + cedula_trabajador + '/' + tipodesangre_id + '/' + estadocivl_id + '/' + grado_intruccion_id + '/' + ocupacion_id + '/';
});

$('#lista_titulares').on('click', '.Historial', function(e) {

    var cedula_trabajador = $(this).attr('cedula_trabajador');



    window.location = '/datos_titular/' + cedula_trabajador;
});





$(document).on('click', '#btnRegresar ', function(e) {
    e.preventDefault();
    window.location = '/vistamedicamentos/';

});



$(document).on('click', '#btnCerrar1 ', function(e) {
    e.preventDefault();
    window.location = '/titulares/';

});


/*
 *Metodo para llenar el combo tipo de sangre
 */
function llenar_combo_Grado_Instruccion(e, grado_intruccion_id) {
    e.preventDefault;

    url = '/listar_Grado_Instruccion';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(JSON.stringify(data))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length > 1) {
                $('#gradointruccion').empty();
                $('#gradointruccion').append('<option value=0>Seleccione</option>');
                if (grado_intruccion_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#gradointruccion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === grado_intruccion_id) {
                            $('#gradointruccion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#gradointruccion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}


/*
 /*
 *Metodo para llenar el combo ocupacion
 */
function llenar_combo_Ocupacion(e, ocupacion_id) {
    e.preventDefault;

    url = '/listar_Ocupacion';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(JSON.stringify(data))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            //console.log(data);
            // alert(data[0].id)
            //alert(data[0].descripcion)

            if (data.length > 1) {
                $('#ocupacion').empty();
                $('#ocupacion').append('<option value=0>Seleccione</option>');
                if (ocupacion_id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#ocupacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {
                        if (item.id === ocupacion_id) {
                            $('#ocupacion').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#ocupacion').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}