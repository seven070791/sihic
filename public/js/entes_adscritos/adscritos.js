/*
 *Este es el document ready
 */
 $(function()
 {
     //alert('hola tu ');
     listarEntesAdscritos();
     //llenar_combo_TipoInventario(Event);
 });
 /*
  * Función para definir datatable:
  */
 function listarEntesAdscritos()
 {
      $('#table_entes_adscritos').DataTable
      (
       {
            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_entes_adscritos",
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
               {data:'id'},
               {data:'rif'},
               {data:'descripcion'},                 
               {data:'estatus'},
               {orderable: true,
                 render:function(data, type, row)
                 {
                  return '<a href="javascript:;" class="btn btn-xs btn-secondary editar" style=" font-size:1px" data-toggle="tooltip" title="Editar" id='+row.id+' rif='+row.rif+'   descripcion="'+row.descripcion+'" fecha_creacion='+row.fecha_creacion+' estatus='+row.estatus+' > <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
      $('#modalagregar').find('#btnGuardar').show();
      $('#modalagregar').find('#btnActualizar').hide();
      $('#modalagregar').find('#borrado').hide();
      $('#modalagregar').find('#activo').hide();
      $("#modalagregar").modal("show");
     

 });



 $('#lista_entes_adscritos').on('click','.editar', function(e)
 {
     var id            =$(this).attr('id');
     var rif      =$(this).attr('rif');
     var descripcion   =$(this).attr('descripcion');
     var fecha_creacion=$(this).attr('fecha_creacion');
     var estatus       =$(this).attr('estatus');
     $('#id').val(id);
     $("#modalagregar").modal("show");
     $('#modalagregar').find('#btnGuardar').hide();
     $('#modalagregar').find('#btnActualizar').show();
     $('#modalagregar').find('#borrado').show();
     $('#modalagregar').find('#activo').show();   
     $('#id').val(id);
     $('#rif').val(rif)
     $('#descripcion').val(descripcion)

     $('#rif_anterior').val(rif)
     $('#descripcion_anterior').val(descripcion)
     $('#fecha').val(fecha_creacion)
     if(estatus=='Activo')
     {
          $('#borrado').attr('checked','checked');
          $('#borrado').val('false');
          $('#borrado_anterior').val('false')
     }
     if(estatus=='Eliminado')
     {
           $('#borrado').removeAttr('checked')
          $('#borrado').val('true')
          $('#borrado_anterior').val('true')
     }
            
         
 });
 


 $(document).on('click','#btnGuardar', function(e)
 {
     e.preventDefault();
     var rif=$('#rif').val();
     var descripcion =$('#descripcion').val();  
     if (rif=='')
     {
          alert('DEBE INGRESAR EL RIF');
     } 
     else if(descripcion=='')
     {
          alert('DEBE INGRESAR EL NOMBRE DEL ENTE');
     }
     else
     {  
          var fechaRegistro=$('#fecha').val();
          var url='/agregarEnteAdscrito';
          var data=
          {
               rif :rif,
               descripcion:descripcion,
               fechaRegistro  :fechaRegistro,
          }
          $.ajax
          ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
               },
               success:function(data)
               {
                    //alert(data);
                    if(data=='1')
                    {
                         alert('Registro Incorporado');
                         window.location = '/entes_adscritos';
                    }
                    else
                    {
                         alert('Error en la Incorporación del Registro');
                    }
               },
               error:function(xhr, status, errorThrown)
               {
                    alert(xhr.status);
                    alert(errorThrown);
               }
          });
     }
});

$(document).on('click','#btnActualizar', function(e)
{
     e.preventDefault();
     var id=$('#id').val();
     var rif=$('#rif').val();
     var descripcion=$('#descripcion').val();
     var fechaRegistro  =$('#fecha').val();
     var borrado='false';
     let descripcion_anterior=$('#descripcion_anterior ').val();
     if (rif=='')
     {
          alert('DEBE INGRESAR EL RIF');
     } 
     else if(descripcion=='')
     {
          alert('DEBE INGRESAR EL NOMBRE DEL ENTE');
     }
     else
     {  

     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }
     var objeto_anterior = {
          "Rif": $('#rif_anterior ').val(),
          "Ente_Adscrito": $('#descripcion_anterior ').val(),
          "Bloqueado": $('#borrado_anterior ').val(),
              
      }
      var objeto_actual= {
          "Rif": $('#rif ').val(),
          "Ente_Adscrito": $('#descripcion ').val(),
          "Bloqueado": borrado,
         
      }
      var camposModificados = [];
      for (var propiedad in objeto_anterior) {
          if (objeto_anterior.hasOwnProperty(propiedad)) {
              if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                  camposModificados.push({
                      propiedad: propiedad,
                      valorAnterior: objeto_anterior[propiedad],
                      valorNuevo: objeto_actual[propiedad]
                  });
              }
          }
      }
      if (camposModificados.length > 0) {
          var datos_modificados = camposModificados.map(function(campo) {
              let string_anterior = campo.valorAnterior;
              let patron = /option-/;
              if (patron.test(string_anterior)) {
                  campo.valorAnterior = string_anterior.replace(patron, "");
              }
              let string_Actual = campo.valorNuevo;
              let patron_Actual = /option-/;
              if (patron.test(string_Actual)) {
                  campo.valorNuevo = string_Actual.replace(patron, "");
              }
              campo.valorAnterior = campo.valorAnterior.trim();

              return campo.valorAnterior === '' ?
                  `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                  `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
          }).join(", ");
     }
     if (datos_modificados == undefined) {
          alert('NO SE HA REALIZADO NINGUNA MODIFICACION');

      } else {
          var data=
          {
               id             :id,
               rif:rif,
               descripcion:descripcion,
               fechaRegistro  :fechaRegistro,
               borrado        :borrado,
               datos_modificados:datos_modificados,
               descripcion_anterior:descripcion_anterior
               
          }
     
         var url='/actualizarEnte_Adscrito';
         $.ajax
         ({
               url:url,
               method:'POST',
               data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
               dataType:'JSON',
               beforeSend:function(data)
               {
                    //alert('Procesando Información ...');
               },
               success:function(data)
               {
                  //alert(data);
                  if(data===1)
                  {
                    alert('Registro Actualizado');
                  }
                  else
                  {
                    alert('Error en la Incorporación del registro');
                  }
                  window.location = '/entes_adscritos';             
               },
               error:function(xhr, status, errorThrown)
               {
                  alert(xhr.status);
                  alert(errorThrown);
               }
          });
      }
}
 });