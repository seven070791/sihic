/*
 *Este es el document ready
 */
 $(function()
 {

     let desde          =$('#desde').val();
     let hasta          =$('#hasta').val();
     if (desde==''&&hasta=='')
     {
     
         desde='null'
         hasta='null'     
     } 
   
     let id_medico   =$('#cmbmedicos').val();
     $('#medico').val('0')

     $('#especialidad').val('0')
     let especialidad=0
     
     let asistio='Seleccione'
     let control='Seleccione'   
     nombre_medico='Seleccione'
     nombre_especialidad='Seleccione' 
     nombre_asistio='Seleccione' 
     medico=0;
    // nombre_medico='Seleccione'
  // llenar_combo_medicos(Event);
  

   listar_citas_consultas(desde,hasta,medico,especialidad,control,asistio, nombre_medico='Seleccione',nombre_especialidad='Seleccione',nombre_asistio='Seleccione');
   llenar_combo_especialidad(Event)
   llenar_combo_MedicoTratante(Event)
   document.getElementById("medico").disabled=true;

 });


 
function listar_citas_consultas(desde=null,hasta=null,medico=0,especialidad=null,control=0,asistio=0,nombre_medico='null',nombre_especialidad='Seleccione',nombre_asistio='Seleccione')
{
   
     let now = new Date();
     let day = ("0" + now.getDate()).slice(-2);
     let month = ("0" + (now.getMonth() + 1)).slice(-2);
     let today= day+"-"+month+"-"+now.getFullYear(); 
//      data=new Date(desde);
//      let dataFormatada_desde = ((data.getDate() + 1 )) + "-" + ((data.getMonth() + 1)) + "-" + data.getFullYear(); 
//      data=new Date(hasta);
//      let dataFormatada_hasta = ((data.getDate() + 1 )) + "-" + ((data.getMonth() + 1)) + "-" + data.getFullYear(); 
    
   
     let fecha_desde = desde;
     let partesFecha = fecha_desde.split("-");
     let dataFormatada_desde = partesFecha[2] + "-" + partesFecha[1] + "-" + partesFecha[0];
     

     let fecha_hasta = hasta;
     let partesFecha_ = fecha_hasta.split("-");
     let dataFormatada_hasta = partesFecha_[2] + "-" + partesFecha_[1] + "-" + partesFecha_[0];
     



    let encabezado=''; 
     if(control!='0'&& control!='Seleccione')
     {
          encabezado=encabezado+'  Control:'+' '+' '+control+' '+' ';
     }

     if(dataFormatada_desde!='undefined-undefined-null' && dataFormatada_hasta!='undefined-undefined-null') 
     {
          encabezado= encabezado + 'Desde:'+' '+dataFormatada_desde+' '+' '+'hasta'+' '+' '+dataFormatada_hasta+' '+' ';
     }
     if(nombre_especialidad!='Seleccione') 
     {
          encabezado=encabezado+'Especialidad:'+' '+' '+nombre_especialidad+' '+' ';
     }

     if(nombre_medico!='Seleccione'&& nombre_medico!='Medicos'&& nombre_medico!='null')
     {
          encabezado=encabezado+'Medico:'+' '+' '+nombre_medico+' '+' ';
     }
      
     if(nombre_asistio!='Seleccione')
     {
          encabezado=encabezado+'Asitencia:'+' '+' '+nombre_asistio+' '+' ';
     }

     let ruta_imagen =rootpath
    let table = $('#table_consulta_citas').DataTable( {

          dom: "Bfrtip",
          buttons:{
              dom: {
                  button: {
                      className: 'btn-md'
                  },
                  
              },
              
              buttons: [
              {
                  //definimos estilos del boton de pdf
                  extend: "pdf",
                  text:'PDF',
                  className:'btn-xs btn-dark',
                  orientation: 'landscape',
                  pageSize: 'LETTER',
                 // title:'RELACION ENTRADAS MEDICAMENTOS',
                  header:true,
                  footer: true,
                  download: 'open',
                  exportOptions: {
                              columns: [ 0,1,2,3,4,5,6],
                              
                  },   
                  alignment: 'center',
                  
                  customize:function(doc)
                  {                       
                  //Remove the title created by datatTables
                       doc.content.splice(0,1);
                       doc.styles.title= 
                       { 
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'                     
                       }
                       doc.styles['td:nth-child(2)'] = 
                       {
                            width: '130px',
                            'max-width': '130px'
                       },
                       doc.styles.tableHeader = 
                       {
                            fillColor:'#4c8aa0',
                            color:'white',
                            alignment:'center'
                       }, 
             // Create a header
             doc.pageMargins = [10,95,0,70];
             doc['header'] = (function (page, pages)
             {                      
                  doc.styles.title = 
                  {    
                       color: '#4c8aa0',
                       fontSize: '18',
                       alignment: 'center',  
                  } 
                  return {  
                            columns:
                            [
                                 {                              
                                   margin: [ 170, 3, 40, 40 ],                                  
                                      image:ruta_imagen,
                                      width: 500,  
                                                               
                                 },                        
                                 {
                                      margin :[ -400, 50, -25, 0 ],
                                      color: '#4c8aa0',
                                      fontSize: '18',
                                      alignment: 'center' , 
                                      text: 'ATENCION DE BENEFICIARIOS',
                                      fontSize: 18,                             
                                 },
                                 {
                                 margin :[ -530, 80, -25, 0 ],     
                                 text:encabezado,
                                 },
                            ], 
                       }                               
              }); 
             // Create a footer
             doc['footer'] = (function (page, pages)
             {    
                  return {
                            columns:
                            [
                                 {
                                      alignment: 'center',
                                      text: ['pagina ', { text: page.toString() },	' of ',	{ text: pages.toString() }]
                                 }
                            ],               
                            }                            
                  });
    
                  },
             },
 
              {    
                   //definimos estilos del boton de excel
                   extend: "excel",
                   text:'Excel',
                   className:'btn-xs btn-dark',
                   title:'ATENCION DE BENEFICIARIOS', 
                  
                   download: 'open',
                   exportOptions: {
                     columns: [1,2,3,4,5,6],
                   },
                   excelStyles: {                                                
                     "template": [
                         "blue_medium",
                         "header_blue",
                         "title_medium"
                     ]                                  
                 },
 
              }
              ]            
          }, 
         
 
          "order":[[5,"desc"]],					
          "paging": true,
          "lengthChange": true,
         
          dom: 'Blfrtip',
          "searching": true,
          "lengthMenu": [
               [ 10, 25, 50, -1 ],
               [ '10', '25', '50', 'Todos' ]
          ], 
         
          "ordering": true,
          "info": true,
          "autoWidth": true,
          //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
          "ajax":
          {
            
             "url":base_url+"/listar_reporte_citas/"+desde+'/'+hasta+'/'+medico+'/'+especialidad+'/'+control+'/'+asistio+'/'+today,
           "type":"GET",
           dataSrc:''
          },
          "columns":
          [
              
           // {data:'id'}, 
           {data:'consulta'}, 
           {data:'n_historial'},  
           {data:'nombre'},   
           {data:'nombremedico'},
           {data:'especialidad'},
           {data:'format_fecha_asistencia'},
           {data:'controlasistencia'},
          
       
       {orderable: true,
            render:function(data, type, row)
            {
             if(row.consulta=='Consulta')
              {
                 return '<a href="javascript:; id="btnconfirmarcita" class=" btn btn-xs btn-primary  disabled"  style=" font-size:1px" data-toggle="tooltip" title="Confirmar"   controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >check</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  control="'+row.consulta+'" nombre="'+row.nombre+'" controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'">   <i class="material-icons  " > delete_forever</i></a>'
              }
            else 
              {

              return '<a href="javascript:; id="btnconfirmarcita" class=" btn btn-xs btn-primary  Confirmar" style=" font-size:1px" data-toggle="tooltip" title="Confirmar"   controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+' asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >check</i></a>'+' '+'<a href="javascript:;" class="btn btn-xs btn-secondary  Bloquear" style="font-size:1px" data-toggle="tooltip" title="Reversar"  control="'+row.consulta+'" nombre="'+row.nombre+'" controlasistencia='+row.controlasistencia+'   id_consulta='+row.id_consulta+'  asistencia='+row.asistencia+'   fecha_asistencia='+row.fecha_asistencia+' borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'">   <i class="material-icons  " > delete_forever</i></a>'
              } 
           
            }
       },

 
            
          ],
          
    "language":
        {
            "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
               {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
               },
               
           ]
        
     },   
          
      });

}

 $('#labelenfermedad_actual').on('click',function(e)
 {   
   $(".efa").css("display", "block")
 });

 $('.labelealergias_medicamentos').on('click',function(e)
 {   
   $(".efa").css("display", "none")
 });



 $(document).on('click','#btnRegresar', function(e)
 {
     e.preventDefault();
     window.location = '/titulares/';
     
 });
 $(document).on('click','#btnGuardar', function(e)
 { 
     e.preventDefault();
    let tipo_beneficiario=$('#tipo_beneficiario').val();
    let id_especialidad=$('#especialidad').val();
    let id_medico=$('#medico').val();
    let cedula=$('#cedulaT').val();
     if($('#especialidad').val()==0) 
     {
          alert('Debe Seleccionar una  especialidad');   
     }
     else if($('#medico').val()>=1) 
     {
         let fecha_asistencia=$('#fecha_del_dia').val();
          let fechaconvertida=moment(fecha_asistencia,'DD-MM-YYYY'); 
          fechaconvertida=fechaconvertida.format('YYYY-MM-DD');
         let n_historial=$('#numeroHistorial').val();
         let peso=$('#peso').val();
         let talla=$('#talla').val();
         let spo2=$('#spo2').val();
         let frecuencia_c=$('#frecuencia_c').val();
         let frecuencia_r=$('#frecuencia_r').val();
         let temperatura=$('#temperatura').val();
         let ta_alta=$('#ta_alta').val();
         let ta_baja=$('#ta_baja').val();
         let imc=$('#imc').val();
         let url='/agregar_cita_historial';
         let ruta_regreso='/vista_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;
          if (peso=='') 
          {
               peso=0; 
          }
          if (talla=='') 
          {
               talla=0; 
               }
          if (spo2=='') 
          {
               spo2=0; 
          }if (frecuencia_c=='') 
          {
               frecuencia_c=0; 
          } if (frecuencia_r=='')
          {
               frecuencia_r=0; 
          }if (temperatura=='') 
          {
               temperatura=0; 
          } if (ta_alta=='') 
          {
               ta_alta=0; 
          } if (ta_baja=='')
          {
               ta_baja=0; 
          }

         let data=
           {
              fecha_asistencia:fechaconvertida,
              n_historial   :n_historial,
              id_medico     :id_medico,
              peso          :peso,               
              talla         :talla,
              spo2          :spo2,
              frecuencia_c  :frecuencia_c,
              frecuencia_r  :frecuencia_r,
              temperatura   :temperatura,
              ta_alta       :ta_alta,
              ta_baja       :ta_baja,
              imc:imc,
              tipo_beneficiario:tipo_beneficiario,
                
           }
      //console.log(data);
          $.ajax
          ({
                url:url,
                method:'POST',
                data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                dataType:'JSON',
                beforeSend:function(data)
                {
                },
                success:function(data)
                {
                     
                     if(data=='1')
                     {
                          alert('Registro Incorporado ');
                      
                          
                          window.location = ruta_regreso;
                     }
                     else if(data=='0')
                     {
                          alert('Error en la Incorporación del Registro');
                     }
      
                     else if(data=='2')
                     {
                          alert('La cedula posee Historial');
                     }
      
      
                },
                error:function(xhr, status, errorThrown)
                {
                     alert(xhr.status);
                     alert(errorThrown);
                }
          });

     }else 
     {
          alert('Debe Seleccionar un Medico'); 
     }
    

 
});   

$('#consulta_citas').on('click','.Confirmar', function(e) 

{
    //let cedula=$(this).attr('cedula'); 
     let consulta_id =$(this).attr('id_consulta'); 
     let asistencia =$(this).attr('asistencia'); 
     let now = new Date();
     let day = ("0" + now.getDate()).slice(-2);
     let month = ("0" + (now.getMonth() + 1)).slice(-2);
     let today= day+"-"+month+"-"+now.getFullYear(); 
     let fecha_asistencia=$(this).attr('fecha_asistencia'); 
          controlasistencia =$(this).attr('controlasistencia'); 
     //SE FORMATEO EL OREDEN DE LA FECHA PARA PODER HACER LA COMPARACION DE DD/MM/YYYY A YYYY/MM/DD
     format_fecha_asistencia=(convertDateFormat(fecha_asistencia));
     format_today=(convertDateFormat(today));
     function convertDateFormat(fecha_asistencia) {
     let info = fecha_asistencia.split('-');
     return info[2] + '/' + info[1] + '/' + info[0];
}
//alert(asistencia);
   
if(format_fecha_asistencia<format_today&&asistencia=='f')
     {
          alert('EL BENEFICIARIO HA PERDIDO SU CITA');
         // window.location = '/reporte_citas/'
     }
   
    
  else if (format_fecha_asistencia!=format_today&& asistencia=='f') {
      alert('EL BENEFICIARIO NO POSEE CITA PARA EL :'+' '+today);
      //window.location = '/reporte_citas/' 
           
      }
   
   else if(format_fecha_asistencia<format_today&& asistencia=='t')
     {
          alert('NO SE PUEDE MODIFICAR EL ESTATUS , COMUNIQUESE CON EL ADMINISTRADOR DEL SISTEMA');
         // window.location = '/reporte_citas/'
     }
 
     else  {
            //alert('si');  
            asistencia='true'; 
          

               Swal.fire({
                    title: 'ASISTENCIA DE CITA !!',
                    icon:'warning',
                    showCancelButton: true,
                    cancelButtonColor:'#d33',
                    confirmButtonText:'Confirmar',
                         
                    }).then((result)=> {
                         if(result.isConfirmed){ 
               
                              Swal.fire(  
                                                       
                                   'LA CITA HA SIDO CONFIRMADA!',
                                   '...',
                                   'success',
                                   {
                                        
                                   }
                              )
                              
                              let data=
                              {
                                   consulta_id:consulta_id,
                                   asistencia     :asistencia,
                              }   
                              console.log(data);
                              let url='/actualizar_asistencia';
                              $.ajax
                              ({
                                   url:url,
                                   method:'POST',
                                   data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                                   dataType:'JSON',
                                   beforeSend:function(data)
                                   {
                                        //alert('Procesando Información ...');
                                   },
                                   success:function(data)
                                   {
                                        //alert(data);
                                        if(data===1)
                                        {
                                        
                                        
               
                                        }
                                        else
                                        {
                                        alert('Error en la Incorporación del registro');
                                        }
                                        setTimeout(function () {
                                             window.location = '/reporte_citas'
                                        }, 1500);
                                        //window.location = '/datos_titular/'+cedula;             
                                   },
                                   error:function(xhr, status, errorThrown)
                                   {
                                        alert(xhr.status);
                                        alert(errorThrown);
                                   }
                              });  
                              
                              }
                         
                         }) 
                    }  
     
        
 });




 $(document).on('click','#btnfiltrar ', function(e)
  {
      
     let desde                =$('#desde').val();
     let hasta                =$('#hasta').val();
     let medico               =$('#medico').val();
     let especialidad         =$('#especialidad').val();
     let nombre_medico       =$('#medico option:selected' ).text();
     let nombre_especialidad       =$('#especialidad  option:selected' ).text();
     //let nombre_medico       =$('#medico option:selected' ).text();
     let asistio     =$('#asistio').val();
     let control  =$('#control option:selected' ).text();
     let nombre_asistio  =$('#asistio option:selected' ).text();

   


     


          if(asistio==1){
               asistio='t'
          }
          
          else if(asistio==2){
               asistio='f'
          }
          else if(asistio==3){
               asistio='En espera'
          }
          else{
               asistio='Seleccione'
          }
         // alert(asistio);
          

     if (medico==null) 
     {
          medico=0;
     }


      if (desde=='') 
      {
          desde='null' 
      }
       if (hasta=='') 
      {
          hasta='null' 
      }
  
       
   $("#table_consulta_citas").dataTable().fnDestroy();
      
  
      
      if (desde=='null'&&hasta!='null') 
      {
          alert('DEDE INDICAR EL CAMPO DESDE');
          
      }
      else if (hasta=='null'&&desde!='null') 
      {
          alert('DEDE INDICAR EL CAMPO HASTA');
      } 
      else if (hasta<desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
      }else{
     listar_citas_consultas(desde,hasta,medico,especialidad,control,asistio,nombre_medico,nombre_especialidad,nombre_asistio);
      }
      
  });

     

 

  $(document).on('click','#btnlimpiar', function(e)
  {
    e.preventDefault();
    $('#desde').val('');
    $('#hasta').val('');
     $('#medico').val('0');
    $('#especialidad').val('0');
    $('#asistio').val('0');
    $('#control').val('0');
        
    let desde          =$('#desde').val();
    let hasta          =$('#hasta').val();
    if (desde==''&&hasta=='')
    {
    
        desde='null'
        hasta='null'     
    } 
  
    let id_medico   =$('#cmbmedicos').val();
    $('#medico').val('0')

    $('#especialidad').val('0')
    let especialidad   =0
    let asistio='Seleccione'  

    let control='Seleccione'   
   
    
    medico=0;
   
      $("#table_consulta_citas").dataTable().fnDestroy();
      listar_citas_consultas(desde,hasta,medico,especialidad,control,asistio);
        
  });



  function  llenar_combo_especialidad(e,id_especialidad)
  {
 
      e.preventDefault;
      
        url='/listar_especialidades_activas_sin_filtro';
         $.ajax
        ({
             url:url,
             method:'GET',
            dataType:'JSON',
            beforeSend:function(data)
            {
            },
            success:function(data)
            {   
  if(data.length>=1)
  {
       $('#especialidad').empty();
       $('#especialidad').append('<option value=0>Seleccione</option>');     
       if(id_especialidad===undefined)
      {
           
           $.each(data, function(i, item)
           {
                //console.log(data)
                $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
 
           });
      }
      else
      {
           $.each(data, function(i, item)
           {
               
 
                if(item.id_especialidad===id_especialidad)
               
                {
                     $('#especialidad').append('<option value='+item.id_especialidad+' selected>'+item.descripcion+'</option>');
                }
                else
                {
                     $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
                }
           });
      }
  }      
  },
  error:function(xhr, status, errorThrown)
  {
      alert(xhr.status);
      alert(errorThrown);
  }
  });
  }

function  llenar_combo_MedicoTratante(e,id)
{

    e.preventDefault;
    
      url='/listar_medicos_activos';
       $.ajax
      ({
           url:url,
           method:'GET',
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {

if(data.length>=1)
{
     $('#cmbmedicotratante').empty();
     $('#cmbmedicotratante').append('<option value=0>Seleccione</option>');     
     if(id===undefined)
    {
         
         $.each(data, function(i, item)
         {
              //console.log(data)
              $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

         });
    }
    else
    {
         
         $.each(data, function(i, item)
         {
             

              if(item.id===id)
             
              {
                   $('#cmbmedicotratante').append('<option value='+item.id+' selected>'+item.nombre+'  '+item.apellido+'</option>');
              }
              else
              {
                   $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');
              }
         });
    }
}      
},
error:function(xhr, status, errorThrown)
{
    alert(xhr.status);
    alert(errorThrown);
}
});
}
  function  llenar_combo_MedicoReferido(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
           
 if(data.length>=1)
 {
      $('#medico').empty();
      $('#medico').append('<option value=0  selected disabled>Medicos</option>');   
      verificar=7;  
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#medico').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
        data=data.filter(dato=>dato.id_especialidad==id_especialidad);
        //console.log(buscar);
           $.each(data, function(i, item)
           {
            $('#medico').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

              
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $("#especialidad").on('change', function(e)
 {
     document.getElementById("medico").disabled=false;
    id_especialidad=$('#especialidad option:selected').val(); 

    llenar_combo_MedicoReferido(e,id_especialidad)
 }); 


 $('#consulta_citas').on('click','.Bloquear', function(e)    

 {
   
     let control   =$(this).attr('control');
      Swal.fire({
           title: 'Reversar el Registro?',
           icon:'warning',
           showCancelButton: true,
           cancelButtonColor:'#d33',
           confirmButtonText:'Confirmar',
                    
           }).then((result)=> {
                  if(result.isConfirmed){ 
      
                     Swal.fire(  
                                                  
                          'REVERSO!!',
                          'Registro Actualizado.',
                          'success',
                          {
                               
                          }
                        )
                        let id   =$(this).attr('id_consulta');
                        let nombre   =$(this).attr('nombre');
                        let cedula   =$(this).attr('cedula');
                        let control   =$(this).attr('control');
                        id = (isNaN(parseInt(id)))? 0 : parseInt(id);
                        let borrado    =true;
                      
 
                        var data=
                        {
                             id:id,
                             borrado:borrado,
                             nombre:nombre,
                             cedula:cedula,
                             control:control
                        }     
                       var url='/borrar_consulta';
                       $.ajax
                        ({
                             url:url,
                             method:'POST',
                             data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                             dataType:'JSON',
                             beforeSend:function(data)
                             {
                                  //alert('Procesando Información ...');
                             },
                             success:function(data)
                             {
                             
                             
                                 if(data==0)
                                {
                                    alert('Error al Reversar el Registro');  
                                  
                                }
 
                                else  if(data==2)
                                {
                                    alert('Hubo un error durante el reverso ');  
                                }
                                setTimeout(function () {
                                    window.location = '/reporte_citas/';  
                                 }, 1500);
                                          
                             },
                             error:function(xhr, status, errorThrown)
                             {
                                alert(xhr.status);
                                alert(errorThrown);
                             }
                       });  
                       
                    }
                    
                  })  
      
         
  });
 
 
 