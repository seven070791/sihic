/*
 *Este es el document ready
 */

$(function() {
    sexo = null;
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    if (desde == '' && hasta == '') {

        desde = 'null'
        hasta = 'null'
    }
    let id_medico = $('#cmbmedicos').val();
    $('#medico').val('0')

    $('#especialidad').val('0')
    let especialidad = 0
    beneficiario = null;
    nombre_medico = 'Seleccione'
    nombre_especialidad = 'Seleccione'
    medico = 0;
    especialidad = 0;
    listar_morbilidad(desde, hasta, medico, especialidad, sexo, beneficiario);
    llenar_combo_especialidad(Event)
    llenar_combo_MedicoTratante(Event)
    llenar_combo_entes(Event);
    $entes_adscritos = 0;
});
/*
 * Función para definir datatable:
 */
function listar_morbilidad(desde = null, hasta = null, medico = 0, especialidad = 0, nombre_especialidad, nombre_medico = 'Seleccione', sexo = null, beneficiario = null) {

    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "/" + month + "/" + now.getFullYear();

    // Convertir la fecha
    var fechaOriginal = desde;
    var dataFormatada_desde = moment(fechaOriginal).format("DD-MM-YYYY");

    var fechaOriginal2 = hasta;
    var dataFormatada_hasta = moment(fechaOriginal2).format("DD-MM-YYYY");

    var encabezado = '';
    if (dataFormatada_desde != 'Invalid date' && dataFormatada_hasta != 'Invalid date') {
        encabezado = encabezado + 'Desde:' + ' ' + dataFormatada_desde + ' ' + ' ' + 'hasta' + ' ' + ' ' + dataFormatada_hasta + ' ' + ' ';
    }

    if (nombre_especialidad != null && nombre_especialidad != 'Seleccione') {
        encabezado = encabezado + 'Especialidad:' + ' ' + ' ' + nombre_especialidad + ' ' + ' ';
    }

    if (nombre_medico != 'Seleccione' && nombre_medico != 'Medicos' && nombre_medico != null) {
        encabezado = encabezado + 'Medico:' + ' ' + ' ' + nombre_medico + ' ' + ' ';
    }

    if (sexo != null) {
        if (sexo == 1) {
            descr_sexo = 'MASCULINO'
        } else if (sexo == 2) {
            descr_sexo = 'FEMENINO'
        }
        encabezado = encabezado + 'SEXO:' + ' ' + ' ' + descr_sexo + ' ' + ' ';
    }

    if (beneficiario != null) {
        if (beneficiario == 'T') {
            descr_beneficiario = 'TITULAR'
        } else if (beneficiario == 'F') {
            descr_beneficiario = 'FAMILIAR'
        } else if (beneficiario == 'C') {
            descr_beneficiario = 'CORTESIA'
        }
        encabezado = encabezado + 'TIPO:' + ' ' + ' ' + descr_beneficiario + ' ' + ' ';
    }

    let ruta_imagen = rootpath
    var table = $('#table_morbilidad').DataTable({
        dom: 'Bfrtip',
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'

                },

            },

            buttons: [{
                    //definimos estilos del boton de pdf
                    extend: "pdf",
                    text: 'PDF',
                    orientation: 'landscape',
                    pageSize: 'A4',
                    className: 'btn-xs btn-dark',
                    //title:'RELACION SALIDAS MEDICAMENTOS',
                    header: true,
                    footer: true,
                    fontSize: '12',
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],

                    },
                    alignment: 'center',




                    customize: function(doc) {
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '12',
                            alignment: 'center',

                        }
                        doc.styles['td:nth-child(2)'] = {
                                width: '200px',
                                'max-width': '200px'
                            },
                            doc.styles.tableHeader = {
                                fillColor: '#4c8aa0',
                                color: 'white',
                                alignment: 'center'
                            },
                            // Create a header
                            doc.pageMargins = [40, 110, 15, 0];
                        doc['header'] = (function(page, pages) {
                            doc.styles.title = {
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                            }
                            return {
                                columns: [{

                                        margin: [100, 5, 40, 20],

                                        image: ruta_imagen,
                                        width: 630,


                                    },
                                    {
                                        margin: [-480, 60, -25, 0],
                                        color: '#4c8aa0',
                                        fontSize: '24',
                                        alignment: 'center',
                                        text: ' CONTROL DE MORBILIDAD',
                                        fontSize: 18,
                                    },
                                    {

                                        margin: [-670, 85, -25, 0],
                                        fontSize: 10,
                                        text: encabezado,
                                        fontSize: 10,
                                    },

                                ],
                            }
                        });
                        // Create a footer
                        doc['footer'] = (function(page, pages) {
                            return {
                                columns: [{
                                    alignment: 'center',
                                    text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }],
                            }
                        });

                    },
                },

                {
                    //definimos estilos del boton de excel
                    extend: "excel",
                    text: 'Excel',
                    className: 'btn-xs btn-dark',
                    title: 'MORBILIDAD',
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
                    },
                    excelStyles: {




                        "template": [

                            "blue_medium",
                            "header_blue",
                            "title_medium"
                        ],
                        "style": {
                            "fill": {
                                "pattern": {
                                    "bgColor": "CCFFCC"
                                }
                            }
                        }
                    }

                },


            ]
        },

         "order": [
            [0, "desc"]
        ], 
        //"order":[0,"desc"],	
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],

        "ordering": true,
        "info": true,
        "autoWidth": true,
        //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
        "ajax": {
            "url": base_url+"/listar_mobilidad/" + desde + '/' + hasta + '/' + medico + '/' + especialidad + '/' + sexo + '/' + beneficiario,
            "type": "GET",
            dataSrc: ''
        },

        "columnDefs": [
            { "className": "text-center", "targets": [3, 4, 5, 6] }
        ],

        "columns": [
            { data: 'fecha_asistencia_normal' },
            { data: 'control' },
            { data: 'cedula' },
            { data: 'nombre' },
            { data: 'edad' },
            { data: 'sexo' },
            { data: 'beneficiario' },
            { data: 'parentesco' },
            { data: 'departamento' },
            { data: 'motivo_consulta' },
            { data: 'diagnostico' },
            { data: 'especialidad' },
            { data: 'nombre_medico' },
            { data: 'reposo' },


        ],


        "language": {
            "decimal": ",",
            "thousands": ".",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoPostFix": "",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "loadingRecords": "Cargando...",
            "lengthMenu": "Mostrar _MENU_ registros",
            "paginate": {
                "first": "Primero",
                "last": "Último",
                "next": "Siguiente",
                "previous": "Anterior"
            },
            "processing": "Procesando...",
            "search": "Buscar:",
            "searchPlaceholder": "Término de búsqueda",
            "zeroRecords": "No se encontraron resultados",
            "emptyTable": "Ningún dato disponible en esta tabla",
            "aria": {
                "sortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sortDescending": ": Activar para ordenar la columna de manera descendente"
            },




            //only works for built-in buttons, not for custom buttons
            "buttons": {
                "create": "Nuevo",
                "edit": "Cambiar",
                "remove": "Borrar",
                "copy": "Copiar",
                "csv": "fichero CSV",
                "excel": "tabla Excel",
                "pdf": "documento PDF",
                "print": "Imprimir",
                "colvis": "Visibilidad columnas",
                "collection": "Colección",
                "upload": "Seleccione fichero...."
            },
            "select": {
                "rows": {
                    _: '%d filas seleccionadas',
                    0: 'clic fila para seleccionar',
                    1: 'una fila seleccionada'
                }
            }


        }

    });
}


function listar_morbilidad_filtro_entes(desde = null, hasta = null, medico = 0, especialidad = 0, nombre_especialidad, nombre_medico = 'Seleccione', sexo = null, beneficiario = null, entes_adscritos = 0, nombre_entes_adscritos = 'Seleccione') {


    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "/" + month + "/" + now.getFullYear();
    var fechaOriginal = desde;
    var dataFormatada_desde = moment(fechaOriginal).format("DD-MM-YYYY");
    var fechaOriginal2 = hasta;
    var dataFormatada_hasta = moment(fechaOriginal2).format("DD-MM-YYYY");

    var encabezado = '';

    if (dataFormatada_desde != 'Invalid date' && dataFormatada_hasta != 'Invalid date') {
        encabezado = encabezado + 'Desde:' + ' ' + dataFormatada_desde + ' ' + ' ' + 'hasta' + ' ' + ' ' + dataFormatada_hasta + ' ' + ' ';
    }
    if (nombre_especialidad != null && nombre_especialidad != 'Seleccione') {
        encabezado = encabezado + 'Especialidad:' + ' ' + ' ' + nombre_especialidad + ' ' + ' ';
    }

    if (nombre_medico != 'Seleccione' && nombre_medico != 'Medicos' && nombre_medico != 'null') {
        encabezado = encabezado + 'Medico:' + ' ' + ' ' + nombre_medico + ' ' + ' ';
    }

    if (sexo != null) {
        if (sexo == 1) {
            descr_sexo = 'MASCULINO'
        } else if (sexo == 2) {
            descr_sexo = 'FEMENINO'
        }
        encabezado = encabezado + 'SEXO:' + ' ' + ' ' + descr_sexo + ' ' + ' ';
    }

    if (beneficiario != null) {
        if (beneficiario == 'T') {
            descr_beneficiario = 'TITULAR'
        } else if (beneficiario == 'F') {
            descr_beneficiario = 'FAMILIAR'
        } else if (beneficiario == 'C') {
            descr_beneficiario = 'CORTESIA'
        }
        encabezado = encabezado + 'TIPO:' + ' ' + ' ' + descr_beneficiario + ' ' + ' ';
    }

    if (nombre_entes_adscritos != 'Seleccione' && nombre_entes_adscritos != 'null') {
        encabezado = encabezado + 'ENTE:' + ' ' + ' ' + nombre_entes_adscritos + ' ' + ' ';
    }



    let ruta_imagen = rootpath
    var table = $('#table_morbilidad_entes').DataTable({

        dom: "Bfrtip",
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'
                },

            },

            buttons: [{
                    //definimos estilos del boton de pdf
                    extend: "pdf",
                    text: 'PDF',
                    orientation: 'landscape',
                    pageSize: 'letter',
                    className: 'btn-xs btn-dark',
                    //title:'RELACION SALIDAS MEDICAMENTOS',
                    header: true,
                    footer: true,
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],

                    },
                    alignment: 'center',

                    customize: function(doc) {
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'
                        }
                        doc.styles['td:nth-child(2)'] = {
                                width: '200px',
                                'max-width': '200px'
                            },
                            doc.styles.tableHeader = {
                                fillColor: '#4c8aa0',
                                color: 'white',
                                alignment: 'center'
                            },
                            // Create a header
                            doc.pageMargins = [40, 110, 15, 20];
                        doc['header'] = (function(page, pages) {
                            doc.styles.title = {
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                            }
                            return {
                                columns: [{

                                        margin: [100, 5, 40, 20],

                                        image: ruta_imagen,
                                        width: 630,


                                    },
                                    {
                                        margin: [-530, 60, -25, 0],
                                        color: '#4c8aa0',
                                        fontSize: '24',
                                        alignment: 'center',
                                        text: ' CONTROL DE MORBILIDAD',
                                        fontSize: 18,
                                    },
                                    {

                                        margin: [-670, 85, -25, 0],
                                        fontSize: 10,
                                        text: encabezado,
                                        fontSize: 10,
                                    },

                                ],
                            }
                        });
                        // Create a footer
                        doc['footer'] = (function(page, pages) {
                            return {
                                columns: [{
                                    alignment: 'center',
                                    text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }],
                            }
                        });

                    },
                },

                {
                    //definimos estilos del boton de excel
                    extend: "excel",
                    text: 'Excel',
                    className: 'btn-xs btn-dark',
                    title: 'MORBILIDAD',

                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],

                    },

                    excelStyles: {
                        "template": [
                            "blue_medium",
                            "header_blue",
                            "title_medium"
                        ]
                    },

                }
            ]
        },

        "order": [
            [0, "desc"]
        ],
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],

        "ordering": true,
        "info": true,
        "autoWidth": true,
        //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
        "ajax": {
            "url": "/listar_mobilidad_entes/" + desde + '/' + hasta + '/' + medico + '/' + especialidad + '/' + sexo + '/' + beneficiario + '/' + entes_adscritos,
            "type": "GET",
            dataSrc: ''
        },
        "columns": [


            { data: 'fecha_asistencia' },
            { data: 'control' },
            { data: 'cedula' },
            { data: 'nombre' },
            { data: 'edad' },
            { data: 'sexo' },
            { data: 'beneficiario' },
            { data: 'parentesco' },
            //{data:'estatus'},
            { data: 'departamento' },
            { data: 'motivo_consulta' },
            { data: 'diagnostico' },
            { data: 'especialidad' },

            { data: 'nombre_medico' },
            { data: 'reposo' },

        ],

        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{

                "targets": [0, 1, 2, 3, 4, 5, 6],
                "visible": true,
                "searchable": true
            }, ]

        },

    });
}





$(document).on('click', '#btnfiltrar ', function(e) {

    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let medico = $('#medico').val();
    let sexo = $('#sexo').val();
    let especialidad = $('#especialidad').val();
    let nombre_medico = $('#medico option:selected').text();
    let nombre_especialidad = $('#especialidad  option:selected').text();
    let beneficiario = $('#beneficiario').val();
    if (medico == null) {
        medico = 0;
    }

    if (desde == '') {
        desde = 'null'
    }
    if (hasta == '') {
        hasta = 'null'
    }

    if (beneficiario == '1') {
        beneficiario = 'T'
    } else if (beneficiario == '2') {
        beneficiario = 'F'
    } else if (beneficiario == '3') {
        beneficiario = 'C'
    }


    $("#table_morbilidad").dataTable().fnDestroy();
    $("#table_morbilidad_entes").hide();
    $("#table_morbilidad_entes").dataTable().fnDestroy();
    $("#table_morbilidad").show();
    listar_morbilidad(desde, hasta, medico, especialidad, nombre_especialidad, nombre_medico, sexo, beneficiario);

    if (desde == 'null' && hasta != 'null') {
        alert('DEDE INDICAR EL CAMPO DESDE');

    } else if (hasta == 'null' && desde != 'null') {
        alert('DEDE INDICAR EL CAMPO HASTA');
    } else if (hasta < desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
    }

});


$(document).on('click', '#filtrocortesia ', function(e) {

    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let medico = $('#medico').val();
    let sexo = $('#sexo').val();
    let especialidad = $('#especialidad').val();
    let nombre_medico = $('#medico option:selected').text();
    let nombre_especialidad = $('#especialidad  option:selected').text();
    let beneficiario = $('#beneficiario').val();
    let entes_adscritos = $('#entes_adscritos').val();
    let nombre_entes_adscritos = $('#entes_adscritos option:selected').text();
    if (medico == null) {
        medico = 0;
    }


    if (desde == '') {
        desde = 'null'
    }
    if (hasta == '') {
        hasta = 'null'
    }

    if (beneficiario == '1') {
        beneficiario = 'T'
    } else if (beneficiario == '2') {
        beneficiario = 'F'
    } else if (beneficiario == '3') {
        beneficiario = 'C'
    }

    $("#table_morbilidad_entes").dataTable().fnDestroy();
    $("#table_morbilidad").dataTable().fnDestroy();
    $("#table_morbilidad_entes").show();
    $("#table_morbilidad").hide();
    listar_morbilidad_filtro_entes(desde, hasta, medico, especialidad, nombre_especialidad, nombre_medico, sexo, beneficiario, entes_adscritos, nombre_entes_adscritos);

    if (desde == 'null' && hasta != 'null') {
        alert('DEDE INDICAR EL CAMPO DESDE');

    } else if (hasta == 'null' && desde != 'null') {
        alert('DEDE INDICAR EL CAMPO HASTA');
    } else if (hasta < desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
    }

});


function llenar_combo_especialidad(e, id_especialidad) {

    e.preventDefault;

    url = '/listar_especialidades_activas_sin_filtro_caja_chica';
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $('#especialidad').empty();
                $('#especialidad').append('<option value=0>Seleccione</option>');
                if (id_especialidad === undefined) {

                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#especialidad').append('<option value=' + item.id_especialidad + '>' + item.descripcion + '</option>');

                    });
                } else {
                    $.each(data, function(i, item) {


                        if (item.id_especialidad === id_especialidad)

                        {
                            $('#especialidad').append('<option value=' + item.id_especialidad + ' selected>' + item.descripcion + '</option>');
                        } else {
                            $('#especialidad').append('<option value=' + item.id_especialidad + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

function llenar_combo_MedicoTratante(e, id) {

    e.preventDefault;

    url = '/listar_medicos_activos';
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            if (data.length >= 1) {
                $('#cmbmedicotratante').empty();
                $('#cmbmedicotratante').append('<option value=0>Seleccione</option>');
                if (id === undefined) {

                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#cmbmedicotratante').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');

                    });
                } else {

                    $.each(data, function(i, item) {


                        if (item.id === id)

                        {
                            $('#cmbmedicotratante').append('<option value=' + item.id + ' selected>' + item.nombre + '  ' + item.apellido + '</option>');
                        } else {
                            $('#cmbmedicotratante').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

function llenar_combo_MedicoReferido(e, id_especialidad) {

    e.preventDefault;

    url = '/listar_medicos_activos';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {

            if (data.length >= 1) {
                $('#medico').empty();
                $('#medico').append('<option value=0  selected disabled>Medicos</option>');
                verificar = 7;
                if (id_especialidad === undefined) {

                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');

                    });
                } else {
                    data = data.filter(dato => dato.id_especialidad == id_especialidad);
                    //console.log(buscar);
                    $.each(data, function(i, item) {
                        $('#medico').append('<option value=' + item.id + '>' + item.nombre + '  ' + item.apellido + '</option>');


                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

$("#especialidad").on('change', function(e) {
    document.getElementById("medico").disabled = false;
    id_especialidad = $('#especialidad option:selected').val();

    llenar_combo_MedicoReferido(e, id_especialidad)
});



$("#entes_adscritos").on('change', function() {


    var adscritos = $('#entes_adscritos').val();

    if (adscritos != 0) {
        $("#btnfiltrocortesia").css("display", "block");

        $("#btnfiltrar").css("display", "none");

    } else {

        $("#btnfiltrocortesia").css("display", "none");
        $("#btnfiltrar").css("display", "block");
    }

});


$("#beneficiario").on('change', function() {

    var beneficiario = $('#beneficiario').val();
    if (beneficiario == '1') {
        $("#btnfiltrar").show();
        $("#filtrocortesia").hide();
        $("#btnlimpiar").show();
        $('#entes_adscritos').val(0);
        // $("#filtrocortesia").css("visibility", "hidden");
        //$("#btnfiltrar").css("visibility", "visible");
        // $("#btnlimpiar").css("visibility", "hidden");


        $("#entes_adscritos").prop('disabled', true);
    } else if (beneficiario == '2') {
        $('#entes_adscritos').val(0);
        $("#btnfiltrar").show();
        $("#btnlimpiar").show();
        $("#filtrocortesia").hide();
        //$("#filtrocortesia").css("visibility", "hidden");
        // $("#btnfiltrar").css("visibility", "visible");
        // $("#entes_adscritos").prop('disabled', true);
    } else if (beneficiario == '3') {
        $("#btnlimpiar").show();
        $("#filtrocortesia").show();
        $("#btnfiltrar").hide();
        $("#entes_adscritos").prop('disabled', false);
        //$("#filtrocortesia").css("visibility", "visible");
        // $("#btnfiltrar").css("visibility", "hidden");

    }

});

function llenar_combo_entes(e, id) {

    e.preventDefault
    url = '/listar_entes_activos';
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $('#entes_adscritos').empty();
                $('#entes_adscritos').append('<option value=0  selected disabled>Seleccione</option>');
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#entes_adscritos').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {

                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $('#entes_adscritos').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');

                        } else {

                            $('#entes_adscritos').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}



$(document).on('click', '#btnlimpiar', function(e) {
    e.preventDefault();

    $('#medico').val('0');
    $('#especialidad').val('0');
    $('#entes_adscritos').val('0');
    $('#beneficiario').val('0');
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let medico = 0;
    let sexo = null
    let especialidad = 0;
    let nombre_medico = $('#medico option:selected').text();
    let nombre_especialidad = $('#especialidad  option:selected').text();
    let beneficiario = null
    if (medico == null) {
        medico = 0;
    }


    if (desde == '') {
        desde = 'null'
    }
    if (hasta == '') {
        hasta = 'null'
    }

    if (beneficiario == '1') {
        beneficiario = 'T'
    } else if (beneficiario == '2') {
        beneficiario = 'F'
    } else if (beneficiario == '3') {
        beneficiario = 'C'
    }
    $("#table_morbilidad").dataTable().fnDestroy();

    listar_morbilidad(desde, hasta, medico, especialidad, sexo, beneficiario);

    $("#table_morbilidad_entes").dataTable().fnDestroy();
    $("#table_morbilidad_entes").dataTable().fnDestroy();
    $("#table_morbilidad_entes").hide();
    $("#table_morbilidad").show();
    $("#entes_adscritos").prop('disabled', true);

});