/*
 *Este es el document ready
 */

$(function () {
    let sexo = null;
    let desde = $("#desde").val();
    let hasta = $("#hasta").val();
    if (desde == "" && hasta == "") {
        desde = "null";
        hasta = "null";
    }

    let beneficiario = null;
   
    listar_frecuencia_atencion(desde = null, hasta = null, beneficiario= null, sexo= null);

});

function listar_frecuencia_atencion(desde, hasta, beneficiario, sexo) {


    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "/" + month + "/" + now.getFullYear();
    // Convertir la fecha
    var fechaOriginal = desde;
    var dataFormatada_desde = moment(fechaOriginal).format("DD-MM-YYYY");
    var fechaOriginal2 = hasta;
    var dataFormatada_hasta = moment(fechaOriginal2).format("DD-MM-YYYY");
    var encabezado = "";
    let ruta_imagen = rootpath;
    if (dataFormatada_desde != 'Invalid date' && dataFormatada_hasta != 'Invalid date') {
        encabezado = encabezado + 'Desde:' + ' ' + dataFormatada_desde + ' ' + ' ' + 'hasta' + ' ' + ' ' + dataFormatada_hasta + ' ' + ' ';
    }
    if (sexo != null) {
        if (sexo == '1') {
            descr_sexo = 'MASCULINO'
        } else if (sexo == '2') {
            descr_sexo = 'FEMENINO'
        }
        encabezado = encabezado + 'SEXO:' + ' ' + ' ' + descr_sexo + ' ' + ' ';
    }

    if (beneficiario != null) {
        if (beneficiario == 'T') {
            descr_beneficiario = 'TITULAR'
        } else if (beneficiario == 'F') {
            descr_beneficiario = 'FAMILIAR'
        } else if (beneficiario == 'C') {
            descr_beneficiario = 'CORTESIA'
        }
        encabezado = encabezado + 'TIPO:' + ' ' + ' ' + descr_beneficiario + ' ' + ' ';
    }


    if (beneficiario != null) {
        if (beneficiario == 'T') {
            descr_beneficiario = 'TITULAR'
        } else if (beneficiario == 'F') {
            descr_beneficiario = 'FAMILIAR'
        } else if (beneficiario == 'C') {
            descr_beneficiario = 'CORTESIA'
        }
        encabezado = encabezado + 'TIPO:' + ' ' + ' ' + descr_beneficiario + ' ' + ' ';
    }

    var table = $('#table_frecuencia_atencion').DataTable({
        dom: "Bfrtip",
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'
                },

            },

            buttons: [{
                //definimos estilos del boton de pdf
                extend: "pdf",
                text: 'PDF',
                className: 'btn-xs btn-dark',
                title: 'FRECUENCIA DE ATENCION  DE USUARIOS',
                download: 'open',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6],

                },
                alignment: 'center',
                customize: function (doc) {
                    //Remove the title created by datatTables
                    doc.content.splice(0, 1);

                    doc.styles.title = {
                        color: '#4c8aa0',
                        fontSize: '18',
                        alignment: 'center'
                    }
                    doc.styles['td:nth-child(2)'] = {
                        width: '100px',
                        'max-width': '100px'
                    },
                        doc.styles.tableHeader = {
                            fillColor: '#4c8aa0',
                            color: 'white',
                            alignment: 'center'
                        },
                        // Create a header
                        doc.pageMargins = [40, 85, 0, 30];
                    doc['header'] = (function (page, pages) {
                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center',
                        }
                        return {
                            columns: [{
                                margin: [40, 3, 40, 40],
                                image: ruta_imagen,
                                width: 500,

                            },
                            {
                                margin: [-470, 40, -25, 0],
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                                text: 'CONTROL DE INVENTARIO',
                                fontSize: 18,
                            },
                            {
                                margin: [-508, 70, -25, 0],
                                text: encabezado,
                            },
                            ],
                        }
                    });
                    // Create a footer
                    doc['footer'] = (function (page, pages) {
                        return {
                            columns: [{
                                alignment: 'center',
                                text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                            }],
                        }
                    });

                },
            },
            {
                //definimos estilos del boton de excel
                extend: "excel",
                text: 'Excel',
                className: 'btn-xs btn-dark',
                title: 'FRECUENCIA DE ATENCION  DE USUARIOS',

                download: 'open',
                exportOptions: {
                    columns: [0, 1, 2, 3, 4, 5, 6]
                },
                excelStyles: {
                    "template": [
                        "blue_medium",
                        "header_blue",
                        "title_medium"
                    ]
                },

            }
            ]
        },


        "order": [
            [1, "asc"]
        ],
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],

        "ordering": true,
        "info": true,
        "autoWidth": true,
        "ajax": {
            "url": base_url + "/listar_frecuencia_atencion/" + desde + '/' + hasta + "/" + beneficiario + '/' + sexo,
            "type": "GET",
            dataSrc: ''
        },
        "columns": [
            { data: "cedula" },
            { data: "nombre" },
            { data: "edad" },
           { data: "sexo" },
            { data: "beneficiario"},
            { data: "parentesco"},
            { data: "veces"},
            // {
            //     orderable: true,
            //     render: function(data, type, row) {

                   
            //             return '<a href="javascript:;" class="btn btn-xs btn-primary  Detalles" style=" font-size:2px" data-toggle="tooltip" title="Detalles" cedula=' + row.cedula + ' historia=' + row.historia + '  beneficiario=' + row.beneficiario + ' > <i class="material-icons " >search</i></a>' 
            //                 // style=" opacity: 0.4;"  
                   


            //     }
            // },
           // { data: "fecha" },
        ],
        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            },]

        },


    });
}



// $('#listar_frecuencias').on('click','.Detalles', function(e)
// {   
//     e.preventDefault();
   
//     $("#modal").modal("show");
//     var n_historial       =$(this).attr('historia');
//     detalle_frecuencia_atencion(n_historial);
// });



function detalle_frecuencia_atencion(n_historial) {
   
   
    $('#table_Detalle_frecuencia_atencion').DataTable
     (
      {
           "order":[[3,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":base_url+"/listar_Historial_consultas/"+n_historial,
            "type":"GET",
            dataSrc:''
           },
           "columns":
           [
              {data:'id'}, 
              {data:'n_historial'},     
              {data:'nombre'},
              {data:'especialidad'},
              {data:'fecha_asistencia'},

            
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": '2023-03-24'
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           });
}

$(document).on("click", "#btnfiltrar ", function (e) {
    let desde = $("#desde").val();
    let hasta = $("#hasta").val();
    let medico = $("#medico").val();
    let sexo = $("#sexo").val();
    let especialidad = $("#especialidad").val();
    let nombre_medico = $("#medico option:selected").text();
    let nombre_especialidad = $("#especialidad  option:selected").text();
    let beneficiario = $("#beneficiario").val();
   
    if (medico == null) {
        medico = 0;
    }

    if (desde == "") {
        desde = "null";
    }
    if (hasta == "") {
        hasta = "null";
    }

    if (beneficiario == "1") {
        beneficiario = "T";
    } else if (beneficiario == "2") {
        beneficiario = "F";
    } else if (beneficiario == "3") {
        beneficiario = "C";
    }

  
    $("#table_frecuencia_atencion").dataTable().fnDestroy();
   // $("#table_morbilidad").show();
   listar_frecuencia_atencion( desde, hasta, beneficiario,  sexo );

    if (desde == "null" && hasta != "null") {
        alert("DEDE INDICAR EL CAMPO DESDE");
    } else if (hasta == "null" && desde != "null") {
        alert("DEDE INDICAR EL CAMPO HASTA");
    } else if (hasta < desde) {
        alert("EL CAMPO DESDE ES MAYOR AL CAMPO HASTA");
    }
});

$(document).on("click", "#btnlimpiar", function (e) {
    e.preventDefault();

    $("#medico").val("0");
    $("#especialidad").val("0");
    $("#entes_adscritos").val("0");
    $("#beneficiario").val("0");
    let desde = $("#desde").val();
    let hasta = $("#hasta").val();
    let medico = 0;
    let sexo = null;
    let especialidad = 0;
    let nombre_medico = $("#medico option:selected").text();
    let nombre_especialidad = $("#especialidad  option:selected").text();
    let beneficiario = null;
    if (medico == null) {
        medico = 0;
    }

    if (desde == "") {
        desde = "null";
    }
    if (hasta == "") {
        hasta = "null";
    }

    if (beneficiario == "1") {
        beneficiario = "T";
    } else if (beneficiario == "2") {
        beneficiario = "F";
    } else if (beneficiario == "3") {
        beneficiario = "C";
    }

    $("#table_frecuencia_atencion").dataTable().fnDestroy();

    listar_frecuencia_atencion(desde, hasta, beneficiario, sexo);

    // $("#table_morbilidad_entes").dataTable().fnDestroy();
    // $("#table_morbilidad_entes").hide();
    // $("#table_morbilidad").show();
    // $("#entes_adscritos").prop("disabled", true);
});