/*
 *Este es el document ready
 */
 $(function()
 {
  
    
     var id_entrada=$('#id_entrada').val();
     listarsalidas(id_entrada);
     document.getElementById("reversar").disabled=true;
     //$('.reversar').attr('disabled','disabled');
   
     //alert($('#id_medicamento').val());
 });
 /*
  * Función para definir datatable:
  */
 function listarsalidas(id_entrada)
 {
    
      $('#table_salidas').DataTable
      (
       {
            "order":[[0,"desc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_salidas_contra_entrada/"+$('#id_entrada').val()+'/'+$('#id_medicamento').val(),
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
                {data:'id'},
                {data:'fecha_salida'}, 
                {data:'nombre_apellido'},                
                {data:'cantidad'},   

                {orderable: true,
                    render:function(data, type, row)
                    {
                     

                         return  '<a href="javascript:;" id="reversar" class="btn btn-info reversar"  style=" font-size:2px" data-toggle="tooltip" title="Reversar" id_salida='+row.id+' fecha_entrada='+row.fecha_entrada+' fecha_vencimiento='+row.fecha_vencimiento+' cantidad='+row.cantidad+'><i class="material-icons " >autorenew</i></a>'
                    
                   
                    }
               }

              
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }



 $(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();

      var id_medicamento=$('#id_medicamento').val();
      var fecha_salida=$('#fecha').val();
      var cantidad=$('#cantidad').val();
      var url='/agregar_salidas';
      var ruta_regreso='/vista_salidas/'+id_medicamento;
     var data=
     {
          id_medicamento    :id_medicamento,
          fecha_salida    :fecha_salida,
          cantidad          :cantidad
     }
         $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
              
               if(data=='1')
               {
                    // alert('Retiro Correcto ');
                
                    
                    window.location = ruta_regreso;
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)     
          {
               alert(xhr.status);
               alert(errorThrown);
          }
      });
});

 $(document).on('click','#borrado_reverso', function(e)
 {
     if($('#borrado_reverso').is(':checked'))
     {
          document.getElementById("btnActualizar_reversar").disabled=false;  
     }
     else
     {
          document.getElementById("btnActualizar_reversar").disabled=true;  
     }
     
 });



 $(document).on('click','#btnRegresar ', function(e)
 {  
     var id_entrada=$('#id_entrada').val();
          var id_medicamento=$('#id_medicamento').val();
         
         e.preventDefault();
         window.location = '/vista_salidas/'+id_medicamento; 
     
 });

 $(document).on('click','#btnCerrar_reversar', function(e)
 { 
          var id_entrada=$('#id_entrada').val();
          var id_medicamento=$('#id_medicamento').val();
        e.preventDefault();
        window.location = '/VistaSalidasContraEntrada/'+id_entrada+'/'+id_medicamento; 
     
 });



 $('#lista_salidas_medicamento').on('click','.reversar', function(e)
 {
    
     var id_salida             =$(this).attr('id_salida'); 
     var fecha_entrada            =$(this).attr('fecha_entrada');
     var fecha_vencimiento        =$(this).attr('fecha_vencimiento');

     var cantidad  =$(this).attr('cantidad');
     $("#modal_reversar").modal("show");
    
    
          $('#id_salida').val(id_salida);
          $('#cantidad').val(cantidad);
        
     

     var borrado='f';

     if(borrado=='f')
     {
          $('#borrado_reverso').removeAttr('checked')
        
               document.getElementById("btnActualizar_reversar").disabled=true;
     }
    else if(borrado=='t')
     {
          $('#borrado_reverso').attr('checked','checked');
          document.getElementById("btnActualizar_reversar").disabled=false;
     }
 

 });

 $(document).on('click','#btnActualizar_reversar', function(e)
 {
   e.preventDefault();
   // var fecha_entrada=$('#fecha').val();
   // var fecha_vencimiento=$('#fecha1').val();
   var cantidad=$('#cantidad').val();
   var id_salida= $('#id_salida').val();
   var id_entrada=$('#id_entrada').val();
   var id_medicamento=$('#id_medicamento').val();
   var estatus ='false';
   var observacion=$('#observacion').val();

   if($('#borrado_reverso').is(':checked'))
   {
        estatus='true';
   }
   else
   {
        estatus='false';
   }
 
  var data=
  {
          id_salida         :id_salida,
          id_entrada        :id_entrada,
          estatus           :estatus,   
          id_medicamento    :id_medicamento,
          cantidad:cantidad,
          observacion: observacion,
        // fecha_entrada     :fecha_entrada,
        // fecha_vencimiento :fecha_vencimiento,
        // cantidad          :cantidad,

  }
 
    //console.log(data);
 var url='/actualizar_salida';
 $.ajax
 ({
       url:url,
       method:'POST',
       data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
       dataType:'JSON',
       beforeSend:function(data)
       {
            //alert('Procesando Información ...');
       },
       success:function(data)
       {
        //console.log(data);
       
          if(data==1)
          {
            alert('Registro Actualizado');
          }
          else if(data==0)
          {
            alert('Error en la Incorporación del registro');
          }
          else{
               alert('Error, Ya exite una Nota de Entrega Asociada a esta Salida, Comuniquese con el Administrador del Sistema!!');   
          }
          window.location = '/vista_salidas/'+id_medicamento;             
       },
       error:function(xhr, status, errorThrown)
       {
          alert(xhr.status);
          alert(errorThrown);
       }
  });
   });


