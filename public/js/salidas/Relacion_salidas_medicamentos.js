/*
 *Este es el document ready
 */
$(function() {
    //$("#cronico").prop('disabled', true);

    //$("#btnfiltrocortesia").css("display", "none");

    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    id_categoria = 0;
    sexo = null;
    beneficiario = null;
    $('#medico').val('0')

    medico = 0;
    entes_adscritos=0;
    let id_medico = $('#cmbmedicos').val();
    let cedula_trabajador = $('#cedula_cortesia').val();
    if (desde == '' && hasta == '') {

        desde = 'null'
        hasta = 'null'
    }
    nombre_medico = 'Seleccione'
    nombre_categoria = 'Seleccione'


    cronicos = 'null';
    $('#categoria').val('0')
    llenar_combo_categoria(Event);
    llenar_combo_entes(Event);
    llenar_combo_medicos(Event);
    listar_salidas_medicamentos(id_categoria, cronicos, desde, hasta, sexo, beneficiario, medico, nombre_medico, nombre_categoria);
   // filtrar_entes_cortesia(id_categoria, cronicos, desde, hasta, sexo, beneficiario, medico, entes_adscritos, nombre_categoria);

    //$('#formulario')[0].reset();


});

function listar_salidas_medicamentos(id_categoria = 0, cronicos = null, desde = null, hasta = null, sexo = null, beneficiario = null, medico = 0, nombre_medico = 'null', nombre_categoria = 'null') {



    // data=new Date(desde);
    // let dataFormatada_desde = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
    // data=new Date(hasta);
    // let dataFormatada_hasta = ((data.getDate() + 1 )) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear(); 
    // Convertir la fecha
    var fechaOriginal = desde;
    var dataFormatada_desde = moment(fechaOriginal).format("DD-MM-YYYY");

    var fechaOriginal2 = hasta;
    var dataFormatada_hasta = moment(fechaOriginal2).format("DD-MM-YYYY");
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "/" + month + "/" + now.getFullYear();
    let ruta_imagen = rootpath
    var encabezado = '';
    var encabezado2 = '';
    id_categoria = (isNaN(parseInt(id_categoria))) ? 0 : parseInt(id_categoria);
    if (id_categoria != 0) {
        encabezado = encabezado + 'CATEGORIA:' + ' ' + ' ' + nombre_categoria + ' ' + ' ';
    }


    if (cronicos == 'true') {
        descr_cronicos = 'Si'
    } else if (cronicos == 'false') {
        descr_cronicos = 'NO'
    }

    if (cronicos != 'null') {
        encabezado = encabezado + 'CRONICO:' + ' ' + descr_cronicos + ' ' + ' ';
    }

    if (dataFormatada_desde != 'NaN/NaN/NaN' && dataFormatada_hasta != 'NaN/NaN/NaN') {
        encabezado = encabezado + 'DESDE:' + ' ' + dataFormatada_desde + ' ' + ' ' + 'HASTA' + ' ' + ' ' + dataFormatada_hasta + ' ' + ' ';
    }

    if (sexo != null) {
        if (sexo == 1) {
            descr_sexo = 'MASCULINO'
        } else if (sexo == 2) {
            descr_sexo = 'FEMENINO'
        }
        encabezado = encabezado + 'SEXO:' + ' ' + ' ' + descr_sexo + ' ' + ' ';
    }

    if (beneficiario != null) {
        if (beneficiario == 'T') {
            descr_beneficiario = 'TITULAR'
        } else if (beneficiario == 'F') {
            descr_beneficiario = 'FAMILIAR'
        } else if (beneficiario == 'C') {
            descr_beneficiario = 'CORTESIA'
        }
        encabezado2 = encabezado2 + 'TIPO:' + ' ' + ' ' + descr_beneficiario + ' ' + ' ';
    }

    if (nombre_medico != 'Seleccione') {
        encabezado2 = encabezado2 + 'MEDICO:' + ' ' + ' ' + nombre_medico + ' ' + ' ';
    }









    var table = $('#table_relacion_salidas').DataTable({

        dom: "Bfrtip",
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'
                },

            },

            buttons: [{
                    //definimos estilos del boton de pdf
                    extend: "pdf",
                    text: 'PDF',
                    orientation: 'landscape',
                    pageSize: 'LETTER',
                    className: 'btn-xs btn-dark',
                    //title:'RELACION SALIDAS MEDICAMENTOS',
                    header: true,
                    footer: true,
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8],

                    },
                    alignment: 'center',

                    customize: function(doc) {
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'
                        }
                        doc.styles['td:nth-child(2)'] = {
                                width: '200px',
                                'max-width': '200px'
                            },
                            doc.styles.tableHeader = {
                                fillColor: '#4c8aa0',
                                color: 'white',
                                alignment: 'center'
                            },
                            // Create a header
                            doc.pageMargins = [20, 110, 15, 20];
                        doc['header'] = (function(page, pages) {
                            doc.styles.title = {
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                            }
                            return {
                                columns: [{

                                        margin: [85, 5, 40, 20],

                                        image: ruta_imagen,
                                        width: 630,


                                    },
                                    {
                                        margin: [-530, 60, -25, 0],
                                        color: '#4c8aa0',
                                        fontSize: '24',
                                        alignment: 'center',
                                        text: 'RELACION SALIDAS DEL MEDICAMENTO',
                                        fontSize: 18,
                                    },
                                    {

                                        margin: [-615, 80, -25, 0],
                                        fontSize: 10,
                                        text: encabezado,
                                        fontSize: 10,
                                    },

                                    {

                                        margin: [-668, 95, -25, 0],
                                        fontSize: 10,
                                        text: encabezado2,
                                        fontSize: 10,
                                    },
                                ],
                            }
                        });
                        // Create a footer
                        doc['footer'] = (function(page, pages) {
                            return {
                                columns: [{
                                    alignment: 'center',
                                    text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }],
                            }
                        });

                    },
                },

                {
                    //definimos estilos del boton de excel
                    extend: "excel",
                    text: 'Excel',
                    className: 'btn-xs btn-dark',
                    title: 'RELACION SALIDAS MEDICAMENTOS',

                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                    },
                    excelStyles: {
                        "template": [
                            "blue_medium",
                            "header_blue",
                            "title_medium"
                        ]
                    },

                }
            ]
        },


        "order": [
            [5, "asc"]
        ],
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],

        "ordering": true,
        "info": true,
        "autoWidth": true,
        //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
        "ajax": {

            "url": base_url+"/Relacion_salidas_medicamentosPDF/" + id_categoria + '/' + cronicos + '/' + desde + '/' + hasta + '/' + sexo + '/' + beneficiario + '/' + medico,
            "type": "GET",
            dataSrc: ''
        },
        "columns": [

            //{data:'cedula_beneficiario'},

            { data: 'fecha_salida' },
            { data: 'descripcion' },
            { data: 'categoria_inventario' },
            { data: 'med_cronico' },
            { data: 'cantidad' },
            { data: 'beneficiario' },
            { data: 'tipo_beneficiario' },
            { data: 'cedula_titular' },
            { data: 'titular' },
            { data: 'usuario' },
            { data: 'medico' },


        ],

        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{

                "targets": [0],
                "visible": false,
                "searchable": false
            }, ]

        },
    });
}




function  filtrar_entes_cortesia(id_categoria, cronicos, desde, hasta, sexo, beneficiario, medico, entes_adscritos, nombre_medico, nombre_entes_adscritos, nombre_categoria){


    var encabezado = '';
    var encabezado2 = '';

    data = new Date(desde);
    let dataFormatada_desde = ((data.getDate() + 1)) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear();
    data = new Date(hasta);
    let dataFormatada_hasta = ((data.getDate() + 1)) + "/" + ((data.getMonth() + 1)) + "/" + data.getFullYear();


    id_categoria = (isNaN(parseInt(id_categoria))) ? 0 : parseInt(id_categoria);
    if (id_categoria != 0) {
        encabezado = encabezado + 'CATEGORIA:' + ' ' + ' ' + nombre_categoria + ' ' + ' ';
    }


    if (cronicos == 'true') {
        descr_cronicos = 'Si'
    } else if (cronicos == 'false') {
        descr_cronicos = 'NO'
    }

    if (cronicos != 'null') {
        encabezado = encabezado + 'CRONICO:' + ' ' + ' ' + descr_cronicos + ' ' + ' ';
    }

    if (dataFormatada_desde != 'NaN/NaN/NaN' && dataFormatada_hasta != 'NaN/NaN/NaN') {
        encabezado = encabezado + 'DESDE:' + ' ' + dataFormatada_desde + ' ' + ' ' + 'HASTA' + ' ' + ' ' + dataFormatada_hasta + ' ' + ' ';
    }


    if (sexo != null) {
        if (sexo == 1) {
            descr_sexo = 'MASCULINO'
        } else if (sexo == 2) {
            descr_sexo = 'FEMENINO'
        }
        encabezado = encabezado + 'SEXO:' + ' ' + ' ' + descr_sexo + ' ' + ' ';
    }

    if (beneficiario != null) {
        if (beneficiario == 'T') {
            descr_beneficiario = 'TITULAR'
        } else if (beneficiario == 'F') {
            descr_beneficiario = 'FAMILIAR'
        } else if (beneficiario == 'C') {
            descr_beneficiario = 'CORTESIA'
        }
        encabezado2 = encabezado2 + 'TIPO:' + ' ' + ' ' + descr_beneficiario + ' ' + ' ';
    }

    if (nombre_entes_adscritos != 'Seleccione') {
        encabezado2 = encabezado2 + 'ENTE ADSCRITO:' + ' ' + nombre_entes_adscritos + ' ' + ' ';
    }
    if (nombre_medico != 'Seleccione') {
        encabezado2 = encabezado2 + 'MEDICO:' + ' ' + nombre_medico + ' ' + ' ';
    }




    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var today = day + "/" + month + "/" + now.getFullYear();
    let ruta_imagen = rootpath
    var table = $('#table_relacion_salidas_entes').DataTable({

        dom: "Bfrtip",
        buttons: {
            dom: {
                button: {
                    className: 'btn-xs-xs'
                },

            },

            buttons: [{
                    //definimos estilos del boton de pdf
                    extend: "pdf",
                    text: 'PDF',
                    className: 'btn-xs btn-dark',
                    orientation: 'landscape',
                    pageSize: 'LETTER',
                    //title:'RELACION SALIDAS MEDICAMENTOS',
                    header: true,
                    footer: true,
                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6],

                    },
                    alignment: 'center',

                    customize: function(doc) {
                        //Remove the title created by datatTables
                        doc.content.splice(0, 1);
                        doc.styles.title = {
                            color: '#4c8aa0',
                            fontSize: '18',
                            alignment: 'center'
                        }
                        doc.styles['td:nth-child(2)'] = {
                                width: '100px',
                                'max-width': '100px'
                            },
                            doc.styles.tableHeader = {
                                fillColor: '#4c8aa0',
                                color: 'white',
                                alignment: 'center'
                            },
                            // Create a header
                            doc.pageMargins = [70, 110, 15, 50];
                        doc['header'] = (function(page, pages) {
                            doc.styles.title = {
                                color: '#4c8aa0',
                                fontSize: '18',
                                alignment: 'center',
                            }
                            return {
                                columns: [{

                                        margin: [70, 5, 40, 20],

                                        image: ruta_imagen,
                                        width: 630,

                                    },
                                    {
                                        margin: [-520, 60, -25, 0],
                                        color: '#4c8aa0',
                                        fontSize: '18',
                                        alignment: 'center',
                                        text: 'RELACION SALIDAS DEL MEDICAMENTO',
                                        fontSize: 12,
                                    },
                                    {
                                        margin: [-615, 80, -25, 0],
                                        fontSize: 10,
                                        text: encabezado,
                                        fontSize: 10,
                                    },

                                    {

                                        margin: [-668, 95, -25, 0],
                                        fontSize: 10,
                                        text: encabezado2,
                                        fontSize: 10,
                                    },


                                ],
                            }
                        });
                        // Create a footer
                        doc['footer'] = (function(page, pages) {
                            return {
                                columns: [{
                                    alignment: 'center',
                                    text: ['pagina ', { text: page.toString() }, ' of ', { text: pages.toString() }]
                                }],
                            }
                        });

                    },
                },

                {
                    //definimos estilos del boton de excel
                    extend: "excel",
                    text: 'Excel',
                    className: 'btn-xs btn-dark',
                    title: 'RELACION SALIDAS MEDICAMENTOS',

                    download: 'open',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5, 6],
                    },
                    excelStyles: {
                        "template": [
                            "blue_medium",
                            "header_blue",
                            "title_medium"
                        ]
                    },

                }
            ]
        },


        "order": [
            [0, "desc"]
        ],
        "paging": true,
        "lengthChange": true,

        dom: 'Blfrtip',
        "searching": true,
        "lengthMenu": [
            [10, 25, 50, -1],
            ['10', '25', '50', 'Todos']
        ],

        "ordering": true,
        "info": true,
        "autoWidth": true,
        //"dom": 'Bfrt<"col-md-6 inline"i> <"col-md-6 inline"p>',
        "ajax": {

            "url": "/Relacion_salidas_medicamentos_entes/" + id_categoria + '/' + cronicos + '/' + desde + '/' + hasta + '/' + sexo + '/' + beneficiario + '/' + medico + '/' + entes_adscritos,
            "type": "GET",
            dataSrc: ''
        },
        "columns": [

            //{data:'cedula_beneficiario'},
            { data: 'fecha_salida' },
            { data: 'descripcion' },
            { data: 'categoria_inventario' },
            { data: 'med_cronico' },
            { data: 'cantidad' },
            { data: 'beneficiario' },
            { data: 'tipo_beneficiario' },
            { data: 'cedula_trabajador' },
            { data: 'nombret' },
            { data: 'usuario' },
            { data: 'medico' },

        ],

        "language": {
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }, ]

        },
    });
}























function llenar_combo_categoria(e, id) {

    e.preventDefault
    url = '/listar_Categoria_activas';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            /*
            console.log(data);
            alert(data[0].id)
            alert(data[0].descripcion)
            */
            if (data.length >= 1) {

                $('#cmbCategoria').empty();
                $('#cmbCategoria').append('<option value=0  selected disabled>Seleccione</option>');
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#cmbCategoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {

                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $('#cmbCategoria').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');

                        } else {

                            $('#cmbCategoria').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}




function llenar_combo_entes(e, id) {

    e.preventDefault
    url = '/listar_entes_activos';
    $.ajax({
        url: url,
        method: 'GET',
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            if (data.length >= 1) {
                $('#entes_adscritos').empty();
                $('#entes_adscritos').append('<option value=0  selected disabled>Seleccione</option>');
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#entes_adscritos').append('<option value=' + item.id + '>' + item.descripcion + '</option>');

                    });
                } else {

                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $('#entes_adscritos').append('<option value=' + item.id + ' selected>' + item.descripcion + '</option>');

                        } else {

                            $('#entes_adscritos').append('<option value=' + item.id + '>' + item.descripcion + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}

























function llenar_combo_medicos(e, id) {

    e.preventDefault
    url = '/listar_medicos';
    $.ajax({
        url: url,
        method: 'GET',
        //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
        dataType: 'JSON',
        beforeSend: function(data) {},
        success: function(data) {
            /*
            console.log(data);
            alert(data[0].id)
            alert(data[0].descripcion)
            */
            if (data.length >= 1) {

                $('#medico').empty();
                $('#medico').append('<option value=0  selected disabled>Seleccione</option>');
                if (id === undefined) {
                    $.each(data, function(i, item) {
                        //console.log(data)
                        $('#medico').append('<option value=' + item.id + '>' + item.nombre + ', ' + '  ' + item.apellido + '</option>');

                    });
                } else {

                    $.each(data, function(i, item) {
                        if (item.id === id) {
                            $('#medico').append('<option value=' + item.id + ' selected>' + item.nombre + ', ' + '  ' + item.apellido + '</option>');

                        } else {

                            $('#medico').append('<option value=' + item.id + '>' + item.nombre + ', ' + '  ' + item.apellido + '</option>');
                        }
                    });
                }
            }
        },
        error: function(xhr, status, errorThrown) {
            alert(xhr.status);
            alert(errorThrown);
        }
    });
}



$("#cmbCategoria").on('change', function() {


    var id_categoria = $('#cmbCategoria').val();
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();


    if (id_categoria == 1) {
        //***DESELECCIONAR EL CHECKBOX**
        $("input[type=checkbox]").prop("checked", false);
        $(".labelcronico").css("display", "block");
        $("#cronico").css("display", "block");
        $("#cronico").prop('disabled', false);
        $("#labelcronico").prop('disabled', false);

    } else if (id_categoria != 1) {

        $("#cronico").prop('disabled', true);
        $("#labelcronico").prop('disabled', true);
        //$(".labelcronico").css("display", "none");
    }

});



$("#entes_adscritos").on('change', function() {


    var adscritos = $('#entes_adscritos').val();

    if (adscritos != 0) {
        $("#btnfiltrocortesia").css("display", "block");

        $("#btnfiltrar").css("display", "none");

    } else {

        $("#btnfiltrocortesia").css("display", "none");
        $("#btnfiltrar").css("display", "block");
    }

});


$("#beneficiario").on('change', function() {


    var beneficiario = $('#beneficiario').val();

    if (beneficiario != 3) {
        $("#filtrocortesia").css("visibility", "hidden");
        $("#btnfiltrar").css("visibility", "visible");
        $("#entes_adscritos").prop('disabled', true);

    } else {

        $("#filtrocortesia").css("visibility", "visible");
        $("#btnfiltrar").css("visibility", "hidden");
        $("#entes_adscritos").prop('disabled', false);
    }

});









$(document).on('click', '#btnfiltrar ', function(e) {

    var id_categoria = $('#cmbCategoria').val();
    if (id_categoria == null) {
        id_categoria = 0;
    }

    let nombre_categoria = $('#cmbCategoria option:selected').text();


    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let cronicos = $('#cronico').val();
    let sexo = $('#sexo').val();
    let beneficiario = $('#beneficiario').val();
    let medico = $('#medico').val();
    let nombre_medico = $('#medico option:selected').text();



    if (medico == null) {
        medico = 0;
    }

    if (entes_adscritos == null) {
        entes_adscritos = 0;
    }

    if (beneficiario == '1') {
        beneficiario = 'T'
    } else if (beneficiario == '2') {
        beneficiario = 'F'
    } else if (beneficiario == '3') {
        beneficiario = 'C'
    }

    //alert(beneficiario);
    if (desde == '') {
        desde = 'null'
    }
    if (hasta == '') {
        hasta = 'null'
    }
    if (cronicos == null) {
        cronicos = 'null';
    } else
    if (cronicos == '1') {
        cronicos = 'true';
    } else {
        cronicos = 'false';

    }

    $("#table_relacion_salidas").dataTable().fnDestroy();
   // $("#table_relacion_salidas_entes").dataTable().fnDestroy();
    $('.salidas_entes').hide();
    $('.salidas').show();
    //$("#table_relacion_salidas_entes").css("display", "none");
    //$("#table_relacion_salidas").css("display", "block")


    listar_salidas_medicamentos(id_categoria, cronicos, desde, hasta, sexo, beneficiario, medico, nombre_medico, nombre_categoria);

    if (desde == 'null' && hasta != 'null') {
        alert('DEDE INDICAR EL CAMPO DESDE');

    } else if (hasta == 'null' && desde != 'null') {
        alert('DEDE INDICAR EL CAMPO HASTA');
    } else if (hasta < desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
    }

});








$(document).on('click', '#filtrocortesia ', function(e) {
    e.preventDefault();

    var id_categoria = $('#cmbCategoria').val();
    if (id_categoria == null) {
        id_categoria = 0;
    }
    let nombre_categoria = $('#cmbCategoria option:selected').text();
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();
    let cronicos = $('#cronico').val();
    let sexo = $('#sexo').val();
    let beneficiario = $('#beneficiario').val();
    let medico = $('#medico').val();
    let entes_adscritos = $('#entes_adscritos').val();
    let nombre_entes_adscritos = $('#entes_adscritos option:selected').text();
    let nombre_medico = $('#medico option:selected').text();
    if (medico == null) {
        medico = 0;
    }
    if (entes_adscritos == null) {
        entes_adscritos = 0;
    }
    if (beneficiario == '1') {
        beneficiario = 'T'
    } else if (beneficiario == '2') {
        beneficiario = 'F'
    } else if (beneficiario == '3') {
        beneficiario = 'C'
    }
    //alert(beneficiario);
    if (desde == '') {
        desde = 'null'
    }
    if (hasta == '') {
        hasta = 'null'
    }
    if (cronicos == null) {
        cronicos = 'null';
    } else
    if (cronicos == '1') {
        cronicos = 'true';
    } else {
        cronicos = 'false';

    }
   
    $("#table_relacion_salidas_entes").dataTable().fnDestroy();
   // $("#table_relacion_salidas").dataTable().fnDestroy();
   // $("#table_relacion_salidas").css("display", "none")
  ///  $("#table_relacion_salidas_entes").css("display", "block")
   // $("#table_relacion_salidas_entes").css("display", "block")
   $('.salidas').hide();
   $('.salidas_entes').show();

    filtrar_entes_cortesia(id_categoria, cronicos, desde, hasta, sexo, beneficiario, medico, entes_adscritos, nombre_medico, nombre_entes_adscritos, nombre_categoria);

    if (desde == 'null' && hasta != 'null') {
        alert('DEDE INDICAR EL CAMPO DESDE');

    } else if (hasta == 'null' && desde != 'null') {
        alert('DEDE INDICAR EL CAMPO HASTA');
    } else if (hasta < desde) {
        alert('EL CAMPO DESDE ES MAYOR AL CAMPO HASTA')
    }

});




$(document).on('click', '#btnlimpiar', function(e) {
    e.preventDefault();
    $('#cronico').val('0');
    $('#cmbCategoria').val('0');
    $('input[type="date"]').val('');
    $('#sexo').val('0');
    $('#beneficiario').val('0');
    $('#medico').val('0');
    $("#entes_adscritos").val('0');
    $("#entes_adscritos").prop('disabled', true);
    let desde = $('#desde').val();
    let hasta = $('#hasta').val();

    if (desde == '' && hasta == '') {
        desde = 'null'
        hasta = 'null'

    }

    var entes_adscritos = $('#entes_adscritos').val();

    if (entes_adscritos == null) {
        entes_adscritos = 0;
    }

    if (entes_adscritos != 0) {
        $("#btnfiltrocortesia").css("display", "block");

        $("#btnfiltrar").css("display", "none");

    } else {

        $("#btnfiltrocortesia").css("display", "none");
        $("#btnfiltrar").css("display", "block");
    }
    $("#table_movimientos_medicamentos").css("display", "none");

    $("#table_relacion_salidas").dataTable().fnDestroy();
    listar_salidas_medicamentos(id_categoria, cronicos, desde, hasta, sexo, beneficiario, medico);

});



$("#entes_adscritos").on('change', function() {

    let entes_adscritos = $('#entes_adscritos').val();


});