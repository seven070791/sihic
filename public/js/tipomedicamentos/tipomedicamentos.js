/*
 *Este es el document ready
 */
 $(function()
 {
     listarTipoMedicamentos();
     llenar_combo_TipoInventario(Event);
 });
 /*
  * Función para definir datatable:
  */
 function listarTipoMedicamentos()
 {
      $('#table_tipo_medicamentos').DataTable
      (
       {
            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_tipo_medicamentos",
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
                {data:'id'},
                 {data:'descripcion'},
                 {data:'fecha_creacion'},                
                 {data:'estatus'},
                 {data:'categoria'},
                {orderable: true,
                 render:function(data, type, row)
                 {
                  return '<a href="javascript:;" class="btn btn-xs btn-secondary editar_tipo_medicamento" style=" font-size:1px" data-toggle="tooltip" title="Actualizar Descripción y Estatus del Tipo de Medicamento" id='+row.id+'  descripcion="'+row.descripcion+'" fecha_creacion='+row.fecha_creacion+' estatus='+row.estatus+' categoria_id='+row.categoria_id+'> <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
      $('#modalagregar').find('#btnGuardar').show();
      $('#modalagregar').find('#btnActualizar').hide();
      $('#modalagregar').find('#borrado').hide();
      $('#modalagregar').find('#activo').hide();
      $("#modalagregar").modal("show");
      $('#modalagregar').find('#tipomedicamento').val(''); 

 });

 /*
* Función para llenar el combo tipo de Inventario
*/    
function llenar_combo_TipoInventario(e,id)
{
      e.preventDefault;
      url='/listar_Categoria_activas';
      $.ajax
      ({
          url:url,
          method:'GET',
          //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
       
if(data.length>=1)
{
     $('#cmbInventario').empty();
     $('#cmbInventario').append('<option value=0 selected disabled >Seleccione</option>');     
     if(id===undefined)
     {
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbInventario').append('<option value='+item.id+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
               if(item.id===id)
               {
                    $('#cmbInventario').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                    $('#cmbInventario_anterior').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
                    
               }
               else
               {
                    $('#cmbInventario').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
}      
},
error:function(xhr, status, errorThrown)
{
    alert(xhr.status);
    alert(errorThrown);
}
});
}


 $('#lista_de_tipo_medicamentos').on('click','.editar_tipo_medicamento', function(e)
 {
     var id            =$(this).attr('id');
     var descripcion   =$(this).attr('descripcion');
     var fecha_creacion=$(this).attr('fecha_creacion');
     var estatus       =$(this).attr('estatus');
     var categoria_id      =$(this).attr('categoria_id');
     $('#id').val(id);
     $("#modalagregar").modal("show");
     $('#modalagregar').find('#btnGuardar').hide();
     $('#modalagregar').find('#btnActualizar').show();
     $('#modalagregar').find('#borrado').show();
     $('#modalagregar').find('#activo').show();  
     $('#id').val(id);
     $('#fecha').val(fecha_creacion)
     $('#tipomedicamento').val(descripcion)
     $('#tipomedicamento_anterior').val(descripcion)
     //Lleno la persiana de tipo de medicamento
     llenar_combo_TipoInventario(e,categoria_id);
     if(estatus=='Activo')
     {
     $('#borrado').attr('checked','checked');
     $('#borrado').val('false');
     $('#borrado_anterior').val('false');
     }
     if(estatus=='Eliminado')
     {
     $('#borrado').removeAttr('checked')
     $('#borrado').val('true')
     $('#borrado_anterior').val('true');
     }

         
 });
 


 $(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();


      var tipoMedicamento=$('#tipomedicamento').val();

      var id_tipoInventario =$('#cmbInventario').val();  
      if (tipoMedicamento=='')
      {
          alert('Por Favor Ingrese el Nombre Del Tipo de Medicamento');
      }
     
      else if (id_tipoInventario=='' || id_tipoInventario==null) {
           alert('Por Favor Ingrese un Tipo de Inventario');
       }
       else
       {
       
       
      var fechaRegistro=$('#fecha').val();

     var url='/agregarTipoMedicamento';
     var data=
     {
          id_tipoInventario :id_tipoInventario,
          tipoMedicamento:tipoMedicamento,
          fechaRegistro  :fechaRegistro,
     }
      $.ajax
      ({
          url:url,
          method:'POST',
         
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
               //alert(data);
               if(data=='1')
               {
                    alert('Registro Incorporado');
                    window.location = '/vistaTipoMedicamento';
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
      });

}
});

$(document).on('click','#btnActualizar', function(e)
{
     e.preventDefault();
   
     var id=$('#id').val();
     var tipoMedicamento=$('#tipomedicamento').val();
     var fechaRegistro  =$('#fecha').val();
     var id_tipoInventario =$('#cmbInventario').val();  
     var borrado='false';
     let descripcion_anterior=$('#tipomedicamento_anterior ').val();
    
     if (tipoMedicamento=='') {
          alert('Por Favor Ingrese un Tipo de Medicamento');
      }
      else
      {

     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }

     var objeto_anterior = {
          "Nombre": $('#tipomedicamento_anterior ').val(),
          "Bloqueado": $('#borrado_anterior ').val(),
          "Categoria": $('#cmbInventario_anterior option:selected').text(),       
          
      }
      var objeto_actual= {
          "Nombre": $('#tipomedicamento').val(),
          "Bloqueado": borrado,
          "Categoria": $('#cmbInventario option:selected').text(),
         
      }
      var camposModificados = [];
      for (var propiedad in objeto_anterior) {
          if (objeto_anterior.hasOwnProperty(propiedad)) {
              if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                  camposModificados.push({
                      propiedad: propiedad,
                      valorAnterior: objeto_anterior[propiedad],
                      valorNuevo: objeto_actual[propiedad]
                  });
              }
          }
      }
      if (camposModificados.length > 0) {
          var datos_modificados = camposModificados.map(function(campo) {
              let string_anterior = campo.valorAnterior;
              let patron = /option-/;
              if (patron.test(string_anterior)) {
                  campo.valorAnterior = string_anterior.replace(patron, "");
              }
              let string_Actual = campo.valorNuevo;
              let patron_Actual = /option-/;
              if (patron.test(string_Actual)) {
                  campo.valorNuevo = string_Actual.replace(patron, "");
              }
              campo.valorAnterior = campo.valorAnterior.trim();

              return campo.valorAnterior === '' ?
                  `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                  `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
          }).join(", ");
     }

     if (datos_modificados == undefined) {
          alert('NO SE HA REALIZADO NINGUNA MODIFICACION');

      } else 
      {

     var data=
     {
          id             :id,
          tipoMedicamento:tipoMedicamento,
          fechaRegistro  :fechaRegistro,
          borrado        :borrado,
          id_tipoInventario:id_tipoInventario,
          datos_modificados:datos_modificados,
          descripcion_anterior:descripcion_anterior
     }

    var url='/actualizarTipoMedicamento';
    $.ajax
    ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
             //alert(data);
             if(data===1)
             {
               alert('Registro Actualizado');
             }
             else
             {
               alert('Error en la Incorporación del registro');
             }
             window.location = '/vistaTipoMedicamento';             
          },
          error:function(xhr, status, errorThrown)
          {
             alert(xhr.status);
             alert(errorThrown);
          }
     });
      }
}
 });