/*
 *Este es el document ready
 */
 $(function()
 {
     listarunidad();
 });
 /*
  * Función para definir datatable:
  */
 function listarunidad()
 {
      $('#table_unidadmedida').DataTable
      (
       {
            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":"/listar_unidadmedida",
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
                {data:'id'},
                {data:'descripcion'},
                {data:'fecha_creacion'},                
                {data:'estatus'},
                {orderable: true,
                 render:function(data, type, row)
                 {
                    return '<a href="javascript:;" class="btn btn-xs btn-secondary editar_unidad_medida" style=" font-size:1px" data-toggle="tooltip" title="Actualizar Descripción y Estatus del Tipo de Medicamento" id='+row.id+' descripcion="'+row.descripcion+'" fecha_creacion='+row.fecha_creacion+' estatus='+row.estatus+'> <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }
 $('#btnAgregar').on('click',function(e)
 { 
      $('#modal').find('#btnGuardar').show();
      $('#modal').find('#btnActualizar').hide();
      $('#modal').find('#borrado').hide();
      $('#modal').find('#activo').hide();
      $("#modal").modal("show");
     

 });
 $('#lista_de_unida_medida').on('click','.editar_unidad_medida', function(e)
 {
     var id            =$(this).attr('id');
     var descripcion   =$(this).attr('descripcion');
     var fecha_creacion=$(this).attr('fecha_creacion');
     var estatus       =$(this).attr('estatus');
     $("#modal").modal("show");
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').show();
     $('#modal').find('#borrado').show();
     $('#modal').find('#activo').show();
     $('#id').val(id);
     $('#unidad').val(descripcion)
     $('#unidad_anterior').val(descripcion)
     $('#fecha').val(fecha_creacion)
     if(estatus=='Activo')
     {
          $('#borrado').attr('checked','checked');
          $('#borrado').val('false');
          $('#borrado_anterior').val('false');
     }
     if(estatus=='Eliminado')
     {
          $('#borrado').removeAttr('checked')
          $('#borrado').val('true')
          $('#borrado_anterior').val('false');
     }
 });




 $(document).on('click','#btnGuardar', function(e)
 {
      e.preventDefault();

      var unidad=$('#unidad').val();
      var fechaRegistro=$('#fecha').val();
     //  var activo               ='true';

/*       alert(tipoMedicamento);
      alert(fechaRegistro);
 */

      if (unidad=='') {
          alert('Por Favor Ingrese una Unidad');
      }
      else
      {
     var url='/agregarUnidadMedida';
     var data=
     {
          unidad:unidad,
          fechaRegistro  :fechaRegistro,
     }
      $.ajax
      ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
               //alert(data);
               if(data=='1')
               {
                    alert('Registro Incorporado');
                    window.location = '/vistaUnidadMedida';
               }
               else
               {
                    alert('Error en la Incorporación del Registro');
               }
          },
          error:function(xhr, status, errorThrown)
          {
               alert(xhr.status);
               alert(errorThrown);
          }
      });


     }
});
$(document).on('click','#btnActualizar', function(e)
{
   
     e.preventDefault();

     var id=$('#id').val();
     var unidad=$('#unidad').val();
     var fechaRegistro  =$('#fecha').val();
     var borrado        =$('#borrado').val();
     var borrado='false';
     let descripcion_anterior=$('#unidad_anterior ').val()
    
     if (unidad=='') {
          alert('Por Favor Ingrese una Unidad');
      }
      else
      {
     
     if($('#borrado').is(':checked'))
     {
          borrado='false';
     }
     else
     {
          borrado='true';
     }
     
     var objeto_anterior = {
          "Medida": $('#unidad_anterior ').val(),
          "Bloqueado": $('#borrado_anterior ').val(),
             
          
      }
      var objeto_actual= {
          "Medida": $('#unidad').val(),
          "Bloqueado": borrado,
         
      }
      var camposModificados = [];
      for (var propiedad in objeto_anterior) {
          if (objeto_anterior.hasOwnProperty(propiedad)) {
              if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                  camposModificados.push({
                      propiedad: propiedad,
                      valorAnterior: objeto_anterior[propiedad],
                      valorNuevo: objeto_actual[propiedad]
                  });
              }
          }
      }
      if (camposModificados.length > 0) {
          var datos_modificados = camposModificados.map(function(campo) {
              let string_anterior = campo.valorAnterior;
              let patron = /option-/;
              if (patron.test(string_anterior)) {
                  campo.valorAnterior = string_anterior.replace(patron, "");
              }
              let string_Actual = campo.valorNuevo;
              let patron_Actual = /option-/;
              if (patron.test(string_Actual)) {
                  campo.valorNuevo = string_Actual.replace(patron, "");
              }
              campo.valorAnterior = campo.valorAnterior.trim();

              return campo.valorAnterior === '' ?
                  `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                  `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
          }).join(", ");
     }
     if (datos_modificados == undefined) {
          alert('NO SE HA REALIZADO NINGUNA MODIFICACION');

      } else 
      {
     var data=
     {
          id             :id,
          unidad         :unidad,
          fechaRegistro  :fechaRegistro,
          borrado        :borrado,
          datos_modificados:datos_modificados,
          descripcion_anterior:descripcion_anterior
     }
     
     var url='/actualizar_medida';
     $.ajax
     ({
          url:url,
          method:'POST',
          data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
          dataType:'JSON',
          beforeSend:function(data)
          {
               //alert('Procesando Información ...');
          },
          success:function(data)
          {
          //alert(data);
          if(data===1)
          {
               alert('Registro Actualizado');
          }
          else
          {
               alert('Error en la Incorporación del registro');
          }
          window.location = '/vistaUnidadMedida';             
          },
          error:function(xhr, status, errorThrown)
          {
          alert(xhr.status);
          alert(errorThrown);
          }
     });
     }
}
});