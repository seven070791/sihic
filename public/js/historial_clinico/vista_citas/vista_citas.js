/*
 *Este es el document ready
 */
 $(function()
 {
    var cedulabeneficiario=19933177;
    var n_historial=$('#numeroHistorialDatatable').val();
   
   listarHistorialMedico(n_historial);
    $('#cmbmedicosreferidos').empty()
    $('#especialidad').empty()   // {orderable: true,
 
   

 });

 function  llenar_combo_especialidad(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_acceso_citas_especialidades';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {   
 if(data.length>=1)
 {
      $('#especialidad').empty();
      $('#especialidad').append('<option value=0>Seleccione</option>');     
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
              

               if(item.id_especialidad===id_especialidad)
              
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }
 function  llenar_combo_MedicoTratante(e,id)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {

 if(data.length>=1)
 {
      $('#cmbmedicotratante').empty();
      $('#cmbmedicotratante').append('<option value=0>Seleccione</option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              

               if(item.id===id)
              
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+' selected>'+item.nombre+'  '+item.apellido+'</option>');
               }
               else
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }




 
 function  llenar_combo_MedicoReferido(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
           
 if(data.length>=1)
 {
      $('#cmbmedicosreferidos').empty();
      $('#cmbmedicosreferidos').append('<option value=0  selected disabled>Medicos</option>');   
      verificar=7;  
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
        data=data.filter(dato=>dato.id_especialidad==id_especialidad);
        //console.log(buscar);
           $.each(data, function(i, item)
           {
            $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

              
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $("#especialidad").on('change', function(e)
 {
     document.getElementById("cmbmedicosreferidos").disabled=false;
    id_especialidad=$('#especialidad option:selected').val(); 

    llenar_combo_MedicoReferido(e,id_especialidad)
 }); 





/*
  * Función para definir datatable:
  */
function listarHistorialMedico(n_historial)
{
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear(); 
     $('#table_historial').DataTable( {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "select": true,
           "autoWidth": true,		
           			
           //"stateSave":true,
           "ajax":
           {
            "url":base_url+"/listar_Historial_citas/"+n_historial,
            "type":"GET",
            dataSrc:''
           },
           "columns":
           [
             // {data:'id'}, 
              {data:'n_historial'},     
              {data:'nombre'},
              {data:'especialidad'},
              {data:'fecha_creacion'},
              {data:'fecha_asistencia'},
              
          

              {orderable: true,
               render:function(data, type, row)
               {
                if(row.fecha_asistencia>=today &&row.asistencia=='f')
                 {
                    return '<button id="btnalerta"class="  btnalerta "disabled="disabled">'+'En espera'+'</button>'
                 }
                 else if(row.asistencia=='t')
                 {
                    return '<button id="btnalerta"class="  btnalerta "disabled="disabled">'+'SI'+'</button>'
                 }

                 {
                    return '<button id="btnalerta"class=" btnalerta "disabled="disabled">'+' NO'+'</button>'
                  
                 } 
              
               }
          },



              
   



               
           ],

           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
          
           }
      });

}
 $('#btnAgregar').on('click',function(e)
 {   
    $("#modal_citas").modal("show"); 
    
    llenar_combo_especialidad(Event)
    llenar_combo_MedicoTratante(Event)
    document.getElementById("cmbmedicosreferidos").disabled=true;

 });

 $('#labelenfermedad_actual').on('click',function(e)
 {   
   $(".efa").css("display", "block")
 });

 $('.labelealergias_medicamentos').on('click',function(e)
 {   
   $(".efa").css("display", "none")
 });



 $(document).on('click','#btnRegresar', function(e)
 {
     e.preventDefault();
     window.location = '/titulares/';
     
 });
 $(document).on('click','#btnGuardar', function(e)
 { 
     e.preventDefault();
     var tipo_beneficiario=$('#tipo_beneficiario').val();
     var id_especialidad=$('#especialidad').val();
     var id_medico=$('#cmbmedicosreferidos').val();
     var cedula=$('#cedulaT').val();
     if($('#especialidad').val()==0) 
     {
          alert('Debe Seleccionar una  especialidad');   
     }
     else if($('#cmbmedicosreferidos').val()>=1) 
     {
          let motivo_consulta=$('#motivo_consulta').val();
          if(motivo_consulta=='') 
          {
               alert('Debe Ingresar el Motivo de Consulta');
          }else
          {

                    var fecha_asis=$('#fecha_del_dia').val();
                    var arregloFecha = fecha_asis.split("-");
                    var anio = arregloFecha[2];
                    var mes = arregloFecha[1] ;
                    var dia = arregloFecha[0];
                    var fecha_asistencia=anio+"-"+mes +"-"+dia; 

                    //****Fecha del dia ****//
                    var now = new Date();
                    var day = ("0" + now.getDate()).slice(-2);
                    var month = ("0" + (now.getMonth() + 1)).slice(-2);
                    var today=now.getFullYear()+"-"+month+"-"+day;      
                  
                         var n_historial=$('#numeroHistorial').val();
                         var peso=$('#peso').val();
                         var talla=$('#talla').val();
                         var spo2=$('#spo2').val();
                         var frecuencia_c=$('#frecuencia_c').val();
                         var frecuencia_r=$('#frecuencia_r').val();
                         var temperatura=$('#temperatura').val();
                         var ta_alta=$('#ta_alta').val();
                         var ta_baja=$('#ta_baja').val();
                         let user_id=$('#id_user').val();
                         var imc=$('#imc').val();
                         var url='/agregar_cita_historial';
                         
                         var ruta_regreso='/vista_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;
                         if (peso=='') 
                         {
                              peso=0; 
                         }
                         if (talla=='') 
                         {
                              talla=0; 
                              }
                         if (spo2=='') 
                         {
                              spo2=0; 
                         }if (frecuencia_c=='') 
                         {
                              frecuencia_c=0; 
                         } if (frecuencia_r=='')
                         {
                              frecuencia_r=0; 
                         }if (temperatura=='') 
                         {
                              temperatura=0; 
                         } if (ta_alta=='') 
                         {
                              ta_alta=0; 
                         } if (ta_baja=='')
                         {
                              ta_baja=0; 
                         }
               
                         var data=
                         {
                         cedula:cedula,
                         fecha_asistencia:fecha_asistencia,
                         n_historial   :n_historial,
                         id_medico     :id_medico,
                         peso          :peso,               
                         talla         :talla,
                         spo2          :spo2,
                         frecuencia_c  :frecuencia_c,
                         frecuencia_r  :frecuencia_r,
                         temperatura   :temperatura,
                         ta_alta       :ta_alta,
                         ta_baja       :ta_baja,
                         imc:imc,
                         tipo_beneficiario:tipo_beneficiario,
                         user_id:user_id,
                         motivo_consulta:motivo_consulta,     
                         }
                    // //console.log(data);
                         $.ajax
                         ({
                              url:url,
                              method:'POST',
                              data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                              dataType:'JSON',
                              beforeSend:function(data)
                              {
                              },
                              success:function(data)
                              {
                                   
                                   if(data=='1')
                                   {
                                        alert('Registro Incorporado ');
                                   
                                        
                                        window.location = ruta_regreso;
                                   }
                                   else if(data=='0')
                                   {
                                        alert('Error en la Incorporación del Registro');
                                   }
                    
                                   else if(data=='2')
                                   {
                                        alert('La cedula posee Historial');
                                   }
                    
                    
                              },
                              error:function(xhr, status, errorThrown)
                              {
                                   alert(xhr.status);
                                   alert(errorThrown);
                              }
                         });
                    

               }

     }else 
     {
          alert('Debe Seleccionar un Medico'); 
     }
    

 
});   

$('#consultas_titulares').on('click','.Confirmar', function(e) 

{

     var asistencia =$(this).attr('asistencia'); 
     var now = new Date();
     var day = ("0" + now.getDate()).slice(-2);
     var month = ("0" + (now.getMonth() + 1)).slice(-2);
     var today= day+"/"+month+"/"+now.getFullYear(); 

     var fecha_asistencia=$(this).attr('fecha_asistencia'); 

           
     if(fecha_asistencia<today&& asistencia=='f'){
          alert('EL BENEFCIARIO HA PERDIDO SU CITA');
           window.location = '/vista_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;  
      }

  else if (fecha_asistencia!=today&& asistencia=='f') {
      alert('EL BENEFICIARIO NO POSEE CITA PARA EL :'+' '+today);
      window.location = '/vista_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;   
           
      }

    else if (fecha_asistencia>=today&& asistencia=='f') {
         // alert('en espera');
         asistencia='true';
          
     }
     else if (asistencia=='t') {
            //alert('si');  
            asistencia='true'; 
          }
    

     Swal.fire({
          title: 'ASISTENCIA DE CITA !!',
          icon:'warning',
          showCancelButton: true,
          cancelButtonColor:'#d33',
          confirmButtonText:'Confirmar',
                   
          }).then((result)=> {
                 if(result.isConfirmed){ 
     
                    Swal.fire(  
                                                 
                         'LA CITA HA SIDO CONFIRMADA!',
                         '...',
                         'success',
                         {
                              
                         }
                       )
                       var cedula=$(this).attr('cedula'); 
                       var tipo_beneficiario=$(this).attr('tipo_beneficiario'); 
                       var n_historial =$(this).attr('n_historial'); 
                       var consulta_id =$(this).attr('id'); 
                      
                       //var  asistencia='true'
                      
                       var data=
                       {
                         consulta_id:consulta_id,
                         asistencia     :asistencia,
                       }   
                       var url='/actualizar_asistencia';
                       $.ajax
                       ({
                            url:url,
                            method:'POST',
                            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                            dataType:'JSON',
                            beforeSend:function(data)
                            {
                                 //alert('Procesando Información ...');
                            },
                            success:function(data)
                            {
                               //alert(data);
                               if(data===1)
                               {
                                 
                                 
        
                               }
                               else
                               {
                                 alert('Error en la Incorporación del registro');
                               }
                               setTimeout(function () {
                                   window.location = '/vista_Citas/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;
                                }, 1500);
                               //window.location = '/datos_titular/'+cedula;             
                            },
                            error:function(xhr, status, errorThrown)
                            {
                               alert(xhr.status);
                               alert(errorThrown);
                            }
                      });  
                      
                    }
                   
               })  
     
        
 });






     

 

