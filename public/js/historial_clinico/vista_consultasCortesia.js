/*
 *Este es el document ready
 */
 $(function()
 {
    var cedulabeneficiario=19933177;
    var n_historial=$('#numeroHistorialDatatable').val();
    listarHistorialMedico(n_historial);
    $('#cmbmedicosreferidos').empty()
    $('#especialidad').empty() 
  
  
 });

 function  llenar_combo_especialidad(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_especialidades_activas';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {   
 if(data.length>=1)
 {
      $('#especialidad').empty();
      $('#especialidad').append('<option value=0>Seleccione</option>');     
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
              

               if(item.id_especialidad===id_especialidad)
              
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#especialidad').append('<option value='+item.id_especialidad+'>'+item.descripcion+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }
 function  llenar_combo_MedicoTratante(e,id)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {

 if(data.length>=1)
 {
      $('#cmbmedicotratante').empty();
      $('#cmbmedicotratante').append('<option value=0>Seleccione</option>');     
      if(id===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
          
          $.each(data, function(i, item)
          {
              

               if(item.id===id)
              
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+' selected>'+item.nombre+'  '+item.apellido+'</option>');
               }
               else
               {
                    $('#cmbmedicotratante').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');
               }
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }




 
 function  llenar_combo_MedicoReferido(e,id_especialidad)
 {

     e.preventDefault;
     
       url='/listar_medicos_activos';
        $.ajax
       ({
            url:url,
            method:'GET',
           //data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
           dataType:'JSON',
           beforeSend:function(data)
           {
           },
           success:function(data)
           {
           
 if(data.length>=1)
 {
      $('#cmbmedicosreferidos').empty();
      $('#cmbmedicosreferidos').append('<option value=0  selected disabled>Medicos</option>');   
      verificar=7;  
      if(id_especialidad===undefined)
     {
          
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

          });
     }
     else
     {
        data=data.filter(dato=>dato.id_especialidad==id_especialidad);
        //console.log(buscar);
           $.each(data, function(i, item)
           {
            $('#cmbmedicosreferidos').append('<option value='+item.id+'>'+item.nombre+'  '+item.apellido+'</option>');

              
          });
     }
 }      
 },
 error:function(xhr, status, errorThrown)
 {
     alert(xhr.status);
     alert(errorThrown);
 }
 });
 }

 $("#especialidad").on('change', function(e)
 {
     document.getElementById("cmbmedicosreferidos").disabled=false;
    id_especialidad=$('#especialidad option:selected').val(); 

    llenar_combo_MedicoReferido(e,id_especialidad)
 }); 





/*
  * Función para definir datatable:
  */
function listarHistorialMedico(n_historial)
{
     $('#table_historial').DataTable
     (
      {
           "order":[[0,"desc"]],					
           "paging":true,
           "info":true,
           "filter":true,
           "responsive": true,
           "autoWidth": true,					
           //"stateSave":true,
           "ajax":
           {
            "url":base_url+"/listar_Historial_consultas/"+n_historial,
            "type":"GET",
            dataSrc:''
           },
           "columns":
           [
              {data:'id'}, 
              {data:'n_historial'},     
              {data:'nombre'},
              {data:'especialidad'},
              {data:'fecha_asistencia'},

               // {orderable: true,
               //     render:function(data, type, row)
               //     {
               //      return '<a href="javascript:;" class="btn btn-xs btn-info Bloquear" style=" font-size:2px" data-toggle="tooltip" title="Eliminar" borrado='+row.borrado+' id='+row.id+' n_historial='+row.n_historial+' tipo_beneficiario='+row.tipo_beneficiario+' cedula="'+row.cedula+'" > <i class="material-icons " >delete_forever</i></a>'
               //      }
               // }
           ],
           "language":
           {
            "sProcessing":    "Procesando...",
            "sLengthMenu":    "Mostrar _MENU_ registros",
            "sZeroRecords":   "No se encontraron resultados",
            "sEmptyTable":    "Ningún dato disponible en esta tabla",
            "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": 
            {
             "sFirst":    "Primero",
             "sLast":    "Último",
             "sNext":    "Siguiente",
             "sPrevious": "Anterior"
            },
            "oAria":
            {
             "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
             "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "columnDefs": 
            [
               {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
            ],			
           }
      });

}
 $('#btnAgregar').on('click',function(e)
 {   
    $("#modal_citas").modal("show"); 
    
    llenar_combo_especialidad(Event)
    llenar_combo_MedicoTratante(Event)
    document.getElementById("cmbmedicosreferidos").disabled=true;

 });

 $('#labelenfermedad_actual').on('click',function(e)
 {   
   $(".efa").css("display", "block")
 });

 $('.labelealergias_medicamentos').on('click',function(e)
 {   
   $(".efa").css("display", "none")
 });



 $(document).on('click','#btnRegresar', function(e)
 {
     e.preventDefault();
     window.location = '/titulares/';
     
 });
 $(document).on('click','#btnGuardar', function(e)
 { 
     e.preventDefault(); 
     var id_especialidad=$('#especialidad').val();
     var id_medico=$('#cmbmedicosreferidos').val();
     var cedula=$('#cedulab').val();
     if($('#especialidad').val()==0) 
     {
          alert('Debe Seleccionar una  especialidad');   
     }
     else if($('#cmbmedicosreferidos').val()>=1) 
     {
          let motivo_consulta=$('#motivo_consulta').val();
          if(motivo_consulta=='') 
          {
               alert('Debe Ingresar el Motivo de Consulta');
          }else
          {
                    var tipo_beneficiario=$('#tipo_beneficiario').val();
                    var fecha_asistencia=$('#fecha_del_dia').val();
                    let fechaconvertida=moment(fecha_asistencia,'DD-MM-YYYY'); 
                    fechaconvertida=fechaconvertida.format('YYYY-MM-DD');

                   // alert(fechaconvertida);
                    var n_historial=$('#numeroHistorial').val();
                    var peso=$('#peso').val();
                    var imc=$('#imc').val();
                    var talla=$('#talla').val();
                    var spo2=$('#spo2').val();
                    var frecuencia_c=$('#frecuencia_c').val();
                    var frecuencia_r=$('#frecuencia_r').val();
                    var temperatura=$('#temperatura').val();
                    var ta_alta=$('#ta_alta').val();
                    var ta_baja=$('#ta_baja').val();
                    let user_id=$('#id_user').val();
                    var url='/agregar_consulta_historial';
                    var ruta_regreso = '/vista_ConsultasCortesia/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;
                    if (peso=='') 
                    {
                         peso=0; 
                    }
                    if (talla=='') 
                    {
                         talla=0; 
                         }
                    if (spo2=='') 
                    {
                         spo2=0; 
                    }if (frecuencia_c=='') 
                    {
                         frecuencia_c=0; 
                    } if (frecuencia_r=='')
                    {
                         frecuencia_r=0; 
                    }if (temperatura=='') 
                    {
                         temperatura=0; 
                    } if (ta_alta=='') 
                    {
                         ta_alta=0; 
                    } if (ta_baja=='')
                    {
                         ta_baja=0; 
                    }if (imc=='')
                    {
                         imc=0; 
                    }

                    var data=
                    {
                cedula:cedula,
                    fechaconvertida:fechaconvertida,
                    n_historial   :n_historial,
                    id_medico     :id_medico,
                    peso          :peso,               
                    talla         :talla,
                    spo2          :spo2,
                    frecuencia_c  :frecuencia_c,
                    frecuencia_r  :frecuencia_r,
                    temperatura   :temperatura,
                    ta_alta       :ta_alta,
                    ta_baja       :ta_baja,
                    tipo_beneficiario:tipo_beneficiario,
                    motivo_consulta:motivo_consulta,
                    imc:imc,    
                    user_id:user_id,
                    }
               //console.log(data);
                    $.ajax
                    ({
                         url:url,
                         method:'POST',
                         data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                         dataType:'JSON',
                         beforeSend:function(data)
                         {
                         },
                         success:function(data)
                         {
                              
                              if(data=='1')
                              {
                                   alert('Registro Incorporado ');
                              
                                   
                                   window.location = ruta_regreso;
                              }
                              else if(data=='0')
                              {
                                   alert('Error en la Incorporación del Registro');
                              }
               
                              else if(data=='2')
                              {
                                   alert('La cedula posee Historial');
                              }
               
               
                         },
                         error:function(xhr, status, errorThrown)
                         {
                              alert(xhr.status);
                              alert(errorThrown);
                         }
                    });
          }     

     }else 
     {
          alert('Debe Seleccionar un Medico'); 
     }
    

 
});   


$('#consultas_cortesia').on('click','.Bloquear', function(e) 

{
     Swal.fire({
          title: 'Borrar el Registro?',
          icon:'warning',
          showCancelButton: true,
          cancelButtonColor:'#d33',
          confirmButtonText:'Confirmar',
                   
          }).then((result)=> {
                 if(result.isConfirmed){ 
     
                    Swal.fire(  
                                                 
                         'BORRADO!',
                         'El Registro ha sido Borrado.',
                         'success',
                         {
                              
                         }
                       )
                       var cedula=$(this).attr('cedula'); 
                       var tipo_beneficiario=$(this).attr('tipo_beneficiario'); 
                       var n_historial =$(this).attr('n_historial'); 
                       var consulta_id =$(this).attr('id'); 
                       var borrado ='true'
                       var data=
                       {
                         
                         consulta_id:consulta_id,
                         borrado     :borrado,
                       }    
                       var url='/borrar_consulta';
                       $.ajax
                       ({
                            url:url,
                            method:'POST',
                            data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
                            dataType:'JSON',
                            beforeSend:function(data)
                            {
                                 //alert('Procesando Información ...');
                            },
                            success:function(data)
                            {
                               //alert(data);
                               if(data===1)
                               {
                                 
                                 
        
                               }
                               else
                               {
                                 alert('Error en la Incorporación del registro');
                               }
                               setTimeout(function () {
                                   window.location = '/vista_ConsultasCortesia/'+cedula+'/'+tipo_beneficiario+'/'+n_historial;
                                }, 1500);
                               //window.location = '/datos_titular/'+cedula;             
                            },
                            error:function(xhr, status, errorThrown)
                            {
                               alert(xhr.status);
                               alert(errorThrown);
                            }
                      });  
                      
                   }
                   
               })  
     
        
 });

  
 
 


 
 

