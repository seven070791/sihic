

/*
 *Este es el document ready
 */
 $(function()
 {
 
    llenar_combo_tipo_medicamento(Event);
    llenar_combo_presentacion(Event);
    llenar_combo_compuesto(Event); 
    llenar_combo_Control(Event);
    document.getElementById("cmbPresentacion").disabled=true;
    document.getElementById("cmbCompuesto").disabled=true;
    document.getElementById("descripcion").disabled=true;   
  
 });


 /*
* Función para ...
*/
function llenar_combo_tipo_medicamento(e, id)
{
     e.preventDefault
     url='/listar_medicamentos_activos';
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(JSON.stringify(data))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {
          if(data.length>1)
          { 
               $('#cmbTipoMedicamento').empty();
               $('#cmbTipoMedicamento').append('<option value=0>Seleccione</option>');    

                if(id===undefined)
                {
                    
                     $.each(data, function(i, item)
                    {
                         //console.log(data)
                          $('#cmbTipoMedicamento').append('<option value='+item.id+'>'+item.descripcion+'</option>');
                        
     
                    });
                }
    
          }      
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
         }
     });
}


/*
* Función para ...
*/    
function llenar_combo_presentacion(e, id)
{
      e.preventDefault;
      url='/listar_presentaciones_activas';
      $.ajax
      ({
          url:url,
          method:'GET',
          //data:{data:btoa(JSON.stringify(data))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
if(data.length>1)
{
     $('#cmbPresentacion').empty();
     $('#cmbPresentacion').append('<option value=0>Seleccione</option>');                    
     if(id===undefined)
     {
          $.each(data, function(i, item)
          {
          
               //console.log(data)
               $('#cmbPresentacion').append('<option value='+item.id+'>'+item.descripcion+'</option>');

          });
     }
     else
     {
          $.each(data, function(i, item)
          {
               if(item.id===id)
               {
                    $('#cmbPresentacion').append('<option value='+item.id+' selected>'+item.descripcion+'</option>');
               }
               else
               {
                    $('#cmbPresentacion').append('<option value='+item.id+'>'+item.descripcion+'</option>');
               }
          });
     }
}      
},
error:function(xhr, status, errorThrown)
{
    alert(xhr.status);
    alert(errorThrown);
}
});
}


/*
* Función para llenar combo ...
*/
function llenar_combo_compuesto(e,id)
{
     e.preventDefault
     url='/listar_Compuestos_activos';
     $.ajax
     ({
         url:url,
         method:'GET',
         //data:{data:btoa(JSON.stringify(data))},
         dataType:'JSON',
         beforeSend:function(data)
         {
         },
         success:function(data)
         {
        
          if(data.length>1)
          { 
               $('#cmbCompuesto').empty();
               $('#cmbCompuesto').append('<option value=0>Seleccione</option>');     

                if(id===undefined)
                {
                    
                     $.each(data, function(i, item)
                    {
                         //console.log(data)
                          $('#cmbCompuesto').append('<option value='+item.id+'>'+item.descripcioncompuesta+'</option>');
                        
     
                    });
                }
    
          }      
         },
         error:function(xhr, status, errorThrown)
         {
              alert(xhr.status);
              alert(errorThrown);
         }
     });
}



/*
* Función para llenar el combo del control de medicamento
*/    
function llenar_combo_Control(e,id)
{
      e.preventDefault;
      url='/listar_controles_activos';
      $.ajax
      ({
          url:url,
          method:'GET',
          //data:{data:btoa(JSON.stringify(data))},
          dataType:'JSON',
          beforeSend:function(data)
          {
          },
          success:function(data)
          {
     //alert(data.length);
if(data.length>1)
{
     $('#cmbControl').empty();
     $('#cmbControl').append('<option value=0>Seleccione</option>');     
     if(id===undefined)
     {
          $.each(data, function(i, item)
          {
               //console.log(data)
               $('#cmbControl').append('<option value='+item.id+'>'+item.descripcion+'</option>');

          });
     }
     
}      
},
error:function(xhr, status, errorThrown)
{
    alert(xhr.status);
    alert(errorThrown);
}
});
}


// ***************METODO PARA UNIR LA INFORMACION DE LOS COMBOS***********
// ******
// *****
$("#cmbTipoMedicamento").on('change', function()
{
     document.getElementById("cmbPresentacion").disabled=false;
     //document.getElementById("cmbCompuesto").disabled=false;
     document.getElementById("descripcion").disabled=false;
     if($('#cmbPresentacion').val()==0 && $('#cmbCompuesto').val()==0 )
     {
           descripcioncompleta=$('#cmbTipoMedicamento option:selected').text();   
       
     }
    
     else if($('#cmbPresentacion').val()>0  )
     {
          
          descripcioncompleta=$('#cmbTipoMedicamento option:selected').text();     
          descripcioncompleta=descripcioncompleta + '/'+$('#cmbPresentacion option:selected').text()+"/"+$('#cmbCompuesto option:selected').text();
          $('#descripcion').val(descripcioncompleta);

     }


    else {
         
          descripcioncompleta=$('#cmbTipoMedicamento option:selected').text();     
          descripcioncompleta=descripcioncompleta + '/'+$('#cmbPresentacion option:selected').text()+'/'+$('#cmbCompuesto option:selected').text();  

     }
     $('#descripcion').val(descripcioncompleta); 
     
    

     
});

$("#cmbPresentacion").change(function()
{
     document.getElementById("cmbCompuesto").disabled=false;
     if($('#cmbCompuesto').val()==0)
     {
          $('#descripcion').val('');
          descripcioncompleta=$('#cmbTipoMedicamento option:selected').text();     
          descripcioncompleta=descripcioncompleta + '/'+$('#cmbPresentacion option:selected').text()+""+"";
          $('#descripcion').val(descripcioncompleta);
     }else

     {
          descripcioncompleta=$('#cmbTipoMedicamento option:selected').text()+'/'+
          $('#cmbPresentacion option:selected').text()+'/'+$('#cmbCompuesto option:selected').text();
          $('#descripcion').val(descripcioncompleta);
     }
             
 })

$("#cmbCompuesto").change(function()
{
      
  
    // alert($('#cmbCompuesto option:selected').text());    
    descripcioncompleta=$('#cmbTipoMedicamento option:selected').text()+'/'+
    $('#cmbPresentacion option:selected').text()+'/'+$('#cmbCompuesto option:selected').text();

$('#descripcion').val(descripcioncompleta);
});

