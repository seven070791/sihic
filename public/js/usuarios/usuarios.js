/*
 *Este es el document ready
 */
 $(function()
 { 
   listar_usuarios();
    $('.form_master').css('display', 'none')

 });

 $('.usuarios').on('click',function(e)
 {   
 window.location="../User_controllers"
    
 });

 $('.auditoria').on('click',function(e)
 {   
 window.location="../AuditoriaController"
    
 });
 $('.inventario').on('click',function(e)
 {   
 window.location="/vistamedicamentos"
    
 });
  
 $(document).on('change','#tipousuario', function(e)
 {
    e.preventDefault();
    let tipousuario=$('#tipousuario').val();
     if (tipousuario=='2') {
        $('.form_master').css('display', 'none');   
        
     }    
     else{
        let check=false
        $('.form_master').css('display', 'none'); 
        check=$('#master').prop('checked',false);
        $('#validarmaster').val('false');
        
     }  
});

$(document).on('change','#master', function(e)
{
   e.preventDefault();
   
   let check=false
   check=$('#master').prop('checked');

   if (check===true) {
    $('#master').val('true')
       $('#validarmaster').val('true');
   }
   else if (check===false)
   {
    $('#master').val('false');
       $('#validarmaster').val('false');
   }
});



 /*
  * Función para definir datatable:
  */
 function listar_usuarios()
 {
      $('#table_usuarios').DataTable
      (
       {
            "order":[[0,"asc"]],					
            "paging":true,
            "info":true,
            "filter":true,
            "responsive": true,
            "autoWidth": true,					
            //"stateSave":true,
            "ajax":
            {
             "url":base_url+"/listar_usuarios",
             "type":"GET",
             dataSrc:''
            },
            "columns":
            [
                {data:'id'},
                {data:'cedula'},
                {data:'nombre'},                
                {data:'apellido'},
                {data:'usuario'},
                {data:'tipousuario'},
                {data:'estatus'},
                {orderable: true,
                 render:function(data, type, row)
                 {
                  return '<a href="javascript:;" class="btn btn-xs btn-secondary editar" style=" font-size:1px" data-toggle="tooltip" title="Editar"  nivel_usu='+row.nivel_usu+'  password='+row.password+'  id='+row.id+' cedula="'+row.cedula+'" nombre='+row.nombre+' apellido='+row.apellido+' tipousuario='+row.tipousuario+' usuario='+row.usuario+' estatus='+row.estatus+'> <i class="material-icons " >create</i></a>'
                    }
                }
            ],
            "language":
            {
             "sProcessing":    "Procesando...",
             "sLengthMenu":    "Mostrar _MENU_ registros",
             "sZeroRecords":   "No se encontraron resultados",
             "sEmptyTable":    "Ningún dato disponible en esta tabla",
             "sInfo":          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
             "sInfoEmpty":     "Mostrando registros del 0 al 0 de un total de 0 registros",
             "sInfoFiltered":  "(filtrado de un total de _MAX_ registros)",
             "sInfoPostFix":   "",
             "sSearch":        "Buscar:",
             "sUrl":           "",
             "sInfoThousands":  ",",
             "sLoadingRecords": "Cargando...",
             "oPaginate": 
             {
              "sFirst":    "Primero",
              "sLast":    "Último",
              "sNext":    "Siguiente",
              "sPrevious": "Anterior"
             },
             "oAria":
             {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
             },
             "columnDefs": 
             [
                {
                 "targets": [ 0 ],
                 "visible": false,
                 "searchable": false
             }
             ],			
            }
       });
 }





 $('#lista_usuarios').on('click','.editar', function(e)
 {
    
    var id            =$(this).attr('id');
    var tipousuario=$(this).attr('nivel_usu');
    llenar_nivel_usuario(e, tipousuario);
    var cedula   =$(this).attr('cedula').trim(); // Aquí se aplica trim()
    var nombre=$(this).attr('nombre').trim(); // Aquí se aplica trim()
    var apellido=$(this).attr('apellido').trim(); // Aquí se aplica trim()
    var usuario=$(this).attr('usuario').trim(); // Aquí se aplica trim()
    var estatus       =$(this).attr('estatus');
    var password       =$(this).attr('password').trim(); // Aquí se aplica trim()
     $("#modal").modal("show");
     $('#modal').find('#btnGuardar').hide();
     $('#modal').find('#btnActualizar').show();
     $('#modal').find('#borrado').show();
     $('#modal').find('#activo').show();
     $('#cedula').val(cedula);
     $('#id').val(id);
     $('#nombre').val(nombre);
     $('#apellido').val(apellido);
     $('#usuario').val(usuario);
     $('#tipousuario').val(tipousuario);
     $('#password').val(password);
     $('#cedula_anterior').val(cedula);
     $('#nombre_anterior').val(nombre);
     $('#apellido_anterior').val(apellido);
     $('#usuario_anterior').val(usuario);
     $('#password_anterior').val(password);
     

     if(estatus=='Eliminado')
     {
          $('#borrado').attr('checked','checked');
          $('#borrado').val('true');
          $('#borrado_anterior').val('t');
          
     }
     if(estatus=='Activo')
     {
          $('#borrado').removeAttr('checked')
          $('#borrado').val('false')
          $('#borrado_anterior').val('f');
     }
 });


 function llenar_nivel_usuario(e, tipousuario) {
   e.preventDefault
   url = '/listar_nivel_usuario';
   $.ajax({
       url: url,
       method: 'GET',
       dataType: 'JSON',
       beforeSend: function(data) {},
       success: function(data) {
           if (data.length >= 1) {
               $('#tipousuario').empty();
               $('#tipousuario').append('<option value=0  selected disabled>Seleccione</option>');
               if (tipousuario === undefined) {
                   $.each(data, function(i, item) {
                       
                       $('#tipousuario').append('<option value=' + item.id + '>' + item.nivel_usuario + '</option>');

                   });
               } else {

                   $.each(data, function(i, item) {
                       if (item.id === tipousuario) {
                           $('#tipousuario').append('<option value=' + item.id + ' selected>' + item.nivel_usuario + '</option>');
                           $('#tipousuario_anterior').append('<option value=' + item.id + ' selected>' + item.nivel_usuario + '</option>');
                           
                       } else {

                           $('#tipousuario').append('<option value=' + item.id + '>' + item.nivel_usuario + '</option>');
                       }
                   });
               }
           }
       },
       error: function(xhr, status, errorThrown) {
           alert(xhr.status);
           alert(errorThrown);
       }
   });
}

   
$(document).on('click','#btnActualizar', function(e)
{
     e.preventDefault();
    // $('#btnActualizar').prop('disabled', true);
     var cedula=$('#cedula').val();
     var nombre=$('#nombre').val();
     var apellido  =$('#apellido').val();
     var usuario =$('#usuario').val();  
     var password =$('#password').val(); 
     let tipousuario =$('#tipousuario ').val();
     let id =$('#id ').val();
     if($('#borrado').is(':checked'))
     {
          borrado='t';
     }
     else
     {
          borrado='f';
     }

     
     var objeto_anterior = {
        "Cedula": $('#cedula_anterior').val().trim(),
        "Nombre": $('#nombre_anterior').val().trim(),
        "Apellido": $('#apellido_anterior').val().trim(),
        "Usuario": $('#usuario_anterior').val().trim(),
        "NivelUsuario": $('#tipousuario_anterior option:selected').text(),
        "Bloqueado": $('#borrado_anterior').val().trim(),
        "Password": $('#password_anterior').val().trim()
    };
    
    var objeto_actual = {
        "Cedula": $('#cedula').val().trim(),
        "Nombre": $('#nombre').val().trim(),
        "Apellido": $('#apellido').val().trim(),
        "Usuario": $('#usuario').val().trim(),
        "NivelUsuario": $('#tipousuario option:selected').text(),
        "Bloqueado": borrado,
        "Password": $('#password').val().trim()
    };
    
  
        var camposModificados = [];
        for (var propiedad in objeto_anterior) {
            if (objeto_anterior.hasOwnProperty(propiedad)) {
                if (objeto_anterior[propiedad] !== objeto_actual[propiedad]) {
                    camposModificados.push({
                        propiedad: propiedad,
                        valorAnterior: objeto_anterior[propiedad],
                        valorNuevo: objeto_actual[propiedad]
                    });
                }
            }
        }
        var datos_modificados = camposModificados.map(function(campo) {
            if (campo.propiedad === "Password") {
                return `El campo: ${campo.propiedad} fue modificado.`;
            } else {
                let string_anterior = campo.valorAnterior;
                let patron = /option-/;
                if (patron.test(string_anterior)) {
                    campo.valorAnterior = string_anterior.replace(patron, "");
                }
                let string_Actual = campo.valorNuevo;
                let patron_Actual = /option-/;
                if (patron.test(string_Actual)) {
                    campo.valorNuevo = string_Actual.replace(patron, "");
                }
                campo.valorAnterior = campo.valorAnterior.trim();
    
                return campo.valorAnterior === '' ?
                    `Ingreso el Valor de ${campo.propiedad} = ${campo.valorNuevo}` :
                    `El campo: ${campo.propiedad} = ${campo.valorAnterior} fue modificado a: ${campo.valorNuevo}`;
            }
        }).join(", ");
        
    

 if (datos_modificados == '') {
   alert('NO SE HA REALIZADO NINGUNA MODIFICACION');

} else 
{

var data=
{
   cedula             :cedula,
   nombre:nombre,
   apellido  :apellido,
   usuario        :usuario,
   tipousuario:tipousuario,
   borrado:borrado,
   datos_modificados:datos_modificados,
   id:id,
   password:password
  
}

var url='/ActualizarUsuario';
$.ajax
({
   url:url,
   method:'POST',
   data:{data:btoa(unescape(encodeURIComponent(JSON.stringify(data))))},
   dataType:'JSON',
   beforeSend:function(data)
   {
        //alert('Procesando Información ...');
   },
   success:function(data)
   {
    
      if(data===1)
      {
        alert('Registro Actualizado');
      }
      else
      {
        alert('Error en la Incorporación del registro');
      }
      window.location = '/User_controllers';             
   },
   error:function(xhr, status, errorThrown)
   {
      alert(xhr.status);
      alert(errorThrown);
   }
});
 }

});

