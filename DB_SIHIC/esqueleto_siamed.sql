--
-- PostgreSQL database dump
--

-- Dumped from database version 14.10 (Ubuntu 14.10-1.pgdg22.04+1)
-- Dumped by pg_dump version 16.1 (Ubuntu 16.1-1.pgdg22.04+1)

-- Started on 2024-01-19 15:02:02 -04

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 16386)
-- Name: historial_clinico; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA historial_clinico;


ALTER SCHEMA historial_clinico OWNER TO postgres;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

-- *not* creating schema, since initdb creates it


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 210 (class 1259 OID 16389)
-- Name: abortos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.abortos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_abortos_numeros_romanos integer NOT NULL
);


ALTER TABLE historial_clinico.abortos OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16394)
-- Name: abortos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.abortos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.abortos_id_seq OWNER TO postgres;

--
-- TOC entry 4079 (class 0 OID 0)
-- Dependencies: 211
-- Name: abortos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.abortos_id_seq OWNED BY historial_clinico.abortos.id;


--
-- TOC entry 212 (class 1259 OID 16395)
-- Name: alergias_medicamentos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.alergias_medicamentos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.alergias_medicamentos OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16403)
-- Name: alergias_medicamentos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.alergias_medicamentos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.alergias_medicamentos_id_seq OWNER TO postgres;

--
-- TOC entry 4080 (class 0 OID 0)
-- Dependencies: 213
-- Name: alergias_medicamentos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.alergias_medicamentos_id_seq OWNED BY historial_clinico.alergias_medicamentos.id;


--
-- TOC entry 214 (class 1259 OID 16404)
-- Name: antecedentes_gineco_osbtetrico; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.antecedentes_gineco_osbtetrico (
    id integer NOT NULL,
    n_historial text,
    id_consulta integer NOT NULL,
    menarquia integer,
    sexarquia integer,
    nps integer,
    ciclo_mestrual text,
    f_u_r date,
    edad_gestacional text,
    dismenorrea boolean DEFAULT false NOT NULL,
    eumenorrea boolean DEFAULT false NOT NULL,
    anticonceptivo boolean DEFAULT false NOT NULL,
    detalles_anticon text,
    diu boolean DEFAULT false NOT NULL,
    t_cobre boolean DEFAULT false NOT NULL,
    mirena boolean DEFAULT false NOT NULL,
    aspiral boolean DEFAULT false NOT NULL,
    i_subdermico boolean DEFAULT false NOT NULL,
    otro text,
    t_colocacion date,
    ets boolean DEFAULT false NOT NULL,
    tipo_ets integer DEFAULT 0,
    detalle_ets text,
    citologia text,
    eco_mamario text,
    mamografia text,
    desintometria text,
    detalle_historia_obst text
);


ALTER TABLE historial_clinico.antecedentes_gineco_osbtetrico OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16419)
-- Name: antecedentes_gineco_osbtetrico_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.antecedentes_gineco_osbtetrico_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.antecedentes_gineco_osbtetrico_id_seq OWNER TO postgres;

--
-- TOC entry 4081 (class 0 OID 0)
-- Dependencies: 215
-- Name: antecedentes_gineco_osbtetrico_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.antecedentes_gineco_osbtetrico_id_seq OWNED BY historial_clinico.antecedentes_gineco_osbtetrico.id;


--
-- TOC entry 216 (class 1259 OID 16420)
-- Name: antecedentes_patologicosf; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.antecedentes_patologicosf (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.antecedentes_patologicosf OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16428)
-- Name: antecedentes_patologicosf_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.antecedentes_patologicosf_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.antecedentes_patologicosf_id_seq OWNER TO postgres;

--
-- TOC entry 4082 (class 0 OID 0)
-- Dependencies: 217
-- Name: antecedentes_patologicosf_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.antecedentes_patologicosf_id_seq OWNED BY historial_clinico.antecedentes_patologicosf.id;


--
-- TOC entry 218 (class 1259 OID 16429)
-- Name: antecedentes_patologicosp; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.antecedentes_patologicosp (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.antecedentes_patologicosp OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16437)
-- Name: antecedentes_patologicosp_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.antecedentes_patologicosp_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.antecedentes_patologicosp_id_seq OWNER TO postgres;

--
-- TOC entry 4083 (class 0 OID 0)
-- Dependencies: 219
-- Name: antecedentes_patologicosp_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.antecedentes_patologicosp_id_seq OWNED BY historial_clinico.antecedentes_patologicosp.id;


--
-- TOC entry 220 (class 1259 OID 16438)
-- Name: antecedentes_quirurgicos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.antecedentes_quirurgicos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.antecedentes_quirurgicos OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16446)
-- Name: antecedentes_quirurgicos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.antecedentes_quirurgicos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.antecedentes_quirurgicos_id_seq OWNER TO postgres;

--
-- TOC entry 4084 (class 0 OID 0)
-- Dependencies: 221
-- Name: antecedentes_quirurgicos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.antecedentes_quirurgicos_id_seq OWNED BY historial_clinico.antecedentes_quirurgicos.id;


--
-- TOC entry 222 (class 1259 OID 16447)
-- Name: cesarias; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.cesarias (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_cesarias_numeros_romanos integer NOT NULL
);


ALTER TABLE historial_clinico.cesarias OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 16452)
-- Name: cesarias_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.cesarias_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.cesarias_id_seq OWNER TO postgres;

--
-- TOC entry 4085 (class 0 OID 0)
-- Dependencies: 223
-- Name: cesarias_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.cesarias_id_seq OWNED BY historial_clinico.cesarias.id;


--
-- TOC entry 224 (class 1259 OID 16453)
-- Name: consultas; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.consultas (
    id integer NOT NULL,
    n_historial text,
    id_medico integer NOT NULL,
    peso integer,
    talla integer,
    spo2 integer,
    frecuencia_c integer,
    frecuencia_r integer,
    temperatura integer,
    tension_alta integer,
    tension_baja integer,
    imc integer,
    fecha_asistencia date,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    asistencia boolean DEFAULT false,
    consulta boolean DEFAULT true NOT NULL,
    tipo_beneficiario text,
    borrado boolean DEFAULT false,
    motivo_consulta text,
    user_id integer
);


ALTER TABLE historial_clinico.consultas OWNER TO postgres;

--
-- TOC entry 225 (class 1259 OID 16462)
-- Name: consultas_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.consultas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.consultas_id_seq OWNER TO postgres;

--
-- TOC entry 4086 (class 0 OID 0)
-- Dependencies: 225
-- Name: consultas_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.consultas_id_seq OWNED BY historial_clinico.consultas.id;


--
-- TOC entry 320 (class 1259 OID 21038)
-- Name: detalle_notas_entrega; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.detalle_notas_entrega (
    id integer NOT NULL,
    nota_entrega_id integer,
    salida_id integer,
    fecha date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.detalle_notas_entrega OWNER TO postgres;

--
-- TOC entry 319 (class 1259 OID 21037)
-- Name: detalle_notas_entrega_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.detalle_notas_entrega_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.detalle_notas_entrega_id_seq OWNER TO postgres;

--
-- TOC entry 4087 (class 0 OID 0)
-- Dependencies: 319
-- Name: detalle_notas_entrega_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.detalle_notas_entrega_id_seq OWNED BY historial_clinico.detalle_notas_entrega.id;


--
-- TOC entry 322 (class 1259 OID 21052)
-- Name: enfermedad_actual; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.enfermedad_actual (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.enfermedad_actual OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 21051)
-- Name: enfermedad_actual_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.enfermedad_actual_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.enfermedad_actual_id_seq OWNER TO postgres;

--
-- TOC entry 4088 (class 0 OID 0)
-- Dependencies: 321
-- Name: enfermedad_actual_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.enfermedad_actual_id_seq OWNED BY historial_clinico.enfermedad_actual.id;


--
-- TOC entry 226 (class 1259 OID 16478)
-- Name: enfermedades_sexuales; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.enfermedades_sexuales (
    id bigint NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.enfermedades_sexuales OWNER TO postgres;

--
-- TOC entry 227 (class 1259 OID 16484)
-- Name: enfermedades_sexuales_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.enfermedades_sexuales_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.enfermedades_sexuales_id_seq OWNER TO postgres;

--
-- TOC entry 4089 (class 0 OID 0)
-- Dependencies: 227
-- Name: enfermedades_sexuales_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.enfermedades_sexuales_id_seq OWNED BY historial_clinico.enfermedades_sexuales.id;


--
-- TOC entry 228 (class 1259 OID 16485)
-- Name: especialidades; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.especialidades (
    id_especialidad integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false,
    acceso_citas boolean DEFAULT false,
    caja_chica boolean DEFAULT false
);


ALTER TABLE historial_clinico.especialidades OWNER TO postgres;

--
-- TOC entry 229 (class 1259 OID 16493)
-- Name: especialidades_id_especialidad_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.especialidades_id_especialidad_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.especialidades_id_especialidad_seq OWNER TO postgres;

--
-- TOC entry 4090 (class 0 OID 0)
-- Dependencies: 229
-- Name: especialidades_id_especialidad_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.especialidades_id_especialidad_seq OWNED BY historial_clinico.especialidades.id_especialidad;


--
-- TOC entry 230 (class 1259 OID 16494)
-- Name: examen_fisico; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.examen_fisico (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.examen_fisico OWNER TO postgres;

--
-- TOC entry 231 (class 1259 OID 16502)
-- Name: examen_fisico_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.examen_fisico_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.examen_fisico_id_seq OWNER TO postgres;

--
-- TOC entry 4091 (class 0 OID 0)
-- Dependencies: 231
-- Name: examen_fisico_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.examen_fisico_id_seq OWNED BY historial_clinico.examen_fisico.id;


--
-- TOC entry 232 (class 1259 OID 16503)
-- Name: gestacion; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.gestacion (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_gestacion_numeros_romanos integer NOT NULL
);


ALTER TABLE historial_clinico.gestacion OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 16508)
-- Name: gestacion_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.gestacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.gestacion_id_seq OWNER TO postgres;

--
-- TOC entry 4092 (class 0 OID 0)
-- Dependencies: 233
-- Name: gestacion_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.gestacion_id_seq OWNED BY historial_clinico.gestacion.id;


--
-- TOC entry 234 (class 1259 OID 16509)
-- Name: habito_historial; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.habito_historial (
    id integer NOT NULL,
    historial_id integer NOT NULL,
    habito_id integer NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.habito_historial OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 16514)
-- Name: habito_historial_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.habito_historial_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.habito_historial_id_seq OWNER TO postgres;

--
-- TOC entry 4093 (class 0 OID 0)
-- Dependencies: 235
-- Name: habito_historial_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.habito_historial_id_seq OWNED BY historial_clinico.habito_historial.id;


--
-- TOC entry 236 (class 1259 OID 16515)
-- Name: habitos_psicosociales; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.habitos_psicosociales (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.habitos_psicosociales OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 16521)
-- Name: habitos_psicosociales_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.habitos_psicosociales_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.habitos_psicosociales_id_seq OWNER TO postgres;

--
-- TOC entry 4094 (class 0 OID 0)
-- Dependencies: 237
-- Name: habitos_psicosociales_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.habitos_psicosociales_id_seq OWNED BY historial_clinico.habitos_psicosociales.id;


--
-- TOC entry 238 (class 1259 OID 16522)
-- Name: historial_informativo; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.historial_informativo (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.historial_informativo OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 16528)
-- Name: historial_informativo_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.historial_informativo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.historial_informativo_id_seq OWNER TO postgres;

--
-- TOC entry 4095 (class 0 OID 0)
-- Dependencies: 239
-- Name: historial_informativo_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.historial_informativo_id_seq OWNED BY historial_clinico.historial_informativo.id;


--
-- TOC entry 240 (class 1259 OID 16529)
-- Name: historial_medico; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.historial_medico (
    id integer NOT NULL,
    n_historial text NOT NULL,
    cedula text NOT NULL,
    fecha_regist date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.historial_medico OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 16536)
-- Name: historial_medico_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.historial_medico_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.historial_medico_id_seq OWNER TO postgres;

--
-- TOC entry 4096 (class 0 OID 0)
-- Dependencies: 241
-- Name: historial_medico_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.historial_medico_id_seq OWNED BY historial_clinico.historial_medico.id;


--
-- TOC entry 242 (class 1259 OID 16537)
-- Name: impresion_diag; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.impresion_diag (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false,
    psicologia boolean DEFAULT false
);


ALTER TABLE historial_clinico.impresion_diag OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 16546)
-- Name: impresion_diag_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.impresion_diag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.impresion_diag_id_seq OWNER TO postgres;

--
-- TOC entry 4097 (class 0 OID 0)
-- Dependencies: 243
-- Name: impresion_diag_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.impresion_diag_id_seq OWNED BY historial_clinico.impresion_diag.id;


--
-- TOC entry 244 (class 1259 OID 16547)
-- Name: medicamentos_patologias; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.medicamentos_patologias (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false,
    id_especialidad integer
);


ALTER TABLE historial_clinico.medicamentos_patologias OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 16555)
-- Name: medicamentos_medicina_interna_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.medicamentos_medicina_interna_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.medicamentos_medicina_interna_id_seq OWNER TO postgres;

--
-- TOC entry 4098 (class 0 OID 0)
-- Dependencies: 245
-- Name: medicamentos_medicina_interna_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.medicamentos_medicina_interna_id_seq OWNED BY historial_clinico.medicamentos_patologias.id;


--
-- TOC entry 312 (class 1259 OID 20003)
-- Name: medicos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.medicos (
    id integer NOT NULL,
    cedula integer NOT NULL,
    nombre text NOT NULL,
    apellido text NOT NULL,
    telefono text,
    especialidad integer NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.medicos OWNER TO postgres;

--
-- TOC entry 311 (class 1259 OID 20002)
-- Name: medicos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.medicos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.medicos_id_seq OWNER TO postgres;

--
-- TOC entry 4099 (class 0 OID 0)
-- Dependencies: 311
-- Name: medicos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.medicos_id_seq OWNED BY historial_clinico.medicos.id;


--
-- TOC entry 246 (class 1259 OID 16563)
-- Name: metodos_anticonceptibos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.metodos_anticonceptibos (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.metodos_anticonceptibos OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 16569)
-- Name: metodos_anticonceptibos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.metodos_anticonceptibos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.metodos_anticonceptibos_id_seq OWNER TO postgres;

--
-- TOC entry 4100 (class 0 OID 0)
-- Dependencies: 247
-- Name: metodos_anticonceptibos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.metodos_anticonceptibos_id_seq OWNED BY historial_clinico.metodos_anticonceptibos.id;


--
-- TOC entry 318 (class 1259 OID 21027)
-- Name: notas_entregas; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.notas_entregas (
    id integer NOT NULL,
    n_historial text NOT NULL,
    fecha_registro date NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    usuario text,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.notas_entregas OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 21026)
-- Name: notas_entregas_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.notas_entregas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.notas_entregas_id_seq OWNER TO postgres;

--
-- TOC entry 4101 (class 0 OID 0)
-- Dependencies: 317
-- Name: notas_entregas_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.notas_entregas_id_seq OWNED BY historial_clinico.notas_entregas.id;


--
-- TOC entry 248 (class 1259 OID 16578)
-- Name: numeros_romanos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.numeros_romanos (
    id integer NOT NULL,
    descripcion character varying(4) NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE historial_clinico.numeros_romanos OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 16582)
-- Name: numeros_romanos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.numeros_romanos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.numeros_romanos_id_seq OWNER TO postgres;

--
-- TOC entry 4102 (class 0 OID 0)
-- Dependencies: 249
-- Name: numeros_romanos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.numeros_romanos_id_seq OWNED BY historial_clinico.numeros_romanos.id;


--
-- TOC entry 250 (class 1259 OID 16583)
-- Name: paraclinicos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.paraclinicos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.paraclinicos OWNER TO postgres;

--
-- TOC entry 251 (class 1259 OID 16591)
-- Name: paraclinicos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.paraclinicos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.paraclinicos_id_seq OWNER TO postgres;

--
-- TOC entry 4103 (class 0 OID 0)
-- Dependencies: 251
-- Name: paraclinicos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.paraclinicos_id_seq OWNED BY historial_clinico.paraclinicos.id;


--
-- TOC entry 252 (class 1259 OID 16592)
-- Name: partos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.partos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_partos_numeros_romanos integer NOT NULL
);


ALTER TABLE historial_clinico.partos OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 16597)
-- Name: partos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.partos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.partos_id_seq OWNER TO postgres;

--
-- TOC entry 4104 (class 0 OID 0)
-- Dependencies: 253
-- Name: partos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.partos_id_seq OWNED BY historial_clinico.partos.id;


--
-- TOC entry 254 (class 1259 OID 16598)
-- Name: plan; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.plan (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_consulta integer,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    fecha_actualizacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE historial_clinico.plan OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 16606)
-- Name: plan_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.plan_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.plan_id_seq OWNER TO postgres;

--
-- TOC entry 4105 (class 0 OID 0)
-- Dependencies: 255
-- Name: plan_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.plan_id_seq OWNED BY historial_clinico.plan.id;


--
-- TOC entry 256 (class 1259 OID 16607)
-- Name: psicologia_primaria; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.psicologia_primaria (
    id integer NOT NULL,
    n_historial text NOT NULL,
    fecha_seguimiento date DEFAULT CURRENT_DATE NOT NULL,
    evolucion text,
    borrado boolean DEFAULT false NOT NULL,
    id_medico integer,
    observacion text,
    id_consulta integer
);


ALTER TABLE historial_clinico.psicologia_primaria OWNER TO postgres;

--
-- TOC entry 257 (class 1259 OID 16614)
-- Name: psicologia_primaria_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.psicologia_primaria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.psicologia_primaria_id_seq OWNER TO postgres;

--
-- TOC entry 4106 (class 0 OID 0)
-- Dependencies: 257
-- Name: psicologia_primaria_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.psicologia_primaria_id_seq OWNED BY historial_clinico.psicologia_primaria.id;


--
-- TOC entry 258 (class 1259 OID 16615)
-- Name: reposos; Type: TABLE; Schema: historial_clinico; Owner: postgres
--

CREATE TABLE historial_clinico.reposos (
    id integer NOT NULL,
    n_historial text NOT NULL,
    id_medico integer NOT NULL,
    fecha_desde date NOT NULL,
    fecha_hasta date NOT NULL,
    motivo text NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE,
    horas integer
);


ALTER TABLE historial_clinico.reposos OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 16622)
-- Name: reposos_id_seq; Type: SEQUENCE; Schema: historial_clinico; Owner: postgres
--

CREATE SEQUENCE historial_clinico.reposos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE historial_clinico.reposos_id_seq OWNER TO postgres;

--
-- TOC entry 4107 (class 0 OID 0)
-- Dependencies: 259
-- Name: reposos_id_seq; Type: SEQUENCE OWNED BY; Schema: historial_clinico; Owner: postgres
--

ALTER SEQUENCE historial_clinico.reposos_id_seq OWNED BY historial_clinico.reposos.id;


--
-- TOC entry 260 (class 1259 OID 16798)
-- Name: auditoria; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auditoria (
    id integer NOT NULL,
    fecha_sesion date DEFAULT CURRENT_DATE NOT NULL,
    id_usuario integer,
    accion text,
    id_medicamento integer,
    cantidad integer,
    hora character(11)
);


ALTER TABLE public.auditoria OWNER TO postgres;

--
-- TOC entry 261 (class 1259 OID 16804)
-- Name: auditoria_de_sistema; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.auditoria_de_sistema (
    id integer NOT NULL,
    user_id integer NOT NULL,
    accion text NOT NULL,
    fecha date DEFAULT CURRENT_DATE NOT NULL,
    hora character(11)
);


ALTER TABLE public.auditoria_de_sistema OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 16810)
-- Name: auditoria_de_sistema_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auditoria_de_sistema_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auditoria_de_sistema_id_seq OWNER TO postgres;

--
-- TOC entry 4108 (class 0 OID 0)
-- Dependencies: 262
-- Name: auditoria_de_sistema_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auditoria_de_sistema_id_seq OWNED BY public.auditoria_de_sistema.id;


--
-- TOC entry 263 (class 1259 OID 16811)
-- Name: auditoria_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.auditoria_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.auditoria_id_seq OWNER TO postgres;

--
-- TOC entry 4109 (class 0 OID 0)
-- Dependencies: 263
-- Name: auditoria_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.auditoria_id_seq OWNED BY public.auditoria.id;


--
-- TOC entry 264 (class 1259 OID 16812)
-- Name: categoria_inventario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.categoria_inventario (
    id integer NOT NULL,
    descripcion text,
    fecha_creacion date DEFAULT CURRENT_DATE,
    borrado boolean DEFAULT false
);


ALTER TABLE public.categoria_inventario OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 16819)
-- Name: categoria_inventario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.categoria_inventario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.categoria_inventario_id_seq OWNER TO postgres;

--
-- TOC entry 4110 (class 0 OID 0)
-- Dependencies: 265
-- Name: categoria_inventario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.categoria_inventario_id_seq OWNED BY public.categoria_inventario.id;


--
-- TOC entry 266 (class 1259 OID 16820)
-- Name: compuestos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.compuestos (
    id_compuesto integer NOT NULL,
    descripcioncompuesta text,
    id_tipo_medicamento integer,
    id_unidad_medida integer,
    cantidad integer,
    borrado boolean DEFAULT false NOT NULL,
    id_medicamento integer NOT NULL
);


ALTER TABLE public.compuestos OWNER TO postgres;

--
-- TOC entry 267 (class 1259 OID 16826)
-- Name: compuestos_id_compuesto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.compuestos_id_compuesto_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.compuestos_id_compuesto_seq OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 16827)
-- Name: compuestos_id_compuesto_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.compuestos_id_compuesto_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.compuestos_id_compuesto_seq1 OWNER TO postgres;

--
-- TOC entry 4111 (class 0 OID 0)
-- Dependencies: 268
-- Name: compuestos_id_compuesto_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.compuestos_id_compuesto_seq1 OWNED BY public.compuestos.id_compuesto;


--
-- TOC entry 269 (class 1259 OID 16828)
-- Name: control; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.control (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.control OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 16835)
-- Name: control_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.control_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.control_id_seq OWNER TO postgres;

--
-- TOC entry 4112 (class 0 OID 0)
-- Dependencies: 270
-- Name: control_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.control_id_seq OWNED BY public.control.id;


--
-- TOC entry 271 (class 1259 OID 16836)
-- Name: cortesia; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cortesia (
    id integer NOT NULL,
    cedula_trabajador integer,
    nacionalidad character varying(1),
    nombre text,
    apellido text,
    telefono text,
    sexo integer,
    fecha_nac_cortesia date DEFAULT CURRENT_DATE,
    cedula text NOT NULL,
    observacion text,
    borrado boolean DEFAULT false NOT NULL,
    tipo_de_sangre integer,
    estado_civil integer,
    ocupacion_id integer,
    grado_intruccion_id integer,
    adscrito boolean DEFAULT false
);


ALTER TABLE public.cortesia OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 16844)
-- Name: cortesia_adscrito_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cortesia_adscrito_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.cortesia_adscrito_id_seq OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 16845)
-- Name: cortesia_adscrito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cortesia_adscrito (
    id bigint DEFAULT nextval('public.cortesia_adscrito_id_seq'::regclass) NOT NULL,
    id_cortesia integer NOT NULL,
    id_adscrito integer NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.cortesia_adscrito OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 16851)
-- Name: cortesia_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cortesia_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.cortesia_id_seq OWNER TO postgres;

--
-- TOC entry 4113 (class 0 OID 0)
-- Dependencies: 274
-- Name: cortesia_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cortesia_id_seq OWNED BY public.cortesia.id;


--
-- TOC entry 275 (class 1259 OID 16852)
-- Name: datos_titulares; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.datos_titulares (
    cedula_trabajador text NOT NULL,
    nacionalidad text NOT NULL,
    nombre text,
    apellido text,
    telefono text,
    sexo text NOT NULL,
    fecha_nacimiento date NOT NULL,
    tipo_de_personal text,
    ubicacion_administrativa text,
    borrado boolean DEFAULT false,
    id integer NOT NULL
);


ALTER TABLE public.datos_titulares OWNER TO postgres;

--
-- TOC entry 276 (class 1259 OID 16858)
-- Name: datos_titulares_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.datos_titulares_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.datos_titulares_id_seq OWNER TO postgres;

--
-- TOC entry 4114 (class 0 OID 0)
-- Dependencies: 276
-- Name: datos_titulares_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.datos_titulares_id_seq OWNED BY public.datos_titulares.id;


--
-- TOC entry 277 (class 1259 OID 16859)
-- Name: ente_adscrito_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ente_adscrito_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.ente_adscrito_id_seq OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 16860)
-- Name: ente_adscrito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ente_adscrito (
    id integer DEFAULT nextval('public.ente_adscrito_id_seq'::regclass) NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    rif text
);


ALTER TABLE public.ente_adscrito OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 21005)
-- Name: entradas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.entradas (
    id integer NOT NULL,
    user_id integer NOT NULL,
    fecha_entrada date DEFAULT CURRENT_DATE,
    fecha_vencimiento date DEFAULT CURRENT_DATE,
    id_medicamento integer NOT NULL,
    cantidad integer NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.entradas OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 21004)
-- Name: entradas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.entradas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.entradas_id_seq OWNER TO postgres;

--
-- TOC entry 4115 (class 0 OID 0)
-- Dependencies: 313
-- Name: entradas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.entradas_id_seq OWNED BY public.entradas.id;


--
-- TOC entry 279 (class 1259 OID 16880)
-- Name: estado_civil; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.estado_civil (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.estado_civil OWNER TO postgres;

--
-- TOC entry 280 (class 1259 OID 16885)
-- Name: estado_civil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.estado_civil_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.estado_civil_id_seq OWNER TO postgres;

--
-- TOC entry 4116 (class 0 OID 0)
-- Dependencies: 280
-- Name: estado_civil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.estado_civil_id_seq OWNED BY public.estado_civil.id;


--
-- TOC entry 324 (class 1259 OID 21072)
-- Name: familiares; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.familiares (
    id integer NOT NULL,
    cedula_trabajador integer,
    nombre text,
    apellido text,
    telefono text,
    sexo integer,
    fecha_nac_familiares date,
    parentesco_id integer,
    cedula text NOT NULL,
    tipo_de_sangre integer,
    estado_civil integer,
    grado_intruccion_id integer,
    ocupacion_id integer,
    borrado boolean DEFAULT false
);


ALTER TABLE public.familiares OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 16902)
-- Name: familiares_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.familiares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.familiares_id_seq OWNER TO postgres;

--
-- TOC entry 323 (class 1259 OID 21071)
-- Name: familiares_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.familiares_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.familiares_id_seq1 OWNER TO postgres;

--
-- TOC entry 4117 (class 0 OID 0)
-- Dependencies: 323
-- Name: familiares_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.familiares_id_seq1 OWNED BY public.familiares.id;


--
-- TOC entry 282 (class 1259 OID 16909)
-- Name: grado_instruccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grado_instruccion (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.grado_instruccion OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 16914)
-- Name: grado_instruccion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grado_instruccion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.grado_instruccion_id_seq OWNER TO postgres;

--
-- TOC entry 4118 (class 0 OID 0)
-- Dependencies: 283
-- Name: grado_instruccion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.grado_instruccion_id_seq OWNED BY public.grado_instruccion.id;


--
-- TOC entry 284 (class 1259 OID 16915)
-- Name: grupo_usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grupo_usuario (
    id integer NOT NULL,
    nivel_usuario character varying(100) NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.grupo_usuario OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 16929)
-- Name: medicamentos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.medicamentos (
    id integer NOT NULL,
    id_tipo_medicamento integer NOT NULL,
    id_control integer NOT NULL,
    id_presentacion integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    stock_minimo integer DEFAULT 10,
    med_cronico boolean DEFAULT false
);


ALTER TABLE public.medicamentos OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 16938)
-- Name: medicamentos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.medicamentos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.medicamentos_id_seq OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 16939)
-- Name: medicamentos_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.medicamentos_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.medicamentos_id_seq1 OWNER TO postgres;

--
-- TOC entry 4119 (class 0 OID 0)
-- Dependencies: 287
-- Name: medicamentos_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.medicamentos_id_seq1 OWNED BY public.medicamentos.id;


--
-- TOC entry 288 (class 1259 OID 16953)
-- Name: nivel_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nivel_usuario_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.nivel_usuario_id_seq OWNER TO postgres;

--
-- TOC entry 4120 (class 0 OID 0)
-- Dependencies: 288
-- Name: nivel_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nivel_usuario_id_seq OWNED BY public.grupo_usuario.id;


--
-- TOC entry 289 (class 1259 OID 16954)
-- Name: ocupacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ocupacion (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.ocupacion OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 16959)
-- Name: ocupacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ocupacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.ocupacion_id_seq OWNER TO postgres;

--
-- TOC entry 4121 (class 0 OID 0)
-- Dependencies: 290
-- Name: ocupacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ocupacion_id_seq OWNED BY public.ocupacion.id;


--
-- TOC entry 291 (class 1259 OID 16960)
-- Name: parentesco; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.parentesco (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.parentesco OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 16966)
-- Name: parentesco_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.parentesco_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.parentesco_id_seq OWNER TO postgres;

--
-- TOC entry 4122 (class 0 OID 0)
-- Dependencies: 292
-- Name: parentesco_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.parentesco_id_seq OWNED BY public.parentesco.id;


--
-- TOC entry 293 (class 1259 OID 16967)
-- Name: pre_tipo_medicamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pre_tipo_medicamento (
    medicamento text
);


ALTER TABLE public.pre_tipo_medicamento OWNER TO postgres;

--
-- TOC entry 294 (class 1259 OID 16972)
-- Name: presentacion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.presentacion (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false
);


ALTER TABLE public.presentacion OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 16979)
-- Name: presentacion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.presentacion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.presentacion_id_seq OWNER TO postgres;

--
-- TOC entry 4123 (class 0 OID 0)
-- Dependencies: 295
-- Name: presentacion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.presentacion_id_seq OWNED BY public.presentacion.id;


--
-- TOC entry 296 (class 1259 OID 16980)
-- Name: reversos; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.reversos (
    id integer NOT NULL,
    tipo_de_reverso text,
    observacion text,
    id_usuario integer NOT NULL,
    id_medicamento integer NOT NULL,
    cantidad integer NOT NULL,
    fecha_reverso date DEFAULT CURRENT_DATE,
    id_entrada integer
);


ALTER TABLE public.reversos OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 16986)
-- Name: reversos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.reversos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.reversos_id_seq OWNER TO postgres;

--
-- TOC entry 4124 (class 0 OID 0)
-- Dependencies: 297
-- Name: reversos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.reversos_id_seq OWNED BY public.reversos.id;


--
-- TOC entry 316 (class 1259 OID 21015)
-- Name: salidas; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.salidas (
    id integer NOT NULL,
    user_id integer NOT NULL,
    fecha_salida date DEFAULT CURRENT_DATE,
    id_medicamento integer,
    id_entrada integer NOT NULL,
    cedula_beneficiario text NOT NULL,
    cantidad integer NOT NULL,
    tipo_beneficiario text NOT NULL,
    borrado boolean DEFAULT false,
    control integer DEFAULT 0,
    id_medico integer
);


ALTER TABLE public.salidas OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 21014)
-- Name: salidas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.salidas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.salidas_id_seq OWNER TO postgres;

--
-- TOC entry 4125 (class 0 OID 0)
-- Dependencies: 315
-- Name: salidas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.salidas_id_seq OWNED BY public.salidas.id;


--
-- TOC entry 298 (class 1259 OID 16999)
-- Name: tipo_de_sangre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_de_sangre (
    id integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.tipo_de_sangre OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 17004)
-- Name: tipo_de_sangre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_de_sangre_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.tipo_de_sangre_id_seq OWNER TO postgres;

--
-- TOC entry 4126 (class 0 OID 0)
-- Dependencies: 299
-- Name: tipo_de_sangre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_de_sangre_id_seq OWNED BY public.tipo_de_sangre.id;


--
-- TOC entry 300 (class 1259 OID 17005)
-- Name: tipo_medicamento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_medicamento (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL,
    categoria_id integer DEFAULT 1
);


ALTER TABLE public.tipo_medicamento OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 17013)
-- Name: tipo_medicamento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_medicamento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.tipo_medicamento_id_seq OWNER TO postgres;

--
-- TOC entry 4127 (class 0 OID 0)
-- Dependencies: 301
-- Name: tipo_medicamento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_medicamento_id_seq OWNED BY public.tipo_medicamento.id;


--
-- TOC entry 302 (class 1259 OID 17014)
-- Name: titulares; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.titulares (
    id bigint NOT NULL,
    cedula_trabajador integer NOT NULL,
    nacionalidad text NOT NULL,
    nombre text NOT NULL,
    apellido text NOT NULL,
    telefono text DEFAULT 'NO TIENE'::text NOT NULL,
    sexo integer NOT NULL,
    fecha_nacimiento date NOT NULL,
    tipo_de_personal text NOT NULL,
    ubicacion_administrativa text,
    borrado boolean DEFAULT false NOT NULL,
    estado_civil integer,
    tipo_de_sangre integer,
    grado_intruccion_id integer,
    ocupacion_id integer
);


ALTER TABLE public.titulares OWNER TO postgres;

--
-- TOC entry 4128 (class 0 OID 0)
-- Dependencies: 302
-- Name: COLUMN titulares.sexo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.titulares.sexo IS '1="M",2="F"';


--
-- TOC entry 303 (class 1259 OID 17021)
-- Name: titulares_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.titulares_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.titulares_id_seq OWNER TO postgres;

--
-- TOC entry 304 (class 1259 OID 17022)
-- Name: titulares_id_seq1; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.titulares_id_seq1
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.titulares_id_seq1 OWNER TO postgres;

--
-- TOC entry 4129 (class 0 OID 0)
-- Dependencies: 304
-- Name: titulares_id_seq1; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.titulares_id_seq1 OWNED BY public.titulares.id;


--
-- TOC entry 305 (class 1259 OID 17038)
-- Name: ubicacion_administrativa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.ubicacion_administrativa (
    id integer NOT NULL,
    descripcion text NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.ubicacion_administrativa OWNER TO postgres;

--
-- TOC entry 306 (class 1259 OID 17044)
-- Name: ubicacion_administrativa_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.ubicacion_administrativa_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.ubicacion_administrativa_id_seq OWNER TO postgres;

--
-- TOC entry 4130 (class 0 OID 0)
-- Dependencies: 306
-- Name: ubicacion_administrativa_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.ubicacion_administrativa_id_seq OWNED BY public.ubicacion_administrativa.id;


--
-- TOC entry 307 (class 1259 OID 17045)
-- Name: unidad_medida; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.unidad_medida (
    id integer NOT NULL,
    descripcion text NOT NULL,
    fecha_creacion date DEFAULT CURRENT_DATE NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.unidad_medida OWNER TO postgres;

--
-- TOC entry 308 (class 1259 OID 17052)
-- Name: unidad_medida_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.unidad_medida_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.unidad_medida_id_seq OWNER TO postgres;

--
-- TOC entry 4131 (class 0 OID 0)
-- Dependencies: 308
-- Name: unidad_medida_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.unidad_medida_id_seq OWNED BY public.unidad_medida.id;


--
-- TOC entry 310 (class 1259 OID 19990)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuarios (
    id integer NOT NULL,
    cedula integer NOT NULL,
    nombre character varying(100) NOT NULL,
    apellido character varying(100) NOT NULL,
    usuario character varying(100) NOT NULL,
    password character varying(100) NOT NULL,
    tipousuario integer NOT NULL,
    borrado boolean DEFAULT false NOT NULL
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- TOC entry 309 (class 1259 OID 19989)
-- Name: usuarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuarios_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.usuarios_id_seq OWNER TO postgres;

--
-- TOC entry 4132 (class 0 OID 0)
-- Dependencies: 309
-- Name: usuarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuarios_id_seq OWNED BY public.usuarios.id;


--
-- TOC entry 325 (class 1259 OID 21081)
-- Name: vista_beneficiarios; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vista_beneficiarios AS
 SELECT (titulares.cedula_trabajador)::text AS cedula,
    'T'::text AS tipo_beneficiario,
    titulares.nombre,
    titulares.apellido,
    titulares.telefono
   FROM public.titulares
  WHERE (titulares.borrado = false)
UNION
 SELECT familiares.cedula,
    'F'::text AS tipo_beneficiario,
    familiares.nombre,
    familiares.apellido,
    familiares.telefono
   FROM public.familiares
  WHERE (familiares.borrado = false)
UNION
 SELECT cortesia.cedula,
    'C'::text AS tipo_beneficiario,
    cortesia.nombre,
    cortesia.apellido,
    cortesia.telefono
   FROM public.cortesia
  WHERE (cortesia.borrado = false);


ALTER VIEW public.vista_beneficiarios OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 21086)
-- Name: vista_beneficiarios_con_ubicacion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vista_beneficiarios_con_ubicacion AS
 SELECT ben.cedula,
    ben.nombre,
    ben.apellido,
    ben.sexo,
    ben.edad,
    tit.ubicacion_administrativa,
    ben.cedula_trabajador,
    ben.tipo,
    ben.parentesco
   FROM (( SELECT (titulares.cedula_trabajador)::text AS cedula,
            titulares.nombre,
            titulares.apellido,
            titulares.sexo,
            date_part('year'::text, ( SELECT age((CURRENT_DATE)::timestamp with time zone, (titulares.fecha_nacimiento)::timestamp with time zone) AS age)) AS edad,
            titulares.cedula_trabajador,
            'T'::text AS tipo,
            'TITULAR'::text AS parentesco
           FROM public.titulares
          WHERE (NOT titulares.borrado)
        UNION
         SELECT familiares.cedula,
            familiares.nombre,
            familiares.apellido,
            familiares.sexo,
            date_part('year'::text, ( SELECT age((CURRENT_DATE)::timestamp with time zone, (familiares.fecha_nac_familiares)::timestamp with time zone) AS age)) AS edad,
            familiares.cedula_trabajador,
            'F'::text AS tipo,
            p.descripcion AS parentesco
           FROM (public.familiares
             JOIN public.parentesco p ON ((familiares.parentesco_id = p.id)))
          WHERE (NOT familiares.borrado)
        UNION
         SELECT cortesia.cedula,
            cortesia.nombre,
            cortesia.apellido,
            cortesia.sexo,
            date_part('year'::text, ( SELECT age((CURRENT_DATE)::timestamp with time zone, (cortesia.fecha_nac_cortesia)::timestamp with time zone) AS age)) AS edad,
            cortesia.cedula_trabajador,
            'C'::text AS tipo,
            'CORTESIA'::text AS parentesco
           FROM public.cortesia
          WHERE (NOT cortesia.borrado)) ben
     JOIN public.titulares tit ON ((tit.cedula_trabajador = ben.cedula_trabajador)));


ALTER VIEW public.vista_beneficiarios_con_ubicacion OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 21091)
-- Name: vista_frecuencia_atencion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.vista_frecuencia_atencion AS
 SELECT
        CASE
            WHEN (c.consulta = true) THEN 'CONS'::text
            ELSE 'CITA'::text
        END AS control,
    hm.cedula,
    concat(b.nombre, ' ', b.apellido) AS nombre,
    b.edad,
    b.parentesco,
    c.descripcion AS especialidad,
    c.id_especialidad,
        CASE
            WHEN (b.sexo = 1) THEN 'M'::text
            WHEN (b.sexo = 2) THEN 'F'::text
            ELSE NULL::text
        END AS sexo,
        CASE
            WHEN (btrim(c.tipo_beneficiario) = 'T'::text) THEN 'T'::text
            WHEN (btrim(c.tipo_beneficiario) = 'F'::text) THEN 'F'::text
            ELSE 'C'::text
        END AS beneficiario,
        CASE
            WHEN (b.edad > (18)::double precision) THEN 'ADULTO'::text
            ELSE 'NINO'::text
        END AS estatus,
    b.ubicacion_administrativa AS departamento,
    c.motivo_consulta,
    c.diagnostico,
    c.fecha_asistencia,
    c.fecha_asistencia_normal,
    c.nombre_medico,
    c.id_medico,
        CASE
            WHEN (r.fecha_hasta > CURRENT_DATE) THEN 'SI'::text
            ELSE 'NO'::text
        END AS reposo
   FROM ( SELECT hc.n_historial AS historia,
            hc.id AS id_cita_consulta,
            hc.id_medico,
            concat(med.nombre, ' ', med.apellido) AS nombre_medico,
            hc.n_historial,
            hc.tipo_beneficiario,
            hc.consulta,
            hc.borrado,
                CASE
                    WHEN (hc.motivo_consulta IS NULL) THEN 'SIN INFORMACION'::text
                    ELSE hc.motivo_consulta
                END AS motivo_consulta,
            to_char((hc.fecha_creacion)::timestamp with time zone, 'dd-mm-yyyy'::text) AS fecha_creacion,
            to_char((hc.fecha_asistencia)::timestamp with time zone, 'dd-mm-yyyy'::text) AS fecha_asistencia,
            hc.fecha_asistencia AS fecha_asistencia_normal,
            e.descripcion,
            e.id_especialidad,
                CASE
                    WHEN (efa.descripcion IS NULL) THEN 'SIN DIAGN\\u00d3STICO'::text
                    ELSE efa.descripcion
                END AS diagnostico
           FROM (((historial_clinico.consultas hc
             JOIN historial_clinico.enfermedad_actual efa ON ((hc.id = efa.id_consulta)))
             JOIN historial_clinico.medicos med ON ((hc.id_medico = med.id)))
             JOIN historial_clinico.especialidades e ON ((med.especialidad = e.id_especialidad)))
          ORDER BY hc.id) c,
    public.vista_beneficiarios_con_ubicacion b,
    (( SELECT historial_medico.cedula,
            historial_medico.n_historial,
            historial_medico.id AS id_historial_medico,
            historial_medico.borrado
           FROM historial_clinico.historial_medico) hm
     LEFT JOIN historial_clinico.reposos r ON ((r.n_historial = hm.n_historial)))
  WHERE ((c.n_historial = hm.n_historial) AND (hm.cedula = b.cedula));


ALTER VIEW public.vista_frecuencia_atencion OWNER TO postgres;

--
-- TOC entry 3538 (class 2604 OID 17068)
-- Name: abortos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.abortos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.abortos_id_seq'::regclass);


--
-- TOC entry 3539 (class 2604 OID 17069)
-- Name: alergias_medicamentos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.alergias_medicamentos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.alergias_medicamentos_id_seq'::regclass);


--
-- TOC entry 3543 (class 2604 OID 17070)
-- Name: antecedentes_gineco_osbtetrico id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_gineco_osbtetrico ALTER COLUMN id SET DEFAULT nextval('historial_clinico.antecedentes_gineco_osbtetrico_id_seq'::regclass);


--
-- TOC entry 3554 (class 2604 OID 17071)
-- Name: antecedentes_patologicosf id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_patologicosf ALTER COLUMN id SET DEFAULT nextval('historial_clinico.antecedentes_patologicosf_id_seq'::regclass);


--
-- TOC entry 3558 (class 2604 OID 17072)
-- Name: antecedentes_patologicosp id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_patologicosp ALTER COLUMN id SET DEFAULT nextval('historial_clinico.antecedentes_patologicosp_id_seq'::regclass);


--
-- TOC entry 3562 (class 2604 OID 17073)
-- Name: antecedentes_quirurgicos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_quirurgicos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.antecedentes_quirurgicos_id_seq'::regclass);


--
-- TOC entry 3566 (class 2604 OID 17074)
-- Name: cesarias id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.cesarias ALTER COLUMN id SET DEFAULT nextval('historial_clinico.cesarias_id_seq'::regclass);


--
-- TOC entry 3567 (class 2604 OID 17075)
-- Name: consultas id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.consultas ALTER COLUMN id SET DEFAULT nextval('historial_clinico.consultas_id_seq'::regclass);


--
-- TOC entry 3690 (class 2604 OID 21041)
-- Name: detalle_notas_entrega id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.detalle_notas_entrega ALTER COLUMN id SET DEFAULT nextval('historial_clinico.detalle_notas_entrega_id_seq'::regclass);


--
-- TOC entry 3693 (class 2604 OID 21055)
-- Name: enfermedad_actual id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.enfermedad_actual ALTER COLUMN id SET DEFAULT nextval('historial_clinico.enfermedad_actual_id_seq'::regclass);


--
-- TOC entry 3572 (class 2604 OID 17078)
-- Name: enfermedades_sexuales id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.enfermedades_sexuales ALTER COLUMN id SET DEFAULT nextval('historial_clinico.enfermedades_sexuales_id_seq'::regclass);


--
-- TOC entry 3574 (class 2604 OID 17079)
-- Name: especialidades id_especialidad; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.especialidades ALTER COLUMN id_especialidad SET DEFAULT nextval('historial_clinico.especialidades_id_especialidad_seq'::regclass);


--
-- TOC entry 3578 (class 2604 OID 17080)
-- Name: examen_fisico id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.examen_fisico ALTER COLUMN id SET DEFAULT nextval('historial_clinico.examen_fisico_id_seq'::regclass);


--
-- TOC entry 3582 (class 2604 OID 17081)
-- Name: gestacion id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.gestacion ALTER COLUMN id SET DEFAULT nextval('historial_clinico.gestacion_id_seq'::regclass);


--
-- TOC entry 3583 (class 2604 OID 17082)
-- Name: habito_historial id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habito_historial ALTER COLUMN id SET DEFAULT nextval('historial_clinico.habito_historial_id_seq'::regclass);


--
-- TOC entry 3586 (class 2604 OID 17083)
-- Name: habitos_psicosociales id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habitos_psicosociales ALTER COLUMN id SET DEFAULT nextval('historial_clinico.habitos_psicosociales_id_seq'::regclass);


--
-- TOC entry 3588 (class 2604 OID 17084)
-- Name: historial_informativo id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.historial_informativo ALTER COLUMN id SET DEFAULT nextval('historial_clinico.historial_informativo_id_seq'::regclass);


--
-- TOC entry 3590 (class 2604 OID 17085)
-- Name: historial_medico id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.historial_medico ALTER COLUMN id SET DEFAULT nextval('historial_clinico.historial_medico_id_seq'::regclass);


--
-- TOC entry 3593 (class 2604 OID 17086)
-- Name: impresion_diag id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.impresion_diag ALTER COLUMN id SET DEFAULT nextval('historial_clinico.impresion_diag_id_seq'::regclass);


--
-- TOC entry 3598 (class 2604 OID 17087)
-- Name: medicamentos_patologias id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.medicamentos_patologias ALTER COLUMN id SET DEFAULT nextval('historial_clinico.medicamentos_medicina_interna_id_seq'::regclass);


--
-- TOC entry 3677 (class 2604 OID 20006)
-- Name: medicos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.medicos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.medicos_id_seq'::regclass);


--
-- TOC entry 3602 (class 2604 OID 17089)
-- Name: metodos_anticonceptibos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.metodos_anticonceptibos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.metodos_anticonceptibos_id_seq'::regclass);


--
-- TOC entry 3687 (class 2604 OID 21030)
-- Name: notas_entregas id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.notas_entregas ALTER COLUMN id SET DEFAULT nextval('historial_clinico.notas_entregas_id_seq'::regclass);


--
-- TOC entry 3604 (class 2604 OID 17091)
-- Name: numeros_romanos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.numeros_romanos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.numeros_romanos_id_seq'::regclass);


--
-- TOC entry 3606 (class 2604 OID 17092)
-- Name: paraclinicos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.paraclinicos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.paraclinicos_id_seq'::regclass);


--
-- TOC entry 3610 (class 2604 OID 17093)
-- Name: partos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.partos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.partos_id_seq'::regclass);


--
-- TOC entry 3611 (class 2604 OID 17094)
-- Name: plan id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.plan ALTER COLUMN id SET DEFAULT nextval('historial_clinico.plan_id_seq'::regclass);


--
-- TOC entry 3615 (class 2604 OID 17095)
-- Name: psicologia_primaria id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.psicologia_primaria ALTER COLUMN id SET DEFAULT nextval('historial_clinico.psicologia_primaria_id_seq'::regclass);


--
-- TOC entry 3618 (class 2604 OID 17096)
-- Name: reposos id; Type: DEFAULT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.reposos ALTER COLUMN id SET DEFAULT nextval('historial_clinico.reposos_id_seq'::regclass);


--
-- TOC entry 3621 (class 2604 OID 17109)
-- Name: auditoria id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria ALTER COLUMN id SET DEFAULT nextval('public.auditoria_id_seq'::regclass);


--
-- TOC entry 3623 (class 2604 OID 17110)
-- Name: auditoria_de_sistema id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria_de_sistema ALTER COLUMN id SET DEFAULT nextval('public.auditoria_de_sistema_id_seq'::regclass);


--
-- TOC entry 3625 (class 2604 OID 17111)
-- Name: categoria_inventario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria_inventario ALTER COLUMN id SET DEFAULT nextval('public.categoria_inventario_id_seq'::regclass);


--
-- TOC entry 3628 (class 2604 OID 17112)
-- Name: compuestos id_compuesto; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compuestos ALTER COLUMN id_compuesto SET DEFAULT nextval('public.compuestos_id_compuesto_seq1'::regclass);


--
-- TOC entry 3630 (class 2604 OID 17113)
-- Name: control id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.control ALTER COLUMN id SET DEFAULT nextval('public.control_id_seq'::regclass);


--
-- TOC entry 3633 (class 2604 OID 17114)
-- Name: cortesia id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia ALTER COLUMN id SET DEFAULT nextval('public.cortesia_id_seq'::regclass);


--
-- TOC entry 3641 (class 2604 OID 17115)
-- Name: datos_titulares id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.datos_titulares ALTER COLUMN id SET DEFAULT nextval('public.datos_titulares_id_seq'::regclass);


--
-- TOC entry 3679 (class 2604 OID 21008)
-- Name: entradas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradas ALTER COLUMN id SET DEFAULT nextval('public.entradas_id_seq'::regclass);


--
-- TOC entry 3645 (class 2604 OID 17117)
-- Name: estado_civil id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado_civil ALTER COLUMN id SET DEFAULT nextval('public.estado_civil_id_seq'::regclass);


--
-- TOC entry 3697 (class 2604 OID 21075)
-- Name: familiares id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.familiares ALTER COLUMN id SET DEFAULT nextval('public.familiares_id_seq1'::regclass);


--
-- TOC entry 3646 (class 2604 OID 17119)
-- Name: grado_instruccion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_instruccion ALTER COLUMN id SET DEFAULT nextval('public.grado_instruccion_id_seq'::regclass);


--
-- TOC entry 3647 (class 2604 OID 17120)
-- Name: grupo_usuario id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupo_usuario ALTER COLUMN id SET DEFAULT nextval('public.nivel_usuario_id_seq'::regclass);


--
-- TOC entry 3649 (class 2604 OID 17121)
-- Name: medicamentos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medicamentos ALTER COLUMN id SET DEFAULT nextval('public.medicamentos_id_seq1'::regclass);


--
-- TOC entry 3654 (class 2604 OID 17123)
-- Name: ocupacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocupacion ALTER COLUMN id SET DEFAULT nextval('public.ocupacion_id_seq'::regclass);


--
-- TOC entry 3655 (class 2604 OID 17124)
-- Name: parentesco id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parentesco ALTER COLUMN id SET DEFAULT nextval('public.parentesco_id_seq'::regclass);


--
-- TOC entry 3657 (class 2604 OID 17125)
-- Name: presentacion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion ALTER COLUMN id SET DEFAULT nextval('public.presentacion_id_seq'::regclass);


--
-- TOC entry 3660 (class 2604 OID 17126)
-- Name: reversos id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reversos ALTER COLUMN id SET DEFAULT nextval('public.reversos_id_seq'::regclass);


--
-- TOC entry 3683 (class 2604 OID 21018)
-- Name: salidas id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.salidas ALTER COLUMN id SET DEFAULT nextval('public.salidas_id_seq'::regclass);


--
-- TOC entry 3662 (class 2604 OID 17128)
-- Name: tipo_de_sangre id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_de_sangre ALTER COLUMN id SET DEFAULT nextval('public.tipo_de_sangre_id_seq'::regclass);


--
-- TOC entry 3663 (class 2604 OID 17129)
-- Name: tipo_medicamento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_medicamento ALTER COLUMN id SET DEFAULT nextval('public.tipo_medicamento_id_seq'::regclass);


--
-- TOC entry 3667 (class 2604 OID 17130)
-- Name: titulares id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.titulares ALTER COLUMN id SET DEFAULT nextval('public.titulares_id_seq1'::regclass);


--
-- TOC entry 3670 (class 2604 OID 17131)
-- Name: ubicacion_administrativa id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_administrativa ALTER COLUMN id SET DEFAULT nextval('public.ubicacion_administrativa_id_seq'::regclass);


--
-- TOC entry 3672 (class 2604 OID 17132)
-- Name: unidad_medida id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_medida ALTER COLUMN id SET DEFAULT nextval('public.unidad_medida_id_seq'::regclass);


--
-- TOC entry 3675 (class 2604 OID 19993)
-- Name: usuarios id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios ALTER COLUMN id SET DEFAULT nextval('public.usuarios_id_seq'::regclass);


--
-- TOC entry 3958 (class 0 OID 16389)
-- Dependencies: 210
-- Data for Name: abortos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.abortos (id, n_historial, id_abortos_numeros_romanos) FROM stdin;
\.


--
-- TOC entry 3960 (class 0 OID 16395)
-- Dependencies: 212
-- Data for Name: alergias_medicamentos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.alergias_medicamentos (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado) FROM stdin;
\.


--
-- TOC entry 3962 (class 0 OID 16404)
-- Dependencies: 214
-- Data for Name: antecedentes_gineco_osbtetrico; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.antecedentes_gineco_osbtetrico (id, n_historial, id_consulta, menarquia, sexarquia, nps, ciclo_mestrual, f_u_r, edad_gestacional, dismenorrea, eumenorrea, anticonceptivo, detalles_anticon, diu, t_cobre, mirena, aspiral, i_subdermico, otro, t_colocacion, ets, tipo_ets, detalle_ets, citologia, eco_mamario, mamografia, desintometria, detalle_historia_obst) FROM stdin;
\.


--
-- TOC entry 3964 (class 0 OID 16420)
-- Dependencies: 216
-- Data for Name: antecedentes_patologicosf; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.antecedentes_patologicosf (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado) FROM stdin;
\.


--
-- TOC entry 3966 (class 0 OID 16429)
-- Dependencies: 218
-- Data for Name: antecedentes_patologicosp; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.antecedentes_patologicosp (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado) FROM stdin;
\.


--
-- TOC entry 3968 (class 0 OID 16438)
-- Dependencies: 220
-- Data for Name: antecedentes_quirurgicos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.antecedentes_quirurgicos (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado) FROM stdin;
\.


--
-- TOC entry 3970 (class 0 OID 16447)
-- Dependencies: 222
-- Data for Name: cesarias; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.cesarias (id, n_historial, id_cesarias_numeros_romanos) FROM stdin;
\.


--
-- TOC entry 3972 (class 0 OID 16453)
-- Dependencies: 224
-- Data for Name: consultas; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.consultas (id, n_historial, id_medico, peso, talla, spo2, frecuencia_c, frecuencia_r, temperatura, tension_alta, tension_baja, imc, fecha_asistencia, fecha_creacion, asistencia, consulta, tipo_beneficiario, borrado, motivo_consulta, user_id) FROM stdin;
\.


--
-- TOC entry 4068 (class 0 OID 21038)
-- Dependencies: 320
-- Data for Name: detalle_notas_entrega; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.detalle_notas_entrega (id, nota_entrega_id, salida_id, fecha, borrado) FROM stdin;
\.


--
-- TOC entry 4070 (class 0 OID 21052)
-- Dependencies: 322
-- Data for Name: enfermedad_actual; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.enfermedad_actual (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado) FROM stdin;
\.


--
-- TOC entry 3974 (class 0 OID 16478)
-- Dependencies: 226
-- Data for Name: enfermedades_sexuales; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.enfermedades_sexuales (id, descripcion, borrado) FROM stdin;
\.


--
-- TOC entry 3976 (class 0 OID 16485)
-- Dependencies: 228
-- Data for Name: especialidades; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.especialidades (id_especialidad, descripcion, borrado, acceso_citas, caja_chica) FROM stdin;
4	GINECOLOGIA Y OBSTETRICIA	f	f	f
2	MEDICINA INTERNA	f	t	f
1	MEDICINA GENERAL	f	f	f
6	PEDIATRIA	f	f	f
8	CONSULTA EXTERNA	f	f	f
5	ATEN.  PSICOLOGICA PRIMARIA	f	t	f
3	MEDICINA OCUPACIONAL	t	f	f
7	SERVICIO MEDICO	f	f	t
9	JORNADAS	f	f	f
\.


--
-- TOC entry 3978 (class 0 OID 16494)
-- Dependencies: 230
-- Data for Name: examen_fisico; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.examen_fisico (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado) FROM stdin;
\.


--
-- TOC entry 3980 (class 0 OID 16503)
-- Dependencies: 232
-- Data for Name: gestacion; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.gestacion (id, n_historial, id_gestacion_numeros_romanos) FROM stdin;
\.


--
-- TOC entry 3982 (class 0 OID 16509)
-- Dependencies: 234
-- Data for Name: habito_historial; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.habito_historial (id, historial_id, habito_id, fecha_creacion, borrado) FROM stdin;
\.


--
-- TOC entry 3984 (class 0 OID 16515)
-- Dependencies: 236
-- Data for Name: habitos_psicosociales; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.habitos_psicosociales (id, descripcion, borrado) FROM stdin;
1	CAFE	f
3	ALCOHOL	f
4	CIGARRILLO	f
2	ILICITOS	f
\.


--
-- TOC entry 3986 (class 0 OID 16522)
-- Dependencies: 238
-- Data for Name: historial_informativo; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.historial_informativo (id, descripcion, borrado) FROM stdin;
3	ENFERMEDAD ACTUAL	t
4	ALERGIAS A MEDICAMENTOS	t
5	ANTECEDENTES PATOLOGICOS PERSONALES	t
6	ANTECEDENTES QUIRURGICOS	t
7	ANTECEDENTES PATOLOGICOS FAMILIARES	t
8	EXAMEN FISICO	t
1	DIAGNOSTICO	f
2	HABITOS PSICOSOCLIALES	t
9	PLAN	f
10	IMPRESION DIAGNOSTICA	t
11	ANTECEDENTES GINECO OBSTETRICOS	t
12	REPOSOS	f
\.


--
-- TOC entry 3988 (class 0 OID 16529)
-- Dependencies: 240
-- Data for Name: historial_medico; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.historial_medico (id, n_historial, cedula, fecha_regist, borrado) FROM stdin;
\.


--
-- TOC entry 3990 (class 0 OID 16537)
-- Dependencies: 242
-- Data for Name: impresion_diag; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.impresion_diag (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado, psicologia) FROM stdin;
\.


--
-- TOC entry 3992 (class 0 OID 16547)
-- Dependencies: 244
-- Data for Name: medicamentos_patologias; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.medicamentos_patologias (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado, id_especialidad) FROM stdin;
\.


--
-- TOC entry 4060 (class 0 OID 20003)
-- Dependencies: 312
-- Data for Name: medicos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.medicos (id, cedula, nombre, apellido, telefono, especialidad, borrado) FROM stdin;
\.


--
-- TOC entry 3994 (class 0 OID 16563)
-- Dependencies: 246
-- Data for Name: metodos_anticonceptibos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.metodos_anticonceptibos (id, descripcion, borrado) FROM stdin;
3	MIRENA	f
4	ASPIRAL	f
5	I. SUBDERMICO	f
1	ORAL	f
2	INTROUTERINO	f
6	T. COLOCACION	f
7	OTROS	f
\.


--
-- TOC entry 4066 (class 0 OID 21027)
-- Dependencies: 318
-- Data for Name: notas_entregas; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.notas_entregas (id, n_historial, fecha_registro, fecha_creacion, usuario, borrado) FROM stdin;
\.


--
-- TOC entry 3996 (class 0 OID 16578)
-- Dependencies: 248
-- Data for Name: numeros_romanos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.numeros_romanos (id, descripcion, borrado) FROM stdin;
1	I	f
2	II	f
3	III	f
4	IV	f
5	V	f
6	VI	f
7	VII	f
8	VIII	f
9	IX	f
10	X	f
0	N/A	f
\.


--
-- TOC entry 3998 (class 0 OID 16583)
-- Dependencies: 250
-- Data for Name: paraclinicos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.paraclinicos (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado) FROM stdin;
\.


--
-- TOC entry 4000 (class 0 OID 16592)
-- Dependencies: 252
-- Data for Name: partos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.partos (id, n_historial, id_partos_numeros_romanos) FROM stdin;
\.


--
-- TOC entry 4002 (class 0 OID 16598)
-- Dependencies: 254
-- Data for Name: plan; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.plan (id, n_historial, id_consulta, descripcion, fecha_creacion, fecha_actualizacion, borrado) FROM stdin;
\.


--
-- TOC entry 4004 (class 0 OID 16607)
-- Dependencies: 256
-- Data for Name: psicologia_primaria; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.psicologia_primaria (id, n_historial, fecha_seguimiento, evolucion, borrado, id_medico, observacion, id_consulta) FROM stdin;
\.


--
-- TOC entry 4006 (class 0 OID 16615)
-- Dependencies: 258
-- Data for Name: reposos; Type: TABLE DATA; Schema: historial_clinico; Owner: postgres
--

COPY historial_clinico.reposos (id, n_historial, id_medico, fecha_desde, fecha_hasta, motivo, borrado, fecha_creacion, horas) FROM stdin;
2	T19933177	1	2024-01-19	2024-01-22	 HIPERTENSION	f	2024-01-19	72
\.


--
-- TOC entry 4008 (class 0 OID 16798)
-- Dependencies: 260
-- Data for Name: auditoria; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auditoria (id, fecha_sesion, id_usuario, accion, id_medicamento, cantidad, hora) FROM stdin;
10	2024-01-19	1	\N	\N	\N	\N
\.


--
-- TOC entry 4009 (class 0 OID 16804)
-- Dependencies: 261
-- Data for Name: auditoria_de_sistema; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.auditoria_de_sistema (id, user_id, accion, fecha, hora) FROM stdin;
2	3	GENERO UN REPOSO PARA  FREDDY D. TORRES S.	2024-01-19	13:35:18 PM
3	1	CREO UNA NOTA DE ENTREGA  PARA      T19933177	2024-01-19	13:52:33 PM
4	1	REVERSO LA NOTA DE ENTREGA  Nº   2	2024-01-19	13:52:50 PM
5	1	CREO UNA NOTA DE ENTREGA  PARA      T19933177	2024-01-19	13:53:01 PM
\.


--
-- TOC entry 4012 (class 0 OID 16812)
-- Dependencies: 264
-- Data for Name: categoria_inventario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.categoria_inventario (id, descripcion, fecha_creacion, borrado) FROM stdin;
2	MATERIALES	2022-09-16	f
3	EQUIPOS MEDICOS	2022-09-16	f
1	MEDICAMENTOS	2022-09-16	f
\.


--
-- TOC entry 4014 (class 0 OID 16820)
-- Dependencies: 266
-- Data for Name: compuestos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.compuestos (id_compuesto, descripcioncompuesta, id_tipo_medicamento, id_unidad_medida, cantidad, borrado, id_medicamento) FROM stdin;
\.


--
-- TOC entry 4017 (class 0 OID 16828)
-- Dependencies: 269
-- Data for Name: control; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.control (id, descripcion, fecha_creacion, borrado) FROM stdin;
3	UNIDAD	2022-06-30	f
4	GALON	2022-06-30	f
5	CAJA	2022-06-30	f
6	FRASCO	2022-06-30	f
2	AMPOLLA	2022-06-30	f
8	GOTAS	2022-08-25	f
1	JARABE	2022-06-28	f
9	MMM	2022-11-21	f
10	dfsf	2022-12-12	f
11	PARES	2023-05-23	f
12	TABLETA	2023-05-23	f
13	BLISTERS	2023-05-23	f
14	SOBRES	2023-06-20	f
\.


--
-- TOC entry 4019 (class 0 OID 16836)
-- Dependencies: 271
-- Data for Name: cortesia; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cortesia (id, cedula_trabajador, nacionalidad, nombre, apellido, telefono, sexo, fecha_nac_cortesia, cedula, observacion, borrado, tipo_de_sangre, estado_civil, ocupacion_id, grado_intruccion_id, adscrito) FROM stdin;
\.


--
-- TOC entry 4021 (class 0 OID 16845)
-- Dependencies: 273
-- Data for Name: cortesia_adscrito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cortesia_adscrito (id, id_cortesia, id_adscrito, fecha_creacion, borrado) FROM stdin;
\.


--
-- TOC entry 4023 (class 0 OID 16852)
-- Dependencies: 275
-- Data for Name: datos_titulares; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.datos_titulares (cedula_trabajador, nacionalidad, nombre, apellido, telefono, sexo, fecha_nacimiento, tipo_de_personal, ubicacion_administrativa, borrado, id) FROM stdin;
\.


--
-- TOC entry 4026 (class 0 OID 16860)
-- Dependencies: 278
-- Data for Name: ente_adscrito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ente_adscrito (id, descripcion, fecha_creacion, borrado, rif) FROM stdin;
4	COMERCIO	2023-03-14	f	QER22ER-HE0
3	ALMACENAMIENTO CARACAS	2023-03-14	f	169DF-GP
2	TODOS	2023-03-14	f	NINGUNO
\.


--
-- TOC entry 4062 (class 0 OID 21005)
-- Dependencies: 314
-- Data for Name: entradas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.entradas (id, user_id, fecha_entrada, fecha_vencimiento, id_medicamento, cantidad, borrado) FROM stdin;
\.


--
-- TOC entry 4027 (class 0 OID 16880)
-- Dependencies: 279
-- Data for Name: estado_civil; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.estado_civil (id, descripcion) FROM stdin;
1	SOLTERO
2	CASADO
3	DIVORCIADO
4	VIUDO
5	CONCUBINATO
0	NO INDICA
\.


--
-- TOC entry 4072 (class 0 OID 21072)
-- Dependencies: 324
-- Data for Name: familiares; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.familiares (id, cedula_trabajador, nombre, apellido, telefono, sexo, fecha_nac_familiares, parentesco_id, cedula, tipo_de_sangre, estado_civil, grado_intruccion_id, ocupacion_id, borrado) FROM stdin;
\.


--
-- TOC entry 4030 (class 0 OID 16909)
-- Dependencies: 282
-- Data for Name: grado_instruccion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grado_instruccion (id, descripcion) FROM stdin;
1	ANALFABETA
2	PRIMARIA
3	SECUNDARIA
4	TECNICO
5	UNIVERSITARIO
0	NO INDICA
\.


--
-- TOC entry 4032 (class 0 OID 16915)
-- Dependencies: 284
-- Data for Name: grupo_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grupo_usuario (id, nivel_usuario, borrado) FROM stdin;
1	ADMINISTRADOR	f
2	FARMACIA	f
3	MEDICOS	f
\.


--
-- TOC entry 4033 (class 0 OID 16929)
-- Dependencies: 285
-- Data for Name: medicamentos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.medicamentos (id, id_tipo_medicamento, id_control, id_presentacion, descripcion, fecha_creacion, borrado, stock_minimo, med_cronico) FROM stdin;
2	1	13	2	ACETAMINOFEN/500MG/BLISTERS	2023-05-25	f	10	f
3	1	13	2	ACETAMINOFEN/500mg/BLISTERS	2023-05-25	f	10	f
4	1	3	5	ACETAMINOFEN/500mg/UNIDAD	2023-05-25	f	10	f
5	2	2	3	ACICLOVIR/250mg/ampolla/AMPOLLA	2023-05-25	f	10	f
6	2	3	5	ACICLOVIR/crema/UNIDAD	2023-05-25	f	10	f
8	4	13	2	ACIDO FOLICO/10MG/BLISTERS	2023-05-25	f	10	f
10	4	3	5	ACIDO FOLICO/10MG/UNIDAD	2023-05-25	f	10	f
11	4	5	1	ACIDO FOLICO/10mg/CAJA	2023-05-25	f	10	f
12	4	13	2	ACIDO FOLICO/5mg/BLISTERS	2023-05-25	f	10	f
13	4	2	3	ACIDO FOLICO/ampolla/AMPOLLA	2023-05-25	f	10	f
14	5	13	2	ACIDO IBANDRONICO/150MG/BLISTERS	2023-05-25	f	10	f
15	6	2	3	ACIDO TRANEXAMICO/100MG/AMPOLLA	2023-05-25	f	10	f
16	7	3	5	ACIDO VALPROICO/500MG/UNIDAD	2023-05-25	f	10	f
17	8	3	5	ADANTOL/CREMA/UNIDAD	2023-05-25	f	10	f
18	9	3	5	ADHESIVO/UNIDAD/UNIDAD	2023-05-25	f	10	f
19	9	3	5	ADHESIVO/unidad/UNIDAD	2023-05-25	f	10	f
20	10	3	5	AGUA DESTILADA/VIAL/UNIDAD	2023-05-25	f	10	f
21	11	2	3	AGUA ESTERIL/ampolla/AMPOLLA	2023-05-25	f	10	f
22	12	3	5	AGUA OXIGENADA/120ml/UNIDAD	2023-05-25	f	10	f
23	13	13	2	ALBENDAZOL/200mg/BLISTERS	2023-05-25	f	10	f
25	13	3	5	ALBENDAZOL/SUSPENSION/UNIDAD	2023-05-25	f	10	f
26	15	6	6	ALCOHOL ETILICO/LITRO/FRASCO	2023-05-25	f	10	f
27	14	3	5	ALCOHOL/LITRO/UNIDAD	2023-05-25	f	10	f
28	14	4	4	ALCOHOL/galon/GALON	2023-05-25	f	10	f
29	16	3	5	ALGODON/ROLLO/UNIDAD	2023-05-25	f	10	f
30	17	13	2	ALUPURINOL/100MG/BLISTERS	2023-05-25	f	10	f
31	18	6	6	AMBROXOL/15MG/100ML PEDIATRICO/FRASCO	2023-05-25	f	10	f
32	18	6	6	AMBROXOL/30 MG JARABE  ADULTO/FRASCO	2023-05-25	f	10	f
33	19	2	3	AMINOFILINA/250MG/AMPOLLA	2023-05-25	f	10	f
34	20	13	2	AMIODARONA/200MG/BLISTERS	2023-05-25	f	10	f
35	21	13	2	AMLODIPINA/10MG/BLISTERS	2023-05-25	f	10	f
36	21	5	1	AMLODIPINA/10MG/CAJA	2023-05-25	f	10	f
37	21	13	2	AMLODIPINA/5MG/BLISTERS	2023-05-25	f	10	f
38	24	3	5	AMOXICILINA + AC CLAVULANICO/600MG/UNIDAD	2023-05-25	f	10	f
39	25	6	6	AMOXICILINA + AC suspensión/600MG + 42,90MG/FRASCO	2023-05-25	f	10	f
40	25	3	5	AMOXICILINA + AC suspensión/600mg+42,9/UNIDAD	2023-05-25	f	10	f
41	23	13	2	AMOXICILINA + AC/500MG/125MG/BLISTERS	2023-05-25	f	10	f
44	27	2	3	AMPICILINA + SULBACTAM/1,5mg/AMPOLLA	2023-05-25	f	10	f
45	26	2	3	AMPICILINA/1gr/AMPOLLA	2023-05-25	f	10	f
46	26	13	2	AMPICILINA/500mg/BLISTERS	2023-05-25	f	10	f
47	26	6	6	AMPICILINA/SUSPENSION/FRASCO	2023-05-25	f	10	f
49	29	4	4	ANTIBACTERIAL CHERRY/GALON/GALON	2023-05-25	f	10	f
50	30	3	5	ANTUX/JARABE/UNIDAD	2023-05-25	f	10	f
51	31	5	1	APYRENE (ACETOMINOFEN)/650mg/CAJA	2023-05-25	f	10	f
52	32	6	6	ASAPROL (ASPIRINA)/81mg/FRASCO	2023-05-25	f	10	f
54	35	3	5	ASPIRINA (10 tab)/81mg/UNIDAD	2023-05-25	f	10	f
55	36	3	5	ASPIRINA (30 tab)/81mg/UNIDAD	2023-05-25	f	10	f
57	34	13	2	ASPIRINA/81MG/BLISTERS	2023-05-25	f	10	f
58	38	5	1	ATENOLOL/100MG/CAJA	2023-05-25	f	10	f
59	38	13	2	ATENOLOL/100mg/BLISTERS	2023-05-25	f	10	f
60	38	5	1	ATENOLOL/100mg/CAJA	2023-05-25	f	10	f
61	38	5	1	ATENOLOL/50MG/CAJA	2023-05-25	f	10	f
62	40	5	1	ATORVASTATINA (30 TAB)/40mg/CAJA	2023-05-25	f	10	f
63	39	13	2	ATORVASTATINA/20MG/BLISTERS	2023-05-25	f	10	f
64	42	3	5	AZITROMICINA (blister)/500mg/UNIDAD	2023-05-25	f	10	f
65	43	5	1	AZITROMICINA (caja 3 tab)/500mg/CAJA	2023-05-25	f	10	f
66	41	6	6	AZITROMICINA/200MG/FRASCO	2023-05-25	f	10	f
67	41	5	1	AZITROMICINA/250MG/CAJA	2023-05-25	f	10	f
68	44	3	5	BACITRECINA/CREMA/UNIDAD	2023-05-25	f	10	f
69	45	3	5	BATA DE CIRUJANO TALLA L/unidad/UNIDAD	2023-05-25	f	10	f
70	46	3	5	BATA DE CIRUJANO TALLA M/unidad/UNIDAD	2023-05-25	f	10	f
71	47	3	5	BATA DE CIRUJANO TALLA SS/unidad/UNIDAD	2023-05-25	f	10	f
72	48	3	5	BATA DE PACIENTE/unidad/UNIDAD	2023-05-25	f	10	f
73	49	3	5	BECLOMETASONA/250MG/UNIDAD	2023-05-25	f	10	f
74	50	2	3	BENCILPENICILINA PROCAINICA FORTIFICADA/1.000.000ui/AMPOLLA	2023-05-25	f	10	f
75	51	3	5	BETAGEN SOLSPEM/AMPOLLA/UNIDAD	2023-05-25	f	10	f
76	52	2	3	BETAMETASON  (ampolla)/4MG/ML/AMPOLLA	2023-05-25	f	10	f
77	53	2	3	BETAMETASONA/4mg/AMPOLLA	2023-05-25	f	10	f
78	53	3	5	BETAMETASONA/8mg/UNIDAD	2023-05-25	f	10	f
80	56	4	4	BLANQUEADOR NEVEX/GALON/GALON	2023-05-25	f	10	f
81	55	4	4	BLANQUEADOR/GALON/GALON	2023-05-25	f	10	f
82	57	5	1	BLOKIUM (ATENOLOL)/100mg/CAJA	2023-05-25	f	10	f
83	58	5	1	BROMAZEPAN/200mg/CAJA	2023-05-25	f	10	f
84	59	3	5	BROMURO DE IPATROPIO/0,0 25% x 20ml/UNIDAD	2023-05-25	f	10	f
85	59	5	1	BROMURO DE IPATROPIO/gotas/CAJA	2023-05-25	f	10	f
86	60	3	5	BUDECORT/GOTAS/UNIDAD	2023-05-25	f	10	f
87	61	3	5	BUDENAS/GOTAS/UNIDAD	2023-05-25	f	10	f
88	63	3	5	BUDESONIDA  (inhalador)/100MCG x 200 DOSIS/UNIDAD	2023-05-25	f	10	f
7	3	3	5	ACIDO ALENDRONATO/70MG/UNIDAD	2023-05-26	f	10	f
89	62	3	5	BUDESONIDA/GOTAS / VIAL/UNIDAD	2023-05-25	f	10	f
43	22	13	2	AMOXICILINA/BLISTERS/500MG	2023-06-20	f	10	f
48	28	3	5	ANLODIPINO/10MG/UNIDAD	2023-07-06	f	10	t
90	64	5	1	BUMENTIN RETARD/300mg/CAJA	2023-05-25	f	10	f
24	13	12	12	ALBENDAZOL/TABLETA	2023-09-15	f	10	f
56	37	13	2	ASPIRINA (cardioaspirin)/BLISTERS	2023-09-15	f	10	f
79	54	3	5	BISTURI/N° 20/UNIDAD	2023-09-15	f	10	f
53	34	5	1	ASPIRINA 75MG/CAJA/	2023-09-15	f	10	f
91	65	6	6	CALCIBON CON VITAMINA D/1500mg/FRASCO	2023-05-25	f	10	f
92	66	13	2	CALCIO CON VIT D3/250mg/125ui/BLISTERS	2023-05-25	f	10	f
94	67	13	2	CAPTOPRIL/50MG/BLISTERS	2023-05-25	f	10	f
95	67	5	1	CAPTOPRIL/50mg/CAJA	2023-05-25	f	10	f
96	68	5	1	CARBAMAZEPINA/200mg/CAJA	2023-05-25	f	10	f
97	69	5	1	CARBIDOPA + LEVODOPA/25m/250mg/CAJA	2023-05-25	f	10	f
98	70	13	2	CARDESARTAN/16mg/BLISTERS	2023-05-25	f	10	f
99	71	3	5	CARVATIVIR (GOTAS)/6MG/UNIDAD	2023-05-25	f	10	f
100	72	13	2	CARVERIDOL/12,5MG/BLISTERS	2023-05-25	f	10	f
101	72	5	1	CARVERIDOL/12,5MG/CAJA	2023-05-25	f	10	f
102	73	6	6	CEFADROXILO/250MG / 60ML/FRASCO	2023-05-25	f	10	f
103	73	6	6	CEFADROXILO/250mg/FRASCO	2023-05-25	f	10	f
104	73	3	5	CEFADROXILO/250mg/UNIDAD	2023-05-25	f	10	f
105	73	5	1	CEFADROXILO/500mg/CAJA	2023-05-25	f	10	f
106	74	5	1	CEFALEXINA/500mg/CAJA	2023-05-25	f	10	f
107	74	6	6	CEFALEXINA/JARABE/FRASCO	2023-05-25	f	10	f
108	75	2	3	CEFTRIAXONA/1GR/AMPOLLA	2023-05-25	f	10	f
110	76	3	5	CENTROS DE CAMA/unidad/UNIDAD	2023-05-25	f	10	f
111	77	13	2	CETIRIZINA/10MG/BLISTERS	2023-05-25	f	10	f
112	77	3	5	CETIRIZINA/10mg/UNIDAD	2023-05-25	f	10	f
113	78	2	3	CIANOCOBALAMINA (B12)/ampolla/AMPOLLA	2023-05-25	f	10	f
114	79	6	6	CIPROFLOXACINA/100MG/FRASCO	2023-05-25	f	10	f
115	79	13	2	CIPROFLOXACINA/500MG/BLISTERS	2023-05-25	f	10	f
116	79	3	5	CIPROFLOXACINA/500mg/UNIDAD	2023-05-25	f	10	f
117	80	3	5	CISTOFLO/BOLSA/UNIDAD	2023-05-25	f	10	f
118	81	3	5	CLARASOL/gotas/UNIDAD	2023-05-25	f	10	f
119	82	6	6	CLEMBUNAL/jarabe/FRASCO	2023-05-25	f	10	f
120	83	13	2	CLINDAMICINA/300MG/BLISTERS	2023-05-25	f	10	f
121	83	5	1	CLINDAMICINA/300mg/CAJA	2023-05-25	f	10	f
122	84	6	6	CLODOXIN (METOCOPLAMIDA +PIRIDOXINA+ VIT B6)/gotas/FRASCO	2023-05-25	f	10	f
123	85	13	2	CLOPIDOGREL/75MG/BLISTERS	2023-05-25	f	10	f
124	85	5	1	CLOPIDOGREL/75MG/CAJA	2023-05-25	f	10	f
125	86	13	2	CLORHIDRATO DE VERAPEMILO/80 MG/BLISTERS	2023-05-25	f	10	f
126	87	2	3	CLORURO DE POTASIO/25meq/AMPOLLA	2023-05-25	f	10	f
127	88	2	3	CLORURO DE SODIO/90mg/AMPOLLA	2023-05-25	f	10	f
128	89	3	5	CLOTRIMAXOL + GENTAMICINA +BETAMETASONA/CREMA/UNIDAD	2023-05-25	f	10	f
129	91	3	5	CLOTRIMAZOL (OVULOS)/500MG/UNIDAD	2023-05-25	f	10	f
130	90	3	5	CLOTRIMAZOL/OVULOS/UNIDAD	2023-05-25	f	10	f
131	90	5	1	CLOTRIMAZOL/crema/CAJA	2023-05-25	f	10	f
132	90	13	2	CLOTRIMAZOL/ovulos/BLISTERS	2023-05-25	f	10	f
133	92	13	2	CO -TRIMOXAZOL/480MG/BLISTERS	2023-05-25	f	10	f
134	93	6	6	CO-TRIMOXAZOL/240MG/FRASCO	2023-05-25	f	10	f
136	94	3	5	COLYPAN/TAB/UNIDAD	2023-05-25	f	10	f
137	95	2	3	COMPLEJO B/AMPOLLA/AMPOLLA	2023-05-25	f	10	f
141	96	3	5	COMPRESA ESTERIL/UNIDAD/UNIDAD	2023-05-25	f	10	f
142	97	3	5	CORTINA PARABAN/unidad/UNIDAD	2023-05-25	f	10	f
143	98	3	5	CORTYNASE/spray/UNIDAD	2023-05-25	f	10	f
144	99	3	5	CURITAS/unidad/UNIDAD	2023-05-25	f	10	f
145	100	3	5	DAYZOL/600MG/4MG/UNIDAD	2023-05-25	f	10	f
146	101	5	1	DEPLAZACORT/6MG/CAJA	2023-05-25	f	10	f
147	102	3	5	DEPRESORES/caja/UNIDAD	2023-05-25	f	10	f
148	103	3	5	DESLORATADINA/5mg/UNIDAD	2023-05-25	f	10	f
149	103	3	5	DESLORATADINA/JARABE/UNIDAD	2023-05-25	f	10	f
153	105	3	5	DEXLITOS SRO/SOBRE/UNIDAD	2023-05-25	f	10	f
154	106	6	6	DEXTROSA 10%/500CC/FRASCO	2023-05-25	f	10	f
155	107	3	5	DEXTROSA 5%/1000cc/UNIDAD	2023-05-25	f	10	f
156	107	3	5	DEXTROSA 5%/500cc/UNIDAD	2023-05-25	f	10	f
157	108	2	3	DIAZEPAM/10mg/AMPOLLA	2023-05-25	f	10	f
158	109	6	6	DICLOFENAC POTASICO/1,8MG SUSPENSION/FRASCO	2023-05-25	f	10	f
159	109	13	2	DICLOFENAC POTASICO/50MG/BLISTERS	2023-05-25	f	10	f
160	109	3	5	DICLOFENAC POTASICO/50mg/UNIDAD	2023-05-25	f	10	f
162	110	13	2	DICLOFENAC SODICO/100MG/BLISTERS	2023-05-25	f	10	f
163	110	13	2	DICLOFENAC SODICO/50MG/BLISTERS	2023-05-25	f	10	f
165	110	2	3	DICLOFENAC SODICO/75mg/ ampolla/AMPOLLA	2023-05-25	f	10	f
166	110	3	5	DICLOFENAC SODICO/GOTAS/UNIDAD	2023-05-25	f	10	f
167	111	13	2	DIGOXINA/0,25MG/BLISTERS	2023-05-25	f	10	f
168	112	2	3	DIPIRONA/1GR/AMPOLLA	2023-05-25	f	10	f
169	113	5	1	DOBET/gotas/CAJA	2023-05-25	f	10	f
170	114	3	5	DOL/450mg/UNIDAD	2023-05-25	f	10	f
171	115	3	5	DOMPERIDONA/10MG/UNIDAD	2023-05-25	f	10	f
172	116	3	5	DORZOLAMIDA + TIMOLOL/gotas/UNIDAD	2023-05-25	f	10	f
173	118	13	2	ENALAPRIL/20mg/BLISTERS	2023-05-25	f	10	f
174	118	3	5	ENALAPRIL/20mg/UNIDAD	2023-05-25	f	10	f
175	120	3	5	ENOXAPARINA SODICA/40mg/UNIDAD	2023-05-25	f	10	f
176	119	3	5	ENOXAPARINA/40MG/UNIDAD	2023-05-25	f	10	f
177	119	3	5	ENOXAPARINA/60mg/UNIDAD	2023-05-25	f	10	f
178	121	3	5	EPAX/1200mg/UNIDAD	2023-05-25	f	10	f
161	109	3	5	DICLOFENAC POTASICO/25MG/AMPOLLA	2023-06-22	f	10	f
93	67	5	1	CAPTOPRIL/25mg/CAJA	2023-06-26	f	30	t
179	122	2	3	EPINEFRINA/ampolla/AMPOLLA	2023-05-25	f	10	f
180	123	3	5	EQUIPO DE LAPAROTOMIA/unidad/UNIDAD	2023-05-25	f	10	f
181	124	3	5	EQUIPO DE SUERO MACROGOTERO/UNIDAD/UNIDAD	2023-05-25	f	10	f
182	124	3	5	EQUIPO DE SUERO MACROGOTERO/unidad/UNIDAD	2023-05-25	f	10	f
138	95	2	3	COMPLEJO B/AMPOLLA	2023-09-15	f	10	f
140	95	6	6	COMPLEJO B/FRASCO	2023-09-15	f	10	f
151	104	2	3	DEXAMETASONA/AMPOLLA	2023-09-15	f	10	f
150	104	13	2	DEXAMETASONA/BLISTERS	2023-09-15	f	10	f
152	104	2	3	DEXAMETASONA/8MG/AMPOLLA	2023-09-15	f	10	f
183	125	3	5	EQUIPO DE TRANFUSION/unidad/UNIDAD	2023-05-25	f	10	f
184	126	6	6	ERITROMICINA/125mg / jarabe/FRASCO	2023-05-25	f	10	f
185	127	2	3	ERITROPOYETINA/10000ui/AMPOLLA	2023-05-25	f	10	f
186	127	2	3	ERITROPOYETINA/30000ui/AMPOLLA	2023-05-25	f	10	f
187	128	13	2	ESOMEPRAZOL/20MG/BLISTERS	2023-05-25	f	10	f
188	129	3	5	ESPECULOS/unidad/UNIDAD	2023-05-25	f	10	f
189	130	13	2	ESPIROLACTONA/25MG/BLISTERS	2023-05-25	f	10	f
190	131	3	5	FEMMEXTRA/200mg/UNIDAD	2023-05-25	f	10	f
191	132	5	1	FENOMAX/120mg/CAJA	2023-05-25	f	10	f
192	133	5	1	FERGANIC/40mg/350mg/CAJA	2023-05-25	f	10	f
193	133	5	1	FERGANIC/40mg/CAJA	2023-05-25	f	10	f
194	134	5	1	FESTAL  (caja 10tab)/tab/CAJA	2023-05-25	f	10	f
195	135	3	5	FESTAL (caja 20tab)/tab/UNIDAD	2023-05-25	f	10	f
196	136	3	5	FIASP (ASPART)/100U/ML/UNIDAD	2023-05-25	f	10	f
197	137	13	2	FLAVOXATO/200MG/BLISTERS	2023-05-25	f	10	f
198	138	3	5	FLORA BALANCE/vial/UNIDAD	2023-05-25	f	10	f
199	139	5	1	FLUCONAZOL (CAJAS 1 TAB)/150mg/CAJA	2023-05-25	f	10	f
200	139	5	1	FLUCONAZOL (CAJAS 2 TAB)/150MG/CAJA	2023-05-25	f	10	f
201	139	5	1	FLUCONAZOL/150mg/CAJA	2023-05-25	f	10	f
202	140	3	5	FLUTICASONE/50 MCG / INHALADOR/UNIDAD	2023-05-25	f	10	f
203	141	3	5	FLUVIRIL/JARABE/UNIDAD	2023-05-25	f	10	f
204	142	3	5	FOSFATO DE DEXAMETASONA/GOTAS/UNIDAD	2023-05-25	f	10	f
205	144	13	2	FUMARATO FERROSO + AC FOLICO/180MG/0,4MG/BLISTERS	2023-05-25	f	10	f
207	145	2	3	FUROSEMIDA/20MG/AMPOLLA	2023-05-25	f	10	f
209	145	13	2	FUROSEMIDA/40MG/BLISTERS	2023-05-25	f	10	f
210	146	13	2	GABAPENTINA/300MG/BLISTERS	2023-05-25	f	10	f
212	147	5	1	GARABET/gotas/CAJA	2023-05-25	f	10	f
213	148	5	1	GARDENAL/100mg/CAJA	2023-05-25	f	10	f
214	150	3	5	GASA ESTERIL/UNIDAD/UNIDAD	2023-05-25	f	10	f
216	149	3	5	GASA/unidad/UNIDAD	2023-05-25	f	10	f
217	151	3	5	GEL ANTIBACTERIAL/500ML/UNIDAD	2023-05-25	f	10	f
218	151	4	4	GEL ANTIBACTERIAL/GALON/GALON	2023-05-25	f	10	f
219	152	4	4	GEL CONDUCTOR/galon/GALON	2023-05-25	f	10	f
220	153	4	4	GEL DE ULTRASONIDO/GALON/GALON	2023-05-25	f	10	f
221	154	2	3	GENTAMICINA/40 MG/ AMPOLLA/AMPOLLA	2023-05-25	f	10	f
222	154	2	3	GENTAMICINA/80 MG/2ML/AMPOLLA	2023-05-25	f	10	f
223	154	5	1	GENTAMICINA/gotas/CAJA	2023-05-25	f	10	f
224	155	4	4	GERDEX/galon/GALON	2023-05-25	f	10	f
226	156	5	1	GLIBENCLAMIDA/5MG/CAJA	2023-05-25	f	10	f
227	156	13	2	GLIBENCLAMIDA/5mg/BLISTERS	2023-05-25	f	10	f
228	157	5	1	GLICERINA/SUPOSITORIOS/CAJA	2023-05-25	f	10	f
229	158	13	2	GLICLAZIDE/30MG/BLISTERS	2023-05-25	f	10	f
230	159	6	6	GLIMEPIRIDE FRASCOS ( TAB) 100/2MG/FRASCO	2023-05-25	f	10	f
231	159	6	6	GLIMEPIRIDE FRASCOS ( TAB) 250/4MG/FRASCO	2023-05-25	f	10	f
232	160	6	6	GLIPIZIDE FRASCOS ( TAB) 100/5MG/FRASCO	2023-05-25	f	10	f
233	161	3	5	GUANTE DESCARTABLE/talla l/UNIDAD	2023-05-25	f	10	f
234	161	3	5	GUANTE DESCARTABLE/talla m/UNIDAD	2023-05-25	f	10	f
235	161	3	5	GUANTE DESCARTABLE/talla s/UNIDAD	2023-05-25	f	10	f
236	162	11	11	GUANTES DESCARTABLES/TALLA L/PARES	2023-05-25	f	10	f
237	163	3	5	GUANTES ESTERIL/talla 7 1/2/UNIDAD	2023-05-25	f	10	f
238	163	3	5	GUANTES ESTERIL/talla 7/UNIDAD	2023-05-25	f	10	f
239	163	3	5	GUANTES ESTERIL/talla 8 1/2/UNIDAD	2023-05-25	f	10	f
240	164	3	5	GUATA/unidad/UNIDAD	2023-05-25	f	10	f
241	165	13	2	HIDROCLOROTIAZIDA/12,5MG/BLISTERS	2023-05-25	f	10	f
242	165	13	2	HIDROCLOROTIAZIDA/25MG / BLISTERS (25 TAB)/BLISTERS	2023-05-25	f	10	f
243	166	2	3	HIDROCORTISONA/100mg/AMPOLLA	2023-05-25	f	10	f
244	166	3	5	HIDROCORTISONA/100mg/UNIDAD	2023-05-25	f	10	f
247	167	6	6	HIERRO                 suspensión/125 MG/ 5ML/FRASCO	2023-05-25	f	10	f
248	168	2	3	HIERRO               (SACAROSA DE HIERRO)/20MG/AMPOLLA	2023-05-25	f	10	f
249	169	2	3	HIERRO SACARATO/ampolla/AMPOLLA	2023-05-25	f	10	f
250	170	2	3	HIERRO SACAROSA/ampolla/AMPOLLA	2023-05-25	f	10	f
251	171	2	3	HIOSCINA        (BUSCAPINA)/20MG/AMPOLLA	2023-05-25	f	10	f
252	172	3	5	HIV/ SIPHILIS DUO/PRUEBA RAPIDA/UNIDAD	2023-05-25	f	10	f
253	174	6	6	IBUPROFENO         suspensión/100MG/5ML/FRASCO	2023-05-25	f	10	f
254	173	13	2	IBUPROFENO/400MG/BLISTERS	2023-05-25	f	10	f
255	173	3	5	IBUPROFENO/600MG/UNIDAD	2023-05-25	f	10	f
256	175	6	6	IMUNIDADE/frasco/FRASCO	2023-05-25	f	10	f
259	176	3	5	INYECTADORA/3cc/UNIDAD	2023-05-25	f	10	f
260	176	3	5	INYECTADORA/5cc/UNIDAD	2023-05-25	f	10	f
261	177	13	2	IRON/60MG /400MCG/BLISTERS	2023-05-25	f	10	f
263	178	5	1	IRRIGOR CAJA (14 TABLETAS)/100mg/CAJA	2023-05-25	f	10	f
264	179	5	1	ISOSORBIDA/5MG/CAJA	2023-05-25	f	10	f
246	166	3	5	HIDROCORTISONA/500mg/UNIDAD	2023-06-20	f	10	f
257	176	3	5	INYECTADORA/10CC/UNIDAD/ SIN AGUJA	2023-06-22	f	10	f
258	176	3	5	INYECTADORA/20CC/UNIDAD/ CON AGUJA	2023-06-22	f	10	f
215	150	3	5	GASA ESTERIL/UNIDAD	2023-06-26	f	10	f
265	180	13	2	IVERMENTINA/6MG/BLISTERS	2023-05-25	f	10	f
267	180	5	1	IVERMENTINA/CAJA/6MG/CAJA	2023-05-25	f	10	f
268	181	3	5	JELCO/#18/UNIDAD	2023-05-25	f	10	f
269	181	3	5	JELCO/#20/UNIDAD	2023-05-25	f	10	f
270	181	3	5	JELCO/#22/UNIDAD	2023-05-25	f	10	f
271	181	3	5	JELCO/#24/UNIDAD	2023-05-25	f	10	f
272	182	6	6	KELFEN/jarabe/FRASCO	2023-05-25	f	10	f
275	184	2	3	KETOPROFENO/AMPOLLA/AMPOLLA/AMPOLLA	2023-05-25	f	10	f
277	184	5	1	KETOPROFENO/CAJA/CAJA/100mg/CAJA	2023-05-25	f	10	f
245	166	3	5	HIDROCORTISONA/500MG/VIAL	2023-09-15	f	10	f
278	185	3	5	KIT BATA DE CIRUJANO/unidad/UNIDAD	2023-05-25	f	10	f
279	186	3	5	KIT DE OBSTETRICIA/UNIDAD/UNIDAD	2023-05-25	f	10	f
280	187	3	5	LAGRIMAS ARTIFICIALES/10ML/UNIDAD	2023-05-25	f	10	f
281	188	5	1	LAMOTRIGINA/30mg/CAJA	2023-05-25	f	10	f
282	189	6	6	LETISAN/jarabe/FRASCO	2023-05-25	f	10	f
283	190	3	5	LEUKOPLAST/UNIDAD/UNIDAD	2023-05-25	f	10	f
284	191	5	1	LEVOCETIRIZINA/5mg/CAJA	2023-05-25	f	10	f
289	193	3	5	LEVONOGESTREL/CAJA (1 TAB)/1.5MG/UNIDAD	2023-05-25	f	10	f
290	193	3	5	LEVONOGESTREL/CAJA (2 TAB)/0,75mg/UNIDAD	2023-05-25	f	10	f
291	195	6	6	LEVOTIROXINA (TYRONA)/100MCG/FRASCO	2023-05-25	f	10	f
292	196	13	2	LEVOTIROXINA SODICA/100mcg/BLISTERS	2023-05-25	f	10	f
293	194	13	2	LEVOTIROXINA/100MCG/BLISTERS	2023-05-25	f	10	f
294	197	3	5	LIDOCAINA/1 %/UNIDAD	2023-05-25	f	10	f
295	198	3	5	LIMPIADOR LARK/GALON/UNIDAD	2023-05-25	f	10	f
296	199	3	5	LIMPIADOR MULTIUSO/GALON/UNIDAD	2023-05-25	f	10	f
297	200	13	2	LIPOFAR (SINVASTATINA)/20MG/BLISTERS	2023-05-25	f	10	f
298	201	5	1	LIPRESAM (LISINOPRIL)/5MG/CAJA	2023-05-25	f	10	f
299	202	6	6	LOPAMIDOL (YODO)/370MG/FRASCO	2023-05-25	f	10	f
300	203	13	2	LOPERAM/BLISTERS/2mg/BLISTERS	2023-05-25	f	10	f
301	203	5	1	LOPERAM/CAJA/2mg/CAJA	2023-05-25	f	10	f
302	204	3	5	LORADEX (LORATADINA)/1 MG / JARABE/UNIDAD	2023-05-25	f	10	f
303	206	6	6	LORATADINA JARABE/5MG/5ML/FRASCO	2023-05-25	f	10	f
305	207	5	1	LOSAPRES PLUS/50mg + 12,5mg/CAJA	2023-05-25	f	10	f
306	208	3	5	LOSARTAN POTASICO/100MG/UNIDAD	2023-05-25	f	10	f
307	208	3	5	LOSARTAN POTASICO/50MG/UNIDAD	2023-05-25	f	10	f
308	209	5	1	LUMBAX (IBUPROFENO)/600mg/CAJA	2023-05-25	f	10	f
309	210	3	5	MASCARILLA DE NEBULIZAR/ADULTO/UNIDAD	2023-05-25	f	10	f
310	210	3	5	MASCARILLA DE NEBULIZAR/NIÑO/UNIDAD	2023-05-25	f	10	f
311	211	3	5	MASCARILLA DE OXIGENO ADULTO/unidad/UNIDAD	2023-05-25	f	10	f
312	212	3	5	MASCARILLA DE OXIGENO NIÑO/unidad/UNIDAD	2023-05-25	f	10	f
313	213	5	1	MATILOL/gotas/CAJA	2023-05-25	f	10	f
314	214	13	2	MEBENDAZOL/100MG/BLISTERS	2023-05-25	f	10	f
315	215	3	5	MEDROXIPROGESTERONA + ESTRADIOL/25+5MG/UNIDAD	2023-05-25	f	10	f
316	217	2	3	MELOXICAM    (ampolla)/15mg/AMPOLLA	2023-05-25	f	10	f
317	216	13	2	MELOXICAM/15MG/BLISTERS	2023-05-25	f	10	f
319	218	13	2	METFORMINA/500MG/BLISTERS	2023-05-25	f	10	f
321	218	3	5	METFORMINA/850mg/UNIDAD	2023-05-25	f	10	f
322	218	5	1	METFORMINA/CAJA (TAB 30)/500MG/CAJA	2023-05-25	f	10	f
323	219	5	1	METHYLPREDNISOLONE/4MG/CAJA	2023-05-25	f	10	f
324	220	6	6	METILDOPA/250MG/FRASCO	2023-05-25	f	10	f
325	221	2	3	METOCLOPLAMIDA/ampolla/AMPOLLA	2023-05-25	f	10	f
326	223	2	3	METOCLOPRAMIDA     (ampolla)/10MG/2ML/AMPOLLA	2023-05-25	f	10	f
327	222	13	2	METOCLOPRAMIDA/BLISTERS/10MG/BLISTERS	2023-05-25	f	10	f
329	225	3	5	METRONIDAZOL + MICONAZOL     (óvulos)/750MG+200MG/UNIDAD	2023-05-25	f	10	f
330	224	6	6	METRONIDAZOL/200MG/5ML/FRASCO	2023-05-25	f	10	f
331	224	6	6	METRONIDAZOL/5 MG/ ML/FRASCO	2023-05-25	f	10	f
332	224	13	2	METRONIDAZOL/500MG/BLISTERS	2023-05-25	f	10	f
333	224	3	5	METRONIDAZOL/500MG/ML/UNIDAD	2023-05-25	f	10	f
334	224	13	2	METRONIDAZOL/CAJA (TAB 8)/500mg/BLISTERS	2023-05-25	f	10	f
335	226	13	2	METROTREXATO/2,5mg/BLISTERS	2023-05-25	f	10	f
336	227	3	5	MIGREN/650mg+50mg+1mg/UNIDAD	2023-05-25	f	10	f
337	228	3	5	MONALIZ/0,05mg/UNIDAD	2023-05-25	f	10	f
338	229	3	5	MONO DE BIOSEGURIDAD/TALLA XL/UNIDAD	2023-05-25	f	10	f
339	230	3	5	MONO DE CIRUJANO/TALLA L/UNIDAD	2023-05-25	f	10	f
341	232	5	1	MUCLAR (AMBROXOL)/4mg/CAJA	2023-05-25	f	10	f
342	233	6	6	MULTIVITAMINA/jarabe/100ML/FRASCO	2023-05-25	f	10	f
344	236	6	6	MULTIVITAMINICO suspensión/100ML/FRASCO	2023-05-25	f	10	f
345	235	13	2	MULTIVITAMINICO/TABLETAS/BLISTERS	2023-05-25	f	10	f
346	237	6	6	NABUMETONE/750MG/FRASCO	2023-05-25	f	10	f
347	238	3	5	NAFAZOLINA     (gotas)/0,01 %/UNIDAD	2023-05-25	f	10	f
348	239	6	6	NANVIT- C/100MG/FRASCO	2023-05-25	f	10	f
349	240	3	5	NIMODIPINO/0,02 %/UNIDAD	2023-05-25	f	10	f
350	241	6	6	NISTATINA/500,000ui/FRASCO	2023-05-25	f	10	f
286	192	13	2	LEVOFLOXACINA/BLISTERS/750MG	2023-06-20	f	10	f
288	193	5	1	LEVONOGESTREL/1.5MG/CAJA(2 TAB)	2023-06-20	f	10	f
343	234	14	12	MULTIVITAMINAS/TABLETA	2023-06-20	f	10	f
266	180	12	12	IVERMENTINA/TABLETA/6MG	2023-06-20	f	10	f
276	184	13	2	KETOPROFENO/BLISTERS/BLISTERS/50MG/BLISTERS	2023-06-20	f	10	f
273	183	3	5	KETOCONAZOL/UNIDAD/CREMA	2023-06-22	f	10	f
274	183	3	5	KETOCONAZOL20%/UNIDAD/KETOWISE	2023-06-22	f	10	f
351	242	13	2	NITAXOZANIDA/500mg/BLISTERS	2023-05-25	f	10	f
352	243	2	3	NITROGLICERINA/1ML/ML/AMPOLLA	2023-05-25	f	10	f
353	244	2	3	NORETISTERONA +  ESTRADIOL/ampolla/AMPOLLA	2023-05-25	f	10	f
354	245	3	5	NOVORAPID FLEX PEN/100U/ML/UNIDAD	2023-05-25	f	10	f
355	246	3	5	OBTURADOR/UNIDAD/UNIDAD	2023-05-25	f	10	f
356	247	5	1	OCUPRED/gotas/CAJA	2023-05-25	f	10	f
357	248	3	5	OLOPATADINA       (gotas)/2MG/5ML/UNIDAD	2023-05-25	f	10	f
359	249	13	2	OMEPRAZOL/BLISTERS/20mg/BLISTERS	2023-05-25	f	10	f
360	250	13	2	ONDASETRON/8MG/BLISTERS	2023-05-25	f	10	f
361	250	2	3	ONDASETRON/ampolla/AMPOLLA	2023-05-25	f	10	f
362	251	5	1	OPAT (OLOPATADINA)/gotas/CAJA	2023-05-25	f	10	f
304	205	13	2	LORATADINA 10MG/BLISTERS	2023-09-15	f	10	f
340	231	13	2	MONTELUKAST/10MG/BLISTERS	2023-09-15	f	10	f
358	249	3	5	OMEPRAZOL/UNIDAD	2023-09-15	f	10	f
363	252	12	12	PANTROPAZOL /TABLETA	2023-09-15	f	10	f
364	254	6	6	PARACETAMOL    suspensión/120MG/5ML x 100ML/FRASCO	2023-05-25	f	10	f
365	253	13	2	PARACETAMOL/250MG/BLISTERS	2023-05-25	f	10	f
366	253	13	2	PARACETAMOL/500MG/BLISTERS	2023-05-25	f	10	f
367	255	3	5	PEDIACORT/jarabe/UNIDAD	2023-05-25	f	10	f
368	256	2	3	PENICILINA  BENZATINICA     (ampolla)/1.200.000 UI/AMPOLLA	2023-05-25	f	10	f
369	257	3	5	PENICILINA G BENZATINICA/2400000ui/UNIDAD	2023-05-25	f	10	f
370	258	2	3	PENICILINA G SODICA/1000000ui/AMPOLLA	2023-05-25	f	10	f
371	259	5	1	PINVEX/topico/CAJA	2023-05-25	f	10	f
372	260	13	2	PIROXICAM/20mg/BLISTERS	2023-05-25	f	10	f
373	261	3	5	PLUMPY NUT/SOBRE/UNIDAD	2023-05-25	f	10	f
374	262	13	2	POLANTAC/250mg +120mg/BLISTERS	2023-05-25	f	10	f
375	263	3	5	POVIDINE/1 LITRO/UNIDAD	2023-05-25	f	10	f
376	263	3	5	POVIDINE/120ml/UNIDAD	2023-05-25	f	10	f
377	264	5	1	PRAXONA/150mg/CAJA	2023-05-25	f	10	f
378	265	3	5	PREDNISOLONA/GOTAS/UNIDAD	2023-05-25	f	10	f
379	266	13	2	PREDNISONA/5mg/BLISTERS	2023-05-25	f	10	f
380	267	3	5	PRESERVATIVOS/unidad/UNIDAD	2023-05-25	f	10	f
381	268	3	5	PRUEBAS DE EMBARAZO/unidad/UNIDAD	2023-05-25	f	10	f
382	269	3	5	PRUEBAS PCR/unidad/UNIDAD	2023-05-25	f	10	f
383	270	3	5	PRUEBAS PDR/unidad/UNIDAD	2023-05-25	f	10	f
384	271	3	5	PRUEBAS TDR/unidad/UNIDAD	2023-05-25	f	10	f
385	272	5	1	QUINOFTAL/gotas/CAJA	2023-05-25	f	10	f
386	273	5	1	QUINOTIC/gotas/CAJA	2023-05-25	f	10	f
387	274	13	2	RANITIDINA/300MG/BLISTERS	2023-05-25	f	10	f
388	274	2	3	RANITIDINA/50mg/2ml/AMPOLLA	2023-05-25	f	10	f
389	274	2	3	RANITIDINA/ampolla/AMPOLLA	2023-05-25	f	10	f
391	275	3	5	RECOLECTOR DE HECES/unidad/UNIDAD	2023-05-25	f	10	f
392	276	3	5	RECOLECTOR DE ORINA/unidad/UNIDAD	2023-05-25	f	10	f
393	277	3	5	REMDESIVIR/100mg/ampolla/UNIDAD	2023-05-25	f	10	f
395	279	3	5	RINGER LACTATO/1000cc/UNIDAD	2023-05-25	f	10	f
396	279	3	5	RINGER LACTATO/500cc/UNIDAD	2023-05-25	f	10	f
398	281	5	1	RISEDRONATO DE SODIO/35MG/CAJA	2023-05-25	f	10	f
399	282	3	5	ROLLO DE ALGODON/UNIDAD/UNIDAD	2023-05-25	f	10	f
400	283	3	5	ROLLO DE GASA/unidad/UNIDAD	2023-05-25	f	10	f
401	284	5	1	ROWELUK (MONTELUKAST)/4mg/CAJA	2023-05-25	f	10	f
402	285	3	5	SABANA DESCARTABLE/UNIDAD / ROLLO/UNIDAD	2023-05-25	f	10	f
403	287	3	5	SALBUTAMOL     (inhalador)/100mcg/UNIDAD	2023-05-25	f	10	f
405	289	3	5	SALBUTAMOL 0,5%/GOTAS/UNIDAD	2023-05-25	f	10	f
406	290	3	5	SALBUTAMOL VIAL/2,5MG/UNIDAD	2023-05-25	f	10	f
407	286	3	5	SALBUTAMOL/gotas/UNIDAD	2023-05-25	f	10	f
408	286	3	5	SALBUTAMOL/inhalador/UNIDAD	2023-05-25	f	10	f
409	291	3	5	SCALP/#21/UNIDAD	2023-05-25	f	10	f
410	291	3	5	SCALP/#23/UNIDAD	2023-05-25	f	10	f
411	291	3	5	SCALP/#25/UNIDAD	2023-05-25	f	10	f
412	292	3	5	SD SIPHILIS 3.0/PRUEBA RAPIDA/UNIDAD	2023-05-25	f	10	f
413	293	13	2	SECNIDAZOL/BLISTERS/500mg/BLISTERS	2023-05-25	f	10	f
414	293	5	1	SECNIDAZOL/CAJA/500MG/CAJA	2023-05-25	f	10	f
415	294	3	5	SET DE INFUSION MACROGOTERO/unidad/UNIDAD	2023-05-25	f	10	f
416	295	6	6	SINOPHARM/VACUNA/FRASCO	2023-05-25	f	10	f
417	295	3	5	SINOPHARM/VACUNA/UNIDAD	2023-05-25	f	10	f
418	296	3	5	SOLUCION 0.9%/1000 CC/UNIDAD	2023-05-25	f	10	f
419	296	6	6	SOLUCION 0.9%/100cc/FRASCO	2023-05-25	f	10	f
420	296	3	5	SOLUCION 0.9%/100cc/UNIDAD	2023-05-25	f	10	f
421	296	3	5	SOLUCION 0.9%/500ML/UNIDAD	2023-05-25	f	10	f
422	296	3	5	SOLUCION 0.9%/500cc/UNIDAD	2023-05-25	f	10	f
423	297	3	5	SOLUCION CLORURO DE SODIO 0,9%/500ML/UNIDAD	2023-05-25	f	10	f
424	298	3	5	SONDA DE LEVIN/N° 14/UNIDAD	2023-05-25	f	10	f
425	299	6	6	SPUTNIKV/VACUNA/FRASCO	2023-05-25	f	10	f
426	300	3	5	SRO/SOBRE/UNIDAD	2023-05-25	f	10	f
427	302	3	5	SULFADIAZINA DE PLATA/AL 1%/UNIDAD	2023-05-25	f	10	f
428	301	3	5	SULFADIAZINA/CREMA 200GR/UNIDAD	2023-05-25	f	10	f
429	303	2	3	SULFATO DE MAGNESIO/10 %/AMPOLLA	2023-05-25	f	10	f
430	304	3	5	SULFATO FERROSO/GOTAS/UNIDAD	2023-05-25	f	10	f
432	306	3	5	SUTURA PROLENE/# 0/UNIDAD	2023-05-25	f	10	f
435	308	3	5	TERMOMETRO RECTAL/unidad/UNIDAD	2023-05-25	f	10	f
436	309	6	6	THYRONA/100mcg/FRASCO	2023-05-25	f	10	f
437	310	3	5	TIMOLOL/GOTAS/UNIDAD	2023-05-25	f	10	f
438	311	13	2	TIOCOLCHICOSIDO/4MG/BLISTERS	2023-05-25	f	10	f
439	311	5	1	TIOCOLCHICOSIDO/4mg/CAJA	2023-05-25	f	10	f
440	311	2	3	TIOCOLCHICOSIDO/ampolla/AMPOLLA	2023-05-25	f	10	f
442	313	12	12	TRAMADOL (MABRON)/50MG/TABLETA	2023-05-25	f	10	f
443	315	6	6	TRIMETROPIN + SULFAMETAZOL  suspensión/240MG/5ML/FRASCO	2023-05-25	f	10	f
444	314	13	2	TRIMETROPIN + SULFAMETAZOL/80MG+400MG/BLISTERS	2023-05-25	f	10	f
445	316	3	5	TUBO DE ENSAYO/AZUL/UNIDAD	2023-05-25	f	10	f
446	316	3	5	TUBO DE ENSAYO/MORADO/UNIDAD	2023-05-25	f	10	f
447	316	3	5	TUBO DE ENSAYO/ROJO/UNIDAD	2023-05-25	f	10	f
448	317	3	5	VACUNA ANTIGRIPAL/VACUNA/UNIDAD	2023-05-25	f	10	f
449	319	3	5	VALPRON ACID/250mg/UNIDAD	2023-05-25	f	10	f
451	320	2	3	VANCOMICINA/500mg/ampolla/AMPOLLA	2023-05-25	f	10	f
452	321	3	5	VENDA DE YESO/20cm/UNIDAD	2023-05-25	f	10	f
453	322	3	5	VENDA ELASTICA/10cm/UNIDAD	2023-05-25	f	10	f
454	322	3	5	VENDA ELASTICA/20cm/UNIDAD	2023-05-25	f	10	f
431	305	3	5	SUTURA MONONYLON 5-0	2023-09-15	f	10	f
433	307	3	5	SUTURA POLYGLYCOIC ACID #1	2023-09-15	f	10	f
434	307	3	5	SUTURA TRUGLYDE # 3-0	2023-09-15	f	10	f
397	280	6	3	RINSULIN NPH/AMPOLLA	2023-10-11	f	50	t
455	322	3	5	VENDA ELASTICA/5cm/UNIDAD	2023-05-25	f	10	f
456	323	5	1	VERACOR/80MG/CAJA	2023-05-25	f	10	f
457	325	6	6	VITAMINA C   (jarabe)/100MG/5ML/FRASCO	2023-05-25	f	10	f
458	324	13	2	VITAMINA C/500MG/BLISTERS	2023-05-25	f	10	f
460	324	2	3	VITAMINA C/ampolla/AMPOLLA	2023-05-25	f	10	f
461	326	13	2	VITAMINA COMPLEJO B/TAB/BLISTERS	2023-05-25	f	10	f
462	327	5	1	VITAMINA D3/1000ui/CAJA	2023-05-25	f	10	f
463	328	5	1	VITAMINA E/400mg/CAJA	2023-05-25	f	10	f
464	329	3	5	VITAMINA K/AMPOLLA/UNIDAD	2023-05-25	f	10	f
465	330	5	1	ZALDIAR/325mg+37,5mg/CAJA	2023-05-25	f	10	f
1	1	3	5	ACETAMINOFEN/180MG/UNIDAD	2023-05-25	f	10	f
466	331	5	3	FREDDY/PRUEBAS/AMPOLLA	2023-05-26	f	25	t
467	13	12	12	ALBENDAZOL/TABLETA	2023-06-20	f	10	f
42	22	6	6	AMOXICILINA/FRASCO/250MG	2023-06-20	f	10	f
135	93	13	2	CO-TRIMOXAZOL/BLISTERS	2023-06-20	f	10	f
285	192	13	2	LEVOFLOXACINA/BLISTERS/500MG	2023-06-20	f	10	f
470	136	3	5	FIASP (ASPART)/UNIDAD	2023-06-20	f	10	f
211	146	12	12	GABAPENTINA/TABLETA/300MG	2023-06-20	f	10	f
471	332	13	2	CLAVOMID625/BLISTERS	2023-06-20	f	0	f
472	333	2	3	PAMECIL/AMPOLLA/1GR	2023-06-20	f	10	f
473	334	3	5	GLUCOSE/5%/UNIDAD/UNIDAD	2023-06-20	f	10	f
450	318	1	6	VALPRON/FRASCO	2023-06-20	f	10	f
474	337	3	5	BOLSA DE ORINA/UNIDAD	2023-06-20	f	10	f
475	335	3	5	CABESTRILLO/UNIDAD/UNIDAD	2023-06-20	f	10	f
476	336	3	5	SOLUCION/INSTINICA/UNIDAD/UNIDAD	2023-06-20	f	10	f
477	338	3	5	SUERO ORAL/UNIDAD	2023-06-22	f	10	f
479	340	3	5	SUTURA ASSUCRY/CURVA #0/UNIDAD	2023-06-22	f	10	f
480	341	13	2	ALUTRIL FORTE/BLISTERS	2023-06-22	f	10	f
481	342	3	5	SUTURA POLYPROPULENE/ #3-0/UNIDAD	2023-06-22	f	10	f
482	343	3	5	AGUJA 21(G)/UNIDAD	2023-06-22	f	10	f
483	344	3	5	OXIDE ZINCA/15%/UNIDAD	2023-06-22	f	10	f
484	345	3	5	SALBUTAMOL VIAL/5MG/2.5MG/UNIDAD	2023-06-22	f	10	f
485	346	3	5	AMOXICILINA/125MG/5ML/UNIDAD	2023-06-22	f	10	f
486	347	6	6	CO-TRIMAZOL 240MG/5ML/FRASCO	2023-06-22	f	10	f
487	348	6	6	AZITROMICINA/200MG/5ML/FRASCO	2023-06-22	f	10	f
488	23	6	6	AMOXICILINA + AC/FRASCO	2023-06-22	f	10	f
491	235	6	6	MULTIVITAMINICO/FRASCO	2023-06-22	f	10	f
492	176	3	5	INYECTADORA/UNIDAD/20CC/SIN AGUJA	2023-06-22	f	10	f
490	350	12	6	URSODIOL/250MG/FRASCO	2023-06-23	f	10	f
441	312	3	5	TOBRAMICIDA/ DEXAMETASONA (TODEX)/0,3% / 0,1%	2023-07-14	f	10	f
493	324	5	1	VITAMINA C/CAJA	2023-07-18	f	10	f
469	117	12	12	DOXYXYCLINE/TABLETA/100MG	2023-07-31	f	10	f
478	339	13	2	IBUPROFENO 200MG/BLISTERS	2023-08-08	f	10	f
489	49	3	5	BECLOMETASONA/UNIDAD/INHALADOR	2023-08-11	f	10	t
9	4	13	2	ACIDO FOLICO/BLISTERS	2023-09-15	f	10	f
494	351	5	1	BISOPROLOL/CAJA	2023-09-15	f	10	t
468	117	12	12	DOXYXYCLINE/TABLETA (14)	2023-09-15	f	10	f
495	117	12	12	DOXYXYCLINE/TABLETA(12)	2023-09-15	f	10	f
496	352	13	2	FLUOXETINE/BLISTERS	2023-09-15	f	0	f
206	143	6	6	FUMARATO FERROSO/FRASCO	2023-09-15	f	10	f
287	192	6	6	LEVOFLOXACINA/FRASCO	2023-09-15	f	10	f
497	353	3	5	MICONAZOLE NITRATO AL 2%/UNIDAD	2023-09-15	f	0	f
394	278	12	12	RETINOL/30TABLETA	2023-09-15	f	10	f
498	280	6	6	RINSULIN NPH/FRASCO	2023-09-29	f	10	t
499	176	3	5	INYECTADORA 20CC/UNIDAD 	2023-09-29	f	0	f
\.


--
-- TOC entry 4037 (class 0 OID 16954)
-- Dependencies: 289
-- Data for Name: ocupacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ocupacion (id, descripcion) FROM stdin;
1	AMA DE CASA
2	ESTUDIANTE
3	PROFESIONAL
0	NO INDICA
\.


--
-- TOC entry 4039 (class 0 OID 16960)
-- Dependencies: 291
-- Data for Name: parentesco; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.parentesco (id, descripcion, borrado) FROM stdin;
1	MADRE	f
2	PADRE	f
3	HIJA	f
4	HIJO	f
5	CONYUGE	f
\.


--
-- TOC entry 4041 (class 0 OID 16967)
-- Dependencies: 293
-- Data for Name: pre_tipo_medicamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pre_tipo_medicamento (medicamento) FROM stdin;
ACETOMINOFEN
ACICLOVIR
ACIDO FOLICO
ADANTOL
AGUA DESTILADA
AGUA ESTERIL
AGUA OXIGENADA
ALBENDAZOL
ALCOHOL
ALGODÓN 
AMBROXOL
AMOXICILINA
AMOXICILINA + AC suspensión 
AMPICILINA
AMPICILINA + SULBACTAM
ANLODIPINA 
APYRENE (ACETOMINOFEN)
ASAPROL (ASPIRINA)
ASPIRIN 
ASPIRINA (10 tab)
ASPIRINA (30 tab)
ATENOLOL
ATORVASTATINA
AZITROMICINA (blister)
AZITROMICINA (caja 3 tab)
BATA DE CIRUJANO
BATA DE CIRUJANO TALLA L
BATA DE PACIENTE
BENCILPENICILINA PROCAINICA FORTIFICADA
BETAMETASONA
BLOKIUM (ATENOLOL)
BROMAZEPAN
BROMURO DE IPATROPIO
BUDECORT
BUDENAS
BUMENTIN RETARD
CALCIBON CON VITAMINA D
CALCIO CON VIT D3
CAPTOPRIL 
CARBAMAZEPINA
CARBIDOPA + LEVODOPA 
CARDESARTAN
CEFADROXILO
CEFALEXINA
CEFTRIAXONA
CENTROS DE CAMA
CETIRIZINA
CIANOCOBALAMINA (B12)
CIPROFLOXACINA
CLARASOL
CLEMBUNAL
CLINDAMICINA
CLODOXIN (METOCOPLAMIDA +PIRIDOXINA+ VIT B6)
CLOPIDROGEL
CLORURO DE POTASIO
CLORURO DE SODIO
CLOTRIMAZOL
CLOTRIMOXAZOL
COMPLEJO B
CORTINA PARABAN
CORTYNASE
CURITAS
DEPRESORES
DESFLAZACORT
DESLORATADINA
DEXAMETASONA
DEXTROSA 
DIAZEPAM
DICLOFENAC
DICLOFENAC POTASICO
DICLOFENAC SODICO
DOBET
DOL
DORZOLAMIDA + TIMOLOL
DOXYXYCLINE
ENALAPRIL
ENOXAPARINA
EPAX
EPINEFRINA
EQUIPO DE LAPARATOMIA KIT DE OBSTETRICIA
EQUIPO DE SUERO MACROGOTERO
ERITROMICINA
ERITROPOYETINA
ESOMEPRAZOL
ESPECULOS
FEMMEXTRA
FENOMAX
FERGANIC
FESTAL  (caja 10tab)
FESTAL (caja 20tab)
FILGASTRIM
FLAVOXATO
FLORA BALANCE
FLUCONAZOL
FLUCOSET
FUMARATO FERROSO
FUNGISTEROL
FUROSEMIDA
GABAPENTINA
GARABET
GARDENAL
GASA ESTERIL
GEL CONDUCTOR
GENTAMICINA
GERDEX 
GLIBENCLAMIDA
GLICERINA
GLUCOSA
GUANTE DESCARTABLE
GUANTES ESTERIL 
GUATA
HENOVIC
HIDROCORTISONA
HIERRO SACARATO
HIERRO SACAROSA
IBUPROFENO
IMUNIDADE
INYECTADORA
IRRIGOR
IVERMENTINA
JELCO
KELFEN
KETOCONAZOL
KETOPROFENO
KIT BATA DE CIRUJANO
LAMOTRIGINA
LETISAN
LEVOCETIRIZINA 
LEVOFLOXACINA
LEVONOGESTREL
LEVOTIROXINA SODICA
LIDOCAINA
LOPERAM
LORATADINA
LOSAPRES PLUS
LOSARTAN
LOSARTAN POTASICO
LUMBAX (IBUPROFENO)
MAILEN
MASCARILLA DE OXIGENO ADULTO
MASCARILLA DE OXIGENO NIÑO
MATILOL
MELOXICAM
METFORMINA
METOCLOPLAMIDA
METOCLOPRAMIDA
METRONIDAZOL
METROTREXATO
MIGREN
MONALIZ
MONO DE BIOSEGURIDAD
MONO DE CIRUJANO
MONTELUKAST
MUCLAR (AMBROXOL)
MULTIVITAMINA
MULTIVITAMINAS
NANVIT- C
NAPROFEN (IBUPROFENO)
NISTATINA
NITAXOZANIDA
NITROGLICERINA
NORETISTERONA +  ESTRADIOL
NOVORAPID FLEX PEN 
OBTURADOR
OCUPRED
OMEPRAZOL
ONDASETRON
OPAT (OLOPATADINA)
ORAL ZINC
PARACETAMOL
PEDIACORT
PENICILINA G BENZATINICA
PENICILINA G SODICA
PINVEX
PIROXICAM
POLANTAC
POVIDINE
PRAXONA
PREDNISONA
PRESERVATIVOS
PROCATEC (CIPROFLOXACINA)
PRUEBAS DE EMBARAZO
PRUEBAS PCR
PRUEBAS PDR
PRUEBAS TDR
QUINOFTAL
QUINOTIC
RANITIDINA
RECOLECTOR DE HECES
RECOLECTOR DE ORINA
REMDESIVIR
RINGER LACTATO
ROLLO DE GASA
ROWELUK (MONTELUKAST)
SALBUTAMOL
SCALP
SECNIDAZOL
SET DE INFUSION MACROGOTERO
SINUTIL (ACETOMINOFEN)
SOLUCION 0.9%
SULFADIAZINA 
SULFATO DE MAGNESIO
SULFATO FERROSO
TERMOMETRO RECTAL
THYRONA
TIOCOLCHICOSIDO
TODENAC
TODEX
TRIMETROPIN + SULFAMETAZOL
VALPRON
VALPRON ACID
VANCOMICINA
VENDA DE YESO
VENDA ELASTICA 
VITAMINA C
VITAMINA COMPLEJO B
VITAMINA D3
VITAMINA E
ZALDIAR
\.


--
-- TOC entry 4042 (class 0 OID 16972)
-- Dependencies: 294
-- Data for Name: presentacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.presentacion (id, descripcion, fecha_creacion, borrado) FROM stdin;
3	AMPOLLA	2022-06-30	f
4	GALON	2022-06-30	f
5	UNIDAD	2022-06-30	f
6	FRASCO	2022-06-30	f
2	BLISTERS	2022-06-30	f
1	CAJA	2022-06-23	f
10	PAQUETES	2023-05-10	f
11	PARES	2023-05-23	f
12	TABLETA	2023-05-23	f
\.


--
-- TOC entry 4044 (class 0 OID 16980)
-- Dependencies: 296
-- Data for Name: reversos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.reversos (id, tipo_de_reverso, observacion, id_usuario, id_medicamento, cantidad, fecha_reverso, id_entrada) FROM stdin;
\.


--
-- TOC entry 4064 (class 0 OID 21015)
-- Dependencies: 316
-- Data for Name: salidas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.salidas (id, user_id, fecha_salida, id_medicamento, id_entrada, cedula_beneficiario, cantidad, tipo_beneficiario, borrado, control, id_medico) FROM stdin;
\.


--
-- TOC entry 4046 (class 0 OID 16999)
-- Dependencies: 298
-- Data for Name: tipo_de_sangre; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_de_sangre (id, descripcion) FROM stdin;
1	AB+
2	AB-
3	A+
4	A-
5	B+
6	B-
7	O+
8	O-
0	NO INDICA
\.


--
-- TOC entry 4048 (class 0 OID 17005)
-- Dependencies: 300
-- Data for Name: tipo_medicamento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_medicamento (id, descripcion, fecha_creacion, borrado, categoria_id) FROM stdin;
2	ACICLOVIR	2023-05-25	f	1
3	ACIDO ALENDRONATO	2023-05-25	f	1
4	ACIDO FOLICO	2023-05-25	f	1
5	ACIDO IBANDRONICO	2023-05-25	f	1
6	ACIDO TRANEXAMICO	2023-05-25	f	1
7	ACIDO VALPROICO	2023-05-25	f	1
8	ADANTOL	2023-05-25	f	1
9	ADHESIVO	2023-05-25	f	1
10	AGUA DESTILADA	2023-05-25	f	1
11	AGUA ESTERIL	2023-05-25	f	1
12	AGUA OXIGENADA	2023-05-25	f	1
13	ALBENDAZOL	2023-05-25	f	1
14	ALCOHOL	2023-05-25	f	1
15	ALCOHOL ETILICO	2023-05-25	f	1
16	ALGODON	2023-05-25	f	1
17	ALUPURINOL	2023-05-25	f	1
18	AMBROXOL	2023-05-25	f	1
19	AMINOFILINA	2023-05-25	f	1
20	AMIODARONA	2023-05-25	f	1
21	AMLODIPINA	2023-05-25	f	1
22	AMOXICILINA	2023-05-25	f	1
23	AMOXICILINA + AC	2023-05-25	f	1
24	AMOXICILINA + AC CLAVULANICO	2023-05-25	f	1
25	AMOXICILINA + AC suspensión	2023-05-25	f	1
26	AMPICILINA	2023-05-25	f	1
27	AMPICILINA + SULBACTAM	2023-05-25	f	1
28	ANLODIPINO	2023-05-25	f	1
29	ANTIBACTERIAL CHERRY	2023-05-25	f	1
30	ANTUX	2023-05-25	f	1
31	APYRENE (ACETOMINOFEN)	2023-05-25	f	1
32	ASAPROL (ASPIRINA)	2023-05-25	f	1
33	ASPIRIN	2023-05-25	f	1
34	ASPIRINA	2023-05-25	f	1
35	ASPIRINA (10 tab)	2023-05-25	f	1
36	ASPIRINA (30 tab)	2023-05-25	f	1
37	ASPIRINA (cardioaspirin)	2023-05-25	f	1
38	ATENOLOL	2023-05-25	f	1
39	ATORVASTATINA	2023-05-25	f	1
40	ATORVASTATINA (30 TAB)	2023-05-25	f	1
41	AZITROMICINA	2023-05-25	f	1
42	AZITROMICINA (blister)	2023-05-25	f	1
43	AZITROMICINA (caja 3 tab)	2023-05-25	f	1
44	BACITRECINA	2023-05-25	f	1
45	BATA DE CIRUJANO TALLA L	2023-05-25	f	2
46	BATA DE CIRUJANO TALLA M	2023-05-25	f	2
47	BATA DE CIRUJANO TALLA SS	2023-05-25	f	2
48	BATA DE PACIENTE	2023-05-25	f	2
49	BECLOMETASONA	2023-05-25	f	1
50	BENCILPENICILINA PROCAINICA FORTIFICADA	2023-05-25	f	1
51	BETAGEN SOLSPEM	2023-05-25	f	1
52	BETAMETASON  (ampolla)	2023-05-25	f	1
53	BETAMETASONA	2023-05-25	f	1
54	BISTURÍ	2023-05-25	f	3
55	BLANQUEADOR	2023-05-25	f	2
56	BLANQUEADOR NEVEX	2023-05-25	f	2
57	BLOKIUM (ATENOLOL)	2023-05-25	f	1
58	BROMAZEPAN	2023-05-25	f	1
59	BROMURO DE IPATROPIO	2023-05-25	f	1
60	BUDECORT	2023-05-25	f	1
61	BUDENAS	2023-05-25	f	1
62	BUDESONIDA	2023-05-25	f	1
63	BUDESONIDA  (inhalador)	2023-05-25	f	1
64	BUMENTIN RETARD	2023-05-25	f	1
65	CALCIBON CON VITAMINA D	2023-05-25	f	1
66	CALCIO CON VIT D3	2023-05-25	f	1
67	CAPTOPRIL	2023-05-25	f	1
68	CARBAMAZEPINA	2023-05-25	f	1
69	CARBIDOPA + LEVODOPA	2023-05-25	f	1
70	CARDESARTAN	2023-05-25	f	1
71	CARVATIVIR (GOTAS)	2023-05-25	f	1
72	CARVERIDOL	2023-05-25	f	1
73	CEFADROXILO	2023-05-25	f	1
74	CEFALEXINA	2023-05-25	f	1
75	CEFTRIAXONA	2023-05-25	f	1
76	CENTROS DE CAMA	2023-05-25	f	2
77	CETIRIZINA	2023-05-25	f	1
78	CIANOCOBALAMINA (B12)	2023-05-25	f	1
79	CIPROFLOXACINA	2023-05-25	f	1
80	CISTOFLO	2023-05-25	f	1
81	CLARASOL	2023-05-25	f	1
82	CLEMBUNAL	2023-05-25	f	1
83	CLINDAMICINA	2023-05-25	f	1
84	CLODOXIN (METOCOPLAMIDA +PIRIDOXINA+ VIT B6)	2023-05-25	f	1
85	CLOPIDOGREL	2023-05-25	f	1
86	CLORHIDRATO DE VERAPEMILO	2023-05-25	f	1
87	CLORURO DE POTASIO	2023-05-25	f	1
88	CLORURO DE SODIO	2023-05-25	f	1
89	CLOTRIMAXOL + GENTAMICINA +BETAMETASONA	2023-05-25	f	1
90	CLOTRIMAZOL	2023-05-25	f	1
91	CLOTRIMAZOL (OVULOS)	2023-05-25	f	1
92	CO -TRIMOXAZOL	2023-05-25	f	1
93	CO-TRIMOXAZOL	2023-05-25	f	1
94	COLYPAN	2023-05-25	f	1
95	COMPLEJO B	2023-05-25	f	1
96	COMPRESA ESTERIL	2023-05-25	f	2
97	CORTINA PARABAN	2023-05-25	f	3
98	CORTYNASE	2023-05-25	f	1
99	CURITAS	2023-05-25	f	1
100	DAYZOL	2023-05-25	f	1
101	DEPLAZACORT	2023-05-25	f	1
102	DEPRESORES	2023-05-25	f	1
103	DESLORATADINA	2023-05-25	f	1
104	DEXAMETASONA	2023-05-25	f	1
105	DEXLITOS SRO	2023-05-25	f	1
106	DEXTROSA 10%	2023-05-25	f	1
107	DEXTROSA 5%	2023-05-25	f	1
108	DIAZEPAM	2023-05-25	f	1
109	DICLOFENAC POTASICO	2023-05-25	f	1
110	DICLOFENAC SODICO	2023-05-25	f	1
111	DIGOXINA	2023-05-25	f	1
112	DIPIRONA	2023-05-25	f	1
113	DOBET	2023-05-25	f	1
114	DOL	2023-05-25	f	1
115	DOMPERIDONA	2023-05-25	f	1
116	DORZOLAMIDA + TIMOLOL	2023-05-25	f	1
117	DOXYXYCLINE	2023-05-25	f	1
118	ENALAPRIL	2023-05-25	f	1
119	ENOXAPARINA	2023-05-25	f	1
120	ENOXAPARINA SODICA	2023-05-25	f	1
121	EPAX	2023-05-25	f	1
122	EPINEFRINA	2023-05-25	f	1
123	EQUIPO DE LAPAROTOMIA	2023-05-25	f	1
124	EQUIPO DE SUERO MACROGOTERO	2023-05-25	f	1
125	EQUIPO DE TRANFUSION	2023-05-25	f	1
126	ERITROMICINA	2023-05-25	f	1
127	ERITROPOYETINA	2023-05-25	f	1
128	ESOMEPRAZOL	2023-05-25	f	1
129	ESPECULOS	2023-05-25	f	2
130	ESPIROLACTONA	2023-05-25	f	1
131	FEMMEXTRA	2023-05-25	f	1
132	FENOMAX	2023-05-25	f	1
133	FERGANIC	2023-05-25	f	1
134	FESTAL  (caja 10tab)	2023-05-25	f	1
135	FESTAL (caja 20tab)	2023-05-25	f	1
136	FIASP (ASPART)	2023-05-25	f	1
137	FLAVOXATO	2023-05-25	f	1
138	FLORA BALANCE	2023-05-25	f	1
139	FLUCONAZOL	2023-05-25	f	1
140	FLUTICASONE	2023-05-25	f	1
141	FLUVIRIL	2023-05-25	f	1
142	FOSFATO DE DEXAMETASONA	2023-05-25	f	1
143	FUMARATO FERROSO	2023-05-25	f	1
144	FUMARATO FERROSO + AC FOLICO	2023-05-25	f	1
145	FUROSEMIDA	2023-05-25	f	1
146	GABAPENTINA	2023-05-25	f	1
147	GARABET	2023-05-25	f	1
148	GARDENAL	2023-05-25	f	1
149	GASA	2023-05-25	f	2
150	GASA ESTERIL	2023-05-25	f	2
151	GEL ANTIBACTERIAL	2023-05-25	f	1
152	GEL CONDUCTOR	2023-05-25	f	1
153	GEL DE ULTRASONIDO	2023-05-25	f	1
154	GENTAMICINA	2023-05-25	f	1
155	GERDEX	2023-05-25	f	1
156	GLIBENCLAMIDA	2023-05-25	f	1
157	GLICERINA	2023-05-25	f	1
158	GLICLAZIDE	2023-05-25	f	1
159	GLIMEPIRIDE	2023-05-25	f	1
160	GLIPIZIDE	2023-05-25	f	1
161	GUANTE DESCARTABLE	2023-05-25	f	2
162	GUANTES DESCARTABLES	2023-05-25	f	2
163	GUANTES ESTERIL	2023-05-25	f	2
164	GUATA	2023-05-25	f	1
165	HIDROCLOROTIAZIDA	2023-05-25	f	1
166	HIDROCORTISONA	2023-05-25	f	1
167	HIERRO                 suspensión	2023-05-25	f	1
168	HIERRO               (SACAROSA DE HIERRO)	2023-05-25	f	1
169	HIERRO SACARATO	2023-05-25	f	1
170	HIERRO SACAROSA	2023-05-25	f	1
171	HIOSCINA        (BUSCAPINA)	2023-05-25	f	1
172	HIV/ SIPHILIS DUO	2023-05-25	f	1
173	IBUPROFENO	2023-05-25	f	1
174	IBUPROFENO         suspensión	2023-05-25	f	1
175	IMUNIDADE	2023-05-25	f	1
176	INYECTADORA	2023-05-25	f	2
177	IRON	2023-05-25	f	1
178	IRRIGOR	2023-05-25	f	1
179	ISOSORBIDA	2023-05-25	f	1
180	IVERMENTINA	2023-05-25	f	1
181	JELCO	2023-05-25	f	2
182	KELFEN	2023-05-25	f	1
183	KETOCONAZOL	2023-05-25	f	1
184	KETOPROFENO	2023-05-25	f	1
185	KIT BATA DE CIRUJANO	2023-05-25	f	2
186	KIT DE OBSTETRICIA	2023-05-25	f	3
187	LAGRIMAS ARTIFICIALES	2023-05-25	f	1
188	LAMOTRIGINA	2023-05-25	f	1
189	LETISAN	2023-05-25	f	1
190	LEUKOPLAST	2023-05-25	f	1
191	LEVOCETIRIZINA	2023-05-25	f	1
192	LEVOFLOXACINA	2023-05-25	f	1
193	LEVONOGESTREL	2023-05-25	f	1
194	LEVOTIROXINA	2023-05-25	f	1
195	LEVOTIROXINA (TYRONA)	2023-05-25	f	1
196	LEVOTIROXINA SODICA	2023-05-25	f	1
197	LIDOCAINA	2023-05-25	f	1
198	LIMPIADOR LARK	2023-05-25	f	1
199	LIMPIADOR MULTIUSO	2023-05-25	f	1
200	LIPOFAR (SINVASTATINA)	2023-05-25	f	1
201	LIPRESAM (LISINOPRIL)	2023-05-25	f	1
202	LOPAMIDOL (YODO)	2023-05-25	f	1
203	LOPERAM	2023-05-25	f	1
204	LORADEX (LORATADINA)	2023-05-25	f	1
205	LORATADINA	2023-05-25	f	1
206	LORATADINA JARABE	2023-05-25	f	1
207	LOSAPRES PLUS	2023-05-25	f	1
208	LOSARTAN POTASICO	2023-05-25	f	1
209	LUMBAX (IBUPROFENO)	2023-05-25	f	1
210	MASCARILLA DE NEBULIZAR	2023-05-25	f	3
211	MASCARILLA DE OXIGENO ADULTO	2023-05-25	f	3
212	MASCARILLA DE OXIGENO NIÑO	2023-05-25	f	3
213	MATILOL	2023-05-25	f	1
214	MEBENDAZOL	2023-05-25	f	1
215	MEDROXIPROGESTERONA + ESTRADIOL	2023-05-25	f	1
216	MELOXICAM	2023-05-25	f	1
217	MELOXICAM    (ampolla)	2023-05-25	f	1
218	METFORMINA	2023-05-25	f	1
219	METHYLPREDNISOLONE	2023-05-25	f	1
220	METILDOPA	2023-05-25	f	1
221	METOCLOPLAMIDA	2023-05-25	f	1
222	METOCLOPRAMIDA	2023-05-25	f	1
223	METOCLOPRAMIDA     (ampolla)	2023-05-25	f	1
224	METRONIDAZOL	2023-05-25	f	1
225	METRONIDAZOL + MICONAZOL     (óvulos)	2023-05-25	f	1
226	METROTREXATO	2023-05-25	f	1
227	MIGREN	2023-05-25	f	1
228	MONALIZ	2023-05-25	f	1
229	MONO DE BIOSEGURIDAD	2023-05-25	f	2
230	MONO DE CIRUJANO	2023-05-25	f	2
231	MONTELIKAST	2023-05-25	f	1
232	MUCLAR (AMBROXOL)	2023-05-25	f	1
233	MULTIVITAMINA	2023-05-25	f	1
234	MULTIVITAMINAS	2023-05-25	f	1
235	MULTIVITAMINICO	2023-05-25	f	1
236	MULTIVITAMINICO suspensión	2023-05-25	f	1
237	NABUMETONE	2023-05-25	f	1
238	NAFAZOLINA     (gotas)	2023-05-25	f	1
239	NANVIT- C	2023-05-25	f	1
240	NIMODIPINO	2023-05-25	f	1
241	NISTATINA	2023-05-25	f	1
242	NITAXOZANIDA	2023-05-25	f	1
243	NITROGLICERINA	2023-05-25	f	1
244	NORETISTERONA +  ESTRADIOL	2023-05-25	f	1
245	NOVORAPID FLEX PEN	2023-05-25	f	1
246	OBTURADOR	2023-05-25	f	2
247	OCUPRED	2023-05-25	f	1
248	OLOPATADINA       (gotas)	2023-05-25	f	1
249	OMEPRAZOL	2023-05-25	f	1
250	ONDASETRON	2023-05-25	f	1
251	OPAT (OLOPATADINA)	2023-05-25	f	1
252	PANTROPAZOL      ( frasco)	2023-05-25	f	1
253	PARACETAMOL	2023-05-25	f	1
254	PARACETAMOL    suspensión	2023-05-25	f	1
255	PEDIACORT	2023-05-25	f	1
256	PENICILINA  BENZATINICA     (ampolla)	2023-05-25	f	1
257	PENICILINA G BENZATINICA	2023-05-25	f	1
258	PENICILINA G SODICA	2023-05-25	f	1
259	PINVEX	2023-05-25	f	1
260	PIROXICAM	2023-05-25	f	1
261	PLUMPY NUT	2023-05-25	f	1
262	POLANTAC	2023-05-25	f	1
263	POVIDINE	2023-05-25	f	1
264	PRAXONA	2023-05-25	f	1
265	PREDNISOLONA	2023-05-25	f	1
266	PREDNISONA	2023-05-25	f	1
267	PRESERVATIVOS	2023-05-25	f	1
268	PRUEBAS DE EMBARAZO	2023-05-25	f	1
269	PRUEBAS PCR	2023-05-25	f	1
270	PRUEBAS PDR	2023-05-25	f	1
271	PRUEBAS TDR	2023-05-25	f	1
272	QUINOFTAL	2023-05-25	f	1
273	QUINOTIC	2023-05-25	f	1
274	RANITIDINA	2023-05-25	f	1
275	RECOLECTOR DE HECES	2023-05-25	f	2
276	RECOLECTOR DE ORINA	2023-05-25	f	2
277	REMDESIVIR	2023-05-25	f	1
278	RETINOL	2023-05-25	f	1
279	RINGER LACTATO	2023-05-25	f	1
280	RINSULIN NPH	2023-05-25	f	1
281	RISEDRONATO DE SODIO	2023-05-25	f	1
282	ROLLO DE ALGODON	2023-05-25	f	2
283	ROLLO DE GASA	2023-05-25	f	2
284	ROWELUK (MONTELUKAST)	2023-05-25	f	1
285	SABANA DESCARTABLE	2023-05-25	f	2
286	SALBUTAMOL	2023-05-25	f	1
287	SALBUTAMOL     (inhalador)	2023-05-25	f	2
288	SALBUTAMOL  (VIAL)	2023-05-25	f	2
289	SALBUTAMOL 0,5%	2023-05-25	f	1
290	SALBUTAMOL VIAL	2023-05-25	f	1
291	SCALP	2023-05-25	f	1
292	SD SIPHILIS 3.0	2023-05-25	f	1
293	SECNIDAZOL	2023-05-25	f	1
294	SET DE INFUSION MACROGOTERO	2023-05-25	f	2
295	SINOPHARM	2023-05-25	f	1
296	SOLUCION 0.9%	2023-05-25	f	1
297	SOLUCION CLORURO DE SODIO 0,9%	2023-05-25	f	1
298	SONDA DE LEVIN	2023-05-25	f	1
299	SPUTNIKV	2023-05-25	f	1
300	SRO	2023-05-25	f	1
301	SULFADIAZINA	2023-05-25	f	1
302	SULFADIAZINA DE PLATA	2023-05-25	f	1
303	SULFATO DE MAGNESIO	2023-05-25	f	1
304	SULFATO FERROSO	2023-05-25	f	1
305	SUTURA NYLON	2023-05-25	f	2
306	SUTURA PROLENE	2023-05-25	f	2
307	SUTURA VICRYL	2023-05-25	f	2
308	TERMOMETRO RECTAL	2023-05-25	f	3
309	THYRONA	2023-05-25	f	1
310	TIMOLOL	2023-05-25	f	1
311	TIOCOLCHICOSIDO	2023-05-25	f	1
312	TOBRAMICIDA/ DEXAMETASONA (TODEX)	2023-05-25	f	1
313	TRAMADOL (MABRON)	2023-05-25	f	1
314	TRIMETROPIN + SULFAMETAZOL	2023-05-25	f	1
315	TRIMETROPIN + SULFAMETAZOL  suspensión	2023-05-25	f	1
316	TUBO DE ENSAYO	2023-05-25	f	3
317	VACUNA ANTIGRIPAL	2023-05-25	f	1
318	VALPRON	2023-05-25	f	1
319	VALPRON ACID	2023-05-25	f	1
320	VANCOMICINA	2023-05-25	f	1
321	VENDA DE YESO	2023-05-25	f	2
322	VENDA ELASTICA	2023-05-25	f	2
323	VERACOR	2023-05-25	f	1
324	VITAMINA C	2023-05-25	f	1
325	VITAMINA C   (jarabe)	2023-05-25	f	1
326	VITAMINA COMPLEJO B	2023-05-25	f	1
327	VITAMINA D3	2023-05-25	f	1
328	VITAMINA E	2023-05-25	f	1
329	VITAMINA K	2023-05-25	f	1
330	ZALDIAR	2023-05-25	f	1
1	ACETAMINOFEN 	2023-05-25	f	1
331	FREDDY/PRUEBAS	2023-05-26	f	1
332	CLAVOMID625	2023-06-20	f	1
333	PAMECIL	2023-06-20	f	1
334	GLUCOSE/5%/UNIDAD	2023-06-20	f	1
335	CABESTRILLO/UNIDAD	2023-06-20	f	2
336	SOLUCION/INSTINICA/UNIDAD	2023-06-20	f	1
337	BOLSA DE ORINA/UUNIADA	2023-06-20	f	2
338	SUERO ORAL	2023-06-22	f	1
339	IBUPOFENO 200MG	2023-06-22	f	1
340	SUTURA ASSUCRY/CURVA #0	2023-06-22	f	1
341	ALUTRIL FORTE	2023-06-22	f	1
342	SUTURA POLYPROPULENE/ #3-0	2023-06-22	f	1
343	AGUJA 21(G)	2023-06-22	f	1
344	OXIDE ZINCA/15%	2023-06-22	f	1
345	SALBUTAMOL VIAL/5MG/2.5MG	2023-06-22	f	1
346	AMOXICILINA/125MG/5ML	2023-06-22	f	1
347	CO-TRIMAZOL 240MG/5ML	2023-06-22	f	1
348	AZITROMICINA/200MG/5ML	2023-06-22	f	1
349	AMOXICILINA + AC	2023-06-22	f	1
350	URSODIOL/250MG	2023-06-22	f	1
351	BISOPROLOL	2023-09-15	f	1
352	FLUOXETINE	2023-09-15	f	1
353	MICONAZOLE NITRATO AL 2%	2023-09-15	f	1
\.


--
-- TOC entry 4050 (class 0 OID 17014)
-- Dependencies: 302
-- Data for Name: titulares; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.titulares (id, cedula_trabajador, nacionalidad, nombre, apellido, telefono, sexo, fecha_nacimiento, tipo_de_personal, ubicacion_administrativa, borrado, estado_civil, tipo_de_sangre, grado_intruccion_id, ocupacion_id) FROM stdin;
\.


--
-- TOC entry 4053 (class 0 OID 17038)
-- Dependencies: 305
-- Data for Name: ubicacion_administrativa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.ubicacion_administrativa (id, descripcion, borrado) FROM stdin;
1	PATENTE	f
2	REGIONES	f
3	INDICACIONES GEOGRAFICAS	f
4	DERECHO DE AUTOR	f
5	REGISTRO DE LA PROPIEDAD INDUSTRIAL	f
6	MARCAS Y OTROS SIGNOS DISTINTIVOS	f
7	DIRECCION GENERAL	f
8	GESTION HUMANA	f
9	ASESORIA JURIDICA	f
10	DIRECCION DE TECNOLOGIA Y SISTEMAS DE INFORMACION	f
11	PLANIFICACION Y PRESUPUESTO	f
12	RELACIONES INTERNACIONALES	f
13	DIFUSION Y COOPERACION	f
14	ATENCION AL CUIDADANO	f
15	ADMINISTRACION	f
16	POLITICAS PUBLICAS	f
\.


--
-- TOC entry 4055 (class 0 OID 17045)
-- Dependencies: 307
-- Data for Name: unidad_medida; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.unidad_medida (id, descripcion, fecha_creacion, borrado) FROM stdin;
2	cc	2022-06-23	f
3	rollos	2022-07-27	f
4	u.i	2022-08-30	f
5	gotas	2022-09-01	f
6	unidad	2022-09-01	f
7	cm	2022-09-01	f
8	crema	2022-09-01	f
9	tab	2022-09-01	f
10	ml	2022-09-01	f
1	mg	2022-06-23	f
12	prueba0	2022-12-12	f
\.


--
-- TOC entry 4058 (class 0 OID 19990)
-- Dependencies: 310
-- Data for Name: usuarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuarios (id, cedula, nombre, apellido, usuario, password, tipousuario, borrado) FROM stdin;
1	19933177	admin	admin	admin	admin	1	f
2	19933178	CARLOS	PEREZ	carlos	123456	2	f
3	19999999	MARIA	MORILLO	mmorillo	123456	3	f
\.


--
-- TOC entry 4133 (class 0 OID 0)
-- Dependencies: 211
-- Name: abortos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.abortos_id_seq', 12, true);


--
-- TOC entry 4134 (class 0 OID 0)
-- Dependencies: 213
-- Name: alergias_medicamentos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.alergias_medicamentos_id_seq', 17, true);


--
-- TOC entry 4135 (class 0 OID 0)
-- Dependencies: 215
-- Name: antecedentes_gineco_osbtetrico_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.antecedentes_gineco_osbtetrico_id_seq', 6, true);


--
-- TOC entry 4136 (class 0 OID 0)
-- Dependencies: 217
-- Name: antecedentes_patologicosf_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.antecedentes_patologicosf_id_seq', 22, true);


--
-- TOC entry 4137 (class 0 OID 0)
-- Dependencies: 219
-- Name: antecedentes_patologicosp_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.antecedentes_patologicosp_id_seq', 31, true);


--
-- TOC entry 4138 (class 0 OID 0)
-- Dependencies: 221
-- Name: antecedentes_quirurgicos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.antecedentes_quirurgicos_id_seq', 32, true);


--
-- TOC entry 4139 (class 0 OID 0)
-- Dependencies: 223
-- Name: cesarias_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.cesarias_id_seq', 11, true);


--
-- TOC entry 4140 (class 0 OID 0)
-- Dependencies: 225
-- Name: consultas_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.consultas_id_seq', 2, true);


--
-- TOC entry 4141 (class 0 OID 0)
-- Dependencies: 319
-- Name: detalle_notas_entrega_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.detalle_notas_entrega_id_seq', 1, false);


--
-- TOC entry 4142 (class 0 OID 0)
-- Dependencies: 321
-- Name: enfermedad_actual_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.enfermedad_actual_id_seq', 1, false);


--
-- TOC entry 4143 (class 0 OID 0)
-- Dependencies: 227
-- Name: enfermedades_sexuales_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.enfermedades_sexuales_id_seq', 10, true);


--
-- TOC entry 4144 (class 0 OID 0)
-- Dependencies: 229
-- Name: especialidades_id_especialidad_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.especialidades_id_especialidad_seq', 9, true);


--
-- TOC entry 4145 (class 0 OID 0)
-- Dependencies: 231
-- Name: examen_fisico_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.examen_fisico_id_seq', 36, true);


--
-- TOC entry 4146 (class 0 OID 0)
-- Dependencies: 233
-- Name: gestacion_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.gestacion_id_seq', 12, true);


--
-- TOC entry 4147 (class 0 OID 0)
-- Dependencies: 235
-- Name: habito_historial_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.habito_historial_id_seq', 31, true);


--
-- TOC entry 4148 (class 0 OID 0)
-- Dependencies: 237
-- Name: habitos_psicosociales_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.habitos_psicosociales_id_seq', 4, true);


--
-- TOC entry 4149 (class 0 OID 0)
-- Dependencies: 239
-- Name: historial_informativo_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.historial_informativo_id_seq', 12, true);


--
-- TOC entry 4150 (class 0 OID 0)
-- Dependencies: 241
-- Name: historial_medico_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.historial_medico_id_seq', 527, true);


--
-- TOC entry 4151 (class 0 OID 0)
-- Dependencies: 243
-- Name: impresion_diag_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.impresion_diag_id_seq', 52, true);


--
-- TOC entry 4152 (class 0 OID 0)
-- Dependencies: 245
-- Name: medicamentos_medicina_interna_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.medicamentos_medicina_interna_id_seq', 30, true);


--
-- TOC entry 4153 (class 0 OID 0)
-- Dependencies: 311
-- Name: medicos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.medicos_id_seq', 1, true);


--
-- TOC entry 4154 (class 0 OID 0)
-- Dependencies: 247
-- Name: metodos_anticonceptibos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.metodos_anticonceptibos_id_seq', 7, true);


--
-- TOC entry 4155 (class 0 OID 0)
-- Dependencies: 317
-- Name: notas_entregas_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.notas_entregas_id_seq', 1, false);


--
-- TOC entry 4156 (class 0 OID 0)
-- Dependencies: 249
-- Name: numeros_romanos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.numeros_romanos_id_seq', 10, true);


--
-- TOC entry 4157 (class 0 OID 0)
-- Dependencies: 251
-- Name: paraclinicos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.paraclinicos_id_seq', 31, true);


--
-- TOC entry 4158 (class 0 OID 0)
-- Dependencies: 253
-- Name: partos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.partos_id_seq', 12, true);


--
-- TOC entry 4159 (class 0 OID 0)
-- Dependencies: 255
-- Name: plan_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.plan_id_seq', 1, true);


--
-- TOC entry 4160 (class 0 OID 0)
-- Dependencies: 257
-- Name: psicologia_primaria_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.psicologia_primaria_id_seq', 1, true);


--
-- TOC entry 4161 (class 0 OID 0)
-- Dependencies: 259
-- Name: reposos_id_seq; Type: SEQUENCE SET; Schema: historial_clinico; Owner: postgres
--

SELECT pg_catalog.setval('historial_clinico.reposos_id_seq', 2, true);


--
-- TOC entry 4162 (class 0 OID 0)
-- Dependencies: 262
-- Name: auditoria_de_sistema_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auditoria_de_sistema_id_seq', 5, true);


--
-- TOC entry 4163 (class 0 OID 0)
-- Dependencies: 263
-- Name: auditoria_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auditoria_id_seq', 10, true);


--
-- TOC entry 4164 (class 0 OID 0)
-- Dependencies: 265
-- Name: categoria_inventario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.categoria_inventario_id_seq', 4, true);


--
-- TOC entry 4165 (class 0 OID 0)
-- Dependencies: 267
-- Name: compuestos_id_compuesto_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.compuestos_id_compuesto_seq', 9, true);


--
-- TOC entry 4166 (class 0 OID 0)
-- Dependencies: 268
-- Name: compuestos_id_compuesto_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.compuestos_id_compuesto_seq1', 1, false);


--
-- TOC entry 4167 (class 0 OID 0)
-- Dependencies: 270
-- Name: control_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.control_id_seq', 14, true);


--
-- TOC entry 4168 (class 0 OID 0)
-- Dependencies: 272
-- Name: cortesia_adscrito_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cortesia_adscrito_id_seq', 1, true);


--
-- TOC entry 4169 (class 0 OID 0)
-- Dependencies: 274
-- Name: cortesia_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cortesia_id_seq', 1, true);


--
-- TOC entry 4170 (class 0 OID 0)
-- Dependencies: 276
-- Name: datos_titulares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.datos_titulares_id_seq', 486, true);


--
-- TOC entry 4171 (class 0 OID 0)
-- Dependencies: 277
-- Name: ente_adscrito_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ente_adscrito_id_seq', 4, true);


--
-- TOC entry 4172 (class 0 OID 0)
-- Dependencies: 313
-- Name: entradas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.entradas_id_seq', 1, false);


--
-- TOC entry 4173 (class 0 OID 0)
-- Dependencies: 280
-- Name: estado_civil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.estado_civil_id_seq', 6, true);


--
-- TOC entry 4174 (class 0 OID 0)
-- Dependencies: 281
-- Name: familiares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.familiares_id_seq', 1, true);


--
-- TOC entry 4175 (class 0 OID 0)
-- Dependencies: 323
-- Name: familiares_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.familiares_id_seq1', 1, false);


--
-- TOC entry 4176 (class 0 OID 0)
-- Dependencies: 283
-- Name: grado_instruccion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grado_instruccion_id_seq', 6, true);


--
-- TOC entry 4177 (class 0 OID 0)
-- Dependencies: 286
-- Name: medicamentos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.medicamentos_id_seq', 282, true);


--
-- TOC entry 4178 (class 0 OID 0)
-- Dependencies: 287
-- Name: medicamentos_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.medicamentos_id_seq1', 499, true);


--
-- TOC entry 4179 (class 0 OID 0)
-- Dependencies: 288
-- Name: nivel_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nivel_usuario_id_seq', 4, true);


--
-- TOC entry 4180 (class 0 OID 0)
-- Dependencies: 290
-- Name: ocupacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ocupacion_id_seq', 4, true);


--
-- TOC entry 4181 (class 0 OID 0)
-- Dependencies: 292
-- Name: parentesco_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.parentesco_id_seq', 5, true);


--
-- TOC entry 4182 (class 0 OID 0)
-- Dependencies: 295
-- Name: presentacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.presentacion_id_seq', 12, true);


--
-- TOC entry 4183 (class 0 OID 0)
-- Dependencies: 297
-- Name: reversos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.reversos_id_seq', 1, true);


--
-- TOC entry 4184 (class 0 OID 0)
-- Dependencies: 315
-- Name: salidas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.salidas_id_seq', 1, false);


--
-- TOC entry 4185 (class 0 OID 0)
-- Dependencies: 299
-- Name: tipo_de_sangre_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_de_sangre_id_seq', 9, true);


--
-- TOC entry 4186 (class 0 OID 0)
-- Dependencies: 301
-- Name: tipo_medicamento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_medicamento_id_seq', 353, true);


--
-- TOC entry 4187 (class 0 OID 0)
-- Dependencies: 303
-- Name: titulares_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.titulares_id_seq', 1, true);


--
-- TOC entry 4188 (class 0 OID 0)
-- Dependencies: 304
-- Name: titulares_id_seq1; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.titulares_id_seq1', 460, true);


--
-- TOC entry 4189 (class 0 OID 0)
-- Dependencies: 306
-- Name: ubicacion_administrativa_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.ubicacion_administrativa_id_seq', 16, true);


--
-- TOC entry 4190 (class 0 OID 0)
-- Dependencies: 308
-- Name: unidad_medida_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.unidad_medida_id_seq', 12, true);


--
-- TOC entry 4191 (class 0 OID 0)
-- Dependencies: 309
-- Name: usuarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuarios_id_seq', 3, true);


--
-- TOC entry 3700 (class 2606 OID 17135)
-- Name: abortos abortos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.abortos
    ADD CONSTRAINT abortos_pkey PRIMARY KEY (id);


--
-- TOC entry 3702 (class 2606 OID 17137)
-- Name: alergias_medicamentos alergias_medicamentos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.alergias_medicamentos
    ADD CONSTRAINT alergias_medicamentos_pkey PRIMARY KEY (id);


--
-- TOC entry 3704 (class 2606 OID 17139)
-- Name: antecedentes_gineco_osbtetrico antecedentes_gineco_osbtetrico_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_gineco_osbtetrico
    ADD CONSTRAINT antecedentes_gineco_osbtetrico_pkey PRIMARY KEY (id);


--
-- TOC entry 3706 (class 2606 OID 17141)
-- Name: antecedentes_patologicosf antecedentes_patologicosf_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_patologicosf
    ADD CONSTRAINT antecedentes_patologicosf_pkey PRIMARY KEY (id);


--
-- TOC entry 3708 (class 2606 OID 17143)
-- Name: antecedentes_patologicosp antecedentes_patologicosp_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_patologicosp
    ADD CONSTRAINT antecedentes_patologicosp_pkey PRIMARY KEY (id);


--
-- TOC entry 3710 (class 2606 OID 17145)
-- Name: antecedentes_quirurgicos antecedentes_quirurgicos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.antecedentes_quirurgicos
    ADD CONSTRAINT antecedentes_quirurgicos_pkey PRIMARY KEY (id);


--
-- TOC entry 3712 (class 2606 OID 17147)
-- Name: cesarias cesarias_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.cesarias
    ADD CONSTRAINT cesarias_pkey PRIMARY KEY (id);


--
-- TOC entry 3714 (class 2606 OID 17149)
-- Name: consultas consultas_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.consultas
    ADD CONSTRAINT consultas_pkey PRIMARY KEY (id);


--
-- TOC entry 3804 (class 2606 OID 21045)
-- Name: detalle_notas_entrega detalle_notas_entrega_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.detalle_notas_entrega
    ADD CONSTRAINT detalle_notas_entrega_pkey PRIMARY KEY (id);


--
-- TOC entry 3806 (class 2606 OID 21062)
-- Name: enfermedad_actual enfermedad_actual_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.enfermedad_actual
    ADD CONSTRAINT enfermedad_actual_pkey PRIMARY KEY (id);


--
-- TOC entry 3716 (class 2606 OID 17155)
-- Name: enfermedades_sexuales enfermedades_sexuales_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.enfermedades_sexuales
    ADD CONSTRAINT enfermedades_sexuales_pkey PRIMARY KEY (id);


--
-- TOC entry 3718 (class 2606 OID 17157)
-- Name: especialidades especialidades_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.especialidades
    ADD CONSTRAINT especialidades_pkey PRIMARY KEY (id_especialidad);


--
-- TOC entry 3720 (class 2606 OID 17159)
-- Name: examen_fisico examen_fisico_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.examen_fisico
    ADD CONSTRAINT examen_fisico_pkey PRIMARY KEY (id);


--
-- TOC entry 3722 (class 2606 OID 17161)
-- Name: gestacion gestacion_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.gestacion
    ADD CONSTRAINT gestacion_pkey PRIMARY KEY (id);


--
-- TOC entry 3726 (class 2606 OID 17163)
-- Name: habitos_psicosociales habitos_psicosociales_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habitos_psicosociales
    ADD CONSTRAINT habitos_psicosociales_pkey PRIMARY KEY (id);


--
-- TOC entry 3728 (class 2606 OID 17165)
-- Name: historial_informativo historial_informativo_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.historial_informativo
    ADD CONSTRAINT historial_informativo_pkey PRIMARY KEY (id);


--
-- TOC entry 3730 (class 2606 OID 17167)
-- Name: historial_medico historial_medico_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.historial_medico
    ADD CONSTRAINT historial_medico_pkey PRIMARY KEY (id);


--
-- TOC entry 3732 (class 2606 OID 17169)
-- Name: impresion_diag impresion_diag_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.impresion_diag
    ADD CONSTRAINT impresion_diag_pkey PRIMARY KEY (id);


--
-- TOC entry 3734 (class 2606 OID 17171)
-- Name: medicamentos_patologias medicamentos_medicina_interna_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.medicamentos_patologias
    ADD CONSTRAINT medicamentos_medicina_interna_pkey PRIMARY KEY (id);


--
-- TOC entry 3796 (class 2606 OID 20011)
-- Name: medicos medicos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.medicos
    ADD CONSTRAINT medicos_pkey PRIMARY KEY (cedula);


--
-- TOC entry 3736 (class 2606 OID 17175)
-- Name: metodos_anticonceptibos metodos_anticonceptibos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.metodos_anticonceptibos
    ADD CONSTRAINT metodos_anticonceptibos_pkey PRIMARY KEY (id);


--
-- TOC entry 3802 (class 2606 OID 21036)
-- Name: notas_entregas notas_entregas_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.notas_entregas
    ADD CONSTRAINT notas_entregas_pkey PRIMARY KEY (id);


--
-- TOC entry 3738 (class 2606 OID 17179)
-- Name: numeros_romanos numeros_romanos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.numeros_romanos
    ADD CONSTRAINT numeros_romanos_pkey PRIMARY KEY (id);


--
-- TOC entry 3740 (class 2606 OID 17181)
-- Name: paraclinicos paraclinicos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.paraclinicos
    ADD CONSTRAINT paraclinicos_pkey PRIMARY KEY (id);


--
-- TOC entry 3742 (class 2606 OID 17183)
-- Name: partos partos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.partos
    ADD CONSTRAINT partos_pkey PRIMARY KEY (id);


--
-- TOC entry 3724 (class 2606 OID 17185)
-- Name: habito_historial pk_habito_historial; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habito_historial
    ADD CONSTRAINT pk_habito_historial PRIMARY KEY (id);


--
-- TOC entry 3744 (class 2606 OID 17187)
-- Name: plan plan_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.plan
    ADD CONSTRAINT plan_pkey PRIMARY KEY (id);


--
-- TOC entry 3746 (class 2606 OID 17189)
-- Name: psicologia_primaria psicologia_primaria_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.psicologia_primaria
    ADD CONSTRAINT psicologia_primaria_pkey PRIMARY KEY (id);


--
-- TOC entry 3748 (class 2606 OID 17191)
-- Name: reposos reposos_pkey; Type: CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.reposos
    ADD CONSTRAINT reposos_pkey PRIMARY KEY (id);


--
-- TOC entry 3752 (class 2606 OID 17203)
-- Name: auditoria_de_sistema auditoria_de_sistema_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria_de_sistema
    ADD CONSTRAINT auditoria_de_sistema_pkey PRIMARY KEY (id);


--
-- TOC entry 3750 (class 2606 OID 17205)
-- Name: auditoria auditoria_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.auditoria
    ADD CONSTRAINT auditoria_pkey PRIMARY KEY (id);


--
-- TOC entry 3758 (class 2606 OID 17207)
-- Name: control control_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.control
    ADD CONSTRAINT control_pkey PRIMARY KEY (id);


--
-- TOC entry 3764 (class 2606 OID 17209)
-- Name: cortesia_adscrito cortesia_adscrito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia_adscrito
    ADD CONSTRAINT cortesia_adscrito_pkey PRIMARY KEY (id);


--
-- TOC entry 3766 (class 2606 OID 17211)
-- Name: ente_adscrito ente_adscrito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ente_adscrito
    ADD CONSTRAINT ente_adscrito_pkey PRIMARY KEY (id);


--
-- TOC entry 3798 (class 2606 OID 21013)
-- Name: entradas entrada_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.entradas
    ADD CONSTRAINT entrada_pkey PRIMARY KEY (id);


--
-- TOC entry 3768 (class 2606 OID 17215)
-- Name: estado_civil estado_civil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.estado_civil
    ADD CONSTRAINT estado_civil_pkey PRIMARY KEY (id);


--
-- TOC entry 3808 (class 2606 OID 21080)
-- Name: familiares familiares_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.familiares
    ADD CONSTRAINT familiares_pkey PRIMARY KEY (cedula);


--
-- TOC entry 3760 (class 2606 OID 17219)
-- Name: cortesia fk_cedula2; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia
    ADD CONSTRAINT fk_cedula2 PRIMARY KEY (cedula);


--
-- TOC entry 3770 (class 2606 OID 17221)
-- Name: grado_instruccion grado_instruccion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grado_instruccion
    ADD CONSTRAINT grado_instruccion_pkey PRIMARY KEY (id);


--
-- TOC entry 3774 (class 2606 OID 17223)
-- Name: medicamentos medicamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.medicamentos
    ADD CONSTRAINT medicamento_pkey PRIMARY KEY (id);


--
-- TOC entry 3772 (class 2606 OID 17227)
-- Name: grupo_usuario nivel_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grupo_usuario
    ADD CONSTRAINT nivel_usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 3776 (class 2606 OID 17229)
-- Name: ocupacion ocupacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ocupacion
    ADD CONSTRAINT ocupacion_pkey PRIMARY KEY (id);


--
-- TOC entry 3754 (class 2606 OID 17231)
-- Name: categoria_inventario pd_categoria; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.categoria_inventario
    ADD CONSTRAINT pd_categoria PRIMARY KEY (id);


--
-- TOC entry 3756 (class 2606 OID 17233)
-- Name: compuestos pk_id_compuesto; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compuestos
    ADD CONSTRAINT pk_id_compuesto PRIMARY KEY (id_compuesto);


--
-- TOC entry 3778 (class 2606 OID 17235)
-- Name: parentesco pk_id_parentesco; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.parentesco
    ADD CONSTRAINT pk_id_parentesco PRIMARY KEY (id);


--
-- TOC entry 3780 (class 2606 OID 17237)
-- Name: presentacion presentacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.presentacion
    ADD CONSTRAINT presentacion_pkey PRIMARY KEY (id);


--
-- TOC entry 3782 (class 2606 OID 17239)
-- Name: reversos reversos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.reversos
    ADD CONSTRAINT reversos_pkey PRIMARY KEY (id);


--
-- TOC entry 3800 (class 2606 OID 21025)
-- Name: salidas salida_id_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.salidas
    ADD CONSTRAINT salida_id_pkey PRIMARY KEY (id);


--
-- TOC entry 3784 (class 2606 OID 17243)
-- Name: tipo_de_sangre tipo_de_sangre_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_de_sangre
    ADD CONSTRAINT tipo_de_sangre_pkey PRIMARY KEY (id);


--
-- TOC entry 3786 (class 2606 OID 17245)
-- Name: tipo_medicamento tipo_medicamento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_medicamento
    ADD CONSTRAINT tipo_medicamento_pkey PRIMARY KEY (id);


--
-- TOC entry 3788 (class 2606 OID 17247)
-- Name: titulares titulares_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.titulares
    ADD CONSTRAINT titulares_pkey PRIMARY KEY (cedula_trabajador);


--
-- TOC entry 3790 (class 2606 OID 17249)
-- Name: ubicacion_administrativa ubicacion_administrativa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.ubicacion_administrativa
    ADD CONSTRAINT ubicacion_administrativa_pkey PRIMARY KEY (id);


--
-- TOC entry 3762 (class 2606 OID 17251)
-- Name: cortesia un_cortesia_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia
    ADD CONSTRAINT un_cortesia_id UNIQUE (id);


--
-- TOC entry 3792 (class 2606 OID 17253)
-- Name: unidad_medida unidad_medida_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.unidad_medida
    ADD CONSTRAINT unidad_medida_pkey PRIMARY KEY (id);


--
-- TOC entry 3794 (class 2606 OID 19996)
-- Name: usuarios usuarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (id);


--
-- TOC entry 3809 (class 2606 OID 17256)
-- Name: habito_historial fk_habito_id; Type: FK CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.habito_historial
    ADD CONSTRAINT fk_habito_id FOREIGN KEY (habito_id) REFERENCES historial_clinico.habitos_psicosociales(id);


--
-- TOC entry 3815 (class 2606 OID 21046)
-- Name: detalle_notas_entrega nota_entrega_id_fk; Type: FK CONSTRAINT; Schema: historial_clinico; Owner: postgres
--

ALTER TABLE ONLY historial_clinico.detalle_notas_entrega
    ADD CONSTRAINT nota_entrega_id_fk FOREIGN KEY (nota_entrega_id) REFERENCES historial_clinico.notas_entregas(id);


--
-- TOC entry 3810 (class 2606 OID 17276)
-- Name: compuestos compuestos2_id_compuesto_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.compuestos
    ADD CONSTRAINT compuestos2_id_compuesto_fkey FOREIGN KEY (id_compuesto) REFERENCES public.unidad_medida(id);


--
-- TOC entry 3813 (class 2606 OID 17286)
-- Name: tipo_medicamento fk_categoria; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_medicamento
    ADD CONSTRAINT fk_categoria FOREIGN KEY (categoria_id) REFERENCES public.categoria_inventario(id);


--
-- TOC entry 3811 (class 2606 OID 17291)
-- Name: cortesia_adscrito fk_cortesia_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia_adscrito
    ADD CONSTRAINT fk_cortesia_id FOREIGN KEY (id_cortesia) REFERENCES public.cortesia(id);


--
-- TOC entry 3812 (class 2606 OID 17296)
-- Name: cortesia_adscrito fk_ente_asdcrito; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cortesia_adscrito
    ADD CONSTRAINT fk_ente_asdcrito FOREIGN KEY (id_adscrito) REFERENCES public.ente_adscrito(id);


--
-- TOC entry 3814 (class 2606 OID 19997)
-- Name: usuarios usuarios_fk_tipousuario; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT usuarios_fk_tipousuario FOREIGN KEY (tipousuario) REFERENCES public.grupo_usuario(id);


--
-- TOC entry 4078 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2024-01-19 15:02:02 -04

--
-- PostgreSQL database dump complete
--

