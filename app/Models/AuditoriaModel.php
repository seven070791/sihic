<?php

namespace App\Models;

use CodeIgniter\Model;



class AuditoriaModel extends Model
{
    protected $table = 'auditoria';
    protected $allowedFields = ['id', 'fecha_sesion', 'accion'];
    public function fecha_auditoria($id_usuario)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('auditoria');
        $data = [
            'id_usuario' => $id_usuario,

        ];

        $builder->insert($data);
    }



    public function agregarAccion_Entrada($data)
    {

       
        $db      = \Config\Database::connect();
        $builder = $db->table('auditoria');
        $dato['id_usuario'] = $data['user_id'];
        $dato['id_medicamento'] = $data['id_medicamento'];
        $dato['cantidad'] = $data['cantidad'];
        $dato['hora'] = $data['hora'];
        $dato['accion'] = 'Realizo una Entrada';
        $query2 = $builder->insert($dato);
        return $query2;
    }

    public function agregarAccion_Salida($data)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('auditoria');
        $dato['id_usuario'] = $data['user_id'];
        $dato['accion'] = 'Realizo una Salida';
        $dato['id_medicamento'] = $data['id_medicamento'];
        $dato['cantidad'] = $data['cantidad'];
        $dato['hora'] = $data['hora'];
        $query2 = $builder->insert($dato);
        return $query2;
    }

    public function auditoria_reverso($data)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('auditoria');
        $dato['id_usuario'] = $data['user_id'];
        $dato['accion'] = 'Reverso una Entrada ';
        $dato['id_medicamento'] = $data['id_medicamento'];
        $dato['cantidad'] = $data['cantidad'];
        $dato['hora'] = $data['hora'];
        $query2 = $builder->insert($dato);
        return $query2;
    }

    public function auditoria_Reverso_Salida($data)
    {


        $db      = \Config\Database::connect();
        $builder = $db->table('auditoria');
        $dato['id_usuario'] = $data['user_id'];
        $dato['accion'] = 'Reverso una Salida';
        $dato['id_medicamento'] = $data['id_medicamento'];
        $dato['cantidad'] = $data['cantidad'];
        $dato['hora'] = $data['hora'];
        $query2 = $builder->insert($dato);
        return $query2;
    }

    public function auditoria_actualizacion_fv($data)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('auditoria');
        $dato['id_usuario'] = $data['user_id'];
        $dato['accion'] = 'Modificó la Fecha de Vencimiento de ';
        $dato['id_medicamento'] = $data['id_medicamento'];
        $dato['cantidad'] = $data['cantidad'];
        $dato['hora'] = $data['hora'];
        $query2 = $builder->insert($dato);
        return $query2;
    }


    public function listar_auditoria($direccion_ip = null, $dispositivo = null)
    {
       

        $db = \Config\Database::connect();
        $builder = $db->table('auditoria');
        $builder->select(["'$direccion_ip' as direccion_ip ","'$dispositivo' as dispositivo ","usuarios.id id_usuario", "CONCAT(usuarios.nombre, ' ', usuarios.apellido) as nombre", 
        "CONCAT(accion,' (', medicamentos.descripcion,')') as accion", 
        "medicamentos.descripcion", 
        "cantidad", "hora", "to_char(auditoria.fecha_sesion,'dd/mm/yyyy') as fecha_sesion"])
            ->join('usuarios', 'auditoria.id_usuario=usuarios.id')
            ->join('medicamentos', 'auditoria.id_medicamento=medicamentos.id');
        $builder->orderBy('auditoria.fecha_sesion', 'DESC');
        $builder->orderBy('auditoria.hora', 'DESC');
        return $builder->get()->getResultArray();
    }



}
