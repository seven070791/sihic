<?php

namespace App\Models;

use CodeIgniter\Model;

class Consulta_Histotia_Model extends BaseModel
{
	public function agregar_consulta_historial($data)
	{
		$builder = $this->dbconn('historial_clinico.consultas');
		$query = $builder->insert($data);
		return $query;
	}



	public function listar_Historial_consultas($n_historial)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***
		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->select(
	    "hc.id
		,hc.n_historial
		,hc.id_medico
		,hc.borrado
		,hc.tipo_beneficiario
		,hm.cedula
		,to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
		,to_char(hc.fecha_asistencia,'dd/mm/yyyy') as fecha_asistencia, hc.fecha_asistencia as fecha
		,CONCAT(m.nombre,' ', m.apellido) AS nombre
		 ,e.descripcion as especialidad"
		);
		$builder->join(' historial_clinico.medicos as m ', 'hc.id_medico=m.id');
		$builder->join(' historial_clinico.especialidades as e ', 'm.especialidad=e.id_especialidad');
		$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
		$builder->where(['hc.consulta' => true]);
		$builder->where(['hc.borrado' => false]);
		$builder->where(['hm.borrado' => false]);
		$builder->where(['hc.n_historial' => $n_historial]);
		$builder->orderBy('fecha', 'DESC');



		$query = $builder->get();
		return $query;
	}


	public function listar_citas_medicos($medico_id)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= "c.fecha_creacion as fecha_orden";
		$strQuery .= ",c.id";
		$strQuery .= ",c.n_historial";
		$strQuery .= ",hm.cedula";
		$strQuery .= ",hm.id as id_historial_medico";
		$strQuery .= ",CONCAT(b.nombre,' ', b.apellido) AS nombre";
		$strQuery .= ",c.id_medico";
		$strQuery .= ",c.tipo_beneficiario";
		$strQuery .= ",c.fecha_creacion as fecha_creacion";
		$strQuery .= ",c.fecha_asistencia as fecha_asistencia";
		$strQuery .= ",CASE WHEN b.telefono='' THEN '___ ___ ___' ELSE b.telefono END AS telefono";
		$strQuery .= ",to_char(c.fecha_creacion,'dd-mm-yyyy') as fecha_creacion";
		$strQuery .= ",CASE WHEN ea.id is null THEN 'NO_ATENDIDO' else 'ATENDIDO' end as  atendido ";
		$strQuery .= "FROM  ";
		$strQuery .= "historial_clinico.consultas AS c ";
		$strQuery .= "JOIN  ";
		$strQuery .= "historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "JOIN ";
		$strQuery .= "vista_beneficiarios AS b ON hm.cedula=b.cedula ";
		$strQuery .= "LEFT JOIN ";
		$strQuery .= "historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta ";
		$strQuery .= "WHERE ";
		$strQuery .= "hm.borrado=false  ";
		$strQuery .= "AND  ";
		$strQuery .= "c.consulta=false  ";
		$strQuery .= "AND  ";
		$strQuery .= "c.borrado=false ";
		$strQuery .= "AND ";
		$strQuery .= "c.id_medico='$medico_id'";
		$strQuery .= "order by atendido desc ";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function listar_consultas_medicos($medico_id)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery = "SELECT ";
		$strQuery .= "c.fecha_creacion as fecha_orden";
		$strQuery .= ",c.id";
		$strQuery .= ",c.n_historial";
		$strQuery .= ",hm.cedula";
		$strQuery .= ",hm.id as id_historial_medico";
		$strQuery .= ",CONCAT(b.nombre,' ', b.apellido) AS nombre";
		$strQuery .= ",c.id_medico";
		$strQuery .= ",c.tipo_beneficiario";
		$strQuery .= ",c.fecha_creacion as fecha_creacion";
		$strQuery .= ",to_char(c.fecha_asistencia,'dd-mm-yyyy') as fecha_asistencia";
		//$strQuery .= ",c.fecha_asistencia as fecha_asistencia";
		$strQuery .= ",CASE WHEN b.telefono='' THEN '___ ___ ___' ELSE b.telefono END AS telefono";
		$strQuery .= ",to_char(c.fecha_creacion,'dd-mm-yyyy') as fecha_creacion";
		$strQuery .= ",CASE WHEN ea.id is null THEN 'NO_ATENDIDO' else 'ATENDIDO' end as  atendido ";
		$strQuery .= "FROM  ";
		$strQuery .= "historial_clinico.consultas AS c ";
		$strQuery .= "JOIN  ";
		$strQuery .= "historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "JOIN ";
		$strQuery .= "vista_beneficiarios AS b ON hm.cedula=b.cedula ";
		$strQuery .= "LEFT JOIN ";
		$strQuery .= "historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta ";
		$strQuery .= "WHERE ";
		$strQuery .= "hm.borrado=false  ";
		$strQuery .= "AND  ";
		$strQuery .= "c.consulta=true  ";
		$strQuery .= "AND  ";
		$strQuery .= "c.borrado=false ";
		$strQuery .= "AND ";
		$strQuery .= "c.id_medico='$medico_id'";
		$strQuery .= "order by atendido desc ";

		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}







	public function listar_Historial_citas($n_historial)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***
		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->select(
			"hc.id
		,hc.n_historial
		,hc.id_medico
		,hc.borrado as borrado
		,hc.consulta
		,hc.asistencia
		,hc.tipo_beneficiario
		,to_char(hc.fecha_asistencia,'dd/mm/yyyy') as fecha_asistencia
		,hm.cedula
		,to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
		,CONCAT(m.nombre,' ', m.apellido) AS nombre
		 ,e.descripcion as especialidad"
		);
		$builder->join(' historial_clinico.medicos as m ', 'hc.id_medico=m.id');
		$builder->join(' historial_clinico.especialidades as e ', 'm.especialidad=e.id_especialidad');
		$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
		//$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
		$builder->where(['hc.consulta' => false]);
		$builder->where(['hc.borrado' => false]);
		$builder->where(['hm.borrado' => false]);
		$builder->where(['hc.n_historial' => $n_historial]);
		$query = $builder->get();
		return $query;
	}





	public function borrar_consulta($data)
	{
		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->where('hc.id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}

	public function actualizar_asistencia($data)
	{

		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->where('hc.id', $data['id'], 'hc.borrado', 'false');
		$query = $builder->update($data);
		return $query;
	}




	public function listar_signos_vitales($n_historial, $id_consulta)
	{
		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " hc.id";
		$strQuery .= ",hc.peso";
		$strQuery .= ",hc.talla";
		$strQuery .= ",hc.spo2";
		$strQuery .= ",to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion";
		$strQuery .= ",hc.frecuencia_c";
		$strQuery .= ",hc.frecuencia_r";
		$strQuery .= ",hc.temperatura ";
		$strQuery .= ",hc.tension_alta";
		$strQuery .= ",hc.tension_baja ";
		$strQuery .= ",hc.imc";
		$strQuery .= ",hc.motivo_consulta ";
		$strQuery .= ",hc.user_id ";
		$strQuery .= ",CONCAT(usuarios.nombre,' ', usuarios.apellido) AS user_nombre ";
		$strQuery .= "FROM ";
		$strQuery .= "historial_clinico.consultas as hc ";
		$strQuery .= "join usuarios on hc.user_id= usuarios.id ";
		$strQuery .= " where n_historial='$n_historial'";
		$strQuery .= " AND  hc.id=$id_consulta";
		// $strQuery .=" AND ";
		//$strQuery .=" hc.borrado=false";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		//return  $strQuery;
		return $resultado;
	}


	public function actualizar_signos_vitales_citas($data)
	{

		$builder = $this->dbconn('historial_clinico.consultas ');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}

	public function listar_reporte_citas($desde = null, $hasta = null, $medico = 0, $especialidad, $control = 0, $asistio = 0, $today)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " c.id_consulta,c.id_medico,c.n_historial,CONCAT(b.nombre,' ', b.apellido) AS nombre";
		$strQuery .= ",CONCAT(nombremedico,' ', apellidomedico) AS nombremedico, b.cedula";
		$strQuery .= ",descripcion as especialidad,id_especialidad";
		$strQuery .= ",c.tipo_beneficiario,c.fecha_creacion as fecha_creacion,c.fecha_asistencia,c.format_fecha_asistencia ";
		$strQuery .= ",c.asistencia,c.controlasistencia ,c.borrado,c.consulta,";
		$strQuery .= " CASE ";
		$strQuery .= "WHEN b.telefono='' THEN '0' ELSE b.telefono END AS telefono ";
		$strQuery .= "FROM";
		$strQuery .= "(";
		$strQuery .= "SELECT c.id as id_consulta,n_historial,id_medico";
		$strQuery .= ",tipo_beneficiario,";
		$strQuery .= " CASE WHEN c.consulta='t' THEN 'Consulta' ELSE 'Cita'  END AS consulta";
		$strQuery .= ",c.borrado,c.asistencia,to_char(c.fecha_creacion,'dd-mm-yyyy') as fecha_creacion";
		$strQuery .= ",to_char(c.fecha_asistencia,'dd-mm-yyyy') as fecha_asistencia";
		$strQuery .= ",c.fecha_asistencia as format_fecha_asistencia,";
		$strQuery .= " CASE WHEN fecha_asistencia<CURRENT_DATE  and consulta='f' and asistencia='f' then 'NO'";
		$strQuery .= " WHEN fecha_asistencia<CURRENT_DATE and consulta='f' and asistencia='t' then 'SI'";
		$strQuery .= " WHEN fecha_asistencia>=CURRENT_DATE and consulta='f' and asistencia='f' then 'En espera'";
		$strQuery .= " WHEN fecha_asistencia=CURRENT_DATE and consulta='f' and asistencia='t' then 'SI'";
		$strQuery .= " else 'SI' end as controlasistencia ";
		$strQuery .= ",med.nombre as nombremedico";
		$strQuery .= ",med.apellido as apellidomedico";
		$strQuery .= ",e.descripcion";
		$strQuery .= ",e.id_especialidad as id_especialidad";
		$strQuery .= " FROM historial_clinico.consultas as c";
		$strQuery .= " join historial_clinico.medicos as med on c.id_medico=med.id";
		$strQuery .= " join historial_clinico.especialidades as e on med.especialidad=e.id_especialidad";
		$strQuery .= ") as c ,";
		$strQuery .= "(";
		$strQuery .= " select cedula,id as id_historial_medico,n_historial,borrado ";
		$strQuery .= "from historial_clinico.historial_medico";
		$strQuery .= ") as hm ,";
		$strQuery .= "(";
		$strQuery .= "SELECT cedula_trabajador::text as cedula,nombre,apellido,telefono ";
		$strQuery .= "FROM titulares ";
		$strQuery .= "UNION ";
		$strQuery .= "SELECT cedula as cedula,nombre,apellido,telefono  ";
		$strQuery .= "FROM familiares  ";
		$strQuery .= "UNION ";
		$strQuery .= "SELECT cedula as cedula,nombre ,apellido,telefono  ";
		$strQuery .= "FROM cortesia ";
		$strQuery .= ") as b ";
		$strWhere = "";
		$strWhere .= "WHERE c.n_historial=hm.n_historial AND hm.cedula=b.cedula  ";
		$strWhere .= "AND hm.borrado=false ";
		$strWhere .= "AND c.borrado=false ";

		//$strWhere ="";

		if ($especialidad != '0') {

			$strWhere .= " AND id_especialidad='" . $especialidad . "'";
		}

		if ($control != 'Seleccione') {

			$strWhere .= " AND c.consulta='" . $control . "'";
		}

		if ($asistio != 'Seleccione') {
			if ($asistio == 't') {
				$strWhere .= " AND c.asistencia='t'";
			} else if ($asistio == 'f') {
				$strWhere .= "AND c.asistencia='f' and c.format_fecha_asistencia < CURRENT_DATE ";
			} else if ($asistio == 'En espera') {
				$strWhere .= " AND c.asistencia='f' and c.format_fecha_asistencia>=CURRENT_DATE";
			}
		}
		if ($medico != '0') {

			$strWhere .= " AND c.id_medico='" . $medico . "'";
		}

		if ($desde != 'null' and $hasta != 'null') {

			$strWhere .= " AND format_fecha_asistencia BETWEEN '$desde'AND '$hasta'";
		}
		
		
		
		$strQuery = $strQuery . $strWhere;
	
		$strQuery .= " order by c.format_fecha_asistencia desc ";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function listar_consultas_atendidas($medico_id)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " count(c.id) as total_atendidos ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta  ";
		$strQuery .= "WHERE hm.borrado=false AND c.consulta=true ";
		$strQuery .= " AND c.borrado=false AND c.id_medico=$medico_id ";
		$strQuery .= " AND ea.id is not null ";
		$strQuery .= " and c.fecha_creacion=current_date  ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();

		return $resultado;
	}
	public function listar_consultas_not_atendidas($medico_id)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " count(c.id) as total_no_atendidos ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta  ";
		$strQuery .= "WHERE hm.borrado=false AND c.consulta=true ";
		$strQuery .= " AND c.borrado=false AND c.id_medico=$medico_id ";
		$strQuery .= " AND ea.id is  null ";
		$strQuery .= " and c.fecha_creacion=current_date  ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();

		return $resultado;
	}
	public function listar_citas_atendidas($medico_id)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " count(c.id) as total_atendidos ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta  ";
		$strQuery .= "WHERE hm.borrado=false AND c.consulta=false ";
		$strQuery .= " AND c.borrado=false AND c.id_medico=$medico_id ";
		$strQuery .= " AND ea.id is not null ";
		$strQuery .= " and c.fecha_creacion=current_date  ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();

		return $resultado;
	}
	public function listar_citas_not_atendidas($medico_id)
	{

		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " count(c.id) as total_no_atendidos ";
		$strQuery .= "FROM historial_clinico.consultas AS c ";
		$strQuery .= "JOIN historial_clinico.historial_medico hm ON c.n_historial=hm.n_historial ";
		$strQuery .= "LEFT JOIN historial_clinico.enfermedad_actual AS ea ON c.id=ea.id_consulta  ";
		$strQuery .= "WHERE hm.borrado=false AND c.consulta=false ";
		$strQuery .= " AND c.borrado=false AND c.id_medico=$medico_id ";
		$strQuery .= " AND ea.id is  null ";
		$strQuery .= " and c.fecha_creacion=current_date  ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();

		return $resultado;
	}
}
