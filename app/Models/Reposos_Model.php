<?php

namespace App\Models;

use CodeIgniter\Model;

class Reposos_Model extends BaseModel
{
	public function listar_reposos($desde = null, $hasta = null, $id_medico = 0, $especialidad = 0)
	{
		$db = \Config\Database::connect();

		$strQuery = "select ";
		$strQuery .= " r.id,r.n_historial,r.id_medico ,r.motivo,r.horas ,";
		$strQuery .= " to_char(r.fecha_desde,'dd/mm/yyyy') as fecha_desde,";
		$strQuery .= " r.fecha_desde as fecha_desde_n,";
		$strQuery .= " to_char(r.fecha_hasta,'dd/mm/yyyy') as fecha_hasta, ";
		$strQuery .= " r.fecha_hasta as fecha_hasta_n,";
		$strQuery .= " case when  r.fecha_hasta  >current_date then'ACTIVO' else 'VENCIDO' end as estatus,";
		$strQuery .= " to_char(r.fecha_creacion,'dd/mm/yyyy') as fecha_creacion_c, ";
		$strQuery .= "historial_clinico.medicos.nombre||' '||historial_clinico.medicos.apellido AS medico, ";
		$strQuery .= " CONCAT(benef.nombre,' ',benef.apellido) as nombre, ";
		$strQuery .= " benef.cedula as cedula_trabajador,";
		$strQuery .= " historial_clinico.medicos.especialidad as id_especialidad, ";
		$strQuery .= " benef.tipo_beneficiario ";
		$strQuery .= "from ";
		$strQuery .= "historial_clinico.reposos as r  ";
		$strQuery .= "join historial_clinico.medicos on r.id_medico=historial_clinico.medicos.id ";
		$strQuery .= "join historial_clinico.historial_medico as hc on r.n_historial=hc.n_historial ";
		$strQuery .= "join vista_beneficiarios as benef on hc.cedula=benef.cedula ";
		$strQuery .= "join historial_clinico.especialidades as e on historial_clinico.medicos.especialidad=e.id_especialidad ";
		$strQuery .= " where r.borrado='false'";

		if ($desde != 'null' && $hasta != 'null') {
			$strQuery .= " AND r.fecha_creacion BETWEEN '$desde' AND '$hasta'";
		}
		if ($id_medico != '0' && $id_medico != 'null') {
			$strQuery .= " AND r.id_medico='$id_medico'";
		}
		if ($especialidad != '0' && $especialidad != 'null') {
			$strQuery .= " AND e.id_especialidad='$especialidad'";
		}


		//return $strQuery;

		$strQuery .= " order by id desc";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function listar_reposos_con_filtro($desde = null, $hasta = null, $n_historial = null)
	{

		$db = \Config\Database::connect();
		$strQuery = "select ";
		$strQuery .= "r.id,r.n_historial,r.id_medico,r.motivo,";
		$strQuery .= " to_char(r.fecha_desde,'dd/mm/yyyy') as fecha_desde,";
		$strQuery .= " to_char(r.fecha_hasta,'dd/mm/yyyy') as fecha_hasta, ";
		$strQuery .= " case when  r.fecha_hasta  >current_date then'ACTIVO' else 'VENCIDO' end as estatus,";
		$strQuery .= " fecha_hasta as fecha_hasta_normal,";
		$strQuery .= "historial_clinico.medicos.nombre||' '||historial_clinico.medicos.apellido AS medico, ";
		$strQuery .= " CONCAT(t.nombre,' ',t.apellido) as nombre ";
		$strQuery .= "from ";
		$strQuery .= "historial_clinico.reposos as r ";
		$strQuery .= "join historial_clinico.medicos on r.id_medico=historial_clinico.medicos.id ";
		$strQuery .= "join historial_clinico.historial_medico as hc on r.n_historial=hc.n_historial ";
		$strQuery .= "join titulares as t on hc.cedula::int=t.cedula_trabajador ";

		$strQuery .= " where r.borrado='false'";
		$strQuery .= " AND r.n_historial='$n_historial'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	public function buscar_info_reposo($n_historial = null, $id_reposo = null)
	{

		$db = \Config\Database::connect();
		$strQuery = "select ";
		$strQuery .= "r.id,r.n_historial,r.id_medico,r.motivo,";
		$strQuery .= " to_char(r.fecha_desde,'dd/mm/yyyy') as fecha_desde,";
		$strQuery .= " to_char(r.fecha_hasta,'dd/mm/yyyy') as fecha_hasta, ";
		$strQuery .= " fecha_hasta as fecha_hasta_normal,";
		$strQuery .= "historial_clinico.medicos.nombre||' '||historial_clinico.medicos.apellido AS medico ";
		$strQuery .= "from ";
		$strQuery .= "historial_clinico.reposos as r ";
		$strQuery .= "join historial_clinico.medicos on r.id_medico=historial_clinico.medicos.id ";
		$strQuery .= " where r.borrado='false'";
		$strQuery .= " AND r.n_historial='$n_historial'";
		$strQuery .= " AND r.id='$id_reposo'";

		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}






	public function Agregar_Reposo($data)
	{
		$builder = $this->dbconn('historial_clinico.reposos');
		$query = $builder->insert($data);
		return $query;
	}
	public function Actualizar_Reposo($data)
	{
		$builder = $this->dbconn('historial_clinico.reposos as r');
		$builder->where('r.id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}


	public function Reversar_reposo($data)
	{
		$builder = $this->dbconn('historial_clinico.reposos as r');
		$builder->where('r.id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}
}
