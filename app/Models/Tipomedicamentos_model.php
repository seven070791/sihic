<?php namespace App\Models;
use CodeIgniter\Model;
class Tipomedicamentos_model extends BaseModel
{
	
     public function getAll($estatus=null)
     {
	  $builder = $this->dbconn('public.tipo_medicamento as tm');
	  $builder->select
	  (
	       "tm.id
		   ,tm.categoria_id
	        ,tm.descripcion
		    ,to_char(tm.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
	        ,CASE WHEN tm.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus
		   ,categoria_inventario.descripcion as categoria"

	  );

	  $builder->join('public.categoria_inventario','tm.categoria_id=categoria_inventario.id');
	  $query = $builder->get();
	  return $query;
	  
	 	
     }

	 public function getAllActivos($id_cmbTipoMedicamento=null)
     {
		
	  $builder = $this->dbconn('public.tipo_medicamento as tm');
	  $builder->select
	  (
	       "tm.id
	       ,tm.descripcion
		   ,tm.categoria_id
		   ,to_char(tm.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
	       ,CASE WHEN tm.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus"
	  );
	  $builder->where(['tm.borrado'=>false]);
	  if($id_cmbTipoMedicamento!='null')
	  {
		$builder->where('tm.id',$id_cmbTipoMedicamento);
	  }
	  $builder->OrderBy('tm.descripcion');
	  $query = $builder->get();
	  return $query;	
     }



	 public function Agregar($data)
	 {
		$builder = $this->dbconn('public.tipo_medicamento');
		$query = $builder->insert($data);  
		return $query;
     }
   

	public function actualizar($data)
	{
		$builder = $this->dbconn(' public.tipo_medicamento');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}



     public function getAllParaSistemas($estatus=null)
     {
	  $builder = $this->dbconn('seguridad.rol as r');
	  $builder->select
	  (
	       "r.id
	       ,r.rol
	       ,CASE WHEN r.activo='t' THEN 'Activo' ELSE 'Bloqueado' END AS Estatus"
	  );
	  $query = $builder->get();
	  return $query;
     }
     
     public function getDatosRol($id=null){
	  $builder = $this->dbconn('seguridad.rol r');
	  $builder->select
	       (
		    'r.id
		    ,r.rol
		    ,r.activo'
	       );
	  $builder->where('r.id', $id);
	  $query = $builder->get();
	  return $query;
     }
}
