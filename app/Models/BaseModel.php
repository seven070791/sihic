<?php namespace App\Models;

use CodeIgniter\Model;

class BaseModel extends Model
{
	/*Metodo que conecta a una tabla*/
	public function dbconn(String $table){
		$db = \Config\Database::connect();
		$builder = $db->table($table);
		return $builder;
	}
	/*Metodo que registra en la tabla de auditoria*/
	public function recordlog($data){
		$builder = $this->dbconn('seguridad.log');
		$query = $builder->insert($data);
		return $query;
	}
}
