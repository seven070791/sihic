<?php namespace App\Models;
use CodeIgniter\Model;
class Metodo_Anticonceptivo_Model extends BaseModel
{
	
	 public function ListarMetodosAnticonceptivos_Activos()
     {
	  $builder = $this->dbconn(' historial_clinico.metodos_anticonceptibos as ma');
	  $builder->select
	  (
		" ma.id
		,ma.descripcion"
	  );
	  $builder->where(['ma.borrado'=>false]);
	  $query = $builder->get();
	  return $query;	
     }
	

	}

	