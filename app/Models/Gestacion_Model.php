<?php namespace App\Models;
use CodeIgniter\Model;
class Gestacion_Model extends BaseModel
{
	
	public function agregar($data)
	{
		
		$builder = $this->dbconn('historial_clinico.gestacion');
		$query = $builder->insert($data);  
	   return $query;
	}

	
	public function actualizar($data)
	{
		$builder = $this->dbconn('historial_clinico.gestacion');
		$builder->where('n_historial', $data['n_historial']);
		$query = $builder->update($data);
		return $query;
	   //return  $strQuery;
	}

		public function listar_gestacion($n_historial,$id_consulta)
	{
 
	   $db      = \Config\Database::connect();
	   $strQuery ="";
	   $strQuery .="SELECT";
	   $strQuery .=" gestacion.id";  
	   $strQuery .=",gestacion.id_gestacion_numeros_romanos "; 
	   $strQuery .=",nr.descripcion as descri_gestacion ";
	   $strQuery .="FROM ";
	   $strQuery .="  historial_clinico.gestacion ";	
	   $strQuery .="  JOIN historial_clinico.numeros_romanos as nr on gestacion.id_gestacion_numeros_romanos=nr.id";	
	   $strQuery .=" where gestacion.n_historial='$n_historial'";
	   $query = $db->query($strQuery);
	   $resultado=$query->getResult(); 
	   return $resultado;
	}
}

	