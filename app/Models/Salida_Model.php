<?php

namespace App\Models;

use CodeIgniter\Model;

class Salida_Model extends BaseModel
{





	public function getAll($id_medicamento = null)
	{
		$builder = $this->dbconn('public.salidas as s');
		$builder->select(
			"s.id
	       ,s.user_id
		   ,s.id_entrada
           ,s.id_medicamento
           ,s.borrado
           ,s.cantidad
           ,to_char(s.fecha_salida,'dd/mm/yyyy') as fecha_salida
	       ,CASE WHEN s.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus"
		);
		$query = $builder->where('s.id_medicamento', $id_medicamento);
		$query = $builder->get();
		return $query;
	}


	public function verificar_salidas_para_la_entrada($id_entrada)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= " s.id";
		$strQuery .= ",s.id_entrada ";
		$strQuery .= "FROM ";
		$strQuery .= "salidas as s ";
		$strQuery .= " WHERE s.id_entrada=$id_entrada";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function buscar_salidas_notas_entregas($datos_2)
	{

		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= " s.id";
		$strQuery .= ",s.user_id";
		$strQuery .= ",s.id_entrada";
		$strQuery .= ",s.id_medicamento";
		$strQuery .= ",s.borrado";
		$strQuery .= ",s.cantidad";
		$strQuery .= ",to_char(s.fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",s.fecha_salida as fecha_salida_normal";
		$strQuery .= ",CASE WHEN s.borrado='t' THEN 'Eliminado' ELSE 'Activo' END AS Estatus";
		$strQuery .= " FROM public.salidas as s ";
		$strQuery .= " WHERE s.cedula_beneficiario='$datos_2[cedula_beneficiario]'";
		$strQuery .= " AND fecha_salida=" . "'" . $datos_2['fecha_registro'] . "'";
		$strQuery .= " AND borrado ='false' ";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}





	public function getAllSalidasContraEntradas($id_entrada = null, $id_medicamento = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= " s.id";
		$strQuery .= ",s.cedula_beneficiario";
		$strQuery .= ",to_char(s.fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",s.cantidad";
		$strQuery .= ",CONCAT(b.nombre,' ', b.apellido) AS nombre_apellido ";
		$strQuery .= "FROM ";
		$strQuery .= "salidas as s ";
		$strQuery .= "LEFT JOIN ";
		$strQuery .= "(";
		$strQuery .= "SELECT ";
		$strQuery .= " cedula_trabajador::text as cedula";
		$strQuery .= ",nombre";
		$strQuery .= ",apellido ";
		$strQuery .= "FROM titulares ";
		$strQuery .= "WHERE NOT borrado ";
		$strQuery .= "UNION ";
		$strQuery .= "SELECT ";
		$strQuery .= " cedula";
		$strQuery .= ",nombre";
		$strQuery .= ",apellido ";
		$strQuery .= "FROM familiares ";
		$strQuery .= "WHERE NOT borrado ";
		$strQuery .= "UNION ";
		$strQuery .= "SELECT ";
		$strQuery .= " cedula";
		$strQuery .= ",nombre";
		$strQuery .= ",apellido ";
		$strQuery .= "FROM cortesia ";
		$strQuery .= "WHERE NOT borrado ";
		$strQuery .= ")";
		$strQuery .= " as b ";
		$strQuery .= "ON s.cedula_beneficiario=b.cedula ";
		$strQuery .= " WHERE s.borrado='false'";
		$strQuery .= "and s.id_entrada=$id_entrada and s.id_medicamento=$id_medicamento";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function Agregar($data)
	{
		//return('en el modelo ');
		$builder = $this->dbconn('public.salidas');
		$query = $builder->insert($data);
		return $query;
	}


	// ***********ESTE METODO GENERA REPORTE GENERAL DE SALIDAS***********

	public function getAllSalidas_MedicamentosPDF($id_categoria = 0, $cronicos = null, $desde = null, $hasta = null, $sexo = null, $beneficiario = null, $medico = 0)
	{

		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",case when medicamentos.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
		$strQuery .= ",usuarios.usuario";
		$strQuery .= ",titulares.nombre||' '||titulares.apellido AS Beneficiario";
		$strQuery .= ",titulares.sexo";
		$strQuery .= ",historial_clinico.medicos.nombre||' '||historial_clinico.medicos.apellido AS medico";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='T' THEN' TITULAR' END tipo_beneficiario ";
		$strQuery .= ",categoria_inventario.descripcion as categoria_inventario ";
		$strQuery .= ",titulares.cedula_trabajador AS cedula_titular ";
		$strQuery .= ",titulares.nombre||' '||titulares.apellido AS titular ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join titulares on salidas.cedula_beneficiario=titulares.cedula_trabajador::text ";
		$strQuery .= "join tipo_medicamento on medicamentos.id_tipo_medicamento=tipo_medicamento.id ";
		$strQuery .= "join categoria_inventario on tipo_medicamento.categoria_id=categoria_inventario.id ";
		$strQuery .= "join historial_clinico.medicos on salidas.id_medico=historial_clinico.medicos.id ";
		$strQuery .= " WHERE salidas.borrado=false";
		$strQuery .= " and  titulares.borrado<>'true'";
		$strWhere = "";
		if ($id_categoria != 0) {
			if (trim($strWhere) == "") {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			} else {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			}
		}

		if ($cronicos != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND med_cronico='" . $cronicos . "'";
			} else {
				$strWhere .= " AND med_cronico='" . $cronicos . "'";
			}
		}
		if ($desde != 'null' and $hasta != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND fecha_salida BETWEEN '$desde'AND '$hasta'";
			} else {
				$strWhere .= " AND fecha_salida BETWEEN '$desde'AND '$hasta'";
			}
		}

		if ($sexo != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND titulares.sexo='" . $sexo . "'";
			} else {
				$strWhere .= " AND titulares.sexo='" . $sexo . "'";
			}
		}

		if ($beneficiario != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " and tipo_beneficiario='" . $beneficiario . "'";
			} else {
				$strWhere .= " AND tipo_beneficiario='" . $beneficiario . "'";
			}
		}

		if ($medico != '0' && $medico != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " and id_medico='" . $medico . "'";
			} else {
				$strWhere .= " AND id_medico='" . $medico . "'";
			}
		}


		$strQuery = $strQuery . $strWhere;

		$strQuery .= " UNION ";
		$strQuery .= "SELECT ";
		$strQuery .= "salidas.id";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",case when medicamentos.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
		$strQuery .= ",usuarios.usuario";
		$strQuery .= ",cortesia.nombre||' '||cortesia.apellido AS Beneficiario ";
		$strQuery .= ",cortesia.sexo::int ";
		$strQuery .= ",historial_clinico.medicos.nombre||' '||historial_clinico.medicos.apellido AS medico";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='C' THEN 'CORTESIA' END tipo_beneficiario ";
		$strQuery .= ",categoria_inventario.descripcion as categoria_inventario ";
		$strQuery .= ",cortesia.cedula_trabajador AS cedula_titular ";
		$strQuery .= ",titulares.nombre||' '||titulares.apellido AS titular ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join cortesia on salidas.cedula_beneficiario=cortesia.cedula ";
		$strQuery .= "join tipo_medicamento on medicamentos.id_tipo_medicamento=tipo_medicamento.id ";
		$strQuery .= "join categoria_inventario on tipo_medicamento.categoria_id=categoria_inventario.id ";
		$strQuery .= "join historial_clinico.medicos on salidas.id_medico=historial_clinico.medicos.id ";
		$strQuery .= "join titulares on titulares.cedula_trabajador=cortesia.cedula_trabajador ";

		$strQuery .= " WHERE salidas.borrado=false";
		$strQuery .= " and  cortesia.borrado<>'true'";


		$strWhere = "";
		if ($id_categoria != 0) {
			if (trim($strWhere) == "") {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			} else {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			}
		}
		if ($cronicos != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND med_cronico='" . $cronicos . "'";
			} else {
				$strWhere .= " AND med_cronico='" . $cronicos . "'";
			}
		}

		if ($desde != 'null' and $hasta != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND fecha_salida BETWEEN '$desde'AND '$hasta'";
			} else {
				$strWhere .= " AND fecha_salida BETWEEN '$desde'AND '$hasta'";
			}
		}


		if ($sexo != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND cortesia.sexo='" . $sexo . "'";
			} else {
				$strWhere .= " AND cortesia.sexo='" . $sexo . "'";
			}
		}

		if ($beneficiario != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND tipo_beneficiario='" . $beneficiario . "'";
			} else {
				$strWhere .= " AND tipo_beneficiario='" . $beneficiario . "'";
			}
		}
		if ($medico != '0' && $medico != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND id_medico='" . $medico . "'";
			} else {
				$strWhere .= " AND id_medico='" . $medico . "'";
			}
		}

		$strQuery = $strQuery . $strWhere;



		$strQuery .= " UNION ";
		$strQuery .= "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",case when medicamentos.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
		$strQuery .= ",usuarios.usuario";
		$strQuery .= ",familiares.nombre||' '||familiares.apellido AS Beneficiario ";
		$strQuery .= ",familiares.sexo";
		$strQuery .= ",historial_clinico.medicos.nombre||' '||historial_clinico.medicos.apellido AS medico";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='F' THEN 'FAMILIAR' END tipo_beneficiario ";
		$strQuery .= ",categoria_inventario.descripcion as categoria_inventario ";
		$strQuery .= ",familiares.cedula_trabajador AS cedula_titular ";
		$strQuery .= ",titulares.nombre||' '||titulares.apellido AS titular ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join familiares on salidas.cedula_beneficiario=familiares.cedula ";
		$strQuery .= "join tipo_medicamento on medicamentos.id_tipo_medicamento=tipo_medicamento.id ";
		$strQuery .= "join categoria_inventario on tipo_medicamento.categoria_id=categoria_inventario.id ";
		$strQuery .= "join historial_clinico.medicos on salidas.id_medico=historial_clinico.medicos.id ";
		$strQuery .= "join titulares on titulares.cedula_trabajador=familiares.cedula_trabajador ";
		$strQuery .= " WHERE salidas.borrado=false";
		$strQuery .= " and  familiares.borrado<>'true'";
		$strWhere = "";
		if ($id_categoria != 0) {
			if (trim($strWhere) == "") {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			} else {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			}
		}
		if ($cronicos != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND med_cronico='" . $cronicos . "'";
			} else {
				$strWhere .= " AND med_cronico='" . $cronicos . "'";
			}
		}
		if ($desde != 'null' and $hasta != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND fecha_salida BETWEEN '$desde'AND '$hasta'";
			} else {
				$strWhere .= " AND fecha_salida BETWEEN '$desde'AND '$hasta'";
			}
		}


		if ($sexo != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND familiares.sexo='" . $sexo . "'";
			} else {
				$strWhere .= " AND familiares.sexo='" . $sexo . "'";
			}
		}

		if ($beneficiario != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND tipo_beneficiario='" . $beneficiario . "'";
			} else {
				$strWhere .= " AND tipo_beneficiario='" . $beneficiario . "'";
			}
		}

		if ($medico != '0' && $medico != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " AND id_medico='" . $medico . "'";
			} else {
				$strWhere .= " AND id_medico='" . $medico . "'";
			}
		}


		$strQuery = $strQuery . $strWhere;
		//die($strQuery);
		//return $strQuery;



		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function getAllSalidas_MedicamentosEntesPDF($id_categoria = 0, $cronicos = null, $desde = null, $hasta = null, $sexo = null, $beneficiario = null, $medico = 0, $entes_adscritos = 0)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= " cortesia.cedula_trabajador,t.nombre||''||t.apellido as nombret,usuario,salidas.id";
		$strQuery .= ",cortesia.adscrito";
		$strQuery .= ",cortesia.id as id_cortesia";
		$strQuery .= ",cortesia_adscrito.id_adscrito";
		$strQuery .= ",ente_adscrito.descripcion";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",case when medicamentos.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
		$strQuery .= ",usuarios.usuario as usuario";
		$strQuery .= ",cortesia.nombre||' '||cortesia.apellido AS Beneficiario ";
		$strQuery .= ",cortesia.sexo::int ";
		$strQuery .= ",historial_clinico.medicos.nombre||' '||historial_clinico.medicos.apellido AS medico";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='C' THEN 'CORTESIA' END tipo_beneficiario";
		$strQuery .= ",categoria_inventario.descripcion as categoria_inventario ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join cortesia on salidas.cedula_beneficiario=cortesia.cedula ";
		$strQuery .= "join tipo_medicamento on medicamentos.id_tipo_medicamento=tipo_medicamento.id ";
		$strQuery .= "join categoria_inventario on tipo_medicamento.categoria_id=categoria_inventario.id ";
		$strQuery .= "join historial_clinico.medicos on salidas.id_medico=historial_clinico.medicos.id ";
		$strQuery .= "left join cortesia_adscrito on cortesia.id=cortesia_adscrito.id_cortesia ";
		$strQuery .= "left join ente_adscrito on cortesia_adscrito.id_adscrito=ente_adscrito.id ";
		$strQuery .= "join titulares as t on cortesia.cedula_trabajador=t.cedula_trabajador ";
		$strWhere = " where salidas.tipo_beneficiario='C' ";



		if ($entes_adscritos > 2) {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE ente_adscrito.id='" . $entes_adscritos . "'";
			} else {
				$strWhere .= " AND ente_adscrito.id='" . $entes_adscritos . "'";
			}
		}

		if ($entes_adscritos == 2) {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE cortesia.adscrito='false'";
			} else {
				$strWhere .= " AND cortesia.adscrito='false'";
			}
		}



		if ($id_categoria != 0) {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE categoria_inventario.id=$id_categoria";
			} else {
				$strWhere .= " AND categoria_inventario.id=$id_categoria";
			}
		}
		if ($cronicos != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE med_cronico='" . $cronicos . "'";
			} else {
				$strWhere .= " AND med_cronico='" . $cronicos . "'";
			}
		}

		if ($desde != 'null' and $hasta != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE fecha_salida BETWEEN '$desde'AND '$hasta'";
			} else {
				$strWhere .= " AND fecha_salida BETWEEN '$desde'AND '$hasta'";
			}
		}


		if ($sexo != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE cortesia.sexo='" . $sexo . "'";
			} else {
				$strWhere .= " AND cortesia.sexo='" . $sexo . "'";
			}
		}

		if ($beneficiario != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE salidas.tipo_beneficiario='" . $beneficiario . "'";
			} else {
				$strWhere .= " AND salidas.tipo_beneficiario='" . $beneficiario . "'";
			}
		}
		if ($medico != 0) {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE salidas.id_medico='" . $medico . "'";
			} else {
				$strWhere .= " AND salidas.id_medico='" . $medico . "'";
			}
		}

		$strQuery = $strQuery . $strWhere;
		// return $strQuery; 

		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	public function getAllSalidas($id_medicamento = null)
	{
		$db      = \Config\Database::connect();

		if ($id_medicamento == null) {
			$filtro = '';
		} else {
			$filtro = " where id_medicamento=$id_medicamento ";
		}

		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		//$strQuery.=",fecha_salida";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre ";
		$strQuery .= ",titulares.nombre||' '||titulares.apellido AS Beneficiario";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='T' THEN' TITULAR' END tipo_beneficiario ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join titulares on salidas.cedula_beneficiario=titulares.cedula_trabajador::text ";
		$strQuery .= $filtro;
		$strQuery .= "UNION ";
		$strQuery .= "SELECT ";
		$strQuery .= "salidas.id";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre";
		$strQuery .= ",cortesia.nombre||' '||cortesia.apellido AS Beneficiario ";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='C' THEN 'CORTESIA' END tipo_beneficiario ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join cortesia on salidas.cedula_beneficiario=cortesia.cedula ";
		$strQuery .= $filtro;
		$strQuery .= "UNION ";
		$strQuery .= "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre";
		$strQuery .= ",familiares.nombre||' '||familiares.apellido AS Beneficiario ";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='F' THEN 'FAMILIAR' END tipo_beneficiario ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join familiares on salidas.cedula_beneficiario=familiares.cedula";
		$strQuery .= $filtro;
		$strQuery .= " ORDER BY fecha_salida ";
		// return $strQuery; 
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	// ***********ESTE METODO GENERA REPORTE INDIVIDUAL DE SALIDAS***********
	public function GenerarReportesSalidasPorFecha(string $desde = null, string $hasta = null, $id_medicamento = null)
	{

		$db      = \Config\Database::connect();
		$filtro = " where id_medicamento=$id_medicamento";
		if ($desde != 'null' and $hasta != 'null') {
			$filtro = $filtro . " AND fecha_salida BETWEEN '$desde'AND '$hasta'";
		}

		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		//$strQuery.=",fecha_salida";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre ";
		$strQuery .= ",titulares.nombre||' '||titulares.apellido AS Beneficiario";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='T' THEN' TITULAR' END tipo_beneficiario ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join titulares on salidas.cedula_beneficiario=titulares.cedula_trabajador::text ";
		$strQuery .= $filtro;
		$strQuery .= " UNION ";
		$strQuery .= "SELECT ";
		$strQuery .= "salidas.id";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre";
		$strQuery .= ",cortesia.nombre||' '||cortesia.apellido AS Beneficiario ";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='C' THEN 'CORTESIA' END tipo_beneficiario ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join cortesia on salidas.cedula_beneficiario=cortesia.cedula ";
		$strQuery .= $filtro;
		$strQuery .= " UNION ";
		$strQuery .= "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre";
		$strQuery .= ",familiares.nombre||' '||familiares.apellido AS Beneficiario ";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='F' THEN 'FAMILIAR' END tipo_beneficiario ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join familiares on salidas.cedula_beneficiario=familiares.cedula";
		$strQuery .= $filtro;
		$strQuery .= " ORDER BY fecha_salida ";

		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery;
	}


	//  *****Este Metodo busca los meficamentos entregador a un titular ****
	public function getAll_infomedicamento_titulares($cedula_trabajador = null, $desde = null, $hasta = null, $id_medico = '0')
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",cantidad";
		$strQuery .= " ,CONCAT(m.nombre,' ', m.apellido) as nombre_medico";
		$strQuery .= ",control.descripcion as control ";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",case when medicamentos.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
		$strQuery .= ",usuarios.nombre ";
		$strQuery .= ",titulares.nombre||' '||titulares.apellido AS Beneficiario";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='T' THEN' TITULAR' END tipo_beneficiario ";
		$strQuery .= "FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join control on salidas.control=control.id ";
		$strQuery .= "join titulares on salidas.cedula_beneficiario=titulares.cedula_trabajador::text ";
		$strQuery .= " JOIN historial_clinico.medicos as m on m.id=salidas.id_medico";
		$strQuery .= " WHERE";
		$strQuery .= " salidas.cedula_beneficiario='$cedula_trabajador'";
		$strQuery .= " AND salidas.borrado='false'";

		//$strQuery .=" order by fecha_salida desc";

		//*****CUANDO DESDE Y HASTA BIENEN VACIOS Y EL MEDICO NO***
		if ($desde == 'null' && $hasta == 'null' && $id_medico != 0) {
			$strQuery .= " AND salidas.id_medico=$id_medico";
		}
		//*****CUANDO DESDE Y HASTA BIENEN CONDATOS Y EL MEDICO VACIO***
		else if ($desde != 'null' && $hasta != 'null' && $id_medico == '0') {
			$strQuery .= " AND salidas.fecha_salida BETWEEN '$desde' AND '$hasta'";
		}
		//*****CUANDO TODOS BIENEN CON DATOS***
		else if ($desde != 'null' && $hasta != 'null' && $id_medico != '0') {
			$strQuery .= " AND (salidas.fecha_salida BETWEEN '$desde' AND '$hasta') AND salidas.id_medico='$id_medico'";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery;
	}



	//  *****Este Metodo busca los meficamentos entregador a un familiar ****
	public function getAll_infomedicamento_familiares($cedula_beneficiario = null, $desde = null, $hasta = null, $id_medico = '0')
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",cantidad";
		$strQuery .= " ,CONCAT(m.nombre,' ', m.apellido) as nombre_medico";
		$strQuery .= ",control.descripcion as control ";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",case when medicamentos.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
		$strQuery .= " FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join control on salidas.control=control.id ";
		$strQuery .= " JOIN historial_clinico.medicos as m on m.id=salidas.id_medico";
		$strQuery .= " WHERE";
		$strQuery .= " salidas.cedula_beneficiario='$cedula_beneficiario'";
		$strQuery .= " AND salidas.borrado='false'";
		//*****CUANDO DESDE Y HASTA BIENEN VACIOS Y EL MEDICO NO***
		if ($desde == 'null' && $hasta == 'null' && $id_medico != 0) {
			$strQuery .= " AND salidas.id_medico=$id_medico";
		}
		// //*****CUANDO DESDE Y HASTA BIENEN CONDATOS Y EL MEDICO VACIO***
		else if ($desde != 'null' && $hasta != 'null' && $id_medico == '0') {
			$strQuery .= " AND salidas.fecha_salida BETWEEN '$desde' AND '$hasta'";
		}
		// //*****CUANDO TODOS BIENEN CON DATOS***
		else if ($desde != 'null' && $hasta != 'null' && $id_medico != '0') {
			$strQuery .= " AND (salidas.fecha_salida BETWEEN '$desde' AND '$hasta') AND salidas.id_medico='$id_medico'";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery;
	}

	//  *****Este Metodo busca los meficamentos entregador a un familiar ****
	public function getAll_infomedicamento_cortesia($cedula_beneficiario = null, $desde = null, $hasta = null, $id_medico = '0')
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",cantidad";
		$strQuery .= " ,CONCAT(m.nombre,' ', m.apellido) as nombre_medico";
		$strQuery .= ",control.descripcion as control ";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",case when medicamentos.med_cronico='t' then 'SI'  else 'NO' end as med_cronico";
		$strQuery .= " FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join control on salidas.control=control.id ";
		$strQuery .= " JOIN historial_clinico.medicos as m on m.id=salidas.id_medico";
		$strQuery .= " WHERE";
		$strQuery .= " salidas.cedula_beneficiario='$cedula_beneficiario'";
		$strQuery .= " AND salidas.borrado='false'";
		//$strQuery .="order by fecha_salida desc";
		//*****CUANDO DESDE Y HASTA BIENEN VACIOS Y EL MEDICO NO***
		if ($desde == 'null' && $hasta == 'null' && $id_medico != 0) {
			$strQuery .= " AND salidas.id_medico=$id_medico";
		}
		//*****CUANDO DESDE Y HASTA BIENEN CONDATOS Y EL MEDICO VACIO***
		else if ($desde != 'null' && $hasta != 'null' && $id_medico == '0') {
			$strQuery .= " AND salidas.fecha_salida BETWEEN '$desde' AND '$hasta'";
		}
		//*****CUANDO TODOS BIENEN CON DATOS***
		else if ($desde != 'null' && $hasta != 'null' && $id_medico != '0') {
			$strQuery .= " AND (salidas.fecha_salida BETWEEN '$desde' AND '$hasta') AND salidas.id_medico='$id_medico'";
		}
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery;
	}



	public function listar_salidas_medicos($cedula_beneficiario)
	{
		$db = \Config\Database::connect();
		$strQuery = "SELECT DISTINCT (id_medico)as id_medico";
		$strQuery .= " ,CONCAT(m.nombre,' ', m.apellido) as nombre";
		$strQuery .= " FROM public.salidas";
		$strQuery .= " JOIN historial_clinico.medicos as m on m.id=salidas.id_medico";
		$strQuery .= " WHERE";
		$strQuery .= " salidas.cedula_beneficiario='$cedula_beneficiario'";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery;

	}

	//  *****Este Metodo busca los medicamentos entregador a un titular para su nota de entrega ****
	public function nota_Entrega_medicamentos_titulares($cedula_beneficiario = null, $fechanotas)
	{


		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",salidas.cedula_beneficiario";
		$strQuery .= ",salidas.user_id";
		$strQuery .= ",cantidad";
		$strQuery .= ",control.descripcion as control ";
		$strQuery .= ",presentacion.descripcion as presentacion ";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= ",usuarios.nombre||' '||usuarios.apellido AS nombre";
		$strQuery .= ",titulares.nombre||' '||titulares.apellido AS Beneficiario";
		$strQuery .= ",CASE WHEN salidas.tipo_beneficiario='T' THEN' TITULAR' END tipo_beneficiario ";
		$strQuery .= " FROM ";
		$strQuery .= " public.salidas ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join presentacion on medicamentos.id_presentacion=presentacion.id";
		$strQuery .= " join usuarios on salidas.user_id=usuarios.id";
		$strQuery .= " join control on salidas.control=control.id";
		$strQuery .= " join titulares on salidas.cedula_beneficiario=titulares.cedula_trabajador::text";
		$strQuery .= " WHERE ";
		$strQuery .= " salidas.cedula_beneficiario='$cedula_beneficiario'";
		$strQuery .= " AND salidas.borrado='false'";
		$strQuery .= " AND";
		$strQuery .= " salidas.fecha_salida='$fechanotas'";
		$strQuery .= "order by fecha_salida desc";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	//  *****Este Metodo busca los meficamentos entregador a un familiar para su nota de entrega ****
	public function nota_Entrega_medicamentos_familiares($cedula_beneficiario = null, $fechanotas)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",cantidad";
		$strQuery .= ",salidas.user_id";
		$strQuery .= ",control.descripcion as control ";
		//$strQuery.=",fecha_salida";
		$strQuery .= ",presentacion.descripcion as presentacion ";
		$strQuery .= ",usuarios.nombre||' '||usuarios.apellido AS nombre";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= " FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join control on salidas.control=control.id ";
		$strQuery .= "join presentacion on medicamentos.id_presentacion=presentacion.id";
		$strQuery .= " WHERE";
		$strQuery .= " salidas.cedula_beneficiario='$cedula_beneficiario'";
		$strQuery .= " AND salidas.borrado='false'";
		$strQuery .= " AND";

		$strQuery .= " salidas.fecha_salida='$fechanotas'";
		$strQuery .= "order by fecha_salida desc";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}


	//  *****Este Metodo busca los meficamentos entregador a un familiar para su nota de entrega ****
	public function nota_Entrega_medicamentos_cortesia($cedula_beneficiario = null, $fechanotas)
	{
		$db      = \Config\Database::connect();
		$strQuery = "SELECT ";
		$strQuery .= "salidas.id ";
		$strQuery .= ",cantidad";
		$strQuery .= ",salidas.user_id";
		$strQuery .= ",control.descripcion as control ";
		//$strQuery.=",fecha_salida";
		$strQuery .= ",presentacion.descripcion as presentacion ";
		$strQuery .= ",usuarios.nombre||' '||usuarios.apellido AS nombre";
		$strQuery .= ",to_char(fecha_salida,'dd/mm/yyyy') as fecha_salida";
		$strQuery .= ",id_medicamento";
		$strQuery .= ",medicamentos.descripcion";
		$strQuery .= " FROM ";
		$strQuery .= "public.salidas ";
		$strQuery .= "join usuarios on salidas.user_id=usuarios.id ";
		$strQuery .= "join medicamentos on salidas.id_medicamento=medicamentos.id ";
		$strQuery .= "join control on salidas.control=control.id ";
		$strQuery .= "join presentacion on medicamentos.id_presentacion=presentacion.id";
		$strQuery .= " WHERE";
		$strQuery .= " salidas.cedula_beneficiario='$cedula_beneficiario'";
		$strQuery .= " AND salidas.borrado='false'";
		$strQuery .= " AND";

		$strQuery .= " salidas.fecha_salida='$fechanotas'";
		$strQuery .= "order by fecha_salida desc";

		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return $strQuery;
	}




	public function reverso_salida($data)
	{
		$builder = $this->dbconn('public.salidas');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);

		return $query;
	}

	public function borrar_salidas_nota_entrega($array_salidas, $contDetalles)
	{
		$builder = $this->dbconn('public.salidas as s');
		$contDetallesPost = 0;
		foreach ($array_salidas as $detalle) {
			$arrDetalle = [];
			$arrDetalle['borrado'] = $array_salidas[$contDetallesPost]['borrado'];
			$builder->where('s.id', $detalle['salida_id']);
			$query = $builder->update($arrDetalle);
			$contDetallesPost++;
		}
		if ($query === false) {
			// Aquí puedes manejar el error, por ejemplo, mostrando un mensaje de error
			return false;
		}
		return true;
	}

	



}
