<?php

namespace App\Models;

use CodeIgniter\Model;

class Notas_Entregas_Model extends BaseModel
{
	public function agregar_nota_entrega($data)
	{

		$builder = $this->dbconn('historial_clinico.notas_entregas');
		$query = $builder->insert($data);
		return $query;
	}

	public function listar_notas_entregas($desde = null, $hasta = null)
	{


		$db = \Config\Database::connect();
		$strQuery = "select ";
		$strQuery .= "n.id,n.id_autorizador,n.n_historial,n.usuario,n.cedula,n.fecha_registro,n.fecha_creacion,n.fecha";
		$strQuery .= ",CASE";
		$strQuery .= " WHEN f.nombre IS NULL AND c.nombre IS NULL THEN t.nombre";
		$strQuery .= " WHEN t.nombre IS NULL AND c.nombre IS NULL THEN f.nombre";
		$strQuery .= " WHEN t.nombre IS NULL AND f.nombre IS NULL THEN c.nombre";
		$strQuery .= " END AS nombre";
		$strQuery .= ",CASE";
		$strQuery .= " WHEN f.cedula IS NULL AND c.cedula IS NULL THEN t.cedula::text";
		$strQuery .= " WHEN t.cedula IS NULL AND c.cedula IS NULL THEN f.cedula";
		$strQuery .= " WHEN t.cedula IS NULL AND f.cedula IS NULL THEN c.cedula";
		$strQuery .= " END AS cedula";
		$strQuery .= ",CASE";
		$strQuery .= " WHEN f.nombre IS NULL AND c.nombre IS NULL THEN 'T'";
		$strQuery .= " WHEN t.nombre IS NULL AND c.nombre IS NULL THEN 'F'";
		$strQuery .= " WHEN t.nombre IS NULL AND f.nombre IS NULL THEN 'C'";
		$strQuery .= " END AS tipobeneficiario ";
		$strQuery .= "from";
		$strQuery .= "(";
		$strQuery .= "SELECT ";
		$strQuery .= "ne.id";
		$strQuery .= ",ne.id_autorizador,ne.n_historial";
		$strQuery .= ",ne.borrado";
		$strQuery .= ",ne.usuario";
		$strQuery .= ",hc.cedula as cedula";
		$strQuery .= ",to_char(ne.fecha_registro,'dd-mm-yyyy') as fecha_registro,";
		$strQuery .= "to_char(ne.fecha_creacion,'dd-mm-yyyy') as fecha_creacion";
		$strQuery .= ",ne.fecha_registro as fecha ";
		$strQuery .= "FROM historial_clinico.notas_entregas ne";
		$strQuery .= " join historial_clinico.historial_medico as hc on ne.n_historial=hc.n_historial ";
		$strQuery .= ") as n";
		$strQuery .= " LEFT JOIN";
		$strQuery .= "(SELECT cedula_trabajador as cedula,CONCAT(nombre,' ',apellido) as nombre FROM titulares WHERE NOT borrado) as t ON n.cedula=t.cedula::text";
		$strQuery .= " LEFT JOIN";
		$strQuery .= "(SELECT cedula,CONCAT(nombre,' ',apellido) as nombre FROM familiares WHERE NOT borrado) as f ON n.cedula=f.cedula";
		$strQuery .= " LEFT JOIN";
		$strQuery .= "(SELECT cedula,CONCAT(nombre,' ',apellido) as nombre  FROM cortesia WHERE NOT borrado) as c ON n.cedula=c.cedula";
		$strQuery .= " where n.borrado='false'";
		if ($desde != 'null' && $hasta != 'null') {
			$strQuery .= " AND n.fecha BETWEEN '$desde' AND '$hasta'";
		}
		$strQuery .= " order by n.id desc";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}




	public function borrar_nota_entrega($data)
	{

		$builder = $this->dbconn('historial_clinico.notas_entregas');
		$builder->where('id', $data['id']);
		$query = $builder->update($data);
		return $query;
	}




	public function buscar_id_notas_entregas($n_historial = null, $fecha_registro = null)
	{

		$db = \Config\Database::connect();
		$strQuery = "select ";
		$strQuery .= "n.id,n.fecha_registro ";
		$strQuery .= "FROM historial_clinico.notas_entregas n";
		$strQuery .= " WHERE n.n_historial='$n_historial'";
		$strQuery .= " AND n.fecha_registro='$fecha_registro' ";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
}
