<?php
namespace App\Models;
use CodeIgniter\Model;
class UsuarioModel extends Model
{
  
    protected $table ='usuarios';
    protected $table2 ='grupo_usuario';
    protected $primaryKey='id';


    public function buscarUsuario($usuario){
        $db= db_connect();
        $builder=$db->table($this->table)
        ->select(['usuarios.id','usuarios.cedula','usuarios.nombre','usuarios.apellido','usuarios.usuario','usuarios.password','usuarios.borrado','tipousuario','grupo_usuario.nivel_usuario'])
        ->join('grupo_usuario','grupo_usuario.id=usuarios.tipousuario')
        ->where('usuario',$usuario)
        ->where(['usuarios.borrado'=>false])
        ->where(['grupo_usuario.borrado'=>false]);

    $resultado=$builder->get();
    // var_dump($resultado->getResult());
    // die();
    return $resultado->getResult() ? $resultado->getResult()[0] : false; 
    
    }



}

