<?php

namespace App\Models;

use CodeIgniter\Config\View;
use CodeIgniter\Model;
use PhpParser\Node\Stmt\Else_;

class Morbilidad_Model extends BaseModel
{


	public function listar_mobilidad($desde = null, $hasta = null, $medico = 0, $especialidad = 0, $sexo = null, $beneficiario = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= "  CASE WHEN r.id_reposo IS NULL THEN 'NO' ELSE 'SI' END AS reposo";
		$strQuery .= " ,case when c.consulta='true' then 'CONS' ELSE 'CITA'end AS Control,";
		$strQuery .= " hm.cedula,CONCAT(b.nombre,' ', b.apellido) AS nombre,b.edad,b.parentesco,";
		$strQuery .= " c.descripcion as especialidad,c.id_especialidad ,";
		$strQuery .= " CASE WHEN b.sexo=1 THEN 'M' WHEN b.sexo=2 then 'F' END AS sexo ,";
		$strQuery .= " Case when TRIM(c.tipo_beneficiario)='T' THEN 'T' WHEN TRIM(c.tipo_beneficiario)='F' THEN 'F' ELSE 'C' END as beneficiario ,";
		$strQuery .= " CASE WHEN b.edad>18 then 'ADULTO' Else 'NINO' END AS Estatus,b.ubicacion_administrativa as departamento,";
		$strQuery .= " c.motivo_consulta ,c.diagnostico,c.fecha_asistencia ,c.fecha_asistencia_normal,c.nombre_medico ,c.id_medico ";
		$strQuery .= " FROM ";
		$strQuery .= " ( ";
		$strQuery .= " SELECT hc.n_historial as historia,hc.id as id_cita_consulta ,hc.id_medico ,";
		$strQuery .= " CONCAT(med.nombre,' ', med.apellido) AS nombre_medico,hc.n_historial,hc.tipo_beneficiario, ";
		$strQuery .= " hc.consulta,hc.borrado, ";
		$strQuery .= " CASE WHEN hc.motivo_consulta IS NULL THEN 'SIN INFORMACION' ELSE hc.motivo_consulta END AS motivo_consulta ,";
		$strQuery .= " to_char(hc.fecha_creacion,'dd-mm-yyyy') as fecha_creacion ,to_char(hc.fecha_asistencia,'dd-mm-yyyy') as fecha_asistencia ,";
		$strQuery .= " hc.fecha_asistencia as fecha_asistencia_normal ,e.descripcion,e.id_especialidad as id_especialidad, ";
		$strQuery .= " CASE WHEN efa.descripcion IS NULL THEN 'SIN DIAGN\u00d3STICO' ELSE efa.descripcion END AS diagnostico";
		$strQuery .= " FROM historial_clinico.consultas as hc ";
		$strQuery .= " JOIN historial_clinico.enfermedad_actual as efa on hc.id=efa.id_consulta";
		$strQuery .= " Join historial_clinico.medicos as med on hc.id_medico=med.id ";
		$strQuery .= "join historial_clinico.especialidades as e on med.especialidad=e.id_especialidad";
		$strQuery .= ") as c";
		$strQuery .= " LEFT JOIN ";
		$strQuery .= " ( ";
		$strQuery .= " select cedula,n_historial,id as id_historial_medico,borrado";
		$strQuery .= " from historial_clinico.historial_medico ";
		$strQuery .= " ) as hm ON c.n_historial=hm.n_historial";
		$strQuery .= " LEFT JOIN vista_beneficiarios_con_ubicacion as b ON hm.cedula=b.cedula";
		$strQuery .= " LEFT JOIN";
		$strQuery .= " (";
		$strQuery .= " select id as id_reposo,n_historial, fecha_hasta ";
		$strQuery .= " from historial_clinico.reposos";
		$strQuery .= " where fecha_hasta>= CURRENT_DATE";
		$strQuery .= " )as r ON hm.n_historial=r.n_historial";
		$strWhere = "";
		$strWhere .= " WHERE  c.n_historial=hm.n_historial AND hm.cedula=b.cedula";
		if ($especialidad != '0') {

			$strWhere .= " AND id_especialidad='" . $especialidad . "'";
		}
		if ($medico != '0') {

			$strWhere .= " AND c.id_medico='" . $medico . "'";
		}

		if ($desde != 'null' and $hasta != 'null') {

			$strWhere .= " AND c.fecha_asistencia_normal BETWEEN '$desde'AND '$hasta'";
		}
		if ($sexo != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE sexo='" . $sexo . "'";
			} else {
				$strWhere .= " AND sexo='" . $sexo . "'";
			}
		}

		if ($beneficiario != 'null') {
			if (trim($strWhere) == "") {
				$strWhere .= " WHERE TRIM(tipo_beneficiario)='" . $beneficiario . "'";
			} else {
				$strWhere .= " AND TRIM(tipo_beneficiario)='" . $beneficiario . "'";
			}
		}

		$strQuery = $strQuery . $strWhere;
		$strQuery .= " order by c.fecha_asistencia_normal desc ";
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function listar_mobilidad_entes($desde = null, $hasta = null, $medico = 0, $especialidad = 0, $sexo = null, $beneficiario = null, $entes_adscritos = 0)
	{
		
		$db      = \Config\Database::connect();

		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= " case when c.consulta='true' then 'CONS' ELSE 'CITA'end AS Control,";
		$strQuery .= " hm.cedula,CONCAT(b.nombre,' ', b.apellido) AS nombre,b.edad,descripcion as especialidad,";
		$strQuery .= " id_especialidad,CASE WHEN b.sexo=1 THEN 'M' WHEN b.sexo=2 then 'F' END AS sexo ,";
		$strQuery .= " Case when TRIM(c.tipo_beneficiario)='T' THEN 'T' WHEN TRIM(c.tipo_beneficiario)='F' THEN 'F' ELSE 'C' END as beneficiario ,";
		$strQuery .= " CASE WHEN b.edad>18 then 'ADULTO' Else 'NINO' END AS Estatus ,b.ubicacion_administrativa AS departamento,";
		$strQuery .= " c.motivo_consulta ,c.diagnostico,c.fecha_asistencia ,c.fecha_asistencia_normal,c.nombre_medico ,c.id_medico ,";
		$strQuery .= " b.id_adscrito ,'CORTESIA' as parentesco ,";
		$strQuery .= " case when r.fecha_hasta >current_date then 'SI' else 'NO' end as reposo ";
		$strQuery .= " FROM ";
		$strQuery .= " (";
		$strQuery .= " SELECT hc.n_historial as historia,hc.id as id_cita_consulta ,hc.id_medico ,";
		$strQuery .= " CONCAT(med.nombre,' ', med.apellido) AS nombre_medico,hc.n_historial,hc.tipo_beneficiario,";
		$strQuery .= " hc.consulta,hc.borrado,";
		$strQuery .= " CASE WHEN hc.motivo_consulta IS NULL THEN 'SIN INFORMACION' ELSE hc.motivo_consulta END AS motivo_consulta ,";
		$strQuery .= " to_char(hc.fecha_creacion,'dd-mm-yyyy') as fecha_creacion ,";
		$strQuery .= " to_char(hc.fecha_asistencia,'dd-mm-yyyy') as fecha_asistencia ,hc.fecha_asistencia as fecha_asistencia_normal,e.descripcion,";
		$strQuery .= " e.id_especialidad as id_especialidad,";
		$strQuery .= " CASE WHEN efa.descripcion IS NULL THEN 'SIN DIAGN\u00d3STICO' ELSE efa.descripcion END AS diagnostico ";
		$strQuery .= " FROM historial_clinico.consultas as hc ";
		$strQuery .= " JOIN historial_clinico.enfermedad_actual as efa on hc.id=efa.id_consulta ";
		$strQuery .= " Join historial_clinico.medicos as med on hc.id_medico=med.id ";
		$strQuery .= " join historial_clinico.especialidades as e on med.especialidad=e.id_especialidad ";
		$strQuery .= " ORDER BY hc.id";
		$strQuery .= " ) as c";
		$strQuery .= " LEFT JOIN ";
		$strQuery .= " ( ";
		$strQuery .= " select cedula,n_historial,id as id_historial_medico,borrado ";
		$strQuery .= " from historial_clinico.historial_medico ";
		$strQuery .= " ) as hm ON c.n_historial=hm.n_historial";
		$strQuery .= " LEFT JOIN";
		$strQuery .= " ( ";
		$strQuery .= " SELECT t.ubicacion_administrativa,cor.cedula as cedula,cor.nombre ,cor.apellido,cor.sexo,cor.adscrito,ca.id_adscrito,ente.descripcion as descripcion_entes,";
		$strQuery .= " extract(year from (select age(CURRENT_DATE,cor.fecha_nac_cortesia))) as edad ";
		$strQuery .= " FROM cortesia as cor ";
		$strQuery .= " left join cortesia_adscrito as ca on cor.id= ca.id_cortesia";
		$strQuery .= " left join ente_adscrito as ente on ca.id_adscrito=ente.id ";
		$strQuery .= " join titulares as t on cor.cedula_trabajador=t.cedula_trabajador ";
		$strQuery .= " ) as b ON hm.cedula=b.cedula";
		$strQuery .= " LEFT JOIN";
		$strQuery .= " (";
		$strQuery .= " select id as id_reposo,n_historial, fecha_hasta ";
		$strQuery .= " from historial_clinico.reposos";
		$strQuery .= " where fecha_hasta>= CURRENT_DATE";
		$strQuery .= " )as r ON hm.n_historial=r.n_historial";
		$strWhere = "";
		$strWhere .= " WHERE TRIM(c.tipo_beneficiario)='C' AND TRIM(c.tipo_beneficiario)='C'";
		

		if ($especialidad != '0') {

			$strWhere .= " AND id_especialidad='" . $especialidad . "'";
		}
		if ($medico != '0') {

			$strWhere .= " AND c.id_medico='" . $medico . "'";
		}

		if ($desde != 'null' and $hasta != 'null') {

			$strWhere .= " AND c.fecha_asistencia BETWEEN '$desde'AND '$hasta'";
		}
		if ($sexo != 'null') {

			$strWhere .= " AND sexo='" . $sexo . "'";
		}

		if ($beneficiario != 'null') {

			$strWhere .= " AND TRIM(c.tipo_beneficiario)='" . $beneficiario . "'";
		}

		if ($entes_adscritos != 'null') {
			if ($entes_adscritos > 2) {

				$strWhere .= " AND b.id_adscrito='" . $entes_adscritos . "'";
			} else if ($entes_adscritos == 2) {
				$strWhere .= " AND TRIM(tipo_beneficiario)='C'  AND b.adscrito='true' ";
			}
		} else {

			$strWhere .= " AND TRIM(tipo_beneficiario)='C'";
		}

		$strQuery = $strQuery . $strWhere;
		$strQuery .= " order by c.fecha_asistencia_normal desc ";
	
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}

	public function listar_frecuencia_atencion($desde = null, $hasta = null, $beneficiario = null, $sexo = null)
	{
		$db      = \Config\Database::connect();
		$strQuery = "";
		$strQuery .= "SELECT ";
		$strQuery .= " a.cedula,CONCAT(a.nombre,' ', a.apellido) AS nombre, ";
		$strQuery .= " a.edad, a.sexo,a.tipo as Beneficiario, a.parentesco";
		$strQuery .= " ,b.veces ";
		$strQuery .= " FROM ";
		$strQuery .= " vista_beneficiarios_con_ubicacion as a ";
		$strQuery .= " LEFT JOIN";
		$strQuery .= " (";
		$strQuery .= " SELECT hc.n_historial AS historia,";
		$strQuery .= " hm.cedula,";
		$strQuery .= " count(hc.n_historial) AS veces";
		$strQuery .= " FROM historial_clinico.consultas hc";
		$strQuery .= " JOIN historial_clinico.enfermedad_actual efa ON hc.id = efa.id_consulta";
		$strQuery .= " JOIN historial_clinico.medicos med ON hc.id_medico = med.id";
		$strQuery .= " JOIN historial_clinico.especialidades e ON med.especialidad = e.id_especialidad";
		$strQuery .= " JOIN historial_clinico.historial_medico hm ON hc.n_historial = hm.n_historial";
		if ($desde != 'null' and $hasta != 'null') {

			$strQuery .= " WHERE hc.fecha_creacion between  '$desde'AND '$hasta'";
		}
		$strQuery .= " GROUP BY hc.n_historial, hm.cedula";
		$strQuery .= " ORDER BY hc.n_historial";
		$strQuery .= " ) AS b ON a.cedula=b.cedula";
		$strQuery .= " WHERE";
		$strQuery .= "  b.veces IS NOT NULL  ";
		$strQuery .= " ";
		$strQuery .= " ";
		if ($sexo != 'null') {

			$strQuery .= " and  sexo='" . $sexo . "'";
		}

		if ($beneficiario != 'null') {
			$strQuery .= " and a.tipo='" . $beneficiario . "'";
		}
		$strQuery .= " ORDER BY cedula";
		$strQuery = $strQuery;
		//return $strQuery;
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
	}
	public function listar_Historial_citas($n_historial)
	{
		// ****Campo consulta de la tabla historial_clinico.consultas indica si es un a consulta o una cita
		// *****EJemplo si consulta == true ENTONCES es un aa CONSULTA SI NO ES UNA CITA  ***
		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->select(
			"hc.id
		,hc.n_historial
		,hc.id_medico
		,hc.borrado
		,hc.tipo_beneficiario
		,to_char(hc.fecha_asistencia,'dd/mm/yyyy') as fecha_asistencia
		,hm.cedula
		,to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
		,CONCAT(m.nombre,' ', m.apellido) AS nombre
		 ,e.descripcion as especialidad"
		);
		$builder->join(' historial_clinico.medicos as m ', 'hc.id_medico=m.id');
		$builder->join(' historial_clinico.especialidades as e ', 'm.especialidad=e.id_especialidad');
		$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
		//$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
		$builder->where(['hc.consulta' => false]);
		$builder->where(['hc.borrado' => false]);
		$builder->where(['hm.borrado' => false]);
		$builder->where(['hc.n_historial' => $n_historial]);
		$query = $builder->get();
		return $query;
	}





	public function borrar_consulta($data)
	{
		$builder = $this->dbconn('historial_clinico.consultas as hc');
		$builder->where('hc.id', $data['id'], 'hc.borrado', 'false');
		$query = $builder->update($data);
		return $query;
	}




	public function listar_signos_vitales($n_historial)
	{

		//$builder = $this->dbconn('historial_clinico.consultas as hc');
		$db      = \Config\Database::connect();

		$strQuery = "";
		$strQuery .= "SELECT";
		$strQuery .= " hc.id";
		$strQuery .= ",hc.peso";
		$strQuery .= ",hc.talla";
		$strQuery .= ",hc.spo2";
		$strQuery .= ",to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion";
		$strQuery .= ",hc.frecuencia_c";
		$strQuery .= ",hc.frecuencia_r";
		$strQuery .= ",hc.temperatura ";
		$strQuery .= ",hc.tension_alta";
		$strQuery .= ",hc.tension_baja ";
		$strQuery .= ",hc.imc";
		$strQuery .= ",hc.motivo_consulta ";
		$strQuery .= "FROM ";
		$strQuery .= "historial_clinico.consultas as hc ";
		$strQuery .= "WHERE id in ";
		$strQuery .= "( ";
		$strQuery .= "SELECT";
		$strQuery .= " max(id) ";
		$strQuery .= "FROM ";
		$strQuery .= "historial_clinico.consultas ";
		$strQuery  = $strQuery . " where n_historial='$n_historial'";
		$strQuery .= "AND ";
		$strQuery .= "consulta=true ";
		$strQuery .= "AND ";
		$strQuery .= "borrado=false";
		$strQuery .= ") ";
		$query = $db->query($strQuery);
		$resultado = $query->getResult();
		return $resultado;
		//return  $strQuery;



	}
}

	


// 	$builder = $this->dbconn('historial_clinico.consultas as hc');
// 	$builder->select
// 	(
// 	  "hc.id
// 	  ,hc.n_historial
// 	  ,hc.id_medico
// 	  ,hc.borrado
// 	  ,hc.tipo_beneficiario
// 	  ,hm.cedula
// 	  ,to_char(hc.fecha_creacion,'dd/mm/yyyy') as fecha_creacion
// 	  ,to_char(hc.fecha_asistencia,'dd/mm/yyyy') as fecha_asistencia
// 	  ,CONCAT(m.nombre,' ', m.apellido) AS nombre
// 	   ,e.descripcion as especialidad"
// 	);
// 	$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');

// 	$builder->join(' historial_clinico.medicos as m ', 'hc.id_medico=m.id');
// 	 $builder->join(' historial_clinico.especialidades as e ', 'm.especialidad=e.id_especialidad');
//   //$builder->join(' historial_clinico.historial_medico  as hm ', 'hc.n_historial=hm.n_historial');
// 	$builder->where(['hc.consulta'=>false]);
// 	$builder->where(['hc.borrado'=>false]);
// 	$builder->where(['hm.borrado'=>false]);
// 	$builder->where(['hc.id_medico'=>$medico_id]);
// 	$query = $builder->get();
// 	return $query;	
