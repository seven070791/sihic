<?php

namespace App\Controllers;

use App\Models\Medicos_Model;
use App\Models\Especialidad_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Auditoria_sistema_Model;
use CodeIgniter\RESTful\ResourceController;

class Medicos_Controllers extends BaseController
{
	use ResponseTrait;
	/*
      * Función para mostrar el listado de Roles
      */
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function listar_especialidad()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Especialidad_Model();
		$query = $model->listar_especialidad();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}



	public function listar_medicos()
	{
		$model = new Medicos_Model();
		$query = $model->listar_medicos();;
		if (empty($query)) {
			$especialidad = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}
	public function Buscarmedicos()
	{
		$model = new Medicos_Model();
		$query = $model->Buscarmedicos();;
		if (empty($query)) {
			$Buscarmedicos = [];
		} else {
			$Buscarmedicos = $query;
		}
		echo json_encode($Buscarmedicos);
	}




	public function listar_medicos_activos()
	{
		$model = new Medicos_Model();
		$query = $model->listar_medicos_activos();;
		if (empty($query)) {
			$especialidad = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}


	public function medicos_activos_por_especialidad($id_especialidad = null)
	{
		$model = new Medicos_Model();
		$query = $model->medicos_activos_por_especialidad($id_especialidad);
		if (empty($query)) {
			$especialidad = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}




	public function listar_especialidades_activas()
	{
		$model = new Especialidad_Model();
		$query = $model->listar_especialidades_activas();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}




	public function listar_acceso_citas_especialidades()
	{
		$model = new Especialidad_Model();
		$query = $model->listar_acceso_citas_especialidades();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}


	public function listar_especialidades_activas_sin_filtro()
	{
		$model = new Especialidad_Model();
		$query = $model->listar_especialidades_activas_sin_filtro();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}



	public function listar_especialidades_activas_sin_filtro_caja_chica()
	{
		$model = new Especialidad_Model();
		$query = $model->listar_especialidades_activas_sin_filtro_caja_chica();
		if (empty($query)) {
			$categoria = [];
		} else {
			$especialidad = $query;
		}
		echo json_encode($especialidad);
	}











	public function vistaMedicos()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicos/vistamedicos');
		echo view('/medicos/footer_Medicos');
	}

	public function Vita_Especialidad()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicos/especialidades');
		echo view('/medicos/footer_Especialidades');
	}

	public function medico_Citas()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicos/medico_Citas');
		echo view('/medicos/footer_medico_Citas');
	}
	public function medico_Consultas()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/medicos/medico_Consultas');
		echo view('/medicos/footer_medico_Consultas');
	}


	/*
      * Método que guarda los datos del Medico 
      */
	//public function save()
	public function agregar_Medicos()
	{
		$model = new Medicos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['cedula']   = $data->cedula;
		$datos['nombre']   = $data->nombre;
		$datos['apellido']   = $data->apellido;
		$datos['telefono']   = $data->telefono;
		$datos['especialidad'] = $data->especialidad;
		$query = $model->agregar_medico($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function actualizar_Medicos()
	{
		$modelo = new Medicos_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id_medico;
		$datos['cedula']   = $data->cedula;
		$datos['nombre']   = $data->nombre;
		$datos['apellido']   = $data->apellido;
		$datos['telefono']   = $data->telefono;
		$datos['especialidad'] = $data->especialidad;
		$datos['borrado'] = $data->borrado;
		$query = $modelo->actualizar_Medicos($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}



	/*
      * Método que guarda el registro de la Especialidad
      */
	//public function save()
	public function agregar_Especialidad()
	{
		$model = new Especialidad_Model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['descripcion']   = $data->descripcion;
		$datos['acceso_citas']   = $data->acceso;
		$query = $model->agregar_especialidad($datos);
		if (isset($query)) {

			$mensaje = 1;
            $auditoria['accion'] = 'REGISTRÓ LA ESPECIALIDAD DE  '.' '.$datos['descripcion'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function actualizar_especialidad()
	{
		$modelo = new Especialidad_Model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id_especialidad'] = $data->id_especialidad;
		$datos['descripcion'] = $data->descripcion;
		$datos['borrado']       = $data->borrado;
		$datos['acceso_citas']       = $data->acceso;
		$datos['decripcion_anterior']= $data->descripcion_anterior;
		$datos_modificados['datos_modificados']       = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$query = $modelo->actualizar_especialidad($datos);
		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DE LA ESPECIALIDAD  '.' '.$datos['decripcion_anterior'].','.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}
}
