<?php

namespace App\Controllers;

use App\Models\Tipousuario;

class Grupousuario extends BaseController
{
    // CREAR GRUPOS
    public function create()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        // instancio mi modelo , de mi  tabla 
        $model2 = model(TipoModel::class);
        if ($this->request->getMethod() === 'post' && $this->validate([
            'nivel_usuario'  => 'required',
        ])) {
            $model2->save([
                'nivel_usuario' => $this->request->getPost('nivel_usuario'),
            ]);
            return view('mensajes/mensaje0');
        } else {
            // luego en la vista  create le pase los datos del arreglo 
            echo view('usuarios/creargrupo.php');
        }
    }
    // MUESTRA LA VISTA DE LOS DATOS DEL USUARIO 
    public function index()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $model = model(TipoModel::class);
        $data =
            [
                // grupousuario es lo que le voy a pasar a mi vist en el foreach
                'Grupousuario'  => $model->getTipo(),
            ];
        echo view('usuarios/vistagrupos', $data);
    }

    public function editgrupo1($id_grupo)
    {
        $model = model(TipoModel::class);
        $arrRegistro = $model->getTipo($id_grupo);
        $data =
            [
                'datosgrupo' => $arrRegistro,
            ];
        echo view('usuarios/editargrupo', $data);
        //  die($arrRegistro['id']);
        //   $arrRegistro ves una viariable cualquiera 
    }
    public function savegrupo1()
    {
        if ($this->request->getPost('borrado') !== NULL) {
            //die('borralo');
            $borrado = 't';
        } else {
            //die('no borres');
            $borrado = 'f';
        }
        $model = model(TipoModel::class);
        $data =
            [
                'id' => trim($this->request->getPost('id')),
                'nivel_usuario' => trim($this->request->getPost('nivel_usuario')),
                'borrado' => $borrado
            ];
        $model->savegrupo2($data);

        return view('mensajes/mensaje_edit_user');

        //echo($id_grupo . $nivel_usuario);
        // echo("En savegrupo $id_grupo, $nivel_usuario");
    }
}
