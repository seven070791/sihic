<?php

namespace App\Controllers;

use App\Models\Salida_Model;
use App\Models\Medicamentos_model;
use App\Models\Beneficiarios_Model;
use App\Models\AuditoriaModel;
use App\Models\Reverso_Model;
use App\Models\Entrada_Model;
use App\Models\Autorizador_Model;
use App\Models\Detalles_Nota_Entrega_Model;
use CodeIgniter\API\ResponseTrait;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \PhpOffice\PhpSpreadsheet\IOFactory;

use CodeIgniter\RESTful\ResourceController;

class Salida_Controller extends BaseController
{
	use ResponseTrait;
	public function index($id)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		return ('Esta es la Pagina de entrada  ...' . $id);
	}


	public function listar_salidas_medicos($cedula_beneficiario)
	{
		$model = new Salida_Model();
		$query = $model->listar_salidas_medicos($cedula_beneficiario);



		if (empty($query)) {
			$Salida_Model = [];
		} else {
			$Salida_Model = $query;
		}
		echo json_encode($Salida_Model);
	}








	public function VistaEntradaParaDarSalida($id_medicamento)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Medicamentos_model();
		$query = $model->getDatosMedicamento($id_medicamento);
		if (empty($query->getResult())) {
			$infomedicamento = [];
		} else {
			foreach ($query->getResult() as $fila) {
				$infomedicamento['medicamento'] = $fila->medicamento;
				$infomedicamento['control']    = $fila->descripcion;
				$infomedicamento['id_control']    = $fila->control;
				$infomedicamento['id_medicamento']    = $fila->id;
				$infomedicamento['estatus_med_cronico']    = $fila->estatus_med_cronico;
				//$infomedicamento['id_medicamento']    =$fila->id;
			}
			$totales = $model->getTotalesParaVistaEntradas($id_medicamento);
			$infomedicamento['total_entradas'] = $totales['total_entradas'];
			$infomedicamento['total_salidas'] = $totales['total_salidas'];
			$infomedicamento['total_stock']   = $totales['total_stock'];
			echo view('/salidas/content_salidas', $infomedicamento);
			echo view('/salidas/footer_salidas');
		}
	}

	public function Relacion_salidas_medicamentos()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		} else {
			echo view('/salidas/Relacion_salidas_medicamentos');
			echo view('/salidas/footer_Relacion_salidas_medicamentos');
		}
	}





	public function getAll($id_medicamento = null)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Salida_Model();
		$query = $model->getAll($id_medicamento);
		if (empty($query->getResult())) {
			$salidas = [];
		} else {
			$salidas = $query->getResultArray();
		}
		echo json_encode($salidas);
	}
	public function VistaSalidasContraEntrada($id_entrada, $id_medicamento)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Medicamentos_model();
		$query = $model->getDatosMedicamento($id_medicamento);

		if (empty($query->getResult())) {
			$infomedicamento = [];
		} else {
			foreach ($query->getResult() as $fila) {
				$infomedicamento['medicamento'] = $fila->medicamento;
				$infomedicamento['control']    = $fila->control;
				$infomedicamento['id']    = $fila->id;
				$infomedicamento['id_entrada']    = $id_entrada;
				$infomedicamento['id_medicamento']    = $id_medicamento;
			}
		}
		echo view('/salidas/content_salidas_contra_entradas', $infomedicamento);
		echo view('/salidas/footer_salidas_contra_entradas');
	}
	public function getAllSalidasContraEntradas($id_entrada = null, $id_medicamento = null)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Salida_Model();
		$query = $model->getAllSalidasContraEntradas($id_entrada, $id_medicamento);
		if (empty($query)) {
			$detalles = [];
		} else {
			$detalles = $query;
		}
		echo json_encode($detalles);
	}


	public function agregar()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");

		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id_entrada']        	= $data->id_entrada;
		$datos['id_medicamento']    	= $data->id_medicamento;
		$datos['user_id']           	= session('id_user');
		$datos['cantidad']          	= $data->cantidad;
		$datos['tipo_beneficiario']     = $data->tipo_beneficiario;
		$datos['cedula_beneficiario']   = $data->cedula_beneficiario;
		$datos['control']          		= $data->control;
		$datos['id_medico']          		= $data->id_medico;
		$datos['fecha_salida']          		= $data->fechaconvertida;

		$auditoria['id_entrada']        	= $data->id_entrada;
		$auditoria['id_medicamento']    	= $data->id_medicamento;
		$auditoria['user_id']           	= session('id_user');
		$auditoria['cantidad']          	= $data->cantidad;
		$auditoria['tipo_beneficiario'] 	= $data->tipo_beneficiario;
		$auditoria['cedula_beneficiario']   = $data->cedula_beneficiario;
		$auditoria['control']          		= $data->control;
		$auditoria['hora'] = $hora;
		$model = new Salida_model();
		$model2 = new AuditoriaModel();
		$query = $model->Agregar($datos);
		$query2 = $model2->agregarAccion_Salida($auditoria);

		//  echo($query);
		//  die();

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	//*****Este metodo es para reportes salidas FPDF**
	public function GenerarReportesSalidasPorFecha($desde = null, $hasta = null, $id_medicamento = null, $todo = null)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}

		$model = new Salida_Model();
		$salidas = $model->GenerarReportesSalidasPorFecha($desde, $hasta, $id_medicamento);
		if (empty($salidas)) {
			$salidas = [];
		} else {
			$salidas = $salidas;
		}
		echo json_encode($salidas);
	}

	// *********************REPORTE GENERAL DE SALIDAS********************

	public function Relacion_salidas_medicamentosPDF($id_categoria = 0, $cronicos = false, $desde = null, $hasta = null, $sexo = null, $beneficiario = null, $medico = 0)
	{
		$model = new Salida_Model();
		$query = $model->getAllSalidas_MedicamentosPDF($id_categoria, $cronicos, $desde, $hasta, $sexo, $beneficiario, $medico);

		if (empty($query)) {
			$salidas = [];
		} else {
			$salidas = $query;
		}
		echo json_encode($salidas);
	}

	public function Relacion_salidas_medicamentos_entes($id_categoria = 0, $cronicos = false, $desde = null, $hasta = null, $sexo = null, $beneficiario = null, $medico = 0, $entes_adscritos = 0)
	{


		$model = new Salida_Model();
		$query = $model->getAllSalidas_MedicamentosEntesPDF($id_categoria, $cronicos, $desde, $hasta, $sexo, $beneficiario, $medico, $entes_adscritos);
		if (empty($query)) {
			$salidas = [];
		} else {
			$salidas = $query;
		}
		echo json_encode($salidas);
	}



	public function VerSalidasMedicamentosPdf()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('L', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Salidas();

		$model = new Salida_model();
		$salidas = $model->getAllSalidas();
		if (empty($salidas)) {
			$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
		} else {
			$i = 0;
			foreach ($salidas as $salidas) {
				$descripcion = $salidas->descripcion;
				$fecha_salida = $salidas->fecha_salida;
				$cantidad = $salidas->cantidad;
				$nombre = $salidas->nombre;
				$tipo_beneficiario = $salidas->tipo_beneficiario;
				$beneficiario = $salidas->beneficiario;

				$pdf->Cell(18, 5, $nombre, 1, 0, 'L');
				$pdf->Cell(82, 5, $descripcion, 1, 0, 'L');
				$pdf->Cell(30, 5, $fecha_salida, 1, 0, 'L');
				$pdf->Cell(20, 5, $cantidad, 1, 0, 'L');
				$pdf->Cell(65, 5, $beneficiario, 1, 0, 'L');
				$pdf->Cell(43, 5, $tipo_beneficiario, 1, 1, 'C');
				$i++;
				if ($i == 42) {
					$pdf->AddPage();
					$pdf->Header_Salidas();
					$i = 0;
				}
			}
		}

		$this->response->setHeader('Content-Type', 'application/pdf');
		$pdf->Output("salidas_pdf.pdf", "I");
	}

	public function getAll_infomedicamento_titulares($cedula_trabajador = null, $desde = null, $hasta = null, $id_medico = '0')
	{

		$model = new Salida_Model();
		$query = $model->getAll_infomedicamento_titulares($cedula_trabajador, $desde, $hasta, $id_medico);
		//echo($query);
		//die();
		if (empty($query)) {
			$salidas = [];
		} else {
			$salidas = $query;
		}
		echo json_encode($salidas);
	}

	public function getAll_infomedicamento_familiares($cedula_beneficiario = null, $desde = null, $hasta = null, $id_medico = '0')
	{

		$model = new Salida_Model();
		$query = $model->getAll_infomedicamento_familiares($cedula_beneficiario, $desde, $hasta, $id_medico);
		//echo($query);
		// die();
		if (empty($query)) {
			$salidas = [];
		} else {
			$salidas = $query;
		}
		echo json_encode($salidas);
	}

	public function getAll_infomedicamento_cortesia($cedula_beneficiario = null, $desde = null, $hasta = null, $id_medico = '0')
	{

		$model = new Salida_Model();
		$query = $model->getAll_infomedicamento_cortesia($cedula_beneficiario, $desde, $hasta, $id_medico);

		if (empty($query)) {
			$salidas = [];
		} else {
			$salidas = $query;
		}
		echo json_encode($salidas);
	}



	/*
      * Método que actualiza la salida
      */
	public function actualizar_salida()
	{
		$modelo = new Salida_Model();
		$model2 = new AuditoriaModel();
		$modeloReversoSalida = new Reverso_Model();
		$buscar_salida_en_detalle_notas = new Detalles_Nota_Entrega_Model;
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']       = $data->id_salida;
		$datos['id_medicamento']       = $data->id_medicamento;
		$datos['id_entrada']       = $data->id_entrada;
		$datos['borrado']       = $data->estatus;
		$datos['cantidad']       = $data->cantidad;
		$datos['user_id']           = session('id_user');
		$datos_reverso['observacion'] = $data->observacion;
		$id_salida = $data->id_salida;
		$query_salida_en_detalle_notas = $buscar_salida_en_detalle_notas->buscar_salida_en_detalle_notas($id_salida);
		if (empty($query_salida_en_detalle_notas)) {
			$auditoria['id']       = $data->id_salida;
			$auditoria['id_medicamento']       = $data->id_medicamento;
			$auditoria['cantidad']       = $data->cantidad;
			$auditoria['user_id']           = session('id_user');
			$auditoria['hora'] = $hora;
			$query = $modelo->reverso_salida($datos);
			$query2 = $model2->auditoria_Reverso_Salida($auditoria);
			$query_reverso_tabla = $modeloReversoSalida->ReversoSalidaTabla($datos, $datos_reverso);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		} else {
			$mensaje = 3;
		}

		// //$mensaje=$datos;
		return json_encode($mensaje);
	}


	// *********************REPORTE GENERAL DE SALIDAS********************
	public function Nota_Entrega_medicamento($cedula_beneficiario, $tipobeneficiario, $fechanotas,$id_autorizador)
	{

		$cedula_sin_espacios = trim($cedula_beneficiario);

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Nota_de_Entrega();
		$model = new Beneficiarios_model();
		$model_Autorizador = new Autorizador_Model();
		$model_medicamento = new Salida_Model();
		if ($tipobeneficiario == 'T') {
			$titular = $model->buscartitular_NotasEntregas($cedula_beneficiario);
			$medicamentos = $model_medicamento->nota_Entrega_medicamentos_titulares($cedula_beneficiario, $fechanotas);
			
			
			if (empty($titular)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($titular as $titular) {
					$fechanotas = date('d M Y', strtotime($fechanotas));
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 63, $fechanotas, 0, 1, 'L');
					$nombre = $titular->nombre;
					$apellido = $titular->apellido;
					$fecha_nacimiento = $titular->fecha_nacimiento;
					$cedula_trabajador = $titular->cedula_trabajador;
					$ubicacion_administrativa = $titular->ubicacion_administrativa;
					$edad_actual = $titular->edad_actual;
					$telefono = $titular->telefono;
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . ' ' . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->Cell(10, 3, '', 0, 2, 'L');
				}

				foreach ($medicamentos as $medicamento) {
					$descripcion = $medicamento->descripcion;
					$cantidad = $medicamento->cantidad;
					$control = $medicamento->control;
					$presentacion = $medicamento->presentacion;
					$usuario = $medicamento->nombre;
					$pdf->Cell(80, 5, $descripcion, 1, 0, 'L');
					$pdf->Cell(30, 5, $cantidad, 1, 0, 'C');
					$pdf->Cell(30, 5, $presentacion, 1, 0, 'C');
					$pdf->Cell(30, 5, $control, 1, 0, 'C');
					$pdf->Cell(30, 5, '', 1, 1, 'C');
				}
				$autorizador = $model_Autorizador->buscar_autorizador_activo($id_autorizador);
				foreach ($autorizador as $auto) 
				{
					$autorizador = $auto->nombre_apellido;
				}
				

				$pdf->footer_notasentregas($usuario,$autorizador);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'F') {
			$familiar = $model->buscarfamiliar_NotasEntregas($cedula_beneficiario);
			$medicamentos = $model_medicamento->nota_Entrega_medicamentos_familiares($cedula_beneficiario, $fechanotas);

			if (empty($familiar)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {


				foreach ($familiar as $familiar) {
					//***TITULAR*****	
					//$fechanotas=$fechanotas;

					$fechanotas = date('d M Y', strtotime($fechanotas));
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 63, $fechanotas, 0, 1, 'L');
					$nombre = $familiar->nombre_titular;
					$apellido = $familiar->apellido_titular;
					$fecha_nacimiento = $familiar->fecha_nacimientot;
					$cedula_trabajador = $familiar->cedula_trabajador;
					$ubicacion_administrativa = $familiar->departamento;
					$edad_actual = $familiar->edad_actualt;
					$telefono = $familiar->telefonot;
					//****FAMILIAR***
					$nombreb = $familiar->nombre;
					$apellidob = $familiar->apellido;
					$fecha_nacimientob = $familiar->fecha_nac_familiares;
					$cedulab = $familiar->cedula;
					$edad_actualb = $familiar->edad_actual;
					$descripcionparentesco = $familiar->descripcionparentesco;
					$telefonob = $familiar->telefono;


					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . ' ' . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					//****FAMILIAR***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 142, $nombreb . ' ' . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 158, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 158, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 158, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 174, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 191, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
				}

				foreach ($medicamentos as $medicamento) {
					$descripcion = $medicamento->descripcion;
					$cantidad = $medicamento->cantidad;
					$control = $medicamento->control;
					$usuario = $medicamento->nombre;
					$presentacion = $medicamento->presentacion;
					$pdf->Cell(80, 5, $descripcion, 1, 0, 'L');
					$pdf->Cell(30, 5, $cantidad, 1, 0, 'C');
					$pdf->Cell(30, 5, $presentacion, 1, 0, 'C');
					$pdf->Cell(30, 5, $control, 1, 0, 'C');
					$pdf->Cell(30, 5, '', 1, 1, 'L');
				}
				$autorizador = $model_Autorizador->getAllActivos();
				foreach ($autorizador as $auto) 
				{
					$autorizador = $auto->nombre_apellido;
				}
				

				$pdf->footer_notasentregas($usuario,$autorizador);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'C') {
			$cortesia = $model->buscarcortesia_NotasEntregas($cedula_beneficiario);

			$medicamentos = $model_medicamento->nota_Entrega_medicamentos_cortesia($cedula_beneficiario, $fechanotas);
			if (empty($cortesia)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {

				foreach ($cortesia as $cortesia) {
					//***TITULAR*****	
					//$fechanotas=$fechanotas;

					$fechanotas = date('d M Y', strtotime($fechanotas));
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 63, $fechanotas, 0, 1, 'L');
					$nombre = $cortesia->nombre_titular;
					$apellido = $cortesia->apellido_titular;
					$fecha_nacimiento = $cortesia->fecha_nacimientot;
					$cedula_trabajador = $cortesia->cedula_trabajador;
					$ubicacion_administrativa = $cortesia->departamento;
					$edad_actual = $cortesia->edad_actualt;
					$telefono = $cortesia->telefonot;
					//****cortesia***
					$nombreb = $cortesia->nombre;
					$apellidob = $cortesia->apellido;
					$fecha_nacimientob = $cortesia->fecha_nac_cortesia;
					$cedulab = $cortesia->cedula;
					$edad_actualb = $cortesia->edad_actual;
					$descripcionparentesco = 'CORTESIA';
					$telefonob = $cortesia->telefono;

					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . ' ' . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');

					//****CORTESIA***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 142, $nombreb . ' ' . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 158, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 158, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 158, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 174, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 191, $telefonob, 0, 1, 'L');
					// $pdf->Cell(10,3,'',0,1,'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
				}

				foreach ($medicamentos as $medicamento) {
					$descripcion = $medicamento->descripcion;
					$cantidad = $medicamento->cantidad;
					$control = $medicamento->control;
					$usuario = $medicamento->nombre;
					$presentacion = $medicamento->presentacion;
					$pdf->Cell(80, 5, $descripcion, 1, 0, 'L');
					$pdf->Cell(30, 5, $cantidad, 1, 0, 'C');
					$pdf->Cell(30, 5, $presentacion, 1, 0, 'C');
					$pdf->Cell(30, 5, $control, 1, 0, 'C');
					$pdf->Cell(30, 5, '', 1, 1, 'C');
				}
				$autorizador = $model_Autorizador->getAllActivos();
				foreach ($autorizador as $auto) 
				{
					$autorizador = $auto->nombre_apellido;
				}
				$pdf->footer_notasentregas($usuario,$autorizador);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		}
	}
}
