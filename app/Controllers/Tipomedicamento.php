<?php

namespace App\Controllers;
use App\Models\Auditoria_sistema_Model;
use App\Models\Tipomedicamentos_model;
use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Tipomedicamento extends BaseController
{
	use ResponseTrait;
	public function index()
	{
		return ('Esta es la Página de tipo de Medicamentos ...');
	}
	/*
      * Función para mostrar el listado de Roles
      */
	public function vista()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/listarTipoMedicamentos/content');
		echo view('/listarTipoMedicamentos/footer');
	}

	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll()
	{
		$model = new Tipomedicamentos_model();
		$query = $model->getAll();
		if (empty($query->getResult())) {
			$tipo_medicamentos = [];
		} else {
			$tipo_medicamentos = $query->getResultArray();
		}
		echo json_encode($tipo_medicamentos);
	}

	public function getAllActivos($id_cmbTipoMedicamento = null)
	{

		$model = new Tipomedicamentos_model();
		$query = $model->getAllActivos($id_cmbTipoMedicamento);

		if (empty($query->getResult())) {
			$tipo_medicamentos = [];
		} else {
			$tipo_medicamentos = $query->getResultArray();
		}
		echo json_encode($tipo_medicamentos);
	}


	/*
      * Método que guarda el registro nuevo
      */
	//public function save()
	public function agregar()
	{
		if ($this->request->isAJAX()) {
			$model = new Tipomedicamentos_model();
			$model_auditoria=new Auditoria_sistema_Model();
			$data = json_decode(base64_decode($this->request->getPost('data')));

			$datos['descripcion']   = $data->tipoMedicamento;
			$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
			$datos['categoria_id'] = $data->id_tipoInventario;
			$query = $model->agregar($datos);
			//echo($query);
			if (isset($query)) {
				$mensaje = 1;
				$auditoria['accion'] = 'REGISTRÓ EL TIPO DE MEDICAMENTO  '.' '.$datos['descripcion'];
				$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
			} else {
				$mensaje = 0;
			}
		} else {
			redirect()->to('/403');
		}
		return json_encode($mensaje);
	}

	public function actualizar()
	{
		$modelo = new Tipomedicamentos_model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']            = $data->id;
		$datos['descripcion']   = $data->tipoMedicamento;
		$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
		$datos['borrado']       = $data->borrado;
		$datos['categoria_id']       = $data->id_tipoInventario;
		$datos_modificados['datos_modificados']       = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$datos_descr_anterior['decripcion_anterior']= $data->descripcion_anterior;
		$query = $modelo->actualizar($datos);
	
		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DEL TIPO DE INVENTARIO  '.' '.$datos_descr_anterior['decripcion_anterior'].','.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		// //$mensaje=$datos;
		 return json_encode($mensaje);
	}
}
