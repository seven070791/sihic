<?php

namespace App\Controllers;

use App\Models\Auditoria_sistema_Model;


use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Auditoria_sistema_Controllers extends BaseController
{
	use ResponseTrait;

	public function auditoria_sistema()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}

		echo view('/auditoria_sistema/content_Auditoria_sistema');
		echo view('/auditoria_sistema/footer_Auditoria_sistema');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */

	public function listar_auditoria_sistema($direccion_ip = null, $dispositivo = null)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Auditoria_sistema_Model();

		$query = $model->listar_auditoria_sistema($direccion_ip, $dispositivo);

		if (empty($query)) {
			$auditoria = [];
		} else {
			$auditoria = $query;
		}
		echo json_encode($auditoria);
	}

	// public function agregar($auditoria)
	// {
	// 	date_default_timezone_set('America/Caracas');
	// 	$hora = date("H:i:s A");
	// 	$model = new Auditoria_sistema_Model();
	// 	$data = json_decode(base64_decode($this->request->getPost('data')));
	// 	$datos['hora'] = $hora;
	// 	$datos['user_id']           = session('id_user');
	// 	$datos['accion']   = $data->accion;
	// 	$query = $model->agregar($auditoria);

	// 	//echo($query);

	// }








	/*
      * Método que guarda el registro nuevo
      */
	//public function save()



}
