<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Historial_info_Model;
use CodeIgniter\RESTful\ResourceController;

class Historial_info_Controllers extends BaseController
{
	use ResponseTrait;

	
	public function Listar_Historial_Informativo()
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Historial_info_Model();

		$query = $model->Listar_Historial_Informativo();

		if (empty($query)) {
			$historial_informativo = [];
		} else {
			$historial_informativo = $query;
		}
		echo json_encode($historial_informativo);
	}


	
}
