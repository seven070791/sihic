<?php

namespace App\Controllers;
use App\Models\Auditoria_sistema_Model;
use App\Models\User_Model;
use App\Models\Tipousuario;
use App\Models\Medicos_Model;
use phpDocumentor\Reflection\PseudoTypes\True_;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;
class User_controllers extends BaseController
{
    public function index()
    {
        if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
        echo view('usuarios/vista_usuarios');
        echo view('usuarios/footer_usuarios.php');
    }
    public function listar_usuarios()
	{
		$model = model(User_Model::class);
		$query = $model->getUsuarios();
		if (empty($query->getResult())) {
			$usuarios = [];
		} else {
			$usuarios = $query->getResultArray();
		}
		echo json_encode($usuarios);
	}
    public function listar_nivel_usuario()
	{
		$model = model(User_Model::class);
		$query = $model->listar_nivel_usuario();
		if (empty($query->getResult())) {
			$nivel_usuario = [];
		} else {
			$nivel_usuario = $query->getResultArray();
		}
		echo json_encode($nivel_usuario);
	}
/*
      * Método que actualiza el registro
      */
      public function ActualizarUsuario()
      {
        $modelo = model(User_Model::class);;
        $model_auditoria=new Auditoria_sistema_Model();
       // $data = json_decode(base64_decode($this->request->getPost('data')));
        $data = json_decode(utf8_encode(base64_decode($this->request->getPost('data'))));
        $datos['id']            = $data->id;
        $datos['cedula']   = $data->cedula;
        $datos['nombre']   = $data->nombre;
        $datos['apellido']   = $data->apellido;
        $datos['usuario']   = $data->usuario;
        $datos['tipousuario']   = $data->tipousuario;
        $datos['borrado'] = $data->borrado;
        //$datos['password'] = $data->password;
        $datos['password'] = password_hash($data->password, PASSWORD_BCRYPT);
          $datos_nombre['datos_nombre']=$data->nombre.' '.$data->apellido;
          $datos_modificados['datos_modificados']       = $data->datos_modificados;
          $datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
           $query = $modelo->actualizar($datos); 
          if (isset($query)) {
              $mensaje = 1;
              $auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DE  '.' '.$datos_nombre['datos_nombre'].','.' '.$datos_modificados['datos_modificados'];
              $Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
          } else {
              $mensaje = 0;
          }
           return json_encode($mensaje);
      }



     


    // CREAR USUARIOS
    public function create()
    {

        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        echo view('/usuarios/footer_usuarios');

        //$model = model(User_Model::class);
        $model = new User_Model();
        $model2 = model(TipoModel::class);
        $medico = model(Medicos_Model::class);
        $modelo_auditoria = new Auditoria_sistema_Model();
        $data['tipos'] = $model2->getTipo();
        if ($this->request->getMethod() === 'post' && $this->validate([
            'cedula' => 'required|min_length[7]|max_length[8]',
            'nombre'  => 'required',
            'tipousuario' => 'required',
        ])) {
            $swGuardarUsuario = false;
            $tipousuario = $this->request->getPost('tipousuario');
            if ($tipousuario == '3') {
                $busca_cedula_medico = $this->request->getPost('cedula');
                $datosmedico = $medico->Buscarmedicos($busca_cedula_medico);
                if (empty($datosmedico)) {
                    $swGuardarUsuario = false;
                    //echo('NO ESTA EN MEDICOS ');
                    return view('mensajes/Error_no_Medico');
                } else {
                    $swGuardarUsuario = true;
                    //echo(' ESTA EN MEDICOS ');                   
                    //echo('SE ENCUENTRA REGISTRADO EN MEDICO');  
                }
            } else {
                $swGuardarUsuario = true;
            }
            if ($swGuardarUsuario == true) {
                $busca_cedula_usuario = $this->request->getPost('cedula');
                $datousuario = $model->buscarusuario($busca_cedula_usuario);
                if (empty($datousuario)) {
                    $data = [
                        'cedula' => $this->request->getPost('cedula'),
                        'nombre'  => $this->request->getPost('nombre'),
                        'apellido'  => $this->request->getPost('apellido'),
                        'password'  => $this->request->getPost('password'),
                        'usuario' => $this->request->getPost('usuario'),
                        //'correo' => $this->request->getPost ('correo'),
                        'tipousuario'  => $this->request->getPost('tipousuario'),
                        //'master'  => $this->request->getPost ('validarmaster'),          
                    ];

                    $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
                    $query = $model->agregar($data);
                    if (isset($query)) {
                       
                         $auditoria['accion'] = 'REGISTRO EL USUARIO  '.' '.$data['usuario'];
			             $Auditoria_sistema_Model = $modelo_auditoria->agregar($auditoria);
                        return view('mensajes/mensaje_c_user');
                    }
                } else {
                    return view('mensajes/Error_Usuario_Registrado');
                }
            }
        } else {
            //     // luego en la vista  create le pase los datos del arreglo 
            echo view('usuarios/create', $data);
        }
    }
    // public function ActualizarUsuario()

    // {
    //     if (!session('nombreUsuario')) {
    //         return redirect()->to(base_url() . '/index.php');
    //     }
    //     // Aqui valido si viene o inactivo 
    //     if ($this->request->getPost('borrado') !== NULL) {
    //         $borrado = 't';
    //     } else {
    //         $borrado = 'f';
    //     }

    //     $model = model(User_Model::class);
    //     if ($this->request->getMethod() === 'post' && $this->validate([
    //         'cedula' => 'required',
    //         'nombre'  => 'required',
    //     ])) {
    //         $data =
    //             [
    //                 'id' => trim(intval($this->request->getPost('id'))),
    //                 'cedula' => trim(intval($this->request->getPost('cedula'))),
    //                 'nombre'  => trim(strval($this->request->getPost('nombre'))),
    //                 'apellido'  => trim(strval($this->request->getPost('apellido'))),
    //                 'password'  => trim(strval($this->request->getPost('password'))),
    //                 'usuario' => trim(strval($this->request->getPost('usuario'))),
    //                 //'correo' => trim(strval($this->request->getPost ('correo'))),
    //                 'tipousuario'  => trim(strval($this->request->getPost('tipousuario'))),
    //                 'borrado'  => $borrado

    //             ];
    //             $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
    //             $model->ActualizarUsuario($data);

    //         return view('mensajes/mensaje_registro_actualizado');
    //         // return redirect()->to('news');
    //     } else {

    //         echo view('usuarios/create');
    //     }
    // }

    public function delete($cedula = null, $estatus = true)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $model = model(User_Model::class);
        $resultado = $model->delete($cedula, $estatus);
        if ($resultado == true) {
            return view('mensajes/mensaje_registro_actualizado');
        } else {
            echo ('Error');
        }
    }

    // MUESTRA LA VISTA DE LOS DATOS DEL USUARIO 

    public function view($slug = null)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url() . '/index.php');
        }
        $model = model(User_Model::class);

        $data['infousuarios'] = $model->getUsuarios($slug);
        if (empty($data['infousuarios'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('no se encontro nada ' . $slug);
        }
        $modeltipos = model(TipoModel::class);
        $data['tipos'] = $modeltipos->getTipo();

        echo view('usuarios/edituser', $data);
        
    }
}
// Esto se para mostrar los que esta llegando al request
        // var_dump($this->request);
