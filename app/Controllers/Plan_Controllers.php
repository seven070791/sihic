<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Plan_Model;
use CodeIgniter\RESTful\ResourceController;

class Plan_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_plan()
	{
		$model = new Plan_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->plan;
		$datos['id_consulta']   = $data->id_consulta;
		$query = $model->agregar($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}





	public function listar_plan($n_historial)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Plan_Model();

		$query = $model->listar_plan($n_historial);
		if (empty($query)) {
			$impresion_diag = [];
		} else {
			$impresion_diag = $query;
		}
		echo json_encode($impresion_diag);
	}


	public function actualizar_plan()
	{
		$model = new Plan_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->plan;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_plan($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
