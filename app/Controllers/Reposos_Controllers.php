<?php

namespace App\Controllers;

use App\Models\Reposos_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Auditoria_sistema_Model;
use App\Models\Medicos_Model;
use App\Models\Notas_Entregas_Model;
use App\Models\Especialidad_Model;
use App\Models\Detalles_Nota_Entrega_Model;
use App\Models\Salida_Model;
use App\Models\Beneficiarios_Model;




use CodeIgniter\RESTful\ResourceController;

class Reposos_Controllers extends BaseController
{
	use ResponseTrait;
	/*
      * Función para mostrar el listado de Roles
      */
	public function reposos()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/reposos/reposos');
		echo view('/reposos/footer_reposos');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */

	public function listar_reposos($desde = null, $hasta = null, $id_medico = 0, $especialidad = 0)
	{

		$model = new Reposos_Model();
		$query = $model->listar_reposos($desde, $hasta, $id_medico, $especialidad);
		if (empty($query)) {
			$reposos = [];
		} else {
			$reposos = $query;
		}
		echo json_encode($reposos);
	}

	public function listar_reposos_con_filtro($desde = null, $hasta = null, $n_historial = null)
	{

		$model = new Reposos_Model();
		$query = $model->listar_reposos_con_filtro($desde, $hasta, $n_historial);


		if (empty($query)) {
			$reposos = [];
		} else {
			$reposos = $query;
		}

		echo json_encode($reposos);
	}


	public function Agregar_Reposo()
	{
		$model_reposos = new Reposos_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->t_n_historial;
		//$datos['id_consulta'] = $data->t_id_consulta;
		$datos['id_medico'] = $data->t_id_medico;
		$datos['fecha_desde']   = $data->fecha_desde;
		$datos['fecha_hasta'] = $data->fecha_hasta;
		$datos['horas'] = $data->horas_d;
		$datos['motivo'] = $data->motivo;
	
		$auditoria['accion'] = $data->accion;
		$query = $model_reposos->Agregar_Reposo($datos);
		if (isset($query)) {
			$mensaje = 1;
			$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function Actualizar_Reposo()
	{
		$model = new Reposos_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id_reposo;
		$datos['n_historial']   = $data->t_n_historial;
		$datos['id_medico'] = $data->t_id_medico;
		$datos['fecha_desde']   = $data->fecha_desde;
		$datos['fecha_hasta'] = $data->fecha_hasta;
		$datos['horas'] = $data->horas_d;
		$datos['motivo'] = $data->motivo;
		$auditoria['accion'] = $data->accion;
		$query = $model->Actualizar_Reposo($datos);
		if (isset($query)) {
			$mensaje = 1;
			$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function Reversar_reposo()
	{
		$model = new Reposos_Model();
		$model_Auditoria_sistema_Model = new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['id'] = $data->id_reposo;
		$datos['borrado'] = $data->borrado;
		$auditoria['accion'] = $data->accion;
		$query = $model->Reversar_reposo($datos);

		if (isset($query)) {
			$mensaje = 1;
			$Auditoria_sistema_Model = $model_Auditoria_sistema_Model->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}



	// *********************REPORTES DE REPOSOS ********************
	public function PDF_Reposos($cedula_beneficiario, $n_historial, $id_reposo, $nombre_medico, $today, $tipo_beneficiario, $horas_d)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_reposos($today,$tipo_beneficiario);
		$model = new Beneficiarios_model();
		$model_reposos = new Reposos_Model();
		$info_reposo = $model_reposos->buscar_info_reposo($n_historial, $id_reposo);

		if ($tipo_beneficiario == 'T') {
			$titular = $model->buscartitular_NotasEntregas($cedula_beneficiario);
			if (empty($titular)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($titular as $titular) {
					$pdf->SetXY(27.1, 11);
					$nombre = $titular->nombre;
					$apellido = $titular->apellido;
					$fecha_nacimiento = $titular->fecha_nacimiento;
					$cedula_trabajador = $titular->cedula_trabajador;
					$ubicacion_administrativa = $titular->ubicacion_administrativa;
					$edad_actual = $titular->edad_actual;
					$telefono = $titular->telefono;
					$pdf->SetXY(70.2, 51);
					$pdf->Cell(80, 64, $nombre . ' ' . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 51);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 51);
					$pdf->Cell(35, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 51);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 51);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 51);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->Cell(10, 3, '', 0, 1, 'L');
				}
				foreach ($info_reposo as $info_reposo) {
					$pdf->Ln(-50);
					$pdf->SetFillColor(247, 249, 249);
					$pdf->SetTextColor(0, 0, 0);
					$motivo = $info_reposo->motivo;
					$fecha_desde = $info_reposo->fecha_desde;
					$fecha_hasta = $info_reposo->fecha_hasta;
					$medico = $info_reposo->medico;
				}
				$pdf->content_reposos($info_reposo, $horas_d);
				$pdf->footer_reposos($medico);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipo_beneficiario == 'F') {
			$familiar = $model->buscarfamiliar_NotasEntregas($cedula_beneficiario);
			
			if (empty($familiar)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($familiar as $familiar) {
					$pdf->SetXY(27.1, 11);
					// Informacion Titular
					$nombre_titular = $familiar->nombre_titular;
					$apellido_titular = $familiar->apellido_titular;
					$cedula_trabajador = $familiar->cedula_trabajador;
					$ubicacion_administrativa = $familiar->departamento;
					$fecha_nacimientot = $familiar->fecha_nacimientot;
					$edad_actualt = $familiar->edad_actualt;
					$telefonot = $familiar->telefonot;
					$pdf->SetXY(70.2, 51);
					$pdf->Cell(80, 64, $nombre_titular . ' ' . $apellido_titular, 0, 1, 'L');
					$pdf->SetXY(70.2, 54);
					$pdf->Cell(80, 74, $fecha_nacimientot, 0, 1, 'L');
					$pdf->SetXY(135.2, 54);
					$pdf->Cell(80, 74, $cedula_trabajador, 0, 1, 'L');
					$pdf->SetXY(192.2, 54);
					$pdf->Cell(80, 74, $edad_actualt, 0, 1, 'L');
					$pdf->SetXY(70.2, 67);
					$pdf->Cell(80, 64, $ubicacion_administrativa, 0, 1, 'L');
					$pdf->SetXY(70.2, 70);
					$pdf->Cell(80, 74, $telefonot, 0, 1, 'L');
					// Informacion Familiar
					$nombre = $familiar->nombre;
					$apellido = $familiar->apellido;
					$fecha_nacimiento = $familiar->fecha_nac_familiares;
					$cedula_familiar = $familiar->cedula;
					$ubicacion_administrativa = 'FAMILIAR'.' '.'('.$familiar->descripcionparentesco.')';
					$edad_actual = $familiar->edad_actual;
					$telefono = $familiar->telefono;
					$pdf->SetXY(70.2, 96);
					$pdf->Cell(80, 64, $nombre . ' ' . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 96);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 96);
					$pdf->Cell(35, 80, $cedula_familiar, 0, 0, 'L');
					$pdf->SetXY(195.2, 96);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 97);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 98);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->Cell(10, 3, '', 0, 1, 'L');
				}
				foreach ($info_reposo as $info_reposo) {
					$pdf->Ln(-50);
					$pdf->SetFillColor(247, 249, 249);
					$pdf->SetTextColor(0, 0, 0);
					$motivo = $info_reposo->motivo;
					$fecha_desde = $info_reposo->fecha_desde;
					$fecha_hasta = $info_reposo->fecha_hasta;
					$medico = $info_reposo->medico;
				}
				$pdf->content_reposos($info_reposo, $horas_d);
				$pdf->footer_reposos($medico);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipo_beneficiario == 'C') {
			$cortesia = $model->buscarcortesia_NotasEntregas($cedula_beneficiario);
			if (empty($cortesia)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($cortesia as $cortesia) {
					$pdf->SetXY(27.1, 11);

				// Informacion Titular
				// $nombre_titular = $cortesia->nombre_titular;
				// $apellido_titular = $cortesia->apellido_titular;
				// $cedula_trabajador = $cortesia->cedula_trabajador;
				// $ubicacion_administrativa = $cortesia->departamento;
				// $fecha_nacimientot = $cortesia->fecha_nacimientot;
				// $edad_actualt = $cortesia->edad_actualt;
				// $telefonot = $cortesia->telefonot;
				// $pdf->SetXY(70.2, 51);
				// $pdf->Cell(80, 64, $nombre_titular . ' ' . $apellido_titular, 0, 1, 'L');
				// $pdf->SetXY(70.2, 54);
				// $pdf->Cell(80, 74, $fecha_nacimientot, 0, 1, 'L');
				// $pdf->SetXY(135.2, 54);
				// $pdf->Cell(80, 74, $cedula_trabajador, 0, 1, 'L');
				// $pdf->SetXY(192.2, 54);
				// $pdf->Cell(80, 74, $edad_actualt, 0, 1, 'L');
				// $pdf->SetXY(70.2, 67);
				// $pdf->Cell(80, 64, $ubicacion_administrativa, 0, 1, 'L');
				// $pdf->SetXY(70.2, 70);
				// $pdf->Cell(80, 74, $telefonot, 0, 1, 'L');
				// Informacion Cortesia
				$nombre = $cortesia->nombre;
				$apellido = $cortesia->apellido;
				$fecha_nacimiento = $cortesia->fecha_nac_cortesia;
				$cedula_cortesia = $cortesia->cedula;
				$ubicacion_administrativa = 'CORTESIA';
				$edad_actual = $cortesia->edad_actual;
				$telefono = $cortesia->telefono;
				$ente_adscrito = $cortesia->descripcion;
				$pdf->SetXY(70.2, 51);
				$pdf->Cell(80, 64, $nombre . ' ' . $apellido, 0, 1, 'L');
				$pdf->SetXY(70.2, 51);
				$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
				$pdf->SetXY(135.2, 51);
				$pdf->Cell(35, 80, $cedula_cortesia, 0, 0, 'L');
				$pdf->SetXY(195.2, 51);
				$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');	
				$pdf->SetXY(70.2, 51);
				$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
				$pdf->SetXY(70.2, 51);
				$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
				$pdf->Cell(10, 3, '', 0, 1, 'L');	
				$pdf->SetXY(70.2, 60);
				$pdf->Cell(80, 111, $ente_adscrito, 0, 1, 'L');
				$pdf->Cell(10, 3, '', 0, 1, 'L');	
						
				}
				foreach ($info_reposo as $info_reposo) {
					$pdf->Ln(-50);
					$pdf->SetFillColor(247, 249, 249);
					$pdf->SetTextColor(0, 0, 0);
					$motivo = $info_reposo->motivo;
					$fecha_desde = $info_reposo->fecha_desde;
					$fecha_hasta = $info_reposo->fecha_hasta;
					$medico = $info_reposo->medico;
				}
				$pdf->content_reposos($info_reposo, $horas_d);
				$pdf->footer_reposos($medico);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		}
	}
}
