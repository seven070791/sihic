<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Morbilidad_Model;
use CodeIgniter\RESTful\ResourceController;

class Morbilidad_Controller extends BaseController
{
	use ResponseTrait;
	/*
      * Función para mostrar el listado de Roles
      */
	public function Hoja_morbilidad()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/reportes/hoja_mobilidad');
		echo view('/reportes/footer_morbilidad');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function listar_mobilidad($desde = null, $hasta = null, $medico = 0, $especialidad = 0, $sexo = null, $beneficiario = null)
	{


		$model = new Morbilidad_Model();
		$query = $model->listar_mobilidad($desde, $hasta, $medico, $especialidad, $sexo, $beneficiario);
		
		if (empty($query)) {
			$morbilidad = [];
		} else {
			$morbilidad = $query;
		}
		echo json_encode($morbilidad);
	}


	public function listar_mobilidad_entes($desde = null, $hasta = null, $medico = 0, $especialidad = 0, $sexo = null, $beneficiario = null, $entes_adscritos = 0)
	{


		if ($desde != 'null' and $hasta != 'null') {
			$date1 = explode('-', $desde);
			$desde = $date1[2] . "-" . $date1[1] . "-" . $date1[0];

			$date2 = explode('-', $hasta);
			$hasta = $date2[2] . "-" . $date2[1] . "-" . $date2[0];
		} else {
			$desde = 'null';
			$hasta = 'null';
		}

		$model = new Morbilidad_Model();
		$query = $model->listar_mobilidad_entes($desde, $hasta, $medico, $especialidad, $sexo, $beneficiario, $entes_adscritos);
		if (empty($query)) {
			$morbilidad = [];
		} else {
			$morbilidad = $query;
		}
		echo json_encode($morbilidad);
	}
}
