<?php

namespace App\Controllers;
use App\Models\Especialidad_Model;
class Home extends BaseController
{

// *****************************MENUS*********************************
    public function create()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('usuarios/create');
    }

    public function mantenimiento()
    {
       
        return view ('login/mantenimiento');
    }



    public function mensaje0()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('mensajes/mensaje0');
    }
  
    public function mensaje()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('mensajes/mensaje');
    }

    public function mensaje2()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('mensajes/mensaje2');
    }

    public function    datosuser()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('usuarios/vista_usuarios');
    }

    public function    edituser()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('usuarios/edituser');
    }

    public function supermenu()
    {   
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        
         echo view('menu/supermenu'); 
        // echo view('menu/footer.php'); 
    }
    public function imagenCentral()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('menu/imagenCentral');
    }

    public function    admin_template($data)
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('menu/admin_template');
    }
    public function   menu_administrador()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('menu/menu_administrador');
    }
    public function    menu_farmacia()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('menu/menu_farmacia');
    }
    public function    prueba()
    {
        if (!session('nombreUsuario')) {
            return redirect()->to(base_url().'/index.php'); 
        } 
        return view ('usuarios/prueba');
    }

    public function    prueba2()
    {
        // if (!session('nombreUsuario')) {
        //     return redirect()->to(base_url().'/index.php'); 
        // } 
        return view ('productos/prueba2');
    }

    public function   prueba3()
    {
        // if (!session('nombreUsuario')) {
        //     return redirect()->to(base_url().'/index.php'); 
        // } 
        return view ('productos/prueba3');
    }
// *****************************Grupo de Usuarios *********************************

public function   vistagrupos()
{
    if (!session('nombreUsuario')) {
        return redirect()->to(base_url().'/index.php'); 
    } 
    return view ('usuarios/vistagrupos');
}
public function  creargrupo()
{
    if (!session('nombreUsuario')) {
        return redirect()->to(base_url().'/index.php'); 
    } 
    return view ('usuarios/creargrupo');
}
public function ultima_fecha()
{
    if (!session('nombreUsuario')) {
        return redirect()->to(base_url().'/index.php'); 
    } 
    return view ('auditoria_user/ultima_fecha');
}
public function  editargrupo()
{
    if (!session('nombreUsuario')) {
        return redirect()->to(base_url().'/index.php'); 
    } 
    return view ('usuarios/editargrupo');
}
// ***************************Productos***************************
public function  registro_producto()
{
    if (!session('nombreUsuario')) {
        return redirect()->to(base_url().'/index.php'); 
    } 
    return view ('productos/registro');
}
}
