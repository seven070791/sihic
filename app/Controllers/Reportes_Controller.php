<?php

namespace App\Controllers;

use App\Models\Entrada_Model;
use App\Models\Salida_Model;
use App\Models\Medicamentos_model;
use App\Models\Beneficiarios_Model;
use App\Models\Consulta_Histotia_Model;
use App\Models\Enfermedad_Actual_Model;
use App\Models\HistorialModel;
use App\Models\Alergias_Medicamentos_Model;
use App\Models\Antecendentes_Patologicosf_Model;
use App\Models\Antecedentes_Quirurgicos_Model;
use App\Models\Antecendentes_Patologicosp_Model;
use App\Models\Examen_Fisico_Model;
use App\Models\Paraclinicos_Model;
use App\Models\Impresion_Diag_Model;
use App\Models\Morbilidad_Model;
use App\Models\Plan_Model;
use App\Models\Habitos_Model;
use App\Models\Antece_Gineco_Obstetrico_Model;
use App\Models\Medicamentos_patologias_Model;
use App\Models\Psicologia_Primaria_Model;
use App\Models\Partos_Model;
use App\Models\Cesarias_Model;
use App\Models\Abortos_Model;
use App\Models\Gestacion_Model;

use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Reportes_Controller extends BaseController
{
	use ResponseTrait;
	public function Vistareportes($id_medicamento)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Medicamentos_model();
		$query = $model->getDatosMedicamento($id_medicamento);
		if (empty($query->getResult())) {
			$infomedicamento = [];
		} else {
			foreach ($query->getResult() as $fila) {
				$infomedicamento['medicamento'] = $fila->medicamento;
				$infomedicamento['control']    = $fila->control;
				$infomedicamento['descripcion'] = $fila->descripcion;
				$infomedicamento['id']    = $fila->id;
			}
		}
		echo view('/reportes/reportes', $infomedicamento);
		echo view('/reportes/footer_Reportes');
	}



	public function PDF_Psicologia_Primaria($cedula, $n_historial, $tipobeneficiario, $usuario, $fecha_asistencia, $id_consulta, $id_historial_medico, $id_especialidad)
	{



		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Psicologia_Primaria($fecha_asistencia);
		$pdf->SetMargins(10, 10);
		$pdf->SetAutoPageBreak(true, 20);
		$model = new Beneficiarios_model();
		$model_medicamento = new Salida_Model();
		$modelo_signosvitales = new Consulta_Histotia_Model();
		$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
		$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
		$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
		$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
		$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
		$modelo_Examen_Fisico = new Examen_Fisico_Model();
		$modelo_Paraclinicos_Model = new Paraclinicos_Model();
		$modelo_Impresion_Diag = new Impresion_Diag_Model();
		$modelo_Plan_Model = new Plan_Model();
		$modelo_Habitos_Model = new Habitos_Model();
		$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
		$modelo_Psicologia_Primaria = new Psicologia_Primaria_Model();


		if ($tipobeneficiario == 'T') {
			$titular = $model->buscartitular_NotasEntregas($cedula);
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			// var_dump($signosvitales);
			// die();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$Psicologia_Primaria = $modelo_Psicologia_Primaria->listar_Psicologia_Primaria_Individual($n_historial);
			$Psicologia_Primaria_observacion = $modelo_Psicologia_Primaria->listar_Psicologia_Primaria_observacion($n_historial, $id_consulta);
			if (empty($titular)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					$contador = count($enfermedad_actual);
					$control = 0;
					$datos_historial['enfermedad_actual'] = '';
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$control++;
						if ($control < $contador) {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion . "\n";
						} else {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion;
						}
					}
				}
				$pdf->Ln(2);
				$pdf->Content_Psicologia_Primaria($datos_historial);
				$i = 0;
				foreach ($Psicologia_Primaria as $Psicologia_Primaria) {
					$id = $Psicologia_Primaria->id;
					$fecha_seguimiento = $Psicologia_Primaria->fecha_seguimiento;
					$evolucion = $Psicologia_Primaria->evolucion;
					$nombre = $Psicologia_Primaria->nombre;


					$pdf->Cell(18, 5, $id, 1, 0, 'L');
					$pdf->Cell(30, 5, $fecha_seguimiento, 1, 0, 'C');
					$pdf->Cell(110, 5, $evolucion, 1, 0, 'C');
					$pdf->Cell(42, 5, $nombre, 1, 1, 'C');

					$i++;
					if ($i == 10) {
						$pdf->AddPage();
						$pdf->Content_Psicologia_Primaria($datos_historial);
						$i = 0;
					}
				}
				/******************OBSERVACION PSICOLGIA*******************/
				if (empty($Psicologia_Primaria_observacion)) {
					$datos_observacion['Psicologia_Primaria_observacion']   = ' ';
				} else {
					foreach ($Psicologia_Primaria_observacion as $Psicologia_Primaria) {
						$datos_observacion['Psicologia_Primaria_observacion'] = $Psicologia_Primaria->observacion;
					}
				}
				$pdf->footer_Psicologia_Primaria($usuario, $datos_observacion);
				foreach ($titular as $titular) {
					$fecha_asistencia = $fecha_asistencia;
					$nombre = $titular->nombre;
					$apellido = $titular->apellido;
					$fecha_nacimiento = $titular->fecha_nacimiento;
					$cedula_trabajador = $titular->cedula_trabajador;
					$ubicacion_administrativa = $titular->ubicacion_administrativa;
					$edad_actual = $titular->edad_actual;
					$telefono = $titular->telefono;
					$sexo = $titular->sexo;
					$pdf->SetXY(28.2, 10.1);
					$pdf->Cell(80, 64, $fecha_asistencia, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->Cell(10, 3, '', 0, 1, 'L');
					$pdf->SetXY(176.2, 30.5);
					$pdf->Cell(30, 111, $sexo, 0, 2, 'L');
				}
			}

			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'F') {
			$familiar = $model->buscarfamiliar_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$modelo_Psicologia_Primaria = new Psicologia_Primaria_Model();
			$Psicologia_Primaria = $modelo_Psicologia_Primaria->listar_Psicologia_Primaria_Individual($n_historial);
			$Psicologia_Primaria_observacion = $modelo_Psicologia_Primaria->listar_Psicologia_Primaria_observacion($n_historial, $id_consulta);

			if (empty($familiar)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {


				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					$contador = count($enfermedad_actual);
					$control = 0;
					$datos_historial['enfermedad_actual'] = '';
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$control++;
						if ($control < $contador) {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion . "\n";
						} else {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion;
						}
					}
				}
				$pdf->Ln(2);
				$pdf->Content_Psicologia_Primaria($datos_historial);
				$i = 0;
				foreach ($Psicologia_Primaria as $Psicologia_Primaria) {
					$id = $Psicologia_Primaria->id;
					$fecha_seguimiento = $Psicologia_Primaria->fecha_seguimiento;
					$evolucion = $Psicologia_Primaria->evolucion;
					$nombre = $Psicologia_Primaria->nombre;


					$pdf->Cell(18, 5, $id, 1, 0, 'L');
					$pdf->Cell(30, 5, $fecha_seguimiento, 1, 0, 'C');
					$pdf->Cell(110, 5, $evolucion, 1, 0, 'C');
					$pdf->Cell(42, 5, $nombre, 1, 1, 'C');

					$i++;
					if ($i == 10) {
						$pdf->AddPage();
						$pdf->Content_Psicologia_Primaria($datos_historial);
						$i = 0;
					}
				}
				/******************OBSERVACION PSICOLGIA*******************/
				if (empty($Psicologia_Primaria_observacion)) {
					$datos_observacion['Psicologia_Primaria_observacion']   = ' ';
				} else {
					foreach ($Psicologia_Primaria_observacion as $Psicologia_Primaria) {
						$datos_observacion['Psicologia_Primaria_observacion'] = $Psicologia_Primaria->observacion;
					}
				}
				$pdf->footer_Psicologia_Primaria($usuario, $datos_observacion);
				foreach ($familiar as $familiar) {

					$fecha_asistencia = $fecha_asistencia;
					//***TITULAR*****		
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 62, $fecha_asistencia, 0, 1, 'L');
					$nombre = $familiar->nombre_titular;
					$apellido = $familiar->apellido_titular;
					$fecha_nacimiento = $familiar->fecha_nacimientot;
					$cedula_trabajador = $familiar->cedula_trabajador;
					$ubicacion_administrativa = $familiar->departamento;
					$edad_actual = $familiar->edad_actualt;
					$telefono = $familiar->telefonot;
					$sexot = $familiar->sexot;
					//****FAMILIAR***
					$nombreb = $familiar->nombre;
					$apellidob = $familiar->apellido;
					$fecha_nacimientob = $familiar->fecha_nac_familiares;
					$cedulab = $familiar->cedula;
					$edad_actualb = $familiar->edad_actual;
					$descripcionparentesco = $familiar->descripcionparentesco;
					$telefonob = $familiar->telefono;
					$sexof = $familiar->sexof;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****FAMILIAR***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 182, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 191, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexof, 0, 2, 'L');
				}
			}

			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'C') {
			$cortesia = $model->buscarcortesia_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$modelo_Psicologia_Primaria = new Psicologia_Primaria_Model();
			$Psicologia_Primaria = $modelo_Psicologia_Primaria->listar_Psicologia_Primaria_Individual($n_historial);
			$Psicologia_Primaria_observacion = $modelo_Psicologia_Primaria->listar_Psicologia_Primaria_observacion($n_historial, $id_consulta);
			if (empty($cortesia)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {

				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					$contador = count($enfermedad_actual);
					$control = 0;
					$datos_historial['enfermedad_actual'] = '';
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$control++;
						if ($control < $contador) {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion . "\n";
						} else {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion;
						}
					}
				}
				$pdf->Ln(2);
				$pdf->Content_Psicologia_Primaria($datos_historial);
				$i = 0;
				foreach ($Psicologia_Primaria as $Psicologia_Primaria) {
					$id = $Psicologia_Primaria->id;
					$fecha_seguimiento = $Psicologia_Primaria->fecha_seguimiento;
					$evolucion = $Psicologia_Primaria->evolucion;
					$nombre = $Psicologia_Primaria->nombre;


					$pdf->Cell(18, 5, $id, 1, 0, 'L');
					$pdf->Cell(30, 5, $fecha_seguimiento, 1, 0, 'C');
					$pdf->Cell(110, 5, $evolucion, 1, 0, 'C');
					$pdf->Cell(42, 5, $nombre, 1, 1, 'C');

					$i++;
					if ($i == 10) {
						$pdf->AddPage();
						$pdf->Content_Psicologia_Primaria($datos_historial);
						$i = 0;
					}
				}
				/******************OBSERVACION PSICOLGIA*******************/
				if (empty($Psicologia_Primaria_observacion)) {
					$datos_observacion['Psicologia_Primaria_observacion']   = ' ';
				} else {
					foreach ($Psicologia_Primaria_observacion as $Psicologia_Primaria) {
						$datos_observacion['Psicologia_Primaria_observacion'] = $Psicologia_Primaria->observacion;
					}
				}
				$pdf->footer_Psicologia_Primaria($usuario, $datos_observacion);
				foreach ($cortesia as $cortesia) {
					// ***TITULAR*****	
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 63, $fecha_asistencia, 0, 1, 'L');
					$nombre = $cortesia->nombre_titular;
					$apellido = $cortesia->apellido_titular;
					$fecha_nacimiento = $cortesia->fecha_nacimientot;
					$cedula_trabajador = $cortesia->cedula_trabajador;
					$ubicacion_administrativa = $cortesia->departamento;
					$edad_actual = $cortesia->edad_actualt;
					$telefono = $cortesia->telefonot;
					$sexot = $cortesia->sexot;
					//****cortesia***
					$nombreb = $cortesia->nombre;
					$apellidob = $cortesia->apellido;
					$fecha_nacimientob = $cortesia->fecha_nac_cortesia;
					$cedulab = $cortesia->cedula;
					$edad_actualb = $cortesia->edad_actual;
					$descripcionparentesco = 'CORTESIA';
					$telefonob = $cortesia->telefono;
					$sexo = $cortesia->sexo;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****CORTESIA***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 183, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 199, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexo, 0, 1, 'L');
				}
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		}
	}






	public function PDF_Medicina_Interna($cedula, $n_historial, $tipobeneficiario, $usuario, $fecha_asistencia, $id_consulta, $id_historial_medico, $id_especialidad)
	{



		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Medicina_Interna($fecha_asistencia);
		$pdf->SetMargins(10, 10);
		$pdf->SetAutoPageBreak(true, 20);
		$model = new Beneficiarios_model();
		$model_medicamento = new Salida_Model();
		$modelo_signosvitales = new Consulta_Histotia_Model();
		$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
		$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
		$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
		$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
		$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
		$modelo_Examen_Fisico = new Examen_Fisico_Model();
		$modelo_Paraclinicos_Model = new Paraclinicos_Model();
		$modelo_Impresion_Diag = new Impresion_Diag_Model();
		$modelo_Plan_Model = new Plan_Model();
		$modelo_Habitos_Model = new Habitos_Model();
		$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
		if ($tipobeneficiario == 'T') {
			$titular = $model->buscartitular_NotasEntregas($cedula);
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			// var_dump($signosvitales);
			// die();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$tipo_de_sangre = $model->getAll_tipodesangre($cedula);

			if (empty($titular)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($titular as $titular) {
					$fecha_asistencia = $fecha_asistencia;
					$nombre = $titular->nombre;
					$apellido = $titular->apellido;
					$fecha_nacimiento = $titular->fecha_nacimiento;
					$cedula_trabajador = $titular->cedula_trabajador;
					$ubicacion_administrativa = $titular->ubicacion_administrativa;
					$edad_actual = $titular->edad_actual;
					$telefono = $titular->telefono;
					$sexo = $titular->sexo;
					$pdf->SetXY(28.2, 10.1);
					$pdf->Cell(80, 64, $fecha_asistencia, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->Cell(10, 3, '', 0, 1, 'L');
					$pdf->SetXY(176.2, 30.5);
					$pdf->Cell(30, 111, $sexo, 0, 2, 'L');
				}
				/******************SIGNOS VIRTALES*******************/
				foreach ($signosvitales as $signosvitales) {
					$datos['peso']   = $signosvitales->peso;
					$datos['talla']   = $signosvitales->talla;
					$datos['spo2']   = $signosvitales->spo2;
					$datos['frecuencia_c']   = $signosvitales->frecuencia_c;
					$datos['frecuencia_r']   = $signosvitales->frecuencia_r;
					$datos['temperatura']   = $signosvitales->temperatura;
					$datos['tension_alta']   = $signosvitales->tension_alta;
					$datos['tension_baja']   = $signosvitales->tension_baja;
					$datos['imc']   = $signosvitales->imc;
					$datos['motivo_consulta']   = $signosvitales->motivo_consulta;
				}

				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					$contador = count($enfermedad_actual);
					$control = 0;
					$datos_historial['enfermedad_actual'] = '';
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$control++;
						if ($control < $contador) {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion . "\n";
						} else {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion;
						}
					}
				}
				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}
				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}

				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}

				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}


				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}


				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}


				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($tipo_de_sangre)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($tipo_de_sangre as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}
				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}


				//$pdf->Conten_medicinageneral_Parte2($datos_historial);
				$pdf->Content_Medicina_Interna($datos, $datos_historial);
				$pdf->footer_Medicina_Interna($usuario);
			}


			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'F') {
			$familiar = $model->buscarfamiliar_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$query = $model->getFamiliar($cedula);
			if (empty($familiar)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($familiar as $familiar) {

					$fecha_asistencia = $fecha_asistencia;
					//***TITULAR*****		
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 62, $fecha_asistencia, 0, 1, 'L');
					$nombre = $familiar->nombre_titular;
					$apellido = $familiar->apellido_titular;
					$fecha_nacimiento = $familiar->fecha_nacimientot;
					$cedula_trabajador = $familiar->cedula_trabajador;
					$ubicacion_administrativa = $familiar->departamento;
					$edad_actual = $familiar->edad_actualt;
					$telefono = $familiar->telefonot;
					$sexot = $familiar->sexot;
					//****FAMILIAR***
					$nombreb = $familiar->nombre;
					$apellidob = $familiar->apellido;
					$fecha_nacimientob = $familiar->fecha_nac_familiares;
					$cedulab = $familiar->cedula;
					$edad_actualb = $familiar->edad_actual;
					$descripcionparentesco = $familiar->descripcionparentesco;
					$telefonob = $familiar->telefono;
					$sexof = $familiar->sexof;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****FAMILIAR***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 182, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 191, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexof, 0, 1, 'L');
				}

				///*******************SIGNOS VITALES*************
				foreach ($signosvitales as $signos) {
					$datos['peso']         = $signos->peso;
					$datos['talla']        = $signos->talla;
					$datos['spo2']         = $signos->spo2;
					$datos['frecuencia_c'] = $signos->frecuencia_c;
					$datos['frecuencia_r'] = $signos->frecuencia_r;
					$datos['temperatura']  = $signos->temperatura;
					$datos['tension_alta'] = $signos->tension_alta;
					$datos['tension_baja'] = $signos->tension_baja;
					$datos['fecha_creacion'] = $signos->fecha_creacion;
					$datos['motivo_consulta'] = $signos->motivo_consulta;
					$datos['imc'] = $signos->imc;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$datos_historial['enfermedad_actual']   = $enfermedad_actual->descripcion;
					}
				}

				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}

				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}


				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}


				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}

				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}

				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}

				//************TIPO DE SANGRE*******
				if (empty($query)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($query as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}
				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}


				$pdf->Content_Medicina_Interna($datos, $datos_historial);
				$pdf->footer_Medicina_Interna($usuario);
			}

			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'C') {
			$cortesia = $model->buscarcortesia_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$query = $model->getCortersia($cedula);
			if (empty($cortesia)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {

				foreach ($cortesia as $cortesia) {
					// ***TITULAR*****	
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 63, $fecha_asistencia, 0, 1, 'L');
					$nombre = $cortesia->nombre_titular;
					$apellido = $cortesia->apellido_titular;
					$fecha_nacimiento = $cortesia->fecha_nacimientot;
					$cedula_trabajador = $cortesia->cedula_trabajador;
					$ubicacion_administrativa = $cortesia->departamento;
					$edad_actual = $cortesia->edad_actualt;
					$telefono = $cortesia->telefonot;
					$sexot = $cortesia->sexot;
					//****cortesia***
					$nombreb = $cortesia->nombre;
					$apellidob = $cortesia->apellido;
					$fecha_nacimientob = $cortesia->fecha_nac_cortesia;
					$cedulab = $cortesia->cedula;
					$edad_actualb = $cortesia->edad_actual;
					$descripcionparentesco = 'CORTESIA';
					$telefonob = $cortesia->telefono;
					$sexo = $cortesia->sexo;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****CORTESIA***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 183, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 199, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexo, 0, 1, 'L');
				}

				///*******************SIGNOS VITALES*************
				foreach ($signosvitales as $signos) {
					$datos['peso']         = $signos->peso;
					$datos['talla']        = $signos->talla;
					$datos['spo2']         = $signos->spo2;
					$datos['frecuencia_c'] = $signos->frecuencia_c;
					$datos['frecuencia_r'] = $signos->frecuencia_r;
					$datos['temperatura']  = $signos->temperatura;
					$datos['tension_alta'] = $signos->tension_alta;
					$datos['tension_baja'] = $signos->tension_baja;
					$datos['fecha_creacion'] = $signos->fecha_creacion;
					$datos['motivo_consulta'] = $signos->motivo_consulta;
					$datos['imc'] = $signos->imc;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$datos_historial['enfermedad_actual']   = $enfermedad_actual->descripcion;
					}
				}
				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}
				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}


				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}
				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}


				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}


				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}
				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($query)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($query as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}
				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}

				$pdf->Content_Medicina_Interna($datos, $datos_historial);
				$pdf->footer_Medicina_Interna($usuario);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		}
	}




	public function PDF_Medicina_General($cedula, $n_historial, $tipobeneficiario, $usuario, $fecha_asistencia, $id_consulta, $id_historial_medico)
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Medicina_General($fecha_asistencia);
		$pdf->SetMargins(10, 10);
		$pdf->SetAutoPageBreak(true, 10);
		$model = new Beneficiarios_model();
		$model_medicamento = new Salida_Model();
		$modelo_signosvitales = new Consulta_Histotia_Model();
		$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
		$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
		$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
		$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
		$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
		$modelo_Examen_Fisico = new Examen_Fisico_Model();
		$modelo_Paraclinicos_Model = new Paraclinicos_Model();
		$modelo_Impresion_Diag = new Impresion_Diag_Model();
		$modelo_Plan_Model = new Plan_Model();
		$modelo_Habitos_Model = new Habitos_Model();
		if ($tipobeneficiario == 'T') {
			$titular = $model->buscartitular_NotasEntregas($cedula);
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$tipo_de_sangre = $model->getAll_tipodesangre($cedula);


			if (empty($titular)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($titular as $titular) {
					$fecha_asistencia = $fecha_asistencia;
					$nombre = $titular->nombre;
					$apellido = $titular->apellido;
					$fecha_nacimiento = $titular->fecha_nacimiento;
					$cedula_trabajador = $titular->cedula_trabajador;
					$ubicacion_administrativa = $titular->ubicacion_administrativa;
					$edad_actual = $titular->edad_actual;
					$telefono = $titular->telefono;
					$sexo = $titular->sexo;
					$pdf->SetXY(28.2, 10.1);
					$pdf->Cell(80, 64, $fecha_asistencia, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->Cell(10, 3, '', 0, 1, 'L');
					$pdf->SetXY(176.2, 30.5);
					$pdf->Cell(30, 111, $sexo, 0, 2, 'L');
				}
				/******************SIGNOS VIRTALES*******************/
				foreach ($signosvitales as $signosvitales) {
					$datos['peso']   = $signosvitales->peso;
					$datos['talla']   = $signosvitales->talla;
					$datos['spo2']   = $signosvitales->spo2;
					$datos['frecuencia_c']   = $signosvitales->frecuencia_c;
					$datos['frecuencia_r']   = $signosvitales->frecuencia_r;
					$datos['temperatura']   = $signosvitales->temperatura;
					$datos['tension_alta']   = $signosvitales->tension_alta;
					$datos['tension_baja']   = $signosvitales->tension_baja;
					$datos['imc']   = $signosvitales->imc;
					$datos['motivo_consulta']   = $signosvitales->motivo_consulta;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					$contador = count($enfermedad_actual);
					$control = 0;
					$datos_historial['enfermedad_actual'] = '';
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$control++;
						if ($control < $contador) {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion . "\n";
						} else {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion;
						}
					}
				}
				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}
				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' .  $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}


				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}

				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}


				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}


				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($tipo_de_sangre)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($tipo_de_sangre as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}



				//$pdf->Conten_medicinageneral_Parte2($datos_historial);

			}
			$pdf->Conten_medicinageneral($datos, $datos_historial);
			$pdf->footer_Medicina_General($usuario);
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'F') {
			$familiar = $model->buscarfamiliar_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$query = $model->getFamiliar($cedula);
			if (empty($familiar)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($familiar as $familiar) {

					$fecha_asistencia = $fecha_asistencia;
					//***TITULAR*****		
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 62, $fecha_asistencia, 0, 1, 'L');
					$nombre = $familiar->nombre_titular;
					$apellido = $familiar->apellido_titular;
					$fecha_nacimiento = $familiar->fecha_nacimientot;
					$cedula_trabajador = $familiar->cedula_trabajador;
					$ubicacion_administrativa = $familiar->departamento;
					$edad_actual = $familiar->edad_actualt;
					$telefono = $familiar->telefonot;
					$sexot = $familiar->sexot;
					//****FAMILIAR***
					$nombreb = $familiar->nombre;
					$apellidob = $familiar->apellido;
					$fecha_nacimientob = $familiar->fecha_nac_familiares;
					$cedulab = $familiar->cedula;
					$edad_actualb = $familiar->edad_actual;
					$descripcionparentesco = $familiar->descripcionparentesco;
					$telefonob = $familiar->telefono;
					$sexof = $familiar->sexof;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****FAMILIAR***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 182, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 191, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexof, 0, 1, 'L');
				}

				///*******************SIGNOS VITALES*************
				foreach ($signosvitales as $signos) {
					$datos['peso']         = $signos->peso;
					$datos['talla']        = $signos->talla;
					$datos['spo2']         = $signos->spo2;
					$datos['frecuencia_c'] = $signos->frecuencia_c;
					$datos['frecuencia_r'] = $signos->frecuencia_r;
					$datos['temperatura']  = $signos->temperatura;
					$datos['tension_alta'] = $signos->tension_alta;
					$datos['tension_baja'] = $signos->tension_baja;
					$datos['fecha_creacion'] = $signos->fecha_creacion;
					$datos['motivo_consulta'] = $signos->motivo_consulta;
					$datos['imc'] = $signos->imc;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$datos_historial['enfermedad_actual']   = $enfermedad_actual->descripcion;
					}
				}

				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}

				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}


				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}


				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}

				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}

				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($query)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($query as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}


				$pdf->Conten_medicinageneral($datos, $datos_historial);
				$pdf->footer_Medicina_General($usuario);
			}

			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'C') {
			$cortesia = $model->buscarcortesia_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$query = $model->getCortersia($cedula);
			if (empty($cortesia)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {

				foreach ($cortesia as $cortesia) {
					// ***TITULAR*****	
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 63, $fecha_asistencia, 0, 1, 'L');
					$nombre = $cortesia->nombre_titular;
					$apellido = $cortesia->apellido_titular;
					$fecha_nacimiento = $cortesia->fecha_nacimientot;
					$cedula_trabajador = $cortesia->cedula_trabajador;
					$ubicacion_administrativa = $cortesia->departamento;
					$edad_actual = $cortesia->edad_actualt;
					$telefono = $cortesia->telefonot;
					$sexot = $cortesia->sexot;
					//****cortesia***
					$nombreb = $cortesia->nombre;
					$apellidob = $cortesia->apellido;
					$fecha_nacimientob = $cortesia->fecha_nac_cortesia;
					$cedulab = $cortesia->cedula;
					$edad_actualb = $cortesia->edad_actual;
					$descripcionparentesco = 'CORTESIA';
					$telefonob = $cortesia->telefono;
					$sexo = $cortesia->sexo;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****CORTESIA***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 183, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 199, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexo, 0, 1, 'L');
				}

				///*******************SIGNOS VITALES*************
				foreach ($signosvitales as $signos) {
					$datos['peso']         = $signos->peso;
					$datos['talla']        = $signos->talla;
					$datos['spo2']         = $signos->spo2;
					$datos['frecuencia_c'] = $signos->frecuencia_c;
					$datos['frecuencia_r'] = $signos->frecuencia_r;
					$datos['temperatura']  = $signos->temperatura;
					$datos['tension_alta'] = $signos->tension_alta;
					$datos['tension_baja'] = $signos->tension_baja;
					$datos['fecha_creacion'] = $signos->fecha_creacion;
					$datos['motivo_consulta'] = $signos->motivo_consulta;
					$datos['imc'] = $signos->imc;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$datos_historial['enfermedad_actual']   = $enfermedad_actual->descripcion;
					}
				}
				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}
				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}
				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}

				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}


				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}
				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($query)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($query as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}

				$pdf->Conten_medicinageneral($datos, $datos_historial);
				$pdf->footer_Medicina_General($usuario);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		}
	}



	public function PDF_Paciente_Pediatrico($cedula, $n_historial, $tipobeneficiario, $usuario, $fecha_asistencia, $id_consulta, $id_historial_medico, $id_especialidad)
	{



		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Paciente_Pediatrico($fecha_asistencia);
		$pdf->SetMargins(10, 10);
		$pdf->SetAutoPageBreak(true, 30);
		$model = new Beneficiarios_model();
		$model_medicamento = new Salida_Model();
		$modelo_signosvitales = new Consulta_Histotia_Model();
		$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
		$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
		$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
		$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
		$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
		$modelo_Examen_Fisico = new Examen_Fisico_Model();
		$modelo_Paraclinicos_Model = new Paraclinicos_Model();
		$modelo_Impresion_Diag = new Impresion_Diag_Model();
		$modelo_Plan_Model = new Plan_Model();
		$modelo_Habitos_Model = new Habitos_Model();
		$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
		if ($tipobeneficiario == 'T') {
			$titular = $model->buscartitular_NotasEntregas($cedula);
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);

			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);

			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$tipo_de_sangre = $model->getAll_tipodesangre($cedula);

			if (empty($titular)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($titular as $titular) {
					$fecha_asistencia = $fecha_asistencia;
					$nombre = $titular->nombre;
					$apellido = $titular->apellido;
					$fecha_nacimiento = $titular->fecha_nacimiento;
					$cedula_trabajador = $titular->cedula_trabajador;
					$ubicacion_administrativa = $titular->ubicacion_administrativa;
					$edad_actual = $titular->edad_actual;
					$telefono = $titular->telefono;
					$sexo = $titular->sexo;
					$pdf->SetXY(28.2, 10.1);
					$pdf->Cell(80, 64, $fecha_asistencia, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->Cell(10, 3, '', 0, 1, 'L');
					$pdf->SetXY(176.2, 30.5);
					$pdf->Cell(30, 111, $sexo, 0, 2, 'L');
				}
				/******************SIGNOS VIRTALES*******************/
				foreach ($signosvitales as $signosvitales) {
					$datos['peso']   = $signosvitales->peso;
					$datos['talla']   = $signosvitales->talla;
					$datos['spo2']   = $signosvitales->spo2;
					$datos['frecuencia_c']   = $signosvitales->frecuencia_c;
					$datos['frecuencia_r']   = $signosvitales->frecuencia_r;
					$datos['temperatura']   = $signosvitales->temperatura;
					$datos['tension_alta']   = $signosvitales->tension_alta;
					$datos['tension_baja']   = $signosvitales->tension_baja;
					$datos['imc']   = $signosvitales->imc;
					$datos['motivo_consulta']   = $signosvitales->motivo_consulta;
				}

				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					$contador = count($enfermedad_actual);
					$control = 0;
					$datos_historial['enfermedad_actual'] = '';
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$control++;
						if ($control < $contador) {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion . "\n";
						} else {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion;
						}
					}
				}
				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}
				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}

				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}

				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}


				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}


				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}


				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($tipo_de_sangre)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($tipo_de_sangre as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}
				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}


				//$pdf->Conten_medicinageneral_Parte2($datos_historial);
				$pdf->Content_Paciente_Pediatrico($datos, $datos_historial);
				$pdf->footer_Paciente_Pediatrico($usuario);
			}


			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'F') {
			$familiar = $model->buscarfamiliar_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$query = $model->getFamiliar($cedula);
			if (empty($familiar)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($familiar as $familiar) {

					$fecha_asistencia = $fecha_asistencia;
					//***TITULAR*****		
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 62, $fecha_asistencia, 0, 1, 'L');
					$nombre = $familiar->nombre_titular;
					$apellido = $familiar->apellido_titular;
					$fecha_nacimiento = $familiar->fecha_nacimientot;
					$cedula_trabajador = $familiar->cedula_trabajador;
					$ubicacion_administrativa = $familiar->departamento;
					$edad_actual = $familiar->edad_actualt;
					$telefono = $familiar->telefonot;
					$sexot = $familiar->sexot;
					//****FAMILIAR***
					$nombreb = $familiar->nombre;
					$apellidob = $familiar->apellido;
					$fecha_nacimientob = $familiar->fecha_nac_familiares;
					$cedulab = $familiar->cedula;
					$edad_actualb = $familiar->edad_actual;
					$descripcionparentesco = $familiar->descripcionparentesco;
					$telefonob = $familiar->telefono;
					$sexof = $familiar->sexof;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****FAMILIAR***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 182, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 191, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexof, 0, 1, 'L');
				}

				///*******************SIGNOS VITALES*************
				foreach ($signosvitales as $signos) {
					$datos['peso']         = $signos->peso;
					$datos['talla']        = $signos->talla;
					$datos['spo2']         = $signos->spo2;
					$datos['frecuencia_c'] = $signos->frecuencia_c;
					$datos['frecuencia_r'] = $signos->frecuencia_r;
					$datos['temperatura']  = $signos->temperatura;
					$datos['tension_alta'] = $signos->tension_alta;
					$datos['tension_baja'] = $signos->tension_baja;
					$datos['fecha_creacion'] = $signos->fecha_creacion;
					$datos['motivo_consulta'] = $signos->motivo_consulta;
					$datos['imc'] = $signos->imc;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$datos_historial['enfermedad_actual']   = $enfermedad_actual->descripcion;
					}
				}

				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}

				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}


				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}


				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}

				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}

				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($query)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($query as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}

				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}


				$pdf->Content_Paciente_Pediatrico($datos, $datos_historial);
				$pdf->footer_Paciente_Pediatrico($usuario);
			}

			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'C') {
			$cortesia = $model->buscarcortesia_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$query = $model->getCortersia($cedula);
			if (empty($cortesia)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {

				foreach ($cortesia as $cortesia) {
					// ***TITULAR*****	
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 63, $fecha_asistencia, 0, 1, 'L');
					$nombre = $cortesia->nombre_titular;
					$apellido = $cortesia->apellido_titular;
					$fecha_nacimiento = $cortesia->fecha_nacimientot;
					$cedula_trabajador = $cortesia->cedula_trabajador;
					$ubicacion_administrativa = $cortesia->departamento;
					$edad_actual = $cortesia->edad_actualt;
					$telefono = $cortesia->telefonot;
					$sexot = $cortesia->sexot;
					//****cortesia***
					$nombreb = $cortesia->nombre;
					$apellidob = $cortesia->apellido;
					$fecha_nacimientob = $cortesia->fecha_nac_cortesia;
					$cedulab = $cortesia->cedula;
					$edad_actualb = $cortesia->edad_actual;
					$descripcionparentesco = 'CORTESIA';
					$telefonob = $cortesia->telefono;
					$sexo = $cortesia->sexo;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****CORTESIA***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 183, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 199, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexo, 0, 1, 'L');
				}

				///*******************SIGNOS VITALES*************
				foreach ($signosvitales as $signos) {
					$datos['peso']         = $signos->peso;
					$datos['talla']        = $signos->talla;
					$datos['spo2']         = $signos->spo2;
					$datos['frecuencia_c'] = $signos->frecuencia_c;
					$datos['frecuencia_r'] = $signos->frecuencia_r;
					$datos['temperatura']  = $signos->temperatura;
					$datos['tension_alta'] = $signos->tension_alta;
					$datos['tension_baja'] = $signos->tension_baja;
					$datos['fecha_creacion'] = $signos->fecha_creacion;
					$datos['motivo_consulta'] = $signos->motivo_consulta;
					$datos['imc'] = $signos->imc;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$datos_historial['enfermedad_actual']   = $enfermedad_actual->descripcion;
					}
				}
				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}
				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}
				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}

				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}


				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}
				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($query)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($query as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}

				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}

				$pdf->Content_Paciente_Pediatrico($datos, $datos_historial);
				$pdf->footer_Paciente_Pediatrico($usuario);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		}
	}


	public function PDF_Ginecologia_Obtertricia($cedula, $n_historial, $tipobeneficiario, $usuario, $fecha_asistencia, $id_consulta, $id_historial_medico, $id_especialidad)
	{



		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$pdf = new \FPDF('P', 'mm', 'letter');
		$pdf->AddPage();
		$pdf->Header_Ginecologia_Obtertricia($fecha_asistencia);
		$pdf->SetMargins(10, 10);
		$pdf->SetAutoPageBreak(true, 20);
		$model = new Beneficiarios_model();
		$model_medicamento = new Salida_Model();
		$modelo_signosvitales = new Consulta_Histotia_Model();
		$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
		$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
		$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
		$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
		$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
		$modelo_Examen_Fisico = new Examen_Fisico_Model();
		$modelo_Paraclinicos_Model = new Paraclinicos_Model();
		$modelo_Impresion_Diag = new Impresion_Diag_Model();
		$modelo_Plan_Model = new Plan_Model();
		$modelo_Habitos_Model = new Habitos_Model();
		$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
		$modelo_atc_gineco_obste = new  Antece_Gineco_Obstetrico_Model();
		$modelo_abortos = new  Abortos_Model();
		$modelo_partos = new  Partos_Model();
		$modelo_cesarias = new  Cesarias_Model();
		$modelo_gestacion = new  Gestacion_Model();
		$query_abortos = $modelo_abortos->listar_abortos($n_historial, $id_consulta);
		$query_partos = $modelo_partos->listar_partos($n_historial, $id_consulta);
		$query_cesarias = $modelo_cesarias->listar_cesarias($n_historial, $id_consulta);
		$query_gestacion = $modelo_gestacion->listar_gestacion($n_historial, $id_consulta);
		if ($tipobeneficiario == 'T') {
			$titular = $model->buscartitular_NotasEntregas($cedula);
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$atc_gineco_obste = $modelo_atc_gineco_obste->listar_Antece_Gineco_obtetrico($n_historial, $id_consulta);
			$tipo_de_sangre = $model->getAll_tipodesangre($cedula);
			$info_titular = $model->getAll($cedula);

			if (empty($titular)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($titular as $titular) {
					$fecha_asistencia = $fecha_asistencia;
					$nombre = $titular->nombre;
					$apellido = $titular->apellido;
					$fecha_nacimiento = $titular->fecha_nacimiento;
					$cedula_trabajador = $titular->cedula_trabajador;
					$ubicacion_administrativa = $titular->ubicacion_administrativa;
					$edad_actual = $titular->edad_actual;
					$telefono = $titular->telefono;
					$sexo = $titular->sexo;
					$pdf->SetXY(28.2, 10.1);
					$pdf->Cell(80, 64, $fecha_asistencia, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->Cell(10, 3, '', 0, 1, 'L');
					$pdf->SetXY(176.2, 30.5);
					$pdf->Cell(30, 111, $sexo, 0, 2, 'L');
				}


				/******************SIGNOS VIRTALES*******************/
				foreach ($signosvitales as $signosvitales) {
					$datos['peso']   = $signosvitales->peso;
					$datos['talla']   = $signosvitales->talla;
					$datos['spo2']   = $signosvitales->spo2;
					$datos['frecuencia_c']   = $signosvitales->frecuencia_c;
					$datos['frecuencia_r']   = $signosvitales->frecuencia_r;
					$datos['temperatura']   = $signosvitales->temperatura;
					$datos['tension_alta']   = $signosvitales->tension_alta;
					$datos['tension_baja']   = $signosvitales->tension_baja;
					$datos['imc']   = $signosvitales->imc;
					$datos['motivo_consulta']   = $signosvitales->motivo_consulta;
				}

				/******************OCUPACION/ESTADO CIVIL/GRADO DE INSTRUCION*******************/
				if (empty($info_titular)) {
					$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
				} else {
					foreach ($info_titular as $fila) {

						$datos['descrip_estado_civil'] = $fila->descrip_estado_civil;
						$datos['descrip_grado_intruccion'] = $fila->descrip_grado_intruccion;
						$datos['descrip_ocupacion'] = $fila->descrip_ocupacion;
					}
				}

				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					$contador = count($enfermedad_actual);
					$control = 0;
					$datos_historial['enfermedad_actual'] = '';
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$control++;
						if ($control < $contador) {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion . "\n";
						} else {
							$datos_historial['enfermedad_actual'] = $datos_historial['enfermedad_actual'] . '*' . ' ' . $enfermedad_actual->descripcion;
						}
					}
				}
				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}
				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}

				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}


				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}


				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}


				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}

				//************TIPO DE SANGRE*******
				if (empty($tipo_de_sangre)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($tipo_de_sangre as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}
				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}



				//************PARTOS /CESARIAS/ABORTOS/GESTACION*******

				if (empty($query_gestacion)) {
					$datos_historial['gestacion']    = 'N/A';
				} else {
					foreach ($query_gestacion as $fila) {
						$datos_historial['gestacion'] = $fila->descri_gestacion;
					}
				}
				if (empty($query_partos)) {
					$datos_historial['partos']     = 'N/A';
				} else {

					foreach ($query_partos as $fila) {
						$datos_historial['partos'] = $fila->descri_partos;
					}
				}

				if (empty($query_cesarias)) {
					$datos_historial['cesarias']      = 'N/A';
				} else {
					foreach ($query_cesarias as $fila) {
						$datos_historial['cesarias'] = $fila->descri_cesarias;
					}
				}
				if (empty($query_abortos)) {
					$datos_historial['abortos']      = 'N/A';
				} else {
					foreach ($query_abortos as $fila) {
						$datos_historial['abortos'] = $fila->descri_abortos;
					}
				}




				//************HISTORIA Y ANTECEDENTES GINECO OBSTERTRICO*******
				if (empty($atc_gineco_obste)) {
					$datos_historial['edad_gestacional']  = 'N/A';
					$datos_historial['f_u_r']  = 'N/A';
					$datos_historial['detalle_historia_obst']   = 'N/A';
					$datos_historial['menarquia']  = ' ';
					$datos_historial['sexarquia']  = ' ';
					$datos_historial['nps']   = ' ';
					$datos_historial['ciclo_mestrual']   = ' ';
					$datos_historial['descr_anticon']   = ' ';
					$datos_historial['detalles_anticon']   = ' ';
					$datos_historial['descr_ets']   = ' ';
					$datos_historial['descrip_tipo_ets']   = ' ';
					$datos_historial['citologia']   = ' ';
					$datos_historial['eco_mamario']   = ' ';
					$datos_historial['desintometria']   = ' ';
					$datos_historial['mamografia']   = ' ';
					$datos_historial['descr_dismenorrea']   = ' ';
					$datos_historial['descr_eumenorrea']   = ' ';

					$datos_historial['diu']   = ' ';
					$datos_historial['t_cobre']   = ' ';
					$datos_historial['mirena']   = ' ';
					$datos_historial['aspiral']   = ' ';
					$datos_historial['i_subdermico']   = ' ';
					$datos_historial['otro']   = ' ';
					$datos_historial['t_colocacion']   = ' ';
				} else {
					foreach ($atc_gineco_obste as $fila) {
						$datos_historial['edad_gestacional'] = $fila->edad_gestacional;
						$datos_historial['f_u_r'] = $fila->f_u_r;
						$datos_historial['detalle_historia_obst'] = $fila->detalle_historia_obst;
						$datos_historial['menarquia'] = $fila->menarquia;
						$datos_historial['sexarquia'] = $fila->sexarquia;
						$datos_historial['nps'] = $fila->nps;
						$datos_historial['ciclo_mestrual'] = $fila->ciclo_mestrual;
						$datos_historial['descr_anticon'] = $fila->descr_anticon;
						$datos_historial['detalles_anticon'] = $fila->detalles_anticon;
						$datos_historial['descr_ets'] = $fila->descr_ets;
						$datos_historial['descrip_tipo_ets'] = $fila->descrip_tipo_ets;
						$datos_historial['citologia'] = $fila->citologia;
						$datos_historial['eco_mamario'] = $fila->eco_mamario;
						$datos_historial['desintometria'] = $fila->desintometria;
						$datos_historial['mamografia'] = $fila->mamografia;
						$datos_historial['descr_dismenorrea'] = $fila->descr_dismenorrea;
						$datos_historial['descr_eumenorrea'] = $fila->descr_eumenorrea;

						$datos_historial['diu'] = $fila->diu;
						$datos_historial['t_cobre'] = $fila->t_cobre;
						$datos_historial['mirena'] = $fila->mirena;
						$datos_historial['aspiral'] = $fila->aspiral;
						$datos_historial['i_subdermico'] = $fila->i_subdermico;
						$datos_historial['otro'] = $fila->otro;
						$datos_historial['t_colocacion'] = $fila->t_colocacion;
					}
				}

				//   var_dump($atc_gineco_obste);
				//   die();


				//$pdf->Conten_medicinageneral_Parte2($datos_historial);
				$pdf->Content_Ginecologia_Obtertricia($datos, $datos_historial, $Habitos_Model);
				$pdf->footer_Ginecologia_Obtertricia($usuario);
			}


			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'F') {
			$familiar = $model->buscarfamiliar_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$query = $model->getFamiliar($cedula);
			$info_familiares = $model->getAllInfoMedicamentosFamiliares($cedula);
			$modelo_atc_gineco_obste = new  Antece_Gineco_Obstetrico_Model();
			$atc_gineco_obste = $modelo_atc_gineco_obste->listar_Antece_Gineco_obtetrico($n_historial, $id_consulta);
			$modelo_abortos = new  Abortos_Model();
			$modelo_partos = new  Partos_Model();
			$modelo_cesarias = new  Cesarias_Model();
			$modelo_gestacion = new  Gestacion_Model();
			$query_abortos = $modelo_abortos->listar_abortos($n_historial, $id_consulta);
			$query_partos = $modelo_partos->listar_partos($n_historial, $id_consulta);
			$query_cesarias = $modelo_cesarias->listar_cesarias($n_historial, $id_consulta);
			$query_gestacion = $modelo_gestacion->listar_gestacion($n_historial, $id_consulta);
			if (empty($familiar)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {
				foreach ($familiar as $familiar) {

					$fecha_asistencia = $fecha_asistencia;
					//***TITULAR*****		
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 62, $fecha_asistencia, 0, 1, 'L');
					$nombre = $familiar->nombre_titular;
					$apellido = $familiar->apellido_titular;
					$fecha_nacimiento = $familiar->fecha_nacimientot;
					$cedula_trabajador = $familiar->cedula_trabajador;
					$ubicacion_administrativa = $familiar->departamento;
					$edad_actual = $familiar->edad_actualt;
					$telefono = $familiar->telefonot;
					$sexot = $familiar->sexot;
					//****FAMILIAR***
					$nombreb = $familiar->nombre;
					$apellidob = $familiar->apellido;
					$fecha_nacimientob = $familiar->fecha_nac_familiares;
					$cedulab = $familiar->cedula;
					$edad_actualb = $familiar->edad_actual;
					$descripcionparentesco = $familiar->descripcionparentesco;
					$telefonob = $familiar->telefono;
					$sexof = $familiar->sexof;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****FAMILIAR***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 182, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 191, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexof, 0, 1, 'L');
				}

				///*******************SIGNOS VITALES*************
				foreach ($signosvitales as $signos) {
					$datos['peso']         = $signos->peso;
					$datos['talla']        = $signos->talla;
					$datos['spo2']         = $signos->spo2;
					$datos['frecuencia_c'] = $signos->frecuencia_c;
					$datos['frecuencia_r'] = $signos->frecuencia_r;
					$datos['temperatura']  = $signos->temperatura;
					$datos['tension_alta'] = $signos->tension_alta;
					$datos['tension_baja'] = $signos->tension_baja;
					$datos['fecha_creacion'] = $signos->fecha_creacion;
					$datos['motivo_consulta'] = $signos->motivo_consulta;
					$datos['imc'] = $signos->imc;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$datos_historial['enfermedad_actual']   = $enfermedad_actual->descripcion;
					}
				}

				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}

				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}


				/******************OCUPACION/ESTADO CIVIL/GRADO DE INSTRUCION*******************/
				if (empty($info_familiares)) {
					$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
				} else {
					foreach ($info_familiares as $fila) {

						$datos['descrip_estado_civil'] = $fila->descrip_estado_civil;
						$datos['descrip_grado_intruccion'] = $fila->descrip_grado_intruccion;
						$datos['descrip_ocupacion'] = $fila->descrip_ocupacion;
					}
				}



				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}


				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}

				//************PARTOS /CESARIAS/ABORTOS/GESTACION*******

				if (empty($query_gestacion)) {
					$datos_historial['gestacion']    = 'N/A';
				} else {
					foreach ($query_gestacion as $fila) {
						$datos_historial['gestacion'] = $fila->descri_gestacion;
					}
				}
				if (empty($query_partos)) {
					$datos_historial['partos']     = 'N/A';
				} else {

					foreach ($query_partos as $fila) {
						$datos_historial['partos'] = $fila->descri_partos;
					}
				}

				if (empty($query_cesarias)) {
					$datos_historial['cesarias']      = 'N/A';
				} else {
					foreach ($query_cesarias as $fila) {
						$datos_historial['cesarias'] = $fila->descri_cesarias;
					}
				}
				if (empty($query_abortos)) {
					$datos_historial['abortos']      = 'N/A';
				} else {
					foreach ($query_abortos as $fila) {
						$datos_historial['abortos'] = $fila->descri_abortos;
					}
				}




				//************HISTORIA Y ANTECEDENTES GINECO OBSTERTRICO*******
				if (empty($atc_gineco_obste)) {
					$datos_historial['edad_gestacional']  = 'N/A';
					$datos_historial['f_u_r']  = 'N/A';
					$datos_historial['detalle_historia_obst']   = 'N/A';
					$datos_historial['menarquia']  = ' ';
					$datos_historial['sexarquia']  = ' ';
					$datos_historial['nps']   = ' ';
					$datos_historial['ciclo_mestrual']   = ' ';
					$datos_historial['descr_anticon']   = ' ';
					$datos_historial['detalles_anticon']   = ' ';
					$datos_historial['descr_ets']   = ' ';
					$datos_historial['descrip_tipo_ets']   = ' ';
					$datos_historial['citologia']   = ' ';
					$datos_historial['eco_mamario']   = ' ';
					$datos_historial['desintometria']   = ' ';
					$datos_historial['mamografia']   = ' ';
					$datos_historial['descr_dismenorrea']   = ' ';
					$datos_historial['descr_eumenorrea']   = ' ';
					$datos_historial['diu']   = ' ';
					$datos_historial['t_cobre']   = ' ';
					$datos_historial['mirena']   = ' ';
					$datos_historial['aspiral']   = ' ';
					$datos_historial['i_subdermico']   = ' ';
					$datos_historial['otro']   = ' ';
					$datos_historial['t_colocacion']   = ' ';
				} else {
					foreach ($atc_gineco_obste as $fila) {
						$datos_historial['edad_gestacional'] = $fila->edad_gestacional;
						$datos_historial['f_u_r'] = $fila->f_u_r;
						$datos_historial['detalle_historia_obst'] = $fila->detalle_historia_obst;
						$datos_historial['menarquia'] = $fila->menarquia;
						$datos_historial['sexarquia'] = $fila->sexarquia;
						$datos_historial['nps'] = $fila->nps;
						$datos_historial['ciclo_mestrual'] = $fila->ciclo_mestrual;
						$datos_historial['descr_anticon'] = $fila->descr_anticon;
						$datos_historial['detalles_anticon'] = $fila->detalles_anticon;
						$datos_historial['descr_ets'] = $fila->descr_ets;
						$datos_historial['descrip_tipo_ets'] = $fila->descrip_tipo_ets;
						$datos_historial['citologia'] = $fila->citologia;
						$datos_historial['eco_mamario'] = $fila->eco_mamario;
						$datos_historial['desintometria'] = $fila->desintometria;
						$datos_historial['mamografia'] = $fila->mamografia;
						$datos_historial['descr_dismenorrea'] = $fila->descr_dismenorrea;
						$datos_historial['descr_eumenorrea'] = $fila->descr_eumenorrea;
						$datos_historial['diu'] = $fila->diu;
						$datos_historial['t_cobre'] = $fila->t_cobre;
						$datos_historial['mirena'] = $fila->mirena;
						$datos_historial['aspiral'] = $fila->aspiral;
						$datos_historial['i_subdermico'] = $fila->i_subdermico;
						$datos_historial['otro'] = $fila->otro;
						$datos_historial['t_colocacion'] = $fila->t_colocacion;
					}
				}




				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}

				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}

				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}

				//************TIPO DE SANGRE*******
				if (empty($query)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($query as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}
				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}


				$pdf->Content_Ginecologia_Obtertricia($datos, $datos_historial, $Habitos_Model);
				$pdf->footer_Ginecologia_Obtertricia($usuario);
			}

			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		} else if ($tipobeneficiario == 'C') {
			$cortesia = $model->buscarcortesia_NotasEntregas($cedula);
			$modelo_signosvitales = new Consulta_Histotia_Model();
			$signosvitales = $modelo_signosvitales->listar_signos_vitales($n_historial, $id_consulta);
			$modelo_Enfermedad_actual = new Enfermedad_Actual_Model();
			$enfermedad_actual = $modelo_Enfermedad_actual->listar_enfermedad_actual_Individual($n_historial, $id_consulta);
			$modelo_Alergias_Medicamentos = new Alergias_Medicamentos_Model();
			$Alergias_Medicamentos = $modelo_Alergias_Medicamentos->listar_Alergias_Medicamentos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosf = new Antecendentes_Patologicosf_Model();
			$Antecendentes_Patologicosf = $modelo_Antecendentes_Patologicosf->listar_Antecendentes_Patologicosf_Individual($n_historial, $id_consulta);
			$modelo_Antecedentes_Quirurgicos = new Antecedentes_Quirurgicos_Model();
			$Antecedentes_Quirurgicos = $modelo_Antecedentes_Quirurgicos->listar_Antecedentes_Quirurgicos_Individual($n_historial, $id_consulta);
			$modelo_Antecendentes_Patologicosp = new Antecendentes_Patologicosp_Model();
			$Antecendentes_Patologicosp = $modelo_Antecendentes_Patologicosp->listar_Antecendentes_Patologicosp_Individual($n_historial, $id_consulta);
			$modelo_Examen_Fisico = new Examen_Fisico_Model();
			$Examen_Fisico = $modelo_Examen_Fisico->listar_Examen_Fisico_Individual($n_historial, $id_consulta);
			$modelo_Paraclinicos_Model = new Paraclinicos_Model();
			$Paraclinicos = $modelo_Paraclinicos_Model->listar_Paraclinicos_Individual($n_historial, $id_consulta);
			$modelo_Impresion_Diag = new Impresion_Diag_Model();
			$Impresion_Diag = $modelo_Impresion_Diag->listar_Impresion_Diag_Individual($n_historial, $id_consulta);
			$modelo_Plan_Model = new Plan_Model();
			$Plan_Model = $modelo_Plan_Model->listar_Plan_Model_Individual($n_historial, $id_consulta);
			$modelo_Habitos_Model = new Habitos_Model();
			$Habitos_Model = $modelo_Habitos_Model->listar_Habitos_Model_Individual($id_historial_medico, $id_consulta);
			$modelo_Medicamentos_patologias = new Medicamentos_patologias_Model();
			$Medicamentos_patologias = $modelo_Medicamentos_patologias->listar_Medicamentos_patologias_Individual($n_historial, $id_especialidad, $id_consulta);
			$query = $model->getCortersia($cedula);
			$info_familiares = $model->getAllInfoMedicamentosCortesia($cedula);
			$modelo_atc_gineco_obste = new  Antece_Gineco_Obstetrico_Model();
			$atc_gineco_obste = $modelo_atc_gineco_obste->listar_Antece_Gineco_obtetrico($n_historial, $id_consulta);
			$modelo_abortos = new  Abortos_Model();
			$modelo_partos = new  Partos_Model();
			$modelo_cesarias = new  Cesarias_Model();
			$modelo_gestacion = new  Gestacion_Model();
			$query_abortos = $modelo_abortos->listar_abortos($n_historial, $id_consulta);
			$query_partos = $modelo_partos->listar_partos($n_historial, $id_consulta);
			$query_cesarias = $modelo_cesarias->listar_cesarias($n_historial, $id_consulta);
			$query_gestacion = $modelo_gestacion->listar_gestacion($n_historial, $id_consulta);
			if (empty($cortesia)) {
				$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
			} else {

				foreach ($cortesia as $cortesia) {
					// ***TITULAR*****	
					$pdf->SetXY(27.1, 11);
					$pdf->Cell(80, 63, $fecha_asistencia, 0, 1, 'L');
					$nombre = $cortesia->nombre_titular;
					$apellido = $cortesia->apellido_titular;
					$fecha_nacimiento = $cortesia->fecha_nacimientot;
					$cedula_trabajador = $cortesia->cedula_trabajador;
					$ubicacion_administrativa = $cortesia->departamento;
					$edad_actual = $cortesia->edad_actualt;
					$telefono = $cortesia->telefonot;
					$sexot = $cortesia->sexot;
					//****cortesia***
					$nombreb = $cortesia->nombre;
					$apellidob = $cortesia->apellido;
					$fecha_nacimientob = $cortesia->fecha_nac_cortesia;
					$cedulab = $cortesia->cedula;
					$edad_actualb = $cortesia->edad_actual;
					$descripcionparentesco = 'CORTESIA';
					$telefonob = $cortesia->telefono;
					$sexo = $cortesia->sexo;
					// ***TITULAR*****	
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 64, $nombre . $apellido, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 80, $fecha_nacimiento, 0, 0, 'L');
					$pdf->SetXY(135.2, 30);
					$pdf->Cell(30, 80, $cedula_trabajador, 0, 0, 'L');
					$pdf->SetXY(195.2, 30);
					$pdf->Cell(20, 80, $edad_actual, 0, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(20, 95, $ubicacion_administrativa, 0, 0, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 111, $telefono, 0, 1, 'L');
					$pdf->SetXY(174.2, 30.5);
					$pdf->Cell(70, 111, $sexot, 0, 1, 'L');
					//****CORTESIA***
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(80, 151, $nombreb . $apellidob, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 167, $fecha_nacimientob, 10, 1, 'L');
					$pdf->SetXY(136.2, 30);
					$pdf->Cell(80, 166, $cedulab, 10, 1, 'L');
					$pdf->SetXY(196.2, 30);
					$pdf->Cell(80, 166, $edad_actualb, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 183, $descripcionparentesco, 10, 1, 'L');
					$pdf->SetXY(70.2, 30);
					$pdf->Cell(50, 199, $telefonob, 0, 1, 'L');
					$pdf->Cell(10, -77, '', 0, 2, 'L');
					$pdf->SetXY(174.2, 73.5);
					$pdf->Cell(70, 111, $sexo, 0, 1, 'L');
				}

				///*******************SIGNOS VITALES*************
				foreach ($signosvitales as $signos) {
					$datos['peso']         = $signos->peso;
					$datos['talla']        = $signos->talla;
					$datos['spo2']         = $signos->spo2;
					$datos['frecuencia_c'] = $signos->frecuencia_c;
					$datos['frecuencia_r'] = $signos->frecuencia_r;
					$datos['temperatura']  = $signos->temperatura;
					$datos['tension_alta'] = $signos->tension_alta;
					$datos['tension_baja'] = $signos->tension_baja;
					$datos['fecha_creacion'] = $signos->fecha_creacion;
					$datos['motivo_consulta'] = $signos->motivo_consulta;
					$datos['imc'] = $signos->imc;
				}
				/******************ENFERMEDAD ACTUAL*******************/
				if (empty($enfermedad_actual)) {
					$datos_historial['enfermedad_actual']   = ' ';
				} else {
					foreach ($enfermedad_actual as $enfermedad_actual) {
						$datos_historial['enfermedad_actual']   = $enfermedad_actual->descripcion;
					}
				}
				/******************ALERGIAS A MEICAMENTOS*******************/
				if (empty($Alergias_Medicamentos)) {
					$datos_historial['alergias_medicamentos']   = ' ';
				} else {
					$contador = count($Alergias_Medicamentos);
					$control = 0;
					$datos_historial['alergias_medicamentos'] = '';
					foreach ($Alergias_Medicamentos as $Alergias_Medicamentos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion . "\n";
						} else {
							$datos_historial['alergias_medicamentos'] = $datos_historial['alergias_medicamentos'] . '*' . ' ' . $Alergias_Medicamentos->descripcion;
						}
					}
				}

				/******************ANTECEDENTES QUIRURGICOS*******************/
				if (empty($Antecedentes_Quirurgicos)) {
					$datos_historial['antecedentes_quirurgicos']   = ' ';
				} else {
					$contador = count($Antecedentes_Quirurgicos);
					$control = 0;
					$datos_historial['antecedentes_quirurgicos'] = '';
					foreach ($Antecedentes_Quirurgicos as $Antecedentes_Quirurgicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_quirurgicos'] = $datos_historial['antecedentes_quirurgicos'] . '*' . ' ' . $Antecedentes_Quirurgicos->descripcion;
						}
					}
				}
				/******************ANTECEDENTES PATOLOGICOS PERSONALES*******************/
				if (empty($Antecendentes_Patologicosp)) {
					$datos_historial['antecedentes_patologicosp']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosp);
					$control = 0;
					$datos_historial['antecedentes_patologicosp'] = '';
					foreach ($Antecendentes_Patologicosp as $Antecendentes_Patologicosp) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion . "\n";
						} else {
							$datos_historial['antecedentes_patologicosp'] = $datos_historial['antecedentes_patologicosp'] . '*' . ' ' . $Antecendentes_Patologicosp->descripcion;
						}
					}
				}

				/******************ANTECEDENTES PARTOLOGICOS FAMILIARES*******************/
				if (empty($Antecendentes_Patologicosf)) {
					$datos_historial['antecendentes_patologicosf']   = ' ';
				} else {
					$contador = count($Antecendentes_Patologicosf);
					$control = 0;
					$datos_historial['antecendentes_patologicosf'] = '';
					foreach ($Antecendentes_Patologicosf as $Antecendentes_Patologicosf) {
						$control++;
						if ($control < $contador) {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion . "\n";
						} else {
							$datos_historial['antecendentes_patologicosf'] = $datos_historial['antecendentes_patologicosf'] . '*' . ' ' . $Antecendentes_Patologicosf->descripcion;
						}
					}
				}


				/******************OCUPACION/ESTADO CIVIL/GRADO DE INSTRUCION*******************/
				if (empty($info_familiares)) {
					$pdf->cell(196, 5, utf8_decode('Sin Información Coincidente'), 1, 1, 'C', 1);
				} else {
					foreach ($info_familiares as $fila) {

						$datos['descrip_estado_civil'] = $fila->descrip_estado_civil;
						$datos['descrip_grado_intruccion'] = $fila->descrip_grado_intruccion;
						$datos['descrip_ocupacion'] = $fila->descrip_ocupacion;
					}
				}
				/******************EXAMEN FISICO*******************/
				if (empty($Examen_Fisico)) {
					$datos_historial['examenfisico']   = ' ';
				} else {
					$contador = count($Examen_Fisico);
					$control = 0;
					$datos_historial['examenfisico'] = '';
					foreach ($Examen_Fisico as $Examen_Fisico) {
						$control++;
						if ($control < $contador) {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion . "\n";
						} else {
							$datos_historial['examenfisico'] = $datos_historial['examenfisico'] . '*' . ' ' . $Examen_Fisico->descripcion;
						}
					}
				}

				/******************PARACLINICOS*******************/
				if (empty($Paraclinicos)) {
					$datos_historial['paraclinicos']   = ' ';
				} else {
					$contador = count($Paraclinicos);
					$control = 0;
					$datos_historial['paraclinicos'] = '';
					foreach ($Paraclinicos as $Paraclinicos) {
						$control++;
						if ($control < $contador) {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion . "\n";
						} else {
							$datos_historial['paraclinicos'] = $datos_historial['paraclinicos'] . '*' . ' ' . $Paraclinicos->descripcion;
						}
					}
				}
				//************PARTOS /CESARIAS/ABORTOS/GESTACION*******

				if (empty($query_gestacion)) {
					$datos_historial['gestacion']    = 'N/A';
				} else {
					foreach ($query_gestacion as $fila) {
						$datos_historial['gestacion'] = $fila->descri_gestacion;
					}
				}
				if (empty($query_partos)) {
					$datos_historial['partos']     = 'N/A';
				} else {

					foreach ($query_partos as $fila) {
						$datos_historial['partos'] = $fila->descri_partos;
					}
				}

				if (empty($query_cesarias)) {
					$datos_historial['cesarias']      = 'N/A';
				} else {
					foreach ($query_cesarias as $fila) {
						$datos_historial['cesarias'] = $fila->descri_cesarias;
					}
				}
				if (empty($query_abortos)) {
					$datos_historial['abortos']      = 'N/A';
				} else {
					foreach ($query_abortos as $fila) {
						$datos_historial['abortos'] = $fila->descri_abortos;
					}
				}



				//************HISTORIA Y ANTECEDENTES GINECO OBSTERTRICO*******
				if (empty($atc_gineco_obste)) {
					$datos_historial['edad_gestacional']  = 'N/A';
					$datos_historial['f_u_r']  = 'N/A';
					$datos_historial['detalle_historia_obst']   = 'N/A';
					$datos_historial['menarquia']  = ' ';
					$datos_historial['sexarquia']  = ' ';
					$datos_historial['nps']   = ' ';
					$datos_historial['ciclo_mestrual']   = ' ';
					$datos_historial['descr_anticon']   = ' ';
					$datos_historial['detalles_anticon']   = ' ';
					$datos_historial['descr_ets']   = ' ';
					$datos_historial['descrip_tipo_ets']   = ' ';
					$datos_historial['citologia']   = ' ';
					$datos_historial['eco_mamario']   = ' ';
					$datos_historial['desintometria']   = ' ';
					$datos_historial['mamografia']   = ' ';
					$datos_historial['descr_dismenorrea']   = ' ';
					$datos_historial['descr_eumenorrea']   = ' ';
					$datos_historial['diu']   = ' ';
					$datos_historial['t_cobre']   = ' ';
					$datos_historial['mirena']   = ' ';
					$datos_historial['aspiral']   = ' ';
					$datos_historial['i_subdermico']   = ' ';
					$datos_historial['otro']   = ' ';
					$datos_historial['t_colocacion']   = ' ';
				} else {
					foreach ($atc_gineco_obste as $fila) {
						$datos_historial['edad_gestacional'] = $fila->edad_gestacional;
						$datos_historial['f_u_r'] = $fila->f_u_r;
						$datos_historial['detalle_historia_obst'] = $fila->detalle_historia_obst;
						$datos_historial['menarquia'] = $fila->menarquia;
						$datos_historial['sexarquia'] = $fila->sexarquia;
						$datos_historial['nps'] = $fila->nps;
						$datos_historial['ciclo_mestrual'] = $fila->ciclo_mestrual;
						$datos_historial['descr_anticon'] = $fila->descr_anticon;
						$datos_historial['detalles_anticon'] = $fila->detalles_anticon;
						$datos_historial['descr_ets'] = $fila->descr_ets;
						$datos_historial['descrip_tipo_ets'] = $fila->descrip_tipo_ets;
						$datos_historial['citologia'] = $fila->citologia;
						$datos_historial['eco_mamario'] = $fila->eco_mamario;
						$datos_historial['desintometria'] = $fila->desintometria;
						$datos_historial['mamografia'] = $fila->mamografia;
						$datos_historial['descr_dismenorrea'] = $fila->descr_dismenorrea;
						$datos_historial['descr_eumenorrea'] = $fila->descr_eumenorrea;
						$datos_historial['diu'] = $fila->diu;
						$datos_historial['t_cobre'] = $fila->t_cobre;
						$datos_historial['mirena'] = $fila->mirena;
						$datos_historial['aspiral'] = $fila->aspiral;
						$datos_historial['i_subdermico'] = $fila->i_subdermico;
						$datos_historial['otro'] = $fila->otro;
						$datos_historial['t_colocacion'] = $fila->t_colocacion;
					}
				}





				/******************IMPRESION DIAGNOSTICA*******************/
				if (empty($Impresion_Diag)) {
					$datos_historial['impresion_diag']   = ' ';
				} else {
					$contador = count($Impresion_Diag);
					$control = 0;
					$datos_historial['impresion_diag'] = '';
					foreach ($Impresion_Diag as $Impresion_Diag) {
						$control++;
						if ($control < $contador) {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion . "\n";
						} else {
							$datos_historial['impresion_diag'] = $datos_historial['impresion_diag'] . '*' . ' ' . $Impresion_Diag->descripcion;
						}
					}
				}
				/******************PLAN*******************/
				if (empty($Plan_Model)) {
					$datos_historial['plan']   = ' ';
				} else {
					$contador = count($Plan_Model);
					$control = 0;
					$datos_historial['plan'] = '';
					foreach ($Plan_Model as $Plan_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion . "\n";
						} else {
							$datos_historial['plan'] = $datos_historial['plan'] . '*' . ' ' . $Plan_Model->descripcion;
						}
					}
				}

				/******************HABITOS PSICOSOCIALES*******************/
				if (empty($Habitos_Model)) {
					$datos_historial['habitos']   = ' ';
				} else {
					$contador = count($Habitos_Model);
					$control = 0;
					$datos_historial['habitos'] = '';
					foreach ($Habitos_Model as $Habitos_Model) {
						$control++;
						if ($control < $contador) {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion . "\n";
						} else {
							$datos_historial['habitos'] = $datos_historial['habitos'] . '*' . ' ' . $Habitos_Model->descripcion;
						}
					}
				}
				//************TIPO DE SANGRE*******
				if (empty($query)) {
					$datos['descripcion_tipodesangre'] = '';
				} else {
					foreach ($query as $tiposangre) {
						$datos['descripcion_tipodesangre']  = $tiposangre->tipodesangre;
					}
				}
				/******************MEDICAMENTOS PATOLOGIAS *******************/
				if (empty($Medicamentos_patologias)) {
					$datos_historial['medicamentos_patologias']   = ' ';
				} else {
					$contador = count($Medicamentos_patologias);
					$control = 0;
					$datos_historial['medicamentos_patologias'] = '';
					foreach ($Medicamentos_patologias as $Medicamentos_patologias) {
						$control++;
						if ($control < $contador) {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion . "\n";
						} else {
							$datos_historial['medicamentos_patologias'] = $datos_historial['medicamentos_patologias'] . '*' . ' ' . $Medicamentos_patologias->descripcion;
						}
					}
				}

				$pdf->Content_Ginecologia_Obtertricia($datos, $datos_historial, $Habitos_Model);
				$pdf->footer_Ginecologia_Obtertricia($usuario);
			}
			$this->response->setHeader('Content-Type', 'application/pdf');
			$pdf->Output("salidas_pdf.pdf", "I");
		}
	}


	public function frecuencia_atencion()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		} else {

			echo view('/reportes/frecuencia_atencion');
			echo view('/reportes/footer_frecuencia_atencion');
		}
	}

	public function listar_frecuencia_atencion($desde = null, $hasta = null, $beneficiario = null, $sexo = null)
	{
		$model = new Morbilidad_Model();
		$query = $model->listar_frecuencia_atencion($desde, $hasta, $beneficiario, $sexo);
		
		if (empty($query)) {
			$frecuencia_atencion = [];
		} else {
			$frecuencia_atencion = $query;
		}
		echo json_encode($frecuencia_atencion);
	}
}
