<?php

namespace App\Controllers;
use App\Models\Auditoria_sistema_Model;
use App\Models\Entes_Adscritos_model;
use CodeIgniter\API\ResponseTrait;

use CodeIgniter\RESTful\ResourceController;

class Entes_Adscritos_Controllers extends BaseController
{
	use ResponseTrait;
	public function index()
	{
		return ('Esta es la Página de tipo de Medicamentos ...');
	}
	/*
      * Función para mostrar el listado de Roles
      */
	public function vista()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/entes_adscritos/content');
		echo view('/entes_adscritos/footer_entesadscritos');
	}

	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll()
	{
		$model = new Entes_Adscritos_model();
		$query = $model->getAll();
		if (empty($query->getResult())) {
			$entes_adscritos = [];
		} else {
			$entes_adscritos = $query->getResultArray();
		}
		echo json_encode($entes_adscritos);
	}

	public function getAllActivos()
	{

		$model = new Entes_Adscritos_model();
		$query = $model->getAllActivos();
		if (empty($query->getResult())) {
			$entes_adscritos = [];
		} else {
			$entes_adscritos = $query->getResultArray();
		}
		echo json_encode($entes_adscritos);
	}




	public function agregar()
	{
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['rif']          = $data->rif;
		$datos['descripcion']                   = $data->descripcion;
		$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
		$model = new Entes_Adscritos_model();
		$model_auditoria=new Auditoria_sistema_Model();
		$query_agregar = $model->Agregar($datos);
		if (isset($query_agregar)) {
				$mensaje = 1;
				$auditoria['accion'] = 'REGISTRÓ EL ENTE ADSCRITO   '.' '.$datos['descripcion'];
				$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	public function actualizar()
	{
		$modelo = new Entes_Adscritos_model();
		$model_auditoria=new Auditoria_sistema_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']          = $data->id;
		$datos['rif']          = $data->rif;
		$datos['descripcion']                   = $data->descripcion;
		$datos['fecha_creacion'] = $this->formatearFecha($data->fechaRegistro);
		$datos['borrado']       = $data->borrado;
		$datos_modificados['datos_modificados']       = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$datos_descr_anterior['decripcion_anterior']= $data->descripcion_anterior;
		$query = $modelo->actualizar($datos);
		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUENTES DATOS DEL ENTE ADSCRITO '.' '.$datos_descr_anterior['decripcion_anterior'].','.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		//$mensaje=$datos;
		return json_encode($mensaje);
	}
}
