<?php

namespace App\Controllers;

use App\Models\UsuarioModel;
use App\Models\AuditoriaModel;
use App\Models\EntradaModel;
use App\Models\Medicos_Model;

class Login extends BaseController
{
    private $usuario;
    public function index()
    {
        return view('login/loginpantalla');
    }

    public function validarIngreso()
    {
        $usuario = $this->request->getPost("usuario");
        /*         $this->usuario= new UsuarioModel();       
        $resultado= $this->usuario->buscarUsuario($usuario);
 */
        $modelo = new UsuarioModel();
        $resultado = $modelo->buscarUsuario($usuario);

        if ($resultado) {
            if (
                $this->request->getMethod() === 'post' && $this->validate(
                    [
                        'nombre' => 'required|min_length[7]|max_length[58]',
                        'contrasena'  => 'required',
                    ]
                )
            );

            // Obtener el password ingresado por el usuario
            $passwordIngresado = $this->request->getPost('contrasena');
            // Obtener el password encriptado almacenado en la base de datos
            $passwordEncriptado = $resultado->password;
            // Verificar si el password ingresado coincide con el password encriptado almacenado en la base de datos
            if (password_verify($passwordIngresado, $passwordEncriptado)) {
                // El password ingresado es correcto
                $data =
                [
                    "nombreUsuario" => $resultado->nombre . ' ' . $resultado->apellido,
                    "id_user" => $resultado->id,
                    "nivel_usuario" => $resultado->tipousuario,
                    "cedula_usuario" => $resultado->cedula,  
                ];
    
                if ($resultado->tipousuario == '3') {
                    //Es un médico:

                    $modelo_medico = new Medicos_Model();
                    $resultado_medico = $modelo_medico->Buscarmedicos($resultado->cedula);
                    $data['medico_id'] = $resultado_medico[0]->id;
                    $data['descripcion'] = $resultado_medico[0]->especialidad;
                    $data['id_especialidad'] = $resultado_medico[0]->id_especialidad;
                    $data['acceso_citas'] = $resultado_medico[0]->acceso_citas;
                   
                } else if ($resultado->tipousuario != '3') {
                    //NO ES un médico:

                    $modelo_medico = new Medicos_Model();
                    $resultado_medico = $modelo_medico->Buscarmedicos($resultado->cedula);
                    // $data['medico_id']=$resultado_medico[0]->id;    
                    //$data['descripcion']=$resultado_medico[0]->especialidad;    
                    //$data['id_especialidad']=$resultado_medico[0]->id_especialidad; 
                    // $data['acceso_citas']=false;


                }
               
                // Coloco en sesión lo que tiene $data:
                session()->set($data);
                $modelo_auditoria = new AuditoriaModel();
                $resultado_aditoria = $modelo_auditoria->fecha_auditoria($resultado->id);
              
                echo view('menu/supermenu', $data);
               // echo view('menu/footer.php'); 
                
            } else {
                $data = ['tipo' => 'danger', 'mensaje' => 'La Contraseña es Incorrecta'];
                return view('login/loginpantalla', $data);
            }
        } else {
            $data = ['tipo' => 'danger', 'mensaje' => 'Usuario incorrecto o inactivo'];
            return view('login/loginpantalla', $data);
        }
    }

    public function cerrarSesion()

    {
        session()->destroy();
        return redirect()->to('/');
    }
}
