<?php

namespace App\Controllers;
use App\Models\Auditoria_sistema_Model;
use App\Models\Beneficiarios_Model;
use App\Models\Entes_Adscritos_model;
use App\Models\Modificacion_Estatus_Beneficiarios_Model;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;

class Beneficiarios_Controler extends BaseController
{
	use ResponseTrait;
	public function titulares()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/content_Titulares');
		echo view('/beneficiarios/footer_Titulares');
	}

	public function  info_medicamentos_titulares($cedula_trabajador)
	{
		$modelo = new Beneficiarios_model();
		$query = $modelo->getAll($cedula_trabajador);
		if (empty($query)) {
			$datos_titular = [];
		} else {
			foreach ($query as $fila) {
				$datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
				$datos_titular['nombre']                  = $fila->nombre;
				$datos_titular['apellido']				  = $fila->apellido;
				$datos_titular['telefono']				  = $fila->telefono;
				$datos_titular['sexo']					  = $fila->sexo;
				$datos_titular['sexon']					  = $fila->sexon;
				$datos_titular['fecha_nacimiento']		  = $this->formatearFecha($fila->fecha_nacimiento);
				$datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
				$datos_titular['tipo_de_personal']		  = $fila->tipo_de_personal;
				$datos_titular['nacionalidad']   		  = $fila->nacionalidad;
			}
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/info_medicamentos_titulares', $datos_titular);
		echo view('/beneficiarios/footer_info_medicamento_titulares');
	}

	public function  info_medicamentos_familiares($cedula, $cedula_trabajador)
	{
		$modelo = new Beneficiarios_model();
		$query = $modelo->getAllInfoMedicamentosFamiliares($cedula);
		$query_titular = $modelo->getAll($cedula_trabajador);
		if (empty($query)) {
			$datos_titular = [];
		} else {

			foreach ($query_titular as $titular) {

				$datos_titular['nombretitular']                  = $titular->nombre;
				$datos_titular['apellidotitular']				  = $titular->apellido;
				$datos_titular['cedulatitular']				  = $titular->cedula_trabajador;
			}
			foreach ($query as $fila) {
				$datos_titular['cedula']       = $fila->cedula;
				$datos_titular['nombre']                  = $fila->nombre;
				$datos_titular['apellido']				  = $fila->apellido;
				$datos_titular['telefono']				  = $fila->telefono;
				$datos_titular['sexo']					  = $fila->sexo;
			}
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/info_medicamentos_familiares', $datos_titular);
		echo view('/beneficiarios/footer_info_medicamento_familiares');
	}


	public function  info_medicamentos_cortesia($cedula, $cedula_trabajador)
	{

		$modelo = new Beneficiarios_model();
		$query = $modelo->getAllInfoMedicamentosCortesia($cedula);
		$query_titular = $modelo->getAll($cedula_trabajador);

		//var_dump($query);
		//die();
		if (empty($query)) {
			$datos_titular = [];
		} else {

			foreach ($query_titular as $titular) {

				$datos_titular['nombretitular']                  = $titular->nombre;
				$datos_titular['apellidotitular']				  = $titular->apellido;
				$datos_titular['cedulatitular']				  = $titular->cedula_trabajador;
			}
			foreach ($query as $fila) {
				$datos_titular['cedula']       = $fila->cedula;
				$datos_titular['nombre']                  = $fila->nombre;
				$datos_titular['apellido']				  = $fila->apellido;
				$datos_titular['telefono']				  = $fila->telefono;
				$datos_titular['sexo']					  = $fila->sexo;
			}
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/info_medicamentos_cortesia', $datos_titular);
		echo view('/beneficiarios/footer_info_medicamento_cortesia');
	}





	public function  editar_titular($cedula_trabajador)
	{
		$modelo = new Beneficiarios_model();
		$query = $modelo->getAll($cedula_trabajador);


		//var_dump($query);
		//die();
		if (empty($query)) {
			$datos_titular = [];
		} else {
			foreach ($query as $fila) {
				$datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
				$datos_titular['nombre']                  = $fila->nombre;
				$datos_titular['apellido']				  = $fila->apellido;
				$datos_titular['telefono']				  = $fila->telefono;
				$datos_titular['sexo']					  = $fila->sexo;
				$datos_titular['sexon']					  = $fila->sexon;
				$datos_titular['fecha_nacimiento']		  = $fila->fecha_nacimiento;
				$datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
				$datos_titular['tipo_de_personal']		  = $fila->tipo_de_personal;
				$datos_titular['nacionalidad']   		  = $fila->nacionalidad;
				$datos_titular['tipo_de_sangre']		   = $fila->tipo_de_sangre;
				$datos_titular['estado_civil']		   = $fila->estado_civil;
				$datos_titular['grado_intruccion_id'] = $fila->grado_intruccion_id;
				$datos_titular['ocupacion_id'] = $fila->ocupacion_id;
				$datos_titular['borrado'] = $fila->borrado;
			}
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/editar_titular', $datos_titular);
		echo view('/beneficiarios/footer_editarTitular');
	}

	public function  editar_Familiar($cedula)
	{

		$modelo = new Beneficiarios_model();
		$query = $modelo->getFamiliar($cedula);

		if (empty($query)) {
			$datos_titular = [];
		} else {
			foreach ($query as $fila) {
				$datos_familiar['cedula_trabajador']       = $fila->cedula_trabajador;
				$datos_familiar['cedula']                  = $fila->cedula;
				$datos_familiar['nombre']                  = $fila->nombre;
				$datos_familiar['apellido']				   = $fila->apellido;
				$datos_familiar['telefono']				   = $fila->telefono;
				$datos_familiar['sexo']					   = $fila->sexo;
				$datos_familiar['id_sexo']					   = $fila->id_sexo;
				$datos_familiar['parentesco_id']		   = $fila->parentesco_id;
				$datos_familiar['tipo_de_sangre']		   = $fila->tipo_de_sangre;
				$datos_familiar['estado_civil']		   = $fila->estado_civil;
				$datos_familiar['grado_intruccion_id'] = $fila->grado_intruccion_id;
				$datos_familiar['ocupacion_id'] = $fila->ocupacion_id;
				$datos_familiar['fecha_nac_familiares']		  = $fila->fecha_nac_familiares;
				$datos_familiar['borrado']		  = $fila->borrado;
			}
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/editar_Familiar', $datos_familiar);
		echo view('/beneficiarios/footer_editarfamiliar');
	}
	public function  editar_Cortesia($cedula)
	{

		$modelo = new Beneficiarios_model();
		$query = $modelo->getCortersia($cedula);


		if (empty($query)) {
			$datos_titular = [];
		} else {
			foreach ($query as $fila) {
				$datos_cortesia['id']       =     $fila->id;
				$datos_cortesia['cedula_trabajador']       = $fila->cedula_trabajador;
				$datos_cortesia['nacionalidad']            = $fila->nacionalidad;
				$datos_cortesia['cedula']                  = $fila->cedula;
				$datos_cortesia['nombre_titular']          = $fila->nombre_titular;
				$datos_cortesia['apellido_titular']        = $fila->apellido_titular;
				$datos_cortesia['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
				$datos_cortesia['nombre']                  = $fila->nombre;
				$datos_cortesia['apellido']				   = $fila->apellido;
				$datos_cortesia['telefono']				   = $fila->telefono;
				$datos_cortesia['sexo']					   = $fila->sexo;
				$datos_cortesia['fecha_nac_cortesia']	   = $fila->fecha_nac_cortesia;
				$datos_cortesia['observacion']		       = $fila->observacion;
				$datos_cortesia['tipo_de_sangre']		   = $fila->tipo_de_sangre;
				$datos_cortesia['estado_civil']		   = $fila->estado_civil;
				$datos_cortesia['grado_intruccion_id']		   = $fila->grado_intruccion_id;
				$datos_cortesia['ocupacion_id']		   = $fila->ocupacion_id;
				$datos_cortesia['borrado']		  = $fila->borrado;
				$datos_cortesia['adscrito']		   = $fila->adscrito;
				$datos_cortesia['id_adscrito']		  = $fila->id_adscrito;
			}
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/editar_Cortesia', $datos_cortesia);
		echo view('/beneficiarios/footer_editarCortesia');
	}
	// ************************ ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR HACIA LA VENTANA DE FAMILIARES************
	public function  listar_Familiares($cedula_trabajador)
	{

		$modelo = new Beneficiarios_model();
		$query = $modelo->getAll($cedula_trabajador);
		//var_dump($query);
		//die();
		if (empty($query)) {
			$datos_titular = [];
		} else {
			foreach ($query as $fila) {
				$datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
				$datos_titular['nombre']                  = $fila->nombre;
				$datos_titular['apellido']				  = $fila->apellido;
				$datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
			}
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/agregar_Familiar', $datos_titular);
		echo view('/beneficiarios/footer_Familiares');
	}
	// ************************ ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR HACIA LA VENTANA DE CORTESIA ************
	public function  listar_Cortesia($cedula_trabajador)
	{
		$modelo = new Beneficiarios_model();
		$query = $modelo->getAll($cedula_trabajador);
		//var_dump($query);
		//die();
		if (empty($query)) {
			$datos_titular = [];
		} else {
			foreach ($query as $fila) {
				$datos_titular['cedula_trabajador']       = $fila->cedula_trabajador;
				$datos_titular['nombre']                  = $fila->nombre;
				$datos_titular['apellido']				  = $fila->apellido;
				$datos_titular['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
			}
		}
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/content_Cortesia', $datos_titular);
		echo view('/beneficiarios/footer_Cortesia');
	}
	// ************************ CARGA LA BANDEJA DE FAMILIARES *********************
	public function getAllFamiliares($cedula_trabajador)
	{
		$model = new Beneficiarios_Model();
		$query = $model->getAllFamiliares($cedula_trabajador);


		if (empty($query)) {
			$familiares = [];
		} else {
			$familiares = $query;
		}
		echo json_encode($familiares);
	}
	// ************************ CARGA LA BANDEJA DE CORTESIA *********************
	public function getAllCortesia($cedula_trabajador)
	{
		$model = new Beneficiarios_Model();
		$query = $model->getAllCortesia($cedula_trabajador);
		if (empty($query)) {
			$familiares = [];
		} else {
			$familiares = $query;
		}
		echo json_encode($familiares);
	}

	public function familiares()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/content_Familiares');
		echo view('/beneficiarios/footer_Familiares');
	}

	public function cortesia()
	{
		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		echo view('/beneficiarios/content_Cortesia');
		echo view('/beneficiarios/footer_Cortesia');
	}
	/*
      * Función parar cargar los registros del Módulo en el Data Table o en las Persianas
      */
	public function getAll()
	{
		$model = new Beneficiarios_Model();
		$query = $model->getAll();

		//if(empty($query->getResult()))
		if (empty($query)) {
			$titulares = [];
		} else {
			$titulares = $query;
		}
		echo json_encode($titulares);
	}
	// *********ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR EN MODAL DE RETIRAR**********
	public function getAllTitulares()
	{
		$data = json_decode(base64_decode($this->request->getGet('data')));

		$datos['cedula_beneficiario'] = $data->cedula_beneficiario;
		$modelo = new Beneficiarios_Model();
		$query = $modelo->getAll($datos['cedula_beneficiario']);
		$respuesta = [];
		if (empty($query)) {
			echo ('sin datos ');
			//$respuesta='no hay registros';
			//$respuesta[]='0';
		} else {
			foreach ($query as $fila) {
				$respuesta['cedula_trabajador'] = $fila->cedula_trabajador;
				$respuesta['nombre'] = $fila->nombre;
				$respuesta['apellido'] = $fila->apellido;
				$respuesta['borrado'] = $fila->borrado;
			}
		}
		return json_encode($respuesta);
	}

	// *********ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR EN MODAL DE NOTAS DE ENTREGAS**********
	public function buscartitular_NotasEntregas()
	{
		$data = json_decode(base64_decode($this->request->getGet('data')));

		$datos['cedula_beneficiario'] = $data->cedula_beneficiario;
		$modelo = new Beneficiarios_Model();
		$query = $modelo->buscartitular_NotasEntregas($datos['cedula_beneficiario']);
		$respuesta = [];
		if (empty($query)) {
			echo ('sin datos ');
			//$respuesta='no hay registros';
			//$respuesta[]='0';
		} else {
			foreach ($query as $fila) {
				$respuesta['cedula_trabajador'] = $fila->cedula_trabajador;
				$respuesta['nombre'] = $fila->nombre;
				$respuesta['apellido'] = $fila->apellido;
				$respuesta['fecha_nacimiento'] = $this->formatearFecha($fila->fecha_nacimiento);
				$respuesta['edad_actual'] = $fila->edad_actual;
				$respuesta['ubicacion_administrativa'] = $fila->ubicacion_administrativa;
				$respuesta['telefono'] = $fila->telefono;
				$respuesta['n_historial'] = $fila->n_historial;
				$respuesta['borrado'] = $fila->borrado;
			}
		}
		return json_encode($respuesta);
	}


	// *********ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR EN MODAL DE RETIRAR**********
	public function getAllfamiliar()
	{
		$data = json_decode(base64_decode($this->request->getGet('data')));

		$datos['cedula_beneficiario'] = $data->cedula_beneficiario;
		$modelo = new Beneficiarios_Model();
		$query = $modelo->getFamiliar($datos['cedula_beneficiario']);
		$respuesta = [];
		if (empty($query)) {
			echo ('sin datos ');
			//$respuesta='no hay registros';
			//$respuesta[]='0';
		} else {
			foreach ($query as $fila) {
				$respuesta['nombre'] = $fila->nombre;
				$respuesta['apellido'] = $fila->apellido;
				$respuesta['nombre_titular'] = $fila->nombre_titular;
				$respuesta['apellido_titular'] = $fila->apellido_titular;
				$respuesta['borrado'] = $fila->borrado;
			}
		}
		return json_encode($respuesta);
	}





	// *********ESTE METODO SE USA PARA PASAR LOS DATOS DEL FAMILIAR EN NOTAS DE ENTREGAS**********
	public function buscarfamiliar_NotasEntregas()
	{
		$data = json_decode(base64_decode($this->request->getGet('data')));

		$datos['cedula_beneficiario'] = $data->cedula_beneficiario;
		$modelo = new Beneficiarios_Model();
		$query = $modelo->buscarfamiliar_NotasEntregas($datos['cedula_beneficiario']);
		//return json_encode($query);
		$respuesta = [];
		if (empty($query)) {
			echo ('sin datos ');
			//$respuesta='no hay registros';
			//$respuesta[]='0';
		} else {
			foreach ($query as $fila) {
				$respuesta['nombre'] = $fila->nombre;
				$respuesta['apellido'] = $fila->apellido;
				$respuesta['fecha_nac_familiares'] = $this->formatearFecha($fila->fecha_nac_familiares);
				$respuesta['edad_actual'] = $fila->edad_actual;
				$respuesta['cedula'] = $fila->cedula;
				$respuesta['telefono'] = $fila->telefono;
				$respuesta['descripcionparentesco'] = $fila->descripcionparentesco;
				$respuesta['departamento'] = $fila->departamento;
				$respuesta['nombre_titular'] = $fila->nombre_titular;
				$respuesta['apellido_titular'] = $fila->apellido_titular;
				$respuesta['fecha_nacimiento'] = $this->formatearFecha($fila->fecha_nacimientot);
				$respuesta['cedula_trabajador'] = $fila->cedula_trabajador;
				$respuesta['edad_actualt'] = $fila->edad_actualt;
				$respuesta['telefonot'] = $fila->telefonot;
				$respuesta['n_historial'] = $fila->n_historial;
				$respuesta['borrado'] = $fila->borrado;
			}
		}
		return json_encode($respuesta);
	}


	// *********ESTE METODO SE USA PARA PASAR LOS DATOS DEL TITULAR EN MODAL DE RETIRAR**********
	public function getAllBuscarCortesia()
	{
		$data = json_decode(base64_decode($this->request->getGet('data')));

		$datos['cedula_beneficiario'] = $data->cedula_beneficiario;
		$modelo = new Beneficiarios_Model();
		$query = $modelo->getCortersia($datos['cedula_beneficiario']);
		$respuesta = [];
		if (empty($query)) {
			echo ('sin datos ');
			//$respuesta='no hay registros';
			//$respuesta[]='0';
		} else {
			foreach ($query as $fila) {
				$respuesta['nombre'] = $fila->nombre;
				$respuesta['apellido'] = $fila->apellido;
				$respuesta['nombre_titular'] = $fila->nombre_titular;
				$respuesta['apellido_titular'] = $fila->apellido_titular;
				$respuesta['borrado'] = $fila->borrado;
			}
		}

		return json_encode($respuesta);
	}


	// *********ESTE METODO SE USA PARA PASAR LOS DATOS DE CORTESIA PARA NOTAS DE ENTREGAS**********
	public function buscarcortesia_NotasEntregas()
	{
		$data = json_decode(base64_decode($this->request->getGet('data')));

		$datos['cedula_beneficiario'] = $data->cedula_beneficiario;
		$modelo = new Beneficiarios_Model();
		$query = $modelo->buscarcortesia_NotasEntregas($datos['cedula_beneficiario']);
		$respuesta = [];
		if (empty($query)) {
			echo ('sin datos ');
			//$respuesta='no hay registros';
			//$respuesta[]='0';
		} else {
			foreach ($query as $fila) {
				$respuesta['nombre'] = $fila->nombre;
				$respuesta['apellido'] = $fila->apellido;
				$respuesta['fecha_nac_cortesia'] = $fila->fecha_nac_cortesia;
				$respuesta['edad_actual'] = $fila->edad_actual;
				$respuesta['cedula'] = $fila->cedula;
				$respuesta['telefono'] = $fila->telefono;
				//$respuesta['descripcionparentesco']=$fila->descripcionparentesco;

				$respuesta['departamento'] = $fila->departamento;
				$respuesta['nombre_titular'] = $fila->nombre_titular;
				$respuesta['apellido_titular'] = $fila->apellido_titular;
				$respuesta['fecha_nacimiento'] = $this->formatearFecha($fila->fecha_nacimientot);
				$respuesta['cedula_trabajador'] = $fila->cedula_trabajador;
				$respuesta['edad_actualt'] = $fila->edad_actualt;
				$respuesta['telefonot'] = $fila->telefonot;
				$respuesta['n_historial'] = $fila->n_historial;
				$respuesta['borrado'] = $fila->borrado;
			}
		}

		return json_encode($respuesta);
	}




	public function agregar()
	{

		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['cedula_trabajador']          = $data->cedula;
		$datos['nombre ']                   = $data->nombre;
		$datos['apellido ']                 = $data->apellido;
		$datos['tipo_de_personal']         = $data->tipo_de_personal;
		$datos['ubicacion_administrativa'] = $data->ubicacionadministrativa;
		$datos['telefono']                 = $data->telefono;
		$datos['sexo']                     = $data->sexo;
		$datos['fecha_nacimiento']         = $data->fecha_nacimiento;
		$datos['nacionalidad']             = $data->nacionalidad;
		$datos['tipo_de_sangre']         = $data->tipodesangre;
		$datos['estado_civil']             = $data->estadocivil;
		$datos['grado_intruccion_id']          = $data->gradoinstruccion_id;
		$datos['ocupacion_id']          = $data->ocupacion_id;
		$model = new Beneficiarios_Model();
		$query2 = $model->getAllTitular($datos);
		if (empty($query2)) {
			$query = $model->agregar($datos);
			if (isset($query)) {
				$mensaje = 1;
			} else {
				$mensaje = 0;
			}
		} else {
			$mensaje = 2;
		}
		return json_encode($mensaje);
	}


	public function actualizar_titular()
	{
		$modelo = new Beneficiarios_Model();
		$model_auditoria = new Auditoria_sistema_Model();
		$modelo_estatus_titular = new  Modificacion_Estatus_Beneficiarios_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");
		$datos['cedula_trabajador']          = $data->cedula_trabajador;
		$datos['tipo_de_personal']         = $data->tipo_de_personal;
		$datos['ubicacion_administrativa'] = $data->ubicacion_administrativa;
		$datos['telefono']                 = $data->telefono;
		$datos['sexo']                     = $data->sexo;
		$datos['fecha_nacimiento']         = $data->fecha_nacimiento;
		$datos['nacionalidad']             = $data->nacionalidad;
		$datos['tipo_de_sangre']                 = $data->tipodesangre_id;
		$datos['estado_civil']                 = $data->estadocivil_id;
		$datos['grado_intruccion_id']          = $data->grado_intruccion_id;
		$datos['ocupacion_id']          = $data->ocupacion_id;
		$datos['borrado']          = $data->estatus_trabajador;
		$datos_beneficiario['borrado']          = $data->estatus_beneficiario;
		$datos_beneficiario['cedula_trabajador']          = $data->cedula_trabajador;
		$query = $modelo->actualizar_titular($datos);
		$query_familiar = $modelo->actualizar_Estatusfamiliar($datos_beneficiario);
		$query_cortesia = $modelo->actualizar_EstatusCortesia($datos_beneficiario);
		$datos['bandera']          				= $data->bandera_Modificacion;
		$bandera['id_usuario']          		= $data->id_usuario;
		$bandera['cedula']                  	= $data->cedula_trabajador;
		$bandera['estatus']          			= $data->estatus_modificado;
		$bandera['motivo']          			= $data->observacion;
		$bandera['hora'] = $hora;
		$datos_modificados['datos_modificados'] = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$datos_nombre['datos_nombre'] = $data->nombre;
		if ($datos['bandera'] == 'true') {
			$query_estatus = $modelo_estatus_titular->Modificacion_Estatus_Beneficiario($bandera);
		}
		if (isset($query_estatus)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUIENTES DATOS DE '.' '.$datos_nombre['datos_nombre'].', '.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}



	public function agregar_Familiares()
	{

		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['cedula_trabajador']         = $data->cedula_trabajador;
		$datos['cedula']                    = $data->cedula;
		$datos['nombre']           			= $data->nombre_familiar;
		$datos['apellido']                  = $data->apellido;
		$datos['telefono']                  = $data->telefono;
		$datos['parentesco_id']             = $data->parentesco_id;
		$datos['sexo']                       = $data->sexo;
		$datos['fecha_nac_familiares']      = $data->fecha_nac_familiares;
		$datos['tipo_de_sangre']                 = $data->tipodesangre;
		$datos['estado_civil']                 = $data->estadocivil;
		$datos['grado_intruccion_id']          = $data->gradoinstruccion_id;
		$datos['ocupacion_id']          = $data->ocupacion_id;

		$model = new Beneficiarios_Model();
		$buscar_familiar = $model->Buscar_Familiares_exixtente($datos['cedula']);
		if (empty($buscar_familiar)) {
			$query = $model->agregar_Familiares($datos);
			if ($query == 'true') {
				$mensaje = 2;
			} else {
				$mensaje = 0;
			}
		} else {
			$mensaje = 1;
		}

		return json_encode($mensaje);
	}
	public function actualizar_familiar($cedula_anterior = null)
	{
		$modelo = new Beneficiarios_Model();
		$model_auditoria = new Auditoria_sistema_Model();
		$modelo_estatus_familiares = new  Modificacion_Estatus_Beneficiarios_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");
		$datos['cedula_trabajador']          	= $data->cedula_trabajador;
		$datos['cedula']                     	= $data->cedula;
		$datos['nombre']                     	= $data->nombre;
		$datos['apellido ']                  	= $data->apellido;
		$datos['telefono']                   	= $data->telefono;
		$datos['sexo']                       	= $data->sexo;
		$datos['fecha_nac_familiares']       	= $data->fecha_nac_familiares;
		$datos['parentesco_id']              	= $data->parentesco_id;
		$datos['tipo_de_sangre']              	= $data->tipo_de_sangre_id;
		$datos['estado_civil']              	= $data->estado_civil_id;
		$datos['grado_intruccion_id']          	= $data->grado_intruccion_id;
		$datos['ocupacion_id']          		= $data->ocupacion_id;
		$datos['borrado']          				= $data->estatus_modificado;
		$query = $modelo->actualizar_familiar($datos, $cedula_anterior);
		$datos['bandera']          				= $data->bandera_Modificacion;
		$bandera['id_usuario']          		= $data->id_usuario;
		$bandera['cedula']                  	= $data->cedula;
		$bandera['estatus']          			= $data->estatus_modificado;
		$bandera['motivo']          			= $data->observacion;
		$bandera['hora'] = $hora;
		$datos_modificados['datos_modificados'] = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$datos_nombre['datos_nombre'] = $data->nombre_anterior.' '.$data->apellido_anterior;
		if ($datos['bandera'] == 'true') {
			$query_estatus = $modelo_estatus_familiares->Modificacion_Estatus_Beneficiario($bandera);
		}
		if (isset($query_estatus)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUIENTES DATOS DE '.' '.$datos_nombre['datos_nombre'].', '.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}


	public function agregar_Cortesia()
	{

		$model = new Beneficiarios_Model();
		$modelentes = new Entes_Adscritos_model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['cedula_trabajador']          	= $data->cedula_trabajador;
		$datos['cedula']                     	= $data->cedula;
		$datos['nombre']                    	= $data->nombre_cortesia;
		$datos['apellido']                  	= $data->apellido_cortesia;
		$datos['nacionalidad']                = $data->nacionalidad;
		$datos['telefono']                   	= $data->telefono;
		$datos['observacion']                	= $data->observacion;
		$datos['sexo']                       	= $data->sexo;
		$datos['fecha_nac_cortesia']      	= $data->fechaconvertida;
		$datos['tipo_de_sangre']              = $data->tipodesangre;
		$datos['estado_civil']                = $data->estadocivil;
		$datos['ocupacion_id']                = $data->ocupacion_id;
		$datos['grado_intruccion_id']         = $data->grado_intruccion_id;
		$id_adscrito['id']         		= $data->id_adscrito;
		$datos['adscrito']         		= $data->check_adscrito;

		$query = $model->agregar_Cortesia($datos);
		if (isset($query)) {
			$mensaje = 1;
			if ($id_adscrito['id'] != null) {
				$buscar_id_cortesia = $model->buscar_id_cortesia();
				foreach ($buscar_id_cortesia->getResultArray() as $adscrito) {
					$datos_adscritos['id_cortesia'] = intval($adscrito['id']);
				}
				$datos_adscritos['id_adscrito'] = intval($data->id_adscrito);
				$insetar_cortesia_adscrita = $modelentes->insetar_cortesia_adscrita($datos_adscritos);
			}
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}
	public function actualizar_Cortesia($cedula_anterior = null)
	{
		$modelo = new Beneficiarios_Model();
		$model_auditoria = new Auditoria_sistema_Model();
		$modelentes = new Entes_Adscritos_model();
		$modelo_estatus_cortesia = new  Modificacion_Estatus_Beneficiarios_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		date_default_timezone_set('America/Caracas');
		$hora = date("H:i:s A");
		$datos['cedula_trabajador']          = $data->cedula_trabajador;
		$datos['cedula']                     = $data->cedula;
		$datos['nombre']                     = $data->nombre;
		$datos['apellido ']                  = $data->apellido;
		$datos['telefono']                   = $data->telefono;
		$datos['sexo']                       = $data->sexo;
		$datos['fecha_nac_cortesia']         = $this->formatearFecha($data->fecha_nac_cortesia);
		$datos['observacion']                = $data->observacion;
		$datos['nacionalidad']                = $data->nacionalidad;
		$datos['tipo_de_sangre']              = $data->tipo_de_sangre_id;
		$datos['estado_civil']              = $data->estado_civil_id;
		$datos['ocupacion_id']                 = $data->ocupacion_id;
		$datos['grado_intruccion_id']                 = $data->grado_intruccion_id;
		$datos['borrado']          = $data->estatus_modificado;
		$datos['adscrito']         		= $data->check_adscrito;
		$query = $modelo->actualizar_Cortesia($datos, $cedula_anterior);
		$datos['bandera']          				= $data->bandera_Modificacion;
		$bandera['id_usuario']          		= $data->id_usuario;
		$bandera['cedula']                  	= $data->cedula;
		$bandera['estatus']          			= $data->estatus_modificado;
		$bandera['motivo']          			= $data->observacion;
		$bandera['hora'] = $hora;
		$datos_modificados['datos_modificados'] = $data->datos_modificados;
		$datos_modificados['datos_modificados'] = strtoupper($datos_modificados['datos_modificados']);
		$datos_nombre['datos_nombre'] = $data->nombre_anterior.' '.$data->apellido_anterior;
		if ($datos['bandera'] == 'true') {
			$query_estatus = $modelo_estatus_cortesia->Modificacion_Estatus_Beneficiario($bandera);
		}
		if (isset($query)) {
			$mensaje = 1;
            $auditoria['accion'] = 'SE MODIFICARON LOS SIGUIENTES DATOS DE '.' '.$datos_nombre['datos_nombre'].', '.' '.$datos_modificados['datos_modificados'];
			$Auditoria_sistema_Model = $model_auditoria->agregar($auditoria);
			$datos_adscritos['id_adscrito'] = intval($data->id_adscrito);
			$datos_adscritos['id_cortesia']          = $data->id_cortesia;
			$buscar_cortesia_adscrita = $modelentes->buscar_cortesia_adscrita($datos_adscritos);
			if (empty($buscar_cortesia_adscrita)) {
				$agregar_cortesia_adscrita = $modelentes->insetar_cortesia_adscrita($datos_adscritos);
			} else {
				$actualizar_cortesia_adscrita = $modelentes->actualizar_cortesia_adscrita($datos_adscritos);
			}
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}

	/**************METODO PARA LLENAR EL SELECT PARENTESCO* ******/


	public function listar_Parentesco()
	{
		$model = new Beneficiarios_Model();
		$query = $model->getAllParentesco();
		if (empty($query->getResult())) {
			$parentesco = [];
		} else {
			$parentesco = $query->getResultArray();
		}
		echo json_encode($parentesco);
	}


	/**************METODO PARA LLENAR EL SELECT GRADO DE INSTRUCCION* ******/


	public function listar_Grado_Instruccion()
	{
		$model = new Beneficiarios_Model();
		$query = $model->getAllGrado_Instruccion();
		if (empty($query->getResult())) {
			$parentesco = [];
		} else {
			$parentesco = $query->getResultArray();
		}
		echo json_encode($parentesco);
	}
	public function listar_Ocupacion()
	{
		$model = new Beneficiarios_Model();
		$query = $model->getAllOcupacion();
		if (empty($query->getResult())) {
			$ocupacion = [];
		} else {
			$ocupacion = $query->getResultArray();
		}
		echo json_encode($ocupacion);
	}




	/**************METODO PARA LLENAR EL SELECT tipo se sangre* ******/


	public function listar_tipo_de_sangre()
	{
		$model = new Beneficiarios_Model();
		$query = $model->getAllTipodeSangre();
		if (empty($query->getResult())) {
			$tipodesangre = [];
		} else {
			$tipodesangre = $query->getResultArray();
		}
		echo json_encode($tipodesangre);
	}


	public function listar_estado_civil()
	{
		$model = new Beneficiarios_Model();
		$query = $model->getAllEstadoCivil();
		if (empty($query->getResult())) {
			$estadocivil = [];
		} else {
			$estadocivil = $query->getResultArray();
		}
		echo json_encode($estadocivil);
	}
}
