<?php

namespace App\Controllers;

use App\Models\Categoria_Model;
use CodeIgniter\API\ResponseTrait;
use App\Models\Medicamentos_patologias_Model;
use CodeIgniter\RESTful\ResourceController;

class Medicamentos_patologias_Controllers extends BaseController
{
	use ResponseTrait;

	public function agregar_medicamentos()
	{
		$model = new Medicamentos_patologias_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['n_historial']   = $data->n_historial;
		$datos['descripcion']   = $data->medicamentos;
		$datos['id_consulta']   = $data->id_consulta;
		$datos['id_especialidad']   = $data->id_especialidad;
		
		$query = $model->agregar($datos);
		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}
		return json_encode($mensaje);
	}


	public function listar_medicamentos($n_historial,$id_especialidad,$id_consulta)
	{

		if (!session('nombreUsuario')) {
			return redirect()->to(base_url() . '/index.php');
		}
		$model = new Medicamentos_patologias_Model();

		$query = $model->listar_medicamentos($n_historial,$id_especialidad,$id_consulta);
		if (empty($query)) {
			$impresion_diag = [];
		} else {
			$impresion_diag = $query;
		}
		echo json_encode($impresion_diag);
	}


	public function actualizar_medicamentos()
	{
		$model = new Medicamentos_patologias_Model();
		$data = json_decode(base64_decode($this->request->getPost('data')));
		$datos['id']   = $data->id;
		$datos['descripcion'] = $data->medicamentos;
		$datos['fecha_actualizacion'] = $data->today;
		$query = $model->actualizar_medicamentos($datos);

		if (isset($query)) {
			$mensaje = 1;
		} else {
			$mensaje = 0;
		}

		return json_encode($mensaje);
	}
}
