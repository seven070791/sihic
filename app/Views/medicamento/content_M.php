<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" /> -->
<link href="/css/medicamentos.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<div class="row ">
		<div class="col-md-5 col-sm-5 col-lg-5 col-xl-5">
		</div>
		<div class="col-md-4 col-sm-4 col-lg-4 col-xl-4">
			<h3 class="center"> Registro de Inventario </h3>
		</div>
		<div class="col-md-2 col-sm-2 col-lg-2 col-xl-2">
			<button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button>
			<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button>
			<input type="hidden" id="usuario" autocomplete="off" style="width:100px;" value='<?= session('nombreUsuario'); ?>'> &nbsp;
		</div>
	</div>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row">

		<div class="col-md-5 col-sm-5 col-lg-5 col-xl-5">
			<input type="hidden" id="categoria" autocomplete="off" style="width:100px;" value=> &nbsp;
			<div class="from-group">
				&nbsp;&nbsp;&nbsp;<label id="">
					<h5>Categoria</h5>
				</label>&nbsp;&nbsp;
				<select class="custom-select" style="width:200px;" id="cmbCategoria" autocomplete="off" required>
					<option value="0">seleccione</option>
				</select>&nbsp;

			</div>
		</div>
		<div class="col-md-1 col-sm-1 col-lg-1 col-xl-1">
			<div class="medic_cronico" id="medic_cronico">
				&nbsp;<label class="labelcronico_0">Crónico</label>
				<select class=" custom-select cronico_0" style="width:130px;" id="cmbcronico" name="cmbcronico" data-style="btn-primary">
					<option value="0" selected disabled>seleccione</option>
					<option value="1">SI</option>
					<option value="2">NO</option>
				</select>&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- <button id="btnfiltrar" class="btn btn-primary btn-sm">Filtrar</button> -->
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-lg-4 col-xl-4 botones" id="botones">
			<button id="btnfiltrar" class="btn btn-primary btn-sm">Filtrar</button>
			<button id="btnlimpiar" class="btn btn-primary btn-sm">Limpiar</button>
		</div>

	</div>


	<form action="<?php echo base_url(); ?>/Medicamento_Controllers/GenerarReportesPorFecha" method="POST" target="_blan">
		<div class="row">

	</form>

	<!-- <style>
   table.dataTable thead, table.dataTable tfoot {
           background: linear-gradient(to right, #1F4A7B,#1F4A7B,#1F4A7B);
       }
   </style> -->

	<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">

		<table class="display table-responsive"  id="table_medicamentos" style="width:100%">
			<thead>
				<tr>
					<td>Descripcion</td>
					<td>Cronico</td>
					<td>F.registro</td>
					<td>Control</td>
					<td>Categoria</td>
					<td>S.Minimo</td>
					<td>Entradas</td>
					<td>Salidas</td>
					<td>Stock</td>

					<td style="width: 130px;" class="text-center">Acciones</td>
				</tr>
			</thead>
			<tbody id="lista_medicamento">
			</tbody>
		</table>
	</div>

	<section class="form-login">

		<!-- Ventana Modal -->
		<div class="row">
			<div class="modal fade" id="modal" role="dialog" aria-labelledby="exampleModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="col-xs-8">
							<h4 class="modal-title center" id="modal">&nbsp;&nbsp;&nbsp;Registro de Inventario&nbsp;&nbsp;</h4>
						</div>
						<div class="col-xs-4">
							<button type="button" class="btn btn-success  btncompuestos" id="btncompuestos">(+)Compuestos</button>
						</div>

						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div>
								<input type="hidden" id='id' name='id'>
								<input type="hidden" id='prueba' name='id_unidadmedida'>
							</div>
							<div class="form-group">
								<label for="nombre0" class="control-label">Descripcion</label>
								<input type="text" class="form-control" onkeyup="mayus(this);" id="descripcion" name="descripcion" style="width:450px;" placeholder="" autocomplete="off" required>
								<br>
								<label for="nombre0" class="control-label">Tipo de Inventario</label>
								<select class="custom-select vm" style="width:200px;" id="cmbTipoMedicamento" autocomplete="off" data-placeholder="Seleccione" autocomplete="off" required>
									<option value="0">seleccione</option>
								</select>
								<br>
								<br>
								<div>
									<label for="medicamento" class="control-label">Presentacion</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<select class="custom-select vm" style="width:200px;" id="cmbPresentacion">
										<option value="0">seleccione</option>
									</select>
								</div>
								<br>
								<div>
									<label for="control" class="control-label"><span>*</span>Control</label>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<select class="custom-select vm" style="width:200px;" id="cmbControl" autocomplete="off" required>
										<option value="seleccione">seleccione</option>
									</select>
								</div>
								<br>
								<div>
									<label class="labeledad">Stock Minimo:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="text" class="sinborder" min="0" onkeypress="return valideKey(event);" id="stock_minimo" style="width:50px;" value='0'>&nbsp;&nbsp;&nbsp;

									<label class="labelfecha">Fecha Registro</label>
									<div class="input-group date" for="fecha">
										<input type="text" class="control fecha" disabled="disabled" name="fecha" autocomplete="off" id="fecha" style="width:103px;" style=" z-index: 1050 !important;">
										<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
										</div>
									</div>

								</div>


								<div>
									<label class="labelborrado" for="borrado" id="activo">Activo</label>
									<input type="checkbox" class="borrado" id="borrado" name="borrado" value='false'>
								</div>

								<div class="chk_cronico" style="display: block;">
									<label class="labelcronico" id="labelcronico">Medicamento Crónico</label>
									<input type="checkbox" class="cronico" id="cronico" value='false'>
								</div>
								<br>
								<br>

								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
									<button type="button" class="btn btn-primary btnGuardar" id="btnGuardar">Guardar datos</button>
									<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
								</div>


	</section>

	<script src="<?= base_url(); ?>/js/cdn/jquery-3.1.0.js"></script>

	<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script> -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
	<script>
		// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
		$(function() {
			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});


		});
	</script>
	<script>
		function mayus(e) {
			e.value = e.value.toUpperCase();
		}
	</script>

	<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->

	<script type="text/javascript">
		function valideKey(evt) {

			// code is the decimal ASCII representation of the pressed key.
			var code = (evt.which) ? evt.which : evt.keyCode;

			if (code == 8) { // backspace.
				return true;
			} else if (code >= 48 && code <= 57) { // is a number.
				return true;
			} else { // other keys.
				return false;
			}
		}
	</script>

	<script>
		$(document).ready(function() {

			var now = new Date();

			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);
			var today = day + "/" + month + "/" + now.getFullYear();
			$("#fecha").val(today);
		});
	</script>


</div>

	<?= $this->endSection(); ?>