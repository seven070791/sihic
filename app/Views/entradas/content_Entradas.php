<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/entradas.css" rel="stylesheet" type="text/css" />
<div class="container"><br />
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row col-md-12">
		<div class="row col-md-12">
			<h3> Registro de Entradas del Medicamento :</h3>
		</div>
		<h4>Descripcion:&nbsp;&nbsp;&nbsp;</h4>
		<div>
			<input type="text" class="sinborder" id="descripcion" disabled="disabled" style="width:600px;" value='<?php echo $medicamento; ?>'>
		</div>
		<h4>&nbsp;&nbsp;Control:&nbsp;&nbsp;&nbsp;&nbsp;</h4>
		<div>
			<input type="text" class="sinborder" disabled="disabled" style="width:200px;" value='<?php echo $control; ?>'>
		</div>
	</div>
	<div class="alert alert-success" style="display: none;"></div>
	<div class="alert alert-danger" style="display: none;"></div>
	<br>
	<div class="row">
		<div class="col-md-1">
			<button id="btnAgregar" class="btn btn-primary">Agregar</button>
		</div>
		<div class="col-md-2">
			<button id="btnRegresar" class="btn btn-secondary">Regresar</button>
		</div>
		<!-- ************* CAJAS DE TEXT************ -->
		<div class="col-md-4">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" disabled="disabled" class=" btn-info">
				<span class="badge badge-light">Entradas:</span>
			</button>
			<input id="total_entradas" class="sinborder" readonly="readonly" disabled="disabled" class="text" value='<?php echo $total_entradas; ?>' style="width:160px;">
		</div>
		<div class="col-md-3">
			<button type="button" disabled="disabled" class="btn-info">
				<span class="badge badge-light">Salidas:</span>
			</button>
			<input id="total_salidas" class="sinborder" readonly="readonly" disabled="disabled" class="text" value='<?php echo $total_salidas; ?>' style="width:160px;">
		</div>
		<div class="col-md-2">
			<button type="button" disabled="disabled" class=" btn-info">
				<span class="badge badge-light">Stock:</span>
			</button>
			<input id="total_stock" class="sinborder" readonly="readonly" disabled="disabled" class="text" value='<?php echo $total_stock; ?>' style="width:80px;">
		</div>
	</div>
	<div>
	</div>
	<br>


	<table class="display" id="table_entradas" cellspacing="2" width="100%">

		<thead>
			<tr>
				<td>Id</td>
				<td>Fecha Entradas</td>
				<td>Fecha Vencimiento</td>
				<td>Entradas</td>
				<td>Salidas</td>
				<td>Stock</td>
				<td>Acciones</td>
			</tr>
		</thead>
		<tbody id="lista_medicamento">
		</tbody>
	</table>

	<!-- Ventana Modal -->
	<div class="modal " id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="title_entradas">Registro de Entrada del Medicamento</h5>
				</div>
				<br>
				<div class="modal-body modal_reversar">
					<div id="datos_ajax_register"></div>
					<div>
						<input type="hidden" id='id_medicamento' name='id_medicamento' value='<?php echo $id; ?>'>
						<input type="hidden" id='id_entrada' name='id_entrada' value=''>
					</div>
					<label>Fecha de Entrada</label>
					<div class="input-group date" for="fecha">
						<input type="text" class="form-control " autocomplete="off" name="fecha" id="fecha" style=" z-index: 1050 !important;">
						<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
						</div>
					</div>
					<label>Fecha de Vencimiento</label>
					<div class="input-group date" for="fecha1">
						<input type="text" class="form-control " autocomplete="off" name="fecha1" id="fecha1" style=" z-index: 1050 !important;">
						<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
						</div>
					</div>
					<div class="form-group">
						<label for="nombre0" class="control-label">Cantidad</label>
						<input type="text" class="form-control" autocomplete="off" id="cantidad" onkeypress="return valideKey(event);" required pattern="[0-9]+" name="cantidad" style="width:250px;" placeholder="">
					</div>
				</div>

				<div class="observacion" style="display: none;">
					<label for="labelobservacion" id="labelobservacion">Observacion</label>
					<textarea class="bodersueve" id="observacion" name="observacion"> </textarea>
					<br><br><br>
					<label for="borrado" id="activo">Reversar</label>
					<input type="checkbox" class="borrado" id="borrado" name="borrado" value='false'>
				</div>

				<br>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
					<button type="button" class="btn btn-primary" disabled="disabled" id="btnActualizar">Actualizar datos</button>
					<button type="button" class="btn btn-primary" style="display:none" id="btnActualizar_fechav">Actualizar</button>
					<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
				</div>

			</div>
		</div>

		<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
		<script>
			// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
			$(function() {
				$("#fecha").datepicker({
					dateFormat: 'dd/mm/yy',
					changeMonth: true,
					changeYear: true,
					yearRange: '1920:2050',
				});
				$("#fecha1").datepicker({
					dateFormat: 'dd/mm/yy',
					changeMonth: true,
					changeYear: true,
					yearRange: '1920:2050',
				});

			});
		</script>


		<script>
			$(document).ready(function() {

				var now = new Date();

				var day = ("0" + now.getDate()).slice(-2);
				var month = ("0" + (now.getMonth() + 1)).slice(-2);
				var today = day + "/" + month + "/" + now.getFullYear();
				var today2 = day + "/" + month + "/" + now.getFullYear();
				// var today= (day)+"-"+(month)+"-"+now.getFullYear();
				// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
				$("#fecha").val(today);
				$("#fecha1").val(today);
			});
		</script>

		<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
		<script type="text/javascript">
			function valideKey(evt) {

				// code is the decimal ASCII representation of the pressed key.
				var code = (evt.which) ? evt.which : evt.keyCode;

				if (code == 8) { // backspace.
					return true;
				} else if (code >= 48 && code <= 57) { // is a number.
					return true;
				} else { // other keys.
					return false;
				}
			}
		</script>

		<?= $this->endSection(); ?>