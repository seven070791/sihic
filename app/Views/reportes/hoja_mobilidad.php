<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->
<link href="/css/vista_morbilidad.css" rel="stylesheet" type="text/css" />
<div class="container">
	<BR>
	<div class="row ">
		<div class="col-5">
		</div>
		<div class="col-4">
			<h5 class="center">HOJA DE MORBILIDAD</h5>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<label for="min">Desde</label>&nbsp;
			<input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('YY-MM-DD'); ?>" name="desde" id="desde">&nbsp;&nbsp;
			<label for="hasta">Hasta</label>&nbsp;&nbsp;
			<input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('YY-MM-DD'); ?>" name="hasta" id="hasta">&nbsp;
			&nbsp;&nbsp;
			<label class="labelbeneficiario">T.Beneficiario</label>&nbsp;
			<select class="custom-select " style="width:145px;" id="beneficiario" name="beneficiario" data-style="btn-primary">
				<option value="0" selected disabled>seleccione</option>
				<option value="1">TITULAR</option>
				<option value="2">FAMILIAR</option>
				<option value="3">CORTESIA</option>
			</select>&nbsp;&nbsp;
			<label class="labelsexo">Sexo</label>&nbsp;
			<select class="custom-select " style="width:145px;" id="sexo" name="sexo" data-style="btn-primary">
				<option value="0" selected disabled>seleccione</option>
				<option value="1">MASCULIINO</option>
				<option value="2">FEMENINO</option>
			</select>

		</div>
	</div>

	<br>
	<div class="row">
		<div class="col-md-12">
			<label class="labelespecialidad">Especialidad&nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:250px;" id="especialidad" autocomplete="off" required>
				<option value="seleccione">Seleccione</option>
			</select>&nbsp;&nbsp;
			<label class="labelmedicotratante" class="control-label">Medico &nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:200px;" id="medico" autocomplete="off" required>
				<option value="0">Seleccione</option>
			</select>
			<label class="labelbeneficiario">Entes Adscritos</label>&nbsp;&nbsp;
			<select class="custom-select " disabled="disabled" style="width:250px;" id="entes_adscritos" name="entes_adscritos" data-style="btn-primary">
				<option value="0">seleccione</option>
			</select>
		</div>

	</div>
	<br>
	<div class="row">
		<div class="col-md-5">
		</div>
		<div class="col-lg-1">
			<button id="btnfiltrar" style="display:block;" class="btn btn-primary btn-sm">Filtrar </button>
		</div>
		<div class="col-lg-1">
			<button id="btnlimpiar" style="display:block;" class="btn btn-primary btn-sm">Limpiar</button>
			<button id="filtrocortesia" style="display:none;" class="btn btn-primary btn-sm">Filtrar</button>
		</div>

	</div>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>


	<div class="row" class="morbilidad_normal">

		<br>
		<div class=" col-lg-12 morbi">
			<table class="display" id="table_morbilidad" style="width:120%">
				<thead>
					<tr>
						<td class="text-center" style="width: 220px;">FECHA </td>
						<td class="text-center" style="width: 70px;">CATG</td>
						<td class="text-center" style="width: 100px;">CEDULA</td>
						<td class="text-center" style="width: 20%">NOMBRE Y APELLIDO</td>
						<td class="text-center" style="width: 20px;">EDAD</td>
						<td class="text-center" style="width: 20px;">SEXO</td>
						<td class="text-center" style="width: 40px;">B</td>
						<td class="text-center" style="width: 40px;">PARENTESCO</td>
						<!-- <td  class="text-center"style="width: 100px;">ESTATUS</td> -->
						<td class="text-center" style="width: 20%">DEPARTAMENTO</td>
						<td class="text-center" style="width: 220px;">MOTIVO DE CONSULTA </td>
						<td class="text-center" style="width: 220px;">DIAGNOSTICO </td>
						<td class="text-center" style="width: 220px;">ESPECIALIDAD</td>
						<td class="text-center" style="width: 220px;">MEDICO</td>
						<td class="text-center" style="width: 220px;">REPOSO</td>
					</tr>
				</thead>
				<tbody id="lista_de_unida_medida">
				</tbody>
			</table>
		</div>
	</div>


	<div class="row" class=" morbilidad_entes">

		<br>
		<div class="col-lg-12 ">
			<table class="display" id="table_morbilidad_entes" style="width:120%">
				<thead>
					<tr>
						<td class="text-center" style="width: 220px;">FECHA </td>
						<td class="text-center" style="width: 70px;">CATG</td>
						<td class="text-center" style="width: 100px;">CEDULA</td>
						<td class="text-center" style="width: 20%">NOMBRE Y APELLIDO</td>
						<td class="text-center" style="width: 20px;">EDAD</td>
						<td class="text-center" style="width: 20px;">SEXO</td>
						<td class="text-center" style="width: 40px;">B</td>
						<td class="text-center" style="width: 40px;">PARENTESCO</td>
						<!-- <td  class="text-center"style="width: 100px;">ESTATUS</td> -->
						<td class="text-center" style="width: 20%">DEPARTAMENTO</td>
						<td class="text-center" style="width: 220px;">MOTIVO DE CONSULTA </td>
						<td class="text-center" style="width: 220px;">DIAGNOSTICO </td>
						<td class="text-center" style="width: 220px;">ESPECIALIDAD</td>
						<td class="text-center" style="width: 220px;">MEDICO</td>
						<td class="text-center" style="width: 220px;">REPOSO</td>
					</tr>
				</thead>
				<tbody id="lista_de_unida_medida_entes">
				</tbody>
			</table>
		</div>
	</div>

</div>




<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>/js/moment.min.js"></script>




<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "-" + month + "-" + now.getFullYear();
		$("#fecha").val(today);
	});
</script>


<script>
	$(function() {
		//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
		$("#fecha").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>

<?= $this->endSection(); ?>