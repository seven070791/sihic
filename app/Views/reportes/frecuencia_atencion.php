<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/vista_frecuencia_atencion.css" rel="stylesheet" type="text/css" />
<div class="container">
	<BR>
	<div class="row ">
		<div class="col-5">
		</div>
		<div class="col-4">
			<h5 class="center">Frecuencia de Atencion</h5>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<label for="min">Desde</label>&nbsp;
			<input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('YY-MM-DD'); ?>" name="desde" id="desde">&nbsp;&nbsp;
			<label for="hasta">Hasta</label>&nbsp;&nbsp;
			<input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('YY-MM-DD'); ?>" name="hasta" id="hasta">&nbsp;
			&nbsp;&nbsp;
			<label class="labelbeneficiario">T.Beneficiario</label>&nbsp;
			<select class="custom-select " style="width:145px;" id="beneficiario" name="beneficiario" data-style="btn-primary">
				<option value="0" selected disabled>seleccione</option>
				<option value="1">TITULAR</option>
				<option value="2">FAMILIAR</option>
				<option value="3">CORTESIA</option>
			</select>&nbsp;&nbsp;
			<label class="labelsexo">Sexo</label>&nbsp;
			<select class="custom-select " style="width:145px;" id="sexo" name="sexo" data-style="btn-primary">
				<option value="0" selected disabled>seleccione</option>
				<option value="1">MASCULIINO</option>
				<option value="2">FEMENINO</option>
			</select>

		</div>
	</div>

	<br>

	<br>
	<div class="row">
		<div class="col-md-5">
		</div>
		<div class="col-lg-1">
			<button id="btnfiltrar" style="display:block;" class="btn btn-primary btn-sm">Filtrar </button>
		</div>
		<div class="col-lg-1">
			<button id="btnlimpiar" style="display:block;" class="btn btn-primary btn-sm">Limpiar</button>
			<button id="filtrocortesia" style="display:none;" class="btn btn-primary btn-sm">Filtrar</button>
		</div>

	</div>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>


	<div class="row" class="morbilidad_normal">


		<div class=" col-lg-12 morbi">
			<table class="display" id="table_frecuencia_atencion" style="width:100%">
				<thead>
					<tr>
						<td class="text-center" style="width: 15%">CEDULA</td>
						<td class="text-center" style="width: 40%">NOMBRE Y APELLIDO</td>
						<td class="text-center" style="width: 5%;">EDAD</td>
						 <td class="text-center" style="width: 5%;">SEXO</td>
						<td class="text-center" style="width: 40px;">B</td>
						<td class="text-center" style="width: 40px;">PARENTESCO</td>
						<td class="text-center" style="width: 40px;">FRECUENCIA</td>
						
					
						<!-- <td class="text-center" style="width: 40px;">CANTIDAD ANTENC.</td> -->
					</tr>
				</thead>
				<tbody id="listar_frecuencias">
				</tbody>
			</table>
		</div>
	</div>

<!-- Ventana Modal -->
<form action="" method="post" name="">
	<div class=" modal fade" id="modal" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row col-md-12">
							<div class="form-group has-feedback" >
							<h5 class="center">Detalles de Atencion</h5>										
								<fieldset>
								<div class=" col-lg-12 morbi">
									
								<table class="display" id="table_Detalle_frecuencia_atencion" style="width:100%">
								<thead>
									<tr>
									<td  class="text-center"style="width: 100px;">NºCita</td>			
								<td  class="text-center"style="width: 220px;">Nº Historial</td> 
								<td  class="text-center"style="width: 220px;">Medico</td>
								<td  class="text-center"style="width: 220px;">Especialidad</td>
								<td  class="text-center"style="width: 130px;">Fecha Consulta</td>	
							
										<!-- <td class="text-center" style="width: 40px;">CANTIDAD ANTENC.</td> -->
									</tr>
								</thead>
							<tbody id="detalles_frecuencia">
							</tbody>
						</table>
						
								</fieldset>
								<br>
								<button id="btnCerrar" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button> 
							</div>
						</div>
					</div>
					
				</div>
				
								
						
					
			</div>
			
		</div>
	</div>							
</form>
	<!-- <div class="row" class=" morbilidad_entes">

		<br>
		<div class="col-lg-12 ">
			<table class="display" id="table_morbilidad_entes" style="width:110%">
				<thead>
					<tr>
						<td class="text-center" style="width: 10%">FECHA </td>
						<td class="text-center" style="width: 70px;">CATG</td>
						<td class="text-center" style="width: 100px;">CEDULA</td>
						<td class="text-center" style="width: 30%">NOMBRE Y APELLIDO</td>
						<td class="text-center" style="width: 20px;">EDAD</td>
						<td class="text-center" style="width: 20px;">SEXO</td>
						<td class="text-center" style="width: 40px;">B</td>
						<td class="text-center" style="width: 40px;">PARENTESCO</td>
					</tr>
				</thead>
				<tbody id="lista_de_unida_medida_entes">
				</tbody>
			</table>
		</div>
	</div> -->

</div>




<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script> -->
<script type="text/javascript" src="<?php echo base_url(); ?>/js/moment.min.js"></script>




<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "-" + month + "-" + now.getFullYear();
		$("#fecha").val(today);
	});
</script>


<script>
	$(function() {
		//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
		$("#fecha").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
	});
</script>

<?= $this->endSection(); ?>