<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- Required meta tags -->
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<link href="/css/vista_citas_consultas.css" rel="stylesheet" type="text/css" />





<div class="container">
	<br />
	<div class="row ">
		<div class="col-md-5 col-sm-5 col-lg-5 col-xl-5">
		</div>
		<div class="col-md-5 col-sm-5 col-lg-5 col-xl-5">
			<h3 class="center">Consultas /Citas </h3>
		</div>
		<div class="col-md-3 col-sm-3 col-lg-3 col-xl-3">
		</div>
	</div>

	<div class="row">

		<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
			<label for="min">Desde</label>&nbsp;
			<input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
			<label for="hasta">Hasta</label>&nbsp;&nbsp;
			<input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;
			&nbsp;&nbsp;
			<label class="labelcita" class="control-label">Control &nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:200px;" id="control" autocomplete="off" required>
				<option value="0">Seleccione</option>
				<option value="1">Cita</option>
				<option value="2">Consulta</option>
			</select>&nbsp;&nbsp;

			<label class="labelasitio" class="control-label">Asistío &nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:200px;" id="asistio" autocomplete="off" required>
				<option value="0">Seleccione</option>
				<option value="1">Si</option>
				<option value="2">No</option>
				<option value="3">En espera</option>
			</select>


		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-7 col-sm-7 col-lg-7 col-xl-7">
			<label class="labelespecialidad">Especialidad&nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:250px;" id="especialidad" autocomplete="off" required>
				<option value="Seleccione">Seleccione</option>
				</h5>
			</select>&nbsp;&nbsp;
			<label class="labelmedicotratante" class="control-label">Medico &nbsp;&nbsp; </label>
			<select class="custom-select vm" style="width:200px;" id="medico" autocomplete="off" required>
				<option value="0">Seleccione</option>
			</select>
		</div>
		<div class="col-md-3 col-sm-3 col-lg-3 col-xl-3">
			<button id="btnfiltrar" style="visibility:visible;" class="btn btn-primary btn-sm">Filtrar </button>&nbsp;
			<button id="btnlimpiar" class="btn btn-primary btn-sm">Limpiar</button>&nbsp;
			<!-- <button id="btnfiltrocortesia" class="btn btn-outline-dark btn-sm">Filtrar por Entes</button>&nbsp; -->
			<button id="filtrocortesia" style="visibility:hidden;" class="btn btn-primary btn-sm">Filtrar</button>&nbsp;
		</div>
	</div>




	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

	<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">

		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<table id="table_consulta_citas" class="display" style="width:115%">
			<thead>
				<tr>
					<!-- <td  class="text-center"style="width: 50px;">NºCita</td>	
					 -->
					<td class="text-center" style="width: 50px;">Control</td>
					<td class="text-center" style="width: 80px;">Nº Historial</td>
					<td class="text-center" style="width: 290px;">Beneficiario</td>
					<td class="text-center" style="width: 220px;">Medico</td>
					<td class="text-center" style="width: 220px;">Especialidad</td>
					<!-- <td  class="text-center"style="width: 130px;">Fecha Creacion</td>	 -->
					<td class="text-center" style="width: 100px;">Fecha </td>
					<!-- <td  class="text-center"style="width: 7%;">Asistencia</td> -->
					<td class="text-center" style="width: 7%;">Asistio</td>
					<td class="text-center" style="width: 30px;">Control</td>


				</tr>
			</thead>
			<tbody id="consulta_citas">
			</tbody>
		</table>
	</div>



</div>
</div>

<style type="text/css">
	option {
		font-family: verdana, arial, helvetica, sans-serif;
		font-size: 14px;
		border: 3;
		border-radius: 20%;
	}

	option {
		border-color: blueviolet;
	}
</style>


</body>

</html>




<script>
	var botones = document.querySelectorAll('.btn-expandir');
	var texto_expandir = document.querySelectorAll('.texto_expandir');
	botones.forEach((elemento, clave) => {
		elemento.addEventListener('click', () => {
			texto_expandir[clave].classList.toggle("abrir_cerrar")
		});

	});
</script>

<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->

<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
 -->





<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>



<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "-" + month + "-" + now.getFullYear();
		// var today= (day)+"-"+(month)+"-"+now.getFullYear();
		// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
		$("#fecha_del_dia").val(today);
	});
</script>
<?= $this->endSection(); ?>