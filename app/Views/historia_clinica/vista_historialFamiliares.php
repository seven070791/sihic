<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/vista_historial.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br />
	<div class="row col-md-12">

		<div class="row col-md-12">
			<h5>Expediente / Familiar </h5>
		</div>
		<input type="hidden" class="sinborder" disabled="disabled" id="cedulat" class="form-control" style="width:200px;" value='<?php echo $cedula_trabajador; ?>'>

		<input type="hidden" disabled="disabled" id="tipo_beneficiario" class="form-control" style="width:50px;" value='F'>
		<label class="labeltitular">TITULAR</label>&nbsp;&nbsp;
		<div>
			<input type="text" class="sinborder" disabled="disabled" id="datos_nombre" class="form-control" style="width:600px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
		</div>&nbsp;&nbsp;
		<label class="labelcedula">CEDULA</label>&nbsp;&nbsp;
		<div>
			<input type="text" class="sinborder" disabled="disabled" id="cedula" class="form-control" style="width:200px;" value='<?php echo $cedula; ?>'>
		</div>
	</div>
	<button id="btnAgregar_familiar" class="btn btn-primary">Agregar</button>
	<button id="btnRegresar_hacia_titulares" class="btn btn-secondary">Regresar</button>
	<div class="alert alert-success" style="display: none;"></div>
	<div class="alert alert-danger" style="display: none;"></div>
	<br><br><br>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>

	<div class="col-lg-12">

		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<table class="display" id="table_historial" style="width:100%" style="margin-top: 20px">
			<thead>
				<tr>
					<td class="text-center" style="width: 100px;">NºHistorial</td>
					<td class="text-center" style="width: 300px;">Nombre</td>
					<td class="text-center" style="width: 300px;">Apellido</td>
					<td class="text-center" style="width: 100px;">Cedula</td>
					<td class="text-center" style="width: 200px;">Fecha Nacimiento</td>
					<td class="text-center" style="width: 170px;">Fecha Registro</td>
					<td class="text-center" style="width: 200px;">Acciones</td>
				</tr>
			</thead>
			<tbody id="historial_medico">
			</tbody>
		</table>
	</div>
</div>
<!-- Ventana Modal -->


<form action="" method="post" name="">
	<div class=" modal fade" id="modal_historial_medico" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog  modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row ">
						<div>
							<div class="form-group has-feedback" id="cajas1">
								<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?> ' />
								<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?> ' />
								<label class="labeltitulo">
									<h5>Registro de Historial Medico</h5>
								</label> &nbsp;
								&nbsp;&nbsp;
								<label class="labelfecha">Fecha</label>
								<input type="text" id="fecha_del_dia" disabled="disabled" value="" />&nbsp;&nbsp;&nbsp;
								<label class="labelhistorial">
									<h5><u>Nº Historial</u> </h5>
								</label>
								&nbsp;&nbsp;<input type="text" disabled="disabled" id="numeroHistorial" style="width:100px;" value='<?php echo 'F' . '' . $cedula; ?>'>
								<form>

									<fieldset>
										<legend class="legend">Datos del Beneficiario </legend>
										<div>
											<label class="labelCedula">Cedula</label>
											<input type="text" id="cedulaF" disabled="disabled" style="width:100px;" value='<?php echo $cedula; ?>'>

											<label class="labelnombreApellido">Nombre y Apellido</label>
											<input type="text" id="nombrepellidoT" disabled="disabled" style="width:300px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
										</div>
										<div>
											<label class="labelfechaNacimiento">Fecha De Nacimiento</label>
											<input type="text" id="fecha_nacimiento" disabled="disabled" value='<?php echo $fecha_nac_familiares; ?> ' />
										</div>

										<input type="hidden" id="cedulatrabajador" disabled="disabled" class="control" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
							</div>
							<div class="row ">
								<div class="col-3">
								</div>
								<div class="col-6">
								</div>
								<div class="col-3">
									<button id="btnAgregarHistorial" class="btn-primary btn-sm">Agregar</button>&nbsp;&nbsp;
									<button type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
								</div>


								</fieldset>


							</div>

						</div>





</form>



<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>

<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script> -->
<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "-" + month + "-" + now.getFullYear();
		// var today= (day)+"-"+(month)+"-"+now.getFullYear();
		// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
		$("#fecha_del_dia").val(today);
	});
</script>

<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		// $("#modal_editar").modal("show");

		$("#fecha").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true,
			yearRange: '1930:2022',
			maxDate: '+30Y',
			inline: true
		});
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true,
			yearRange: '1930:2022',
			maxDate: '+30Y',
			inline: true
		});


	});
</script>
<?= $this->endSection(); ?>