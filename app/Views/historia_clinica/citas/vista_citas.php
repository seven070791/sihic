<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<!-- Required meta tags -->
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


<link href="/css/vista_consultas.css" rel="stylesheet" type="text/css" />

<style>
	table.dataTable thead,
	table.dataTable tfoot {
		background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
	}
</style>



<div class="container">
	<br />
	<div class="row ">
		<div class="col-5">
		</div>
		<div class="col-5">
			<h3 class="center">Registro /Citas Titulares</h3>
		</div>
		<br>
		<div class="col-3">
			<button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button> &nbsp;&nbsp;
			<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button>

		</div>
	</div>

	<br>

	<div class="col-lg-12">

		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<table id="table_historial" class="display" style="width:105%">
			<thead>
				<tr>
					<!-- <td  class="text-center"style="width: 50px;">NºCita</td>			 -->
					<td class="text-center" style="width: 100px;">Nº Historial</td>
					<td class="text-center" style="width: 220px;">Medico</td>
					<td class="text-center" style="width: 220px;">Especialidad</td>
					<td class="text-center" style="width: 130px;">Fecha Creacion</td>
					<td class="text-center" style="width: 130px;">Fecha Asistencia</td>
					<td class="text-center" style="width: 130px;">Asistio</td>
				</tr>
			</thead>
			<tbody id="consultas_titulares">
			</tbody>
		</table>
	</div>

	<input type="hidden" id="id_user" disabled="disabled" value='<?php echo (session('id_user')) ?> ' />
	<input type="hidden" disabled="disabled" id="numeroHistorialDatatable" class="form-control" style="width:100px;" value='<?php echo $n_historial; ?>'>
	<!-- Ventana Modal -->
	<input type="hidden" id="tipo_beneficiario" disabled="disabled" value='<?php echo $tipo_beneficiario; ?> ' />
	<div class=" modal fade " id="modal_citas" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog  modal-lg " role="document">
			<div class="modal-content ">
				<div class="modal-header ">
					<h5><u>Registro de Citas</u></h5>
				</div>
				<div class="row ">
					<div class="col-sm-1">

					</div>
					<div class="col-lg-10" id="consultas">
						<div>
							<label class="labelfecha">Fecha de Asistencia</label>
							<input type="text" class="bodersueve" id="fecha_del_dia" style="width:120px;" value="" />
							<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?> ' />
							<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?> ' />
							<br>
							<fieldset>
								<legend class="legend">Datos del Beneficiario </legend>
								<label class="labelCedula">Cedula</label>
								<input type="text" id="cedulaT" disabled="disabled" style="width:80px;" value='<?php echo $cedula_trabajador; ?>'>&nbsp;&nbsp;
								<label class="labelnombreApellido">Nombre y Apellido </label>
								<input type="text" id="nombrepellidoT" disabled="disabled" style="width:365px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
								<div>
									<label class="labelhistorial"><u>Nº Historial</u> </label>&nbsp;&nbsp;
									<input type="text" disabled="disabled" id="numeroHistorial" style="width:100px;" value='<?php echo 'T' . '' . $cedula_trabajador; ?>'>&nbsp;&nbsp;
									<label class="labelfechaNacimiento">Fecha De Nacimiento</label>
									<input type="text" id="fecha_nacimiento" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nacimiento; ?> ' />&nbsp;&nbsp;

									<label class="labelEdad">Edad</label>&nbsp;&nbsp;
									<input type="text" id="edad" disabled="disabled" style="width:100px;" value='<?php echo $edad_actual; ?>'>
									<label class="labelsexo">Sexo</label>&nbsp;&nbsp;
									<input type="text" disabled="disabled" id="sexo" style="width:50px;" value='<?php echo  $sexo; ?>'>
									<label class="labeldepartamento">Departamento</label>
									<input type="text" id="departamento" disabled="disabled" style="width:320px;" value='<?php echo $ubicacion_administrativa; ?>'>

									<label class="labeltelefono">Nº Telefono</label>&nbsp;&nbsp;
									<input type="text" disabled="disabled" id="telefono" class="control" style="width:115px;" value='<?php echo  $telefono; ?>'>&nbsp;&nbsp;


								</div>

								<label class="labelespecialidad">Especialidad&nbsp;&nbsp; </label>
								<select class="custom-select vm" style="width:250px;" id="especialidad" autocomplete="off" required>
									<option value="seleccione">seleccione</option>
									</h5>
								</select>
								<label class="labelmedicotratante" class="control-label">Medico Tratante &nbsp;&nbsp; </label>
								<select class="custom-select vm" style="width:200px;" id="cmbmedicosreferidos" autocomplete="off" required>
									<option value="0">seleccione</option>
								</select>
								<br>
								<div>
							</fieldset>
							<br>
							<span class="labelimpresiond"><b>MOTIVO DE CONSULTA</b>: </span>
							<input type="text" id="motivo_consulta" onkeyup="mayus(this);" class="control motivo_consulta" style="width:570px;" autocomplete="off">
							<br>
							<fieldset>
								<legend class="legend">Signos vitales </legend>
								<div>
									<label class="labelpeso">PESO:</label>
									<input type="number" id="peso" class="bodersueve" min="0" value="0" name="peso" style="width:50px;">
									<label class="labelkg">(KG)</label>&nbsp;&nbsp;
									<label class="labeltalla">TALLA:</label>
									<input type="number" id="talla" class="bodersueve" min="0" value="0" name="talla" style="width:50px;">
									<label class="labelcm">(CM)</label>&nbsp;&nbsp;
									<label class="labelspo2">SPO2:</label>
									<input type="number" id="spo2" class="bodersueve" min="0" value="0" name="spo2" style="width:50px;">&nbsp;&nbsp;
									<label class="labelfc">FC:</label>
									<input type="number" id="frecuencia_c" class="bodersueve" min="0" value="0" name="fc" style="width:50px;">
									<label class="labelfr">(X) &nbsp;&nbsp;FR:</label>&nbsp;&nbsp;
									<input type="number" id="frecuencia_r" class="bodersueve" min="0" value="0" name="fr" style="width:50px;">
									<label class="labelx">(X)</label>
								</div>

								<label class="labeltemperatura">TEMPERATURA:</label>
								<input type="number" id="temperatura" class="bodersueve" min="0" value="0" name="temperatura" style="width:50px;">&nbsp;&nbsp;
								<label class="labelc">(ºC)</label>&nbsp;&nbsp;
								<label class="labelta">T/A:</label>
								<input type="number" id="ta_alta" class="bodersueve" min="0" placeholder="t.a" name="ta" style="width:50px;">
								<input type="number" id="ta_baja" class="bodersueve" min="0" placeholder="t.b" name="ta" style="width:50px;">
								<label class="labelmmhg">(MMHg)</label>
								<input type="text" id="Tbenficiario" disabled="disabled" style="width:100px;" value='Titular'> &nbsp;&nbsp;
								<label class="labeltipodesangre">Imc</label>
								<input type="number" class="bodersueve" id="imc" class="bodersueve" min="0" value="0" style="width:50px;">



							</fieldset>




							<div class="modal-footer">
								<button id="btnGuardar" class="btn-primary btn-sm">Guardar</button>
								<button id="btnCerrar" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

</div>
</div>

<style type="text/css">
	option {
		font-family: verdana, arial, helvetica, sans-serif;
		font-size: 14px;
		border: 3;
		border-radius: 20%;
	}

	option {
		border-color: blueviolet;
	}
</style>


</body>

</html>




<script>
	var botones = document.querySelectorAll('.btn-expandir');
	var texto_expandir = document.querySelectorAll('.texto_expandir');
	botones.forEach((elemento, clave) => {
		elemento.addEventListener('click', () => {
			texto_expandir[clave].classList.toggle("abrir_cerrar")
		});

	});
</script>

<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->

<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

<!-- datatables-->
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<!-- datatables extension SELECT -->
<script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>

<!-- extension BOTONES -->
<script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>




<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha_del_dia").datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});
		$("#fecha1").datepicker({
			dateFormat: 'dd-mm-yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>

<script>
	function mayus(e) {
		e.value = e.value.toUpperCase();
	}
</script>

<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "-" + month + "-" + now.getFullYear();
		// var today= (day)+"-"+(month)+"-"+now.getFullYear();
		// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
		$("#fecha_del_dia").val(today);
	});
</script>
<?= $this->endSection(); ?>