<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/vista_ConsultasCortesia.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br />

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row ">
		<div class="col-4">
		</div>
		<div class="col-5">
			<h3 class="center">Registro Citas / Cortesia</h3>
		</div>
		<br>
		<div class="col-4">
			<button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button> &nbsp;&nbsp;
			<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button>

		</div>
	</div>

	<br>
	<input type="hidden" id="id_user" disabled="disabled" value='<?php echo (session('id_user')) ?> ' />
	<div class="col-lg-12">
		<input type="hidden" disabled="disabled" id="numeroHistorialDatatable" style="width:100px;" value='<?php echo $n_historial; ?>'>
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<table class="display" id="table_historial" style="margin-top: 20px">
			<thead>
				<tr>
					<td class="text-center" style="width: 50px;">NºCita</td>
					<td class="text-center" style="width: 100px;">Nº Historial</td>
					<td class="text-center" style="width: 220px;">Medico</td>
					<td class="text-center" style="width: 220px;">Especialidad</td>
					<td class="text-center" style="width: 130px;">Fecha Creacion</td>
					<td class="text-center" style="width: 130px;">Fecha Asistencia</td>
					<td class="text-center" style="width: 100px;">Acciones</td>
				</tr>
			</thead>
			<tbody id="consultas_cortesia">
			</tbody>
		</table>
	</div>
</div>


<!-- Ventana Modal -->
<input type="hidden" id="tipo_beneficiario" disabled="disabled" value='<?php echo $tipo_beneficiario; ?>' />
<div class=" modal fade" id="modal_citas" tabindex="-1" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="row ">
					<div class="col-md-12">
						<div class="form-group has-feedback" id="cajas1">
							<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?>' />
							<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?>' />
							<label class="labeltitulo">
								<h4>Registro Citas / Cortesia</h4>
							</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<label class="labelfecha">Fecha de Asistencia</label>
							<input type="text" id="fecha_del_dia" style="width:120px;" value="" />

							<fieldset>
								<legend class="legend"><b>Datos del Trabajador</b></legend>

								<div>
									<label class="labelnombreApellido">Nombre y Apellido</label>
									<input type="text" id="nombrepellidoT" disabled="disabled" style="width:300px;" value='<?php echo $nombre_titular . ' ' . $apellido_titular; ?>'>
								</div>
								<label class="labelCedula">Cedula</label>
								<input type="text" id="cedulaT" disabled="disabled" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
								<label class="labeldepartamento">Departamento</label>
								<input type="text" id="departamento" disabled="disabled" style="width:300px;" value='<?php echo $departamento; ?>'>
						</div>


						</fieldset>

						<fieldset>
							<legend class="legend"><b>Datos del Beneficiario</b></legend>
							<div>
								<label class="labelnombreApellidoB">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" id="nombrepellidoB" disabled="disabled" style="width:300px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
								&nbsp;&nbsp;&nbsp;&nbsp;<label class="labelhistorial"><u>Nº Historial</u> </label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" disabled="disabled" id="numeroHistorial" class="control" style="width:120px;" value='<?php echo 'C' . '' . $cedula; ?>'>
							</div>
							<div>
								<label class="labelfechaNacimiento">Fecha De Nacimiento</label>
								<input type="text" id="fecha_nacimiento" class="control" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nac_cortesia; ?> ' />
								&nbsp;&nbsp;&nbsp;&nbsp;<label class="labelCedulab">Cedula</label>
								<input type="text" id="cedulab" disabled="disabled" class="control" style="width:100px;" value='<?php echo $cedula; ?>'>
								&nbsp;&nbsp;&nbsp;&nbsp;<label class="labelbeneficiario">Tipo Beneficiario</label>
								<input type="text" id="Tbenficiario" disabled="disabled" class="control" style="width:100px;" value='Cortesia'>
							</div>


							<label class="labelEdad">Edad</label>
							<input type="text" id="edad" disabled="disabled" class="control" style="width:100px;" value='<?php echo $edad_actual; ?>'>
							<label class="labeltelefono">Nº Telefono</label>
							<input type="text" disabled="disabled" id="telefono" class="control" style="width:120px;" value='<?php echo  $telefono; ?>'>
							<label class="labelsexo">Sexo</label>
							<input type="text" disabled="disabled" id="sexo" class="control" style="width:50px;" value='<?php echo  $sexo; ?>'>
							<div>
								<label class="labelespecialidad">Especialidad&nbsp;&nbsp; </label>
								<select class="custom-select" style="width:290px;" id="especialidad" autocomplete="off" required>
									<option value="seleccione">seleccione</option>
									</h5>
								</select>
								<label class="labelmedicotratante" class="control-label">Medico Tratante &nbsp;&nbsp; </label>
								<select class="custom-select" style="width:200px;" id="cmbmedicosreferidos" autocomplete="off" required>
									<option value="0">seleccione</option>
								</select>
							</div>
						</fieldset>
						<br>
						<span class="labelimpresiond"><b>MOTIVO DE CONSULTA</b>: </span>
						<input type="text" id="motivo_consulta" onkeyup="mayus(this);" class="control motivo_consulta" style="width:570px;" autocomplete="off">
						<br>
						<div>
							<fieldset>
								<legend class="legend"><b>Signos Vitales</b></legend>
								<div>
									<label class="labelpeso">PESO:</label>
									<input type="number" id="peso" class="control" min="0" value="0" name="peso" style="width:50px;">
									<label class="labelkg">(KG)</label>&nbsp;&nbsp;
									<label class="labeltalla">TALLA:</label>
									<input type="number" id="talla" class="control" min="0" value="0" name="talla" style="width:50px;">
									<label class="labelcm">(CM)</label>&nbsp;&nbsp;
									<label class="labelspo2">SPO2:</label>
									<input type="number" id="spo2" class="control" min="0" value="0" name="spo2" style="width:50px;">&nbsp;&nbsp;
									<label class="labelfc">FC:</label>
									<input type="number" id="frecuencia_c" class="control" min="0" value="0" name="fc" style="width:50px;">
									<label class="labelfr">(X) &nbsp;&nbsp;FR:</label>&nbsp;&nbsp;
									<input type="number" id="frecuencia_r" class="control" min="0" value="0" name="fr" style="width:50px;">
									<label class="labelx">(X)</label>


									<label class="labeltemperatura">TEMPERATURA:</label>
									<input type="number" id="temperatura" class="control" min="0" value="0" name="temperatura" style="width:50px;">&nbsp;&nbsp;
									<label class="labelc">(ºC)</label>&nbsp;&nbsp;
									<label class="labelta">T/A:</label>
									<input type="number" id="ta_alta" class="control" min="0" placeholder="t.a" name="ta" style="width:50px;">
									<input type="number" id="ta_baja" class="control" min="0" placeholder="t.b" name="ta" style="width:50px;">
									<label class="labelmmhg">(MMHg)</label>&nbsp;&nbsp;
									<label class="labeltipodesangre">Imc</label>
									<input type="number" class="bodersueve" id="imc" class="control" min="0" value="0" style="width:50px;">

							</fieldset>
							<div class="modal-footer">
								<button id="btnGuardar" class="btn-primary btn-sm">&nbsp;&nbsp;Guardar</button>&nbsp;&nbsp;
								<button id="btnCerrar" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
							</div>
							</body>

							</html>



							<style type="text/css">
								option {
									font-family: verdana, arial, helvetica, sans-serif;
									font-size: 14px;
									border: 3;
									border-radius: 20%;
								}

								option {
									border-color: blueviolet;
								}
							</style>

							<script>
								var botones = document.querySelectorAll('.btn-expandir');
								var texto_expandir = document.querySelectorAll('.texto_expandir');
								botones.forEach((elemento, clave) => {
									elemento.addEventListener('click', () => {
										texto_expandir[clave].classList.toggle("abrir_cerrar")
									});

								});
							</script>

							<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->

							<script type="text/javascript">
								function valideKey(evt) {

									// code is the decimal ASCII representation of the pressed key.
									var code = (evt.which) ? evt.which : evt.keyCode;

									if (code == 8) { // backspace.
										return true;
									} else if (code >= 48 && code <= 57) { // is a number.
										return true;
									} else { // other keys.
										return false;
									}
								}
							</script>

							<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
							<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
							<script>
								// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
								$(function() {
									$("#fecha_del_dia").datepicker({
										dateFormat: 'dd-mm-yy',
										changeMonth: true,
										changeYear: true
									});
									$("#fecha1").datepicker({
										dateFormat: 'dd-mm-yy',
										changeMonth: true,
										changeYear: true
									});


								});
							</script>

							<script>
								function mayus(e) {
									e.value = e.value.toUpperCase();
								}
							</script>
							<script>
								$(document).ready(function() {

									var now = new Date();

									var day = ("0" + now.getDate()).slice(-2);
									var month = ("0" + (now.getMonth() + 1)).slice(-2);
									var today = day + "-" + month + "-" + now.getFullYear();
									// var today= (day)+"-"+(month)+"-"+now.getFullYear();
									// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
									$("#fecha_del_dia").val(today);
								});
							</script>
							<?= $this->endSection(); ?>