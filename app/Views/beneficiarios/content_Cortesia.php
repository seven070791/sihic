<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/cortesia.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br />
	<div class="row col-md-12">
		<div class="row col-md-12">
			<h3>Beneficiarios / Cortesia </h3>
		</div>
		<h6>Titular:&nbsp;&nbsp;&nbsp;</h6>
		<div>
			<input type="text" readonly="readonly" class="sinborder" style="width:600px;" value='<?php echo $nombre . ' ' . ' ' . ' ' . ' ' . $apellido; ?>'>
		</div>
		<h6>&nbsp;&nbsp;Cedula:&nbsp;&nbsp;&nbsp;&nbsp;</h6>
		<div>
			<input type="text" readonly="readonly" id="cedula_trabajador " class="sinborder" style="width:200px;" value='<?php echo $cedula_trabajador; ?>'>
		</div>
	</div>
	<div class="alert alert-success" style="display: none;"></div>
	<div class="alert alert-danger" style="display: none;"></div>
	<br>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row">
		<div class="col-md-1">
			<button id="btnAgregarcortesia" class="btn btn-primary">Agregar</button>
		</div>
		<div class="col-md-2">
			<button id="btnRegresar_hacia_titulares" class="btn btn-secondary">Regresar</button>
		</div>
		<!-- ************* CAJAS DE TEXT************ -->
	</div>
	<div>
	</div>
	<br>
	<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
	<div class="row col-md-12 corte">
		<table class="display" id="table_cortesia" style="margin-top: 20px">
			<thead>
				<tr>
					<td style="width: 20px;">Nc</td>

					<td class="text-center" style="width: 80px;">Cedula</td>
					<td class="text-center" style="width: 180px;">Nombre</td>
					<td class="text-center" style="width: 180px;">Apellido</td>
					<td class="text-center" style="width: 100px;">Telefono</td>
					<td class="text-center" style="width: 10px;">Sexo</td>
					<td class="text-center" style="width: 120px;">Fecha de Nacimiento</td>
					<!-- <td class="text-center"style="width: 100px;">Estatus</td> -->
					<td class="text-center" style="width: 100px;">Acciones</td>
				</tr>
			</thead>
			<tbody id="lista_cortesia">
			</tbody>
		</table>
	</div>
</div>
<!-- Ventana Modal -->
<form action="" method="post" name="">
	<div class=" modal fade" id="modal_agregar_familiar" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">

					<div class="row col-md-12">
						<div>
							<div class="form-group has-feedback" id="cajas1">
								<form>
									<fieldset>
										<legend class="legend">Datos del Titular</legend>
										<!-- ****************************DATOS DEL TITULAR*************************************	 -->
										<div>
											<label>Cedula</label>
											<input type="text" class="sinborder" id="cedula_trabajador" disabled="disabled" name="cedula_trabajador" readonly="readonly" value='<?php echo $cedula_trabajador; ?>'>
											<label for="name">Nombre</label>&nbsp;
											<input type="text" class="sinborder" id="nombre" readonly="readonly" disabled="disabled" name="nombre" value='<?php echo $nombre . ' ' . ' ' . $apellido; ?>' style="width:400px;"> &nbsp;
											<br>
											<label for="mail">Ubicacion Administrativa</label>
											<input type="text" class="sinborder" id="ubicacion_administrativa" disabled="disabled" readonly="readonly" name="ubicacion_administrativa" value='<?php echo $ubicacion_administrativa; ?>' style="width:350px;">
										</div>
							</div>
							</fieldset>

							<!-- ****************************DATOS DE PERSONAL DE CORTESIA *************************************	 -->
							<div id="cajas2">

								<fieldset>
									<legend class="legend">Datos del Beneficiario</legend>

									<div>
										<label for="mail">Cedula</label>
										&nbsp; &nbsp; <input type="text" class="sinborder" id="cedula" autocomplete="off" onkeypress="return valideKey(event);" minlength="7" maxlength="11" required pattern="[0-9]+" name="cedula" style="width:130px;" value=''>
										&nbsp;&nbsp;<label for="name">Nombre</label>
										&nbsp; <input type="text" class="sinborder" onkeyup="mayus(this);" id="nombre_cortesia" autocomplete="off" required pattern="[aA-Za-z]+" name="nombre_cortesia" value='' style="width:190px;">
										&nbsp;&nbsp;<label for="mail">Apellido</label>
										&nbsp; <input type="text" class="sinborder" id="apellido_cortesia" onkeyup="mayus(this);" autocomplete="off" required pattern="[aA-Za-z]+" name="apellido_cortesia" value='' style="width:190px;">
										&nbsp;<label class="labeltelefono">Telefono</label>
										<input type="text" id="telefono" class="sinborder" autocomplete="off" onkeypress="return valideKey(event);" required pattern="[0-9]+" name="telefono" style="width:130px;" placeholder="04143222222" value=''>
										&nbsp;
										<label for="name">Nacionalidad</label>&nbsp;
										<input type="text" id="nacionalidad" class="sinborder" onkeyup="mayus(this);" name="nacionalidad" value="V" style="width:41px;">
										&nbsp; &nbsp;
										<label for="mail">Fecha de Nacimiento</label>&nbsp;&nbsp;
										<input type="text" class="sinborder" name="fecha" id="fecha" style="width:215px;" autocomplete="off" value="" style=" z-index: 1050 !important;">
										<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">

										</div>
										<div>
											<!-- ******Estado Civl******* -->
											<label for="name">Tipo de Sangre </label>&nbsp;
											<select class="classic" id="tipodesangre" name="tipodesangre" data-style="btn-primary" style="width:145px;">
												<option value="seleccione">seleccione</option>
											</select> &nbsp;
											<!-- ******Estado Civl******* -->
											<label for="name">Estado Civl</label>&nbsp;
											<select class="classic" id="estadocivil" name="estadocivil" data-style="btn-primary" style="width:145px;">
												<option value="seleccione">seleccione</option>
											</select>&nbsp;
											<!-- ******SELECT DEL OCUPACION******* -->
											<label for="name">Ocupacion</label>
											<select class="classic" id="ocupacion" name="ocupacion" data-style="btn-primary" style="width:145px;">
												<option value="seleccione">seleccione</option>
											</select>
										</div>
										<div>
											<!-- ******Grado de Instruccion******* -->
											<label for="name">Grado Instrucc</label>
											&nbsp;<select class="classic" id="gradointruccion" name="gradointruccion" data-style="btn-primary" style="width:190px;">
												<option value="seleccione">seleccione</option>
											</select>&nbsp;
											<!-- ******SELECT DEL SEXO******* -->
											<label class="labelsexo">Sexo</label>
											&nbsp;&nbsp; <select class="classic" id="sexo" name="sexo" style="width:145px;" data-style="btn-primary">
												<option value="0" selected disabled>seleccione</option>
												<option value="1">Masculino</option>
												<option value="2">Femenino</option>
											</select> &nbsp;&nbsp;

											<label class="labelborrado" for="borrado">Ente Adscrito</label>&nbsp; &nbsp;
											<input type="checkbox" class="check_borrado" id="adscrito" name="adscrito" value='false'>
										</div>

										<div class="ocultar_entes" style="display:none;">
											<label class="labelbeneficiario">Ente Adscrito</label>&nbsp;&nbsp;&nbsp;
											<select class="classic" style="width:350px;" id="entes_adscritos" name="entes_adscritos" data-style="btn-primary">
												<option value="0" selected disabled>seleccione</option>
											</select>
										</div>

										<div class="span">
											<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
												Observacion
											</SPAN>
											<textarea id="observacion" onkeyup="mayus(this);" autocomplete="off" name="observacion"> </textarea>
										</div>
									</div>
							</div>
							</fieldset>
							<div class="row">
								<div class="col-3">
								</div>
								<div class="col-6">

								</div>
								<div class="col-3">
									<button id="btnAgregarCortesia" class="btn-primary btn-sm">Agregar</button>
									&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnClose" name="btnCerrar">Cerrar</button>
								</div>
							</div>



						</div>
					</div>
				</div>
</form>
<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	function mayus(e) {
		e.value = e.value.toUpperCase();
	}
</script>
<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "-" + month + "-" + now.getFullYear();
		// var today= (day)+"-"+(month)+"-"+now.getFullYear();
		// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
		$("#fecha").val(today);
	});
</script>

<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>

<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1920:2050',
			dateFormat: 'dd/mm/yy',
		})
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>


<?php if (session('nivel_usuario') === '2' || session('nivel_usuario') === '3') : ?>
	<script>
		$('#btnAgregarcortesia').prop('disabled', true);
	</script>
	<style>
		#btnAgregarcortesia {
			pointer-events: none;
			opacity: 0.4;
		}
	</style>

<?php endif; ?>
<?= $this->endSection(); ?>