<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/cortesia.css" rel="stylesheet" type="text/css" />


<!-- Ventana Modal -->
<form action="" method="post" name="">
	<div class=" modal fade" id="modal_editar" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row ">
						<div class="col-md-12">
							<div>
								<div class="form-group has-feedback" id="cajas1">
									<form>

										<input type="hidden" id="id_user" name="id_user" readonly="readonly" value='<?= session('id_user'); ?>'>
										<!-- <label >Cedula</label> -->
										<input type="hidden" id="cedula_trabajador" disabled="disabled" name="cedula_trabajador" readonly="readonly" value='<?php echo $cedula_trabajador; ?>'>
										<!-- <label for="name">Nombre</label>&nbsp; -->
										<input type="hidden" id="nombre" readonly="readonly" disabled="disabled" name="nombre" value='<?php echo $nombre . ' ' . ' ' . $apellido; ?>' style="width:400px;"> &nbsp;

										<!-- <label for="mail">Ubicacion Administrativa</label> -->
										<input type="hidden" id="ubicacion_administrativa" disabled="disabled" readonly="readonly" name="ubicacion_administrativa" value='<?php echo $ubicacion_administrativa; ?>' style="width:350px;">

										<fieldset>
											<legend class="legend">Datos del Beneficiario </legend>
											<input type="hidden" id="tipodesangre_id" name="cedula_trabajador" readonly="readonly" value='<?php echo $tipo_de_sangre; ?>'>
											<input type="hidden" id="estadocivil_id" name="cedula_trabajador" readonly="readonly" value='<?php echo $estado_civil; ?>'>
											<input type="hidden" id="grado_intruccion_id" name="grado_intruccion_id" readonly="readonly" value='<?php echo $grado_intruccion_id; ?>'>
											<input type="hidden" id="ocupacion_id" name="ocupacion_id" readonly="readonly" value='<?php echo $ocupacion_id; ?>'>
											<!-- ****************************DATOS DEL TITULAR*************************************	 -->


											<!-- ****************************DATOS DE PERSONAL DE CORTESIA *************************************	 -->
											<div id="cajas2">
												<div>
													<label for="mail">Cedula</label>
													&nbsp; &nbsp; <input type="text" class="sinborder" id="cedula" minlength="7" maxlength="11" name="cedula" style="width:130px;" value='<?php echo $cedula; ?>'>
													<input type="hidden" class="sinborder" id="cedula_anterior" minlength="7" style="width:120px;" maxlength="11" name="cedula" required pattern="[0-9]+" value='<?php echo $cedula; ?>'>
													<label for="name">Nombre</label>
													&nbsp; <input type="text" class="sinborder" id="nombre_cortesia" onkeyup="mayus(this);" name="nombre_cortesia" value='<?php echo $nombre; ?>' style="width:168px;">
													<label for="mail">Apellido</label>
													&nbsp; <input type="text" class="sinborder" id="apellido_cortesia" onkeyup="mayus(this);" name="apellido_cortesia" value='<?php echo $apellido; ?>' style="width:168px;">
													<label class="labeltelefono">Telefono</label>
													&nbsp; <input type="text" class="sinborder" id="telefono" autocomplete="off" name="telefono" style="width:100px;" placeholder="04143222222" value='<?php echo $telefono; ?>'>
													<label for="name">Nacionalidad</label>
													<input type="text" class="sinborder" id="nacionalidad" name="nacionalidad" value="<?php echo $nacionalidad; ?>" style="width:41px; ">
													<label for="mail">Fecha de Nacimiento</label>
													<input type="text" autocomplete="off" class="sinborder" value='<?php echo $fecha_nac_cortesia; ?>' name="fecha" id="fecha" style="width:140px;" style=" z-index: 1050 !important;">
													<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
													</div>
													<div>

														<!-- ******Estado Civl******* -->
														<label for="name">Tipo de Sangre </label>
														<select class="classic" id="tipodesangre" name="tipodesangre" data-style="btn-primary" style="width:120px;">
															<option value="Seleccione">Seleccione</option>
														</select>
														<!-- ******Estado Civl******* -->
														<label for="name">Estado Civl</label>
														<select class="classic" id="estadocivil" name="estadocivil" data-style="btn-primary" style="width:120px;">
															<option value="Seleccione">Seleccione</option>
														</select>
														<!-- ******SELECT DEL Ocupacion******* -->
														<label for="name">Ocupacion</label>
														<select class="classic" id="ocupacion" name="ocupacion" data-style="btn-primary" style="width:147px;">
															<option value="Seleccione">Seleccione</option>
														</select>
													</div>
													<div>



														<!-- ******Grado de Instruccion******* -->
														<label for="name">Grado de Instruccion</label>
														&nbsp;<select class="classic" id="gradointruccion" name="gradointruccion" data-style="btn-primary" style="width:170px;">
															<option value="Seleccione">Seleccione</option>
														</select>
														<!-- ******SELECT DEL SEXO******* -->
														&nbsp;<label class="labelsexo">Sexo</label>
														<input type="hidden" id="id_sexo" value='<?php echo $sexo; ?>'>
														&nbsp;&nbsp; <select class="classic" style="width:120px;" id="sexo" name="sexo" data-style="btn-primary">
															<?php if ($sexo == 'M') : ?>
																<option value="1" selected>Masculino</option>
																<option value="2">Femenino</option>
															<?php else : ?>
																<option value="2" selected>Femenino</option>
																<option value="1">Masculino</option>
															<?php endif; ?>
														</select>
													</div>

													<div>
														<label for="name">Estatus del Beneficiario:</label>&nbsp; &nbsp;

														<input type="checkbox" class="check_borrado" id="check_borrado" name="check_borrado" value='false'>
														<input type="hidden" class="borrado" id="borrado" value='<?php echo $borrado; ?>'>
														<input type="hidden" class="estatus_actual" id="estatus_actual" value='false'>
														<input type="hidden" class="estatus_anterior" id="estatus_anterior" value='false'>

														<input type="hidden" class="estatus_anterior" id="id_cortesia" value='<?php echo $id; ?>'>
														<input type="hidden" class="estatus_anterior" id="estatus_adscrito" value='<?php echo $adscrito; ?>'>
														<input type="hidden" class="estatus_anterior" id="estatus_id_adscrito" value='<?php echo $id_adscrito; ?>'>
														&nbsp;&nbsp;&nbsp;
														<label class="labelborrado" for="borrado">Ente Adscrito</label>&nbsp; &nbsp;
														<input type="checkbox" class="check_borrado" id="adscrito" name="adscrito" value='false'>
													</div>


												</div>

												<div class="ocultar_entes" style="display:block;">
													<label class="labelbeneficiario">Ente Adscrito</label>&nbsp;&nbsp;&nbsp;
													<select class="classic" style="width:350px;" id="entes_adscritos" name="entes_adscritos" data-style="btn-primary">
														<option value="0" selected disabled>Seleccione</option>
													</select>
												</div>
												<div class="motivo_estatus">
													<div class="span">
														<SPAN class="texto" id="texto" style="position: absolute; top: 5 px; left: 400 px;">
															Motivo de Cambio
														</SPAN>
														<SPAN class="texto2" style="position: absolute; top: 250 px; left: 400 px;">
															De Estatus
														</SPAN>
														<textarea class="textarea" id="observacion" onkeyup="mayus(this);" name="observacion" value=" "><?php echo $observacion; ?> </textarea>
													</div>
												</div>

										</fieldset>
										<div class="row">
											<div class="col-3">
											</div>
											<div class="col-6">

											</div>
											<div class="col-3">
												<button id="btnActulizarCortesia" class="btn-primary btn-sm">Actualizar</button>
												&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn-primary btn-sm" data-dismiss="modal" id="btnCerrarCortesia" name="btnCerrar">Cerrar</button>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
</form>


<!-- METODO QUE TOMA LOS VALORES ANTERIORES DEL FORMULARIO -->
<!-- ******CEDULA******* -->
<input type="hidden" class="sinborder" id="cedula_anterior" minlength="7" style="width:120px;" maxlength="11" name="cedula" required pattern="[0-9]+" onkeypress="return valideKey(event);" value='<?php echo $cedula; ?>'>
<!-- ******NOMBRE******* -->
<input type="hidden" class="sinborder" id="nombre_cortesia_anterior" onkeyup="mayus(this);" name="nombre_familiar" value='<?php echo $nombre; ?>' style="width:220px;">
<!-- ******APELLIDO******* -->
<input type="hidden" class="sinborder" id="apellido_cortesia_anterior" onkeyup="mayus(this);" autocomplete="off" required pattern="[aA-Za-z]+" name="apellido" value='<?php echo $apellido; ?>' style="width:200px;">
<!-- ******TELEFONO******* -->
<input type="hidden" class="sinborder" id="telefono_anterior" onkeypress="return valideKey(event);" required pattern="[0-9]+" name="telefono" style="width:110px;" placeholder="04143222222" value='<?php echo $telefono; ?>'>
<!-- ******FECHA_NACIMIENTO******* -->
<input type="hidden" class="sinborder" autocomplete="off" value='<?php echo $fecha_nac_cortesia; ?>' name="fecha" id="fecha_anterior" style="width:140px;" style=" z-index: 1050 !important;">
<!-- ******NACIONALIDAD******* -->
<input type="hidden" class="sinborder" id="nacionalidad_anterior" name="nacionalidad" value="<?php echo $nacionalidad; ?>" style="width:41px; ">
<!-- ******FECHA DE NACIMIENTO******* -->
<input type="hidden" autocomplete="off" class="sinborder" value='<?php echo $fecha_nac_cortesia; ?>' name="fecha" id="fecha_anterior" style="width:140px;" style=" z-index: 1050 !important;">

<!-- ******SELECT DE LA OCUPACION******* -->
<select class="classic" id="ocupacion_anterior" name="ocupacion" data-style="btn-primary" style="width:145px;">
<option value="Seleccione">Seleccione</option>
</select>
<!-- ******SELECT DE TIPO DE SANGRE******* -->
<select class="classic" id="tipodesangre_anterior" name="tipodesangre" data-style="btn-primary" style="width:130px;">
<option value="Seleccione">Seleccione</option>
</select>
<!-- ******Estado Civl******* -->
<select class="classic" id="estadocivil_anterior" name="gradointruccion" data-style="btn-primary" style="width:140px;">
<option value="Seleccione">Seleccione</option>
</select>

<!-- ******GRADO DE INTRUCCION******* -->
<select class="classic" id="gradointruccion_anterior" name="gradointruccion" data-style="btn-primary" style="width:140px;">
<option value="Seleccione">Seleccione</option>
</select>
<!-- ******SEXO******* -->
&nbsp;&nbsp; <select class="classic" style="width:120px;" id="sexo_anterior" name="sexo" data-style="btn-primary">
<?php if ($sexo == 'M') : ?>
<option value="1" selected>Masculino</option>
<option value="2">Femenino</option>
<?php else : ?>
<option value="2" selected>Femenino</option>
<option value="1">Masculino</option>
<?php endif; ?>
</select>

<select class="classic" style="width:350px;" id="entes_adscritos_anterior" name="entes_adscritos" data-style="btn-primary">
<option value="0" selected disabled>Seleccione</option>
</select>

<!-- ******Control de estatus del beneficiario cortesia ******* -->
<input type="hidden" class="borrado" id="borrado_anterior" value=''>
<script>
	document.getElementById('estadocivil_anterior').style.display = 'none';
	document.getElementById('gradointruccion_anterior').style.display = 'none';
	document.getElementById('ocupacion_anterior').style.display = 'none';
	document.getElementById('tipodesangre_anterior').style.display = 'none';
	document.getElementById('sexo_anterior').style.display = 'none';
	document.getElementById('entes_adscritos_anterior').style.display = 'none';
</script>






<script>
	function mayus(e) {
		e.value = e.value.toUpperCase();
	}
</script>
<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1920:2050',
			dateFormat: 'dd/mm/yy',
		})
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>



<?= $this->endSection(); ?>