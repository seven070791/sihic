<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/editar_familiares.css" rel="stylesheet" type="text/css" />

<!-- Ventana Modal -->
<form action="" method="post" name="">
	<div class=" modal fade" id="modal_editar_familiar" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row ">
						<div class="col-md-12">
							<div>
								<div class="form-group has-feedback" id="cajas1">
									<form>

										<fieldset>
											<input type="hidden" id="id_user" name="id_user" readonly="readonly" value='<?= session('id_user'); ?>'>
											<input type="hidden" id="tipo_beneficiario" name="tipo_beneficiario" readonly="readonly" value='F'>
											<legend class="legend">Datos del Beneficiario</legend>
											<input type="hidden" id="tipodesangre_id" name="cedula_trabajador" readonly="readonly" value='<?php echo $tipo_de_sangre; ?>'>
											<input type="hidden" id="estadocivil_id" name="cedula_trabajador" readonly="readonly" value='<?php echo $estado_civil; ?>'>
											<input type="hidden" id="grado_intruccion_id" name="grado_intruccion_id" readonly="readonly" value='<?php echo $grado_intruccion_id; ?>'>
											<input type="hidden" id="ocupacion_id" name="ocupacion_id" readonly="readonly" value='<?php echo $ocupacion_id; ?>'>
											<input type="hidden" id="cedula_trabajador" name="cedula_trabajador" readonly="readonly" value='<?php echo $cedula_trabajador; ?>'>

											<div>

												<label for="mail">Cedula</label>
												<input type="text" class="sinborder" id="cedula" minlength="7" style="width:120px;" maxlength="11" name="cedula" required pattern="[0-9]+" onkeypress="return valideKey(event);" value='<?php echo $cedula; ?>'>
												<label for="name">Nombre</label>
												<input type="text" class="sinborder" id="nombre_familiar" onkeyup="mayus(this);" name="nombre_familiar" value='<?php echo $nombre; ?>' style="width:220px;">
												<label for="mail">Apellido</label>
												<input type="text" class="sinborder" id="apellido" onkeyup="mayus(this);" autocomplete="off" required pattern="[aA-Za-z]+" name="apellido" value='<?php echo $apellido; ?>' style="width:200px;">
												<label for="mail">Telefono</label>
												&nbsp; <input type="text" class="sinborder" id="telefono" onkeypress="return valideKey(event);" required pattern="[0-9]+" name="telefono" style="width:110px;" placeholder="04143222222" value='<?php echo $telefono; ?>'>


												<input type="hidden" class="sinborder" id="cedula_anterior" minlength="7" style="width:120px;" maxlength="11" name="cedula" required pattern="[0-9]+" onkeypress="return valideKey(event);" value='<?php echo $cedula; ?>'>

												<label for="mail">Fecha de Nacimiento</label>
												<input type="text" class="sinborder" autocomplete="off" value='<?php echo $fecha_nac_familiares; ?>' name="fecha" id="fecha" style="width:140px;" style=" z-index: 1050 !important;">
												<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">

												</div>

												<!-- ******SELECT DEL Parentesco******* -->
												<input type="hidden" id="parentesco_id" name="parentesco_id" style="width:500px;" value='<?php echo $parentesco_id; ?>'>
												<label for="name">Parentesco</label>&nbsp;&nbsp;
												<select class="classic" id="cmbParentesco" name="cmbParentesco" data-style="btn-primary" value='' style="width:145px;">
													<option value="Seleccione">Seleccione</option>
												</select>&nbsp;&nbsp;

												<!-- ******SELECT DE LA OCUPACION******* -->
												<label for="name">Ocupacion</label>&nbsp;&nbsp;
												<select class="classic" id="ocupacion" name="ocupacion" data-style="btn-primary" style="width:145px;">
													<option value="Seleccione">Seleccione</option>
												</select>&nbsp;&nbsp;

												<label for="name">Tipo de Sangre </label>&nbsp;
												<select class="classic" id="tipodesangre" name="tipodesangre" data-style="btn-primary" style="width:130px;">
													<option value="Seleccione">Seleccione</option>

												</select>
												
											</div>

											<div>

												<!-- ******Estado Civl******* -->
												<label for="name">Estado Civl</label>&nbsp;
												<select class="classic" id="estadocivil" name="estadocivil" data-style="btn-primary" style="width:146px;">
													<option value="Seleccione">Seleccione</option>&nbsp;
												</select>
												<!-- ******Grado de Instruccion******* -->
												<label for="name">Grado de Instruccion</label>
												&nbsp;<select class="classic" id="gradointruccion" name="gradointruccion" data-style="btn-primary" style="width:140px;">
													<option value="Seleccione">Seleccione</option>
												</select>
												<!-- ****SEXO******* -->
												<label for="name">sexo</label>
												<input type="hidden" id="id_sexo" value='<?php echo $id_sexo; ?>'>
												&nbsp;<select class="classic" id="sexo" name="sexo" data-style="btn-primary" style="width:140px;">
													<?php if ($id_sexo == 1) : ?>

														<option value="1" selected>MASCULINO</option>
														<option value="2">FEMENINO</option>

													<?php else : ?>
														<option value="2" selected>FEMENINO</option>
														<option value="1">MASCULINO</option>
													<?php endif; ?>
												</select>
												
											</div>
											<div>
												<label for="name">Estatus del Familiar:</label>
											</div>
											<div class="motivo_estatus">
												<div class="span">
													<SPAN class="texto" style="position: absolute; top: 195 px; left: 400 px;">
														Motivo de Cambio
													</SPAN>
													<SPAN class="texto2" style="position: absolute; top: 250 px; left: 400 px;">
														De Estatus
													</SPAN>
													<textarea id="observacion" onkeyup="mayus(this);" autocomplete="off" name="observacion"> </textarea>
												</div>
											</div>

											<label class="labelborrado" for="borrado" id="activo">Activo</label>&nbsp; &nbsp;
											<input type="checkbox" class="check_borrado" id="check_borrado" name="check_borrado" value='false'>
											<input type="hidden" class="borrado" id="borrado" value='<?php echo $borrado; ?>'>
											<input type="hidden" class="estatus_actual" id="estatus_actual" value='false'>
											<input type="hidden" class="estatus_anterior" id="estatus_anterior" value='false'>
											<label for="name"></label>




								</div>
							</div>
							<div class="modal-footer">
								<button id="btnActualizar" class="btn-primary btn-sm">Actualizar</button>
								<button id="btncerrar" class="btn-primary btn-sm">Cerrar</button>
								</fieldset>
							</div>
						</div>
						<div>

						</div>
					</div>
				</div>
</form>




<!-- METODO QUE TOMA LOS VALORES ANTERIORES DEL FORMULARIO -->
<!-- ******CEDULA******* -->
<input type="hidden" class="sinborder" id="cedula_anterior" minlength="7" style="width:120px;" maxlength="11" name="cedula" required pattern="[0-9]+" onkeypress="return valideKey(event);" value='<?php echo $cedula; ?>'>
<!-- ******NOMBRE******* -->
<input type="hidden" class="sinborder" id="nombre_familiar_anterior" onkeyup="mayus(this);" name="nombre_familiar" value='<?php echo $nombre; ?>' style="width:220px;">
<!-- ******APELLIDO******* -->
<input type="hidden" class="sinborder" id="apellido_anterior" onkeyup="mayus(this);" autocomplete="off" required pattern="[aA-Za-z]+" name="apellido" value='<?php echo $apellido; ?>' style="width:200px;">
<!-- ******TELEFONO******* -->
<input type="hidden" class="sinborder" id="telefono_anterior" onkeypress="return valideKey(event);" required pattern="[0-9]+" name="telefono" style="width:110px;" placeholder="04143222222" value='<?php echo $telefono; ?>'>
<!-- ******FECHA_NACIMIENTO******* -->
<input type="hidden" class="sinborder" autocomplete="off" value='<?php echo $fecha_nac_familiares; ?>' name="fecha" id="fecha_anterior" style="width:140px;" style=" z-index: 1050 !important;">

<!-- ******SELECT DE LA PARENTESCO******* -->
<select class="classic" id="cmbParentesco_anterior" name="cmbParentesco" data-style="btn-primary" value='' style="width:145px;">
<option value="Seleccione">Seleccione</option>
</select>
<!-- ******SELECT DE LA OCUPACION******* -->
<select class="classic" id="ocupacion_anterior" name="ocupacion" data-style="btn-primary" style="width:145px;">
<option value="Seleccione">Seleccione</option>
</select>
<!-- ******SELECT DE TIPO DE SANGRE******* -->
<select class="classic" id="tipodesangre_anterior" name="tipodesangre" data-style="btn-primary" style="width:130px;">
<option value="Seleccione">Seleccione</option>
</select>
<!-- ******Estado Civl******* -->
<select class="classic" id="estadocivil_anterior" name="gradointruccion" data-style="btn-primary" style="width:140px;">
<option value="Seleccione">Seleccione</option>
</select>
<!-- ******GRADO DE INTRUCCION******* -->
<select class="classic" id="gradointruccion_anterior" name="gradointruccion" data-style="btn-primary" style="width:140px;">
<option value="Seleccione">Seleccione</option>
</select>
<!-- ******SEXO******* -->
<select class="classic" id="sexo_anterior" name="sexo" data-style="btn-primary" style="width:140px;">
<?php if ($id_sexo == 1) : ?>
<option value="1" selected>MASCULINO</option>
<option value="2">FEMENINO</option>
<?php else : ?>
<option value="2" selected>FEMENINO</option>
<option value="1">MASCULINO</option>
<?php endif; ?>
</select>

<!-- ******Control del estatus del FAMILIAR ******* -->
<input type="hidden" class="borrado" id="borrado_anterior" value=''>

<script>
	document.getElementById('estadocivil_anterior').style.display = 'none';
	document.getElementById('gradointruccion_anterior').style.display = 'none';
	document.getElementById('cmbParentesco_anterior').style.display = 'none';
	document.getElementById('ocupacion_anterior').style.display = 'none';
	document.getElementById('tipodesangre_anterior').style.display = 'none';
	document.getElementById('sexo_anterior').style.display = 'none';
</script>
<!-- ******Motivo del cambio de estatus del familiar ******* -->
<textarea id="observacion_anterior" onkeyup="mayus(this);" autocomplete="off" name="observacion"> </textarea>
<script>
	function mayus(e) {
		e.value = e.value.toUpperCase();
	}
</script>

<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>
<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->




<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1920:2050',
			dateFormat: 'dd/mm/yy',
		})
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>
<?= $this->endSection(); ?>