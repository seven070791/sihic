<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/editartitulares.css" rel="stylesheet" type="text/css" />
<!-- Ventana Modal -->
<form action="" method="post" name="">
	<div class=" modal fade" id="modal_editar" tabindex="-1" data-backdrop="static">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="row ">
						<div class="col-md-12">
							<div>
								<div class="form-group has-feedback">
									<form>
										<input type="hidden" id="id_user" name="id_user" readonly="readonly" value='<?= session('id_user'); ?>'>
										<input type="hidden" id="tipodesangre_id" name="cedula_trabajador" readonly="readonly" value='<?php echo $tipo_de_sangre; ?>'>
										<input type="hidden" id="estadocivil_id" name="cedula_trabajador" readonly="readonly" value='<?php echo $estado_civil; ?>'>
										<input type="hidden" id="grado_intruccion_id" name="grado_intruccion_id" readonly="readonly" value='<?php echo $grado_intruccion_id; ?>'>
										<input type="hidden" id="ocupacion_id" name="ocupacion_id" readonly="readonly" value='<?php echo $ocupacion_id; ?>'>
										<fieldset>
											<legend class="legend">Datos del Beneficiario </legend>
											<div>
												<label>Cedula</label>
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input class="ocultar" type="text" id="cedula_trabajador" name="cedula_trabajador" disabled="disabled" readonly="readonly" value='<?php echo $cedula_trabajador; ?>'>
												<label for="name">Nombre</label>
												&nbsp; <input type="text" id="nombre" class="ocultar" disabled="disabled" readonly="readonly" name="nombre" value='<?php echo $nombre . ' ' . ' ' . $apellido; ?>' style="width:348px;">
												&nbsp; <input type="hidden" id="apellido" disabled="disabled" readonly="readonly" name="apellido" value='<?php echo $apellido; ?>' style="width:348px;">
												<label for="name">Nacionalidad</label>
												<select class="classic" name="select" style="width:145px;" id="nacionalidad" value="">
													<?php
													if ($nacionalidad == 'V') :
														echo ("<option value='V'selected>VENEZOLANO</option>");
														echo ("<option value='E'>EXTRANJERO</option>");
													elseif ($nacionalidad == 'E') :
														echo ("<option value='V'>VENEZOLANO</option>");
														echo ("<option value='E' selected>EXTRANJERO</option>");
													endif;
													?>
												</select>
												<!-- ******Estado Civl******* -->
												&nbsp;<label for="name">Estado Civl</label>
												&nbsp;<select class="classic" id="estadocivil" style="width:130px;" name="estadocivil" data-style="btn-primary" style="width:115px;">
													<option value="Seleccione">Seleccione</option>
												</select>
												<!-- ******Grado de Instruccion******* -->
												<label for="name">Grado Instruccion</label>
												&nbsp;<select class="classic" id="gradointruccion" name="gradointruccion" data-style="btn-primary" style="width:130px;">
													<option value="Seleccione">Seleccione</option>
												</select>
											</div>

											<div>
												<label class="labelcronico" id="labelcronico">Estatus del Trabajador : </label>&nbsp;&nbsp;


												<label class="labelcronico" id="labelcronico"> Activo </label>&nbsp;&nbsp;
												<input type="checkbox" class="estatus_tra" id="estatus_tra" value=''>
												<input type="hidden" class="borrado" id="borrado" value='<?php echo $borrado; ?>'>
												<input type="hidden" class="estatus_actual" id="estatus_actual" value='false'>
												<input type="hidden" class="estatus_anterior" id="estatus_anterior" value='false'>

											</div>
											<div class="motivo_estatus">
												<div class="span">
													<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
														Observacion
													</SPAN>
													<div class="row col-10">
														<div class="texto2">
															<textarea class="textarea" id="observacion" onkeyup="mayus(this);" name="observacion" value=" "> </textarea>
														</div>
													</div>
										</fieldset>
										<br>
										<fieldset>
											<label for="mail">Tipo de Perso </label>
											&nbsp; <input type="text" id="tipo_de_personal" name="tipopersonal" value='<?php echo $tipo_de_personal; ?>' style="width:150px;">
											<!-- ******Ubicacion Admi******* -->
											<label for="mail"></label>

											<label for="name">Ubicacion </label>
											<select class="classic" id="ubicacion" name="ubicacion" data-style="btn-primary" style="width:393px;">
												<option value="0" selected disabled>Seleccione</option>
											</select>

											<input type="hidden" id="ubicacion_administrativa" onkeyup="mayus(this);" name="ubicacion_administrativa" value='<?php echo $ubicacion_administrativa; ?>' style="width: 300px;">

											<div>
												<label for="name">Tipo de Sangre </label>
												<select class="classic" id="tipodesangre" name="tipodesangre" data-style="btn-primary" style="width:135px;">
													<option value="Seleccione">Seleccione</option>
												</select>
												<!-- ******SELECT DEL OCUPACION******* -->
												<label for="name">Ocupacion</label>
												<select class="classic" id="ocupacion" name="ocupacion" data-style="btn-primary" style="width:133px;">
													<option value="Seleccione">Seleccione</option>
												</select>
												<!-- ******SELECT DEL SEXO******* -->
												&nbsp;&nbsp;&nbsp;&nbsp;<label for="SEXO">Sexo</label>
												<!-- <input type="text"  value='<php echo $sexo;?>' > -->
												&nbsp;&nbsp;<select name="select" class="classic" style="width:120px;" id="sexo" value="">
													<?php
													if ($sexon == '1') :
														echo ("<option value='1' selected>M</option>");
														echo ("<option value='2'>F</option>");
													elseif ($sexon == 2) :
														echo ("<option value='1'>M</option>");
														echo ("<option value='2' selected>F</option>");
													endif;
													?>
												</select>
											</div>
											<label for="mail">Telefono</label>
											&nbsp; <input type="text" id="telefono" onkeypress="return valideKey(event);" name="telefono" style="width:150px;" placeholder="04143222222" value='<?php echo $telefono; ?>'>
											<label for="mail">Fecha de Nacimiento</label>
											<input type="text" autocomplete="off" value='<?php echo $fecha_nacimiento; ?>' name="fecha" id="fecha" style="width:140px;" style=" z-index: 1050 !important;">
											<br>
								</div>
								<div>
									<br>

									<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
									</div>
									</fieldset>
									<div class="modal-footer">
										<button id="btnActualizar" class="btn-primary btn-sm">Actualizar</button>
										<button id="btnCerrar1" class="btn-primary btn-sm">Cerrar</button>
									</div>
								</div>
</form>




<!-- METODO QUE TOMA LOS VALORES ANTERIORES DEL FORMULARIO -->
<select type ="classic"   name="select" style="width:145px;" id="nacionalidad_anterior" value="">
	<?php
	if ($nacionalidad == 'V') :
		echo ("<option value='V'selected>VENEZOLANO</option>");
		echo ("<option value='E'>EXTRANJERO</option>");
	elseif ($nacionalidad == 'E') :
		echo ("<option value='V'>VENEZOLANO</option>");
		echo ("<option value='E' selected>EXTRANJERO</option>");
	endif;
	?>
</select>

<!-- ******Estado Civl******* -->
&nbsp;<select class="classic" id="estadocivil_anterior" style="width:130px;" name="estadocivil" data-style="btn-primary" style="width:115px;">
	<option value="Seleccione">Seleccione</option>
</select>

<!-- ******Grado de Instruccion******* -->
&nbsp;<select class="classic" id="gradointruccion_anterior" name="gradointruccion" data-style="btn-primary" style="width:130px;">
<option value="Seleccione">Seleccione</option>
</select>

<!-- ******Tipo de personal******* -->
&nbsp; <input type="text" id="tipo_de_personal_anterior" name="tipopersonal" value='<?php echo $tipo_de_personal; ?>' style="width:150px;">
<script>
	function mayus(e) {
		e.value = e.value.toUpperCase();
	}
</script>

<!-- ******Ubicacion Administrativa******* -->
<select class="classic" id="ubicacion_anterior" name="ubicacion" data-style="btn-primary" style="width:393px;">
	<option value="0" selected disabled>Seleccione</option>
</select>
<!-- ******Control del estatus del trabajador ******* -->
<input type="hidden" class="borrado" id="borrado_anterior" value=''>

<!-- ******SELECT DEL OCUPACION******* -->
<select class="classic" id="ocupacion_anterior" name="ocupacion" data-style="btn-primary" style="width:133px;">
	<option value="Seleccione">Seleccione</option>
</select>

<!-- ******SELECT DEL TIPO DE SANGRE******* -->
<select class="classic" id="tipodesangre_anterior" name="tipodesangre" data-style="btn-primary" style="width:135px;">
<option value="Seleccione">Seleccione</option>
</select>

<!-- sexo -->
&nbsp;&nbsp;<select name="select" class="classic" style="width:120px;" id="sexo_anterior" value="">
<?php
if ($sexon == '1') :
echo ("<option value='1' selected>M</option>");
echo ("<option value='2'>F</option>");
elseif ($sexon == 2) :
echo ("<option value='1'>M</option>");
echo ("<option value='2' selected>F</option>");
endif;
?>
</select>
<!-- telefono -->
 <input type="hidden" id="telefono_anterior" onkeypress="return valideKey(event);" name="telefono" style="width:150px;" placeholder="04143222222" value='<?php echo $telefono; ?>'>

<!-- fecha de nacimiento -->
 <input type="hidden" autocomplete="off" value='<?php echo $fecha_nacimiento; ?>' name="fecha" id="fecha_anterior" style="width:140px;" style=" z-index: 1050 !important;">

<script>
	document.getElementById('nacionalidad_anterior').style.display = 'none';
	document.getElementById('ubicacion_anterior').style.display = 'none';
	document.getElementById('tipo_de_personal_anterior').style.display = 'none';
	document.getElementById('gradointruccion_anterior').style.display = 'none';
	document.getElementById('estadocivil_anterior').style.display = 'none';
	document.getElementById('ocupacion_anterior').style.display = 'none';
	document.getElementById('tipodesangre_anterior').style.display = 'none';
	document.getElementById('sexo_anterior').style.display = 'none';
</script>
<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>


<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1920:2050',
			dateFormat: 'dd/mm/yy',
		})
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>





<?= $this->endSection(); ?>