<link rel="stylesheet" href="<?php echo base_url(); ?>/css/menufarmacia.css">
<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu2">
  <li class="nav-item ">
    <a href="../supermenu" class="nav-link active"><i class="nav-icon fas fa-tachometer-alt"></i>
      <p> Inicio</p>
    </a>
  </li>

  <li class="nav-item">
    <a href="#" class="nav-link"><i class="nav-icon 	fas fa-user-md" style='font-size:20px'></i>
      <p> Farmacia </p>
      <i class="right fas fa-angle-left"></i>
    </a>
    <ul class="nav nav-treeview">
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/vistamedicamentos" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
          <p>Consultar Inventario</p>
        </a>
      </li>

      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/vistaMedicos" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
          <p>Registro de Medicos</p>
        </a>
      </li>

      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/vistaNotasEntregas" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
          <p>Notas de Entregas</p>
        </a>
      </li>

      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/reposos" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
          <p>Reposos</p>
        </a>
      </li>

    </ul>
  </li>

  <li class="nav-item">
    <a href="<?php echo base_url(); ?>/titulares" class="nav-link"><i class="nav-icon fas fa-copy" style='font-size:20px'></i>
      <p> Beneficiarios </p>

    </a>

  </li>





  <!-- </ul> -->


  <li class="nav-item">
    <a href="#" class="nav-link"><i class="nav-icon 	fas fa-book-open" style='font-size:18px'></i>
      <p> Reportes Generales</p>
      <i class="right fas fa-angle-left" style='font-size:20px'></i>
    </a>
    <ul class="nav nav-treeview">

      <li class="nav-item">
        <a href="#" class="nav-link"><i class="nav-icon 	fas fa-book-open" style='font-size:18px'></i>
          <p>Inventario</p>
          <i class="right fas fa-angle-left" style='font-size:20px'></i>
        </a>
        <ul class="nav nav-treeview">
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>/Relacion_entradas_medicamentos" target='_blank' class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
              <p>Entradas </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>/Relacion_salidas_medicamentos" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
              <p>Salidas</p>
            </a>
          </li>

          <li class="nav-item">
            <a href="<?php echo base_url(); ?>/vista_stock_minimo" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
              <p>Stock Minimo</p>
            </a>
          </li>


        </ul>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/reporte_citas" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
          <p>Consultas / Citas</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/Hoja_morbilidad" target='_blank' class="nav-link"><i class=" far fa-edit " style='font-size:20px'></i>
          <p>Hoja de Morbilidad </p>
        </a>
      </li>

    </ul>
  </li>




  <li class="nav-item">
    <a href="#" class="nav-link"><i class="nav-icon 	fas fa-cogs"></i>
      <p> Mantenimiento</p>
      <i class="right fas fa-angle-left" style='font-size:22px'></i>
    </a>
    <ul class="nav nav-treeview">

      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/Vita_Especialidad" class="nav-link"><i class="	nav-icon 	fas fas fa-book-medical"></i>
          <p>Especialidad Medica</p>
        </a>
      </li>

      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/vistaTipoMedicamento" class="nav-link"><i class="	nav-icon 	fas fas fa-book-medical"></i>
          <p> Tipo Medicamento</p>
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/vistaUnidadMedida" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
          <p> Unidad de Medida </p>
        </a>
      </li>
      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/vistapresentacion" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
          <p>Agregar Presentacion </p>
        </a>
      </li>

      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/vistacontrol" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
          <p>Control </p>
        </a>
      </li>

      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/vistacategoria" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
          <p>Categoria Inv. </p>
        </a>
      </li>

      <li class="nav-item">
        <a href="<?php echo base_url(); ?>/entes_adscritos" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
          <p>Entes Adscritos </p>
        </a>
      </li>


    </ul>
</ul>
<script src="https://code.jquery.com/jquery-3.1.0.js"></script>

<input type="hidden" id="master" disabled="disabled" value='<?= session('master'); ?>' />
<script>
  $(document).ready(function() {
    $('.usuarios').prop('disabled', true);
    $('.auditoria').prop('disabled', true);
    $('.reportes').prop('disabled', true);

  });
</script>

<!-- background-color: rgba(00,00,255,0.5); -->