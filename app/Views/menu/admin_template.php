<!-- <link href="/css/admintemplate.css" rel="stylesheet" type="text/css" /> -->
<!-- ********************rol del administrador****************** -->
<li class="nav-item">
  <!-- <a class="nav-link" data-widget="control-sidebar" data-controlsidebar-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i> -->
  </a>
</li>
</ul>
</nav>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: linear-gradient(to right, #263846, #344958, #263846);">
  <br>
  <section class="superior1_form">
    <div class="row">
      <div class="col-lg-6">
        <img src="<?= base_url() ?>/img/siamed2.png" width="140" alt="Avatar" id="superior1">
      </div>
    </div>
  </section>
  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo base_url(); ?>/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">
          <h5><?= session('nombreUsuario'); ?></h5>
        </a>
      </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item menu-close">
          <a href="/supermenu" class="nav-link active"><i class="nav-icon fas fa-tachometer-alt"></i>
            &nbsp;&nbsp;<p>Administrador - Inicio</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link"><i class="nav-icon  fas fa-user-cog" style='font-size:20px'></i>

            <p> Control de Usuarios</p>
            <i class="right fas fa-angle-left"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/User_controllers" class="nav-link">
                <i class="nav-icon 	fas fa-address-card" style='font-size:20px'></i>
                <p>Usuarios</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/Grupousuario" class="nav-link">
                <i class="nav-icon 	fas  fa-users" style='font-size:20px'></i>
                <p>Grupo Usuarios</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link"><i class="nav-icon  fas fa-sync" style='font-size:20px'></i>
            <p>Auditoria</p>
            <i class="right fas fa-angle-left"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/AuditoriaController" class="nav-link">
                <i class="nav-icon 	fas fa-address-card" style='font-size:20px'></i>
                <p>Entradas y Salidas</p>
              </a>
            </li>



            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/auditoria_sistema" class="nav-link">
                <i class="nav-icon 	fas fa-address-card" style='font-size:20px'></i>
                <p>Auditoria de Sistema</p>
              </a>
            </li>
          </ul>








        <li class="nav-item">
          <a href="#" class="nav-link"><i class="nav-icon 	fas fa-user-md" style='font-size:20px'></i>
            <p> Farmacia </p>
            <i class="right fas fa-angle-left"></i>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistamedicamentos" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                <p>Consultar Inventario</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistaMedicos" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                <p>Registro de Medicos</p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistaNotasEntregas" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                <p>Notas de Entregas</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/reposos" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                <p>Reposos</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item">
          <a href="<?php echo base_url(); ?>/titulares" class="nav-link"><i class="nav-icon fas fa-copy" style='font-size:20px'></i>
            <p> Beneficiarios </p>

          </a>

        </li>






        <!-- </ul>  -->


        <li class="nav-item">
          <a href="#" class="nav-link"><i class="nav-icon 	fas fa-book-open" style='font-size:18px'></i>
            <p> Reportes Generales</p>
            <i class="right fas fa-angle-left" style='font-size:20px'></i>
          </a>
          <ul class="nav nav-treeview">

            <li class="nav-item">
              <a href="#" class="nav-link"><i class="nav-icon 	fas fa-book-open" style='font-size:18px'></i>
                <p>Inventario</p>
                <i class="right fas fa-angle-left" style='font-size:20px'></i>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/Relacion_entradas_medicamentos"  class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                    <p>Entradas </p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/Relacion_salidas_medicamentos" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
                    <p>Salidas</p>
                  </a>
                </li>

                <li class="nav-item">
                  <a href="<?php echo base_url(); ?>/vista_stock_minimo" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                    <p>Stock Minimo</p>
                  </a>
                </li>


              </ul>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/reporte_citas" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                <p>Consultas / Citas</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/Hoja_morbilidad"  class="nav-link"><i class=" far fa-edit " style='font-size:20px'></i>
                <p>Hoja de Morbilidad </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/frecuencia_atencion"  class="nav-link"><i class=" far fa-edit " style='font-size:20px'></i>
                <p>Frecuencia de Atencion </p>
              </a>
            </li>

          </ul>
        </li>



        <li class="nav-item">
          <a href="#" class="nav-link"><i class="nav-icon 	fas fa-cogs"></i>
            <p> Mantenimiento</p>
            <i class="right fas fa-angle-left" style='font-size:22px'></i>
          </a>
          <ul class="nav nav-treeview">

          <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistaAutorizador" class="nav-link"><i class="	nav-icon 	fas fas fa-book-medical"></i>
                <p>Autorizador</p>
              </a>
            </li>


            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/Vita_Especialidad" class="nav-link"><i class="	nav-icon 	fas fas fa-book-medical"></i>
                <p>Especialidad Medica</p>
              </a>
            </li>

           

            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistaTipoMedicamento" class="nav-link"><i class="	nav-icon 	fas fas fa-book-medical"></i>
                <p> Tipo de Inventario</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistaUnidadMedida" class="nav-link"><i class=" nav-icon  	fas fa-first-aid " style='font-size:20px'></i>
                <p> Unidad de Medida </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistapresentacion" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
                <p>Agregar Presentacion </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistacontrol" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
                <p>Control </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/vistacategoria" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
                <p>Categoria Inv. </p>
              </a>
            </li>

            <li class="nav-item">
              <a href="<?php echo base_url(); ?>/entes_adscritos" class="nav-link"><i class="nav-icon 	 fas 	fas fa-file-medical" style='font-size:22px'></i>
                <p>Entes Adscritos </p>
              </a>
            </li>


          </ul>
    </nav>

  </div>
  <!-- /.sidebar -->
</aside>

<!-- este controla el menu -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- *****CON ESTO RENDERISO LA SECCION DEL CONTENT****     -->
</div>
</body>

</html>