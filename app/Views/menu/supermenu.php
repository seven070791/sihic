<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="<?= base_url() ?>/img/siamedcorazon.jpg" style="width: 50px; height: 50px;">
  <title>SIAMED</title>
  <link rel=" stylesheet" href="<?php echo base_url(); ?>/css/supermenu.css">
  <!-- Bootstrap CSS -->
  <!-- CSS personalizado -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/datatable.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/bootstrap-grid.css">
  <!--datables CSS básico-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/datatables/datatables.min.css" />
  <!--datables estilo bootstrap 4 CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/bootstrap.min.css">
  <!-- CSS personalizado -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/datatable.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/botones_datatable.css">

  <!--datables CSS básico-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/datatables/datatables.min.css" />
  <!--datables estilo bootstrap 4 CSS-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/css/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
  <!-- Google Font: Source Sans Pro -->
  <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/plugins/summernote/summernote-bs4.min.css">
  <!--Swee Alert-->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/plugins/sweetalert2/sweetalert2.min.css">
  <!--<php echo base_url();?>/plugins/sweetalert2/sweetalert2.all.min.js -->


  <!-- REFERENCIAS CDN CSS-->
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/cdn/jquery-ui.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/cdn/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/cdn/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/css/cdn/icon.css">



 

  <script src="<?=base_url();?>/js/jquery-3.3.1.slim.min.js"></script>

<script src="<?=base_url();?>/js/bootstrap.min2.js"></script>

  <!-- datapiquer -->
  <!--  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" /> -->
  <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">  -->
</head>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>

      </ul>

      <ul class="navbar-nav ml-auto">
        <!-- <a href="javascript:history.back()"><img src="<php echo base_url();?>/img/i2.jpg" class="img-size-50 img-circle mr-3"></a> -->
        <li class="nav-item">
        <li class="nav-item d-none d-sm-inline-block">
          <a href="#Foo" onclick="cerrarSesion();" class="nav-link">Salir</a>
        </li>
        <script type="text/javascript">
          function cerrarSesion() {
            // alert('hola soy cerrar sesion');
            Swal.fire({
              title: 'Desea salir?',
              icon: 'warning',
              showCancelButton: true,
              cancelButtonColor: '#d33',
              confirmButtonText: 'Si, salir'
            }).then((result) => {
              if (result.isConfirmed) {
                window.location.href = ('/Login/cerrarSesion');
              }
            })
          }
        </script>
        </li>
      </ul>
    </nav>

    <aside class="main-sidebar sidebar-dark-primary elevation-4" style="background: linear-gradient(to right, #263846, #344958, #263846);">
      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
        <img src="<?php echo base_url(); ?>/img/logosapi-01.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-4" style="opacity: .8">
        <span class="brand-text font-weight-light">Servicio Medico </span>
      </a>
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="<?php echo base_url(); ?>/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
          </div>
          <div class="info">
            <a href="#" class="d-block">
              <h6><?= session('nombreUsuario'); ?></h6>
            </a>
          </div>
        </div>

        <!-- ************************* ACCESO A CITAS ************************* -->
        <input type="hidden" id="acceso_citas" disabled="disabled" value='<?= session('acceso_citas'); ?>' />
        <!-- *************************** ************************************** -->
        <!-- ************************* ID ESPECIALIDAD ************************* -->
        <input type="hidden" id="id_especialidad" disabled="disabled" value='<?= session('id_especialidad'); ?>' />
        <!-- *************************** ************************************** -->


        <div class="info">
          <a href="#" class="d-block">
            <!-- Menu Administrador  -->
            <h4><?php if (session('nivel_usuario') === '1') : ?></h4>
            <?php include('admin_template.php'); ?>
            <!-- Menu Administrador  -->
            <h4><?php elseif (session('nivel_usuario') === '3') : ?></h4>
            <?php include_once('menu_medico.php'); ?>
            <!-- Menu Farmacia  -->
            <h4> <?php elseif (session('nivel_usuario') === '2') : ?></h4>
            <?php include_once('menu_farmacia.php'); ?>

          <?php endif; ?>
          <h6> <a href="#Foo" onclick="cerrarSesion();" class="nav-link">Salir</a></h6>

        </div>
      </div>
      <!-- *****************************hasta aqui el menu*****************************          -->
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      
      <?= $this->renderSection('content'); ?>
      
      <?= $this->extend('menu/supermenu') ?>
      
      <!-- INICIO DE LA SECION CONTENT -->
      <?= $this->section('content') ?>
     

      <!-- Main content -->
      <section class="cartelera" id="cartelera">
        <div class="col-md-3">
          <div class="from-group">
          </div>
        </div>
        <div class="container-fluid content_carteleras">
          <!-- Small boxes (Stat box) -->
          <div class="row ">
            <div class="col-md-3  usuarios">
              <!-- small box -->
              <div class="small-box bg-secondary ">
                <div class="inner">
                  <h4>Usuarios</h4>
                  <br>
                  <br>
                  <p></p>
                </div>
                <div class="icon">
                  <i class='fas fa-address-card' style='color:#f1f0e9'></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-md-3  inventario">
              <!-- small box -->
              <div class="small-box bg-success">
                <div class="inner">
                  <h4> Inventario</h4>
                  <br>
                  <br>
                  <p></p>
                </div>
                <div class="icon">
                  <i class="fas fa-book-open " style='color:#f1f0e9'></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-md-3  historial">
              <!-- small box -->
              <div class="small-box bg-info">
                <div class="inner">
                  <h4>Historial</h4>
                  <br>
                  <br>
                  <p></p>
                </div>
                <div class="icon">
                  <i class="fas fa-receipt" style='color:#f1f0e9'></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <div class="col-md-3  auditoria">
              <!-- small box -->
              <div class="small-box bg-danger">
                <div class="inner">
                  <h4>Auditoria</h4>
                  <br>
                  <br>
                  <p></p>
                </div>
                <div class="icon">
                  <i class="fas fa-user-secret" style='color:#f1f0e9'></i>
                </div>
                <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
      </section>
      <!-- /.row -->
      <!-- Main row -->
      <!-- Left col -->
      <section class="form-login_imagen">
        <div class="imagencentral">

          <img class="img-fluid mx-auto d-block" src="<?= base_url() ?>/img/siamed2.png" id="imagencentral">

        </div>
    </div>
    </section>
   

    <!-- /.content-header -->
    <?php include_once('footer.php'); ?>