<?= $this->extend('menu/supermenu') ?>
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<input type="text" class="sinborder" id="id_especialidad" disabled="disabled" style="width:300px;" value='<?php echo $especialidad; ?>'>

<link href="/css/actualizar_citas_familiares.css" rel="stylesheet" type="text/css" />
<link href="/css/expandir.css" rel="stylesheet" type="text/css" />
<br>

<div class="row ">
	<div class="col-md-2">
	</div>
	<input type="hidden" disabled="disabled" id="numeroHistorialDatatable" style="width:100px;" value='<?php echo $n_historial; ?>'>
	<div class="col-md-8">

		<input type="hidden" class="sinborder" id="id_historial_medico" disabled="disabled" style="width:100px;" value='<?php echo $id_historial_medico; ?>'>
		<input type="hidden" class="sinborder" id="id_consulta" disabled="disabled" style="width:100px;" value='<?php echo $id_consulta; ?>'>
		<!--****** ********************MEDICINA GENERAL************************ -->
		<div class="form-group has-feedback" id="cajas1" style="display: none;">
			<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?>' />
			<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?>' />
			<br>
			<fieldset class="bodersueve_fieldset">
				<img src="<?= base_url() ?>/img/2.png" width="120" alt="Avatar" id="img_beneficiario">
				<label class="titulo_mgeneral">FICHA DE REGISTRO DE PACIENTE ADULTO MEDICINA GENERAL</label>
				<legend class="legend"><b>Datos del Trabajador</b></legend>
				<label class="labelfechamgeneral">Fecha de Asistencia</label>
				<input type="text" class="sinborder" id="fecha_asistencia_mgeneral" style="width:120px;" value='<?php echo $fecha_asistencia; ?>' />
				<div>
					<label class="datos">Nombre y Apellido</label>
					<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:400px;" value='<?php echo $nombre_titular . ' ' . $apellido_titular; ?>'>
				</div>
				<div>
					<label class="datos">Cedula</label>
					<input type="text" class="sinborder" id="cedulaT" disabled="disabled" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
					<label class="datos">Departamento</label>
					<input type="text" class="sinborder" id="departamento" disabled="disabled" style="width:300px;" value='<?php echo $departamento; ?>'>
				</div>
			</fieldset>
			<br>
			<fieldset class="bodersueve_fieldset">
				<legend class="legend"><b>Datos del Beneficiario</b></legend>
				<div>
					<label class="datos">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="sinborder" id="nombrepellidoB" disabled="disabled" style="width:300px;" value='<?php echo $nombre . ' ' . $apellido; ?>'> &nbsp;&nbsp;&nbsp;&nbsp;
					<label class="labelhistorial"><u>Nº Historial</u> </label>
					<input type="text" class="sinborder" disabled="disabled" id="numeroHistorial" class="control" style="width:120px;" value='<?php echo 'F' . '' . $cedula; ?>'>
				</div>
				<div>
					<label class="datos">Fecha De Nacimiento</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="sinborder" id="fecha_nacimiento" class="control" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nac_familiares; ?> ' />
					<label class="datos">Cedula</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="sinborder" id="cedulab" disabled="disabled" class="control" style="width:100px;" value='<?php echo $cedula; ?>'>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="datos">Tipo Beneficiario</label>
					<input type="text" class="sinborder" id="Tbenficiario" disabled="disabled" class="control" style="width:100px;" value='Familiar'>
				</div>
				<label class="datos">Edad</label>&nbsp;&nbsp;
				<input type="text" class="sinborder" id="edad" disabled="disabled" class="control" style="width:100px;" value='<?php echo $edad_actual; ?>'>
				&nbsp;&nbsp;<label class="datos">Nº Telefono</label>&nbsp;&nbsp;
				&nbsp;&nbsp;<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:120px;" value='<?php echo  $telefono; ?>'>
				<label class="datos">Sexo</label>&nbsp;&nbsp;
				&nbsp;&nbsp;<input type="text" class="sinborder" disabled="disabled" id="sexo" class="control" style="width:50px;" value='<?php echo  $sexo; ?>'>
			</fieldset>
			<br>
			<fieldset class="bodersueve_fieldset">
				<legend class="legend"><b>Signos Vitales </b> </legend>
				<div>
					<label class="datos">Peso:</label>
					<input type="number" class="bodersueve" id="peso" disabled="disabled" class="control" min="0" value='<?php echo  $peso; ?>' name="peso" style="width:50px;">
					<label class="datos">(Kg)</label>&nbsp;&nbsp;
					<label class="datos">Talla:</label>
					<input type="number" class="bodersueve" id="talla" class="control" disabled="disabled" min="0" value='<?php echo  $talla; ?>' name="talla" style="width:50px;">
					<label class="datos">(Cm)</label>&nbsp;&nbsp;
					<label class="datos">Spo2:</label>
					<input type="number" class="bodersueve" disabled="disabled" id="spo2" class="control" min="0" value='<?php echo  $spo2; ?>' name="spo2" style="width:50px;">&nbsp;&nbsp;
					<label class="datos">Fc:</label>
					<input type="number" class="bodersueve" disabled="disabled" id="frecuencia_c" class="control" min="0" value='<?php echo  $frecuencia_c; ?>' name="fc" style="width:50px;">
					<label class="datos">(X) &nbsp;&nbsp;Fr:</label>&nbsp;&nbsp;
					<input type="number" class="bodersueve" disabled="disabled" id="frecuencia_r" class="control" min="0" value='<?php echo  $frecuencia_r; ?>' name="fr" style="width:50px;">
					<label class="datos">(X)</label>
				</div>
				<label class="datos">Temperatura:</label>
				<input type="number" class="bodersueve" disabled="disabled" id="temperatura" class="control" min="0" value='<?php echo  $temperatura; ?>' name="temperatura" style="width:50px;">&nbsp;&nbsp;
				<label class="datos">(ºC)</label>&nbsp;&nbsp;
				<label class="datos">T/a:</label>
				<input type="number" class="bodersueve" disabled="disabled" id="ta_alta" class="control" min="0" value='<?php echo  $tension_alta; ?>' placeholder="t.a" name="ta" style="width:50px;">
				<input type="number" class="bodersueve" disabled="disabled" id="ta_baja" class="control" min="0" value='<?php echo  $tension_baja; ?>' placeholder="t.b" name="ta" style="width:50px;">
				<label class="datos">(MMHg)</label>&nbsp;&nbsp;
				<label class="datos">IMC</label>
				<input type="number" class="bodersueve" disabled="disabled" id="imc" class="control" min="0" value='<?php echo  $imc; ?>' placeholder="t.b" name="ta" style="width:50px;">

				<!-- <label class="labelfechaconsulta">Fecha Consulta</label> -->
				<input type="hidden" class="sinborder" id="fechaconsulta" disabled="disabled" style="width:120px;" value='<?php echo $fecha_creacion; ?>' />
			</fieldset>
			<span class="labelimpresiond"><b>MOTIVO DE CONSULTA</b>: </span>
			<input type="text" id="motivo_consulta" readonly="readonly" class="control impresiond" style="width:400px;" value='<?php echo $motivo_consulta; ?>'>
			<select class="classic" id="aciones" name="gradointruccion" data-style="btn-primary" style="width:320px;">
				<option value=0 selected disabled>HISTORIAL INFORMATIVO</option>
				<option value="1">ENFERMEDAD ACTUAL</option>
				<option value="2">ALERGIAS A MEDICAMENTOS</option>
				<option value="3">ANTECEDENTES PATOLOGICOS FAMILIARES</option>
				<option value="4">ANTECEDENTES QUIRURGICOS</option>
				<option value="5">ANTECEDENTES PATOLOGICOS PERSONALES</option>
				<option value="6">EXAMEN FISICO</option>
				<option value="7">PARACLINICOS</option>
				<option value="8">IMPRESION DIAGNOSTICA</option>
				<option value="9">PLAN</option>
			</select>
			<br>
			<br>
			<div class="enfermedadactual" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ENFERMEDAD ACTUAL</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_enfermedad_actual">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="table_enfermedad_actual" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Diagnostico</td>
										<td style="width:60%">Medico</td>
										<td>Fecha </td>
										<td>Fecha Actualizacion</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_enfermedad_actual">
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
				<!-- Ventana Modal ENFERMEDAD ACTUAL -->
				<div class="modal fade" id="modal_enfermedad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div id="datos_ajax_register"></div>
								<div class="span">
									<input type="hidden" id="id" value='' />
									<input type="hidden" id="id_psicologiap" value='' />

									<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
									<label>Enfermedad Actual</label>
									<textarea class="observacion" id="enfermedadactual" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" id="btnagregar_enfermedadactual">Guardar datos</button>
								<button type="button" class="btn btn-primary" id="btnActualizar_enfermedadactual">Actualizar</button>
								<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							</div>
						</div>
					</div>
				</div>


			</div>
			<div class="alergiasmedicamentos" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ALERGIAS A MEDICAMENTOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_alergias_medicamentos">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="table_alergias_medicamentos" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Fecha </td>
										<td>Fecha Actualizacion</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_alergias_medicamentos">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Ventana Modal ALERGIAS A MEDICAMENTOS -->
			<div class="modal fade" id="alergias_medica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion" id="alergias" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_alergias_medi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_alergias_medi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
			<div class="antecedentespatologicosfamiliares" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES PATOLOGICOS FAMILIARES</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar2" id="btnMostrar_antecedentes_pf">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="antecedentes_pf" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Fecha </td>
										<td>Fecha Actualizacion</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_pf">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Ventana Modal Antecedentes Patologicos Familiares -->
			<div class="modal fade" id="antecedentes_familiares" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion" id="antecedentes_patologicosf" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_familiares">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_familiares">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="antecedentesquirurgicos" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES QUIRURGICOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar2" id="btnMostrar_antecedentes_qx">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="antecedentes_qx" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_qx">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal Antecedente Quirurgicos-->
			<div class="modal fade" id="modal_antecedentes_quirurgicos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion" id="antecedentes_quirurgicos" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_quirurgicos">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_quirurgicos">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


			<div class="antecedentespatologicospersonales" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES PATOLOGICOS PERSONALES</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar2" id="btnMostrar_antecedentes_pp">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="antecedentes_pp" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_pp">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Ventana Modal Antecedentes Patologicos Personales -->
			<div class="modal fade" id="antecedentes_personales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion" id="antecedentes_patologicosp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_personales">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_personales">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>



			<div class="examenfisico" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>EXAMEN FISICO</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_examen_fisico">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="examen_fisico" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_examen_fisico">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal EXAMEN FIFISCO -->
			<div class="modal fade" id="modalexamen_fisico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion" id="obs_examen_fisico" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_examen_fisico">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_examen_fisico">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="paraclinicos" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>PARACLINICOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_paraclinicos">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="tableparaclinicos" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_paraclinicos">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal PARACLINICOS -->
			<div class="modal fade" id="modal_paraclinicos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion" id="obs_paraclinicos" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_paraclinicos">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_paraclinicos">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="impresiondiagnostica" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>IMPRESION DIAGNOSTICA</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_impresion_diag">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="impresion_diag" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_impresion_diag">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal impresion diagnostica -->
			<div class="modal fade" id="modal_impresion_diag" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion" id="obs_impresion_diag" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_impresion_diag">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_impresion_diag">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
			<div class="plan" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>PLAN</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_plan">Agregar(+)</button>
						<div class="col-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="table-hover table-bordered" id="table_plan" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_plan">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<br>
			<!-- Ventana Modal PLAN -->
			<div class="modal fade" id="modal_plan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion" id="obs_plan" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_plan">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_plan">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<br>

			<div class="row">
				<div class="col-lg-5">

				</div>
				<div class="col-lg-6">
					<!-- <button type="button" class="btn btn btn-primary btnActualizar" id="btnActualizar_enfermedadactual">Actualizar</button>
						<button type="button" class="btn btn btn-primary btnActualizar" id="btnActualizar_alergias_medicamentos">Actualizar</button>  	
						<button type="button" class="btn btn btn-primary btnActualizar" id="btnAntecedentes_patologicosf">Actualizar</button>
						<button type="button" class="btn btn btn-primary btnActualizar" id="btnAntecedentes_quirurgicos">Actualizar</button>
						<button type="button" class="btn btn btn-primary btnActualizar" id="btnAntecedentes_patologicosp">Actualizar</button>
						<button type="button" class="btn btn btn-primary btnActualizar" id="btnexamen_fisico">Actualizar</button>
						<button type="button" class="btn btn btn-primary btnActualizar" id="btnparaclinicos">Actualizar</button>
						<button type="button" class="btn btn btn-primary btnActualizar" id="btnimpresiondiag">Actualizar</button>
						<button type="button" class="btn btn btn-primary btnActualizar" id="btnplan">Actualizar</button>
																-->



				</div>
			</div>

		</div>




		<!--****** ********************PSICOLOGIA PRIMARIA************************ -->
		<div class="form-group has-feedback" id="cajas6" style="display: none;">
			<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?>' />
			<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?>' />
			<br>
			<fieldset class="bodersueve_fieldset">
				<label class="titulo_psicologia">ATENCION PSICOLOGICA PRIMARIA</label>
				<legend class="legend"><b>Datos del Trabajador(A):</b></legend>
				<label class="labelfecha">Fecha de Asistencia</label>
				<input type="text" class="sinborder" id="fecha_asist" style="width:120px;" value='<?php echo $fecha_asistencia; ?>' />
				<div>
					<label class="labelnombreApellido">Nombre y Apellido:</label>
					<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:400px;" value='<?php echo $nombre_titular . ' ' . $apellido_titular; ?>'>
					<label class="labelCedula">Cedula:</label>
					<input type="text" class="sinborder" id="cedulaT" disabled="disabled" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
				</div>
				<div>
					<label class="labelsexot">Genero:</label>
					<input type="text" class="sinborder" id="genereot" disabled="disabled" style="width:50px;" value='<?php echo $sexot; ?>'>
					<label class="labeledad">Edad:</label>
					<input type="text" class="sinborder" id="edad_actualt" disabled="disabled" style="width:120px;" value='<?php echo $edad_actualt; ?>'>
					<label class="labelfecha_nacimientot">Fecha de Nacimiento:</label>
					<input type="text" class="sinborder" id="fecha_nacimientot" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nacimientot; ?>'>
				</div>
				<div>
					<label class="labeldepartamento">Unidad Administrativa:</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" id="departamento" disabled="disabled" style="width:270px;" value='<?php echo $departamento; ?>'>
					<label class="labeltipo_de_personal">Cargo:</label>
					<input type="text" class="sinborder" id="tipo_de_personal" disabled="disabled" style="width:300px;" value='<?php echo $tipo_de_personal; ?>'>
				</div>
			</fieldset>
			<br>
			<label class="label_1ra_conslulta">Fecha 1ra Consulta:</label>
			<input type="text" class="sinborder" id="fecha_1ra_consulta" style="width:120px;" value='<?php echo $fecha_asistencia; ?>' />
			<fieldset class="bodersueve_fieldset">
				<legend class="legend"><b>Datos del Beneficiario(A):</b></legend>
				<div>
					<label class="labelnombreApellidoB">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;
					<input type="text" class="sinborder" id="nombrepellidoB" disabled="disabled" style="width:300px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
					<label class="labelCedulab">Cedula</label>
					<input type="text" class="sinborder" id="cedulab" disabled="disabled" class="control" style="width:70px;" value='<?php echo $cedula; ?>'>
					<label class="labelhistorial"><u>Nº Historial</u> </label>
					<input type="text" class="sinborder" disabled="disabled" id="numeroHistorial" class="control" style="width:110px;" value='<?php echo 'F' . '' . $cedula; ?>'>
				</div>
				<div>
					<label class="labelsexo">Genero</label>&nbsp;&nbsp;
					&nbsp;&nbsp;<input type="text" class="sinborder" disabled="disabled" id="sexo" class="control" style="width:50px;" value='<?php echo  $sexo; ?>'>
					<label class="labelEdad">Edad</label>
					<input type="text" class="sinborder" id="edad" disabled="disabled" class="control" style="width:100px;" value='<?php echo $edad_actual; ?>'>
					<label class="labelfechaNacimiento">Fecha De Nacimiento</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="sinborder" id="fecha_nacimiento" class="control" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nac_familiares; ?> ' />
					<label class="labeldescripcion_parentesco">Parentesco</label>
					<input type="text" class="sinborder" id="descripcion_parentesco" disabled="disabled" class="control" style="width:60px;" value='<?php echo $descripcion_parentesco; ?> '>
					<label class="labelocupacion">Ocupacion</label>
					<input type="text" class="sinborder" id="ocupacion" disabled="disabled" class="control" style="width:100px;" value='<?php echo $ocupacion; ?> '>
					&nbsp;&nbsp;<label class="labeltelefono">Nº Telefono</label>&nbsp;&nbsp;
					&nbsp;&nbsp;<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:120px;" value='<?php echo  $telefono; ?>'>
					<!-- <label class="labelbeneficiario">Tipo Beneficiario</label>
					<input type="text" class="sinborder" id="Tbenficiario"  disabled="disabled"  class="control" style="width:100px;" value='Familiar'>  -->
				</div>
			</fieldset>
			<br>
			<div class="impresion_diag">
				<span class="labelimpresiond"><B>IMPRESION DIAGNOSTICA:</B> </span> &nbsp;&nbsp;
				<textarea class="textimpresiond" id="textimpresiond" onkeyup="mayus(this);" autocomplete="off" name="textimpresiond"> </textarea>
				<br>
				<br>
				<span class="labelimpresiond"><B><u>SEGUIMIENTOS:</u></B> </span>


				<button type="button" class="btn btn btn-primary btnagregar" id="btnagregar">Agregar(+)</button>
				<table class="table-hover table-bordered" id="table_psicologia" cellspacing="2" width="100%">
					<thead>
						<tr>
							<td class="text-center" style="width: 1px;">Nº</td>
							<td class="text-center" style="width: 5px;">F.SEGUIMIENTO</td>
							<td class="text-center" style="width: 260px;">EVOLUCION</td>
							<td class="text-center" style="width: 20px;">PSICOLOG@</td>

							<td class="text-center" style="width: 20px;">Acciones</td>
						</tr>
					</thead>
					<tbody id="table_psicologia_primaria">
					</tbody>
				</table>
				<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
				<!-- Ventana Modal -->
				<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
					<div class="modal-dialog" role="document">
						<div class="modal-content">

							<div class="modal-body">
								<div id="datos_ajax_register"></div>
								<div class="span">
									<input type="hidden" id="id_psicologiap" value='' />

									<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
									<label>EVOLUCION </label>
									<textarea class="observacion" id="evolucion" onkeyup="mayus(this);" autocomplete="off" name="observacion"> </textarea>
								</div>
								<div class="span">
									<label>OBSERVACION </label>
									<textarea class="observacion" id="observacion" onkeyup="mayus(this);" autocomplete="off" name="observacion"> </textarea>
								</div>
								<!-- <div class="span"> 
											<label class="labelsexo">ACTIVO</label>
											<input type="checkbox" class="borrado" id="borrado" name="borrado" value='false'>					 
										</div> -->
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
								<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
								<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							</div>
						</div>

					</div>

				</div>


			</div>



			</body>

			</html>





			<script>
				function mayus(e) {
					e.value = e.value.toUpperCase();
				}
			</script>


			<style type="text/css">
				option {
					font-family: verdana, arial, helvetica, sans-serif;
					font-size: 14px;
					border: 3;
					border-radius: 20%;
				}

				option {
					border-color: blueviolet;
				}
			</style>


			</body>

			</html>

			<script>
				var id_especialidad = $('#id_especialidad').val();
				if (id_especialidad == '5') {
					//document.getElementById("cajas2").style.visibility = "visible"; 
					$("#cajas2").show();

				} else if (id_especialidad == '1') {
					$("#cajas1").show();

					//document.getElementById("cajas1").style.visibility = "visible"; 



				} else if (id_especialidad == '2') {
					$("#cajas2").show();


				} else {
					alert('eres otro');
				}
			</script>



			<script>
				var botones = document.querySelectorAll('.btn-expandir');
				var texto_expandir = document.querySelectorAll('.texto_expandir');
				botones.forEach((elemento, clave) => {
					elemento.addEventListener('click', () => {
						texto_expandir[clave].classList.toggle("abrir_cerrar")
					});

				});
			</script>
			<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->

			<script type="text/javascript">
				function valideKey(evt) {

					// code is the decimal ASCII representation of the pressed key.
					var code = (evt.which) ? evt.which : evt.keyCode;

					if (code == 8) { // backspace.
						return true;
					} else if (code >= 48 && code <= 57) { // is a number.
						return true;
					} else { // other keys.
						return false;
					}
				}
			</script>

			<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->

			<script>
				// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
				$(function() {
					$("#fecha_del_dia").datepicker({
						dateFormat: 'dd/mm/yy',
						changeMonth: true,
						changeYear: true
					});
					$("#fecha1").datepicker({
						dateFormat: 'dd/mm/yy',
						changeMonth: true,
						changeYear: true
					});


				});
			</script>



			<script>
				$(document).ready(function() {

					var now = new Date();

					var day = ("0" + now.getDate()).slice(-2);
					var month = ("0" + (now.getMonth() + 1)).slice(-2);
					var today = day + "-" + month + "-" + now.getFullYear();
					// var today= (day)+"-"+(month)+"-"+now.getFullYear();
					// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
					$("#fecha_del_dia").val(today);
				});
			</script>
			<?= $this->endSection(); ?>