<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/medicos.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<br>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row ">
		<div class="col-5">
		</div>
		<div class="col-5">
			<h3 class="center">Registro De Medicos </h3>
		</div>

		<div class="col-2">
			<button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button><br /><br />
			<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button><br /><br />

		</div>
	</div>


	<div class="row">
		<div class="col-lg-12">
			<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
			<table class="display" id="table_medicos" style="width:100%" style="margin-top: 20px">
				<thead>
					<tr>
						<td>ID</td>
						<td>Cedula</td>
						<td>Nombre </td>
						<td>Apellido</td>
						<td>Telefono </td>
						<td>Especialidad</td>
						<td>Estatus</td>
						<td class="text-center" style="width: 50px;">Acciones</td>
					</tr>
				</thead>
				<tbody id="lista_de_categoria">
				</tbody>
			</table>
		</div>
	</div>


	<!-- Ventana Modal -->
	<form action="" method="post" name="">

		<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<div class="row">
							<div class="col-12">

								<h4> Datos del Medico </h4>
							</div>

						</div>
						<input type="hidden" autocomplete="off" class="form-control categoria" id="id_especialidad" value="" name="id_especialidad" style="width:80px;" placeholder="">
						<input type="hidden" autocomplete="off" class="form-control categoria" id="id_medico" value="" name="id_medico" style="width:80px;" placeholder="">
					</div>
					<div class="modal-body">


						<div id="datos_ajax_register"></div>
						<div class="input-group">

							<label class="input-fill">
								<input type="text" id="cedula" name="cedula" style="width: 400px;" minlength="7" maxlength="8" onkeypress="return valideKey(event);" placeholder=" Cedula: " autocomplete="off" minlength="7" maxlength="8" required pattern="[0-9]+" />
								<!-- <span class="input-label">Cedula:</span> -->
								<i class="		fas fa-shield-alt"></i>
							</label>
						</div>

						<div class="input-group">
							<label class="input-fill">
								<input type="text" id="nombre" name="nombre" style="width: 400px;" onkeyup="mayus(this);" placeholder=" Nombre:" required pattern="[aA-Za-z]+" autocomplete="off" />
								<!-- <span class="input-label">Nombre</span> -->
								<i class="	fas fa-portrait"></i>
							</label>
						</div>
						<div class="input-group">
							<label class="input-fill">
								<input type="text" id="apellido" name="apellido" style="width: 400px;" onkeyup="mayus(this);" placeholder=" Apellido:" required pattern="[aA-Za-z]+" autocomplete="off" />
								<!-- <span class="input-label">Apellido</span> -->
								<i class="far fa-id-badge"></i>
							</label>
						</div>

						<div class="input-group">
							<label class="input-fill">
								<input type="text" id="telefono" name="telefono" style="width: 400px;" onkeypress="return valideKey(event);" placeholder=" Telefono:" required pattern="[0-9]+" autocomplete="off" />
								<!-- <span class="input-label">Username</span> -->
								<i class="	fas fa-user-circle"></i>
							</label>
						</div>


						<label class="labelespecialidad">Especialidad</label>
						<select class="form-control especialidad " id="especialidad" name="especialidad" data-style="btn-primary" style="width:300px;">
							<option value="seleccione">seleccione</option>
						</select>

						<label class="activo" id="activo">Activo</label>
						<input type="checkbox" class="borrado" id="borrado" name="borrado" value='false'>
						<br>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	<script>
		function mayus(e) {
			e.value = e.value.toUpperCase();
		}
	</script>
	<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
	<script type="text/javascript">
		function valideKey(evt) {

			// code is the decimal ASCII representation of the pressed key.
			var code = (evt.which) ? evt.which : evt.keyCode;

			if (code == 8) { // backspace.
				return true;
			} else if (code >= 48 && code <= 57) { // is a number.
				return true;
			} else { // other keys.
				return false;
			}
		}
	</script>

	<script>
		$(function() {
			//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});
		});
	</script>
	<?= $this->endSection(); ?>