<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<input type="hidden" class="sinborder" id="id_especialidad" disabled="disabled" style="width:300px;" value='<?php echo $especialidad; ?>'>

<link href="/css/actualizar_citas_cortesia.css" rel="stylesheet" type="text/css" />
<link href="/css/expandir.css" rel="stylesheet" type="text/css" />

<br />

<style>
	table.dataTable thead,
	table.dataTable tfoot {
		background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
	}
</style>
<div class="row ">
	<div class="col-md-2 col-sm-2 col-lg-2 col-xl-2">
	</div>
	<input type="hidden" disabled="disabled" id="numeroHistorialDatatable" style="width:100px;" value='<?php echo $n_historial; ?>'>
	<div class="col-md-8 col-sm-8 col-lg-8 col-xl-8" id="prueba1">
		<input type="hidden" id="id_user" value='<?= session('id_user'); ?> ' />
		<input type="hidden" class="sinborder" id="id_historial_medico" disabled="disabled" style="width:100px;" value='<?php echo $id_historial_medico; ?>'>
		<input type="hidden" class="sinborder" id="id_consulta" disabled="disabled" style="width:100px;" value='<?php echo $id_consulta; ?>'>
		<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
		<!--***************************************************MEDICINA GENERAL*********************************************** -->
		<div class="form-group has-feedback" id="cajas1" style="display: none;">
			<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?>' />
			<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?>' />

			<br>
			<fieldset class="bodersueve_fieldset">
				<!-- <img src="<=base_url()?>/img/2.png" width="120" alt="Avatar" id="img_beneficiario">  -->
				<label class="titulo_mgeneral">FICHA DE REGISTRO / MEDICINA GENERAL</label>
				<legend class="legend"><b>Datos del Trabajador</b></legend>
				<div>
					<label class="labelfecha">Fecha de Asistencia </label>
					<input type="text" id="fecha_creacion" style="width:120px;" value='<?php echo $fecha_asistencia; ?>'>
					<br>
					<label class="datos">Nombre y Apellido</label>
					<input type="text" id="nombrepellidoT" class="sinborder" disabled="disabled" style="width:300px;" value='<?php echo $nombre_titular . ' ' . $apellido_titular; ?>'>
				</div>
				<label class="datos">Cedula</label>
				<input type="text" id="cedulaT" class="sinborder" disabled="disabled" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
				<label class="datos">Departamento</label>
				<input type="text" id="departamento" class="sinborder" disabled="disabled" style="width:300px;" value='<?php echo $departamento; ?>'>
			</fieldset>
			<fieldset class="bodersueve_fieldset">
				<legend class="legend"><b>Datos del Beneficiario</b></legend>
				<div>
					<label class="datos">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" id="nombrepellidoB" class="sinborder" disabled="disabled" style="width:300px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
					&nbsp;&nbsp;&nbsp;&nbsp;<label class="datos"><u>Nº Historial</u> </label>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" disabled="disabled" class="sinborder" id="numeroHistorial" class="control" style="width:120px;" value='<?php echo 'C' . '' . $cedula; ?>'>
				</div>
				<div>
					<label class="datos">Fecha De Nacimiento</label>
					<input type="text" class="sinborder" id="fecha_nacimiento" class="control" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nac_cortesia; ?> ' />
					&nbsp;&nbsp;&nbsp;&nbsp;<label class="datos">Cedula</label>
					<input type="text" class="sinborder" id="cedulab" disabled="disabled" class="control" style="width:100px;" value='<?php echo $cedula; ?>'>
					&nbsp;&nbsp;&nbsp;&nbsp;<label class="datos">Tipo Beneficiario</label>
					<input type="text" class="sinborder" id="Tbenficiario" disabled="disabled" class="control" style="width:100px;" value='Cortesia'>
				</div>
				<label class="datos">Edad</label>
				<input type="text" class="sinborder" id="edad" disabled="disabled" class="control" style="width:100px;" value='<?php echo $edad_actual; ?>'>
				<label class="datos">Nº Telefono</label>
				<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:120px;" value='<?php echo  $telefono; ?>'>
				<label class="datos">Sexo</label>
				<input type="text" class="sinborder" disabled="disabled" id="sexo" class="control" style="width:50px;" value='<?php echo  $sexo; ?>'>
				<label>T.Beneficiario:</label>

			</fieldset>
			<br>
			<div>
				<fieldset class="bodersueve_fieldset">
					<legend class="legend"><b>Signos Vitales </b> </legend>
					<div>
						<label class="labelpeso">Peso:</label>
						<input type="number" class="bodersueve" id="peso" class="control" min="0" value='<?php echo  $peso; ?>' name="peso" style="width:50px;">
						<label class="labelkg">(Kg)</label>&nbsp;&nbsp;
						<label class="labeltalla">Talla:</label>
						<input type="number" class="bodersueve" id="talla" class="control" min="0" value='<?php echo  $talla; ?>' name="talla" style="width:50px;">
						<label class="labelcm">(Cm)</label>&nbsp;&nbsp;
						<label class="labelspo2">Spo2:</label>
						<input type="number" class="bodersueve" id="spo2" class="control" min="0" value='<?php echo  $spo2; ?>' name="spo2" style="width:50px;">&nbsp;&nbsp;
						<label class="labelfc">Fc:</label>
						<input type="number" class="bodersueve" id="frecuencia_c" class="control" min="0" value='<?php echo  $frecuencia_c; ?>' name="fc" style="width:50px;">
						<label class="labelfr">(X) &nbsp;&nbsp;Fr:</label>&nbsp;&nbsp;
						<input type="number" class="bodersueve" id="frecuencia_r" class="control" min="0" value='<?php echo  $frecuencia_r; ?>' name="fr" style="width:50px;">
						<label class="labelx">(X)</label>&nbsp;&nbsp;
						<label class="labeltipodesangre">Imc</label>
						<input type="number" class="bodersueve" id="imc" class="control" value='<?php echo  $imc; ?>' style="width:50px;">
					</div>
					<label class="labeltemperatura">Temperatura:</label>
					<input type="number" class="bodersueve" id="temperatura" class="control" min="0" value='<?php echo  $temperatura; ?>' name="temperatura" style="width:50px;">&nbsp;&nbsp;
					<label class="labelc">(ºC)</label>&nbsp;&nbsp;
					<label class="labelta">T/a:</label>
					<input type="number" class="bodersueve" id="ta_alta" class="control" min="0" value='<?php echo  $tension_alta; ?>' placeholder="t.a" name="ta" style="width:50px;">
					<input type="number" class="bodersueve" id="ta_baja" class="control" min="0" value='<?php echo  $tension_baja; ?>' placeholder="t.b" name="ta" style="width:50px;">
					<label class="labelmmhg">(MMHg)</label>&nbsp;&nbsp;

				</fieldset>
				<br>
				<fieldset class="bodersueve_fieldset">
					<span class="labelimpresiond"><b>MOTIVO DE CONSULTA</b>: </span>
					<textarea id="motivo_consulta" class="impresiond" onkeyup="mayus(this);" autocomplete="off" name="observacion_gb"><?php echo ' ' . ' ' . $motivo_consulta; ?></textarea>
					<button type="button" class="btn btn btn-primary btnActualizar_signos" id="btnActualizar_signos_mg">Actualizar</button>&nbsp;&nbsp;
					<select class="classic" id="aciones" data-style="btn-primary" style="width:220px;">
						<option value="0">Seleccione</option>
					</select>
				</fieldset>
				<br>

				<div class="enfermedadactual" style="display:none;">
					<fieldset class="bodersueve_fieldset">
						<div class="span">
							<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
								<b>DIAGNOSTICO</b>
							</SPAN>
							<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar" id="btnMostrar_enfermedad_actual">Agregar(+)</button>
							<br>
							<br>
							<div class="row">

								<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
									<table class="display" id="table_enfermedad_actual" cellspacing="2" width="100%">

										<thead>
											<tr>

												<td style="width:60%">Diagnostico</td>
												<td style="width:20%">Medico</td>
												<td>Fecha </td>
												<!-- <td>Actualizacion</td>					 -->
												<td>Acciones</td>
											</tr>
										</thead>
										<tbody id="lista_enfermedad_actual">
										</tbody>
									</table>
								</div>
							</div>
					</fieldset>
				</div>
				<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
				<!-- Ventana Modal ENFERMEDAD ACTUAL -->
				<div class="modal fade" id="modal_enfermedad" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-body">
								<div id="datos_ajax_register"></div>
								<div class="span">
									<input type="hidden" id="id" value='' />
									<input type="hidden" id="id_psicologiap" value='' />

									<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
									<label>DIAGNOSTICO</label>
									<textarea class="observacion_gb" id="enfermedadactual" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" id="btnagregar_enfermedadactual">Guardar datos</button>
								<button type="button" class="btn btn-primary" id="btnActualizar_enfermedadactual">Actualizar</button>
								<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="alergiasmedicamentos" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ALERGIAS A MEDICAMENTOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_alergias_medicamentos">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="table_alergias_medicamentos" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Fecha </td>
										<td>Fecha Actualizacion</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_alergias_medicamentos">
								</tbody>
							</table>
						</div>
					</div>


				</div>
			</div>
			<!-- Ventana Modal ALERGIAS A MEDICAMENTOS -->
			<div class="modal fade" id="alergias_medica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="alergias" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_alergias_medi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_alergias_medi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="antecedentespatologicosfamiliares" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES PATOLOGICOS FAMILIARES</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar2" id="btnMostrar_antecedentes_pf">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="antecedentes_pf" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Fecha </td>
										<td>Fecha Actualizacion</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_pf">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Ventana Modal Antecedentes Patologicos Familiares -->
			<div class="modal fade" id="antecedentes_familiares" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="antecedentes_patologicosf" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_familiares">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_familiares">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="antecedentesquirurgicos" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES QUIRURGICOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar2" id="btnMostrar_antecedentes_qx">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="antecedentes_qx" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_qx_mg">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal Antecedente Quirurgicos-->
			<div class="modal fade" id="modal_antecedentes_quirurgicos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="antecedentes_quirurgicos" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_quirurgicos">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_quirurgicos">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="antecedentespatologicospersonales" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES PATOLOGICOS PERSONALES</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar2" id="btnMostrar_antecedentes_pp">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="antecedentes_pp" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_pp">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>


			<!-- Ventana Modal Antecedentes Patologicos Personales -->
			<div class="modal fade" id="antecedentes_personales" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="antecedentes_patologicosp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_personales">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_personales">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>



			<div class="examenfisico" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>EXAMEN FISICO</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_examen_fisico">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="examen_fisico" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_examen_fisico">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal EXAMEN FIFISCO -->
			<div class="modal fade" id="modalexamen_fisico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_examen_fisico" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_examen_fisico">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_examen_fisico">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
			<div class="paraclinicos" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>PARACLINICOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_paraclinicos">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="tableparaclinicos" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_paraclinicos">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Ventana Modal PARACLINICOS -->
			<div class="modal fade" id="modal_paraclinicos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_paraclinicos" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_paraclinicos">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_paraclinicos">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="impresiondiagnostica" style="display:none;">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>IMPRESION DIAGNOSTICA</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar" id="btnMostrar_impresion_diag">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="impresion_diag" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_impresion_diag">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal impresion diagnostica -->
			<div class="modal fade" id="modal_impresion_diag" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_impresion_diag" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_impresion_diag">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_impresion_diag">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
			<div class="plan" style="display:none;">
				<fieldset class="bodersueve_fieldset">
					<div class="span">
						<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
							<b>PLAN</b>
						</SPAN>
						<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar" id="btnMostrar_plan">Agregar(+)</button>
						<br>
						<br>
						<div class="row">


							<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
								<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
								<table class="display" id="table_plan" cellspacing="2" width="100%">
									<thead>
										<tr>

											<td style="width:60%">Descripcion</td>
											<td>Medico</td>
											<td>Especialidad</td>
											<td>Fecha</td>
											<td>Acciones</td>
										</tr>
									</thead>
									<tbody id="lista_plan">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<br>
			<!-- Ventana Modal PLAN -->
			<div class="modal fade" id="modal_plan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_plan" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_plan">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_plan">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-5">

				</div>
				<div class="col-lg-6">

				</div>
			</div>

		</div>

		<div class="habitos" style="display:none;">
			<div class="span">
				<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
					<b>HABITOS PSICOSOCLIALES</b>
				</SPAN>

				<br>
				<div class="row">
					<!-- <button type="button" class="btn btn btn-primary btnMostrar3" id="btnMostrar_habitos">Agregar(+)</button>  -->
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="habitos_psicosociales" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Fecha</td>
								</tr>
							</thead>
							<tbody id="lista_habitos">
							</tbody>
						</table>
					</div>
				</div>

			</div>


		</div>

		<div class="reposos" style="display:none;">
			<fieldset class="bodersueve_fieldset">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>REPOSOS</b>
					</SPAN>
					<button type="button" style="position: absolute; top: 200 px; left: 420 px;" class="btn btn btn-primary btnMostrar" id="btnMostrar_reposo">Agregar(+)</button>
					<br>
					<div class="row">

						<input type="hidden" class="sinborder" id="nombre_medico" disabled="disabled" value='<?= session('nombreUsuario'); ?> ' />


						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="table_reposos" style="width:100%" style="margin-top: 20px">
								<thead>
									<tr>

										<td class="text-center" style="width: 5%;">N_HISTORIAL</td>
										<td class="text-center" style="width: 60%;">MOTIVO</td>
										<td class="text-center" style="width: 5%;">DESDE</td>
										<td class="text-center" style="width: 5%;">HASTA</td>
										<td class="text-center" style="width: 20%;">MEDICO: </td>
										<td class="text-center" style="width: 2%;">ESTATUS: </td>
										<td class="text-center" style="width: 2%;">ACCIONES: </td>

									</tr>
								</thead>
								<tbody id="lista_reposos">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<!-- Ventana Modal REPOSOS -->
		<div class="modal fade" id="modal_reposos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<h5 class="modal-title text-center">REPOSO MEDICO</h5>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" class="sinborder" id="id_consulta" value='<?php echo $id_consulta; ?>'>
							<input type="hidden" id="n_historialT" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="id_reposo" value='' />

							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
						</div>
						<fieldset>

							<legend class="legend">Datos del Trabajador</legend>
							<div>
								<label for="name">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="nombretitular" id="nombretitular" readonly="readonly" disabled="disabled" name="nombretitular"> &nbsp;
							</div>
							<div>
								<label>Fecha De Nacimiento</label>
								<input type="text" class="fecha_nacimientotitular" readonly="readonly" disabled="disabled" id="fecha_nacimientotitular" name="fecha_nacimientotitular">
								<label>CI</label>
								<input type="text" class="cedulatitular" id="cedulatitular" disabled="disabled" name="cedulatitular" readonly="readonly">
								<label>Edad</label>
								<input type="text" class="edadtitular" style="width:110px;" id="edadtitular" disabled="disabled" name="edadtitular" readonly="readonly">
							</div>

							<div>
								<label for="mail">Unidad Administrativa</label>
								<input type="text" class="unidadtitular" id="unidadtitular" readonly="readonly" disabled="disabled" style="width:51%;" name="unidadtitular">
								<label for="mail">Telefono</label>
								<input type="text" class="telefonotitular" style="width:90px;" readonly="readonly" disabled="disabled" id="telefonotitular" name="telefonotitular">
							</div>

							<div>
								<label for="mail">Tipo de Reposo</label>
								<select class="custom-select vm" style="width:100px;" id="tipo_reposo" autocomplete="off" data-placeholder="Seleccione" autocomplete="off" required>
									<option value="0" disabled="disabled">seleccione</option>
									<option value="1">Dias</option>
									<option value=" 2">Horas</option>
								</select>
								&nbsp;&nbsp;
								<label for=" min">Inicio</label>
								<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;

								<label id="label_retorno">Retorno</label>&nbsp;&nbsp;
								<input type="date" disabled="disabled" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;&nbsp;

								<label id="labe_dias" disabled="disabled">Dias</label>&nbsp;&nbsp;
								<input type=" text" class="dias" disabled="disabled" style=" width:40px;" name="dias" value="0" min="0" id="dias">&nbsp;&nbsp;
								<input type="text" style="display: none;" disabled="disabled" style=" width:40px;" name="dias" min="0" id="dia_en_horas">&nbsp;&nbsp;



								<label id="labe_horas" style="display: none;">Horas</label>&nbsp;&nbsp;
								<input type="text" class="horas" style="display: none;" style="width:20px;" name="horas" value="0" min="0" id="horas">&nbsp;&nbsp;



							</div>
							&nbsp;<label>Motivo de Reposo :</label>
							<textarea class="observacion_reposos" id="motivo_reposo" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</fieldset>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_reposos">Guardar</button>
						<button type="button" style="display: none;" class="btn btn-primary" id="btnActualizar_reposos">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


















		<!--*******************************************PSICOLOGIA PRIMARIA***************************************************** -->
		<div class="form-group has-feedback" id="cajas2" style="display: none;">
			<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?>' />
			<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?>' />
			<br>
			<fieldset class="bodersueve_fieldset">
				<label class="titulo_psicologia">ATENCION PSICOLOGICA PRIMARIA</label>
				<legend class="legend"><b>Datos del Trabajador(A):</b></legend>

				<div>
					<label class="labelfechaasitencia">Fecha de Asistencia</label>
					<input type="text" class="sinborder" id="fecha_asist" disabled="disabled" style="width:80px;" value='<?php echo $fecha_asistencia; ?>' />
					<br>
					<label class="datos">Nombre y Apellido:</label>
					<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:360px;" value='<?php echo $nombre_titular . ' ' . $apellido_titular; ?>'>
					<label class="datos">Cedula:</label>
					<input type="text" class="sinborder" id="cedulaT" disabled="disabled" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
				</div>
				<div>
					<label class="datos">Genero:</label>
					<input type="text" class="sinborder" id="genereot" disabled="disabled" style="width:50px;" value='<?php echo $sexot; ?>'>
					<label class="datos">Edad:</label>
					<input type="text" class="sinborder" id="edad_actualt" disabled="disabled" style="width:120px;" value='<?php echo $edad_actualt; ?>'>
					<label class="datos">Fecha de Nacimiento:</label>
					<input type="text" class="sinborder" id="fecha_nacimientot" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nacimientot; ?>'>
				</div>
				<div>
					<label class="datos">Unidad Administrativa:</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" id="departamento" disabled="disabled" style="width:270px;" value='<?php echo $departamento; ?>'>
					<label class="datos">Cargo:</label>
					<input type="text" class="sinborder" id="tipo_de_personal" disabled="disabled" style="width:300px;" value='<?php echo $tipo_de_personal; ?>'>
				</div>
			</fieldset>
			<br>

			<fieldset class="bodersueve_fieldset">
				<legend class="legend"><b>Datos del Beneficiario(A):</b></legend>
				<div>

					<label class="label_1ra_conslulta">Fecha 1ra Consulta:</label>
					<input type="text" class="sinborder" id="fecha_1ra_consulta" disabled="disabled" style="width:80px;" value='<?php echo $fecha_asistencia; ?>' />
					<br>
					<label class="datos">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;
					<input type="text" class="sinborder" id="nombrepellidoB" disabled="disabled" style="width:300px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
					<label class="datos">Cedula</label>
					<input type="text" class="sinborder" id="cedulab" disabled="disabled" class="control" style="width:70px;" value='<?php echo $cedula; ?>'>
					<label class="datos"><u>Nº Historial</u> </label>
					<input type="text" class="sinborder" disabled="disabled" id="numeroHistorial" class="control" style="width:100px;" value='<?php echo 'C' . '' . $cedula; ?>'>
				</div>
				<div>
					<label class="datos">Genero</label>&nbsp;&nbsp;
					&nbsp;&nbsp;<input type="text" class="sinborder" disabled="disabled" id="sexo" class="control" style="width:50px;" value='<?php echo  $sexo; ?>'>
					<label class="datos">Edad</label>
					<input type="text" class="sinborder" id="edad" disabled="disabled" class="control" style="width:100px;" value='<?php echo $edad_actual; ?>'>
					<label class="datos">Fecha De Nacimiento</label>&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="text" class="sinborder" id="fecha_nacimiento" class="control" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nac_cortesia ?> ' />
					<label class="datos">Parentesco</label>
					<input type="text" class="sinborder" id="descripcion_parentesco" disabled="disabled" class="control" style="width:100px;" value='CORTESIA '>
					<label class="datos">Ocupacion</label>
					<input type="text" class="sinborder" id="ocupacion" disabled="disabled" class="control" style="width:100px;" value='<?php echo $ocupacion; ?> '>
					&nbsp;&nbsp;<label class="datos">Nº Telefono</label>&nbsp;&nbsp;
					&nbsp;&nbsp;<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:120px;" value='<?php echo  $telefono; ?>'>
					<!-- <label class="labelbeneficiario">Tipo Beneficiario</label>
				<input type="text" class="sinborder" id="Tbenficiario"  disabled="disabled"  class="control" style="width:100px;" value='Familiar'>  -->
					<input type="text" class="sinborder" id="Tbenficiario" disabled="disabled" class="control" style="width:100px;" value='Cortesia'>

				</div>
			</fieldset>
			<br>
			<div class="impresion_diagnost">
				<fieldset class="bodersueve_fieldset">
					<span class="labelimpresiond"><B>IMPRESION DIAGNOSTICA:</B> </span>&nbsp;&nbsp;
					<textarea id="textimpresiond" class="textimpresiond" onkeyup="mayus(this);" autocomplete="off" name="textimpresiond"><?php echo ' ' . ' ' . $impresion_diag; ?></textarea>
					<?php if ($impresion_diag !== '') : ?>
						<select class="classic" id="aciones_Psicol" data-style="btn-primary" style="width:180px;">
							<option value="0">Historial</option>
						</select>
					<?php else : ?>
						<select class="classic" id="aciones_Psicol" style="display:none" data-style="btn-primary" style="width:180px;">
							<option value="0">Historial</option>
						</select>
					<?php endif; ?>
				</fieldset>
			</div>

			<div class="enfermedadactual" style="display:none;">

				<fieldset class="bodersueve_fieldset">
					<SPAN class="texto_PSICO" style="position: absolute; top: 200 px; left: 400 px;">
						<b>DIAGNOSTICO</b>
					</SPAN>
					<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_PSICO" id="btnMostrar_enfermedad_actual_psico">Agregar(+)</button>
					<br>
					<br>
					<div class="row">

						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<table ALIGN="right" class="display" id="table_diagnostico" cellspacing="2" style="width:100%">
								<thead>
									<tr>
										<td style="width:25%">Diagnostico</td>
										<td style="width:2%">Medico</td>
										<td style="width:2%">Fecha</td>
										<td style="width:2%">Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_enfermedad_actual">
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</div>
			<!-- Ventana Modal DIAGNOSTICO -->
			<div class="modal fade" id="modal_enfermedad_psico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Diagnostico</label>
								<textarea class="observacion_gb" id="enfermedadactual_psico" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_enfermedadactual_psico">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_enfermedadactual_psico">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


			<div class="plan" style="display:none;">
				<fieldset class="bodersueve_fieldset">
					<div class="span">
						<SPAN class="texto_PSICO" style="position: absolute; top: 200 px; left: 400 px;">
							<b>PLAN</b>
						</SPAN>
						<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_PSICO" id="btnMostrar_plan_psico">Agregar(+)</button>
						<br>
						<br>
						<div class="row">

							<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
								<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
								<table class="display" id="table_plan_PSICO" cellspacing="2" width="100%">
									<thead>
										<tr>

											<td style="width:60%">Descripcion</td>
											<td>Medico</td>
											<td>Especialidad</td>
											<td>Fecha</td>
											<td>Acciones</td>
										</tr>
									</thead>
									<tbody id="lista_plan">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<br>
			<!-- Ventana Modal PLAN -->
			<div class="modal fade" id="modal_plan_psico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>PLAN</label>
								<textarea class="observacion_gb" id="obs_plan_psico" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_plan_psico">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_plan_psico">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


			<button type="button" class="btn btn btn-primary btnagregar_psi" id="btnagregar">Agregar(+)</button>
			<br>
			<span class="labelimpresiond"><B><u>SEGUIMIENTOS:</u></B> </span>



			<table class="display" id="table_psicologia" cellspacing="2" width="100%">
				<thead>
					<tr>
						<td class="text-center" style="width: 1px;">Nº</td>
						<td class="text-center" style="width: 5px;">F.SEGUIMIENTO</td>
						<td class="text-center" style="width: 260px;">EVOLUCION</td>
						<td class="text-center" style="width: 20px;">PSICOLOG@</td>

						<td class="text-center" style="width: 20px;">Acciones</td>
					</tr>
				</thead>
				<tbody id="table_psicologia_primaria">
				</tbody>
			</table>
			<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
			<!-- Ventana Modal -->
			<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>EVOLUCION </label>
								<textarea class="observacion_gb" id="evolucion" onkeyup="mayus(this);" autocomplete="off" name="observacion_gb"> </textarea>
							</div>
							<div class="span">
								<label>OBSERVACION </label>
								<textarea class="observacion_gb" id="observacion" onkeyup="mayus(this);" autocomplete="off" name="observacion_gb"> </textarea>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
								<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button>
								<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Ventana Modal REPOSOS -->
		<div class="modal fade" id="modal_reposos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<h5 class="modal-title text-center">REPOSO MEDICO</h5>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" class="sinborder" id="id_consulta" value='<?php echo $id_consulta; ?>'>
							<input type="hidden" id="n_historialT" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="id_reposo" value='' />

							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
						</div>
						<fieldset>

							<legend class="legend">Datos del Trabajador</legend>
							<div>
								<label for="name">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="nombretitular" id="nombretitular" readonly="readonly" disabled="disabled" name="nombretitular"> &nbsp;
							</div>
							<div>
								<label>Fecha De Nacimiento</label>
								<input type="text" class="fecha_nacimientotitular" readonly="readonly" disabled="disabled" id="fecha_nacimientotitular" name="fecha_nacimientotitular">
								<label>CI</label>
								<input type="text" class="cedulatitular" id="cedulatitular" disabled="disabled" name="cedulatitular" readonly="readonly">
								<label>Edad</label>
								<input type="text" class="edadtitular" style="width:110px;" id="edadtitular" disabled="disabled" name="edadtitular" readonly="readonly">
							</div>

							<div>
								<label for="mail">Unidad Administrativa</label>
								<input type="text" class="unidadtitular" id="unidadtitular" readonly="readonly" disabled="disabled" style="width:51%;" name="unidadtitular">
								<label for="mail">Telefono</label>
								<input type="text" class="telefonotitular" style="width:90px;" readonly="readonly" disabled="disabled" id="telefonotitular" name="telefonotitular">
							</div>

							<div>
								<label for="min">Inicio</label>
								<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
								<label for="hasta">Retorno</label>&nbsp;&nbsp;
								<input type="date" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;&nbsp;
							</div>
							&nbsp;<label>Motivo de Reposo 2:</label>
							<textarea class="observacion_reposos" id="motivo_reposo" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</fieldset>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_reposos">Guardar</button>
						<button type="button" style="display: none;" class="btn btn-primary" id="btnActualizar_reposos">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>




		<!-- 
//*********************************************MEDICINA INTERNA************************************************** -->
		<div class="form-group has-feedback" id="cajas3" style="display: none;">
			<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?> ' />
			<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?> ' />
			<br>

			<fieldset class="bodersueve_fieldset">

				<legend class="legend"><b>Datos del Trabajador(A):</b></legend>

				<div>
					<label class="labelCedula">Cedula:</label>
					<input type="text" class="sinborder" id="cedulaT" disabled="disabled" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
					<label class="labelnombreApellido">Nombre y Apellido:</label>
					<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:320px;" value='<?php echo $nombre_titular . ' ' . $apellido_titular; ?>'>
					<label class="labelsexot">Genero:</label>
					<input type="text" class="sinborder" id="genereot" disabled="disabled" style="width:50px;" value='<?php echo $sexot; ?>'>
				</div>
				<div>

					<label class="labeledad">Edad:</label>
					<input type="text" class="sinborder" id="edad_actualt" disabled="disabled" style="width:40px;" value='<?php echo $edad_actualt; ?>'>
					<label class="labelfecha_nacimientot">Fecha de Nacimiento:</label>
					<input type="text" class="sinborder" id="fecha_nacimientot" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nacimientot; ?>'>
					<label class="labeltipo_de_personal">Cargo:</label>
					<input type="text" class="sinborder" id="tipo_de_personal" disabled="disabled" style="width:280px;" value='<?php echo $tipo_de_personal; ?>'>

				</div>
				<div>
					<label class="labeldepartamento">Unidad Administrativa:</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" id="departamento" disabled="disabled" style="width:270px;" value='<?php echo $departamento; ?>'>
					<label class="labeltelefono">Nº Telefono</label>
					<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:115px;" value='<?php echo  $telefonot; ?>'>
				</div>
			</fieldset>
			<br>
			<fieldset class="bodersueve_fieldset">
				<!-- <img src="<=base_url()?>/img/2.png" width="120" alt="Avatar" id="img_beneficiario"> 				 -->
				<label class="titulo_mgeneral">FICHA DE REGISTRO / MEDICINA INTERNA</label>
				<label class="labelfecha">Cita Programada</label>
				<input type="text" class="sinborder" disabled="disabled" id="fecha_del_dia_mi" style="width:120px;" value='<?php echo $fecha_asistencia; ?>'>
				<input type="hidden" class="sinborder" id="fecha_asist" style="width:80px;" value='<?php echo $fecha_asistencia; ?>' />

				<legend class="legend">Datos del Beneficiario </legend>
				<label class="labelCedula">Cedula</label>
				<input type="text" class="sinborder" id="cedulab" disabled="disabled" style="width:80px;" value='<?php echo $cedula; ?>'>&nbsp;&nbsp;
				<label class="labelnombreApellido">Nombre y Apellido </label>
				<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:340px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
				<div>
					<label class="labelhistorial"><u>Nº Historial</u> </label>&nbsp;&nbsp;
					<input type="text" class="sinborder" disabled="disabled" id="numeroHistorial" style="width:100px;" value='<?php echo 'C' . '' . $cedula; ?>'>&nbsp;&nbsp;
					<label class="labelfechaNacimiento">Fecha De Nacimiento</label>
					<input type="text" class="sinborder" id="fecha_nacimiento" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nac_cortesia; ?> ' />&nbsp;&nbsp;
					<label class="labelEdad">Edad</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" id="edad" disabled="disabled" style="width:100px;" value='<?php echo $edad_actual; ?>'>
					<label class="labelsexo">Sexo</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" disabled="disabled" id="sexo" style="width:50px;" value='<?php echo  $sexo; ?>'>

					<label class="labeltelefono">Nº Telefono</label>
					<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:115px;" value='<?php echo  $telefono; ?>'>
					<label>T.Beneficiario:</label>
					<input type="text" class="bodersueve" disabled="disabled" id="Tbenficiario" disabled="disabled" style="width:80px;" value='CORTESIA'>
				</div>
			</fieldset>
			<br>

			<fieldset class="bodersueve_fieldset">
				<legend class="legend"><b>Signos Vitales </b> </legend>
				<div>

					<label class="labelfechaconlta">Fecha Consulta</label>
					<input type="text" class="sinborder" id="fecha_consulta" disabled="disabled" style="width:120px;" value='<?php echo $fecha_creacion; ?>' />

				</div>
				<div>
					<label class="labelpeso">Peso:</label>
					<input type="number" class="bodersueve" id="peso_mi" class="control" min="0" value='<?php echo  $peso; ?>' name="peso" style="width:50px;">
					<label class="labelkg">(Kg)</label>&nbsp;&nbsp;
					<label class="labeltalla">Talla:</label>
					<input type="number" class="bodersueve" id="talla_mi" class="control" min="0" value='<?php echo  $talla; ?>' name="talla" style="width:50px;">
					<label class="labelcm">(Cm)</label>&nbsp;&nbsp;
					<label class="labelspo2">Spo2:</label>
					<input type="number" class="bodersueve" id="spo2_mi" class="control" min="0" value='<?php echo  $spo2; ?>' name="spo2" style="width:50px;">&nbsp;&nbsp;
					<label class="labelfc">Fc:</label>
					<input type="number" class="bodersueve" id="frecuencia_c_mi" class="control" min="0" value='<?php echo  $frecuencia_c; ?>' name="fc" style="width:50px;">
					<label class="labelfr">(X) &nbsp;&nbsp;Fr:</label>&nbsp;&nbsp;
					<input type="number" class="bodersueve" id="frecuencia_r_mi" class="control" min="0" value='<?php echo  $frecuencia_r; ?>' name="fr" style="width:50px;">
					<label class="labelx">(X)</label>&nbsp;&nbsp;
					<label class="labeltipodesangre">Imc</label>
					<input type="number" class="bodersueve" id="imc_mi" class="control" value='<?php echo  $imc; ?>' style="width:50px;">
				</div>
				<label class="labeltemperatura">Temperatura:</label>
				<input type="number" class="bodersueve" id="temperatura_mi" class="control" min="0" value='<?php echo  $temperatura; ?>' name="temperatura" style="width:50px;">&nbsp;&nbsp;
				<label class="labelc">(ºC)</label>&nbsp;&nbsp;
				<label class="labelta">T/a:</label>
				<input type="number" class="bodersueve" id="ta_alta_mi" class="control" min="0" value='<?php echo  $tension_alta; ?>' placeholder="t.a" name="ta" style="width:50px;">
				<input type="number" class="bodersueve" id="ta_baja_mi" class="control" min="0" value='<?php echo  $tension_baja; ?>' placeholder="t.b" name="ta" style="width:50px;">
				<label class="labelmmhg">(MMHg)</label>&nbsp;&nbsp;
				<label class="labeltipodesangre">Tipo de Sangre</label>
				<input type="text" class="bodersueve" readonly="readonly" id="tipodesangre" class="control" style="width:50px;" value='<?php echo $tipo_de_sangrec; ?>' />

			</fieldset>
			<br>
			<div>
				<fieldset class="bodersueve_fieldset">
					<span class="labelimpresiond"><b>MOTIVO DE CONSULTA</b>: </span>
					<textarea id="motivo_consulta_mi" class="impresiond" onkeyup="mayus(this);" autocomplete="off" name="observacion_gb"><?php echo ' ' . ' ' . $motivo_consulta; ?></textarea>
					<button type="button" class="btn btn btn-primary btnActualizar_signos" id="btnActualizar_signos_mi">Actualizar</button>
					<select class="classic" id="aciones_mi" name="gradointruccion" data-style="btn-primary" style="width:36%">
						<option value="0">Seleccione</option>
					</select>
				</fieldset>
			</div>
			<div class="enfermedadactual_mi" style="display:none;">
				<fieldset class="bodersueve_fieldset">
					<div class="span">
						<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
							<b>DIAGNOSTICO</b>
						</SPAN>
						<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_mi" id="btnMostrar_enfermedad_actual_mi">Agregar(+)</button>
						<br>
						<br>
						<div class="row">

							<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
								<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
								<table class="display" id="table_enfermedad_actual_mi" cellspacing="2" width="100%">
									<thead>
										<tr>

											<td style="width:60%">Diagnostico</td>
											<td style="width:60%">Medico</td>
											<td>Fecha </td>
											<td>Fecha Actualizacion</td>
											<td>Acciones</td>
										</tr>
									</thead>
									<tbody id="lista_enfermedad_actual_mi">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<!-- Ventana Modal ENFERMEDAD ACTUAL MEDICINA INTERNA -->
			<div class="modal fade" id="modal_enfermedad_mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Enfermedad Actual</label>
								<textarea class="observacion_gb" id="enfermedadactual_mi" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_enfermedadactual_mi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_enfermedadactual_mi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="habitos_mi" style="display:none;">
				<div class="span">
					<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
						<b>HABITOS PSICOSOCLIALES</b>
					</SPAN>

					<br>
					<div class="row">
						<!-- <button type="button" class="btn btn btn-primary btnMostrar3_mi" id="btnMostrar_habitos_mi">Agregar(+)</button>  -->
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="habitos_psicosociales_mi" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Fecha</td>
									</tr>
								</thead>
								<tbody id="lista_habitos">
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>
			<div class="medicamentos_mi" style="display:none;">
				<div class="span">
					<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
						<b>MEDICAMENTOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar_mi" id="btnMostrar_medicamentos_mi">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="table_medicamentos_mi" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Diagnostico</td>
										<td style="width:60%">Medico</td>
										<td>Fecha </td>
										<td>F.Actualizacion</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_medicamentos_mi">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Ventana Modal MEDICAMENTOS MEDICINA INTERNA-->
			<div class="modal fade" id="modal_medicamentos_mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_medicamentos_mi" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_medicamentos_mi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_medicamentos_mi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="antecedentesquirurgicos_mi" style="display:none;">
				<div class="span">
					<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES QUIRURGICOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar3_mi" id="btnMostrar_antecedentes_qx_mi">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="antecedentes_qx_mi" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico </td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_qx_mi">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>



			<!-- Ventana Modal Antecedente Quirurgicos-->
			<div class="modal fade" id="modal_antecedentes_quirurgicos_mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="antecedentes_quirurgicos_mi" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_quirurgicos_mi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_quirurgicos_mi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


			<div class="antecedentespatologicospersonales_mi" style="display:none;">
				<div class="span">
					<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES PERSONALES</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar3_mi" id="btnMostrar_antecedentes_pp_mi">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="antecedentes_pp_mi" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_pp_mi">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>


			<!-- Ventana Modal Antecedentes Patologicos Personales medicina interna -->
			<div class="modal fade" id="antecedentes_personales_mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="antecedentes_patologicosp_mi" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_personales_mi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_personales_mi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


			<div class="examenfisico_mi" style="display:none;">
				<div class="span">
					<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
						<b>EXAMEN FISICO</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar_mi" id="btnMostrar_examen_fisico_mi">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="examen_fisico_mi" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico </td>
										<td>Especialidad </td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_examen_fisico_mi">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Ventana Modal EXAMEN FIFISCO -->
			<div class="modal fade" id="modalexamen_fisico_mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_examen_fisico_mi" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_examen_fisico_mi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_examen_fisico_mi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


			<div class="paraclinicos_mi" style="display:none;">
				<div class="span">
					<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
						<b>PARACLINICOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar_mi" id="btnMostrar_paraclinicos_mi">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="tableparaclinicos_mi" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico </td>
										<td>Especialidad </td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_paraclinicos_mi">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal PARACLINICOS -->
			<div class="modal fade" id="modal_paraclinicos_mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_paraclinicos_mi" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_paraclinicos_mi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_paraclinicos_mi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


			<div class="impresiondiagnostica_mi" style="display:none;">
				<div class="span">
					<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
						<b>IMPRESION DIAGNOSTICA</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar_mi" id="btnMostrar_impresion_diag_mi">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="impresion_diag_mi" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico </td>
										<td>Especialidad </td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_impresion_diag_mi">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- Ventana Modal impresion diagnostica -->
			<div class="modal fade" id="modal_impresion_diag_mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_impresion_diag_mi" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_impresion_diag_mi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_impresion_diag_mi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>


			<div class="plan_mi" style="display:none;">
				<fieldset class="bodersueve_fieldset">
					<div class="span">
						<SPAN class="texto_mi" style="position: absolute; top: 200 px; left: 400 px;">
							<b>PLAN</b>
						</SPAN>
						<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_mi" id="btnMostrar_plan_mi">Agregar(+)</button>
						<br>
						<br>
						<div class="row">

							<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
								<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
								<table class="display" id="table_plan_mi" cellspacing="2" width="100%">
									<thead>
										<tr>

											<td style="width:60%">Descripcion</td>
											<td>Medico </td>
											<td>Especialidad </td>
											<td>Fecha</td>
											<td>Acciones</td>
										</tr>
									</thead>
									<tbody id="lista_plan_mi">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<br>
			<!-- Ventana Modal PLAN -->
			<div class="modal fade" id="modal_plan_mi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>Descripcion</label>
								<textarea class="observacion_gb" id="obs_plan_mi" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_plan_mi">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_plan_mi">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="reposos" style="display:none;">
			<fieldset class="bodersueve_fieldset">
				<div class="span">
					<SPAN class="texto" style="position: absolute; top: 200 px; left: 400 px;">
						<b>REPOSOS</b>
					</SPAN>
					<button type="button" style="position: absolute; top: 200 px; left: 420 px;" class="btn btn btn-primary btnMostrar" id="btnMostrar_reposo">Agregar(+)</button>
					<br>
					<br>
					<div class="row">

						<input type="hidden" class="sinborder" id="nombre_medico" disabled="disabled" value='<?= session('nombreUsuario'); ?> ' />


						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="table_reposos" style="width:100%" style="margin-top: 20px">
								<thead>
									<tr>

										<td class="text-center" style="width: 5%;">N_HISTORIAL</td>
										<td class="text-center" style="width: 60%;">MOTIVO</td>
										<td class="text-center" style="width: 5%;">DESDE</td>
										<td class="text-center" style="width: 5%;">HASTA</td>
										<td class="text-center" style="width: 20%;">MEDICO: </td>
										<td class="text-center" style="width: 2%;">ESTATUS: </td>
										<td class="text-center" style="width: 2%;">ACCIONES: </td>

									</tr>
								</thead>
								<tbody id="lista_reposos">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<!-- Ventana Modal REPOSOS -->
		<div class="modal fade" id="modal_reposos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<h5 class="modal-title text-center">REPOSO MEDICO</h5>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" class="sinborder" id="id_consulta" value='<?php echo $id_consulta; ?>'>
							<input type="hidden" id="n_historialT" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="id_reposo" value='' />

							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
						</div>
						<fieldset>

							<legend class="legend">Datos del Trabajador</legend>
							<div>
								<label for="name">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="nombretitular" id="nombretitular" readonly="readonly" disabled="disabled" name="nombretitular"> &nbsp;
							</div>
							<div>
								<label>Fecha De Nacimiento</label>
								<input type="text" class="fecha_nacimientotitular" readonly="readonly" disabled="disabled" id="fecha_nacimientotitular" name="fecha_nacimientotitular">
								<label>CI</label>
								<input type="text" class="cedulatitular" id="cedulatitular" disabled="disabled" name="cedulatitular" readonly="readonly">
								<label>Edad</label>
								<input type="text" class="edadtitular" style="width:110px;" id="edadtitular" disabled="disabled" name="edadtitular" readonly="readonly">
							</div>

							<div>
								<label for="mail">Unidad Administrativa</label>
								<input type="text" class="unidadtitular" id="unidadtitular" readonly="readonly" disabled="disabled" style="width:51%;" name="unidadtitular">
								<label for="mail">Telefono</label>
								<input type="text" class="telefonotitular" style="width:90px;" readonly="readonly" disabled="disabled" id="telefonotitular" name="telefonotitular">
							</div>

							<div>
								<label for="min">Inicio</label>
								<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
								<label for="hasta">Retorno</label>&nbsp;&nbsp;
								<input type="date" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;&nbsp;
							</div>
							&nbsp;<label>Motivo de Reposo:</label>
							<textarea class="observacion_reposos" id="motivo_reposo" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</fieldset>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_reposos">Guardar</button>
						<button type="button" style="display: none;" class="btn btn-primary" id="btnActualizar_reposos">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>



		<!--****************************************************************PACIENETE PEDIATRICO**************************************************** -->
		<div class="form-group has-feedback" id="cajas4" style="display: none;">
			<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?> ' />
			<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?> ' />
			<br>
			<fieldset class="bodersueve_fieldset">

				<legend class="legend"><b>Datos del Trabajador(A):</b></legend>

				<div>
					<label class="labelCedula">Cedula:</label>
					<input type="text" class="sinborder" id="cedulaT" disabled="disabled" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
					<label class="labelnombreApellido">Nombre y Apellido:</label>
					<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:320px;" value='<?php echo $nombre_titular . ' ' . $apellido_titular; ?>'>
					<label class="labelsexot">Genero:</label>
					<input type="text" class="sinborder" id="genereot" disabled="disabled" style="width:50px;" value='<?php echo $sexot; ?>'>
				</div>
				<div>

					<label class="labeledad">Edad:</label>
					<input type="text" class="sinborder" id="edad_actualt" disabled="disabled" style="width:40px;" value='<?php echo $edad_actualt; ?>'>
					<label class="labelfecha_nacimientot">Fecha de Nacimiento:</label>
					<input type="text" class="sinborder" id="fecha_nacimientot" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nacimientot; ?>'>
					<label class="labeltipo_de_personal">Cargo:</label>
					<input type="text" class="sinborder" id="tipo_de_personal" disabled="disabled" style="width:280px;" value='<?php echo $tipo_de_personal; ?>'>

				</div>
				<div>
					<label class="labeldepartamento">Unidad Administrativa:</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" id="departamento" disabled="disabled" style="width:270px;" value='<?php echo $departamento; ?>'>
					<label class="labeltelefono">Nº Telefono</label>
					<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:115px;" value='<?php echo  $telefonot; ?>'>
				</div>
			</fieldset>
			<br>
			<fieldset class="bodersueve_fieldset">
				<!-- <img src="<=base_url()?>/img/2.png" width="120" alt="Avatar" id="img_beneficiario"> 				 -->
				<label class="titulo_mgeneral">FICHA DE REGISTRO / PACIENTE PEDIATRICO</label>
				<label class="labelfecha">Cita Programada</label>
				<input type="text" class="sinborder" disabled="disabled" id="fecha_del_dia_mi" style="width:80px;" value='<?php echo $fecha_asistencia; ?>'>
				<!-- <input type="text" class="sinborder" id="fecha_asist" style="width:80px;" value='<php echo $fecha_asistencia; ?>' /> -->
				<br>
				<legend class="legend">Datos del Beneficiario </legend>
				<label class="labelCedula">Cedula</label>
				<input type="text" class="sinborder" id="cedulab" disabled="disabled" style="width:80px;" value='<?php echo $cedula; ?>'>&nbsp;&nbsp;
				<label class="labelnombreApellido">Nombre y Apellido </label>
				<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:340px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
				<div>
					<label class="labelhistorial"><u>Nº Historial</u> </label>&nbsp;&nbsp;
					<input type="text" class="sinborder" disabled="disabled" id="numeroHistorial" style="width:100px;" value='<?php echo 'F' . '' . $cedula; ?>'>&nbsp;&nbsp;
					<label class="labelfechaNacimiento">Fecha De Nacimiento</label>
					<input type="text" class="sinborder" id="fecha_nacimiento" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nac_cortesia; ?> ' />&nbsp;&nbsp;
					<label class="labelEdad">Edad</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" id="edad" disabled="disabled" style="width:100px;" value='<?php echo $edad_actual; ?>'>
					<label class="labelsexo">Sexo</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" disabled="disabled" id="sexo" style="width:50px;" value='<?php echo  $sexo; ?>'>

					<label class="labeltelefono">Nº Telefono</label>
					<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:115px;" value='<?php echo  $telefono; ?>'>
					<label>T.Beneficiario:</label>
					<input type="text" class="bodersueve" disabled="disabled" id="Tbenficiario" disabled="disabled" style="width:80px;" value='CORTESIA'>
				</div>
			</fieldset>
			<br>

			<fieldset class="bodersueve_fieldset">
				<legend class="legend"><b>Signos Vitales </b> </legend>
				<div>

					<label class="labelfechaconlta">Fecha Consulta</label>
					<input type="text" class="sinborder" id="fecha_consulta" disabled="disabled" style="width:120px;" value='<?php echo $fecha_creacion; ?>' />
					<!-- <label class="labelfechaconlta">Captados por:</label>
				<input type="text" class="sinborder" id="user_nombre" disabled="disabled"  value='<php echo $user_nombre;?> '/> -->
					<button type="button" class="btn btn btn-primary btnActualizar_signos" id="btnActualizar_signos_pp">Actualizar</button>
				</div>
				<div>
					<label class="labelpeso">Peso:</label>
					<input type="number" class="bodersueve" id="peso_pp" class="control" min="0" value='<?php echo  $peso; ?>' name="peso" style="width:50px;">
					<label class="labelkg">(Kg)</label>&nbsp;&nbsp;
					<label class="labeltalla">Talla:</label>
					<input type="number" class="bodersueve" id="talla_pp" class="control" min="0" value='<?php echo  $talla; ?>' name="talla" style="width:50px;">
					<label class="labelcm">(Cm)</label>&nbsp;&nbsp;
					<label class="labelspo2">Spo2:</label>
					<input type="number" class="bodersueve" id="spo2_pp" class="control" min="0" value='<?php echo  $spo2; ?>' name="spo2" style="width:50px;">&nbsp;&nbsp;
					<label class="labelfc">Fc:</label>
					<input type="number" class="bodersueve" id="frecuencia_c_pp" class="control" min="0" value='<?php echo  $frecuencia_c; ?>' name="fc" style="width:50px;">
					<label class="labelfr">(X) &nbsp;&nbsp;Fr:</label>&nbsp;&nbsp;
					<input type="number" class="bodersueve" id="frecuencia_r_pp" class="control" min="0" value='<?php echo  $frecuencia_r; ?>' name="fr" style="width:50px;">
					<label class="labelx">(X)</label>&nbsp;&nbsp;
					<label class="labeltipodesangre">Imc</label>
					<input type="number" class="bodersueve" id="imc_pp" class="control" value='<?php echo  $imc; ?>' style="width:50px;">
				</div>
				<label class="labeltemperatura">Temperatura:</label>
				<input type="number" class="bodersueve" id="temperatura_pp" class="control" min="0" value='<?php echo  $temperatura; ?>' name="temperatura" style="width:50px;">&nbsp;&nbsp;
				<label class="labelc">(ºC)</label>&nbsp;&nbsp;
				<label class="labelta">T/a:</label>
				<input type="number" class="bodersueve" id="ta_alta_pp" class="control" min="0" value='<?php echo  $tension_alta; ?>' placeholder="t.a" name="ta" style="width:50px;">
				<input type="number" class="bodersueve" id="ta_baja_pp" class="control" min="0" value='<?php echo  $tension_baja; ?>' placeholder="t.b" name="ta" style="width:50px;">
				<label class="labelmmhg">(MMHg)</label>&nbsp;&nbsp;
				<label class="labeltipodesangre">Tipo de Sangre</label>
				<input type="text" class="bodersueve" readonly="readonly" id="tipodesangre" class="control" style="width:50px;" value='<?php echo $tipo_de_sangrec; ?>' />


			</fieldset>
			<br>
			<div>
				<fieldset class="bodersueve_fieldset">
					<span class="labelimpresiond"><b>MOTIVO DE CONSULTA</b>: </span>
					<textarea id="motivo_consulta_pp" class="impresiond" onkeyup="mayus(this);" autocomplete="off" name="observacion_gb"><?php echo ' ' . ' ' . $motivo_consulta; ?></textarea>
					<button type="button" class="btn btn btn-primary btnActualizar_signos" id="btnActualizar_signos_pp">Actualizar</button>
					<select class="classic" id="aciones_pp" name="gradointruccion" data-style="btn-primary" style="width:36%">
						<option value="0">Seleccione</option>
					</select>
				</fieldset>
			</div>
			<br>

			<div class="enfermedadactual_pp" style="display:none;">
				<fieldset class="bodersueve_fieldset">
					<div class="span">
						<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
							<b>DIAGNOSTICO</b>
						</SPAN>
						<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_pp" id="btnMostrar_enfermedad_actual_pp">Agregar(+)</button>
						<br>
						<br>
						<div class="row">

							<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
								<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
								<table class="display" id="table_enfermedad_actual_pp" cellspacing="2" width="100%">
									<thead>
										<tr>

											<td style="width:60%">Diagnostico</td>
											<td style="width:60%">Medico</td>

											<td>Fecha</td>
											<td style="width:1%">Acciones</td>
										</tr>
									</thead>
									<tbody id="lista_enfermedad_actual_pp">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<!-- Ventana Modal ENFERMEDAD ACTUAL MEDICINA INTERNA -->
			<div class="modal fade" id="modal_enfermedad_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>DIANOSTICO</label>
								<textarea class="observacion_gb" id="enfermedadactual_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_enfermedadactual_pp">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_enfermedadactual_pp">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="medicamentos_pp" style="display:none;">
				<div class="span">
					<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
						<b>MEDICAMENTOS</b>
					</SPAN>

					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar_pp" id="btnMostrar_medicamentos_pp">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">

							<table class="display" id="table_medicamentos_pp" cellspacing="2" width="100%">
								<thead>
									<tr>
										<td style="width:60%">Diagnostico</td>
										<td style="width:60%">Medico</td>
										<td>Fecha </td>
										<td>F.Actualizacion</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_medicamentos_pp">
								</tbody>
							</table>
						</div>
					</div>

				</div>
			</div>

			<!-- Ventana Modal MEDICAMENTOS PACIENTE PEDIATRICO -->
			<div class="modal fade" id="modal_medicamentos_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>MEDICAMENTOS</label>
								<textarea class="observacion_gb" id="obs_medicamentos_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_medicamentos_pp">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_medicamentos_pp">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>

			<div class="antecedentespatologicosfamiliares_pp" style="display:none;">
				<div class="span">
					<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES PATOLOGICOS FAMILIARES</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar2_pp" id="btnMostrar_antecedentes_pf_pp">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->

							<table class="display" id="antecedentes_pf_pp" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico</td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_pf_pp">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- Ventana Modal Antecedentes Patologicos Familiares -->
			<div class="modal fade" id="antecedentes_familiares_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-body">
							<div id="datos_ajax_register"></div>
							<div class="span">
								<input type="hidden" id="id" value='' />
								<input type="hidden" id="id_psicologiap" value='' />
								<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
								<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
								<label>ANTECEDENTES PATOLOGICOS FAMILIARES</label>
								<textarea class="observacion_gb" id="antecedentes_patologicosf_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_familiares_pp">Guardar datos</button>
							<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_familiares_pp">Actualizar</button>
							<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						</div>
					</div>
				</div>
			</div>




			<div class="antecedentesquirurgicos_pp" style="display:none;">
				<div class="span">
					<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
						<b>ANTECEDENTES QUIRURGICOS</b>
					</SPAN>
					<br>
					<div class="row">
						<button type="button" class="btn btn btn-primary btnMostrar_pp" id="btnMostrar_antecedentes_qx_pp">Agregar(+)</button>
						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="antecedentes_qx_pp" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico </td>
										<td>Especialidad</td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_antecedentes_qx_pp">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- Ventana Modal Antecedente Quirurgicos-->
		<div class="modal fade" id="modal_antecedentes_quirurgicos_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>ANTECEDENTES QUIRURGICOS</label>
							<textarea class="observacion_gb" id="antecedentes_quirurgicos_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_quirurgicos_pp">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_quirurgicos_pp">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="antecedentespatologicospersonales_pp" style="display:none;">
			<div class="span">
				<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
					<b>ANTECEDENTES PERSONALES</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_pp" id="btnMostrar_antecedentes_pp_pp">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<table class="display" id="antecedentes_pp_pp" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico</td>
									<td>Especialidad</td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_antecedentes_pp_pp">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>


		<!-- Ventana Modal Antecedentes Patologicos Personales medicina interna -->
		<div class="modal fade" id="antecedentes_personales_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>ANTECEDENTES PERSONALES</label>
							<textarea class="observacion_gb" id="antecedentes_patologicosp_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_personales_pp">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_personales_pp">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="examenfisico_pp" style="display:none;">
			<div class="span">
				<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
					<b>EXAMEN FISICO</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_pp" id="btnMostrar_examen_fisico_pp">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="table_examen_fisico_pp" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico</td>
									<td>Especialidad</td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_examen_fisico_pp">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<!-- Ventana Modal EXAMEN FIFISCO -->
		<div class="modal fade" id="modalexamen_fisico_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>EXAMEN FISICO</label>
							<textarea class="observacion_gb" id="obs_examen_fisico_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_examen_fisico_pp">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_examen_fisico_pp">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="paraclinicos_pp" style="display:none;">
			<div class="span">
				<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
					<b>PARACLINICOS</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_pp" id="btnMostrar_paraclinicos_pp">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="tableparaclinicos_pp" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico </td>
									<td>Especialidad </td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_paraclinicos_pp">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Ventana Modal PARACLINICOS -->
		<div class="modal fade" id="modal_paraclinicos_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>PARACLINICOS</label>
							<textarea class="observacion_gb" id="obs_paraclinicos_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_paraclinicos_pp">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_paraclinicos_pp">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="impresiondiagnostica_pp" style="display:none;">
			<div class="span">
				<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
					<b>IMPRESION DIAGNOSTICA</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_pp" id="btnMostrar_impresion_diag_pp">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="impresion_diag_pp" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico </td>
									<td>Especialidad </td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_impresion_diag_pp">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Ventana Modal impresion diagnostica -->
		<div class="modal fade" id="modal_impresion_diag_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>IMPRESION DIAGNOSTICA</label>
							<textarea class="observacion_gb" id="obs_impresion_diag_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_impresion_diag_pp">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_impresion_diag_pp">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="plan_pp" style="display:none;">
			<fieldset class="bodersueve_fieldset">
				<div class="span">
					<SPAN class="texto_pp" style="position: absolute; top: 200 px; left: 400 px;">
						<b>PLAN</b>
					</SPAN>
					<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_pp" id="btnMostrar_plan_pp">Agregar(+)</button>
					<br>
					<br>
					<div class="row">

						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="table_plan_pp" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico </td>
										<td>Especialidad </td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_plan_pp">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<br>
		<!-- Ventana Modal PLAN -->
		<div class="modal fade" id="modal_plan_pp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>PLAN</label>
							<textarea class="observacion_gb" id="obs_plan_pp" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_plan_pp">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_plan_pp">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>




	</div>

</div>

<!-- Ventana Modal REPOSOS -->
<div class="modal fade" id="modal_reposos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<h5 class="modal-title text-center">REPOSO MEDICO</h5>
			<div class="modal-body">
				<div id="datos_ajax_register"></div>
				<div class="span">
					<input type="hidden" id="id" value='' />
					<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
					<input type="hidden" class="sinborder" id="id_consulta" value='<?php echo $id_consulta; ?>'>
					<input type="hidden" id="n_historialT" value='' />
					<input type="hidden" id="id_psicologiap" value='' />
					<input type="hidden" id="id_reposo" value='' />

					<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
				</div>
				<fieldset>

					<legend class="legend">Datos del Trabajador</legend>
					<div>
						<label for="name">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="text" class="nombretitular" id="nombretitular" readonly="readonly" disabled="disabled" name="nombretitular"> &nbsp;
					</div>
					<div>
						<label>Fecha De Nacimiento</label>
						<input type="text" class="fecha_nacimientotitular" readonly="readonly" disabled="disabled" id="fecha_nacimientotitular" name="fecha_nacimientotitular">
						<label>CI</label>
						<input type="text" class="cedulatitular" id="cedulatitular" disabled="disabled" name="cedulatitular" readonly="readonly">
						<label>Edad</label>
						<input type="text" class="edadtitular" style="width:110px;" id="edadtitular" disabled="disabled" name="edadtitular" readonly="readonly">
					</div>

					<div>
						<label for="mail">Unidad Administrativa</label>
						<input type="text" class="unidadtitular" id="unidadtitular" readonly="readonly" disabled="disabled" style="width:51%;" name="unidadtitular">
						<label for="mail">Telefono</label>
						<input type="text" class="telefonotitular" style="width:90px;" readonly="readonly" disabled="disabled" id="telefonotitular" name="telefonotitular">
					</div>

					<div>
						<label for="min">Inicio</label>
						<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="desde">&nbsp;&nbsp;
						<label for="hasta">Retorno</label>&nbsp;&nbsp;
						<input type="date" value="<?php echo date('y-m-d'); ?>" name="hasta" id="hasta">&nbsp;&nbsp;
					</div>
					&nbsp;<label>Motivo de Reposo:</label>
					<textarea class="observacion_reposos" id="motivo_reposo" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
				</fieldset>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="btnagregar_reposos">Guardar</button>
				<button type="button" style="display: none;" class="btn btn-primary" id="btnActualizar_reposos">Actualizar</button>
				<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
			</div>
		</div>
	</div>
</div>





<div class="row ">
	<div class="col-md-2 col-sm-2 col-lg-2 col-xl-2">
	</div>
	<div class="col-md-8 col-sm-8 col-lg-8 col-xl-8" id="prueba2">

		<!--****************************************************************GINECOLOGIA Y OBSTETRICIA**************************************************** -->
		<div class="form-group has-feedback" id="cajas5" style="display: none;">
			<input type="hidden" id="apellido" disabled="disabled" value='<?php echo $apellido; ?> ' />
			<input type="hidden" id="nombre" disabled="disabled" value='<?php echo $nombre; ?> ' />
			<br>
			<fieldset class="bodersueve_fieldset">

				<legend class="legend"><b>Datos del Trabajador(A):</b></legend>

				<div>
					<label class="labelCedula">Cedula:</label>
					<input type="text" class="sinborder" id="cedulaT" disabled="disabled" style="width:100px;" value='<?php echo $cedula_trabajador; ?>'>
					<label class="labelnombreApellido">Nombre y Apellido:</label>
					<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:320px;" value='<?php echo $nombre_titular . ' ' . $apellido_titular; ?>'>
					<label class="labelsexot">Genero:</label>
					<input type="text" class="sinborder" id="genereot" disabled="disabled" style="width:50px;" value='<?php echo $sexot; ?>'>
				</div>
				<div>

					<label class="labeledad">Edad:</label>
					<input type="text" class="sinborder" id="edad_actualt" disabled="disabled" style="width:40px;" value='<?php echo $edad_actualt; ?>'>
					<label class="labelfecha_nacimientot">Fecha de Nacimiento:</label>
					<input type="text" class="sinborder" id="fecha_nacimientot" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nacimientot; ?>'>
					<label class="labeltipo_de_personal">Cargo:</label>
					<input type="text" class="sinborder" id="tipo_de_personal" disabled="disabled" style="width:280px;" value='<?php echo $tipo_de_personal; ?>'>

				</div>
				<div>
					<label class="labeldepartamento">Unidad Administrativa:</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" id="departamento" disabled="disabled" style="width:270px;" value='<?php echo $departamento; ?>'>
					<label class="labeltelefono">Nº Telefono</label>
					<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:115px;" value='<?php echo  $telefonot; ?>'>
				</div>
			</fieldset>
			<br>
			<fieldset class="bodersueve_fieldset">
				<!-- <img src="<=base_url()?>/img/2.png" width="120" alt="Avatar" id="img_beneficiario"> 				 -->
				<label class="titulo_mgeneral_pp">FICHA DE REGISTRO / GINECOLOGIA Y OBSTETRICIA</label>
				<label class="labelfecha">Cita Programada</label>
				<input type="text" class="sinborder" disabled="disabled" id="fecha_del_dia_mi" style="width:120px;" value='<?php echo $fecha_asistencia; ?>'>
				<input type="hidden" class="sinborder" id="fecha_asist" style="width:80px;" value='<?php echo $fecha_asistencia; ?>' />

				<legend class="legend">Datos del Beneficiario </legend>
				<label class="labelCedula">Cedula</label>
				<input type="text" class="sinborder" id="cedulab" disabled="disabled" style="width:80px;" value='<?php echo $cedula; ?>'>&nbsp;&nbsp;
				<label class="labelnombreApellido">Nombre y Apellido </label>
				<input type="text" class="sinborder" id="nombrepellidoT" disabled="disabled" style="width:340px;" value='<?php echo $nombre . ' ' . $apellido; ?>'>
				<div>
					<label class="labelhistorial"><u>Nº Historial</u> </label>&nbsp;&nbsp;
					<input type="text" class="sinborder" disabled="disabled" id="numeroHistorial" style="width:100px;" value='<?php echo 'F' . '' . $cedula; ?>'>&nbsp;&nbsp;
					<label class="labelfechaNacimiento">Fecha De Nacimiento</label>
					<input type="text" class="sinborder" id="fecha_nacimiento" disabled="disabled" style="width:100px;" value='<?php echo $fecha_nac_cortesia; ?> ' />&nbsp;&nbsp;
					<label class="labelEdad">Edad</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" id="edad" disabled="disabled" style="width:100px;" value='<?php echo $edad_actual; ?>'>
					<label class="labelsexo">Sexo</label>&nbsp;&nbsp;
					<input type="text" class="sinborder" disabled="disabled" id="sexo" style="width:50px;" value='<?php echo  $sexo; ?>'>

					<label class="labeltelefono">Nº Telefono</label>
					<input type="text" class="sinborder" disabled="disabled" id="telefono" class="control" style="width:115px;" value='<?php echo  $telefono; ?>'>
					<label>T.Beneficiario:</label>
					<input type="text" class="bodersueve" disabled="disabled" id="Tbenficiario" disabled="disabled" style="width:80px;" value='CORTESIA'>
				</div>
			</fieldset>
			<br>

			<fieldset class="bodersueve_fieldset">
				<legend class="legend"><b>Signos Vitales </b> </legend>
				<div>

					<label class="labelfechaconlta">Fecha Consulta</label>
					<input type="text" class="sinborder" id="fecha_consulta" disabled="disabled" style="width:120px;" value='<?php echo $fecha_creacion; ?>' />

				</div>
				<div>
					<label class="labelpeso">Peso:</label>
					<input type="number" class="bodersueve" id="peso_gb" class="control" min="0" value='<?php echo  $peso; ?>' name="peso" style="width:50px;">
					<label class="labelkg">(Kg)</label>&nbsp;&nbsp;
					<label class="labeltalla">Talla:</label>
					<input type="number" class="bodersueve" id="talla_gb" class="control" min="0" value='<?php echo  $talla; ?>' name="talla" style="width:50px;">
					<label class="labelcm">(Cm)</label>&nbsp;&nbsp;
					<label class="labelspo2">Spo2:</label>
					<input type="number" class="bodersueve" id="spo2_gb" class="control" min="0" value='<?php echo  $spo2; ?>' name="spo2" style="width:50px;">&nbsp;&nbsp;
					<label class="labelfc">Fc:</label>
					<input type="number" class="bodersueve" id="frecuencia_c_gb" class="control" min="0" value='<?php echo  $frecuencia_c; ?>' name="fc" style="width:50px;">
					<label class="labelfr">(X) &nbsp;&nbsp;Fr:</label>&nbsp;&nbsp;
					<input type="number" class="bodersueve" id="frecuencia_r_gb" class="control" min="0" value='<?php echo  $frecuencia_r; ?>' name="fr" style="width:50px;">
					<label class="labelx">(X)</label>&nbsp;&nbsp;
					<label class="labeltipodesangre">Imc</label>
					<input type="number" class="bodersueve" id="imc_gb" class="control" value='<?php echo  $imc; ?>' style="width:50px;">
				</div>
				<label class="labeltemperatura">Temperatura:</label>
				<input type="number" class="bodersueve" id="temperatura_gb" class="control" min="0" value='<?php echo  $temperatura; ?>' name="temperatura" style="width:50px;">&nbsp;&nbsp;
				<label class="labelc">(ºC)</label>&nbsp;&nbsp;
				<label class="labelta">T/a:</label>
				<input type="number" class="bodersueve" id="ta_alta_gb" class="control" min="0" value='<?php echo  $tension_alta; ?>' placeholder="t.a" name="ta" style="width:50px;">
				<input type="number" class="bodersueve" id="ta_baja_gb" class="control" min="0" value='<?php echo  $tension_baja; ?>' placeholder="t.b" name="ta" style="width:50px;">
				<label class="labelmmhg">(MMHg)</label>&nbsp;&nbsp;
				<label class="labeltipodesangre">Tipo de Sangre</label>
				<input type="text" class="bodersueve" readonly="readonly" id="tipodesangre" class="control" style="width:50px;" value='<?php echo $tipo_de_sangrec; ?>' />


			</fieldset>

		</div>

		<div>
			<fieldset class="bodersueve_fieldset">
				<span class="labelimpresiond"><b>MOTIVO DE CONSULTA</b>: </span>
				<textarea id="motivo_consulta_GB" class="impresiond" onkeyup="mayus(this);" autocomplete="off" name="observacion_gb"><?php echo ' ' . ' ' . $motivo_consulta; ?></textarea>&nbsp;&nbsp;
				<button type="button" class="btn btn btn-primary " id="btnAgregar_signos_gb">Actualizar </button>&nbsp;&nbsp;
				<select class="classic" id="aciones_gb" name="gradointruccion" data-style="btn-primary" style="width:36%">
					<option value="0">Seleccione</option>
				</select>

			</fieldset>
		</div>
		<br>

		<div class="enfermedadactual_gb" style="display:none;">
			<fieldset class="bodersueve_fieldset">
				<div class="span">
					<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
						<b>DIAGNOSTICO</b>
					</SPAN>
					<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_enfermedad_actual_gb">Agregar(+)</button>
					<br>
					<br>
					<div class="row">

						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="table_enfermedad_actual_gb" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Diagnostico</td>
										<td style="width:60%">Medico</td>

										<td>Fecha</td>
										<td style="width:1%">Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_enfermedad_actual_gb">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<!-- Ventana Modal ENFERMEDAD ACTUAL MEDICINA INTERNA -->
		<div class="modal fade" id="modal_enfermedad_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>DIAGNOSTICO</label>
							<textarea class="observacion_gb" id="enfermedadactual_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_enfermedadactual_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_enfermedadactual_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<div class="medicamentos_gb" style="display:none;">
			<div class="span">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>MEDICAMENTOS</b>
				</SPAN>

				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_medicamentos_gb">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">

						<table class="display" id="table_medicamentos_gb" cellspacing="2" width="100%">
							<thead>
								<tr>
									<td style="width:60%">Diagnostico</td>
									<td style="width:60%">Medico</td>
									<td>Fecha </td>
									<td>F.Actualizacion</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_medicamentos_gb">
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>

		<!-- Ventana Modal MEDICAMENTOS PACIENTE PEDIATRICO -->
		<div class="modal fade" id="modal_medicamentos_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>MEDICAMENTOS</label>
							<textarea class="observacion_gb" id="obs_medicamentos_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_medicamentos_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_medicamentos_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>

		<div class="antecedentespatologicosfamiliares_gb" style="display:none;">
			<div class="span">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>ANTECEDENTES PATOLOGICOS FAMILIARES</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar3_gb" id="btnMostrar_antecedentes_pf_gb">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->

						<table class="display" id="antecedentes_pf_gb" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:40%">Descripcion</td>
									<td>Medico</td>
									<td>Especialidad </td>
									<td>Fecha</td>
									<td style="width:1%">Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_antecedentes_pf_gb">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<!-- Ventana Modal Antecedentes Patologicos Familiares -->
		<div class="modal fade" id="antecedentes_familiares_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>ANTECEDENTES PATOLOGICOS FAMILIARES</label>
							<textarea class="observacion_gb" id="antecedentes_patologicosf_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_familiares_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_familiares_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>




		<div class="antecedentesquirurgicos_gb" style="display:none;">
			<div class="span">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>ANTECEDENTES QUIRURGICOS</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_antecedentes_qx_gb">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="antecedentes_qx_gb" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico </td>
									<td>Especialidad</td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_antecedentes_qx_gb">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>



		<!-- Ventana Modal Antecedente Quirurgicos-->
		<div class="modal fade" id="modal_antecedentes_quirurgicos_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>ANTECEDENTES QUIRURGICOS</label>
							<textarea class="observacion_gb" id="antecedentes_quirurgicos_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_quirurgicos_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_quirurgicos_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="antecedentespatologicospersonales_gb" style="display:none;">
			<div class="span">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>ANTECEDENTES PERSONALES</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_antecedentes_gb">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<table class="display" id="antecedentes_pp_gb" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico</td>
									<td>Especialidad</td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_antecedentes_gb_gb">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>


		<!-- Ventana Modal Antecedentes Patologicos Personales medicina interna -->
		<div class="modal fade" id="antecedentes_personales_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>ANTECEDENTES PERSONALES</label>
							<textarea class="observacion_gb" id="antecedentes_patologicosp_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_antecedentes_personales_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_antecedentes_personales_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="examenfisico_gb" style="display:none;">
			<div class="span">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>EXAMEN FISICO</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_examen_fisico_gb">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="table_examen_fisico_gb" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico</td>
									<td>Especialidad</td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_examen_fisico_gb">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<!-- Ventana Modal EXAMEN FIFISCO -->
		<div class="modal fade" id="modalexamen_fisico_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>EXAMEN FISICO</label>
							<textarea class="observacion_gb" id="obs_examen_fisico_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_examen_fisico_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_examen_fisico_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="paraclinicos_gb" style="display:none;">
			<div class="span">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>PARACLINICOS</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_paraclinicos_gb">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="tableparaclinicos_gb" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico </td>
									<td>Especialidad </td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_paraclinicos_gb">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Ventana Modal PARACLINICOS -->
		<div class="modal fade" id="modal_paraclinicos_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>PARACLINICOS</label>
							<textarea class="observacion_gb" id="obs_paraclinicos_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_paraclinicos_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_paraclinicos_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<div class="impresiondiagnostica_gb" style="display:none;">
			<div class="span">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>IMPRESION DIAGNOSTICA</b>
				</SPAN>
				<br>
				<div class="row">
					<button type="button" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_impresion_diag_gb">Agregar(+)</button>
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="impresion_diag_gb" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Medico </td>
									<td>Especialidad </td>
									<td>Fecha</td>
									<td>Acciones</td>
								</tr>
							</thead>
							<tbody id="lista_impresion_diag_gb">
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Ventana Modal impresion diagnostica -->
		<div class="modal fade" id="modal_impresion_diag_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>IMPRESION DIAGNOSTICA</label>
							<textarea class="observacion_gb" id="obs_impresion_diag_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_impresion_diag_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_impresion_diag_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
		<div class="habitos_gb" style="display:none;">
			<div class="span">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>HABITOS PSICOSOCLIALES</b>
				</SPAN>

				<br>
				<div class="row">
					<!-- <button type="button" class="btn btn btn-primary btnMostrar3_gb" id="btnMostrar_habitos_gb">Agregar(+)</button>  -->
					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
						<table class="display" id="habitos_psicosociales_gb" cellspacing="2" width="100%">
							<thead>
								<tr>

									<td style="width:60%">Descripcion</td>
									<td>Fecha</td>
								</tr>
							</thead>
							<tbody id="lista_habitos">
							</tbody>
						</table>
					</div>
				</div>

			</div>
		</div>

		<div class="plan_gb" style="display:none;">
			<fieldset class="bodersueve_fieldset">
				<div class="span">
					<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
						<b>PLAN</b>
					</SPAN>
					<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_plan_gb">Agregar(+)</button>
					<br>
					<br>
					<div class="row">

						<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
							<!--table class="table display" id="table_roles" style="margin-top: 20px"-->
							<table class="display" id="table_plan_gb" cellspacing="2" width="100%">
								<thead>
									<tr>

										<td style="width:60%">Descripcion</td>
										<td>Medico </td>
										<td>Especialidad </td>
										<td>Fecha</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody id="lista_plan_gb">
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</fieldset>
		</div>
		<!-- Ventana Modal PLAN -->
		<div class="modal fade" id="modal_plan_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
							<label>PLAN</label>
							<textarea class="observacion_gb" id="obs_plan_gb" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_plan_gb">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_plan_gb">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar_gb" name="btnCerrar_gb">Cerrar</button>
					</div>
				</div>
			</div>
		</div>


		<!-- REPOSOS-->
		<div class="reposos_gb" style="display:none;">
			<fieldset class="bodersueve_fieldset">
				<SPAN class="texto_gb" style="position: absolute; top: 200 px; left: 400 px;">
					<b>REPOSOS</b>
				</SPAN>

				<button type="button" style="position: absolute; top: 200 px; left: 400 px;" class="btn btn btn-primary btnMostrar_gb" id="btnMostrar_reposo_gb">Agregar(+)</button>
				<br>
				<br>
				<div class="row">
					<input type="hidden" class="sinborder" id="nombre_medico" disabled="disabled" value='<?= session('nombreUsuario'); ?> ' />


					<div class="col-md-12 col-sm-12 col-lg-12 col-xl-12">
						<table class="display" id="table_reposos_gb" style="width:100%" style="margin-top: 20px">
							<thead>
								<tr>

									<td class="text-center" style="width: 5%;">id</td>
									<td class="text-center" style="width: 5%;">N_HISTORIAL</td>
									<td class="text-center" style="width: 60%;">MOTIVO</td>
									<td class="text-center" style="width: 5%;">DESDE</td>
									<td class="text-center" style="width: 5%;">HASTA</td>
									<td class="text-center" style="width: 20%;">MEDICO: </td>
									<td class="text-center" style="width: 2%;">ESTATUS: </td>
									<td class="text-center" style="width: 2%;">ACCIONES: </td>
								</tr>
							</thead>
							<tbody id="lista_reposos_gb">
							</tbody>
						</table>
					</div>
				</div>
			</fieldset>
		</div>


		<!-- Ventana Modal REPOSOS -->
		<div class="modal fade" id="modal_reposos_gb" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop="static">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<h5 class="modal-title text-center">REPOSO MEDICO</h5>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div class="span">
							<input type="hidden" id="id" value='' />
							<input type="hidden" id="medico_id" value='<?= session('medico_id'); ?> ' />
							<input type="hidden" class="sinborder" id="id_consulta" value='<?php echo $id_consulta; ?>'>
							<input type="hidden" id="r_n_historialT" value='' />
							<input type="hidden" id="id_psicologiap" value='' />
							<input type="hidden" id="id_reposo" value='' />

							<input type="hidden" id="tipobeneficiario" disabled="disabled" class="control" style="width:100px;" value='<?php echo $tipo_beneficiario; ?> '>
						</div>
						<fieldset>

							<legend class="legend">Datos del Trabajador</legend>
							<div>
								<label for="name">Nombre y Apellido</label>&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="nombretitular" id="r_nombretitular" readonly="readonly" disabled="disabled" style="width:530px;" name="nombretitular"> &nbsp;
							</div>
							<div>
								<label>Fecha De Nacimiento</label>
								<input type="text" class="fecha_nacimientotitular" readonly="readonly" disabled="disabled" id="r_fecha_nacimientotitular" name="fecha_nacimientotitular">
								<label>CI</label>
								<input type="text" class="cedulatitular" id="r_cedulatitular" disabled="disabled" name="cedulatitular" readonly="readonly">
								<label>Edad</label>
								<input type="text" class="edadtitular" style="width:110px;" id="r_edadtitular" disabled="disabled" name="edadtitular" readonly="readonly">
							</div>

							<div>
								<label for="mail">Unidad Administrativa</label>
								<input type="text" class="unidadtitular" id="r_unidadtitular" readonly="readonly" disabled="disabled" style="width:51%;" name="unidadtitular">
								<label for="mail">Telefono</label>
								<input type="text" class="telefonotitular" style="width:90px;" readonly="readonly" disabled="disabled" id="r_telefonotitular" name="telefonotitular">
							</div>

							<div>
								<label for="mail">Tipo de Reposo</label>
								<select class="custom-select vm" style="width:100px;" id="r_tipo_reposo" autocomplete="off" data-placeholder="Seleccione" autocomplete="off" required>
									<option value="0" disabled="disabled">seleccione</option>
									<option value="1">Dias</option>
									<option value=" 2">Horas</option>
								</select>
								&nbsp;&nbsp;
								<label for=" min">Inicio</label>
								<input type="date" value="<?php echo date('y-m-d'); ?>" name="desde" id="r_desde">&nbsp;&nbsp;

								<label id="label_retorno">Retorno</label>&nbsp;&nbsp;
								<input type="date" disabled="disabled" value="<?php echo date('y-m-d'); ?>" name="hasta" id="r_hasta">&nbsp;&nbsp;

								<label id="r_labe_dias" disabled="disabled">Dias</label>&nbsp;&nbsp;
								<input type="text" class="dias" disabled="disabled" style=" width:40px;" name="r_dias" value="0" min="0" id="r_dias">&nbsp;&nbsp;
								<input type=" hidden" style="display: none;" disabled="disabled" style=" width:40px;" name="dias" min="0" id="r_dia_en_horas">&nbsp;&nbsp;



								<label id="r_labe_horas" style="display: none;">Horas</label>&nbsp;&nbsp;
								<input type="text" class="horas" style="display: none;" style="width:20px;" name="r_horas" value="0" min="0" id="r_horas">&nbsp;&nbsp;



							</div>
							&nbsp;<label>Motivo de Reposo:</label>
							<textarea class="observacion_reposos" id="r_motivo_reposo" onkeyup="mayus(this);" autocomplete="off" name="evolucion"> </textarea>
						</fieldset>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="btnagregar_reposos_gb">Guardar</button>
						<button type="button" class="btn btn-primary" id="btnActualizar_reposos_gb">Actualizar</button>
						<button type="button" class="btn btn-default" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>
				</div>
			</div>
		</div>






		<!-- //**************************************** VENTANA MODAL DE ANTECEDENTES GINECO-OBSTETRICOS************************* -->
		<div class="modal" id="modal_prueba" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-body">
						<fieldset class="bodersueve_fieldset">
							<input type="hidden" id="bandera_de_llenado" value='<?php echo  $bandera_de_llenado; ?>' />
							<input type="hidden" id="id_gestacion" value='<?php echo  $gestacion; ?>' />
							<input type="hidden" id="id_partos" value='<?php echo  $partos; ?>' />
							<input type="hidden" id="id_cesarias" value='<?php echo  $cesarias; ?>' />
							<input type="hidden" id="id_abortos" value='<?php echo  $abortos; ?>' />
							<input type="hidden" id="tipo_enfermedad_ts" value='<?php echo  $tipo_ets; ?>' />
							<legend class="legend">Historia Obstetrica </legend>
							<div>
								<label class="labelgestacion">Gestacion:</label>&nbsp;&nbsp;
								<select class="classic tamaño" id="gestacion" name="gestacion" data-style="btn-primary" style="width:100px;">

									<option value=0 selected disabled>Seleccione</option>
								</select>&nbsp;&nbsp;
								<label class="labelpartos">Partos:</label>&nbsp;&nbsp;
								<select class="classic tamaño" id="partos" name="partos" data-style="btn-primary" style="width:100px;">
									<option value=0 selected disabled>Seleccione</option>
								</select>&nbsp;&nbsp;
								<label class="labelcesarias">Cesarias:</label>&nbsp;&nbsp;
								<select class="classic tamaño" id="cesarias" name="cesarias" data-style="btn-primary" style="width:100px;">
									<option value=0 selected disabled>Seleccione</option>
								</select>&nbsp;&nbsp;
								<label class="labelabortos">Abortos:</label>&nbsp;&nbsp;
								<select class="classic tamaño" id="abortos" name="abortos" data-style="btn-primary" style="width:100px;">
									<option value=0 selected disabled>Seleccione</option>
								</select>
								<label class="laeblesdadg">Edad G.</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="text" class="bodersueve" id="eg" class="control" value='<?php echo  $edad_gestacional; ?>' style="width:120px;" />
								<label class="labelfecha_regla">F.u.r:</label>&nbsp;
								<input type="DATE" class="bodersueve" id="f_u_r" class="control" value='<?php echo  $f_u_r; ?>' style="width:120px;">
								<div>
									<label class="label_detalle_historia">Detalles</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<textarea class="bodersueve detalle_historia" id="detalle_historia" style="width:640px;"><?php echo $detalle_historia_obst; ?> </textarea>


								</div>

							</div>
						</fieldset>
						<fieldset class="bodersueve_fieldset">
							<legend class="legend">Antecedentes Gineco-Obstetrico</legend>
							<div>
								<label class="labelmenarquia">Menarquia:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="number" class="bodersueve" id="menarquia" class="control" value='<?php echo  $menarquia; ?>' min="0" name="menarquia" style="width:50px;">&nbsp;&nbsp;&nbsp;&nbsp;
								<label class="labeltalla">Sexarquia:</label>&nbsp;&nbsp;&nbsp;
								<input type="number" class="bodersueve" id="sexarquia" class="control" value='<?php echo  $sexarquia; ?>' min="0" name="sexarquia" style="width:50px;">&nbsp;&nbsp;&nbsp;&nbsp;
								<label class="labelnps">NPS:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input type="number" class="bodersueve" id="nps" class="control" value='<?php echo  $nps; ?>' min="0" name="nps" style="width:50px;">&nbsp;&nbsp; &nbsp;&nbsp;
								<label class="labelciclom">Ciclo Mestrual:</label>
								<input type="text" class="bodersueve" id="ciclo_mestrual" value='<?php echo  $ciclo_mestrual; ?>' class="control" name="ciclo_mestrual" style="width:100px;">
							</div>
							<div>
								<label class="labeltalla">Dismenorrea:</label>&nbsp;&nbsp;&nbsp;&nbsp;
								<select class="classic tamaño" id="dismenorrea" name="dismenorrea" data-style="btn-primary" style="width:130px;">
									<?php $dismenorrea = trim($dismenorrea); ?>
									<?php if ($dismenorrea == 't') : ?>
										<option value="1" selected>SI</option>
										<option value="2">NO</option>
									<?php elseif ($dismenorrea == 'f') : ?>
										<option value="2" selected>NO</option>
										<option value="1">SI</option>
									<?php else : ?>
										<option value=0 selected disabled>Seleccione</option>
										<option value="1">SI</option>
										<option value="2">NO</option>
									<?php endif; ?>
								</select>&nbsp;
								<label class="labelspo2">Eumenorrea:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<select class="classic tamaño" id="eumenorrea" name="eumenorrea" data-style="btn-primary" style="width:100px;">
									<?php $eumenorrea = trim($eumenorrea); ?>
									<?php if ($eumenorrea == 't') : ?>
										<option value="1" selected>SI</option>
										<option value="2">NO</option>
									<?php elseif ($eumenorrea == 'f') : ?>
										<option value="2" selected>NO</option>
										<option value="1">SI</option>
									<?php else : ?>
										<option value=0 selected disabled>Seleccione</option>
										<option value="1">SI</option>
										<option value="2">NO</option>
									<?php endif; ?>
								</select>
							</div>
							<div>
								<label class="labeltalla">Anticonceptivo Oral:</label>
								<select class="classic tamaño" id="anticon" name="anticonceptivo" data-style="btn-primary" style="width:100px;">
									<?php $ets = trim($ets); ?>
									<?php if ($ets == 't') : ?>
										<option value="1" selected>SI</option>
										<option value="2">NO</option>
									<?php elseif ($ets == 'f') : ?>
										<option value="2" selected>NO</option>
										<option value="1">SI</option>
									<?php else : ?>
										<option value=0 selected disabled>Seleccione</option>
										<option value="1">SI</option>
										<option value="2">NO</option>
									<?php endif; ?>
								</select>&nbsp;

								<label class="labelpeso">Detalles:</label>&nbsp;
								<textarea class="bodersueve textarea" id="detalles_anticon" style="width:410px;"><?php echo  $detalles_anticon; ?> </textarea>
							</div>

							<div>


								<label class="labelborrado" for="borrado" id="activo">DIU:&nbsp;&nbsp;&nbsp;&nbsp;</label>
								<?php if ($diu == 't') : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="diu" name="diu" checked>
								<?php else : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="diu" name="diu">
								<?php endif; ?>
								&nbsp;&nbsp;
								<label class="labelborrado" for="borrado" id="activo">T-COBRE:</label>
								<?php if ($t_cobre == 't') : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="t_cobre" name="t_cobre" checked>
								<?php else : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="t_cobre" name="t_cobre">
								<?php endif; ?>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


								<label class="labelborrado" for="borrado" id="activo">MIRENA:</label>

								<?php if ($mirena == 't') : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="mirena" name="mirena" checked>
								<?php else : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="mirena" name="mirena">
								<?php endif; ?>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label class="labelborrado" for="borrado" id="activo">ASPIRAL:</label>
								<?php if ($aspiral == 't') : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="aspiral" name="aspiral" checked>
								<?php else : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="aspiral" name="aspiral">
								<?php endif; ?>

								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<label class="labelborrado" for="borrado" id="activo">I.SUBDERMICO:</label>
								<?php if ($i_subdermico == 't') : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="i_subdermico" name="i_subdermico" checked>
								<?php else : ?>
									<input class="form-check-input" class="borrado" type="checkbox" id="i_subdermico" name="i_subdermico">
								<?php endif; ?>

							</div>
							<div>
								<label class="labelborrado" for="borrado" id="activo">OTRO:</label>
								<input type="text" class="bodersueve" id="otro" value='<?php echo  $otro; ?>' name="borrado" style="width:430px;">

								<label class="labelfecha_regla">T.Colocacion:</label>&nbsp;&nbsp;
								<input type="DATE" class="bodersueve" id="t_colocacion" class="control" value='<?php echo  $t_colocacion; ?>' style="width:120px;">


							</div>
							<div>
								<label class="labeltalla">E.T.S:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<select class="classic tamaño" id="ets" data-style="btn-primary" style="width:100px;">
									<?php $anticonceptivo = trim($anticonceptivo); ?>
									<?php if ($anticonceptivo == 't') : ?>
										<option value="1" selected>SI</option>
										<option value="2">NO</option>
									<?php elseif ($anticonceptivo == 'f') : ?>
										<option value="2" selected>NO</option>
										<option value="1">SI</option>
									<?php else : ?>
										<option value=0 selected disabled>Seleccione</option>
										<option value="1">SI</option>
										<option value="2">NO</option>
									<?php endif; ?>
								</select>&nbsp;



								<label class="labelspo2">Tipo E.T.S</label>&nbsp;
								<select class="classic tamaño" disabled="disabled" id="tipo_ets" name="tipo_ets" data-style="btn-primary" style="width:210px;">
									<option value=0 selected disabled>Seleccione</option>
									<option value="1">ACP</option>
									<option value="2">DIU</option>
								</select>&nbsp;&nbsp;&nbsp;
								<label class="labelpeso">Detalles:</label>
								<textarea class="bodersueve textarea" id="detalles_ets" style="width:142px;"> <?php echo  $detalle_ets; ?></textarea>
							</div>
							<div>
								<label class="texo_obster">Citologia:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<textarea class="bodersueve textarea" id="citologia" style="width:250px;"> <?php echo  $citologia; ?></textarea>&nbsp;&nbsp;&nbsp;
								<label class="texo_obster">Eco Mamario:</label>&nbsp;&nbsp;
								<textarea class="bodersueve textarea" id="eco_mamario" style="width:250px;"> <?php echo  $eco_mamario; ?></textarea>

							</div>
							<div>
								<label class="texo_obster">Mamografia:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<textarea class="bodersueve textarea" id="mamografia" style="width:250px;"> <?php echo  $mamografia; ?></textarea> &nbsp;&nbsp;
								<label class="texo_obster">Desintometria Osea:</label>
								<textarea class="bodersueve textarea" id="desintometria" style="width:215px;"> <?php echo  $desintometria; ?></textarea> &nbsp;&nbsp;
							</div>

						</fieldset>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" style="display: none;" id="btnagregar_Antecedentes_gineco_obstetricos">Guardar</button>
						<button type="button" class="btn btn-primary" style="display: none;" id="btnActualizar_Antecedentes_gineco_obstetricos">Actualizar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
					</div>



				</div>

			</div>
		</div>
	</div>



	<script>
		function mayus(e) {
			e.value = e.value.toUpperCase();
		}
	</script>


	</body>

	</html>
	<script>
		var id_especialidad = $('#id_especialidad').val();
		if (id_especialidad == '5') {
			//document.getElementById("cajas2").style.visibility = "visible"; 

			$("#cajas2").show();
			$("#prueba2").hide();
			$("#cajas3").hide();
			$("#cajas5").hide();
		} else if (id_especialidad == '1') {

			$("#cajas1").show();
			$("#prueba2").hide();


		} else if (id_especialidad == '2') {
			$("#prueba2").hide();
			$("#cajas3").show();
			$("#prueba1").show();


		} else if (id_especialidad == '6') {
			$("#prueba2").hide();
			$("#cajas4").show();


		} else if (id_especialidad == '4') {

			$("#cajas5").show();
			$("#prueba1").hide();


		}
	</script>

	<style type="text/css">
		option {
			font-family: verdana, arial, helvetica, sans-serif;
			font-size: 14px;
			border: 3;
			border-radius: 20%;
		}

		option {
			border-color: blueviolet;
		}
	</style>

	<script>
		var botones = document.querySelectorAll('.btn-expandir');
		var texto_expandir = document.querySelectorAll('.texto_expandir');
		botones.forEach((elemento, clave) => {
			elemento.addEventListener('click', () => {
				texto_expandir[clave].classList.toggle("abrir_cerrar")
			});

		});
	</script>

	<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->

	<script type="text/javascript">
		function valideKey(evt) {

			// code is the decimal ASCII representation of the pressed key.
			var code = (evt.which) ? evt.which : evt.keyCode;

			if (code == 8) { // backspace.
				return true;
			} else if (code >= 48 && code <= 57) { // is a number.
				return true;
			} else { // other keys.
				return false;
			}
		}
	</script>

	<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
	<script>
		// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
		$(function() {
			$("#fecha_del_dia").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});
			$("#fecha1").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});


		});
	</script>


	<script>
		$(document).ready(function() {

			var now = new Date();

			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);
			var today = day + "-" + month + "-" + now.getFullYear();
			// var today= (day)+"-"+(month)+"-"+now.getFullYear();
			// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
			$("#fecha_del_dia").val(today);
		});
	</script>
	<?= $this->endSection(); ?>