<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/tipomedicamentos.css" rel="stylesheet" type="text/css" />
<br>
<div class="container">


	<div class="row ">
		<div class="col-5">
		</div>
		<div class="col-5">
			<h3 class="center">Tipo de Inventario</h3>
		</div>

		<div class="col-2">
			<button id="btnAgregar" class="btn btn-primary">Agregar</button><br /><br />

		</div>

	</div>

	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row ">
		<div class="col-1">
		</div>
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<div class="col-lg-10">

			<table class="display" id="table_tipo_medicamentos" style="width:100%" style="margin-top: 20px">
				<thead>
					<tr>
						<td>Id</td>
						<td>Descripcion</td>
						<td>Fecha Registro</td>
						<td>Estatus</td>
						<td>Tipo de Inventario</td>
						<td class="text-center" style="width: 90px;">Acciones</td>
					</tr>
				</thead>
				<tbody id="lista_de_tipo_medicamentos">
				</tbody>
			</table>
		</div>
	</div>

	<!-- Ventana Modal -->
	<form action="" method="post" name="">
		<div class="modal fade" id="modalagregar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="modalagregar"> Registro de Tipo de Inventario</h4>
					</div>
					<div class="modal-body">
						<div id="datos_ajax_register"></div>
						<div>
							<input type="hidden" id='id' name='id'>
						</div>
						<div class="form-group">
							<label for="nombre0" class="control-label">Nombre </label>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="text" class="control sinborder" onkeyup="mayus(this);" autocomplete="off" id="tipomedicamento" style="width:250px;" required pattern="[aA-Za-z]+" autocomplete="off" name="tipomedicamento" value=''>
						</div>
						<div>
							<label for="control" class="control-label">Categoria</label>&nbsp;&nbsp;
							<select class="custom-select" style="width:245px;" id="cmbInventario" autocomplete="off" required>
								<option value="seleccione">seleccione</option>
							</select>
						</div>
						<br>
						<div>
							<label>Fecha </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

							<input type="text" disabled="disabled" class="sinborder" autocomplete="off" id="fecha" value=''>
							&nbsp;&nbsp;&nbsp;
						</div>
						<label for="borrado" id="activo">Activo</label>
						<input type="checkbox" class="" id="borrado" name="borrado" value='false'>



						<!-- <div class="form-check">
        				
        				
      				</div> -->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
						<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
						<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos </button>

					</div>
				</div>
			</div>
	</form>

<!-- 
METODO QUE TOMA LOS VALORES ANTERIORES DEL FORMULARIO  -->
<!-- DESCRIPCIN MEDICAMETNO -->
<input type="hidden" class="control sinborder" onkeyup="mayus(this);" autocomplete="off" id="tipomedicamento_anterior" style="width:250px;" required pattern="[aA-Za-z]+" autocomplete="off" name="tipomedicamento" value=''>
<!-- BORRADO -->
<input type="hidden" class="borrado" id="borrado_anterior" name="borrado" value=''>	&nbsp;&nbsp;&nbsp;&nbsp; 
<label for="control" class="control-label">Categoria</label>&nbsp;&nbsp;
<!-- CATEGORIA -->
<select class="custom-select" style="width:245px;" id="cmbInventario_anterior" autocomplete="off" required>
<option value="seleccione">seleccione</option>
</select>

<script>
		document.getElementById('cmbInventario_anterior').style.display = 'none';
</script>

	<script>
		function mayus(e) {
			e.value = e.value.toUpperCase();
		}
	</script>
	<script>
		$(document).ready(function() {

			var now = new Date();

			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);
			var today = day + "/" + month + "/" + now.getFullYear();
			// var today= (day)+"-"+(month)+"-"+now.getFullYear();
			// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
			$("#fecha").val(today);
		});
	</script>


	<script>
		$(function() {
			//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
			$("#fecha").datepicker({
				dateFormat: 'dd/mm/yy',
				changeMonth: true,
				changeYear: true
			});
		});
	</script>

	<?= $this->endSection(); ?>