<?= $this->extend('menu/supermenu') ?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content') ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="/css/salidas.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<style>
		table.dataTable thead,
		table.dataTable tfoot {
			background: linear-gradient(to right, #4a779c, #7e9ab1, #5f7a91);
		}
	</style>
	<div class="row">
		<div class="col-4 ">
		</div>

		<div class="col-6 ">

			<h4> Registro de Entradas / Para dar Salidas</h4>

		</div>

		<div class="col-2 ">
			<button id="btnRegresar" class="btn btn-secondary">Regresar</button>
		</div>
	</div>

	<div class="row">
		<div class="col-0 ">
		</div>

		&nbsp;&nbsp;&nbsp;&nbsp;<h5>Descripcion:&nbsp;&nbsp;&nbsp;</h5>
		<div>
			<input type="text" disabled="disabled" class="sinborder" style="width:300px;" value='<?php echo $medicamento; ?>'>
		</div>
		<h5>&nbsp;&nbsp;Control:&nbsp;&nbsp;&nbsp;&nbsp;</h5>
		<div>
			<input type="text" disabled="disabled" id="id_control" class="sinborder" style="width:200px;" value='<?php echo $control; ?>'>
			<input type="hidden" disabled="disabled" id="id_control_x" class="form-control" style="width:200px;" value='<?php echo $id_control; ?>'>
		</div>
	</div>

	<div class="row">
		<div class="col-0 ">
		</div>

		&nbsp;&nbsp;&nbsp;&nbsp;<h5>Medicamento Crónico:&nbsp;&nbsp;&nbsp;</h5>
		<div>
			<input type="text" disabled="disabled" class="estatus_croni" style="width:50px;" value='<?php echo $estatus_med_cronico; ?>'>
		</div>

	</div>

	<div class="row detalles">
		<div class="col-md-4">
			<button type="button" disabled="disabled" class="btn-info">
				<span class="badge badge-light" id="labelsalidas">Salidas:</span>
			</button>
			<input id="total_salidas" class="sinborder" readonly="readonly" disabled="disabled" class="text" value='<?php echo $total_salidas; ?>' style="width:60px;">
			<button type="button" disabled="disabled" class=" btn-info">
				<span class="badge badge-light">Stock:</span>
			</button>
			<input id="total_stock" class="sinborder" readonly="readonly" disabled="disabled" class="text" value='<?php echo $total_stock; ?>' style="width:80px;">

		</div>

	</div>


	<div class="row ">

		<div class="col-0">
		</div>
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<div class="col-lg-11">
			<table id="table_entradas" class="display" style="width:100%">

				<thead>
					<tr>
						<th style="width:3%">NªE</th>
						<th style="width:17%">Fecha de Entrada</th>
						<th style="width:17%">Fecha Vencimiento</th>
						<th>Entradas</th>
						<th>Salidas</th>
						<th>Stock</th>
						<th>Estatus</th>
						<th style="width:100px">Acciones</th>

					</tr>
				</thead>
				<tbody id="lista_entradas_medicamento">
				</tbody>
				</thead>
			</table>

		</div>
	</div>

	<!-- ventana modal  -->


	<form action="" method="post" name="">
		<div class="modal" tabindex="-1" id="modal_retirar" data-keyboard="false" data-backdrop="static">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<h5 class="modal-title text-center" id="titulo">Retiro de Inventario</h5>
					<div class="modal-header">
						<div class="row">


							<div class="col-lg-12">

								<button type="button" id="btnclose" class="btnclose" data-bs-dismiss="modal">
									<img src="<?php echo base_url(); ?>/img/11.png" class="imagencerrar" style="width:25px;">
								</button>
								<fieldset class="buscar">
									<legend class="legend">SELECCIONE EL TIPO DE BENEFICIARIO</legend>
									<div>
										<!-- ******TITULAR*** -->
										&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input" type="radio" style="width:25px;" name="inlineRadioOptions" id="Radio1" value="1">
										&nbsp;<label class="titular">TITULAR</label>
										</button>
										<!-- ******FAMILIAR*** -->
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input" type="radio" style="width:25px;" name="inlineRadioOptions" id="Radio2" value="2">
										&nbsp;<label class="familiar">FAMILIAR</label>&nbsp;
										</button>
										<!-- ******CORTESIA*** -->
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="form-check-input" type="radio" style="width:25px;" name="inlineRadioOptions" id="Radio3" value="3">
										&nbsp;<label class="cortesia">CORTESIA</label>
										</button>

									</div>
									<div id="verificar2">
										<label class="labelcedula"> &nbsp;&nbsp;Cedula</label>
										<input type="text" autocomplete="off" minlength="7" maxlength="11" onkeypress="return valideKey(event);" required pattern="[0-9]+" id="cedula_beneficiario" class="control cedula" style="width:160px;" value=''>
										<button type="button" class=" btn-primary btnbotones" id="btntitulares">Buscar</TItle> </button>
										<button type="button" class=" btn-primary btnbotones" id="btnfamiliares">Buscar</button>
										<button type="button" class=" btn-primary btnbotones" id="btncortesia">Buscar</button>
										<br>
										<br>

										<label class="labelcedulat" id="labelcedulat"> &nbsp;&nbsp;Titular:</label>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" autocomplete="off" id="nombre_titular" readonly="readonly" class="control nombre_titular" style="width:310px;" value=''>



									</div>
									<br>
								</fieldset>
							</div>




						</div>
						<img src="<?= base_url() ?>/img/n1.jpeg" width="100" alt="Avatar" id="n1">
						<div class="row">
							<div class="col-lg-2">
							</div>
							<div class="col-lg-8">
								<label class="labelbuscarfecha"> Fecha Salida</label>
								<input type="text" class="fecha" style="width:100px;" autocomplete="off" name="fecha" id="fecha" style=" z-index: 1050 !important;">
							</div>
						</div>
					</div>




					<div class="col-lg-8">
						<button type="button" id="btncerrarbeneficiario" class="btnclose btncerrarbeneficiario" data-bs-dismiss="modal">
							<!-- <img src="<php echo base_url();?>/img/11.png" class="imagencerrar" style="width:25px;"> -->
						</button>



						<fieldset class="datosbeneficiario" id="datosbeneficiario">
							<legend class="legend"> DATOS DEL BENEFICIARIO</legend>
							<div>
								<label class="nombret">NOMBRE</label>
								<input type="text" disabled="disabled" id="nombre_beneficiario" autocomplete="off" class="control nombre" value=''>
								<label class="apellidot">APELLIDO:</label>
								<input type="text" disabled="disabled" style="width:170px;" id="apellido_beneficiario" autocomplete="off" class="control apellido" value=''>


							</div>
							<br>
						</fieldset>
						<br>
						<fieldset class="datosbeneficiario" id="datosbeneficiario">
							<legend class="legend"> DATOS DEL MEDICO</legend>
							<div>
								&nbsp;&nbsp;
								<label class="labelespecialidad">Especialidad&nbsp;&nbsp; </label>
								<select class="custom-select" style="width:270px;" id="especialidad" autocomplete="off" required>
									<option value="seleccione">seleccione</option>
									</h5>
								</select>
								<label class="labelmedicotratante" class="control-label">Medico &nbsp;&nbsp; </label>
								<select class="custom-select" style="width:270px;" id="cmbmedicosreferidos" autocomplete="off" required>
									<option value="0">seleccione</option>
								</select>

							</div>

						</fieldset>
						<br>
						<fieldset class="datosbeneficiario" id="datosbeneficiario">
							<legend class="legend"> DATOS DEL MEDICAMENTO</legend>
							<div>
								<label class="descripciont">DESCRIPCION DEL MEDICAMENTO:</label>
								<input type="text" disabled="disabled" id="descripcion" style="width:400px;" class="control descripcion" value='<?php echo $medicamento; ?>'>
								<br>
								<br>

								<label class="labelstock">STOCK</label>
								&nbsp; &nbsp; <input type="text" readonly="readonly" style="width:50px;" autocomplete="off" class="control stock" id='stock' name='stock' value=''>
								<label class="labelcantidad" id="labelcantidad">CANTIDAD</label>
								<input type="text" id='cantidad' class="cantidad" style="width:80px;" autocomplete="off" name='cantidad' value=''>
								<label class="labelcronico">MEDICAMENTO CRÓNICO</label>
								<input type="text" disabled="disabled" id="med_cronico" style="width:50px;" class="control med_cronico" value='<?php echo $estatus_med_cronico; ?>'>
								<button type="button" class=" btn-primary btnGuardarSalida" id="btnGuardarSalida">Guardar </button>
								<br>
								<br>


							</div>
						</fieldset>
						<br>



						<div class="form-check form-check-inline">
							<input type="hidden" id="tipo_beneficiario" class="control" value=''>
							<input type="hidden" id="cedula_titular" class="control" value=''>
						</div>
						<div id="cajas">
							<input type="hidden" id='id_entrada' name='id_entrada' value=''>
							<input type="hidden" id='id_medicamento' name='id_medicamento' value='<?php echo $id_medicamento; ?>'>
						</div>


						<br>



					</div>

				</div>
			</div>
		</div>

</div>
</div>
</form>

<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script> -->
<script>
	$(document).ready(function() {
		$('#datosbeneficiario').prop('disabled', true);

	});
</script>

<script>
	function sanear(e) {
		let contenido = e.target.value;
		e.target.value = contenido.toUpperCase().replace(" ", "");
	}
</script>

<script>
	document.getElementById("cedula_beneficiario").addEventListener('keyup', sanear);
</script>




<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
<script type="text/javascript">
	function valideKey(evt) {

		// code is the decimal ASCII representation of the pressed key.
		var code = (evt.which) ? evt.which : evt.keyCode;

		if (code == 8) { // backspace.
			return true;
		} else if (code >= 48 && code <= 57) { // is a number.
			return true;
		} else { // other keys.
			return false;
		}
	}
</script>
<script>
	function mayus(e) {
		e.value = e.value.toUpperCase();
	}
</script>



<script>
	$(document).ready(function() {

		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var today = day + "/" + month + "/" + now.getFullYear();
		$("#fecha").val(today);
	});
</script>
<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script>
	// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
	$(function() {
		$("#fecha").datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: 'today:2060',
			dateFormat: 'dd/mm/yy',
		})
		$("#fecha1").datepicker({
			dateFormat: 'dd/mm/yy',
			changeMonth: true,
			changeYear: true
		});


	});
</script>

<?= $this->endSection(); ?>
<!-- data-backdrop= "static"= no deja hacer click fuera del modal  -->