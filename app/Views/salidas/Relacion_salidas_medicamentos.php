<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
 
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="/css/relacion_salidas_medicamento.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	
	<div class="row">

    <div class="col-3">

    </div>

    <div class="col-8">

        <h5 class="center">RELACION SALIDAS DE MEDICAMENTOS</h5>

    </div>

    <input type="hidden" id="usuario" autocomplete="off" style="width:102px;" value='<?= session('nombreUsuario');?>'>

</div>

<div class="row">

    <div class="col-md-5">

        <div class="form-group">

            <label for="min">Desde</label>&nbsp;

            <input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('y-m-d');?>" name="desde" id="desde">&nbsp;&nbsp;

            <label for="hasta">Hasta</label>&nbsp;&nbsp;

            <input type="date" class="bodersueve" style="width:135px;" value="<?php echo date('y-m-d');?>" name="hasta" id="hasta">

        </div>

    </div>

    <div class="col-md-4">

        <div class="form-group">

            <label class="labelcategoria">Categoria</label>&nbsp;

            <input type="hidden" id="categoria" autocomplete="off" style="width:60px;">

            <select class="custom-select" style="width:180px;" id="cmbCategoria" autocomplete="off" required>

                <option value="0">seleccione</option>

            </select>

        </div>

    </div>

    <div class="col-md-3">

        <div class="form-group">

            <label class="labelcronico" disabled="disabled">Crónico</label>&nbsp;

            <select class="custom-select" style="width:120px;" disabled="disabled" id="cronico" name="cronico" data-style="btn-primary">

                <option value="0" selected disabled>seleccione</option>

                <option value="1">SI</option>

                <option value="2">NO</option>

            </select>

           
        </div>

    </div>
			

</div>
  <div class="row">
	
		<div class="col-md-11">	
			<label class="labelbeneficiario">T.Beneficiario</label>&nbsp;
			<select class="custom-select " style="width:170px;"  id="beneficiario" name= "beneficiario" data-style="btn-primary" >
				<option value="0"selected disabled>seleccione</option> 
				<option value="1">TITULAR</option> 
				<option value="2">FAMILIAR</option> 
				<option value="3">CORTESIA</option> 
			</select>&nbsp;&nbsp;
			<label class="labelsexo">Sexo</label>&nbsp;
            <select class="custom-select" style="width:120px;" id="sexo" name="sexo" data-style="btn-primary">

                <option value="0" selected disabled>seleccione</option>

                <option value="1">MASCULIINO</option>

                <option value="2">FEMENINO</option>

            </select>

			<label class="labelbeneficiario">Medico</label>&nbsp;
			<select class="custom-select " style="width:195px;"  id="medico" name= "medico" data-style="btn-primary" >
				<option value="0"selected disabled>seleccione</option> 
			</select>&nbsp;&nbsp;

			<label class="labelbeneficiario">Entes Adscritos</label>&nbsp;&nbsp;
			<select class="custom-select " disabled="disabled" style="width:115px;"  id="entes_adscritos" name= "entes_adscritos" data-style="btn-primary" >
				<option value="0">seleccione</option> 
				
			</select>	
		</div>
	</div>
				
		
<style>
   table.dataTable thead, table.dataTable tfoot {
           background: linear-gradient(to right, #4a779c,#7e9ab1,#5f7a91);
       }
   </style>
		<div class="row">
			<div class="col-md-5">	
			</div>
			<div class="col-md-3">
				<button id="btnfiltrar"style="visibility:visible;" class="btn btn-primary btn-sm">Filtrar </button>&nbsp;
				<button id="btnlimpiar" class="btn btn-primary btn-sm">Limpiar</button>&nbsp;
				<button id="filtrocortesia" style="visibility:hidden;"  class="btn btn-primary btn-sm">Filtrar</button>&nbsp;	
			</div>
		</div>	
		</div>	



	<div class="col-lg-12">
		<div class="salidas">
			<table class="display" id="table_relacion_salidas" style="width:100%" style="margin-top: 20px">
				<thead>
					<tr>
						<td class="text-center"style="width:5%">FECHA </td>	
						<td class="text-center" style="width:10%">INVENTARIO</td>	
						<td class="text-center" style="width:5%">CATEGORIA</td>
						<td  class="text-center"style="width:5%">CRONICO</td>					
						<td  class="text-center"style="width:5%">CANT</td>				
						<td class="text-center" style="width:4%">BENEFICIARIO</td>	
						<td class="text-center" style="width:4%">TIPO  </td>	
						<td class="text-center" style="width:1%">CED_T</td>
						<td class="text-center" style="width:4%">NOMBRE_T</td>  
						<td class="text-center"style="width:1%">ENTREGADO</td>	
						<td class="text-center"style="width:1%">MEDICO</td>		 	
					</tr>
				</thead>
				<tbody id="lista_notas_Entregas">
				</tbody>
			</table>
		</div>
		
		<div class="salidas_entes">
			<table class="display"  id="table_relacion_salidas_entes" style="width:100%" style="margin-top: 20px">
				<thead>
					<tr>
						<td class="text-center"style="width:5%">FECHA </td>	
						<td class="text-center" style="width:10%">INVENTARIO</td>	
						
						<td class="text-center" style="width:5%">CATEGORIA</td>
						<td  class="text-center"style="width:5%">CRONICO</td>					
						<td  class="text-center"style="width:5%">CANT</td>				
						<td class="text-center" style="width:4%">BENEFICIARIO</td>	
						<td class="text-center" style="width:4%">TIPO  </td>	
						<td class="text-center" style="width:1%">CED_T</td>
						<td class="text-center" style="width:4%">NOMBRE_T</td>  
						<td class="text-center"style="width:1%">ENTREGADO</td>	
						<td class="text-center"style="width:1%">MEDICO</td>		 

						
					</tr>
				</thead>
				<tbody id="lista_notas_Entregas">
				</tbody>
			</table>

		</div>
	</div>
	
		

<!-- <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
<script> 
				// ***CONFIGURACION DE DATAPICKER :( MOSTRAR AÑO Y DAR FORMATO DEFECHA D-M-A)*****
			$( function() {
				 $( "#fecha" ).datepicker({dateFormat:'dd/mm/yy',changeMonth:true, changeYear:true});	
				 $( "#fecha1" ).datepicker({dateFormat:'dd/mm/yy',changeMonth:true, changeYear:true});
					
					
			} );
		</script>


<script>
	$(document).ready(function() {

var now = new Date();

var day = ("0" + now.getDate()).slice(-2);
var month = ("0" + (now.getMonth() + 1)).slice(-2);
var today= day+"/"+month+"/"+now.getFullYear();
// var today= (day)+"-"+(month)+"-"+now.getFullYear();
// var today = now.getFullYear()+"-"+(month)+"-"+(day) ;
$("#fecha").val(today);
});
</script>

<!-- ***** FUNCION PARA SOLO NUMEROS***-** -->
<script type="text/javascript">
function valideKey(evt){
    
    // code is the decimal ASCII representation of the pressed key.
    var code = (evt.which) ? evt.which : evt.keyCode;
    
    if(code==8) { // backspace.
      return true;
    } else if(code>=48 && code<=57) { // is a number.
      return true;
    } else{ // other keys.
      return false;
    }
}
</script> 
<?= $this->endSection(); ?>