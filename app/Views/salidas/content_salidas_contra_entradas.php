<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
  
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="/css/reversarsalidas.css" rel="stylesheet" type="text/css" />
	<br>
<div class="container">
<style>
   table.dataTable thead, table.dataTable tfoot {
           background: linear-gradient(to right, #4a779c,#7e9ab1,#5f7a91);
       }
   </style>
    <div class="row ">
		<div class="col-4 ">
		</div>
		<div class="col-6 ">
		  	<h4> Detalle de Salidas Contra Entrada :</h4>
		   	<input type="hidden"disabled="disabled" class="form-control" id='id_entrada' name='id_entrada' style="width:100px;" value='<?php echo $id_entrada;?>'>
			   <input type="hidden"disabled="disabled" class="form-control" id='id_medicamento' name='id_medicamento' style="width:100px;" value='<?php echo $id_medicamento;?>'>  
	    </div>	 
		<div class="col-2 ">
			<button id="btnRegresar" class="btn btn-primary">Regresar</button>
		</div>
	</div>

	<div class="row">
		<div class="col-1 ">
		</div>
		    
		<h5>Descripcion:&nbsp;&nbsp;&nbsp;</h5>
		<div>
			<input type="text" disabled="disabled" class="form-control" style="width:600px;" value='<?php echo $medicamento;?>'> 
		</div>
	    <div class="alert alert-success" style="display: none;"></div>
	    <div class="alert alert-danger" style="display: none;"></div>	
        <br>
	</div> 
	
	  <br>

	<div class="row ">
		<div class="col-lg-1">	
		</div>
		<div class="col-lg-10">	

		<table id="table_salidas" class="display"style="width:100%" >


	
			<thead>
				<tr>
					<td>Id</td>
					<td>Fecha de Salida</td>	
					<td>Beneficiario</td>		
					<td>Cantidad</td>
					<td>Acciones</td>
				</tr>
			</thead>
			<tbody id="lista_salidas_medicamento">
			</tbody>
		</table>
	</div>
</div>

<br>
<br>




<div class="modal fade" id="modal_reversar"  >
  <div class="modal-dialog modal-md">
    <form id="new-seguimiento" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Registro de Salidas</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <input type="hidden" name="" id="idsegcas" value="">
        <div class="modal-body">
					<input type="hidden" id='id_medicamento' name='id_medicamento' value='<?php echo $id;?>'> 
					<input type="hidden" id='id_salida' name='id_salida' value=''> 
					<input type="hidden" id='id_entrada' name='id_entrada' value='<?php echo $id_entrada;?>'> 
					<label for="labelobservacion" id ="labelobservacion" >Observacion</label>	
					<textarea class="bodersueve" id="observacion" name="observacion" > </textarea>  
        </div>
		
        <div class="modal-footer justify-content-between">			 		
		<div>
		<label for="activo" id ="activo" >Reversar</label>
			<input type="checkbox" class="" id="borrado_reverso" name="borrado_reverso" value='false'>  
		</div>
			<input type="hidden" id='cantidad' autocomplete="off"name='id_salida' value=''> 
			<button type="button" class="btn btn-primary" id="btnActualizar_reversar">Actualizar datos</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </form>
  </div>
  <!-- /.modal-dialog -->
</div>



















	

		


<script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>
<?= $this->endSection(); ?>