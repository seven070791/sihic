<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">    
  
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="/css/categoria.css" rel="stylesheet" type="text/css" />
<div class="container">
	<br>
	<br>


	<div class="row ">
  <div class="col-5">
  </div>
  	<div class="col-5">
	<h3 class="center">Categoria de Inventario</h3>
	</div>	
	
	<div class="col-2">
	<button id="btnAgregar" class=" btn-primary btnAgregar">Agregar</button><br /><br />
	<button id="btnRegresar" class=" btn-secondary btnRegresar">Regresar</button><br /><br />
		
  </div>
  </div>
  <style>
   table.dataTable thead, table.dataTable tfoot {
           background: linear-gradient(to right, #4a779c,#7e9ab1,#5f7a91);
       }
   </style>

<div class="row">   
	<div class="col-lg-1">
	</div>
	<div class="col-lg-10">
		<!--table class="table table-hover table-bordered" id="table_roles" style="margin-top: 20px"-->
		<table class="display" id="table_categoria" style="width:100%" style="margin-top: 20px">
			<thead>
				<tr>
					<td >ID</td>
					<td style="width:40%"  >Compuesto Activo</td>	
					<td>Fecha de Creacion </td>				
					<td>Estatus </td>				
					<td class="text-center" style="width: 90px;">Acciones</td>
				</tr>
			</thead>
			<tbody id="lista_de_categoria">
			</tbody>
		</table>
	</div>
</div>


<!-- Ventana Modal -->
<form action="" method="post" name ="">
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" data-backdrop= "static">
    	<div class="modal-dialog" role="document">
      		<div class="modal-content">
        		<div class="modal-header">
					<div class="row">
						<div class="col-6">

			<h4 class="modal-title " id="modal"> Categoria</h4>
			</div>
			</div>
					   <input type="hidden"autocomplete="off" class="form-control categoria" id="id_categoria" value="" name="id_categoria" style="width:80px;" placeholder="">
        		</div>
 			<div class="modal-body">
   				<div id="datos_ajax_register"></div>
				   <label for="nombre0" class="control-label">Descripcion</label>
				   <input type="text" class="sinborder" onkeyup="mayus(this);"  class="form-control "autocomplete="off" id="categoria" value="" name="categoria" style="width:250px;" placeholder="">
				 <br>  
				   <label class="activo"id="activo" >Activo</label>	
				<input type="checkbox" class="borrado" id="borrado" name="borrado" value='false'>					 
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal" id="btnCerrar" name="btnCerrar">Cerrar</button>
				<button type="button" class="btn btn-primary" id="btnGuardar">Guardar datos</button>
				<button type="button" class="btn btn-primary" id="btnActualizar">Actualizar datos</button> 		      
    		</div>
			</div>
			</div>
  		</div>
	</div>
</form>
<input type="hidden" class="sinborder" onkeyup="mayus(this);"  class="form-control "autocomplete="off" id="categoria_anterior" value="" name="categoria" style="width:250px;" placeholder="">
<input type="hidden" class="form-control " id="borrado_anterior"  value='false'>	
<script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>
<!-- <script>
			$( function() {
				//$( "#fecha" ).datepicker({changeMonth:true, changeYear:true});	
				$( "#fecha" ).datepicker({dateFormat: 'dd/mm/yy',changeMonth:true, changeYear:true});			
			} );
		</script> -->
<?= $this->endSection(); ?>