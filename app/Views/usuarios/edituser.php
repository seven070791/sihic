<?= $this->extend('menu/supermenu')?>
<!-- INICIO DE LA SECION CONTENT -->
<?= $this->section('content')?>
<link href="/css/editarusuario.css" rel="stylesheet" type="text/css" /> 

        <?php  
          if (empty($_GET['alert'])) 
          {
            echo "";
          } 
          elseif ($_GET['alert'] == 1)
          {
            echo "<div class='alert alert-danger alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                <h4>  <i class='icon fa fa-times-circle'></i> Error al entrar!</h4>
               Usuario o la contraseña es incorrecta, vuelva a verificar su nombre de usuario y contraseña.
              </div>";
          }
          elseif ($_GET['alert'] == 2)
          {
            echo "<div class='alert alert-success alert-dismissable'>
                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                <h4>  <i class='icon fa fa-check-circle'></i> Exito!!</h4>
              Has salido con éxito.
              </div>";
            }?>

  <div class="row">  
    <div class="col-4">
    </div>
    <div class="col-lg-4">
      <br>
      <h3> <p class="login-box-msg"><i class="fa fa-user icon-title"></i>Edicion de  usuario</p></h3>  
      <section class="form-login"> 
      <form action="/User_controllers/ActualizarUsuario" method="post">
              <?= session()->getFlashdata('error') ?>
              <?= service('validation')->listErrors() ?>
              <?= csrf_field() ?>  
              <div class="input-group">
                <label class="input-fill">
                  <i class="		fas fa-shield-alt"></i>   
                  <input type="text" id="cedula" name="cedula"  minlength="7" maxlength="8"  size="8" required pattern="[0-9\s]+" value="<?= esc($infousuarios['cedula'])?> "/>
                </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                <input type="text" id="nombre"  name="nombre" onkeyup="mayus(this);" required pattern="[aA-Za-z]+" value="<?= esc($infousuarios['nombre'])?>">
                  <i class="	fas fa-portrait"></i>   
                  </label>
                </div>
              <div class="input-group">
                <label class="input-fill">
                <input type="text" id="apellido"  name="apellido"onkeyup="mayus(this);"required pattern="[aA-Za-z]+"  value="<?= esc($infousuarios['apellido'])?>">
                  
                  <i class="far fa-id-badge"></i>   
                  </label>
              </div>
              <div class="input-group">
                <label class="input-fill">
                <input type="text" id="username"  name="usuario"  required pattern="[aA-Za-z0-9\s]+"value="<?= esc($infousuarios['usuario'])?> ">
                  <!-- <span class="input-label">Username</span> -->
                  <i class="	fas fa-user-circle"></i>   
                  </label>
                </div>
              <div class="input-group">
                <label class="input-fill">
                <input type="password" id="password" name="password" required  value="<?= esc($infousuarios['password'])?>">
                  <!-- <span class="input-label">Password</span> -->
                  <i class="	fas fa-lock"></i>   
                </label>
              </div>
            
              <div class="row">
              <div class="col-5">
              
             <label ><H6>Nivel  Usuario</H6> </label> 
                </div>
                <div class="col-6">
                  
                
              <div class="input-group-fluid">
                <label class="input-fill">
                <label for="nombre0" class="label">Nivel Usuario</label>
                <select class="form-control tipousuario " name="tipousuario" id="tipousuario" required>
                <?php foreach($tipos as $tipo): ?>
                     <?php if($infousuarios['tipousuario']===$tipo['id']): ?>
                       <option value="<?= $tipo['id']; ?>" selected><?= $tipo['nivel_usuario']; ?></option>
                       <?php else: ?>
                       <option value="<?= $tipo['id']; ?>"><?= $tipo['nivel_usuario']; ?></option>
                       <?php endif; ?>
                <?php endforeach; ?>   
              </div>
            
              </select>

              <label class="input-fill">
 
          <?php if($infousuarios['borrado']=='f'): ?>
     
       <input class="form-check-input" type="checkbox"   value="<?= $infousuarios['borrado']?>"  style="width: 18px;"id="borrado" name="borrado" >
          <?php else: ?>
          <input class="form-check-input" type="checkbox" value="<?= $infousuarios['borrado']?>" style="width: 18px;" id="borrado" name="borrado" checked>
          <?php endif; ?>
         
     </div>

   
        <label id="bloquear" for="bloquear" >Bloqueado </label>

            <input type="hidden" class="form-control" name="id" value="<?= esc($infousuarios['id'])?> ">
            <div style="text-align: right;width:300px">
            <input type="submit" value="Actualizar" class=" btn-primary actualizar" />
            
     
              </form>      
   
    
      </section>
    
   
  </div> 
  <script>
  function mayus(e) {
    e.value = e.value.toUpperCase();
}
</script>
<script>
          $("#cedula").keyup(function()
        {              
          var ta      =   $("#cedula");
          letras      =   ta.val().replace(/ /g, "");
          ta.val(letras)
        });
          $("#nombre").keyup(function()
        {              
          var ta      =   $("#nombre");
          letras      =   ta.val().replace(/ /g, "");
          ta.val(letras)
        });

          $("#apellido").keyup(function()
        {              
          var ta      =   $("#apellido");
          letras      =   ta.val().replace(/ /g, "");
          ta.val(letras)
        });
          $("#username").keyup(function()
        {              
          var ta      =   $("#username");
          letras      =   ta.val().replace(/ /g, "");
          ta.val(letras)
        });
        </script>

    <!-- jQuery 2.1.3 -->
    <script src="/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="/js/bootstrap.min.js" type="text/javascript"></script>

  </body>
</html>
              
<?= $this->endSection()?>

   