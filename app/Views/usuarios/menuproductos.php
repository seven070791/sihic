<!DOCTYPE html>
  <html lang="en">
    <head>
    <meta charset="UTF-8">
    
	
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
    <link rel="stylesheet" href="/css/main.css" />
  </head>
  <body>
    <!-- ****************************AQUI INICIA EL HEADER*********************************** -->
    <header id="header">
      <div class="logo pull-left"> Servico Medico</div>
      <div class="header-content">
      <div class="header-date pull-left">
        <strong><?php echo date("d/m/Y  g:i a");?></strong>
      </div>
      <div class="pull-right clearfix">
        <ul class="info-menu list-inline list-unstyled">
          <li class="profile">
            <a href="#" data-toggle="dropdown" class="toggle" aria-expanded="false">
              
              
            </a>
            <ul class="dropdown-menu">
              <li>
                  
                      <i class="glyphicon glyphicon-user"></i>
                      Perfil
                  </a>
              </li>
             <li>
                 <a href="edit_account.php" title="edit account">
                     <i class="glyphicon glyphicon-cog"></i>
                     Configuración
                 </a>
             </li>
             <li class="last">
                 <a href="logout.php">
                     <i class="glyphicon glyphicon-off"></i>
                     Salir
                 </a>
             </li>
           </ul>
          </li>
        </ul>
      </div>
     </div>
    </header>
<!-- ****************************AQUI FINALIZA EL HEADER*********************************** -->
    <div class="sidebar">

    <ul>
  <li>
    <a href="admin">
      <i class="glyphicon glyphicon-home"></i>
      <span> Inicio</span>
    </a>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-user"></i>
      <span> Usuarios</span>
    </a>
    <ul class="nav submenu">
      
      <li><a href="/admin">Administrar usuarios</a> </li>
      
      <li><a href="creargrupo">Administrar grupos</a> </li>
   </ul>
  </li>
 
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-th-large"></i>
      <span>Farmacia</span>
    </a>
    <ul class="nav submenu">
       <li><a href="product.php">Administrar productos</a> </li>
       <li><a href="add_product.php">Agregar productos</a> </li>
	<li><a href="rproduct.php">Retirar productos</a> </li>
       <li><a href="add_product.php">Consultas</a> </li>
	 <li><a href="add_product.php">Reportes</a> </li>

   </ul>
  </li>
 
  <li>

    
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-signal"></i>
       <span>Historial Clinico </span>
      </a>
      <ul class="nav submenu">
        <li><a href="sales_report.php">Psicologia Primaria </a></li>
        <li><a href="monthly_sales.php">Medicina Interna </a></li>
        <li><a href="daily_sales.php">Paciente Pediatrico </a> </li>
        <li><a href="monthly_sales.php">Medicina General </a></li>
        <li><a href="daily_sales.php">Ginecologia y Obstetricia <br>prueba de contexto </br> </a> </li>
      </ul>


  </li>
</ul>

<ul>
  
  <li>
    <a href="categorie.php" >
      <i class="glyphicon glyphicon-indent-left"></i>
      <span>Categorías</span>
    </a>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-th-large"></i>
      <span>Productos</span>
    </a>
    <ul class="nav submenu">
       <li><a href="product.php">Administrar productos</a> </li>
       <li><a href="add_product.php">Agregar producto</a> </li>
   </ul>
  </li>
  <li>
    <a href="media.php" >
      <i class="glyphicon glyphicon-picture"></i>
      <span>Media</span>
    </a>
  </li>
</ul>

      
    <ul>
  
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-th-list"></i>
       <span>Ventas</span>
      </a>
      <ul class="nav submenu">
         <li><a href="sales.php">Administrar ventas</a> </li>
         <li><a href="add_sale.php">Agregar venta</a> </li>
     </ul>
  </li>
  <li>
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-signal"></i>
       <span>Reporte de ventas</span>
      </a>
      <ul class="nav submenu">
        <li><a href="sales_report.php">Ventas por fecha </a></li>
        <li><a href="monthly_sales.php">Ventas mensuales</a></li>
        <li><a href="daily_sales.php">Ventas diarias</a> </li>
      </ul>
  </li>
</ul>




   </div>



 
  </li>
 
  <li>

    
    <a href="#" class="submenu-toggle">
      <i class="glyphicon glyphicon-signal"></i>
       <span>Historial Clinico </span>
      </a>
      <ul class="nav submenu">
        <li><a href="sales_report.php">Psicologia Primaria </a></li>
        <li><a href="monthly_sales.php">Medicina Interna </a></li>
        <li><a href="daily_sales.php">Paciente Pediatrico </a> </li>
        <li><a href="monthly_sales.php">Medicina General </a></li>
        <li><a href="daily_sales.php">Ginecologia y Obstetricia <br>prueba de contexto </br> </a> </li>
      </ul>


  </li>
</ul>



<!-- ************************AQUI INICIA EL FOOTER************************************ -->
</div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
  <script type="text/javascript" src="/js/functions.js"></script>
  </body>
</html>

<?php if(isset($db)) { $db->db_disconnect(); } ?>

<!-- ************************AQUI FINALIZA EL FOOTER************************************ -->
hola
